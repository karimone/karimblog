Title: Mi licenzio dalla Lituania
Date: 2007-04-26
Category: Lituania
Slug: mi-licenzio-dalla-lituania
Author: Karim N Gorjux
Summary: Con grande piacere pubblico la mail di una persona che ha provato in tutte le maniere di "sfondare" in Lituania cercando di crearsi una nuova vita, sposarsi, fare affari. Insomma, una persona[...]

Con grande piacere pubblico la mail di una persona che ha provato in tutte le maniere di "sfondare" in Lituania cercando di crearsi una nuova vita, sposarsi, fare affari. Insomma, una persona come tutti noi che viviamo in Italia. Pubblico la sua mail mantendo l'anonimato come da lui richiestomi.


 **Mi licenzio dalla Lituania**  
   
*Non ce l'ho fatta. Questa breve frase potrei ripeterla mille volte per quello che ho provato a fare in questo Paese.*

 Ho aperto una società ed è sempre stata solo un nome, nonostante i miei sforzi, neanche un cliente, ho più volte rivisto i miei progetti, ma non c'e' stato nessun miglioramento. Eppure in Italia ho aperto 5 società, grandi e di successo. Ho deciso di mollare il colpo, ma per fortuna posso vivere di rendita dal mio passato, non mi piace ma è così. Presto ricomincierò da capo.


 Non ce l'ho fatta ad avere pazienza. La pazienza di sopportare chi **non mi saluta** e mi incontra tutti i giorni sul pianerottolo e sull'ascensore. La pazienza di sopportare chi non mi dice neanche grazie dopo avergli tenuto la porta aperta, mai una volta, qui e' la regola, ma non per me e io ci sto molto male.


 Non ce l'ho fatta ad avere solo amici italiani. Mi sembrava illogico essere in un altro paese e creare il ghetto, eppure devo essere proprio antipatico a tutti, mai nessuno che mi abbia chiesto di bere una birra insieme o condividere un pensiero. Gli italiani invece mi hanno aiutato molto e ho scoperto che qui abbiamo tutti gli stessi problemi, c'e' chi e' gia' scappato, ma non lo va a scrivere di certo su un blog.


 A chi mi invia mail con la solita domanda: "Si possono fare affari in Lituania?" rispondo una volta per tutte: "Preparati a fare molta fatica, non ho ancora visto nessuno farcela, e credimi che di imprenditori che ci hanno provato qui in Lituania, ne ho visti davvero tanti."

 Questo non è un a mail nervosa, il nervosismo è già passato come è passata l'incredulità di non riuscire a vivere sereno in Lituania dopo averlo fatto negli Stati Uniti ed in Spagna. Semplicemente in Lituania non ce l'ho fatta.  


