Title: Ritorno in Lituania: impressioni dopo 1 mese di Italia
Date: 2010-08-02
Category: altro
Slug: ritorno-in-lituania-impressioni-dopo-1-mese-di-italia
Author: Karim N Gorjux
Summary: E' stata dura. E non è ancora finita. Partiremo per la Lituania tra qualche giorno, ma ho ancora alcune cose da sbrigare se il tempo me lo permette.

[![Come funziona il gossip](http://www.karimblog.net/wp-content/uploads/2010/08/how-gossip-works-300x208.jpg "how-gossip-works")](http://www.karimblog.net/wp-content/uploads/2010/08/how-gossip-works.jpg)E' stata dura. E non è ancora finita. Partiremo per la Lituania tra qualche giorno, ma ho ancora alcune cose da sbrigare se il tempo me lo permette.


 Questo viaggio è stato fatto per lo più per capire cosa fare nel prossimo futuro, dove far nascere il bambino e dove abitare. **Sinceramente l'Italia non ha rassicurato nessuno dei due**.  
  
Intanto l'Italia è davvero cara. Si paga tutto e questo già lo sai, sinceramente però non me la ricordavo così eccessivamente cara. Ringraziando noi abbiamo avuto delle spese limitate perché eravamo ospiti, ma il concetto è che bisogna scucire molto denaro per portare avanti una famiglia. Due stipendi minimo e se non hai la casa di proprietà l'affitto diventa una spada di Damocle sopra la testa che rischia di cadere ogni mese. L'asilo è un altro salasso inevitabile e su cui fare affidamento nel caso si lavori in due e non ci siano nonni disponibili. Dato che io non ho la casa di proprietà e dato che non ho nonni a cui affidare i figli buona parte di uno stipendio se ne andrebbe solo per colmare le lacune sociali dell'Italia. Quindi se proprio devo emigrare, perché devo scegliere l'Italia? Pensi davvero che in Italia si facciano pochi bambini solo per scelta? [In Svezia è diverso.](http://onewaytosweden.blogspot.com/2010/07/di-famiglie-e-dintorni.html "La famiglia in Svezia")

 **La mia impressione è che in Italia si debba lavorare come uno schiavo tutto il giorno**, in modo da arrivare a non pensare nemmeno il perché si lavora così tanto. Se lavori per pagarti il mutuo, ma ti rimane 1 giorno e mezzo la settimana per godertela che senso ha lavorare così tanto? Se devi lavorare per pagare le tasse o scervellarti per non pagarle che senso ha pagarsi le cure mediche, l'asilo o tutti i servizi che lo stato non elargisce?

 Ho notato che l'Italia è sempre un paese più ignorante. **La televisione è sempre peggio e studio aperto che è il mio ignoranzometro di fiducia è la conferma alle mie ipotesi**. Ovviamente sto parlando di impressioni che ho avuto della massa e non del totale degli italiani che per fortuna non è lo specchio di studio aperto.


 Il campeggio dove ho passato ben due giorni di questo mese, riflette bene la vita di un piccolo paese. E' un continuo vociare e spettegolare sulle persone (me compreso), le parole vengono travisate, modificate e le frasi vengono riportate al di fuori del contesto solo per fare del male. Sembra che sia giovani che anziani non sappiano più cosa fare durante il giorno e trovino il massimo del godimento quotidiano a spettegolare (male) di altri. **Non mi stupisce che un programma come il Grande Fratello sia arrivato alla decima edizione**. Mentre in Lituania non sanno nemmeno cosa sia.


 In Lituania tutto ciò non esiste ed infatti non ero più abituato allo spettegolezzo dei piccoli paesi. Anche esserne stato investito è stata una cosa "nuova", ma non ho dato molto peso alle parole. Anzi, mi sono anche divertito un po' a sentirmi importante.


 **Oltre allo spettegolezzo c'è "l'offesa"**. Avendo zii, cugini e amici, avrei dovuto andare a trovare tutti e bussare porta per porta. Gli amici con cui gioco a calcetto sono stati intelligenti a organizzare una cena dopo una partita per fare un saluto collettivo. E' stato bello sia giocare a calcetto che fare la cena. Mi ha reso davvero felice.


 **I parenti, ma fortunatamente non tutti, pretendevano la cena singola**. Avendo 9 zii e 38 cugini risultava difficile gestire una situazione del genere. Se poi ci metti ancora la moglie incinta e la bambina da portarmi dietro, l'afa incessante e la Clio del 1993 con cui mi sposto in provincia, l'impresa era degna per essere inclusa tra le fatiche di Ercole. 

 Di conseguenza sono andato a trovare pochi. Chi mi ha detto di chiamare per dire quando sarei andato a cena da lui non l'ho mai chiamato. Avrei davvero preferito fare una cena collettiva (solo una) e vedere tutti, ma di certo non potevo organizzare io una cosa del genere. L'unica cosa che mi dispiace è che qualcuno si offenderà nel pieno della tradizione italica e senza fare il minimo sforzo empatico per capire la difficoltà della mia situazione. Tra una cosa e l'altra **ho percorso più di 2000km con la Clio e probabilmente arriverò a toccare i 3000km** prima di partire, ho avuto un sacco di cose da fare e non vedo l'ora di tornare a casa per godermi le vacanze lavorando nel mio ufficio in Lituania.


 E' stato piacevole incontrare alcuni parenti ed incontrare un paio di persone del campeggio che non vedevo davvero da tanto tempo. Mi ha fatto piacere rivedere la mia famiglia anche se ho sforato i miei livelli di sopportazione. Mi ha anche fatto piacere incontrare persone del mio paese che mi hanno confidato di essere diventati lettori del mio blog dopo l'articolo su [Enrico](www.karimblog.net/.../ho-avuto-la-fortuna-di-conoscere-enrico-olocco-detto-rambo-detto-donny-flash/). (Proprio quell'articolo mi ha portato ad incontrare la mamma di Enrico che mi ha ringraziato molto per ciò che ho scritto. Non me lo aspettavo, la ringrazio e le auguro di ritrovare la felicità che merita.)

 Ho incontrate anche tante persone del mio paese che hanno letto l'articolo su [Centallo Informa](http://www.karimblog.net/2010/03/30/primi-assaggi-di-primavera-a-klaipeda-ed-intanto-la-mia-intervista-viene-pubblicata/) e che mi hanno fatto complimenti o fatto domande. Ho visto gli amici della mia palestra anche se avrei voluto andarci di più ed allenarmi invece di mettere su pancia mangiando le prelibatezze italiane.


 E' stato strano invece incontrare una persona a cui ho parlato due volte in vent'anni a Centallo e che **ho conosciuto veramente grazie a Facebook**. E' stato bello anche perché lui è giovane come me e ha già una bimba ed una moglie. Siamo in pochi, ma siamo felici :-)

 Dulcis in fundo ho fatto il trasloco a mio padre e ho aiutato [un illusionista](http://www.zapotek.net/) a recuperare un po' di mobili per la sua casa. Inoltre, come se non bastasse, ho preparato tutta la mia roba da spedire in Lituania e finire il trasloco che ho iniziato due anni fa.


 Sono stanco. Ci credi?

