Title: Fuso dal fuso
Date: 2008-04-05
Category: altro
Slug: fuso-dal-fuso
Author: Karim N Gorjux
Summary: Sono tornato da due giorni ed il viaggio di ritorno è stato un calvario. Dodici ore di volo, questa volta in business, e due ore e mezza di macchina da Malpensa a[...]

Sono tornato da due giorni ed il viaggio di ritorno è stato un **calvario**. Dodici ore di volo, questa volta in business, e due ore e mezza di macchina da Malpensa a Cuneo. Una volta arrivato a casa sono **svenuto** sul letto.


 Il primo giorno è stato come vivere un sogno, ho passato il pomeriggio totalmente **rincoglionito** che mi sembrava di dormire in piedi, la sera invece sono riuscito ad addormentarmi solo alle 4 e di conseguenza stamattina mi sono svegliato all'ora di pranzo.


 La mia preoccupazione maggiore è riuscire a svegliarmi domani mattina per andare (nuovamente) a Malpensa a prendere le mie lituane.  
  
[![Coloro che capiscono...](http://farm4.static.flickr.com/3141/2387543921_4bddf1a9ee_m.jpg)](http://www.flickr.com/photos/kmen-org/2387543921/ "Coloro che capiscono... di karimblog, su Flickr")  
  
  


