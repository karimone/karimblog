Title: L'avventura del mio blog
Date: 2005-11-27
Category: altro
Slug: lavventura-del-mio-blog
Author: Karim N Gorjux
Summary: Vorrei per prima cosa ringraziarvi. Il mio blog è entrato nella classifica della top 1000 italiana, per qualcuno può sembrare una cavolata, ma io ne vado orgoglioso. Voglio ringraziare chi mi segue[...]

**Vorrei per prima cosa ringraziarvi**. Il mio blog è entrato nella classifica della top 1000 italiana, per qualcuno può sembrare una cavolata, ma io ne vado orgoglioso. Voglio ringraziare chi mi segue e legge gli articoli, ma soprattutto chi lascia i commenti e gradisce ciò che scrivo. Solo nel mese di Ottobre ho avuto 7157 visite (escluso il mio IP) e riguardo il mese di Novembre ho raggiunto **8300** visite (e non è ancora finito il mese!)  
  
  
[![Statiche del 2005](http://www.kmen.org/wp-content/upload/Statistiche-tm.jpg "Statiche del 2005")](http://www.kmen.org/wp-content/upload/Statistiche.png)  


 Penso che il 2006 sarà l'anno dei cambiamenti, ho intenzione di cambiare host, ma soprattutto nome di dominio, vorrei registrarne uno più semplice che renda più l'idea di che si parla sul mio blog. Dato che è un blog personale pensavo qualcosa come **karimblog.net**, ma ho lasciato un sondaggio aperto sulla sinistra per sapere il vostro parere. Ne avrei davvero bisogno.  
...la colpa è di [Luca Conti](http://www.pandemia.info "Il sito di Luca Conti"), se sono arrivato a questi livelli (secondo me altissimi) tutte le volte che gli chiedo consigli su skype, non si lamenta mai e mi consiglia gratuitamente, questa è la magia dei bloggers, si diventa amici e ci si aiuta a vicenda. Grazie Luca! Ultimamente però una parte considerevole l'ha avuta anche [Alessandro Bonino](http://eiochemipensavo.diludovico.it/ "Il sito di Alessandro Bonino"), il primo compaesano che ho conosciuto grazie ad un blog ;-). (Ed in più è Mac User come me!)  
Grazie ancora a tutti quelli che mi leggono! Siete meravigliosi! :-)

   
  
adsense  
  


