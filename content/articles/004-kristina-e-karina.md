Title: Kristina e Karina
Date: 2004-08-12
Category: Lettonia
Slug: kristina-e-karina
Author: Karim N Gorjux
Summary: Eccola qui! La piccola Karina mentre scrive alcuni appunti, lei è una bambina di 12 anni che con la sorella gemella passeggia per Kalku iela vendendo piccole noci caramellate. Particolarmente intelligente e[...]

Eccola qui! La piccola Karina mentre scrive alcuni appunti, lei è una bambina di 12 anni che con la sorella gemella passeggia per Kalku iela vendendo piccole noci caramellate. Particolarmente intelligente e simpatica parla lettone, russo e inglese. Siamo subito diventati e amici e... come si fà solitamente con i gemelli, quando ho incontrato la sorella (Kristina), le ho parlato credendo che fosse Karina. Un'altra figura da aggiungere all'album delle figure di emme. Chiedere l'elemosina in Riga è lo sport nazionale dopo l'hockey, ma Karina è differente: è socievole e molto spiritosa. Premetto che non le ho mai comprato una confezione di dolci, ma le ho sempre dato qualcosa con un mucchio di scuse: una foto, un paio di dolci a scrocco che le prendo da quelli che offre ai passanti (e sono buoni!) :D In Riga incontrato bambini che chiedevano soldi per nulla, 30enni che cantavano per la strada e vecchietti che con la bilancia ti fanno pagare una pesata 5 santimi, nessuno regge la concorrenza di Karina. :-)

 [![Karina](http://www.kmen.org/images/karina.jpg)](http://www.kmen.org/images/karina.jpg)  




