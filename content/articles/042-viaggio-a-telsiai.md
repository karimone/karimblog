Title: Viaggio a Telsiai
Date: 2005-01-31
Category: Lituania
Slug: viaggio-a-telsiai
Author: Karim N Gorjux
Summary: Lo dico subito, il mio ritorno avverrà probabilemete per la domenica del 20 febbraio così al mattino mi farò un giro per il mercato del paese, mi comprerò i 2 numeri di[...]

Lo dico subito, il mio ritorno avverrà probabilemete per la domenica del 20 febbraio così al mattino mi farò un giro per il mercato del paese, mi comprerò i 2 numeri di Dylan Dog che mi mancano e forse anche Applicando. Dico questo perchè almeno non mi sentirò chiedere ogni momento: "*Quando torni?*", il bello di avere un sito e che **puoi dire tutto una volta sola e le voci girano**. La data non è ancora ufficiale, comprerò questo pomeriggio il biglietto per il ritorno. Se le cose andranno come devono comperò anche il biglietto di ritorno. :-) Non mi sbilancio...


 Sono a stato a Telsiai, 90km da Klaipeda. Sono partito la mattina a piedi verso la stazione, un freddo degno di una glaciazione, fare 5 minuti a piedi alle 7 del mattino in Lituania rende bene l'idea della teoria della relatività. Benedetto Albert, mi è sembrato di fare la maratona di New York!

 Mi aspettavo di prendere un treno come quelli lettoni, spoglio, scomodo e freddo. Sono rimasto stupito, era comodo e pulito, il classico nostro treno a cuccette con la differenza che tutto era in ordine e soprattutto pulito. Naturalmente il controllore era una donna, diversamente da noi il biglietto viene controllato prima di salire, così sanno quando devi scendere e se per caso durante il tragitto i conti non tornano, vengono a controllare ogni cuccetta dove si trova il furbo. Un'ora di viaggio e mi ritrovo alla stazione di Telshai.


 La stazione non è degna di una cittadina di 35.000 abitanti, ma anche se il numero di anime può far pensare ad una delle nostre città (come può per esempio essere Fossano, per chi è delle mie zone), in realtà tutto è molto più spoglio. Non c'è praticamente vita serale e probabilmente anche diurna, naturalmente la gente mi guardava come uno scappato di casa, la neve era molto più abbondante di Klaipeda e soprattutto faceva molto più freddo.  
La povertà si nota sempre e ovunque, il freddo rende il tutto ancora più toccante. La gente si veste come può, ho in mente alcune immagini del mercato nel centro del paese dove le bancarelle si tenevano in piedi per grazia ricevuta e la donne che vendevano quei pochi vestiti erano imbaccuccate come mummie. Come ogni paese la Lituania è bella se hai i soldi, se vivi come questa gente resisti poco, soprattutto se arrivi dalla repubblica degli spaghetti, dove si mangia bene e si gira in maniche corte per 8 mesi l'anno.


 Al di là dell'economia e della politica, la natura ha fatto del suo meglio per rendere questo posto di cui forse voi non avete mai sentito il nome letteralmente magnifico. Circondata da piccoli colli, Telsiai viene calorosamente chiamata dai suoi abitanti "*la piccola Roma*", dai suoi colli è possibile vedere il lago e la chiesa. Sul [sito](http://www.kmen.org/gallerie/telsiai/) potete ammirare qualche foto del panorama. Per la prima volta ho visto le persone pescare sul ghiaccio come vedevo nei cartoni animati da piccolo. Ho visto della gente seduta sul lago sopra la sedia che aspettava al freddo mentre teneva in mano uno spago che affondava in un buco nel ghiaccio. Ho anche camminato sul lago! Ho visto i bambini trainarsi sulle slitte di legno come vedevo tanti anni fà quando ero piccolo. Il freddo era sempre presente. Dentro la chiesa di Telshai non c'era nessuno, non sono un critico d'arte quindi non posso esprimere un giudizio, non sono il tipo di persona adatta a descrivere le opere d'arte, mi limito a dire che non era molto grossa, ma ben curata e addobbata. Una nota curiosa: c'era ancora il presepe e l'albero di Natale. Forse in questo periodo hanno avuto di meglio da fare.


 A questo punto chi legge si chiede: "*cosa diavolo ci facevi a Telsiai?*" ovviamente non ci sono andato da solo, ero in compagnia di Rita che come avrete capito è stata la causa della mia assenza dal sito in questi giorni. Ho visto la sua casa, naturalmente qui tutti vivono in palazzoni comunisti come il mio, ho la netta impressione che 50 anni fà la Russia ha fatto 5-6 progetti e ha mandato in giro per il suo impero delle copie perchè qui tutti i palazzi sono uguali!  
In lettonia sono entrato in casa di parenti di Kristina, non l'ho mai scritto, ma le condizioni erano allucinanti, tutto diroccato e non troppo pulito. L'appartemento di Rita era piccolo, ma molto bello. L'ingegno del padre ha combattuto contro il grigiore e la tristezza comunista rendendo il piccolo appartamento accogliente e confortevole. Il preingresso aveva i muri rivestiti di legno che mi ricordavano molto la danimarca (o l'aeroporto di Riga per chi c'e' stato), i mobili artigianali, le foto e i quadri appesi con calcolata cura sono piccoli dettagli che hanno reso la mia permanenza in casa di Rita, anche se solo per un giorno, molto particolare e piacevole.


 Sono tornato a Klaipeda la sera, prendendo uno scomodissimo treno economico e lento che ha richiesto il doppio del tempo per venire a Telsiai, sono cose che capitano :-)

