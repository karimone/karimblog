Title: Ad aspettarci al rientro l'Agosto più caldo degli ultimi cento anni
Date: 2010-08-23
Category: Lituania
Slug: ad-aspettarci-al-rientro-lagosto-più-caldo-degli-ultimi-cento-anni
Author: Karim N Gorjux
Summary: Siamo tornati ormai da una decina di giorni, stiamo bene e siamo contenti. I quattro pallet di roba che ancora avevo spedito dall'Italia sono arrivati sani e salvi; mi sono spedito vestiti,[...]

[![20050827_viesulas Palangoje_002.JPG](http://farm4.static.flickr.com/3328/3280590771_275ace77e3_m.jpg)](http://www.flickr.com/photos/mariukasm/3280590771/ "20050827_viesulas Palangoje_002.JPG di MariukasM, su Flickr")  
Siamo tornati ormai da una decina di giorni, stiamo bene e siamo contenti. I quattro pallet di roba che ancora avevo spedito dall'Italia sono arrivati sani e salvi; mi sono spedito vestiti, mobili, libri e persino olio, vino e altre cose buone da mangiare.


 Sotto un sole cocente ho portato tutti i pacchi in casa, Greta era dalla nonna a Telsiai e Rita mi ha aiutato nei limiti imposti dal suo pancione; ora posso dire ufficialmente di aver finito il trasloco. (In realtà è venuto ad aiutarmi [qualcuno](http://www.mirkobarreca.net/), ma è arrivato molto tardi...)  
  
**Cosa dire del mese passato in Italia?** Tante cose positive. L'Italia è sempre un bellissimo posto con un buon cibo e tanta gente, ma **soggettivamente** parlando ora preferiamo stare qui a Klaipeda. Rispetto a prima però ho intenzione di tornare più spesso, pensavo almeno ogni tre mesi ma, se il lavoro lo richiede, anche di più.


 Una cosa è certa, il viaggio in Italia ha chiarito molto su ciò che vogliamo fare in futuro. Ha delineato bene cosa fare nel mio lavoro e cosa abbiamo intenzione di fare nei prossimi due anni, c'è però una questione aperta. Greta oggi compie quattro anni quindi abbiamo ancora due anni per decidere dove andare a fare gli emigranti.  
Stare qui in Lituania? Tornare in Italia? Andare in un altro paese? Tutto dipende dai nostri desideri e da come si svilupperanno i prossimi mesi, per ora aspettiamo che nasca il bimbo ed intanto godiamoci la Lituania.


 Ricollegandomi al titolo dell'articolo, ha fatto un caldo caraibico per più di una settimana, ogni giorno il termometro superava i trenta gradi e la sera c'era il temporale a rinfrescare le idee. Adesso la situazione è migliorata tornando alle medie stagionali, ma ti assicuro che c'era veramente da morire. I temporali inoltre hanno fatto qualche danno nell'entroterra, qui a Klaipeda per fortuna è andata bene e ce la siamo cavata con solo qualche pioggia anche se sono stati già avvistati ben due tornado sul mar Baltico nei pressi di Klaipeda. **La foto che accompagna questo articolo è a dir poco inquietante.**

