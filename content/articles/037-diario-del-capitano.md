Title: Diario del capitano...
Date: 2005-01-16
Category: Lituania
Slug: diario-del-capitano
Author: Karim N Gorjux
Summary: Il continuo cambiamento climatico è molto fastidioso: ieri ha fatto bello per 15 minuti, nel giro di un paio di minuti ha iniziato a nevicare, dopo altri 10 minuti è tornato il[...]

Il continuo cambiamento climatico è molto fastidioso: ieri ha fatto bello per 15 minuti, nel giro di un paio di minuti ha iniziato a nevicare, dopo altri 10 minuti è tornato il sole poi è diventato nuvolo e poi ha nevischiato. In un paio d'ore ho assistito a un continuo sconvolgimento climatico, ma ho resistito e sono uscito lo stesso.


 Ieri pomeriggio sono andato alla spiaggia in compagnia di Milda, (per la cronaca Milda è anche il nome della statua della libertà di Riga). La spiaggia è a 5 minuti di bus da dove abito, sinceramente rapportato ai nostri mari e alla vicina, per me, costa azzurra, il Baltico mi ha dato un'impressione molto spoglia. La spiaggia e' grande e allo stesso tempo molto vuota, molte persone vengono a passeggiare sulla spiaggia, ma la sensazione permane. A parte questa sensazione il frastagliarsi delle onde è sempre una musica naturale e rilassante. Sull'orizzonte il mare e le nuvole si uniscono in un tutt'uno e le onde aggrediscono la spiaggia sfogando la propria rabbia. Il mare mosso e la sabbia densa simboleggiano la situazione di questi paese in un contrasto naturale unico.


 Con Milda ho fatto la mia prima escursione nella città e ho preso per la prima volta l'autobus in Lituania (da solo). Al modico prezzo di 0.30€ puoi farti una tratta qualsiasi del bus, compri il biglietto sull'autobus e provvedi alla obliterazione manuale. Sembra un pò complicato e secondo me funziona solo perchè è in Lituania. In centro ho trascorso due piacevoli ore in un piccolo bar locale a chiacchierare del più e del meno. Ormai credo di essere una calamita per le brave ragazze, Milda è bionda, alta, occhi azzurri, studentessa dal portamento raffinato ed elegante. In questo periodo non ho nessuna voglia di fare foto quindi non posso mostrarvi Milda.


 Sono ancora alla ricerca di roba da vestire, ieri sera nel piccolo centro commerciale di Klaipeda ho visto le giacche di Hugo Boss da 700€! Ma dai... non scherziamo! Chi se le compra? Ho visto la libreria dove si vendono i libri, incredibilmente ci si può sedere ai tavoli e ordinare il the o il caffè, immagino in Italia dove una libreria del genere diventerebbe una biblioteca dove nessuno comprerebbe niente. Nessuno di voi sarà stupito nel sapere che non mi sono comprato niente e ho concluso il preserata tornando a casa a schiacciare un pisolino.


 In serata sono andato all'Honolulu, una delle discoteche più in di Klaipeda. Ho visto tantissime ragazze elegantissime e soprattutto molto belle, ho avuto una difficoltà estrema a focalizzare una persona soltanto e a cercare di conoscere qualcuno. Le distrazioni sono ovunque. Incredibilmente per la seconda volta nei paesi baltici mi è stato offerto da bere, un gruppo di ragazze mi ha chiamato al tavolo dove erano accomodate e ho scambiato due chiacchiere. Tra loro c'era Agatha una diciottenne un pò strana, ma molto molto bella. Alta, formosa, occhi azzurri, sguardo molto intenso. Un leggerissimo particolare a suo sfavore: completamente rincoglionita. Vai a capire come abusano del loro corpo questi ragazzini, le campagne antifumo e antidroga si possono leggere per tutta Klaipeda, ma da quel poco che ho visto sembrano non funzionare.


 Una domanda mi viene posta di frequente: "*Ti trovi meglio a Klaipeda o a Riga?*" Difficile dare una risposta esauriente. Riga la conosco bene e in più ho molte amicizie, Klaipeda la conosco solo da 4 giorni e devo dire che per ora mi piace anche se è una realtà del tutto differente. Saprò dire di meglio tra qualche giorno..


