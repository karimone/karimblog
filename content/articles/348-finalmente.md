Title: Finalmente!
Date: 2007-07-13
Category: altro
Slug: finalmente
Author: Karim N Gorjux
Summary: Gli affari iniziano a prendere una buona piega, siamo in un periodo brutto per le vendite, l'estate è vicina e molta gente pensa più ad andare al mare o in montagna. Bene[...]

Gli affari iniziano a prendere una buona piega, siamo in un periodo brutto per le vendite, l'estate è vicina e molta gente pensa più ad andare al mare o in montagna. Bene o male però qualcosa si sta muovendo.


 Una cosa che però non credevo di dover fare sono i siti, eppure eccomi qua che devo finire alcune magagne e mi ritroverò a fare altri siti, ringraziando il cielo esistono [wordpress](http://www.wordpress.org) e [joomla](http://www.joomla.org), ma soprattutto esiste [dreamhost](http://www.dreamhost.com).


 Il mio lavoro mi piace, ho l'ufficio in casa e tratto dei prodotti che mi danno delle soddisfazioni incredibili. Pochi giorni fa ho visitato un cliente per mettere una rete wifi, entro nell'ufficio ed ecco che mi vedo un bel MacBook Pro. "L'ho comprato perché mia figlia insisteva tanto..." Grande ragazza! Ho fatto un figurone quando in 2 ore circa ho messo a posto il computer dando qualche piccola lezione base su MacOsX e soprattutto ho acquistato la fiducia del cliente tanto da arrivare a far assistenza con Timbuktu direttamente da casa.


 Concludendo:   
  
2. Il mio lavoro mi diverte
  
4. A Cuneo non sanno vendere i Mac.

   
7. i Mac stanno prendendo molto piede anche tra i "non grafici" (LOL).

  


