Title: Ho avuto la fortuna di conoscere Enrico Olocco, detto Rambo, detto Donny Flash
Date: 2009-10-05
Category: altro
Slug: ho-avuto-la-fortuna-di-conoscere-enrico-olocco-detto-rambo-detto-donny-flash
Author: Karim N Gorjux
Summary: Enrico è sempre stato un tipo pittoresco, stravagante, tant' é che la sua fama l'ha sempre preceduto anche in un paesino piccolo come Centallo; Io ad esempio, che avevo 12 anni meno[...]

[![Enrico-Olocco](http://www.karimblog.net/wp-content/uploads/2009/10/Enrico-Olocco1.jpg "Enrico-Olocco")](http://www.karimblog.net/wp-content/uploads/2009/10/Enrico-Olocco1.jpg)Enrico è sempre stato un tipo pittoresco, stravagante, tant' é che la sua fama l'ha sempre preceduto anche in un paesino piccolo come Centallo; Io ad esempio, che avevo 12 anni meno di lui, sapevo già chi era senza che lui sapesse nulla di me. 

 Mi ricordo che era piccolino, un po' come me, folti capelli ricci, la barbetta alla Lenny Kravitz e gli occhi verdi e scintillanti come due smeraldi. Il suo carisma e il suo modo di essere erano davvero qualcosa di inconsueto a Centallo, talmente inconsueto che persino Daria Bignardi gli ha dedicato parte di una puntata di Tempi Moderni qualche anno fa.  
  
Ricordo inoltre che guardai due film solo per vedere la sua apparizione come comparsa. Del primo non ricordo il nome, ma la sua apparizione fu lampo, giusto qualche inquadratura, mentre nel "Il partigiano Johnny", Enrico strappa qualche minuto e qualche battuta come comparsa. Purtroppo non sono riuscito a recuperare nulla a parte i primi secondi di [questo video](http://www.youtube.com/watch?v=fv2kthsMoIU) su youtube.


 Di Enrico ho vari ricordi, il primo di tanti anni fa, quando lui, all'inizio della sua carriera poco fortunata, venne a casa mia per chiedermi se poteva usare il mio computer per collegarmi ad Internet. Mi ricordo ancora adesso la sensazione che provai quando aprendo la porta, mi ritrovai lui che mi disse: "Ciao, ho bisogno di guardare la posta su internet, ma non so proprio da chi andare! Posso usare il tuo computer?". Quel giorno fu il primo vero contatto con l'uomo che poteva vantarsi più soprannomi di tutta la popolazione centallese.


 Negli anni ho sempre avuto modo di incontrarlo e scambiare due parole. Il suo modo di essere e di affrontare la vita era senza dubbio affascinante, l'impressione è che lui fosse una specie di Superman nei confronti delle avversità della vita. Tra i vari soprannomi aveva anche "Rambo", forse non buttato li a caso, quindi la mia idea di lui era di un eterno ragazzo al di sopra del conformismo di tutti giorni, un pazzo, un artista, un genio... non lo so, purtroppo non conosco molto i particolari della sua vita e, a causa dell'età così diversa e del modo di vivere, non gli sono mai stato abbastanza vicino da capire chi realmente fosse.


 Devo dire però che ogni volta che mi sono fermato a parlare con lui, non l'ho mai visto triste, corrucciato o incazzato. Cosa mi colpiva ogni volta che lo incontravo era il suo sorriso smagliante accompagnato da quegli occhi verdi e così splendenti. Enrico è scomparso il 20 Agosto, non si è fatto più sentire da un momento all'altro ed è sparito. Ho seguito la vicenda da qui in Lituania e anche sui giornali, persino "Chi l'ha visto?" aveva dedicato dello spazio per il suo caso.


 Questa mattina leggo su internet che Enrico non potrà tornare più a casa. 

 Ci sono rimasto male. Enrico è una di quelle persone che anche se hai frequentato poco, non dimentichi facilmente. Io non avevo nessun legame particolare con lui, sono morte molte persone che conoscevo di vista e molto cinicamente pensavo "meglio a te che a me", non sono così ipocrita da negarlo, ma con Enrico un senso di tristezza si è impadronito di me. 

 Ho un senso di tristezza perché sto facendo rivivere Enrico nei miei ricordi. Penso a tutte le volte che ci siamo incontrati nei soliti posti della provincia Granda; ricordo le sue teorie su come si vive la vita, le sue risate contagiose e il suo modo di prendersi non troppo sul serio, ma ora capisco che dietro quei sorrisi, dietro quell'atteggiamento stravagante e fuori dalle righe si nascondeva una persona fragile afflitta dai problemi di tutti i giorni proprio come tutti noi. Purtroppo però l'epilogo non è stato dei migliori, lui ha fatto una scelta tragica. Una scelta di quelle che non lascia spazio a ripensamenti.


 Link: [Enrico Olocco su Facebook](http://it-it.facebook.com/people/Donny-Flash-Enrico-Olocco/1092590902)  
Link: [La notizia del ritrovamento di Enrico](http://www.cuneocronaca.it/news.asp?id=21296&typenews=primapagina)

