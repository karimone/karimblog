Title: Particolarità della società lituana vista attraverso gli occhi di un italiano (Parte 1)
Date: 2009-12-14
Category: Lituania
Slug: particolarità-della-società-lituana-vista-attraverso-gli-occhi-di-un-italiano-parte-1
Author: Karim N Gorjux
Summary: Ogni volta che incontro qualcuno mi ritrovo la sua mano destra dritta di fronte a me ed ogni volta mi tocca stringerla come se stessimo mettendoci d'accordo per qualcosa di losco. Stringersi[...]

![Gene Wilder](http://www.karimblog.net/wp-content/uploads/2009/12/incredibile-gene-wilder.png "Gene Wilder")Ogni volta che incontro qualcuno mi ritrovo la sua mano destra dritta di fronte a me ed ogni volta mi tocca stringerla come se stessimo mettendoci d'accordo per qualcosa di losco. Stringersi la mano è una prassi normale nella vita dei lituani a cui non riesco ancora a farci l'abitudine.


 **Non capisco se bisogna stringere la mano anche alle donne**, il più delle volte do il bacio all'italiana, ma noto che le donne non sono preparate. Persino mia suocera che mi conosce da 5 anni, si trova sembre in imbarazzo con il bacio sulla guancia. Anche i ragazzini si stringono la mano come gli adulti, questa cosa mi ha fatto sorridere sin da subito perché mentre negli adulti trovo questo rito "strano" nei ragazzini lo trovo buffo.  
  
Camminando per strada, mi sono trovato a dover stringere la mano "al volo", come se fosse un passaggio di testimone, accompagnando il gesto da un paio di farfugliamenti del tipo "Ciao, come và...". Sono quotidianità che ogni volta mi lasciano perplesso ed impreparato a tal punto che **se posso evito di stringere la mano**, ma agisco all'italiana restituendo un poco di imbarazzo. Alla stretta di mano non mi ci abituerò mai, sono cose troppo distanti dalla mia genetica di stampo latina.


 **In casa è assolutamente vietato usare le scarpe**, non solo a casa degli altri, ma anche a casa mia dato che la signora al comando è lituana. Non indossare le scarpe con tutte le zozzerie di fuori annesse è una sana abitudine che ho fatto subito mia. Quelle poche volte che giro per casa con le scarpe, mi sento a disagio, fuori posto e lo stesso mi accade quando sono in Italia dove indossare le scarpe per la casa è una cosa normalissima.


 I lituani quindi vengono a casa tua e si tolgono le scarpe appena entrano, in più **hanno sempre qualcosa piccolo dono di apprezzamento** come una tortina o del cioccolato, difficilmente se invitate un lituano a casa vostra si presenterà alla porta con le mani vuote anche se con qualcosa di piccolo ed insignificante.  
**  
In palestra quasi nessuno si porta l'asciugamano per il sudore** o per stenderlo sui macchinari che sta utilizzando. Nel bel mezzo della palestra, agganciato ad una colonna portanto dell'edificio, c'è un distributore di salviette a limitare i danni igienici. In Italia, nelle palestre che ho frequentato, è sempre stata buona norma portarsi l'asciugamano ed evitare di inzozzare di sudore gli attrezzi. E' strano: sudore si, ma scarpe sporche no.


 **Nei ristoranti non ti viene il mal di testa**, le persone parlano a voce bassa e non fanno rumore. Dopo anni di Lituania, le serate ai ristoranti italiani sono un'emicrania assicurata. Il megafono italico incorporato nelle corde vocali è una di quelle caratteristiche latine di cui sento meno la mancanza da quando la Lituania mi ospita. E' curioso, ma quando torno dall'Italia, de**vo "tarare" nuovamente le corde vocali** per riabiutarmi ai nuovi standard sonori.


 **I momenti di silenzio sono frequenti con i lituani**, forse sono dovuti solo a causa delle mia evidenti fattezze "extrabaltiche", ma sono abiutato a ritrovarmi nello spogliatoio della palestra tra 2 e 3 persone e non sentire una parola per diversi minuti. In effetti se non c'è niente da dire, perché bisogna per forza dire qualcosa? Gli italiani hanno paura del momento del silenzio e hanno sempre qualcosa da dire, **i lituani invece sono persone molto silenziose**, il più delle volte mi salutano con gli occhi o con uno sguardo e le ragazze si limitano ad un sorriso a 32 denti, ma sono riti degni di un film muto dove ogni gesto alla sua importanza. Forse è una semplice conseguenza dell'"effetto straniero"?

