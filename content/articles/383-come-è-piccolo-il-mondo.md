Title: Come è piccolo il mondo
Date: 2007-11-14
Category: Lituania
Slug: come-è-piccolo-il-mondo
Author: Karim N Gorjux
Summary: Sandro è a Kaunas e fino a pochi giorni fa c'era anche quel pazzo di Henry.

[Sandro](http://www.sandro.madeinblog.net) è a Kaunas e fino a pochi giorni fa c'era anche quel pazzo di [Henry](http://www.henryblog.net).


  
Videochiamo Sandro su skype per sapere come gli vanno le cose a Kaunas. Il video è fluido e sinceramente, essendo la connessione Italia - Lituania, ne rimango piacevolmente sorpreso. Rita, quando videochiama i suoi genitori su skype, ha un video degno del terzo mondoì e quindi chiamo Rita per mostrarle il miracolo di cui ero testimone.


  
L'avvento della **madonna tecnologica di Kaunas** si trasforma in un salotto bizzarro. Rita nello stesso istante era in chat con sua madre, ovvero mia suocera, che frequenta l'università di Kaunas e dalla conversazione a 5 scopriamo che mia suocera ha il numero di telefono di Sandro che quest'ultimo si spaccia per la Lituania come emerito professore di Italiano, Spagnolo e Piemontese.


  
Quanto è piccolo il mondo.


  
Rettifico: quanto il web ha reso piccolo il mondo.


  
Ho reso l'idea?  


  
  
   
  
 [![Indre e Sandro](http://farm3.static.flickr.com/2283/2021290323_8fa69d2753_m.jpg)](http://www.flickr.com/photos/kmen-org/2021290323/ "Indre e Sandro by karimblog, on Flickr")  
  
  
  
 Clicca sull'immagine per ingrandirla  
  


