Title: Trova le differenze con l'Italia: burocrazia australiana per un cittadino australiano
Date: 2015-02-05
Category: Australia
Slug: trova-le-differenze-con-litalia-burocrazia-australiana-per-un-cittadino-australiano
Author: Karim N Gorjux
Summary: Arrivato in Australia, le prime cose da fare sono queste: comprare una sim telefonica, aprire un conto in banca, convertire la patente, chiedere il codice fiscale (TFN), iscriversi al sistema sanitario (medicare)[...]

[![Burocrazia](http://www.karimblog.net/wp-content/uploads/2015/02/shutterstock_burocrazia2-300x254.jpg)](http://www.karimblog.net/wp-content/uploads/2015/02/shutterstock_burocrazia2.jpg)Arrivato in Australia, le prime cose da fare sono queste: comprare una sim telefonica, aprire un conto in banca, convertire la patente, chiedere il codice fiscale (TFN), iscriversi al sistema sanitario (medicare) e farsi conoscere dall'assistenza sociale australiana (centrelink).


 L'argomento è abbastanza lungo quindi l'articolo è diviso in due parti, ma procedo con ordine e ti spiego come ho vissuto tutto questo con gli occhi da cittadino australiano. Quando avrai finito di leggere, trai le tue conclusioni comparando con il sistema italiano, io mi limito solo a raccontare la mia esperienza da cittadino australiano in Australia.  
  
### Il numero di telefono: la SIM

  
Prima di fare qualsiasi cosa bisogna avere un numero di telefono. Qui tutto funziona con il telefono e se hai lo smartphone è molto meglio perché molti servizi pubblici e anche privati hanno "l'app" che facilita di molto l'utilizzo dei servizi.  
Sapevo già qualche compagnia scegliere, il 4 Novembre, nonostante fosse festa qui a Melbourne, sono andato in un negozio Optus e ho comprato una sim con 1GB di internet e 6 ore di chiamate per 30$. Il tutto mi ha richiesto 5 minuti e il passaporto. Metto la SIM, accendo il telefono, si autoconfigura et voilà, internet attivo e chiamate attive.  
### Il conto in banca

  
Anche la banca l'avevo già scelta prima ancora di arrivare in Australia. Avevo optato per la NAB che ti permette di aprire il conto gratis e non ha nessuna spesa di mantenimento, a quanto mi risulta fino adesso anche senza tasse nascoste. Mi reco alla filiale centrale in Bourke Street nel centro di Melbourne, l'unico impedimento iniziale è il riconoscimento dei 100 punti. Ovvero bisogna presentare dei documenti di riconoscimento che sommati insieme totalizzino 100 punti. Ad esempio il passaporto vale 70 punti e la patente ne vale 40 e se hai la patente formato europeo, non hai nessun problema ad aprire un conto in Australia, io purtroppo avevo con me la patente italiana formato "gazzetta dello sport" e non me l'hanno accettata. Dato però che la lista dei documenti accettati è lunga, ho optato per il certificato di cittadinanza sommato con il passaporto australiano.


 Il procedimento di apertura del conto dura 3/5 minuti e bisogna avere il telefono con il numero attivo. La signorina mi porge un foglio da firmare fronte e retro, poi gira il suo monitor verso di me e mi chiede di inserire numero di telefono ed una password e confermare il codice che mi sarebbe arrivato via sms in pochi secondi. Confermo il codice e la signorina mi da un documento con i dati importanti del conto. Tutto a posto, il conto è aperto e posso versare contati e fare ciò che voglio. La carta di debito mi arriverà in 5 giorni a casa.


 Verso il contante e mi faccio stampare un estratto conto. Questo è davvero importante da fare perché **nell'estratto conto c'è la prova dell'indirizzo di residenza**. Qui non è come in Italia che devi dire dove abiti e poi arriva a controllarti il vigile per verificare se abiti veramente li, qui basta avere il documento della banca e fa fede quello.  
### La patente

  
Prima di partire dall'Italia avevo fatto la patente internazionale pensando che mi sarei evitato di fare la traduzione qui in Australia, ma questo non è bastato. Avendo la patente di tipo vecchio ho dovuto farmela tradurre da un traduttore certificato per 33$. Poco male. Per fortuna che l'Italia e l'Australia hanno una convenzione e la patente si può convertire senza esame. Sono andato alla motorizzazione ed una impiegata gentilissima mi ha dato il modulo da compilare e la lista dei documenti da portare. Purtroppo, per fare la conversione c'è bisogno di un appuntamento e l'impiegata tutta dispiaciuta mi dice: **"mi spiace, ma OGGI non abbiamo posti liberi, le va bene domani alle 11?"**. Mi sono quasi commosso, ho prenotato per il giorno dopo.


 Alle 11 della mattina seguente torno alla motorizzazione e, memore delle istruzioni dell'impiegata, segnalo la mia presenza in sala di attesa all'incaricato, ma non prendo nessun numerino per fare la coda. Alle 11:03 mi chiamano allo sportello, presento i documenti e una signora tutta sorridente e dal forte accento australiano mi chiede: "*Per quanto vuole fare la patente, tre anni o dieci?*". *Vada per tre!* rispondo io! Pago 73$ con carta di debito semplicemente sfiorando la carta sul lettore magnetico e senza nemmeno digitare il pin. In quel momento una lacrima mi scorre sulla guancia memore delle attese alla posta con il bollettino postale in mano. "*Vada dal mio collega per la foto.*" mi dice la signora indicando il suo collega. Mi ritrovo davanti ad una fotocamera, "*cheesseee...."* ed ecco che sul monitor vedo la versione digitale della mia patente con la mia faccia sopra. "Le va bene?", ovvio che mi va bene. L'impiegato mi rilascia una ricevuta che fa patente provvisoria e mi assicura che la patente arriverà a casa in 10 giorni.


 La procedura per la conversione è durata circa 8 minuti e la patente mi venne recapitata per posta solo 6 giorni dopo. Uscito dalla motorizzazione avevo come la forte sensazione di aver fatto la cosa giusta a buttarmi a pesce in un'avventura lontana 20.000km da casa.


 ### Il codice fiscale: Tax File Number

  
Avere il codice fiscale è molto importante in Australia per svariati motivi che non sto ad elencare. Per richiedere il TFN, basta fare la richiesta online dando la propria casella di posta elettronica. L'australian taxation office ti manda una mail con allegato un pdf che deve essere stampato e consegnato di persona ad un qualsiasi ufficio postale abilitato al riconoscimento, ovviamente nel pdf c'è il link al sito per fare la ricerca dell'ufficio più vicino.  
Mi presento all'ufficio postale e in 5 minuti faccio tutto. Si prendono una copia del mio passaporto e devo solo fare una firma in digitale su un'apparecchio elettronico. "*Le arriverà il numero direttamente a casa in 20 giorni.*". Così mi liquida l'impiegata, ma stranamente il numero mi arriva dopo una settimana. Ho quasi la sensazione che lo facciano apposta a dire che ci mette di più. (Nota bene: per i non residenti, mi sembra che non sia necessario il riconoscimento, ma il codice arriva direttamente per posta)

 Tutte queste procedure mi hanno chiesto circa due giorni per essere eseguite. Il più è aspettare che arrivino i documenti via posta, ma le cose più importanti mi sono arrivate nella prima settimana di permanenza. La patente e la carta di debito sono le due cose più importanti: l**a patente qui in Australia equivale alla carta d'identità** ed una volta ricevuta ho potuto mettere il passaporto sull'altare in casa dove viene da me venerato come se fosse la mamma del Duca Conte Piercarlo ing. Semenzara. La carta di debito la uso praticamente per pagare qualsiasi cosa e mi evito di portarmi i contanti dietro.


 Prossima puntata: medicare e centrelink.


