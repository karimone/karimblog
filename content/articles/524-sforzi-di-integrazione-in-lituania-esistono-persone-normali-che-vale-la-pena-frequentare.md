Title: Sforzi di integrazione: in Lituania esistono persone normali che vale la pena frequentare
Date: 2009-03-02
Category: Lituania
Slug: sforzi-di-integrazione-in-lituania-esistono-persone-normali-che-vale-la-pena-frequentare
Author: Karim N Gorjux
Summary: 

[![Io che penso](http://farm4.static.flickr.com/3345/3318930938_ff57a33689_m.jpg "Think")](http://www.flickr.com/photos/39283264@N00/3318930938/)

 Quando vado in giro per Klaipeda, soprattutto di notte, faccio sempre molta attenzione. Evito di passare in certe zone e vicino a gruppi di persone che non conosco. Evito di frequentare i locali a richio, anche se, a dire il vero, una sera mi sono avventurato da solo in una delle discoteche più mal frequentate di Klaipeda. (E non mi è successo nulla)  
  
Adesso ho degli amici lituani. Sono sia ragazzi che ragazze dai 20 sino ai 25 anni circa. C'è chi studia c'è chi lavora e c'è anche chi convive e ha famiglia. Mi salutano tutti, mi parlano in lituano ed in inglese senza troppi problemi, vado alle loro feste, bevo con loro, faccio i tornei di calcetto e li aggiungo tra i contatti su [facebook](http://www.facebook.com).


 Questi ragazzi sono assolutamente normali. Bevono, si drogano senza esagerare, imprecano, scherzano, litigano pochissimo e sono gentili. Le donne sono come gli uomini, ma molto più belle. Le mie esperienze passate, mi avevano portato a [giudicare](http://www.karimblog.net/2006/08/13/caro-lituano/) i lituani sotto un aspetto molto negativo. In realtà esistono anche quei lituani, come in ogni stato del mondo esiste della gente grezza, rozza, ignorante e maleducata.


 Mi trovo bene. In questi giorni sono rimasto a casa da solo perché Rita e Greta sono dovute andare a Kaunas per alcune faccende importanti. Io ne ho approfittato per fare *la vita dello studente*. Mi sono divertito, ho fatto altre conoscenze e ho migliorato, anche se di poco, il mio lituano.


 Il mio intento è di essere completamente integrato scrollandomi di dosso quella sensazione di merda di essere uno straniero. Per riuscirci seguo alcune semplici regole:  
  
 * Cerco di parlare il lituano il più possibile (che lingua di mer...)
  
 * Frequento i lituani (sia uomini che donne)
  
 * Mi muovo necessariamente anche da solo senza per forza fare gruppo con gli italiani
  
  
Riguardo alla lingua lituana, penso che sia l'elemento assolutamente più importante. L'effetto di sentire uno straniero che parla il lituano non è paragonabile di certo ad uno straniero che parla l'italiano e se fate uno sforzo di immaginazione, capirete facilmente **perché la gente mi prende in simpatia** quando mi sforzo a farmi capire usando lingua locale.


 Frequento sia ragazzi che ragazze e cerco sempre di ricordarne i nomi. Gli uomini lituani si salutano sempre con la stretta di mano che dalle mie parti è alquanto inusuale, e io accompagno sempre il saluto con il nome della persona. Lo faccio sia per ricordarmi i nomi che per noi italiani risultano non molto orecchiabili e anche per dare importanza alla persona.  
Con le donne parlo più volentieri che con gli uomini: sono più belle e sono sicuro che il fine non sia un litigio portato dall'alcol e dalla xenofobia.


 **Il frequentare gli italiani del posto rende la permanenza in Lituania più leggera, ma alla fine è controproducente**. Qui a Klaipeda siamo veramente in pochi a frequentarci e non sempre riusciamo a vederci come vorremmo, io fortunatamente sono abituato a muovermi da solo. Molte volte mi è capitato di andare in un locale da solo e così essere obbligato a conoscere nuove persone, per molti questo modo di fare è difficile, ma a me non ha mai dato fastidio.


 Concludendo: sono contento di come sto vivendo questi primi sette mesi in Lituania, di come ho impostato la mia vita, di dove ho preso casa, ma soprattutto di aver rivalutato il paese cercando di **circondarmi di cosa la Lituania ha di buono da offrire.**

