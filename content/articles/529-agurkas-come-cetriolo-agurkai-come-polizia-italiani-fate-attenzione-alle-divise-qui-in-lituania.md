Title: Agurkas come cetriolo, agurkai come polizia: italiani, fate attenzione alle divise qui in Lituania
Date: 2009-03-24
Category: Lituania
Slug: agurkas-come-cetriolo-agurkai-come-polizia-italiani-fate-attenzione-alle-divise-qui-in-lituania
Author: Karim N Gorjux
Summary: Mi sono trovato in una di quelle situazioni che è sempre avere qualche amico pronto ad aiutarti. La ruota era bucata e io e Fernando abbiamo aiutato Fabrizio, vecchia conoscenza del blog,[...]

[![](http://farm3.static.flickr.com/2269/2163623344_2b7b04f685_m.jpg "Volante della polizia lituana")](http://www.flickr.com/photos/58389952@N00/2163623344/)Mi sono trovato in una di quelle situazioni che è sempre avere qualche amico pronto ad aiutarti. La ruota era bucata e io e Fernando abbiamo aiutato Fabrizio, vecchia conoscenza del blog, a cambiare la gomma della sua auto. 

 I bulloni emanavano qualcosa di mistico, quella macchina presa [dal rent a car](http://www.autoeurope.it/) di sempre mi aveva trasformato la giornata grigia in cui mi ero svegliato in una giornata speciale. Il colore rossastro della ruggine sul metallo dalla forma irregolare e usurata facevano presupporre che la ruota fosse stata creata direttamente dal Signore degli Anelli. Ad accrescere il velo di mistero sulle origini della ruota, il curioso fatto che la chiave, invece di svitare il bullone, si piegava sotto i nostri inutili sforzi.  
  
Niente da fare, la ruota non si riesce a cambiare. Fernando chiama un altro "scappato da casa" che approfitta del Litas per dare valore alla sua pensione, il nostro caro Pedro, spagnolo cinquantenne ben ambientato in questo piccolo angolo di Lituania, che se ne arriva con la chiave a croce e lo spray del buon viaggiatore esperto e pronto ad ogni evenienza. Nemmeno le spruzzate dell'acqua miracolosa e la chiave celtica hanno rimosso la ruota.


 Tra parlate in lituano, spagnolo, italiano e chiamate al carro attrezzi, sono passati si e no una quindicina di volte due volanti della polizia a vedere cosa facessimo. Nemmeno una volta si sono fermati a chiedere qualcosa o se avevamo lontanamente bisogno di un meccanico o di un rent a car dopo 1 ora e mezza al freddo e al gelo.


 Mi rimarrà impresso negli anni a venire, il momento in cui una BMW è passata davanti a noi, in centro abitato, a circa 100 km orari, prendendo una curva come se alla guida ci fosse [Bruce Willis](http://en.wikipedia.org/wiki/Bruce_Willis) e non un rincoglionito lituano. Il tutto è avvenuto davanti agli occhi di un paio di poliziotti su una volante che hanno avuto la stessa reazione come se a guidare la macchina fosse stato realmente Bruce Willis: hanno continuato a mangiare i popcorns e bere cocacola.


 In quel preciso istante ho realizzato che la Lituania sulla percezione popare della sicurezza investe quanto la Colombia e che è sempre meglio controllare la macchina prima di farsi un viaggio per la Lituania.


