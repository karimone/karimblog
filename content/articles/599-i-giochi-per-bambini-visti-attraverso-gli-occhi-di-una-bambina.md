Title: I giochi per bambini visti attraverso gli occhi di una bambina
Date: 2010-04-03
Category: Lituania
Slug: i-giochi-per-bambini-visti-attraverso-gli-occhi-di-una-bambina
Author: Karim N Gorjux
Summary: Questo è un piccolo esperimento. Ho provato a raccontare una piccola esperienza di vita attraverso gli occhi di mia figlia. E' un piccolo esercizio di empatia e di scrittura in stile racconto.[...]

![](http://farm3.static.flickr.com/2545/3761066721_3e5d4f4de5_m.jpg "Greta sullo scivolo")  
Questo è un piccolo esperimento. Ho provato a raccontare una piccola esperienza di vita attraverso gli occhi di mia figlia. E' un piccolo esercizio di empatia e di scrittura in stile racconto. Fammi sapere cosa ne pensi.


 Quando usciamo fuori, mio padre mi fa prendere l'ascensore, a me piace entrare velocemente nell'ascensore appena le porte si aprono così premo il tasto che fa chiudere le porte. Il tasto 1 lo schiaccio quando andiamo all'asilo o usciamo a giocare, il tasto 3 invece quando torniamo a casa. Mio padre mi ha detto che il tasto giallo che sembra una piccola campana non lo devo premere ed infatti ho notato che gli adulti che ogni tanto prendono l'ascensore con noi, non lo schiacciano mai. Schiacciano tutti altri simboli strani, ma forse perché loro vanno ad un asilo diverso dal mio.  
  
Nel mio quartiere, i palazzi sono colorati e sono grandissimi, quando scendiamo in cortile, mi sembra di essere una formichina in mezzo ai lego con cui gioco spesso; davanti al palazzo dove abito c'è un palazzo rosso, ma non tanto distante c'è ne un altro blu, altri invece sono marroni ed altri ancora sono dipinti con più colori. Nessuno edificio è alto uguale, proprio come le mie costruzioni quando gioco con i mattoncini.


 Tra tutti questi palazzi ci sono delle piccole oasi verdi per i bambini come me. Dei piccoli quadrati di erba verde ospitano lo scivolo, la sabbia per le costruzioni, l'altalena e anche le panchine per fare sedere mia mamma e papà quando mi portano fuori a giocare. I giochi hanno un forte profumo di legno, proprio come gli alberi nella foresta vicino a casa mia, ma a me piace più andare sullo scivolo o sedermi vicino sulla sabbia con il secchiello perché andare nella foresta non è così divertente. Mio padre dice che nella foresta ci sono tanti animali come la volpe, il lupo e il cerbiatto, ma tutte le volte che siamo andati a fare una passeggiata nella foresta io non ho mai visto niente. Meglio giocare!

 Lo scivolo è bellissimo, quello davanti a casa mia mi piace tanto perché è facile salirci sopra, non ha le scale come tutti gli altri scivoli, ma una larga salita fatta con tronchi di legno che porta in alto fino alla torre dello scivolo. Quando sono sulla torre, mi sembra di essere su un castello e chiamo il mio papà per venire qui con me. Lui dice che non può salire perché è troppo grande e allora viene vicino alla torre e io lo guardo dall'alto attraverso i paletti di legno. Come è piccolo il mio papà da qui e come è bello stare in alto, sembra tutto più piccolo e io mi sento tanto grande.


 I bambini che vengono a giocare sulla torre si siedono e scivolano seduti sulla parte argentata che sembra uno specchi, ma a me piace di più correre prendendo al contrario la salita di legno. Mi piace perché non devo sedermi e vado molto più veloce, ogni tanto cado e mio padre me lo ha anche detto di fare attenzione a non cadere, ma sto diventando sempre più brava e corro sempre più veloce. Quando mi stufo, porto il mio papà allo scivolo vicino alla foresta. Questo è più piccolo dello scivolo vicino a casa mia, ha le scale, non è in legno, ma è molto più alto. Mi piace fare le scale e poi scivolare giù, ma purtroppo la scivolata dura poco e allora appena arrivo a terra corro subito sulle scale per scivolare di nuovo, mio padre intanto mi ricorda di fare attenzione dalla panchina su cui e seduto a guardarmi.


 Io intanto guardo il mio papà seduto e penso che deve essere proprio brutto essere genitore, devi stare sempre seduto e non puoi mai andare sullo scivolo, forse non si vogliono sporcare. Ma non lo sanno questi genitori che il divertimento è proprio quello?

