Title: Squali
Date: 2008-01-29
Category: altro
Slug: squali
Author: Karim N Gorjux
Summary: L'impressione che ho in questo periodo è di essere Nemo in un acquario di squali, ogni occasione è buona per chiederti qualcosa, per farti pagare... soldi, soldi e ancora soldi. Quando nella[...]

L'impressione che ho in questo periodo è di essere [Nemo](http://thecia.com.au/reviews/f/images/finding-nemo-2.jpg) in un acquario di squali, ogni occasione è buona per chiederti qualcosa, per farti pagare... soldi, soldi e ancora soldi. Quando nella buca delle lettere vedo qualcosa di nuovo mi viene già paura, quale sigla si saranno inventati stavolta? ... inail, irpef, ici, inps, aci, oci, eci, uci e vi. Vi come vaffanculo.


 Dopo questa esperienza, che non vedo l'ora di chiudere, ho maturato un odio viscerale con alcune categorie:  
  
* **I commercialisti**: figura emblematica che sa tutti i cazzi tuoi e gode nel saperli quasi come un prete al confessionale. "E poi?! Cosa hai fatto? Ti sei toccato? Confessa figliolo...". Il commercialista si fa pagare per fare qualcosa che non ho mai capito bene cosa fosse, se poi sbaglia i conti chi deve pagare ovviamente sei tu. Infame!


 - **I notai**: apri una società paghi, la modifichi paghi, la chiudi paghi. Io sono generoso. Vaffanculo! Gratis.



 - **Le banche**: dire che le banche sono ladre è troppo poco lascio a voi il piacere di aggiungere tutti gli insulti immaginabili per descrivere cos'è una banca in Italia. Speculano con i soldi dei loro clienti, aggiungono delle commissioni strane targate "competenze a debito", mandano un modulo di trasparenza in cui non si capisce nulla e sono disoneste. Vaffanculo anche a loro. A credito.

  


