Title: Thunder!
Date: 2008-05-30
Category: Lituania
Slug: thunder
Author: Karim N Gorjux
Summary: Questo angolo al nord-ovest dell'Italia è particolarmente sfigato, mentre in tutto il resto del paese è già estate, qui continua a piovere senza ritegno. A parte due o tre giorni di pausa[...]

Questo angolo al nord-ovest dell'Italia è particolarmente sfigato, mentre in tutto il resto del paese è già estate, qui continua a piovere senza ritegno. A parte due o tre giorni di pausa qua e là, è dal 1 Maggio che piove.  
  
Non che mi dispiaccia la pioggia, ma tutto, dopo un po', inizia a stufare! Tuonasse sarebbe meglio...


 Inoltre tutto questo nuvolo mi ricorda l'autunno lituano, domani è il primo Giugno e qui sembra di essere in Lituania. Ne risente parecchio il mio blog, la pioggia distrugge la mia ispirazione.


   
  


