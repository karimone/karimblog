Title: Scelte
Date: 2006-12-29
Category: altro
Slug: scelte
Author: Karim N Gorjux
Summary: Come racconta Steve Jobs in questa intervista (via mat), la vita è fatta di scelte. Ogni scelta ha delle conseguenze e l'io che sono adesso non è nient'altro la somma di scelte[...]

Come racconta Steve Jobs in [questa intervista](http://espresso.repubblica.it/dettaglio/Siate-curiosi-siate-folli/1463668) (via [mat](http://blog.wastedthoughts.net/?p=266)), la vita è fatta di scelte. Ogni scelta ha delle conseguenze e l'io che sono adesso non è nient'altro la somma di **scelte e non scelte** che ho fatto in passato.


 Alcune decisioni hanno fatto semplicemente cagare, ma hanno portato a cose stupende. Per fare un esempio pratico, se ora fossi laureato Greta e Rita non farebbero parte della mia vita, sinceramente questo mi rattrista tantissimo, soprattutto se penso alle [possibilità di lavoro](http://www.precariarestanca.it/2006/02/22/universita-la-ureati-meno-disoccupati-ma-piu-precari/) in Italia di un laureato.


 Ora ho 27 anni compiuti, ho una moglie stupenda e una bimba altrettanto stupenda, una bella casetta, buoni amici, la salute, 10kg di troppo, pochi soldi e tante idee e buoni propositi per riuscire. Ho sempre pensato da piccolo a quanto fosse incomprensibile e difficile la vita da adulto; ora mi ritrovo ad essere adulto, piano piano mi ci sono ritrovato incastrato senza accorgermene, mancano i soldi, devo cambiare macchina, c'è l'affitto da pagare, ma sono tutte cose che invece di buttarmi giù mi stimolano.


 Nel libro che Matteo ha [sfogliato](http://blog.wastedthoughts.net/?p=265) a casa mia, ci sono tanti aforismi, uno di questi calza a pennello con il post:  

> *La vita o è un'audace avventura o non è niente.*  
> [Helen Keller](http://it.wikipedia.org/wiki/Helen_Keller)

