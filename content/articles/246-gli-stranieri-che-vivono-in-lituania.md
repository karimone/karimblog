Title: Gli stranieri che vivono in Lituania
Date: 2006-10-27
Category: Lituania
Slug: gli-stranieri-che-vivono-in-lituania
Author: Karim N Gorjux
Summary: Stranieri che vivono o hanno vissuto in Lituania.

Stranieri che vivono o hanno vissuto in Lituania.  


  - **55%** Prima centrifugati (nel senso che gli è stata fatta girare la testa) , poi fatti a fettine sottili sottili, soffritti a dovere e quindi respinti dal tessuto sociale (in pratica se ne sono dovuti andare per disparate ragioni).



  - **10%** Hanno perso l‘uso della ragione (per comodità scriviamo ricoverati in manicomio)


  - **10%** Alcolizzati o finti schiavi di vizi di vario genere.



  - **5%** Stanno cercando di capire perché sono sempre impegnati nel business non guadagnando un soldo, ma non hanno il tempo di fermarsi a pensarci.



  - **3%** Hanno una posizione lavorativa tale che perfino l‘assicella del water di casa è motorizzata.



  - **7%** Ci vivono e non si lamentano.



  - **10%** Ci vivono non bene ma probabilmente vivrebbero peggio altrove.



 