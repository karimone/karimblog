Title: Che palle!
Date: 2005-05-05
Category: Lituania
Slug: che-palle
Author: Karim N Gorjux
Summary: Rita mi ha lasciato da solo e mi sto rompendo le palle! Dai, non mi sto rompendo cosi' tanto, pero' devo dire che la sua non presenza lascia spazio ad un vuoto[...]

Rita mi ha lasciato da solo e mi sto rompendo le palle! Dai, non mi sto rompendo cosi' tanto, pero' devo dire che la sua non presenza lascia spazio ad un vuoto a cui non ero abituato.


 Tra i vari impegni non sono andato in palestra, Rita e' partita alle 14 e non e' nemmeno riuscita ad aiutarmi a prendere il libro che mi e' arrivato in posta, devo presentarmi nell'ufficio compilando un foglio tutto in lituano, questo e' il problema a vivere qui e non capire la lingua locale. Bisogna porre rimedio a questi impedimenti delle pallissime... chiedero' aiuto a Tolvaida o a Rasa.


 Stranamente nella palestra da superifco se non entri prima delle 16 non ti fanno piu' entrare. L'orario di apertura e' 07-16, ma valli a capire! Sono andato a fare un giro verso la spiaggia, tra nuvole e vento ho giusto toccato la sabbia durante una telefonata con Enrico e mi sono subito avviato per la via del ritorno. Passeggiando tra le case e la gente, ho fatto qualche foto ai vari edifici, mai terminati, che testimoniano il non troppo delicato passaggio URRS - Indipendenza, ho visto qualche donzella che ho salutato con il mio sguardo impenetrabile e significativo :shock: Un salto all'iki market per comprare due schifezze da mangiare ed eccomi qui che aggiorno il mio sito e aspetto di andare con i miei amici in centro.


 Tutto bene figlioli..


 (a fondo pagina sulla destra trovate la votazione!!!)

