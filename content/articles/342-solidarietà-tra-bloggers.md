Title: Solidarietà tra bloggers
Date: 2007-06-29
Category: altro
Slug: solidarietà-tra-bloggers
Author: Karim N Gorjux
Summary: Sono molto vicino a Ivan dopo ciò che gli è successo. Certe cose non dovrebbero mai capitare.

Sono molto vicino a Ivan dopo ciò che gli è [successo](http://ivanmez.blogspot.com/2007/06/quelle-maledette-non-hanno-rubato-solo.html). Certe cose non dovrebbero mai capitare.


 Purtroppo queste vicende lasciano l'amaro in bocca, come si fa ad uscirne mantenendo la fiducia per gli altri? Essere derubato è veramente qualcosa che ti atterra.


 Dai Ivan! Passerai anche questa.


