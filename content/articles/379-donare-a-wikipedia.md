Title: Donare a Wikipedia
Date: 2007-10-25
Category: altro
Slug: donare-a-wikipedia
Author: Karim N Gorjux
Summary: Ho trovato molto genuino e affascinante il video di Jimmy Wales, il fondatore di Wikipedia, dove spiega il motivo per cui, persone come me e voi, dovrebbero donare anche una modesta somma[...]

Ho trovato molto genuino e affascinante il video di [Jimmy Wales](http://it.wikipedia.org/wiki/Jimmy_Wales), il fondatore di Wikipedia, dove spiega il motivo per cui, persone come me e voi, dovrebbero donare anche una modesta somma di denaro a Wikipedia.


 Il video è sottotitolato in Italiano e lo potete vedere direttamente[qui](http://wikimediafoundation.org/donate/2007/psa/subtitled-it.html).


 La conoscenza è tutto!

