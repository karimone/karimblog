Title: Hell freeze over, but is always a hell
Date: 2007-12-29
Category: Lituania
Slug: hell-freeze-over-but-is-always-a-hell
Author: Karim N Gorjux
Summary: Preso già dallo sconforto e dalla lontananza dalla madre patria, mi arriva in aiuto Davide (senza link) con questo interessante sito: wikipedia in piemontese. In particolare ho notato questo aiuto contro l'analfabetismo[...]

Preso già dallo sconforto e dalla lontananza dalla madre patria, mi arriva in aiuto Davide (senza link) con questo interessante sito: [wikipedia in piemontese](http://pms.wikipedia.org/wiki/Intrada). In particolare ho notato questo aiuto contro [l'analfabetismo piemontese](http://pms.wikipedia.org/wiki/Wikipedia:Lese_%C3%ABl_piemont%C3%A8is) che ormai tocca il 98% della popolazione (anche quel mio zio affetto da bibliomania da cesso).


 Greta sta ora meglio. Dopo aver visto due dottori, abbiamo iniziato una cura che la sta portando piano piano alla pronta guarigione, niente più febbre e finalmente nottate lunghe passate a dormire e non a cantare le ninne nanne. Per il resto è sempre la **solita menata**, non lasciate mai niente per scontato e soprattutto non permettere che un lituano/a organizzi il tuo viaggio in Lituania: siamo bloccati nel cuore della Lituania, in mezzo al nulla, senza niente da fare e senza una macchina con cui spostarci. Dulcis in fundo stendiamo un velo pietoso sul mangiare che è ancora peggio di come lo ricordavo e non ci sono nemmeno ristoranti dove andare a passare il pranzo.


 Case sovietiche e nulla, questa è l**a Lituania nascosta** ai vari turisti che soggiornano a Vilnius in cerca di gonnelle, la noia predomina le tipiche giornate invernali in questi paesini dimenticati da dio. La gente qui dovrebbe lavorare gratis perché senza lavoro non avrebbe niente da fare, assolutamente niente con cui riempire la giornata, ma questo vale solo per l'inverno, che, purtroppo per loro, dura 8 mesi l'anno.


 Ormai avrete capito che qui non c'è nulla che sta andando per il verso giusto, ringrazio di avere una connessione ad internet quasi decente, altrimenti sarei ancora più nella merda, purtroppo però mi tocca usare un PC rumoroso, ingombrante e pieno di fili con sopra installato **Windows XP in Lituano!** (Passo il più del tempo a clicclare alla cieca) Il mio Mac, purtroppo, non posso collegarlo alla rete e se ne rimane sempre nella borsa.


 Non me ne vanno tante per il verso giusto, in primis la saluta di Greta, ma almeno il tempo passa. Dedico questi **domiciliari forzati** in casa [Chruščëv](http://it.wikipedia.org/wiki/Nikita_Khru%C5%A1%C4%8D%D1%91v) studiando, chattando e facendo tante altre cosette al computer. Se non guardo la bambina sono con l'iPod che ascolto musica e gioco a [Klondike](http://it.wikipedia.org/wiki/Klondike_%28solitario%29). Inutile dire che dal 23 che sono arrivato qui avrò passato si e no circa 2 ore all'aria aperta. E ci credo, sono a [Telsiai](http://it.wikipedia.org/wiki/Contea_di_Tel%C5%A1iai).  
  
[![Picture 199](http://farm3.static.flickr.com/2293/2146067802_f9f6a85734_m.jpg)](http://www.flickr.com/photos/kmen-org/2146067802/ "Picture 199 di karimblog, su Flickr")  


