Title: Uno dei motivi per scappare
Date: 2006-10-12
Category: Lituania
Slug: uno-dei-motivi-per-scappare
Author: Karim N Gorjux
Summary: Ovviamente il turista che si fa il giro in Lituania per 15 giorni a salutare le morose non se ne può rendere conto, ma la Lituania ha il record come peggiore sistema[...]

Ovviamente il turista che si fa il giro in Lituania per 15 giorni a salutare le morose non se ne può rendere conto, ma la Lituania ha il record come **peggiore sistema sanitario d'Europa**. E' stato uno dei motivi, toccati con mano, che mi ha fatto tornare a casa a gambe levate.


 Fonte: [Baltic Times](http://www.baltictimes.com/news/articles/15731/)

