Title: Intervista a Mirko, studente Erasmus a Klaipeda
Date: 2011-07-20
Category: altro
Slug: intervista-a-mirko-studente-erasmus-a-klaipeda
Author: Karim N Gorjux
Summary: Continua la serie delle interviste ai ragazzi erasmus a Klaipeda.

[![Punto interrogativo](http://www.karimblog.net/wp-content/uploads/2011/03/question_mark.jpeg "Domanda")](http://www.karimblog.net/wp-content/uploads/2011/03/question_mark.jpeg)Continua la serie delle interviste ai ragazzi erasmus a Klaipeda.


 **Come ti chiami, quanti anni hai, di dove sei e cosa studi**  
Mi chiamo Mirko, ho 22 anni, vengo da un paese della provincia di Brindisi e studio Lingue a Parma...il classico studente fuori sede

 **Dove è stato e quanto è durato il tuo erasmus**  
Il mio Erasmus è durato 5 mesi ed è stato a Klaipeda in Lituania  
  
**Ricordi le tue prime impressioni della Lituania?**  
Le prime impressioni quando sono arrivato in Lituania sono state: la neve!!! mai vista così tanta e il freddo tanto tanto freddo!!  
e non pensavo di trovare così tanti italiani in generale in lituania.


 **Com'è cambiata la tua idea sulla Lituania da quando sei arrivato a quando te ne sei andato?**  
Prima di partire pensavo che la lituania fosse un paese con molte differenze rispetto all'italia e sono ancora di questa opinione... di certo non mi aspettavo che in alcune cose fossero più organizzati e più moderni rispetto all'italia..... esempi: wi-fi ovunque, non servono documenti per una sim card, etc... la lituania è comunque un paese difficile in cui vivere anche per i lituani stessi e molti giovani conosciuti durante l’erasmus cercano di andare via il prima possibile.


 **Quali sono le differenze culturali più grandi di uno studente della tua stessa età?**  
La maggior parte dei giovani e dei lituani sentono molto il concetto di nazione.. forse a volte esagerando!! di sicuro esagerano nel bere... ma sostanzialmente non riscontro grandi differenze con i giovani italiani...


 **Quali sono i grandi pregi dei lituani e quali sono i grandi difetti?**  
I pregi dei lituani è che se ti accettano sanno essere molto disponibili, cordiali e simpatici e con i nostri coetanei è più facile socializzare rispetto a persone più adulte.... i difetti: NON SORRIDONO MAI!!! e alcuni di loro sono molto diffidenti verso gli stranieri...


 **Cosa ti è piaciuto della Lituania e cosa non ti è piaciuto?**  
Una delle cose che mi è piaciuta della lituania oltre alle ragazze, è stata la birra e anche il cibo non è male... poi hanno dei posti interessanti da visitare!!  
cose che non mi sono piaciute: il tempo!! troppo rigido per me!! (è uno dei motivi per cui non potrei vivere in lituania) e il carattere delle persone credo sia influenzato dal tempo soprattutto..


 **L'esperienza Erasmus è sempre particolare, non è paragonabile ad una vita normale nel paese che ti ha ospitato, ma da come hai visto la vita in Lituania, ti piacerebbe lavorarci e viverci?**  
L'Erasmus è diverso dalla vita reale e dopo 5 mesi trascorsi in Lituania posso dire che tornerò per una vacanza ma non per viverci né tantomeno per lavorarci… gli stipendi medi lituani sono più bassi rispetto agli standard europei, è davvero difficile sopravvivere in lituania…. 

 **Quali sono i consigli che daresti ad un ragazzo che come te si sta avvicinando alla Lituania per Erasmus ed in particolare nella città di Klaipeda?**  
Un consiglio per chi va in erasmus in lituania è quello di cercare di visitare tutte le città e i bei posti che ci sono in lituania perchè ne vale davvero la pena..... Klaipeda è uno di questi soprattutto il centro storico molto carino…..


 **Hai avuto una relazione in Lituania? Se si, pensi di tornarci per amore?**  
Non ho avuto una relazione durante l’erasmus e comunque è normale che ci tornerei per amore ma non per viverci stabilmente lì

 **Che consigli daresti a chi sta affrontando una relazione da studente erasmus in Lituania?**  
Un consiglio che posso dare è quello di avere pazienza tanta pazienza come in ogni rapporto, specie se è a distanza....


 **La tua esperienza più bella?**  
L'esperienza più bella è stata conoscere tanta gente (italiani,lituani e del resto d'Europa) e trascorrere con loro tanti bei momenti..... Uno di questo è stato conoscere un ragazzo di origine palestinese e un altro di origine israeliana che sono grandi amici…

 **La tua esperienza più brutta?**  
INCONTRARE KARIM!!! Esperienze brutte non ci sono state sostanzialmente.... a parte patire davvero il freddo!! il prossimo inverno sarà una passeggiata in italia.. ;)

