Title: Ritorno in Lituania (e non in bellezza)
Date: 2007-12-25
Category: Lituania
Slug: ritorno-in-lituania-e-non-in-bellezza
Author: Karim N Gorjux
Summary: Sembra quasi che lo faccia apposta, ma in realtà io non ne posso niente, non sono io ad avercela con la Lituania, ma la Lituania ad avercela con me.

Sembra quasi che lo faccia apposta, ma in realtà io non ne posso niente, non sono io ad avercela con la Lituania, ma la Lituania ad avercela con me.


 Fino a dove arrivavano le mie competenze non c'è stato il minimo problema, albergo, custodia della macchina, volo... tutto è andato per il meglio. I casini sono cominciati quando siamo arrivati a Vilnius. Il primo intoppo è stato il viaggio, lasciamo stare il mezzo, lasciamo stare il casino per le strade, ma alla fine dopo circa 350km siamo arrivati a Telsiai, nel bel mezzo della Lituania.  
Qui non c'è nulla, la mattina mi sveglio e sembra già sera, non c'è niente da vedere o da fare e fa un freddo cane. Non voglio lamentarmi del freddo o del buio perché questa è la Lituania, si sa che d'inverno qui non è un bel alloggiare.


 La prima notte l'ho passata sul cesso a vomitare il pasto di FlyLal (Lithuanian Airlines) e Greta ha iniziato a stare male prendendo la febbre ad ore alterne. Ma il peggio è la noia, sempre chiusi in casa, poche possibilità di viaggiare, di uscire o prendere una macchina... mi tocca, sono 10 giorni, ma mi tocca.


 Prossimo anno: Caraibi.


