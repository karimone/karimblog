Title: Flavio D'Inca
Date: 2007-06-27
Category: Lituania
Slug: flavio-dinca
Author: Karim N Gorjux
Summary: Nella mia modesta permanenza a Klaipeda ho avuto la fortuna di conoscere un personaggio interessante come Flavio d'Inca. Flavio e sua moglie sono due persone assolutamente fantastiche e la loro storia di[...]

Nella mia modesta permanenza a Klaipeda ho avuto la fortuna di conoscere un personaggio interessante come Flavio d'Inca. Flavio e sua moglie sono due persone assolutamente fantastiche e la loro storia di viaggi per il mondo è assolutamente incredibile.


 Flavio è consorte sono di Milano, circa 15 anni fa, **stressati dal traffico, dallo smog e dal cemento** in ogni angolo decidono di cambiare vita. Partono per la [Malesia](http://it.wikipedia.org/wiki/Malaysia) dove vivono per 5 anni circa lavorando in un piccolo B&B di cui sono proprietari.


 Purtroppo la morte dei piccoli sono le grandi multinazionali, i grandi nomi del turismo prendono di mira la Malesia e Flavio è costretto a cambiare: destinazione Portogallo.  
In Portogallo ristrutturano una casetta e continuano il lavoro nel turismo, un bel B&B dal tipico sapore locale, ma il tragico attentato del 11 Settembre 2001 uccide completamente il turismo locale e Flavio è costretto a cambiare di nuovo, rimane in Francia per un paio d'anni prima di scegliere come meta Palanga.


 Flavio mi invita a casa sua, un piccolo appartamento a pochi passi dal centro di Palanga, e mi racconta la sua avventura. La scelta sulla Lituania è ricaduta dopo giorni e giorni a cercare informazioni su internet dove **le cifre sono tutte a favore** del popolo Lituano, una fra tutti la crescita economica che è tra le più alte d'Europa. Palanga è un posto altamente turistico, ma le cose non vanno per il senso giusto, la risposta della Lituania non è quella che si può intuire leggendo su internet, anche **le cose facili diventano difficili**. Flavio decide di lasciare la Lituania, il suo commento lasciato in merito a questo [articolo](http://www.karimblog.net/2007/03/23/italiettuva) confuta ogni dubbio.  

>  Tra due mesi lascerò la Lituania; dopo tre anni di residenza a Palanga. Forse dovrei dire che sto lasciando Palanga e Klaipeda e non la Lituania perchè Vilnius e’ un altra cosa: più moderna, più aperta.


 Peccato che sul mare ci sia Klaipeda e non Vilnius. Forse sarei durato un po’ di più? Non credo. Il “non capisco ma mi adeguo” qui non vale, almeno per me. Io non capisco e non mi adeguo. Peccato. I lati positivi li ho trovati anche qui e me li son goduti. Ho alcuni amici e amiche e un po’ mi dispiace lasciarli. Quando uno lascia un luogo, sembra che i rapporti si intensifichino e si arriva al solito “Che peccato! Propio adesso che cominciavamo a…”

 Fatto sta che non venni qui aspettandomi di fare amicizie lituane ma per investire in un piccolo business, così come ho fatto in altri Paesi. Resomi conto delle difficoltà, dei rischi e dei prezzi ormai astronomici, ho tirato le somme. **Risultato: negativo**.


 Tra le cose che ho imparato vivendo qui c’è la seguente: non resisto ad autunno e inverno così lunghi. Voglio il sole e la primavera tutto l’anno! …e so dove sono, un po’ lontani ma sto per raggiungerli.


 Ah…dimenticavo! **Non e’ vero che il tempo passa. Siamo noi a passare**.


 Iki.


 Flavio

