Title: Particolarità della società lituana vista attraverso gli occhi di un italiano (Parte 2)
Date: 2009-12-15
Category: Lituania
Slug: particolarità-della-società-lituana-vista-attraverso-gli-occhi-di-un-italiano-parte-2
Author: Karim N Gorjux
Summary: Riprendo il discorso delle particolarità lituane iniziato con l'articolo precedente. Questa mattina, mentre facevo colazione, mi sono venute in mente altre cosette che potrebbero incuriosire chi visita la Lituania.

![De Gustibus](http://www.karimblog.net/wp-content/uploads/2009/12/degustibus.jpg "De Gustibus")Riprendo il discorso delle particolarità lituane iniziato con [l'articolo precedente](http://www.karimblog.net/2009/12/14/particolarita-della-societa-lituana-attraverso-gli-occhi-di-un-italiano/). Questa mattina, mentre facevo colazione, mi sono venute in mente altre cosette che potrebbero incuriosire chi visita la Lituania.


 **Le prese elettriche sono tutte uguali**, sono del tipo "tedesco" o almeno così vengono chiamate in Italia, testa rotonda con le due corna senza la massa centrale. Qualsiasi presa al muro funziona con gli apparecchi di casa, non esiste grande senza massa, piccola con massa e non devi andare nel negozio a chiedere un convertitore da piccolo a grande e sentirti dire per l'ennesima volta che non sono a norma.   
  
Ho una scatola nell'armadio con tutte le prese multiple italiane che qui non uso. **Ricordo lo stupore e il disappunto di Rita** ogni qualvolta in Italia mi trovavo in difficoltà elettriche solamente ad attaccare un apparecchio alla rete elettrica.


 Nella camera da letto dovevamo far passare una prolunga dietro all'armadio per raggiungere l'unica presa "grande". Robe da pazzi! Qui in Lituania tutto funziona semplicemente, attacchi e và! Ci voleva tanto?

 Rimanendo in tema "cose di casa", **gli interruttori della luce sono ad altezza bambino **e questo è davvero utile se hai figli in casa. Dato che i lituani i figli li fanno e soprattutto crescono i bimbi meno rincoglioniti, fanno in modo che a tre anni un bimbo possa andare in bagno e accendersi la luce da solo dato che l'interruttore è a 90cm (novanta) da terra. (Fidatevi, l'ho misurato io!)

 **Le tapparelle non esistono o non sono cosa gradita**. Ogni casa che ho visitato e tutte le finestre del mio quartiere hanno delle tendine internet. Il mio padrone di casa ha messo delle tendine particolari che hanno due caratteristiche che le rendono l'acquisto dell'anno: si rompono e non coprono la luce. Rita dice che le tapparelle esterne non si possono mettere per questioni estetiche; probabilmente è vero, se fai un giro per le città potrai notare come l'aspetto più schifoso dei vecchi palazzi sovietici sono i balconi e le finestre tutte diverse.


 Ricordo che da mia nonna mi facevo delle dormite fantastiche nella camera degli ospiti, tiravo giù la tapparella e anche alle due del pomeriggio del 15 Agosto calavano le tenebre in tutta la stanza. Non capirò mai come l**a Lituania che d'estate ha 20 ore di luce al giorno**, non abbia adottato questa bella invenzione della tapparella.


 **In cucina vanno di moda le piastre elettriche di nuova generazione**. Non sono un cuoco, ma l'unico vantaggio che trovo nelle piastre elettriche è la facilità nel pulirle, per tutto il resto sono solo un modo in più per renderti la vita in cucina più difficile.  
Le piastre si accendono premendo dei pulsanti in puro stile futuristico vicino alle piastre, il bello è che può capitare di sfiorarle per spegnere tutto senza rendersene conto inoltre è facile dimenticarsi un fornello spento o acceso dato che il fuoco non c'è. Dubito che i ristoranti lituani usino le piastre elettriche.


