Title: Ogni giorno sento parlare tante lingue diverse. Mi sento più in Europa qui che in Italia
Date: 2009-04-07
Category: Lituania
Slug: ogni-giorno-sento-parlare-tante-lingue-diverse-mi-sento-più-in-europa-qui-che-in-italia
Author: Karim N Gorjux
Summary: Ogni giorno sento diverse lingue, in casa sento parlare italiano, fuori casa sento sia il russo che il lituano. Con gli erasmus sento parlare inglese, francese e spagnolo. Ultimamente mi è persino[...]

[![Uunione Europea](http://www.karimblog.net/wp-content/uploads/2009/04/unione-europea.jpg "Unione Europea")](http://www.karimblog.net/wp-content/uploads/2009/04/unione-europea.jpg)Ogni giorno sento diverse lingue, in casa sento parlare italiano, fuori casa sento sia il russo che il lituano. Con gli erasmus sento parlare inglese, francese e spagnolo. Ultimamente mi è persino capitato di sentire parlare l'ungherese e il tedesco.


 Ieri ero al corso di Lituano. Siamo in pochi a seguirlo giusto qualche erasmus e qualche straniero "residente" a Klaipeda: io, uno spagnolo, Chiara, una polacca, un'ungherese, una slovacca e una ragazza bielorussa. Il corso viene tenuto in inglese, ma l'insegnante parla con la ragazza bielorussa in russo e qualche volta scappa anche qualche parola in spagnolo.   
  
Proprio ieri, durante il corso, ho realizzato che mi toccherà studiare il lituano seriamente, se non tutti i giorni, quasi tutti. Ho anche realizzato che l'Italia sta diventando sempre più un ricordo lontano; il sentire parlare più lingue diverse quotidianamente non è proprio all'ordine del giorno in un paese come Cuneo.


 Rialacciandomi al discorso degli [italiani e l'inglese](http://www.karimblog.net/2009/04/02/gli-italiani-non-sanno-linglese-purtroppo-e-un-dato-di-fatto-in-lituania-i-bambini-conoscono-tre-lingue/). Qui sapere l'inglese è una condizione che non viene nemmeno messa in discussione.


