Title: Essere un buon marito ed un buon padre.
Date: 2006-04-24
Category: altro
Slug: essere-un-buon-marito-ed-un-buon-padre
Author: Karim N Gorjux
Summary: Al "mio" paese, sono pronto a scommettere, molti hanno detto e continueranno a dire (o meglio a predire) le sorti della mia vita e di chi mi vuole bene. Le persone sono[...]

Al "mio" paese, sono pronto a scommettere, molti hanno detto e continueranno a dire (o meglio a predire) le sorti della mia vita e di chi mi vuole bene. Le persone sono tutte uguali, ti giudicano, ti criticano, ti maledicono e ti sfottono. Questi sono gli insormontabili problemi che un paesino della provincia Granda può creare. La soluzione? Essere consapevole che il problema non sussiste, tutto è ancora da fare e il lavoro è molto.  
  
Bene, in teoria dovrei aver già chiaro come dovrei comportarmi perché, **sempre in teoria**, dovrei aver ricevuto una buona educazione come tutti, ma in realtà è meglio confutare tutto; meglio non fidarsi e istruirsi con dei buoni libri.  
Questi benedetti libri hanno l'ingrato compito di mettere del sale in zucca a me per non rovinare la vita di un bambino innocente che, se tutto và bene, dovrebbe entrar a far parte di questo sporco mondo a metà Agosto. Più leggo i libri e più mi viene paura, non è un mestiere facile quello del genitore o marito, bisogna conoscere i propri limiti, bisogna sapere che è possibile sbagliare, ma soprattutto ammettere che c'è ancora molto da imparare. 

 In passato ho conosciuto delle persone che in qualsiasi caso e in qualsiasi argomento volevano avere l'ultima parola e sempre ragione, a parte che una vita del genere è frustrante sia per chi la conduce e sia per chi la subisce, da cosa deriva un comportamento del genere? I genitori, purtroppo sono sempre loro i responsabili, volenti o nolenti. E' preoccupante però che questi bambini complessati diventeranno adulti, genitori e sbaglieranno a loro volta. è una catena molto difficile da spezzare.


 Leggendo mi rendo conto che molte cose sbagliate, le avrei fatte tutte in buona fede e senza il minimo dubbio di essere un ipotetico buon genitore, ma in realtà i piccoli particolari sono determinanti nello sviluppo di un bambino. Non riporto gli esempi perché passerei ore e ore a trascrivere pagine di libri coperti da copyright, ma ho capito che gli ingredienti sono molto semplici, sia con il partner che con il figlio, è importante avere rispetto, dare amore e soprattutto trattare la persona come una persona. Troppi genitori vedono il bambino come un semi-essere, un semi-adulto.** Il bambino è competente!** 

 Sarà dura..  
  
  
adsense  
  


