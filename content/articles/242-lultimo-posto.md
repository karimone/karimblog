Title: L'ultimo posto
Date: 2006-10-15
Category: altro
Slug: lultimo-posto
Author: Karim N Gorjux
Summary: Saint Vincent e Grenadine è uno stato composto da circa 125 isole facenti parte delle Piccole Antille. L'isola principale è Saint Vincent di 345 km² sulla quale è posta la capitale Kingstown.[...]

*Saint Vincent e Grenadine è uno stato composto da circa 125 isole facenti parte delle Piccole Antille. L'isola principale è Saint Vincent di 345 km² sulla quale è posta la capitale Kingstown. L'economia è legata soprattutto alla coltivazione delle banane ed alla pesca, il turismo non è ancora molto sviluppato. Popolazione: 116.812 abitanti.*

 Fonte: [wikipedia](http://it.wikipedia.org/wiki/San_Vincenzo_e_Grenadine)

 Suicidi a Saint Vincent e Grenadine: 0.


 Fonte: [Nation Master](http://www.nationmaster.com/graph/hea_sui_rat_mal-health-suicide-rate-males#rest)

