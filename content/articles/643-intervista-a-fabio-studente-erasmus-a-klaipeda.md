Title: Intervista a Fabio, studente Erasmus a Klaipeda
Date: 2011-07-18
Category: Lituania
Slug: intervista-a-fabio-studente-erasmus-a-klaipeda
Author: Karim N Gorjux
Summary: Nel mio ultimo anno a Klaipeda ho avuto la fortuna di conoscere Fabio e Mirko, due ragazzi poco più che ventenni che hanno passato il loro Erasmus in quella che fino a[...]

[![Erasmus al computer](http://www.karimblog.net/wp-content/uploads/2010/09/ErasmusComputer.jpeg "Erasmus al computer")](http://www.karimblog.net/wp-content/uploads/2010/09/ErasmusComputer.jpeg)Nel mio ultimo anno a Klaipeda ho avuto la fortuna di conoscere Fabio e Mirko, due ragazzi poco più che ventenni che hanno passato il loro Erasmus in quella che fino a pochi mesi fa era la mia città. Ho chiesto loro di rispondere ad alcune domande per il mio blog e sono stato subito accontentato. Ecco la prima intervista in ordine di arrivo :-) 

 **Come ti chiami, quanti anni hai, di dove sei e cosa studi.**  
Buondì :D Mi chiamo Fabio, vengo dalla Sardegna e studio lingue a Parma.


 **Dove è stato e quanto è durato il tuo erasmus**  
Ho passato il mio erasmus a Klaipeda, in Lituania. 5 mesi fantastici.  
  
**Ricordi le tue prime impressioni della Lituania?**  
Prima impressione assolutamente negativa. Il primo mese non è stato per niente facile. Appena arrivati ci siamo ritrovati sommersi dalla neve e circondati da persone ostili che non parlavano inglese. Poi per fortuna abbiamo avuto dei buoni amici, italiani e non, che ci hanno dato una mano ad ambientarci. 

 **Com'è cambiata la tua idea sulla Lituania da quando sei arrivato a quando te ne sei andato?**  
E' cambiata moltissimo, specialmente per quanto riguarda le persone. Ho sempre pensato che la gente del nord fosse molto fredda, ma i ragazzi e le ragazze che abbiamo incontrato li sono riusciti a cancellare il pregiudizio. Ovviamente non tutti, ma quelli di cui ci siamo circondati erano ottimi amici lituani.


 **Quali sono le differenze culturali più grandi di uno studente della tua stessa età?**  
La prima che ho notato è stata quella dei pasti. Mentre noi abbiamo degli orari più o meno prestabiliti, loro non hanno orario, semplicemente mangiano quando hanno fame. Poi tendono ad esagerare in molte cose: nel bere, nel fare palestra, nell'attaccamento alla nazione, persino nello studiare a volte, mentre noi siamo più rilassati. E più felici! Ovviamente queste sono generalizzazioni, non tutti i lituani rispecchiano queste differenze.


 **Quali sono i grandi pregi dei lituani e quali sono i grandi difetti?**  
Quelli che ho conosciuto generalmente avevano come difetto l'attaccamento allo studio, visto a volte come una via di fuga dalla Lituania, e molti un esagerato nazionalismo. Esternamente ai nostri amici ho notato una scarsa felicità e a volte un po di scontrosità. Tra i pregi posso inserire la grande ospitalità che hanno una volta conosciuti a fondo, la tendenza dei più giovani a voler eliminare i vecchi schemi sovietici e ad essere più curiosi verso gli stranieri. Parlando di ragazze, l'80 % sono belle, intelligenti e simpatiche. Poi sono campioni di basket, questo bisogna riconoscerlo!

 **Cosa ti è piaciuto della Lituania e cosa non ti è piaciuto?**  
Della Lituania mi sono piaciuti i paesaggi che si vedono al di fuori delle città, la vista del mare ghiacciato d'inverno e il sole che d'estate non lascia mai il cielo. Poi meritano menzione le ragazze, in quanto la maggior parte sono veramente bellissime, la birra e il cibo, spesso pesante ma molto saporito. Non mi è piaciuto il freddo esagerato dell'inverno, e il mare, che quando non è ghiacciato è terribilmente sporco (ma vabbè io per quello sono viziato!!).


 **L'esperienza dell'erasmus è sempre particolare, non è paragonabile ad una vita normale nel paese che ti ha ospitato, ma da come hai visto la vita in Lituania, ti piacerebbe lavorarci e viverci?**  
Difficilmente tornerò a lavorare o vivere in Lituania, penso che non sia un bel posto dove abitare al di là dell'erasmus. Una nazione fantastica, piena di pregi, ma per niente vivibile secondo me.


 **Quali sono i consigli che daresti ad un ragazzo che come te si sta avvicinando alla Lituania per Erasmus ed in particolare nella città di Klaipeda?**  
Non fatevi troppe aspettative sulle lituane. Sono belle si, ma non sono per niente stupide, e soprattutto non sono tutte di facili costumi come molti pensano. Non basta venire dal sud europa per venirsi a prendere una ragazza. Poi munitevi di abiti pesanti e di un buon fegato. Se farete amicizia con dei lituani vi servirà una certa esperienza nel bere!! Infine, state attenti ai posti che frequentate e a come trattate le persone: è un posto piuttosto pericoloso, almeno per quanto riguardo la città in cui ero io.


 **Hai avuto una relazione in Lituania? Se si, pensi di tornarci per amore?**  
Ebbene sì, ho avuto una relazione che tutt'ora va avanti. Penso che ci tornerò piuttosto spesso per amore, o almeno spero. Magari non andrò a viverci, ma la Lituania avrà sempre qualcosa di affascinante per me.


 **Che consigli daresti a chi sta affrontando una relazione da studente erasmus in Lituania?**  
Una relazione? Beh di viverla con leggerezza perchè le probabilità che finisca con la fine dell'erasmus sono altissime. Poi se avete la fortuna (o la sfortuna, dipende sempre dal/la compagna/o) di vedere la storia andare avanti dopo l'erasmus, allora cercate di mettere da parte dei soldi e di vedere il/la vostro/a partner più spesso possibile, e di trovare un punto d'incontro in caso di un futuro insieme.


 **La tua esperienza più bella?**  
Sarebbe scontato scrivere qualcosa riguardo la mia ragazza, quindi preferisco dire che l'esperienza erasmus è in se la mia esperienza più bella, grazie alle persone fantastiche che ho avuto modo di conoscere, lituane e non.


 **La tua esperienza più brutta?**  
Non so che scrivere qua. Tutto quello che ho passato in erasmus è servito in qualche modo a crescere, e anche le cose che all'inizio sembravano negative si sono trasformate in esperienze positive.


