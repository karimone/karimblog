Title: I piccoli imprevisti dell'ex unione sovietica
Date: 2006-05-18
Category: Lituania
Slug: i-piccoli-imprevisti-dellex-unione-sovietica
Author: Karim N Gorjux
Summary: Klaipeda, come tutte le città ex-sovietiche, può nascondere dei piccoli imprevisti. In questi giorni circa metà della città è senza acqua calda a causa di alcune riparazioni che hanno luogo in un[...]

Klaipeda, come tutte le città ex-sovietiche, può nascondere dei piccoli imprevisti. In questi giorni circa metà della città è senza acqua calda a causa di alcune riparazioni che hanno luogo in un non ben precisato punto della città. La spiacevole conseguenza è di lasciare al "freddo" circa 100.000 abitanti.


 Non ho idea se l'acqua calda è stata interrota per riparazioni, ma nulla esclude la possibilità di un ammodernamento delle strutture; ho notato che Klaipeda in questi mesi sta cambiando, sia nel centro che qui dove mi trovo io, sono in atto opere di ristrutturazione delle strade e di ammodernamento, non ho idea di cosa stiano facendo, ma tirano cavi, li sostituiscono, non lo so. Dovrei parlarne con Enrico.


 Rispetto ad un anno fa Klaipeda ha fatto dei cambiamenti notevoli, l'akropolis con la sua multisala annessa ne è forse l'esempio più lampante, ma ho fatto una visita alla nuova biblioteca comunale di cui l'ammodernamento è tale da far impallidire le nostre biblioteche: entrata con tessera magnetica, sala informazioni, nuove sale di consultazione, nuovi computer a disposizione dei lettori, personale di servizio e. dulcis in fundo, il tutto è pulito e molto ben curato. Enrico mi ha anche fatto sapere che l'università di Klaipeda ha aggiornato tutti i computer, non sono Mac, ma almeno sono nuovi. Con il tempo spero di sapere qualcosa di più sui vari lavori per capire cosa sta succedendo, i cantieri sono tanti e la città cambia in fretta anzi, la Lituania cambia in fretta...


 Da fonti attendibili ho saputo che questi ammodernamenti sono in atto grazie ad un progetto della comunità europea per il finanziamento delle aree più disagiate d'Europa. Alcuni anni fa il nostro mezzogiorno ha avuto dei finanziamenti dallo stesso progetto, ma ora tocca ai paesi Baltici ed il risultato è sotto il naso di tutti.


 W l'Italia!

 **Aggiornamento: **Enrico mi ha spiegato che l'acqua calda manca a causa della manutenzione che fanno ogni anno nel periodo di Maggio, questa è la mia settimana, la prossima toccherà ad Enrico..  
 technorati tags start 

 tags: [ammodernamento](http://www.technorati.com/tag/ammodernamento), [cantieri](http://www.technorati.com/tag/cantieri), [economia](http://www.technorati.com/tag/economia), [europa](http://www.technorati.com/tag/europa), [finanziamenti](http://www.technorati.com/tag/finanziamenti), [klaipeda](http://www.technorati.com/tag/klaipeda), [lithuania](http://www.technorati.com/tag/lithuania), [lituania](http://www.technorati.com/tag/lituania), [ue](http://www.technorati.com/tag/ue)

  technorati tags end 

