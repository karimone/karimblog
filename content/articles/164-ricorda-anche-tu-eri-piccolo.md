Title: Ricorda! Anche tu eri piccolo!
Date: 2006-04-15
Category: altro
Slug: ricorda-anche-tu-eri-piccolo
Author: Karim N Gorjux
Summary: Una cosa che ho sempre odiato è la poca considerazione che un bambino in famiglia. Generalmente il bimbo è considerato una protesi di un adulto: quando parla dice cazzate, se dice qualcosa,[...]

Una cosa che ho sempre odiato è la poca considerazione che un bambino in famiglia. Generalmente il bimbo è considerato una protesi di un adulto: quando parla dice cazzate, se dice qualcosa, ha poco senso e non è degna di essere ascoltata. Quando esprime la sua opinione è presa sempre sottogamba perché a giudizio dell'adulto è stupida.  
  
Mi mandava in bestia non essere ascoltato, poter dire qualcosa è già sapere che non sarebbe stato preso in considerazione non predisponeva al dialogo. Mi fà ridere che ora le parti si sono invertite: gli stessi adulti dicono le stesse cose, ma alle mie orecchie quelle frasi, che tanti anni prima risultavano incomprensibili, ora hanno un senso, stupido, ma hanno un senso.


 La mitica frase... *"quando ero giovane io..."* eviterò di usarla perché è l'unica difesa che un adulto **asincrono** ha quando si rende conto che suo figlio ha una marcia in più. Mia madre dice che quando era giovane lei non c'era questo, quello e quell'altro... Per la croncaca, quando mia madre aveva 15 anni era il 1972; non c'era internet, non c'era il cellulare, la televisione era poco diffusa, la musica si ascoltava alla radio, non ci si mandava gli sms, non si scrivevano email e ben poche persone avevano il lusso di possedere una macchina. Mi spiegate come si può paragonare un giovane del 1972 con un giovane del 2006? (non discutiamo su quale delle due annate sia la migliore, tanto i giovani d'oggi possono vivere solo questa giovinezza che è il presente).


 Ho comprato qualche bel libro di pedagogia, ma uno lo sto leggendo in particolare, ricordate che [Il bambino è competente](http://www.internetbookshop.it/ser/serdsp.asp?shop=1&c=SARTR3AR6TDBT)!  
  
   
adsense  
  


