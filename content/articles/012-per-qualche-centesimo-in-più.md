Title: Per qualche centesimo in più
Date: 2004-08-21
Category: Lettonia
Slug: per-qualche-centesimo-in-più
Author: Karim N Gorjux
Summary: Questi giornate hanno il sapore tipico dell'autunno, la mattina piove e i pomeriggi danno il benvenuto ad un sole timido, ma riposante. Non fa' caldo e non fa' freddo. Semplicemente si sta'[...]

Questi giornate hanno il sapore tipico dell'autunno, la mattina piove e i pomeriggi danno il benvenuto ad un sole timido, ma riposante. Non fa' caldo e non fa' freddo. Semplicemente si sta' bene.


 Ho visto una ragazza bionda di circa 20 anni, vestita con i tipici costumi locali (trovate gli stessi tipi di vestiti nella catena self-service lido) che suonava un particolare strumento in legno simile ad un violino. La mia ignoranza regna sovrana, non ho proprio idea di che strumento sia, ma la musica era molto piacevole. Ovviamente la signorina elemosinava qualche centesimo.. ci va' coraggio e molta voglia a rimanere a suonare in piazze mentre la gente ti scatta le foto o ti guarda incuriosita. Rispetto all'italia qui ci sono molti ragazzi giovani che suonano in kalku iela (la via piu' famosa di Riga) i piu' disparati strumenti per qualche moneta. Proprio ieri una ragazzina suonava la colonna sonora de "il padrino" con la tromba, penso che questo sintetizzi la situazione italiana a Riga. :-D

 Lo sapete qual'e' l'alimento tipico Lettone? La patata! Il primo che inizia a ridere o fa' qualche battuta maliziosa si riceve un'esercito incazzato di virus via email!!! La patata viene cucinata in circa un miliardo di modi, andate al Lido e trovere un reparto riservato alla patata... fritta, bollita, a frittella, dolce, salata, predigerita, in polvere etc... ok ok.. sto' scherzando. :-)  
Sono stato nella posta centrale, non vi dico per cosa, ma è impressionante! Per fare la coda prendi il ticket come da noi soltanto che lo distribuisce una macchinetta elettronica dove con un tasto selezione il tipo di servizio di cui avete bisogno e poi ti stampa il ticket, tutto funzionante, pulito e ordinato. E la gente rispetta la fila... roba da non credere! :)

 Ieri sera ho provato a fare gli spaghetti al pomodoro (ho preparato il sugo da zero!), sono venuti quasi perfetti se non fosse che ho sbagliato a mettere troppo sale! Sono soddisfatto come primo esperimento non è male, vedro' di migliorare la prossima volta. (ringrazio ufficialmente la mamma per avermi pazientemente spiegato il tutto via telefono a mie spese che poi sarebbero a sue.. :D)

 Bene, non ho molto da dire e infatti penso che non scriveroper qualche giorno a meno che non ci siano importanti novità... sono diventato amico di un ragazzo turco che è nato qui... mi piace viaggiare :-)

