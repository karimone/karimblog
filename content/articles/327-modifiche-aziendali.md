Title: Modifiche aziendali
Date: 2007-05-31
Category: altro
Slug: modifiche-aziendali
Author: Karim N Gorjux
Summary: A breve la mia società, di cui detengo il 50%, diventerà mia al 99%. Il bello è che l'1% rimanente apparterrà a mia moglie. In pratica è una ditta individuale. :cool_tb:

A breve la mia società, di cui detengo il 50%, diventerà mia al 99%. Il bello è che l'1% rimanente apparterrà a mia moglie. In pratica è una ditta individuale. :cool\_tb:

