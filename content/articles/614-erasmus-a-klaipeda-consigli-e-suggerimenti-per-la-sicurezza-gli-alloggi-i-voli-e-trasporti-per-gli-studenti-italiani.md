Title: Erasmus a Klaipeda: consigli e suggerimenti per la sicurezza, gli alloggi, i voli e trasporti per gli studenti italiani
Date: 2010-09-02
Category: altro
Slug: erasmus-a-klaipeda-consigli-e-suggerimenti-per-la-sicurezza-gli-alloggi-i-voli-e-trasporti-per-gli-studenti-italiani
Author: Karim N Gorjux
Summary: Tra le tante persone che mi contattano ci sono anche gli studenti italiani che vengono a Klaipeda per il programma di scambio universitario Erasmus. Il fenomeno inoltre sembra in aumento da quando[...]

[![Erasmus al computer](http://www.karimblog.net/wp-content/uploads/2010/09/ErasmusComputer.jpeg "Erasmus al computer")](http://www.karimblog.net/2010/09/02/erasmus-a-klaipeda-consigli-e-suggerimenti-per-la-sicurezza-gli-alloggi-i-voli-e-trasporti-per-gli-studenti-italiani/erasmuscomputer/)Tra le tante persone che mi contattano ci sono anche gli studenti italiani che vengono a Klaipeda per il programma di scambio universitario [Erasmus](http://ec.europa.eu/education/lifelong-learning-programme/doc80_en.htm). Il fenomeno inoltre sembra in aumento da quando sono arrivato qui, ospite dal mio amico in Erasmus, nel lontano 2004.  
Klaipeda è la terza città lituana in ordine di grandezza e l'università rispetta questo ordine di importanza venendo dopo l'università di Vilnius e quella di Kaunas. Klaipeda è inoltre la città più bella della Lituania, ma detto da me che ci vivo può sembrare poco obiettivo, perché dico questo? Perché a Klaipeda abbiamo il mare, [Palanga](http://it.wikipedia.org/wiki/Palanga) e [Nida](http://en.wikipedia.org/wiki/Nida,_Lithuania) inoltre è una città di oltre 200.000 abitanti sulla carta quindi non è troppo piccola e non è caotica come Kaunas e Vilnius. I servizi ci sono tutti e l'inverno è un poco meno freddo che negli altri paesi lituani.  


 #### Klaipeda è una città sicura? Posso stare tranquillo?

  
A me non è mai successo niente di grave, qualche fastidio ogni tando da qualche ubriaco, ma alla fine si è sempre risolto tutto. Se sei scuro da sembrare un Turco o sei nero come un africano allora puoi avere dei problemi. Sotto questo aspetto la Lituania è decisamente xenofoba e non sopporta troppo gli stranieri, le cose stanno però cambiando a Vilnius dove gli stranieri aumentano anno dopo anno.  
Al di là del colore della pelle, secondo me gli omosessuali sono le persone più a rischio sicurezza a Klaipeda. Se c'è una cosa che non viene tollerata qui in Lituania è proprio l'omosessualità, meglio non farsi riconoscere perché il pestaggio facile è all'ordine del giorno.


 #### I trasporti sono buoni? Ho bisogno dell'auto?

  
Klaipeda ha una buona rete di trasporto pubblico e per la notte ci sono i mini bus ovvero i taxi collettivi che ripercorrono le tratte dei bus (non proprio le stesse a dire il vero). Se proprio non trovi niente c'è sempre un taxi disposto a portarti dove vuoi.  
Per viaggiare in Lituania o nei paesi Baltici in generale, c'è sempre il treno o il bus. Io consiglio il Bus perché ti porta davvero dove vuoi a prezzi economici e velocemente. Il treno è comodo per andare a Vilnius senza fretta dato che da Klaipeda ci mette circa 5 ore contro le 3 di un mini bus.


 Se arrivi in aeroporto a Riga, a Vilnius o a Kaunas, non avrai nessuna difficoltà a trovare un mini bus per portarti a Klaipeda. Forse l'unica difficoltà la puoi avere a Kaunas visto che l'aeroporto è piccolino, in quel caso puoi sempre farti portare alla stazione degli autobus e prendere un bus da li.


 #### Studio il russo, ci sono problemi a parlarlo a Klaipeda?

  
Klaipeda è la città che in rapporto agli abitanti ha più russi residenti. Dato che qui c'è il porto e Kalinigrad non è molto distante, i russi sono parecchi. C'è da dire però che questa è la Lituania e qui si parla il Lituano. Sicuramente ti capiranno tutti dai 20 anni in su, ma non è detto che tutti ti vorranno capire. Se sai il russo e sai l'inglese sicuramente te la caverai, ma se hai abbastanza tempo ti consiglio di impararti un poco di lituano per assaporare al meglio la cultura locale.


 Io faccio sempre un esempio quando mi si parla del russo in Lituania: "Andresti in Israele ad imparare ed esercitare il tuo tedesco?". Il concetto è pressapoco lo stesso.


 #### Hai qualche consiglio da darmi per prendere un appartamento in affitto?

  
In soldoni ci sono due tipi di appartamento che puoi scegliere: quello sovietico e quello europeo (nuovo). Io abito in una zona nuova con appartamenti nuovi. D'inverno si sta al caldo e d'estate è fresco; il vicinato è ottimo e la zona è così tranquilla da risultare persino noiosa. Il prezzo mensile è europeo.


 Gli appartamenti sovietici sono piccoli, a volte restaurati a volte no, d'inverno schiatti dal caldo perché il riscaldamento non è autonomo e quindi ti tocca tenerlo acceso tutto il giorno (e pagare!). Il costo mensile è basso, ma ha più spese. Il vicinato poi non è sempre dei migliori quindi alle volte può essere pericoloso, se provi a cercare uno di questi appartamenti, è probabile che troverai qualcosa di buono, ma non farti spaventare dalle scale di servizio, dagli ascensori o dagli ingressi. Qui il problema è che le case vecchie non vengono restaurate e stanno, poco per poco, cadendo a pezzi.


 Se hai visto la mappa di Klaipeda, l'Università si trova a circa 1km dal centro quindi ti consiglio di scegliere il centro così ti eviti di prendere bus o minibus per andare all'università, ma se vuoi prendere qualcosa di buono senza andare troppo lontano ti consiglio la zona chiamata Misko (foresta) che è a Nord dell'università e dove si trova l'altra università di Klaipeda: [l'LCC](http://www.lcc.lt/)

 In sostanza cerca qualcosa in centro o a nord verso Palanga. Un buon sito con prezzi esposti è [OberHaus](http://www.ober-haus.lt/) mentre [MemelHaus](http://www.memelhaus.lt/) è un sito che tratta immobili solo della zona di Klaipeda, ma non è in lingua inglese.


 Evita assolutamente la zona portuale a sud. E' malfamata e molto pericolosa.


 #### Per arrivare in Lituania in aereo hai qualche consiglio?

  
[Andrea Russo](http://andrearusso1979.blogspot.com/) ha scritto un [buon articolo](http://www.karimblog.net/2009/11/13/viaggiare-in-lituania-consigli-per-voli-aerei-dallitalia-alla-lituania/) per il mio blog riguardo ai voli aerei per la Lituania in partenza dall'Italia.


 Hai altre domande? Commenta o scrivimi in privato, provvederò a tenere aggiornato questo articolo.


 Nota bene: sono stato contattato un paio di volte da studenti erasmus. Per ora sono stati solo dei contatti dove sono stato scambiato per il **centro informazioni gratuito in lingua italiana per il soggiorno a Klaipeda**. Dopo avermi contattato, fatti anche vedere! Io e [Mirko](http://www.mirkobarreca.net/) siamo a disposizione ad aiutare un compaesano che vive nella nostra città e dato che siamo gli unici italiani a Klaipeda, ci fa anche piacere un po' di compagnia.


