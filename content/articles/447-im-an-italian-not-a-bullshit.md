Title: I'm an italian, not a bullshit!
Date: 2008-04-23
Category: Lituania
Slug: im-an-italian-not-a-bullshit
Author: Karim N Gorjux
Summary: Ovvero: la grottesca situazione del popolo italiano che agli occhi del mondo civile suscita le emozioni di chi per la prima volta guarda Fantozzi. Ridere o piangere?

Ovvero: la grottesca situazione del popolo italiano che agli occhi del mondo civile suscita le emozioni di chi per la prima volta guarda [Fantozzi](http://it.wikipedia.org/wiki/Fantozzi_%28film%29). Ridere o piangere?

 Segnalo un [articolo stupendo](http://marcoinhouston.blogspot.com/2008/04/good-luck-my-italy.html) redatto dal fratello bello del [Baltic Man](http://www.sandro.madeinblog.net).  
Da leggere e rileggere perché riesce a riassumere in poche righe le domande che ricevo quotidianamente da Rita e riesce persino a far assaporare in chi legge lo stupore che Rita prova ad ogni mia risposta; Rita le notizie sulla politica italiana le riceve dai siti lituani, a volte ne sa più lei di me.


 Tutto ciò mi ricorda la Lituania, mentre qualcuno scendeva in campo, abbandonava definitivamente la lettura del [Pravda](http://it.wikipedia.org/wiki/Pravda).


