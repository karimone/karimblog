Title: e se karimblog.net diventasse anche un libro?
Date: 2010-04-01
Category: altro
Slug: e-se-karimblognet-diventasse-anche-un-libro
Author: Karim N Gorjux
Summary: In questi giorni sto pensando di riprendere parte del mio blog, revisionarlo e farlo diventare un libro. Non ho esperienza di editoria e sarebbe qualcosa di nuovo e affascinante, ma dato che[...]

[![](http://farm3.static.flickr.com/2325/1501497507_29502aea4f_m.jpg "Reinventati")](http://www.flickr.com/photos/m-c/1501497507/)In questi giorni sto pensando di riprendere parte del mio blog, revisionarlo e farlo diventare un libro. Non ho esperienza di editoria e sarebbe qualcosa di nuovo e affascinante, ma dato che gli articoli presenti su questo blog coprono un periodo di tempo di 6 anni spaziando dai temi più vari, penso di avere abbastanza materiale per creare un libro interessante.


 Che cosa ne pensi?  
Come immagineresti il libro tratto da questo blog?  
Come lo imposteresti?  
Hai dei consigli da darmi?  
Puoi aiutarmi o conosci qualcuno che possa aiutarmi?

