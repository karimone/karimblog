Title: Cinque consigli per superare le difficoltà nella vostra relazione con una donna lituana (o del nord europa in generale)
Date: 2008-12-08
Category: Lituania
Slug: cinque-consigli-per-superare-le-difficoltà-nella-vostra-relazione-con-una-donna-lituana-o-del-nord-europa-in-generale
Author: Karim N Gorjux
Summary: Negli ultimi mesi sono stato contattato diverse volte su skype o via email per consigli riguardo alle relazioni tra uomini italiani e lituane. Non so bene cosa dire in a tutti voi[...]

Negli ultimi mesi sono stato contattato diverse volte su skype o via email per consigli riguardo alle relazioni tra uomini italiani e lituane. Non so bene cosa dire in a tutti voi perché ognuno di voi ha una storia diversa da raccontare e le mie esperienze in tal proposito sono differenti dalla maggior parte delle vostre.  
  
 Intanto una donna lituana, russa, lettone, polacca o qualsiasi altra nazione di questo nord d'Europa si contraddistingue per i tratti somatici: pelle chiara, capelli biondi, occhi azzurri, sorriso smagliante, ma alla fine della fiera sempre donna è. Qualsiasi donna trattata da principessa si cala nella parte senza troppi indugi e noi italiani siamo dei fessi di prim'ordine a cascare davanti a due occhi azzurri.


   
2. **Oltre le apparenze**. Le donne di questa parte del mondo spende molto tempo e denaro al loro apparire: capelli ben curati, trucco, tacchi alti, vestiti vistosi, unghie finte, sopracciglia... Ogni giorno è una sfilata di moda, voi vi incantate e il cervello si scollega dal resto del corpo. Tornate a terra e guardate lei per quello che è realmente (una donna tra le tante?).



 - **Oltre le parole**. Se in una discussione partite con il presupposto che ciò che dice lei sia inconfutabile e quindi rappresenta ciò che lei davvero pensa allora avete perso in partenza. Le nordiche smascherano il vero problema per poi finire litigare per stupide cose, forse è un modo per essere al centro dell'attenzione, ma è uno spreco di energie che vi costerà caro. Andate oltre le parole e cercate di arrivare alla radice del vero problema (difficile!) non date troppo peso a ciò che dice, ma a come si comporta. Consiglio: evitate di smontare ogni sua affermazione è come un depistaggio senza fine, vi porta solo lontano dalla verità.



 - **Respirate la loro cultura**. Per capirla a fondo bisogna dapprima capire la cultura che sta dietro alla persona. Sapete che influenza ha la famiglia? La religione? Lo stato sociale? Avete cercato di sapere qualcosa del paese di origine e cosa ha dovuto subire negli anni? Assaporate questi posti e capirete molto di più su di lei


 - **Imparate la loro lingua** (o insegnatele la vostra). Non ha assolutamente senso parlare una lingua di transizione come l'inglese per comunicare. Può andare bene all'inizio, ma poi è limitativo e snervante, finirete di perdere molto della vostra relazione. Avete due possibilità: imparare la sua lingua o insegnarle la vostra. Se decidete di imparare la sua lingua non contate troppo sul suo aiuto, non arrabbiatevi e vi risparmierete dei mal di pancia; molto probabilmente non vi parlerà mai nella sua lingua fino a quando non avrete bisogno del suo aiuto. Personalmente le ho insegnato l'italiano, ci ho messo più di 1 anno, ma con tanta pazienza ho iniziato a parlarle tricolore e da allora non ho più smesso.



 - Siate gentili, amabili e dolci, ma **non svalutatevi**. Siate uomini, non venditori di voi stessi. Essere dolci e gentili fa parte del carattere latino, ma le donne di queste parti sono abituate all'uomo che decide e che impone. E' bello essere gentili e dolci, piace, ma esserlo sempre ne fa perdere il valore. Ogni tanto un bel pugno sul tavolo e un po' di carattere serve a far capire che voi siete l'uomo. Non è un discorso maschilista e non sto parlando di picchiare o diventare rozzi o maleducati. Se avete capito cosa intendo siete sulla buona strada
  


