Title: Problemi?
Date: 2007-04-01
Category: altro
Slug: problemi
Author: Karim N Gorjux
Summary: Tutti noi ne abbiamo, ne avremo sempre. Non esiste una vita senza problemi.

Tutti noi ne abbiamo, ne avremo sempre. Non esiste una vita senza problemi.


 Secondo me l'intelligenza è un metro per misurare la nostra attitudine verso i problemi della vita.


