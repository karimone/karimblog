Title: Un punto di vista obbiettivo
Date: 2006-08-20
Category: Lituania
Slug: un-punto-di-vista-obbiettivo
Author: Karim N Gorjux
Summary: Beh, sono molto contento che il mio post "Caro lituano" abbia avuto così tanto successo da essere criticato e quotato allo stesso tempo. Mi fa piacere di essere opinato perché questo rende[...]

Beh, sono molto contento che il mio post "[Caro lituano](http://www.karimblog.net/2006/08/13/caro-lituano)" abbia avuto così tanto successo da essere criticato e quotato allo stesso tempo. Mi fa piacere di essere opinato perché questo rende i miei post degni di attenzione e quindi vi ringrazio. Ora però vorrei scrivere qualcosetta sia sul popolo lituano che su di noi, ma per fare ciò ho bisogno di una piccola premessa.


   
Per poter criticare nel bene o nel male il paese in cui vivo, **ho bisogno di fare un paragone**. Volente o nolente posso solo farlo con l'unico posto in cui ho vissuto per tanto tempo: la provincia di Cuneo. Vorrei sottolineare che fare un paragone Lituania / realtà di Cuneo è molto differente dal fare un paragone con altre regioni o città d'Italia. Qui penso che siamo tutti d'accordo, l'Italia è bella, è grande ed è popolata da persone pittoresche accomunati da una sola bandiera, ma divisi da differenti radici, cultura e persino dialetto. Io sono mezzo francese e mezzo calabro e la differenza la capisco **molto bene, **non entro nel merito, ma penso che se qualcuno dei miei lettori ha origini del sud capisce bene cosa intendo.


 Molto però dipende da come siamo noi, di come ci proponiamo con la gente e di cosa vogliamo trovare. A Cuneo le persone sono cordiali, ma non tutte. I cuneesi come tutte le persone del pianeta sono pieni di difetti. A Cuneo non ci sono bei locali per divertirsi la sera e la provincia è parecchio noiosa, ma le montagne, i paesini, la gente, rendono la provincia di Cuneo piena di risorse.


 La Lituania ha molto da offrire ai turisti e ha anche molto da offrire a chi vuole vivere in questo paese, ma ovviamente un turista vede l'aspetto migliore della Lituania, se si può scegliere si viene da queste parti **d'estate**, si va in giro a vedere tutto ciò che ha da offrire questo paese e questo popolo e ci si diverte pure. E' un modo di vedere la Lituania che spero possa far apprezzare il più possibile questo paese agli stranieri, ma ahimè questa non è la vera Lituania. Non posso permettermi di generalizzare, Giuseppe, mi ha fatto giustamente notare che per conoscere una cultura o un popolo ci puoi impiegare 10 anni come tutta la vita, quindi **non posso generalizzare**, ma se non generalizzo diventa molto complicato scrivere qualsiasi post o qualsiasi commento su Klaipeda e i suoi abitanti, dovete sempre tenere a mente che quando dico i lituani sono... mi limito a descrivere una realtà che ho conosciuto io.


 Perché ho scritto quel "Caro Lituano" in una sera qualunque di questo fresco Agosto Klaipediano? Semplicemente perché mi era successo un fatto **strano**. La faccio breve: mio cognato perde i documenti in un negozio, gli cadono per terra mentre paga alla cassa. Un tizio li raccoglie, esce dal negozio e va a cercare mio cognato. <<Se vuoi riavere i documenti, comprami 1 litro di vodka!>>, mi cognato lo manda a cagare e gli risponde che se non gli da i documenti, chiama la polizia. Il tizio di risposta, gli butta i documenti dietro una siepe e lo manda a cagare. Quando vedo certe cose, mi sento come un pesce fuor d'acqua, mi vengono alla mente aneddoti simili dalle mie parti che si finiva a parlare del più e del meno ringraziando con un caffè al bar. E' l'ostilità che caratterizza la **maggior **parte della gente lituana che mi ferisce.


 Dal canto mio penso di fare un errore di base, non posso pretendere di ritrovare qui quello che ho lasciato a casa e non posso nemmeno permettermi di criticare troppo cosa mi viene offerto. Il mio blog non è un quotidiano, non sono obbligato a essere obiettivo, ma **non voglio screditare questo paese quando non lo merita.** L'inverno è duro e deprimente, il freddo, il buio, la povertà sono tutte attenuanti che spezzano una lancia in favore dei lituani, le donne sono più socievoli e con questo non voglio dire che sono **battone** per carità, ci sono anche quelle, ma sappiamo bene che le battone sono presenti in tutto il mondo. Leggendo un mio post tenete sempre a mente che io mi limito a parlare di una realtà lituana di 200.000 abitanti chiamata Klaipeda che non rispecchia tutta la Lituania, ma può aiutarmi a dare un'idea di questo paese. (Dal mio punto di vista).


 Io qui non lavoro, sono in attesa di un bambino (la scadenza è domani) e passo la maggior parte del mio tempo con mia moglie. La mia vita sociale è data principalmente dalla palestra che frequento in centro Klaipeda. Socializzare con la gente della palestra non è difficilissimo, il difficile è frequentarli fuori dalla palestra. La lingua è un ostacolo, parlare inglese permette di sbrogliarsi da quasi tutte le situazioni, ma non permette di integrarsi in questa società, ho anche notato che i lituani a volte sembrano scorbutici, ma in realtà sono solamente timidi. Li saluti e rispondono quasi sottovoce, come se fosse qualcosa di anormale essere salutati, se poi provo a cacciargli le parole di bocca allora iniziano ad essere più cordiali e socievoli.


 Se rileggete i commenti al mio post c'è chi mi dice che ho torto e c'è invece chi mi quota completamente sul [suo blog](http://www.lucianogiustini.org/blog/archives/2006/08/lamento_dellitaliano_in_lituania.shtml). Io penso che abbiate tutti ragione, ma sbagliate nel fare un paragone come risposta ad una mia critica. Se dico che i lituani non sono gentili quando provo a parlare la loro lingua, dico una mezza verità perché quando ho scritto il post avevo accumulato una serie di feedback negativi che mi hanno esasperato fino a farmi scrivere il famoso ["Caro lituano"](http://www.karimblog.net/2006/08/13/caro-lituano). In realtà ho avuto dei sorrisi e degli incoraggiamenti mentre provavo a parlare questa antichissima e ostica lingua. **Incoraggiamenti sia da donne che da uomini.**

 In Lituania come in tanti altri paesi ci sono cose che vanno bene e che non vanno bene, ma da italiano fatico ancora ad abituarmi a vedere tanti ubriachi a qualsiasi ora del giorno o sporcizia da tutte le parti, anche in mezzo alle spiagge o ai boschi. **Questa è una verità**, l'ho visto con i miei occhi e spero che la prossima generazione di lituani si comporterà in modo diverso. Ora potete commentare elencando i difetti degli italiani come se questo sminuisse i difetti dei lituani, ma non cambia nulla. Al momento in Lituania c'è troppa gente che beve e troppa gente che sporca; in Italia ci sono troppi mammoni e le banche chiudono sempre all'una. Tutte cose vere, ma se parlo della Lituania, perché rispondermi con un paragone all'Italia? Con ragionamenti del genere diventa tutto ammissibile.  
 technorati tags start tags: [italia](http://www.technorati.com/tag/italia), [abitare](http://www.technorati.com/tag/abitare), [klaipeda](http://www.technorati.com/tag/klaipeda), [lituani](http://www.technorati.com/tag/lituani), [lituania](http://www.technorati.com/tag/lituania), [critica](http://www.technorati.com/tag/critica)



  technorati tags end 

