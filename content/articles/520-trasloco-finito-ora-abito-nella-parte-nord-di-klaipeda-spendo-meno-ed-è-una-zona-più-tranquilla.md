Title: Trasloco finito: ora abito nella parte nord di Klaipeda, spendo meno ed è una zona più tranquilla
Date: 2009-02-06
Category: Lituania
Slug: trasloco-finito-ora-abito-nella-parte-nord-di-klaipeda-spendo-meno-ed-è-una-zona-più-tranquilla
Author: Karim N Gorjux
Summary: Ecco fatto. Mi sono trasferito. Ora abito a qualche kilometro dal centro, spendo meno di affitto e ho persino un appartamento più bello. Giusto oggi ho riavuto internet dopo circa 12 giorni[...]

Ecco fatto. Mi sono trasferito. Ora abito a qualche kilometro dal centro, spendo meno di affitto e ho persino un appartamento più bello. Giusto oggi ho riavuto internet dopo circa 12 giorni di attesa, tutto funziona e con il tempo metterò a posto l'appartamento come piace a noi.


 Perché mi sono spostato? Semplice, i prezzi degli affitti sono scesi, qui a Klaipeda, del 30%, ma se qualcuno si accontenta può prendersi un appartamento discreto pagando giusto le spese. La crisi economica sta arrivando e il mio ex padrone di casa voleva farla pagare solo a me.


 Anche i prezzi dei terreni e degli immobili stanno ricevendo la giusta ridimensionata. Dopo il boom del 2005 le cose sembrano tornare normali, disoccupazione e inflazione al seguito.


 Una curiosità. Dove abito ora, ci sono tre grossi cantieri: il primo è un palazzo di una ventina di piani, dirimpetto alla statale, di cui la costruzione procede a gonfie vele. Il secondo e il terzo sono all'estrema opposta del primo, alla fine del quartiere. Da quando bazzico questo posto, non ho mai visto nessuno lavorarci; le struttura, compreso il tetto, c'è. Le finestre pure, ma è tutto fermo. Colpa della crisi?

