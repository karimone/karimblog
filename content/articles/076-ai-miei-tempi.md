Title: Ai miei tempi...
Date: 2005-06-13
Category: altro
Slug: ai-miei-tempi
Author: Karim N Gorjux
Summary: Una volta si diceva in piemontese, ancora prima in latino..... "ai miei tempi...". Una frase che nasconde un significato intrinseco cammuffato solo dall'intonazione.Ve lo riassumo:"Figliolo, io sono un dinosauro travestito da genitore,[...]

Una volta si diceva in piemontese, ancora prima in latino..... "*ai miei tempi...*". Una frase che nasconde un significato intrinseco cammuffato solo dall'intonazione.  
  
Ve lo riassumo:  

> "Figliolo, io sono un dinosauro travestito da genitore, non faccio parte di questo mondo popolato da mp3, house music, zip unzip, chat, sms, squilli, download... Sono un anacronismo. Il mondo come lo conoscevo io e' ormai morto e io sono qui come un viaggiatore nel tempo che ha saltato la metamorfosi di questi anni. Ora i giovani navigano, cliccano, scaricano, comunicano alla velocita' di 160 caratteri. Ai miei tempi tutte queste cose non c'erano. Alla tua eta' avevo meno dell'1% delle possibilita' di informazione che hai tu ora. Sono indietro e sono cosi' ignorante da sentirmi importante quando dico... Ai miei tempi.."  
>   
La penultima generazione (quella dei nostri genitori) e' la piu' comica che abbia mai visto: **l'ultima a prendere i calci in culo dai genitori e la prima a prenderli dai figli**. :lol\_tb:

