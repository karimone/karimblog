Title: Visite dalla Lituania. Stesse opinioni su posti diversi
Date: 2011-12-30
Category: Lituania
Slug: visite-dalla-lituania-stesse-opinioni-su-posti-diversi
Author: Karim N Gorjux
Summary: In questi giorni un'amica di Rita è venuta da noi per una piccola vacanza di una settimana. Purtroppo non abbiamo potuto farle vedere l'Italia dei luoghi più conosciuti, ma ci siamo limitati[...]

In questi giorni un'amica di Rita è venuta da noi per una piccola vacanza di una settimana. Purtroppo non abbiamo potuto farle vedere l'Italia dei luoghi più conosciuti, ma ci siamo limitati a mostrarle i posti che viviamo tutti i giorni.


 **Passare da una realtà di provincia lituana ad una realtà di provincia del nord d'Italia** è stato per lei suggestivo se non addirittura "incredibile". E' vero che vivere l'Italia turistica non è come vivere in Italia, ma è curioso come i confronti siano gli stessi e dagli stessi risultati. Ho sentito persone dire le stesse cose di questa amica di Rita riferito alla Lituania nei confronti dell'Italia.  
  
Posti stupendi, gente sorridente, locali meravigliosi, cibo ottimo. Anche Rita dice più o meno le stesse cose, anche un ragazzo di Milano che incontravo ogni tanto a Klaipeda diceva le stesse cose dei lituani ogni volta che lasciava l'Italia per venire in Lituania.


 Chi ha ragione, chi ha torto? 

 **Non ha importanza.  
**  
Ci sono stati momenti che a me la Lituania andava bene e altri che andava male, ma è solo un discorso di percezione per poter fare un discorso costruttivo bisogna andare oltre la mente ed è proprio il tipo di argomenti che scrivo con altre due persone sul sito [oltrelamente.net](http://www.oltrelamente.net/).


 Se vuoi sapere davvero chi ha ragione o chi ha torto, purtroppo non esiste una risposta. Puoi dire che hanno torto entrambi o ragione entrambi, ma sprechi solo il tuo tempo. Assaporare una cultura, viverlo per un periodo lungo o breve dovrebbe essere fatto al di là di ogni giudizio, obbietterai che è impossibile, che il confronto è obbligato. E' vero, il confronto lo puoi fare, puoi sentire le differenze, ma nel momento in cui giudichi non stai giudicando ciò che vivi, ma in che cosa ti identifichi o non ti identifichi.


 **Ovviamente è una teoria opinabile.**

