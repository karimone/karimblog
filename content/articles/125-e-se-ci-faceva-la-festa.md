Title: E se ci faceva la festa?
Date: 2005-11-27
Category: Lituania
Slug: e-se-ci-faceva-la-festa
Author: Karim N Gorjux
Summary: Sabato sera io e Rita siamo usciti tardi, ci aspettavano al Memelis per le 22, ma siamo arrivati con 20 minuti di ritardo. Un freddo animale! Il solito Kurpiai era pieno affollato,[...]

Sabato sera io e Rita siamo usciti tardi, ci aspettavano al Memelis per le 22, ma siamo arrivati con 20 minuti di ritardo. Un freddo animale! Il solito Kurpiai era pieno affollato, allora io Rita, Rasa, e una sua amica di cui non ricordo il nome siamo andati al Memelis.  
  
Il Memelis è il famoso locale di cui ho parlato tempo addietro (e dove ho conosciuto Rita), in questo periodo sta cambiando a mio parere in peggio. Il piano terra e il primo piano sono in ristrutturazione, quindi siamo stati obbligati ad andare al secondo piano riuscendo ad evitare il **controllo dei 21 anni**. Porca miseria! Rita deve simulare il famoso *"Ho dimenticato la carta d'identità"* io invece, porca miseria, passo sempre per *idoneo*, ma ho sta faccia da vecchiaccio?? :-)

 Dato che Sirchia qui non è conosciuto, la sala era piena di fumo, alcuni tavoli erano liberi, ma non spiegatemi il perché per avere il posto bisognava pagare. Il prezzo variava da 50 a 100 litas ovvero 15 o 30€. Vi rendete conto che ladrata insulsa? Io mi sarei mangiato volentieri qualcosa dato che non avevo ancora cenato e Rita aveva una fame boia. Enrico che ci aveva raggiunti dopo consiglia di andare al *Black Cat Pub 2* che è il classico British pub e si trova in faccia al Memelis.


 Salutiamo Rasa e la sua amica e andiamo al pub. Entriamo e ci sediamo a due piccoli tavolini in legno avvicinati tra loro. Rita si siede sul lato "divano" io invece mi siedo dal lato sedie-di-legno-spacca-schiena ed Enrico si siede di fianco a me. Dopo aver ordinato ecco che arriva il problema. Un tipico nordico 35enne, occhi azzurri, 185cm, castano, mascelloso e guarda un po' anche ben piantato si avvicina al tavolo e accovacciandosi si svacca di fianco a me. Mi guarda e spara qualche cazzata in tipico accento *ubriacone*, Rita mi dice di non rispondere, Enrico è ammutolito.


 Che si fà ora? Se non gli dico niente si incazza, se gli dico qualcosa "capisce" che sono uno straniero e si incazza ancora di più. Gli dico che non capisco, lui sorride e si mette vicino a Rita ed inizia a fumare. La situazione è abbastanza di merda perché non è come essere in Italia che arriva Cristian, dice "beviamo tutti! Festazza! Cantiamo le canzoni dell'ostu!!" e si ride tutti insieme. Questo tizio è russo, e i russi ubriachi sono incazzati. Inizia a parlare in russo, io qualcosa lo capisco, ma Enrico che è praticamente lo Zingarelli su due gambe inizia a spiegargli che non è piacevole la sigaretta e non è gradita la sua compagnia. Il tipo inizia ad incazzarsi, comincia ad insultare me ed Enrico e la butta sulla politica, fanculo l'unione europea, la nato, io sono russo vi taglio la gola (accompagnato da eloquente gesto della mano sulla carotide.


 Ringraziando il cielo sono arrivate le cameriere e hanno accompagnato il tizio al suo tavolo e gli hanno chiesto di pagare e andarsene minacciandolo con una chiamata al 911. Brutta, bruttissima situazione. Sembra niente di ché scritta in queste poche righe, ma qui la gente se la prende per niente e si rischia di fare una discussione con le bottiglie di birra rotte. Gli errori di grammatica si pagano caro.


 Sorvoliamo sull'accaduto e consumiamo la cena, decidiamo di andare all'Honolulu dove io e Rita incontriamo la bella e brava Oksana e in più facciamo anche qualche sculettata sulla disco.. serata ordinaria...  
  
[![Oksana e Rita](http://www.kmen.org/wp-content/upload//Oksana-Rita-tm.jpg "Oksana e Rita")](http://www.kmen.org/wp-content/upload//Oksana-Rita.jpg)  
  
  
  
 adsense  
  


