Title: Emigrare e' molto difficile ed emigrare puo' essere un fallimento: hai un piano B?
Date: 2017-06-12
Category: Australia
Slug: emigrare-e-molto-difficile-ed-emigrare-puo-essere-un-fallimento-hai-un-piano-b
Author: Karim N Gorjux
Summary: Qualche giorno fa, mi hanno contattato per farmi una piccola intervista. Nessun problema, scritto e mandato l'articolo. Un mese fa, invece, mi ha contattato un sito che in cambio di visibilita' mi[...]

Qualche giorno fa, mi hanno contattato per farmi una piccola intervista. Nessun problema, scritto e mandato l'articolo. Un mese fa, invece, mi ha contattato un sito che in cambio di visibilita' mi chiedeva di scrivere articoli originali per il loro portale. In pratica mi volevano come creatore di contenuti gratuito ed e' il modo di fare italiano che odio di piu'. Non pagare le competenze altrui per un motivo che ancora non mi e' chiaro. 

 L'intervista, linkata al fondo di questo articolo, tocca argomenti abbastanza comuni tra gli expat: le difficolta' nel trasferimento, cosa ti manca, cosa ci hai guadagnato, e cosi' via. Ci sono alcuni aspetti interessanti che pero' non vengono mai affrontati abbastanza bene nei vari blog e siti. **Emigrare e' molto difficile ed emigrare puo' essere un fallimento**.


  Io sono stato parecchi anni in Lituania, sono ritornato in Italia da "ibrido" ed e' stato anche quello emigrare, sono stato in Svezia per qualche giorno e alla fine sono arrivato qui in Australia nel 2014. E' stata dura, ho sbagliato tante volte in questi anni facendo tesoro dei miei errori. L'Australia e' stato, fino ad ora, un successo. Purtroppo pero', tante persone sono spinte dalla disperazione e affrontano l'emigrazione qui in Australia troppo alla leggera.


 Qualche tempo fa mi scrisse una signora su facebook, emigrata a circa 55 anni con il marito, si erano fidati di una persona che li aveva convinti a venire qui perche' tanto il marito sa fare le pizze e **qui nessuno capisce di pizze**. Sarebbe stata una passeggiata anche se non sapevano l'inglese che avrebbero imparato strada facendo. Dopo essersi spesi quasi tutti i soldi, la coppia di coniugi e' stata costretta a fare i lavapiatti e organizzare partenze separate per riuscire a ritornare in Italia. Immagina con che morale si puo' tornare in Italia a quell'eta' e dopo essere stati qui.


 Il caso piu' eclatante di cui sono stato testimone e' di un signore separato di 40 anni che si e' trasferito qui fidandosi di un'infatuazione presa su internet. Ha dato troppe cose per scontato e non e' stato attento ai dettagli riguardo allo stipendio e al lavoro e purtroppo chi ci va' di mezzo e' la bambina di 10 anni che si e' portato appresso. Ovviamente l'infatuazione si e' rilevata un calesse, il castello sulla sabbia e' crollato e tutti i problemi dell'expat si sono presentati alla porta a chiedere il conto: dove metto la bambina, i soldi non bastano, trovare alloggio e cosi' via.


 Per emigrare ci va' cognizione di causa. Bisogna fare attenzione ad ogni dettaglio e soprattutto **avere un piano B sempre pronto**. Bisogna soprattutto essere obiettivi! Come dico sempre ad un mio amico, "*tu hai i numeri e sei venuto qui con il [visto 189](https://www.border.gov.au/Trav/Visa-1/189-). Io sono venuto qui con il passaporto*". Si perche' anche se in questi ultimi tre anni ho sputato sangue al lavoro per fare un minimo di carriera, se non avessi avuto il passaporto australiano, non sarei mai riuscito ad entrare. Questo e' essere obiettivi. Venire a vivere qui in Australia puo' semplicemente essere impossibile per qualcuno.


 Nonostante avessi il passaporto, ho perso mesi per organizzare tutto cio' che mi serviva a per venire qui e nonostante questo, ho avuto fortuna e sono stato aiutato da altri italiani a sistemarmi. Ci sono davvero tante cose che devono essere considerate e valutate, ma la parte piu' difficile, secondo me, e' quella di essere qui da solo. Si, non tutti se ne rendono conto, ma **quando sei qui, sei davvero da solo**. Non ci sono parenti o amici da chiamare per un aiuto, certo, te li fai, ma avere dei genitori o degli amici di quelli che hai dall'infanzia e' tutt'altra cosa. Qui sei per conto tuo, sei in macchina che guidi di notte in mezzo al nulla australiano e pensi che sei dall'altra parte del mondo, piu' vicino all'Antartide che a qulla che consideravi casa tua e che sei per conto tuo. Questo e' difficile da gestire, soprattutto i primi anni.


  Nell'intervista ho scritto:

 "*Personalmente essere un expat significa avere coraggio facendo una scelta che non molti sono in grado di fare con consapevolezza. Significa mettersi in gioco, per cercare di migliorare la propria situazione al di la' del risultato. Nel momento in cui atteri in un nuovo posto dove tutto e' diverso da cio' a cui eri abituato, non e' solo la tua situazione economica che cambia, ma anche qualcosa dentro di te. Da quel momento in poi stai diventando un' "ibrido".*"

 Ed e' proprio cosi', non ti accontenti, vai via anche se tutti ti dicono di accontentarti, ti metti in gioco, sbagli, correggi il tiro, se e' il caso ti sposti di nuovo, ma dal momento che te ne sei andato hai espanso i tuoi confini culturali e mentali. Non appartieni piu' ad una regione o ad un paese, ti ridefinisci in meglio diventando un "apolide culturale" in grado di assorbire il meglio che il mondo possa offrire.


 Link: [intervista sul blog di ProntoPro](https://www.prontopro.it/blog/quanto-coraggio-ci-vuole-per-mollare-tutto-ed-iniziare-una-nuova-vita-allestero/)

