Title: Integrazione: come ho iniziato a crearmi una vita sociale in Lituania
Date: 2009-02-16
Category: Lituania
Slug: integrazione-come-ho-iniziato-a-crearmi-una-vita-sociale-in-lituania
Author: Karim N Gorjux
Summary: Ieri sera nevicava, ma non faceva freddo. Incurante del tempo ostile sono uscito e ho preso il minibus per il centro. Destinazione: Smukle Miesto (Pub della città)

Ieri sera nevicava, ma non faceva freddo. Incurante del tempo ostile sono uscito e ho preso il minibus per il centro. Destinazione: [Smukle Miesto](http://www.miestosmukle.lt/) (Pub della città)

 Ormai la mia vita sociale si concentra, per la maggior parte del tempo, in quel piccolo pub nella città vecchia frequentato da giovani ragazzi universitari. Il motivo di tanta assiduità non è ne il bere e nemmeno, come qualche malizioso può pensare, qualche bella bionda, ma sono **due semplici calciobalilla.**  
  
Non avrei mai e poi mai creduto di trovare un calciobalilla in Lituania, eppure i lituani hanno da poco iniziato ad apprezzare questo gioco tanto da creare [un'organizzazione a livello nazionale](http://www.foosball.lt) e ogni Lunedì organizzare tornei, a Klaipeda e in altre città, dove partecipano sia ragazze che ragazzi.


 Lo stile di gioco italiano l'abbiamo importato con molto orgoglio: gioco veloce, parolacce a non finire ed emotività agonistica. I lituani hanno un gioco più lento, qualche parolaccia, **pochissima emotività agonistica** e soprattutto tante regole. La palla deve essere messa al centro e poi toccata tre volte, se la palla è in una zona morta allora deve essere battuta in un certo modo. Il tutto a danno della velocità che, a mio parere, è il vero divertimento di questo gioco.


 In questo lungo inverno il calciobalilla è diventata una distrazione importante e lo "Smukle" è diventato un luogo di ritrovo, **la porta d'ingresso all'integrazione** dove posso esercitare la lingua lituana e frequentare dei lituani senza sentirmi uno straniero.


 Se sei un italiano che vive o vuole venire a vivere in Lituania (o in qualsiasi altro posto simile) è molto importante che non limiti la tua giornata alla solita routine "casa - lavoro - amici italiani" **altrimenti non sarai mai integrato e troverai davvero lungo vivere qui**.


 Una curiosità: in tutta la Lituania i calciobalilla sono 16 e sono tutti di tipo professionale ovvero quelli usati nei tornei internazionali e che costano all'incirca 1600€ (made in Italy).  
  
[![Final Match](http://farm4.static.flickr.com/3172/3284351538_d726cfc031.jpg)](http://www.flickr.com/photos/kmen-org/3284351538/ "Final Match di karimblog, su Flickr")  


