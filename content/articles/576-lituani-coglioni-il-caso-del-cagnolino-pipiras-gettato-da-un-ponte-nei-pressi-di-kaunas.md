Title: Lituani coglioni: Il caso del cagnolino Pipiras gettato da un ponte nei pressi di Kaunas
Date: 2009-11-27
Category: Lituania
Slug: lituani-coglioni-il-caso-del-cagnolino-pipiras-gettato-da-un-ponte-nei-pressi-di-kaunas
Author: Karim N Gorjux
Summary: Un coglione lituano è il tipico ubriaco dai capelli rasati che non ha idea della ragione per cui è al mondo. Il coglione lituano è "qualcuno" solamente quando si muove in branco,[...]

[![](http://farm3.static.flickr.com/2634/4143260478_06af81356e_m.jpg "Il cane pensa...")](http://farm3.static.flickr.com/2634/4143260478_a4402ce304_o.png)Un coglione lituano è il tipico ubriaco dai capelli rasati che non ha idea della ragione per cui è al mondo. Il coglione lituano è "qualcuno" solamente quando si muove in branco, altrimenti, preso da solo, è un pezzo di merda qualsiasi appartenente al più infimo grado della scala sociale. E' vigliacco, ignorante, prepotente, alcolizzato, sporco e pericoloso soprattutto se spalleggiato dagli amici e dal tasso alcolico.


 Ultimamente il coglione lituano si è fatto conoscere in tutto il mondo per l'episodio di Pipiras (Pepe), un cane che ha avuto la sfortuna di essere passato sotto le grinfie di un coglione come Svajūnė Beniuk, di anni 22, tipico alcolizzato e delinquente della campagna lituana. Svajūnė significa "il sognatore", ma a discapito del nome, il video del lancio di Pipiras da un ponte è un vero e proprio incubo partorito dai deliri di un pazzo.  
  
**Questi tipi di lituani sono molto pericolosi**, ne vedo spesso per le vie di Klaipeda e li evito sempre cambiando strada e senza farmi riconoscere come "diverso". Ma cosa fa più paura non è tanto l'ignoranza, ma la forza del branco unita all'alcool. Guardate il video, se ci riuscite, e noterete che gli elementi della follia ci sono tutti, sono in gruppo e sono ubriachi.


 "Vediamo se i cani riescono a volare"... ed ecco Pipiras che si fa un volo dal ponte.


 Questo episodio ha dato modo al mondo di conoscere l'aggettivo che accomuna i paesi baltici con i paesi stereotipati dell'est: l'alcolismo.  
**  
La Lituania è un bellissimo paese, abitato da persone stupende che non fanno notizia, ma la povertà, la carenza di possibilità, la mancanza di assistenza sociale e l'alcolismo sono delle piage che non si possono far finta di non vedere.**

   
  
  
  
  
Pipiras è morto il 22 Novembre a causa delle gravi lesioni interne.  
  


