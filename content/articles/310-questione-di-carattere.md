Title: Questione di carattere
Date: 2007-04-03
Category: altro
Slug: questione-di-carattere
Author: Karim N Gorjux
Summary: Per affrontare certe situazioni o si è fatti della pasta giusta o e meglio lasciare stare. Forse sono fatto della pasta giusta, forse no e solo il tempo saprà darmi una risposta[...]

Per affrontare certe situazioni o si è fatti della pasta giusta o e meglio lasciare stare. Forse sono fatto della pasta giusta, forse no e solo il tempo saprà darmi una risposta precisa...


 Due cose mi piacciono del mio carattere: **l'estroversione e l'oculatezza**.


 La mia estroversione è un'arma (a doppio taglio) che sono fiero di possedere. Riuscire a sorridere nelle situazioni più di cacca o trovare un motivo sempre valido per non buttarmi dal piano terra, ovvero dove abito ora, è una caratteristica preziosa del mio carattere che mi ha aiutato tante, tantissime volte.


 L'oculatezza invece è ancora più fantastica della estroversione, non sempre però riesco a mettere in atto questa virtù donatami dall'esperienza. **Essere oculati non è semplice**, a volte non mi riesce, ma più invecchio e più rifletto prima di parlare.  
Sabato sera scorso, sono stato accusato a ragione o a torto, ma non è questa la sede per discuterne e ho preferito non dire nulla per **rimandare tutto al momento opportuno** ovvero quando i bicchierini di troppo saranno solo un lontano ricordo.


