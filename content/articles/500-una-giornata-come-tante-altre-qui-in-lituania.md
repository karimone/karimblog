Title: Una giornata come tante altre qui in Lituania
Date: 2008-10-30
Category: Lituania
Slug: una-giornata-come-tante-altre-qui-in-lituania
Author: Karim N Gorjux
Summary: Sveglia alle 7.

Sveglia alle 7.


 Rita porta Greta all'asilo entro le 8 così oltre ad arrivare in tempo all'università(avrebbe voluto studiare in direzione di un master in design ma data la situazione ha scelto cose piú pratiche) riesce a far si che la nostra piccolina mangi colazione con tutti i suoi amichetti. Giusto per informare i parenti, l'integrazione è andata più che bene, Greta mangia, dorme e gioca all'asilo senza piangere o fare capricci vari.  
  
Questa mattina ho lavorato al mio sito, quello professionale che ora, lontano da preoccupazioni e fastidi vari ha finalmente una sua identità. Il mio sito, oltre che vendere le mie consulenze sarà il catalizzatore di tutti gli articoli di carattere informatico che mi passano per la testa e che non ho assolutamente voglia di mischiare con la Lituania.


 Nel primo pomeriggio ho lavorato ad altre cose top secret e ho fatto *esplodere qualche altro ponte* con l'Italia (figurativo). Alle 16 ero in centro Klaipeda, nella vecchia città che tanto ricorda i paesi scandinavi, per vedere il nuovo appartamento di Andrea. Chi è Andrea? Andrea è uno dei tanti ragazzi che si è stufato di tante cose e dato che quando si è stufi si è molto caldi, è venuto a sbollentarsi qui a Klaipeda, il suo intento è di stabilirsi qui, ma la vita si fa beffe dei nostri progetti esattamente come io mi sto facendo beffe di voi scrivendo queste righe di **quasi** perfetto italiano stando seduto sul cesso. (Il portatile non lo venderò mai)

 Alle 17 eravamo entrambi in università per seguire il corso di lituano. L'università di Klaipeda non è molto rinomata, anzi non è paragonabile all'università di Vilnius o Kaunas e a sentir dire ne risente anche molto a livello di finanziamenti statali.   
Al corso di lituano siamo in pochi; non superiamo mai le 5 persone! C'è un ragazzo spagnolo, un tedesco, una bielorussa, una polacca e, quando non è in giro per affari, anche uno signore svedese. L'insegnante è una bella biondina di 27 anni che riesce a "*switchare*" dal russo all'inglese al tedesco e al lituano nel giro di pochi minuti, le lezioni sono davvero interessanti.   
Sono entrato nel gruppo alla tredicesima lezione e per fortuna non sono molto indietro, la lingua non è facile, ma **tutto è difficile prima di diventare facile.**

 Finita la lezione Rita passa a prendermi in macchina, è andata a fare la spesa togliendomi uno dei più grandi fastidi della mia vita: **fare la spesa**. Io le passo la lista e lei compra, una volta arrivati a casa abbiamo preso una pizza che è arrivata a casa dopo 45 minuti. Ragazzi, erano già le 20 e di cucinare non ne avevo nessuna voglia, giuro che domani preparo qualcosa già al mattino. Ricordo che quella buonanima di mia nonna, puro sangue terron-calabro, metteva già il sugo a cuocere alle 9 del mattino. Gran donna! Gran sugo!

 A proposito di cucina, ho preso le redini dell'apparato digerente famigliare perché ero davvero stufo di mangiare veleno. Cucino io e mi diverto pure, qualcosa già lo sapevo fare, a volte chiedo consiglio ad Andrea (che cucina decisamente meglio di me) ma il più delle volte cerco le ricette su internet, prendo 4 o 5 ricette dello stesso piatto e poi creo la mia variante. La prossima tappa sarà farsi il database con le varie ricette e annotazioni (deformazione informatica professionale o una scusa come tante per stare seduto davanti al mac?). Ho notato che Youtube è piena di ricette, ma in particolare ho trovato dei ragazzi che oltre a insegnare come fare piatti pieni di **porcume** ti fanno spaccare dalle risate. Vi consiglio di vederli perché sono due africani siciliani (come si definiscono loro) troppo simpatici. Visitate il sito del [gambero grosso](http://www.gamberogrosso.it)!

 Acquisti del giorno: oggi è stata la giorna di iTunes Store, oltre a fare un *buono regalo* per il compleanno di mio padre mi sono preso un album che volevo acquistare da una vita. Il [primo album](http://www.amazon.co.uk/Boston-Remastered/dp/B000YQPMMG/ref=sr_1_2?ie=UTF8&s=music&qid=1225402970&sr=1-2) dei "Boston" rilasciato nel lontano anno 1976, già che c'ero ho anche acquistato due singoli dei Kansas, indovinate voi quali, non è difficile

 Ho addormentato la bambina, fatto due parole con Rita, salutato gli amici su skype e ora sono qui che finisco di scrivere questo lungo post scritto di getto ascoltando i Boston.


 Rileggere? Correggere? Mia nonna avrebbe detto: "Man'co pu' cazz!"

 Buonanotte  
  
[![Tolvaida e Greta](http://farm4.static.flickr.com/3038/2986989381_61baf34d91_m.jpg)](http://www.flickr.com/photos/kmen-org/2986989381/ "Tolvaida e Greta di karimblog, su Flickr")  


