Title: Esperienze di un giovane imprenditore: i contratti
Date: 2007-06-20
Category: altro
Slug: esperienze-di-un-giovane-imprenditore-i-contratti
Author: Karim N Gorjux
Summary: Prima magagnaA causa di un contratto con una concessionaria di Cuneo che ricorda vagamente il monte più famoso del Giappone, mi ritrovo con delle grane che non avrei mai voluto avere.

**Prima magagna**  
  
A causa di un contratto con una concessionaria di Cuneo che ricorda vagamente il monte più famoso del Giappone, mi ritrovo con delle grane che non avrei mai voluto avere.


 Il tutto è partito da un presupposto di fiducia con il venditore, assunto da poco e a cottimo, che ha stipulato un contratto con il mio ex socio per l'acquisto di una macchina da 13.000€ a nome della ditta. Ovviamente alla stipula del contratto non è stata richiesta nessuna caparra dato che ci è stato assicurato che il contratto **non aveva valore vincolante**.


 Ora la situazione è che il venditore ha sbagliato a non chiedere una caparra e il mio ex-socio ha sbagliato a fidarsi di lui. Succede che io e il mio ex-socio cambiamo lo statuto, lui esce e io tengo la società. Avvertiamo il venditore di questa decisione e lui ci dice di portare il nuovo statuto in concessionaria e che ci annulla il contratto.  
Dopo qualche giorno ricevo una telefonata dal venditore che mi dice che la concessionaria vuole percorrere le vie legali nei confronti della società.


 Chiamo la mia cuginona Perry "Emma" Mason che ha le palle d'acciaio come Mazinga e mi sgancia una meteora nei confronti del venditore degna [dell'estinzione dei dinosauri](http://en.wikipedia.org/wiki/Dinosaur#Asteroid_collision). Io intanto, da socio abbonato, contatto [Altroconsumo](http://www.altroconsumo.it).


 Le telefonate di mia cugina sono incredibili, da grande donna di origini calabresi si fa rispettare e non si fa mettere i piedi in testa da nessuno. Altroconsumo conferma che siamo nel giusto e nel caso in cui la concessionaria faccia dei problemi interviene direttamente lei contattando la concessionaria e la sede italiana della casa automobilistica.


 Il brutto di questa faccenda è che il venditore ha cercato di trovarmi delle soluzione in cui io pago e ci rimetto per un loro sbaglio, tra l'altro il tipo di soluzioni da lui proposte, a detta del commercialista, mi incasinavano nei confronti della finanza e soprattutto erano delle soluzioni più favorevoli a loro che a me oltre che **poco legali**.   
La mia sensazione è stata quella che può provare un ragazzino fermato sotto i portici di Cuneo a cui chiedono una firma per una petizione, per poi trovarsi l'obbligo d'acquisto di una enciclopedia una volta tornato a casa. Sono **completamente schifato**, mi sono difeso ricorrendo ad internet e alle mie conoscenze, ma un'altra persona che non usa internet o non sa a chi chiedere? Se una persona avanti con gli anni va in quella concessionaria, si trova davanti al venditore gentile e cortese dalle belle parole e poi rimane fregata, **come si tutela**?  
![Onesta](http://www.karimblog.net/wp-content/uploads/2007/06/onesta.jpg)

 Non sono sicuro se posso, ma mi sentirei quasi in dovere di dare le generalità della concessionaria come consumatore insoddisfatto.


 **Seconda magagna**  
  
A causa della mia inesperienza contatto un venditore Vodafone della zona e stipulo un contratto business per delle SIM per me e il mio socio. Il venditore in questione è il nostro referente per qualsiasi problema.


 Dopo la decisione del cambio statuto lo chiamo per spiegargli la situazione, ma non risponde. Gli scrivo una mail, il giorno dopo lo chiamo, risponde e mi dice che ha letto l'email e che **non ha idea **se posso dare disdetta delle sim dato che il contratto ha durata 24 mesi. Mi dice che si fa sentire, ma è passato 1 mese e non l'ho ancora sentito. Ma che razza di referente è?   
Chiamo il 190 e dicono che per ogni SIM devo sborsare 300€ di penale. Pazzesco! Ma siamo matti?

 Cerco su internet e scopro che la legge 40/2007 del 2 Aprile 2007 comunemente chiamata "Decreto Bersani", con effetto retroattivo, mi libera dal pagamento di qualsiasi penale in caso di trasferimento o disdetta di una SIM. Chiamo altroconsumo e mi **conferma** il tutto.


 Mi chiedo io, ma se non andavo a leggere su internet? Se non chiamavo altroconsumo per le conferme? Quante persone pagano le penali **senza sapere** che possono rifiutarsi per legge?

 In conclusione: chi vende, nella maggior parte dei casi, vuole **solo guadagnare.** Noi siamo consumatori, **non siamo persone**, siamo solo manipolati da venditori per indurci ad acquistare. Soldi, soldi e solo soldi. Dove è finita l'onestà? Intanto è sicuro, prima di firmare qualcosa farò leggere sia al commercialista che alla mia cuginona.


