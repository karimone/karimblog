Title: Argomentazione valida
Date: 2006-10-13
Category: Lettonia
Slug: argomentazione-valida
Author: Karim N Gorjux
Summary: Sempre così, se dico che sono stato 45 anni in Lituania allora qualsiasi cosa dico è vera. Argomentazione valida, ovviamente:

Sempre così, se dico che sono stato 45 anni in Lituania allora qualsiasi cosa dico è vera. Argomentazione valida, ovviamente:

 Karim: le donne lituane sono bionde dalla pelle bianca.  
tipo: cazzo dici, sono 146 anni che sono in lituania. Sono tutte negre!  
Karim: ah, è vero. Hai ragione. Scusa.


 E' come dire che per parlare di ippica, **bisogna essere un cavallo. **

 (liberamente tratto da un commento lasciato da me in giro, ho giusto semanticamente corretto l'esempio)

