Title: Allarmi, antifurti, macchine: come la Lituania può apparire decisamente strana
Date: 2009-07-23
Category: Lituania
Slug: allarmi-antifurti-macchine-come-la-lituania-puo-apparire-decisamente-strana
Author: Karim N Gorjux
Summary: La concentrazione di macchine che vedo nel mio quartire è impressionante. Il quartiere è stato progettato con palazzi di varie altezza da 4 piani fino a 9 piani, ad esclusione del palazzo[...]

[![](http://farm2.static.flickr.com/1119/3168421168_94ee30c473_m.jpg "Tante macchine di merda")](http://www.flickr.com/photos/undress/3168421168/)La concentrazione di macchine che vedo nel mio quartire è impressionante. Il quartiere è stato progettato con palazzi di varie altezza da 4 piani fino a 9 piani, ad esclusione del palazzo di 20 piani adiacente alla statale che sembra una costruzione a sè; la cosa che trovo davver strana sono i parcheggi sotterranei, sono troppo piccoli. Non ho fatto i calcoli, ma penso che siano costruiti per tenere al massimo il 20% delle macchine presenti  
  
Di conseguenza questo quartiere ha ampi parcheggi all'aperto che separano i palazzi, ma non manca l'area verde per i bambini che è ben curata e dotata di giochi in legno di qualità e sempre ben tenuti. E' una bella zona, mi piace e ci viviamo bene. Pensa che nei [quartieri Kruciov](http://www.karimblog.net/2009/06/30/una-mia-considerazione-personale-sui-condomini-sovietici-meglio-noti-come-i-blocchi-krusciov/), essendo stati costruiti con una mentalità ben diversa, non esiste nessuno spazio adibito alle macchine e quindi la situazione non è piacevole. 

 Cosa non sopporto di tutte queste macchine sono gli antifurti. Hai mai sentito l'antifurto di una macchina lituana? Mi è capitato già più di una volta di far assistere involontariamente a qualche mio cliente con cui parlavo al telefono i famosi **allarmi polifonici a 4 melodie stereo con clacson**. Rimangono tutti sorpresi e tutti mi chiedono cosa sta succedendo. 

 Non pensare che gli allarmi suonino perché c'è sempre un membro della banda bassotti a cercare di rubare una macchina. Purtroppo gli antifurto sono installati su **tutti i tipi di macchine** sia nuove che vecchie di vent'anni ed in più sono regolati da avere una sensibilità ai limiti della decenza. Non è raro camminare di fianco ad una macchina in un parcheggio e sentire una piccola segnalazione "di avvertimento" accompagnato dalle quattro frecce. 

 Ricordo che gli antifurto sono stati la prima cosa che ho notato quando ho iniziato a vivere in Lituania. Ad essere più precisi ho notato che l'antifurto fa parte di un preciso scambio con l'Italia: niente chiesa con le campane ad ogni ora, ma [tante auto vecchie e nuove](http://www.karimblog.net/2009/06/05/nei-paesi-baltici-puoi-vedere-molte-di-macchine-di-lusso-ma-non-e-tutto-oro-quello-che-luccica/) con l'allarme che suona senza motivo.


