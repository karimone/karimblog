Title: Steve Jobs ottiene la laurea ad honorem
Date: 2005-11-29
Category: altro
Slug: steve-jobs-ottiene-la-laurea-ad-honorem
Author: Karim N Gorjux
Summary: Di seguito potete leggere il discorso di Steve Jobs tenuto alla Stanford University di Palo Alto per il conferimento della laurea ad honorem, vi consiglio di leggerlo perché non parla solo di[...]

Di seguito potete leggere il discorso di Steve Jobs tenuto alla Stanford University di Palo Alto per il conferimento della laurea ad honorem, vi consiglio di leggerlo perché non parla solo di computer, ma di vita e di morte...   
  
*Sono onorato di essere con voi oggi, per la vostra laurea in una delle migliori università del mondo. Io non mi sono mai laureato. Ad essere sincero, questo è la cosa più vicina ad una laurea, per me.  
Oggi voglio raccontarvi tre storie che mi appartengono. Tutto qui. Niente di particolare. Solo tre storie.  
  
  
La prima storia parla di unire i puntini.  
  
  
Ho smesso di frequentare il Reed College dopo i primi 6 mesi, ma gli sono rimasto attorno per altri 18 mesi prima di lasciarlo definitivamente. Perchè lo feci?  
  
  
Tutto cominciò prima che nascessi. Mia madre biologica era una giovane studente universitaria nubile e decise di darmi in adozione. Sentiva nel suo cuore che io dovessi essere adottato da un laureato, così venne preparata la mia adozione, alla nascita, per un avvocato e sua moglie.  
  
  
Solo quando vidi la luce questi decisero all’ultimo momento di desiderare una bambina. Quindi i miei genitori, che erano in lista d’attesa, vennero chiamati nel mezzo della notte da una voce che chiedeva: “Abbiamo un bambino indesiderato, lo volete?” Essi dissero: “Certo”. Mia madre biologica scoprì in seguito che mia madre non si era mai laureata a che mio padre non aveva neanche il diploma di scuola superiore. Rifiutò di firmare i documenti per l’adozione. Accettò, riluttante, solo qualche mese dopo quando i miei genitori promisero che un giorno sarei andato all’università.  
  
  
17 anni dopo andai all’università. Ma ingenuamente scelsi un istituto universitario costoso quanto Stanford, e tutti i risparmi dei miei genitori lavoratori furono spessi per la retta. Dopo sei mesi non riuscivo a vederne l’utilità. Non avevo idea di cosa fare nella vita e nessun indizio su come l’università avrebbe potuto aiutarmi a capirlo. Così spesi tutti i soldi che i miei genitori avevano risparmiato in un’intera vita di lavoro. Decisi di non seguire il piano degli studi obbligatorio, confidando nel fatto che tutto si sarebbe sistemato. Ero molto spaventato da quella decisione, ma col senno di poi, sarebbe stata una delle migliori decisioni che avessi mai preso. Nel momento in cui scelsi un piano di studio personalizzato avevo la possibilità di ignorare le lezioni che non mi interessavano e di scegliere quelle che mi apparivano più interessanti.  
  
  
Non era per niente romantico. Non avevo una stanza al dormitorio, così dormivo sul pavimento in stanze di amici. Restituivo i vuoti di cocacola per i 5 centesimi di deposito, ci compravo da mangiare, e mi facevo più di 10 kilometri a piedi attraverso la città, ogni domenica notte, per avere un pasto a settimana al tempio Hare Krishna. Che bello. Tutto quello in cui inciampai semplicemente seguendo la mia curiosità ed il mio intuito si rivelarono in seguito di valore inestimabile.  
  
  
Per esempio:  
  
  
il Reed College all’epoca offriva quello che era probabilmente il miglior corso di calligrafia del paese. In tutto il campus, ogni manifesto, ogni etichetta su ogni cassetto, era meravigliosamente scritto a mano. Decisi di prendere lezioni di calligrafia. Appresi la differenza tra i tipi di caratteri con grazie e senza grazie. Imparai l’importanza della variazione dello spazio tra combinazioni diverse di caratteri. Mi insegnarono quali elementi fanno della tipografia, una grande tipografia. Era affascinante: si trattava di storia, bellezza ed arte come la scienza non può catturare.  
  
  
Niente di tutto ciò aveva la benchè mnima speranza di una qualunque applicazione nella mia vita. Ma dieci anni dopo, quando stavamo progettando il primo computer Macintosh, tutto mi tornò utile. E lo mettemo interamente nel Mac. Era il primo computer che curasse la tipografia. Se non avessi mai scelto quel corso, al college, il Mac non avrebbe mai avuto font proporzionali e font a larghezza fissa. E siccome Windows ha copiato il Mac, è probabile che nessun computer li avrebbe avuti. Se non avessi scelto di interrompere il piano degli studi obbligatorio non avrei scelto quel corso di calligrafia ed i personal computer avrebbero potuto non avere la stupenda tipografia che hanno. Era ovviamente impossibile unire i puntini guardando al futuro mentre ero al college e capire in cosa si sarebbe concretizzat. Ma la realizzazione era estremamenta chiara, guardardando alle spalle, dieci anni dopo.  
  
  
Ve lo ripeto, non puoi unire i puntini guardando al futuro, puoi connetterli in un disegno, solo se guardi al passato. Dovete quindi avere fiducia nel fatto che i puntini si connetteranno, in qualche modo, nel vostro futuro. Dovete avere fede in qualcosa - il vostro intuito, il destino, la vita, il karma, quello che sia. Questo approccio non mi ha mai deluso e ha fatto tutta la differenza nella mia vita  
  
  
La seconda storia parla d’amore e di perdita.  
  
  
Sono stato fortunato - ho scoperto quello che amavo fare molto presto. Woz ed io fondammo la Apple nel garage dei miei genitori quando avevo vent’anni. Lavorammo duro, e in 10 anni la Apple crebbe dai due che eravamo in un garage ad una società da 2 miliardi di dollari con più di 4000 impiegati.  
  
  
Avevamo appena creato il nostro miglior prodotto - il Macintosh - un anno prima, e io avevo appena compiuto 30 anni. E fui licenziato. Come si fa ad essere licenziata dalla compagnia che hai fondato? Beh, non appena la Apple si espanse assumemmo qualcuno che pensavo fosse molto capace nel gestire l’aziende con me, e per il primo anno le cose andarono bene.  
  
  
Ma la nostra visione del futuro cominciò a divergere e alla fine decidemmo di rompere. Quando ci fu la rottura i nostri dirigenti decisero di stare dalla sua parte. Così, a trent’anni, ero fuori. E molto pubblicamente. Il centro della mia vita da adulto era completamente andato, sparito, è stato devastante.  
  
  
Non ho saputo che pesci pigliare per un po’ di mesi. Sentivo di aver deluso la precedente generazione di imprenditori per aver mollato la presa. Incontrai David Packard e Bob Noyce per cercare di scusarmi per aver rovinato tutto così malamente. Fu un fallimento pubblico, pensai addirittura di andarmene. Ma qualcosa, lentamente, si faceva luce in me. Amavo ancora quello che avevo realizzato. L’inaspettato e repentino cambiamento alla Apple non avevano cambiato quello che provavo, neanche un poco. Ero stato rifiutato, ma ero ancora innamorato. Quindi decisi di ricominciare.  
  
  
All’epoca non me ne accorsi, ma il mio licenziamento dalla Apple fu la cosa migliore che poteva capitarmi. Il peso del successo fu rimpiazzato dall’ illuminazione di essere un principiante ancora una volta, con molta meno sicurezza su tutto. Questo mi liberò e mi consentì di entrare in uno dei periodi più creativi della mia vita.  
  
  
Durante i cinque anni successivi, fondai una società di nome NeXT, un’altra di nome Pixar, a mi innamorai di una meravigliosa donna che sarebbe poi diventata mia moglie.  
  
  
Pixar finì per creare il primo film animato al computer della storia, Toy Story, ed è ora lo studio di animazione più famoso al mondo. Apple, con una mossa notevole, acquisì NeXT, io tornai ad Apple, e la tecnologia che sviluppo con NeXT è oggi nel cuore dell’attuale rinascimento di Apple. Laurene ed io abbiamo una stupenda famiglia.  
  
  
Sono sicurissimo che niente di tutto ciò sarebbe accaduto se non fossi stato licenziato da Apple. E’ stato un boccone amarissimo da buttar giù, ma era la medicina di cui avevo bisogno. A volte la vita ti colpisce in testa come un mattone. Non perdete la fede. Sono convinto del fatto che l’unica cosa che mi ha consentito di proseguire sia stato l’amore che provavo per quello che facevo. dovete trovare ciò che amate. E’ questo è tanto vero per il vostro lavoro quanto per chi vi ama. Il lavoro riempirà gran parte della vostra vita e l’unico modo per essere veramente soddisfatti e quello di fare quello che pensate sia il lavoro migliore. E l’unico modo per fare il lavoro migliore e quello di amare quello che fate. Se non lo avete ancora trovato, continuate a cercare. Non vi fermate. Come tutti gli affari di cuore, lo saprete quando lo troverete. E, come nelle migliori relazioni, diventerà sempre migliore al passare degli anni. Quindi, continuate a cercarlo fino a quando non l’avrete trovato. Non fermatevi.  
  
  
La terza storia parla di morte.  
  
  
Quando avevo 17 anni, lessi un brano che diceva più o meno: “se vivi ogni giorno come se fosse l’ultimo, prima o poi si lo sarà veramente”. Rimassi impresso, e da allora, per gli ultimi 33 anni, ho guardato nello specchio ogni mattina e mi sono chiesto:”se oggi fosse l’ultimo giorno della mia vita, vorrei veramente fare quello che sto per fare oggi?” E ogni volta che la risposta fosse “No” per troppi giorni di seguito sapevo di aver bisogno di cambiare qualcosa.  
  
  
Ricordare che morirò presto è lo strumento più importante che mi ha consentito di fare le scelte più grandi della mia vita. Perchè praticamente tutto - tutte le aspettative, l’orgoglio, le paure di fallire - tutte queste cose semplicemente svaniscono di fronte alla morte, lasciandoci con quello che è veramente importante.  
  
  
Ricordarsi che moriremo è il modo migliore che conosco per evitare le trappola di pensare di avere qualcosa da perdere. Siete già nudi. Non c’è nessun motivo per non seguire il vostro cuore.  
  
  
Circa un anno fa mi è stato diagnosticato un cancro. Ho fatto una TAC alle 7:30 del mattino e mostrava chiaramente un tumore nel mio pancreas. Non sapevo neanche cosa fosse un pancreas. I dottori mi dissero che si trattava sicuramente di un tipo di cancro incurabile, e che avrei avuto un’aspettativa di vita non superiore ai 3-6 mesi. Il mio dottore mi consigliò di andare a casa e di sistemare le mie cose, che è il messaggio in codice dei dottori per dirti di prepararti a morire. Significa che devi provare a dire ai tuoi bambini ogni cosa che pensavi di dirgli nei prossimi dieci anni, in pochi mesi. Significa che devi assicurarti che ogni cosa sia a posto così che sarà la più facile possibile per la tua famiglia. Significa che devi dire addio.  
  
  
Ho vissuto con quella diagnosi tutto il giorno. Più tardi, nel pomeriggio, mi è stata fatta una biopsia. Mi hanno infilato un endoscopio nella gola che è passato per il mio stomaco ed il mio intestino. hanno messo un ago nel mio pancreas e hanno prelevato alcune cellule dal tumore. Ero sotto sedativi, ma mia moglie, che era lì, mi ha detto che quando hanno analizzato le cellule al microscopio i dottori cominciarono a piangere perchè scoprirono che si trattava di una rarissima forma di cancro pancreatico curabile con la chirurgia. Sono stato operato. Ora sto bene.  
  
  
E’ stata la mia esperienza più vicina alla morte e spero che rimanga tale per qualche decennio ancora. Avendola superata posso finalmente dirvi con più certezza di quando la morte era semplicemente un utile concetto ma puramente intellettuale:  
  
  
Nessuno vuole morire. Neanche chi vuole andare in paradiso vuole morire per arrivarci. E nonostante tutto, la morte è la destinazione che condividiamo. Nessuno vi è mai sfuggito. E così dovrebbe essere perchè la Morte è probabilmente l’unica, migliore invenzione della Vita. E’ l’agente di cambiamento della Vita. Elimina il vecchio per far spazio al nuovo. Proprio adesso il nuovo siete voi, ma un giorno non troppo distante da oggi, diventerete gradualmente il vecchio che deve essere eliminato. Mi dispiace essere così drammatico, ma questa è la verità.  
  
  
Il vostro tempo è limitato, quindi non sprecatelo vivendo la vita di qualcun altro. Non lasciatevi intrappolare dai dogmi - che vuol dire vivere seguendo i risultati del pensiero di altri. Non lasciate che il rumore delle opinioni altrui lasci affogare la vostra voce interiore. E, cosa più importante, abbiate il coraggio di seguire il vostro cuore ed il vostro intuito. Loro sanno già quello che voi volete veramente diventare. Tutto il resto è secondario.  
  
  
Quando ero giovane, c’era un’incredibile pubblicazione chiamata The Whole Earth Catalog, che era una delle bibbie della mia generazione. Era stata creata da un tizio di nome Stewart Brand non troppo lontano da qui, a Menlo Park, e la portò alla luce con il suo tocco poetico. Stiamo parlando dei tardi anni ‘60, prima dei computer ed il desktop publishing, quidi era tutta fatta con macchine da scrivere, forbici e Polaroid. Era una sorta di Google di carta, 35 anni prima della venuta di Google: era idealistico, e pieno di strumenti utili ed informazioni preziose.  
  
  
Stewart ed il suo gruppo pubblicaro molti numeri del Grande Catalogo Mondiale fino all’ultima edizione. Eravamo a metà degli anni ‘70 ed io avevo la vostra età. Sul retro di copertina dell’ultimo numero c’erà la foto di una strada di campagna all’alba, quel tipo di strada sulla quale potreste trovarvi a fare l’autostop se voste così avventurosi. Sotto c’erano queste parole “Siate affamati, siate assurdi”. Questo era il messaggio di congedo. Rimanere affamato. Rimanere assurdo. Me lo sono sempre augurato. Ed ora, per voi che state per laurearvi, lo auguro a voi.  
  
  
Siate affamati. Siate folli.  
  
  
Grazie.*  
  
  
  
  
link [Stay hungry, Stay foolish](http://lucaedeborah.blogspot.com/2005/11/stay-hungry-stay-foolish.html)  
  


