Title: Richiesta di consigli su come affrontare il freddo buio e molto lungo inverno Baltico
Date: 2009-10-20
Category: Lituania
Slug: richiesta-di-consigli-su-come-affrontare-il-freddo-buio-e-molto-lungo-inverno-baltico
Author: Karim N Gorjux
Summary: Ultimamente sto lottando contro l'autunno freddo, buio e triste che purtroppo è solo il preludio di un lungo ed inevitabile inverno. I weekend sono tragici, Rita e Greta sono a casa, piove[...]

[![](http://farm2.static.flickr.com/1174/3167892776_115ecdf7ac_m.jpg "Strada innevata")](http://www.flickr.com/photos/you-rate/3167892776/)Ultimamente sto lottando contro l'autunno freddo, buio e triste che purtroppo è solo il preludio di un lungo ed inevitabile inverno. I weekend sono tragici, Rita e Greta sono a casa, piove e fa freddo ci sono poche cose da fare, ma mentre Rita qualcosa da fare se lo inventa sempre, il problema grosso è la bambina che di stare in casa ha ben poca voglia.  
  
Klaipeda è un paese piccolino e non ci sono grandi svaghi. Dopo che sei andato al cinema, in palestra, al ristorante o hai fatto un giro nei centri commerciali, hai finito le proposte.. Gli unici parenti che abbiamo sono i genitori di Rita ed abitano a circa 80km da qui, montagne non ce ne sono e anche se ci fossero piove troppo per andare a funghi o fare scampagnate.


 Purtroppo i soldi non lo permettono, ma sarebbe bello poter ogni mese farsi 3-4 giorni nel sud della Spagna o nel nord d'Africa per ricaricarsi le batterie un po' come una calcolatrice solare. Un mio amico che vive a Malaga, mi ha mandato una sua foto mentre è con la sua fidanzata in spiaggia a prendersi il sole; la foto è stata scatta il 18 Ottobre. Nell'entroterra lituano abbiamo già visto i primi fiocchi di neve.  
Non è poi solo una questione di "svago", ma la mancanza di sole ha degli effetti depressivi sull'umore; in lituania vanno molto di moda i solarium che non vengono solamente per bellezza, ma soprattutto per sopperire alla mancanza di luce del periodo da Ottobre a Marzo.


 Non ho la pretesa di aspettarmi il sole caraibico da un paese affacciato sul mar Baltico, ma non posso negare che sia difficile chiudere nel cassetto dei ricordi le partite di calcio, i pomeriggi al mare e le corse in bici nella foresta rimandando tutto ad Aprile.  
Anche a Cuneo l'inverno e freddo, nevica e piove, ma a differenza della Lituania, non è raro svegliarsi la mattina con il sole e vedersi le alpi innevate che ti augurano il buongiorno. 

 Tra un paio di giorni mi scade l'abbonamento della palestra, sto meditando di rinnovarlo con una formula che copra anche il Sabato e la Domenica giusto per avere qualcosa da fare che non sia diventare il miglior amico del divano. E' un piccolo passo avanti, ma in questo periodo sono a corto d'idee. Tu hai qualche consiglio? Che cosa faresti per combattere il freddo e il buio di circa sei mesi di lungo inverno Baltico? Cosa faresti fare alla bambina nel periodo invernale?

 Sarebbe interessante avere l'opionione del [coach motivazionale dei blogger](http://www.efficacemente.com/ "Efficacemente.com"), nel caso sia anche lui sintonizzato sul blog :-)

