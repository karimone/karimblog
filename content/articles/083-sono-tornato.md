Title: Sono tornato
Date: 2005-08-01
Category: altro
Slug: sono-tornato
Author: Karim N Gorjux
Summary: La palestra dove ho insegnato è tornata ad essere la mia palestra. Io sono cambiato, in peggio, ma non tutto è perduto.Spero che possa essere veramente così. Tornare in quell'ambiente mi ha[...]

La palestra dove ho insegnato è tornata ad essere la mia palestra. Io sono cambiato, in peggio, ma non tutto è perduto.  
  
Spero che possa essere veramente così. Tornare in quell'ambiente mi ha dato una carica che non sentivo da mesi, le pianificazioni per il futuro sono ottime, ringraziando il cielo ho alcune persone attorno a me che mi danno una mano e delle motivazioni non indifferenti. Spero di ritornare ai livelli di un tempo, anzi, meglio.


