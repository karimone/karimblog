Title: Klaipeda Riga andata e ritorno
Date: 2006-09-29
Category: Lituania
Slug: klaipeda-riga-andata-e-ritorno
Author: Karim N Gorjux
Summary: Ho dovuto portare i parenti all'aeroporto di Riga, ma purtroppo mentre percorrevo la A1 mi si è spaccata la cinghia che collega il motore alla dinamo. In pratica dopo 1 ora avevo[...]

Ho dovuto portare i parenti all'aeroporto di Riga, ma purtroppo mentre percorrevo la A1 mi si è spaccata la cinghia che collega il motore alla dinamo. In pratica dopo 1 ora avevo la batteria a terra e ho dovuto cercare un meccanico che ci risolvesse il problema in tempo da far prendere l'aereo ai miei genitori.  
  
Ci fermiamo ad un autogrill e usando il mio lituano rozzo chiedo alla barista se conosce un meccanico nelle vicinanze, stranamente la tipa ci risponde malissimo, io continuavo a parlare in lituano e lei continuava a rispondere russo, fortunatamente un autoservizio ci ha aiutati a raggiungere un meccanico onesto che ha risolto i nostri alla modica cifra di 55 litas.


 Dato che l'aereo partiva alle 15, ho avuto il tempo di passare il pomeriggio in Riga in compagnia della mia amica Jana, Riga è cambiata tantissimo molti negozi, molti stranieri, tantissime macchine. Ormai Riga è una capitale viva, molto diversa da come l'avevo vista io anni addietro. Ho avuto l'impressione che il turismo sia amato/odiato, amato a causa dei soldi che il turista porta e "odiato" a causa della *riservatezza* del popolo lettone (e non solo).


 La sera me ne sono tornato a Klaipeda, una volta varcata la frontiera mi sono sentito come a casa, questo è il problema a sapere (parolona) una lingua parlata da appena 3 milioni di persone.


