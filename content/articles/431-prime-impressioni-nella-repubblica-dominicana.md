Title: Prime impressioni nella Repubblica Dominicana
Date: 2008-03-27
Category: altro
Slug: prime-impressioni-nella-repubblica-dominicana
Author: Karim N Gorjux
Summary: Sono partito alle 15 da Milano Malpensa per arrivare dopo 10 ore di volo alle 20:30 a Punta Cana. Al mio arrivo il mio amico Lello mi aspettava all'aeroporto con un taxi[...]

Sono partito alle 15 da Milano Malpensa per arrivare dopo 10 ore di volo alle 20:30 a Punta Cana. Al mio arrivo il mio amico **Lello** mi aspettava all'aeroporto con un taxi noleggiato per l'occasione.


 **L'aereo è una macchina infernale**, stai dieci ore seduto una poltrona e poi ti ritrovi in un altro mondo, in un altro tempo. La lingua è diversa, la gente è diversa, gli odori sono diversi.  
Per arrivare qui a Boca de Yuma abbiamo percorso circa un centinaio di km di strade, era buio e quindi non ho potuto vedere molto, ma passando nel mezzo di Punta Cana e di Higuey ho potuto notare che la Repubblica Dominicana oltre che essere assolutamente uno stato molto povero subisce anche molto **l'influenza statunitense**, sia nei modi di fare, di vestirsi che nella ricerca degli status symbol.


 A Boca de Yuma sembra di essere in un piccolo paesino turistico circondato da una piccola favelas. A me fa persino strano che qui ci siano dei ristoranti ed un albergo che tra l'altro è nuovo; le strade per il 90% non sono asfaltate, 9 bambini su 10 non hanno le scarpe, le macchine hanno 1 targa si e due no, i motorini non hanno le targhe nel modo più assoluto e ci viaggiano sopra in 3 o 4 ovviamente senza casco.


 **Il mare è stupendo** ed essendo Boca de Yuma un baya di pescatori, per arrivare alla spiaggia caraibica bisogna farsi portare all'altra sponda del piccolo golfo che non è raggiungibile a piedi se non con due ore di camminata. La piccola traversata nella barca di legno ha un non so che di suggestivo, sembra di far parte di **una piccola spedizione **che si vedono nei documentari di Discovery Channel, ma soprattutto di appartenere ad un altro tempo.


 Arrivati sull'altra sponda, camminiamo per 1km tra **baracche e bambini** che vengono a salutare Lello per chiedere le caramelle di cui non siamo mai senza. I bambini sono veramente tantissimi, mi è capitato di vedere una piccola ragazzina a cui non avrei dato, ad occhio e croce, più di 16 anni con il suo piccolo bambino di 1 anno in braccio. Non sono molto abituato a queste cose, la cosa mi ha lasciato leggermente turbato.


 La spiaggia è proprio dello stile caraibico: solitaria, sabbia finissima, mare chiarissimo, ma un unico neo: i dominicani non hanno **nessuna concezione dell'ambiente**, sono peggio dei maiali. Buttano qualsiasi cosa che hanno in mano per terra, nel mare, in mezzo ai prati, fuori dal finestrino del bus o direttamente sulla strada davanti a casa loro. Sono zozzi! Purtroppo questo è un paese che vive di turismo e i dominicani vogliono il turismo, ma manca la cultura. Il livello generale di cultura è bassissimo, "l'ingegnero" del paese ha l'istruzione equivalente ad un nostro ragazzino di terza media ed è realmente laureato!

 Per concludere questo post, spendo due parole sulla presenza tecnologica in questo paese. La luce elettrica manca all'incirca per 5-6 ore al giorno, ma per fortuna abbiamo dei generatori di corrente. Non esiste il telefono fisso, la connessione ad internet è un gprs via cellulare che mi permette appena di leggere le email. L'acqua è fredda ed tenuta su una cisterna nel tetto che periodicamente andiamo a mischiare con del cloro. 

 Dedicherò un articolo a parte solo per descrivere questa gente che a mio parere è **sorprendente**.


