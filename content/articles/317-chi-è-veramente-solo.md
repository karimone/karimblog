Title: Chi è veramente solo?
Date: 2007-04-18
Category: altro
Slug: chi-è-veramente-solo
Author: Karim N Gorjux
Summary: Sono 29 giorni che sono da solo, non mi sono sballato, non mi sono dato alla pazza gioia semplicemente perché per me non è liberta essere senza moglie e figlia, anzi, a[...]

Sono 29 giorni che sono da solo, non mi sono sballato, non mi sono dato alla pazza gioia semplicemente perché per me non è liberta essere senza moglie e figlia, anzi, a dire il vero da quando sono andate via le mie signore mi sento incatenato ai miei vuoti dell'inutilità.


 Il sabato sera generalmente non esco, se esco generalmente esco con un'amica. A Pasquetta mi sono ritrovato a costineggiare con un gruppo di persone che non frequento quasi mai, ma tra queste persone c'era un amico che tutti vorrebbero avere, ma nessuno mai valorizza. Non è bello, non è ricco, non veste alla moda, non ha la ragazza stupenda, ma è di cuore buono, è [semplice](http://marialuisamirabella.splinder.com/tag/semplicit%C3%A0), è genuino. E' troppo semplice e troppo genuino per essere apprezzato, è la solita bassa plebaglia che non interessa a nessuno.


 A Pasquetta mi sono trovato bene: costinata, partita a calcio nei prati, sfida a risiko. Tutte cose semplici e armoniose che non provavo da tempo. E' il divertirsi con poco che ho visto molte volte in Lituania, dove i ragazzi si divertono giusto stando insieme, una pratica sempre più rara dalle nostre parti.


