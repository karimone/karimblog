Title: Buon Lunedi' a tutti
Date: 2005-04-25
Category: Lituania
Slug: buon-lunedi-a-tutti
Author: Karim N Gorjux
Summary: Ieri sono andato in spiaggia per vedermi il tramonto, mi spiace ammetterlo, ma la gente qui sporca molto e volentieri. Ho notato rifiuti dappertutto: mozziconi di sigarette, lattine, bottiglie. Le avevo notate[...]

Ieri sono andato in spiaggia per vedermi il tramonto, mi spiace ammetterlo, ma la gente qui sporca molto e volentieri. Ho notato rifiuti dappertutto: mozziconi di sigarette, lattine, bottiglie. Le avevo notate anche durante il viaggio in treno da Vilnius a Klaipeda, ogni tanto buttavo un occhio dal finestrino e i ricordi di picnic estivi erano ancora li' ad aspettare l'erosione del tempo. Peccato davvero, con tutta la bella natura che hanno mi dispiace davvero tanto vederla trattata male cosi' :-( 

 In compenso il tramonto e' stato stupendo.


 La sera siamo andati al [Memelis](http://www.memelis.lt) dove mi sono abbuffato di piatti locali con Rita, Davide e Torvaida (che parla pure italiano), la serata e' finita presto causa attacchi di sonno vari. Nota di cronaca: fa' un freddo incredibile! :-o

