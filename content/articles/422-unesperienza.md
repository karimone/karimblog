Title: Un'esperienza
Date: 2008-02-25
Category: altro
Slug: unesperienza
Author: Karim N Gorjux
Summary: Tra qualche giorno chiuderò finalmente la mia società. E' stata un'esperienza particolare e, secondo Oscar Wilde, l'esperienza è il nome che gli uomini danno ai propri errori. Oltre ad aver sprecato un[...]

Tra qualche giorno **chiuderò finalmente la mia società**. E' stata un'esperienza particolare e, secondo Oscar Wilde, *l'esperienza è il nome che gli uomini danno ai propri errori*. Oltre ad aver **sprecato** un mucchio di soldi ho imparato delle cose che altre persone hanno pagato molto più caro di quanto abbia pagato io. 

 Chiudo, ma solo per poter ricominciare, da solo e con un'altra mentalità, con un'altra istruzione.


 La lezione più importante l'ho ricevuta da un mio amico che i soldi ha saputo guadagnarseli facendo tesoro dei propri errori: "*mai delegare collaboratori ad agire rinunciando preventivamente al chiarimento sul loro operato*" (Giovanni Agnelli)

