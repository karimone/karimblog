Title: Akropolis -Aggiornato-
Date: 2006-02-08
Category: Lituania
Slug: akropolis-aggiornato-
Author: Karim N Gorjux
Summary: Ieri pomeriggio ero a casa di Enrico per installare la ubuntu linux sul suo nuovo pc.

Ieri pomeriggio ero a casa di Enrico per installare la ubuntu linux sul suo nuovo pc. 

 Il governo Lituano rimborsa il 30% dell'acquisto di un computer se ovviamente acquistato con software originale, quindi qui i pc assemblati vengono venduti nei supermercati e nei negozi con [Baltix Linux](http://baltix.akl.lt/ "Baltix Linux") una distribuzione creata per Lituani e Lettoni. Linux costa 5 Litas (1,5€) e Windows XP Home costa circa 200 Litas (70€ circa). Boicottano Windows..  
  
Sono stato all'akropolis di Klaipeda, è pazzesco quanto sia grande questo posto, sembra l'akropolis di Vilnius anzi a dire il vero non credo che abbia niente da invidiare a Vilnius. E' impressionante quanti sono i centri commerciali in apertura e aperti in Klaipeda e il motivo è semplice: **non esistono i negozietti**.


 Sempre ieri, ho parlato per circa 2 ore via Skype con [Massimo](http://www.italialituania.com/ "Italia Lituania"), uno di quei pezzi grossi che ho conosciuto grazie al mio blog. L'argomento principale è stato lo scambio di vedute non sulla Lituania geografica, ma sulla Lituania culturare, sulla gente! Devo assolutamente precisare che non ha senso fare di un'erba un fascio, ma il 90% dei Lituani li abbiamo "centrati".


 E' un peccato che Massimo non mi abbia lasciato registrare la telefonata, perché il suo sfogo vale 1000 parole. L'Italiano in Lituania crea la sua Little Italy come il cinese crea la sua Chinatown, questa è sopravvivenza. La vita sociale al di fuori delle mure di casa è categorizzata da due tipi d'incontri:  
  
* La donna interessata: si perché le ragazze sono affascinate dallo straniero, ma non è facile ottenere un'amicizia che sia quello che è, generalmente cercano un coinvolgimento.

  
* L'uomo disinteressato: gli uomini oltre che essere grossi e sempre con ste cazzo di facce da teppista, ti guardano sempre di brutto; se poi sono in branco fai attenzione a non guardarli troppo, potrebbe dare fastidio. Succede più a Klaipeda che a Vilnius... a quanto pare.

  


 Ho anche notato che tutte le belle cose che caratterizzano la gentilezza italiana qui non esistono; su [Italietuva](http://www.italietuva.com/ "Italietuva") si tende a difendere la Lituania in tutto e per tutto, non bisogna mai parlarne male e mai dire qualcosa di negativo altrimenti passi per il solito italianotto figlio di papà che sà solo criticare. A me di quel sito c'è una cosa che non piace: "l'immagine random". Solo ed esclusivamente ragazze e per giunta belle... come se in Lituania non esistessero le cesse..


 PS: Ho rimosso il commento di **Paulus** (*Il modo in cui ti esprimi non merita commenti. Significa che non apprezzi davvero la Lituania. Ma probabilmente sei lì coi soldini che il papà ti mette sul conto corrente e i pochi spiccioli che ti passano le agenzie interinali o i conoscenti per cui lavoricchi in Italia. E dunque non sei in grado di maturare un giudizio autonomo e obiettivo sul paese di cui sei ospite, sempre ammesso che tu abbia il diritto di farlo.*). Caro, su questo blog vale tutto ciò che è descritto su questo [disclaimer](http://www.diveintomydelirium.it) che affitto, a tot al mese, dal mio caro amico Matteo. Pias nen?  
  
  
  
  
adsense  
  
  
  


