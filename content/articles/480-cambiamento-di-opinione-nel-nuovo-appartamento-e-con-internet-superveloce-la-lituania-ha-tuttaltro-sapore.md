Title: Cambiamento di opinione. Nel nuovo appartamento e con internet superveloce, la Lituania ha tutt'altro sapore
Date: 2008-09-03
Category: Lituania
Slug: cambiamento-di-opinione-nel-nuovo-appartamento-e-con-internet-superveloce-la-lituania-ha-tuttaltro-sapore
Author: Karim N Gorjux
Summary: Lo devo ammettere, devo rivisitare molti dei concetti e delle idee che mi ero fatto sulla Lituania quando ho vissuto qui. In primo luogo le basi sono totalmente diverse, prima abitavo in[...]

Lo devo ammettere, devo rivisitare molti dei concetti e delle idee che mi ero fatto sulla Lituania quando ho vissuto qui. In primo luogo le basi sono totalmente diverse, prima abitavo in un Krusciovska ora invece sono in un appartamento nuovo e questa è la **differenza basilare** che cambia il modo di vedere le cose.


 Se sei in un ambiente negativo puoi solo creare negatività ed è ben difficile riemergere se non hai un posto, un angolo dove stai bene. Ora, facendo tesoro delle mie esperienze, ho cercato un posto dove vivere che fosse il più positivo possibile, il risultato è stato ritrovarmi al diciasettesimo piano di un palazzo appena costruito a pochi passi della Klaipeda vecchia.  
  
Il prezzo è lo stesso dell'affitto che pagavo a Centallo con la differenza che a Centallo avevo l'appartamento vuoto e leggermente più grande, mentre qui è completamente arredato a nuovo, mancano solo piccole cose che aggiungerò con il tempo. La vista che ho dalla finestra è semplicemente fantastica.  
  
[![Tramonto dal mio balcone](http://farm4.static.flickr.com/3249/2819367188_e36a4a85c7_m.jpg)](http://www.flickr.com/photos/kmen-org/2819367188/ "Tramonto dal mio balcone di karimblog, su Flickr")  
  
Gli ambienti cambiano, il vicinato pure, nel Krusciovska i vicini di casa erano quella che erano e non proseguo oltre nel riesumare vecchi ricordi, dove vivo ora è totalmente diverso, vivo in un paese civile.


 Ieri mi hanno messo internet in fibra ottica che due anni fa qui nessuno sapeva cosa fosse, con **30€** al mese (senza telefono) ho una [DSL](http://it.wikipedia.org/wiki/DSL "DSL su Wikipedia") da 6mbit in download e upload di cui ho fatto due semplici test sul server di [Roma](http://www.speedtest.net/result/316656434.png "Test DSL sul server di Roma") e sul server di [New York](http://www.speedtest.net/result/316776138.png "Test DSL sul server di New York"). Dulcis in fundo, Klaipeda è completamente coperta dal Wifi. Ovunque vado il mio iPhone si connette alla rete [Zebra](http://www.zebra.lt/ "Sito in Lituano di \"Zebra\"") di cui non mi sono ancora interessato sui costi, ma lo farò presto per poter scrivere due righe quando mi trovo in giro per il paese. Ne sono cambiate di cose in due anni, vero?

