Title: Boca de Yuma
Date: 2008-03-26
Category: altro
Slug: boca-de-yuma
Author: Karim N Gorjux
Summary: La playta selvaggia non e' cosi' immediata da raggiungere, bisogna farsela a piedi fino alle barche, aspettare il "taxi" che ci porta fino altra sponda del Rio Yuma (il fiume che sfocia[...]

  
[![](http://farm4.static.flickr.com/3066/2363786389_9db82bfc5f_m.jpg)](http://www.flickr.com/photos/kmen-org/2363786389/ "photo sharing")  
  
La playta selvaggia non e' cosi' immediata da raggiungere, bisogna farsela a piedi fino alle barche, aspettare il "taxi" che ci porta fino altra sponda del Rio Yuma (il fiume che sfocia sull'Atlantico).  
  
  
  
Una volta arrivati all'altra sponda dobbiamo farcela a piedi per 1km circa pagando il pedaggio in dolci ai bambini fancazzisti che incontriamo sulla strada.  
  
  
  
La spiaggia e' bellissima e desolata, a parte le feste comandate non e´raro ritrovarsi da soli sulla spiaggia. Peccato che i Dominicani non si facciano il minimo scrupolo a buttare l'immondizia dappertutto rovinando uno dei posti piu' belli del mondo.




