Title: Ora posso dire la mia sull'Australia
Date: 2019-02-21
Category: Australia
Slug: ora-posso-dire-la-mia-sullaustralia
Author: Karim N Gorjux
Tags: Melbourne, Australia
Summary: Sono arrivato nel 2014 in Australia, ora sono ben 5 anni che vivo qui e non mi sono mai esposto piu' di tanto nel commentare la vita in questo paese.

Sono arrivato nel 2014 in Australia, ora sono ben 5 anni che vivo qui e non mi sono mai esposto piu' di tanto nel commentare la vita in questo paese.


 Ci sono un sacco di siti che parlano della vita in Australia soprattutto nel commentare e documentare le procedure per venire a vivere qui. Non e' il mio caso, io sono arrivato da cittadino e ho vissuto fin'ora come un australiano dall'accento maccheronico.


 Quali sono le piu' grandi difficolta' nel vivere in Australia, piu' precisamente a Melbourne, al di la' delle difficolta' burocratiche con l'immigrazione?

 Ora ho abbastanza esperienza per andare a fondo nelle questioni australiane, nelle difficolta' a scegliere il posto dove vivere, a trovare il lavoro, a costruire una vita sociale... insomma a creare dell'Australia la propria casa.


