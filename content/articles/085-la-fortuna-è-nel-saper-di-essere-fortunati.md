Title: La fortuna è nel saper di essere fortunati
Date: 2005-08-04
Category: altro
Slug: la-fortuna-è-nel-saper-di-essere-fortunati
Author: Karim N Gorjux
Summary: Ora che hanno aperto il primo tratto della A33, ovvero il pezzo di autostrada che porterà Cuneo alla civiltà, non ho più quel gran lavoro all'albergo. I dipendenti che lavoravano alla costruzione[...]

Ora che hanno aperto il primo tratto della A33, ovvero il pezzo di autostrada che porterà Cuneo alla civiltà, non ho più quel gran lavoro all'albergo. I dipendenti che lavoravano alla costruzione del casello mi occupavano gran parte delle stanze disponibili.  
  
La giornata tipo non era delle più allegre, non parlo di me, ma degli operai. Sveglia alle 5:45, colazione alle 6, lavoro sotto il sole fino alle 18:30 con un'unica pausa a metà giornata. Il tutto per poco più di 1000€ al mese. Ogni operaio ha una sua storia da raccontare, per quel poco che li vedevo al mattino e alla sera cercavo di parlare del più e del meno. Giuseppe è calabrese, Evaristo è napoletano, Riccardo è anche lui calabrese... il fine settimana non tornavano nemmeno dai familiari, rimanevano qui a Centallo a far passare il tempo e a chiamare la famiglia con il telefono. Per il lavoro all'autostrada sono rimasti nella zona per più di 2 mesi.


 Vedere queste realtà (per chi **vuole vederle**), mi fa pensare. Ci sono moltissime persone in Italia che lavorano in questo modo e a questi ritmi per uno stipendio da fame. L'euro è in circolazione da quasi tre anni e ha sconvolto tutto meno che gli stipendi. La maggior parte delle persone deve sudare e rinunciare a molto pur di arrivare alla fine del mese. Purtroppo non tutti vedono queste cose come le vedo io, molti sono indifferenti. Avere la fortuna di non essere in una situazione del genere, meriterebbe una consapevolezza e un rispetto di ciò che si ha. Io mi ritengo fortunato.


 Vi consiglio un film che penso molte persone dovrebbero vedere, a partire da una delle mie sorelle: [L'impero del sole](http://www.imdb.com/title/tt0092965/?fr=c2l0ZT1ha2F8dHQ9MXxmYj11fHBuPTB8cT1sJ2ltcGVybyBkZWwgc29sZXxteD0yMHxsbT01MDB8aHRtbD0x;fc=1;ft=20;fm=1). Nota: questo è il film in cui debutto al cinema [](http://www.imdb.com/name/nm0001774/)

