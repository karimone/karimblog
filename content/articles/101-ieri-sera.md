Title: Ieri sera
Date: 2005-10-07
Category: Lituania
Slug: ieri-sera
Author: Karim N Gorjux
Summary: Serata iniziata la Kurpiai dove Enrico si cimentava in baccaglio incrociato, il gruppo musicale della serata si esibiva con pezzi d'autore del calibro dei Creedence e dei Beatles. Per la gioia dei[...]

Serata iniziata la Kurpiai dove Enrico si cimentava in baccaglio incrociato, il gruppo musicale della serata si esibiva con pezzi d'autore del calibro dei Creedence e dei Beatles. Per la gioia dei più grandi ho aggiunto l'album delle foto nella mia sezione delle fotografie. Naturalmente per evitare inconvenienti bisogna registrarsi per vedere il tutto... [[Link alla gallery](http://www.kmen.org/gallery "Galleria fotografica di Kmen.org")]... se guardate bene c'è anche un bel RSS per l'album..  
  
Per chi non si vuole registrare o non gli è permesso, ecco un piccolo assaggio.. :devil\_tb:  
  
[![CIMG1326.JPG](http://www.kmen.org/wp-content/CIMG1326-tm.jpg "CIMG1326.JPG")](http://www.kmen.org/wp-content/CIMG1326.jpg)  


