Title: Che fatica!
Date: 2006-06-22
Category: Lituania
Slug: che-fatica
Author: Karim N Gorjux
Summary: Stare dietro a mio fratello è stato molto dispendioso, lui ha solo 13 anni e ovviamente non sa parlare una lingua diversa dall'italiano o dal piemontese.

Stare dietro a mio fratello è stato molto dispendioso, lui ha solo 13 anni e ovviamente non sa parlare una lingua diversa dall'italiano o dal piemontese.


   
In 10 giorni ho dovuto fargli da balia, portarlo in giro, farlo divertire. Non è stato facile. Ad un certo punto non ne potevo più, da una parte volevo che questi 10 giorni passassero velocemente, dall'altra ero contento che mi tenesse compagnia. I dieci giorni sono passati ed è tornato a casa, è la prima volta che siamo stati così tanto insieme e ne sono felice, mi dispiace che se ne sia andato, ma non potevo fare altrimenti, non ho lo spazio e nemmeno il tempo da dedicargli per tenerlo "occupato".


 Giusto per chiudere in bellezza, la sera del 20 ho iniziato ad accusare i primi sintomi della mia allergia primaverile, solo oggi mi sento un po meglio. Una nota importante: fa un caldo incredibile, ci sono più di 30 gradi ed un afa soffocante rende le giornate impossibili. [Che abbia ragione?](http://blog.wastedthoughts.net/?p=184)  
  
**Ultima nota:** la partita dell'Italia. La tv lituana ha scelto di trasmettere in diretta la partita Ghana - Usa invece di Italia - Rep. Ceca. Sto evitando qualsiasi informazione in attesa di andare a vedere la partita con Enrico al Memelis (sembra che porti fortuna! :-)  


  
  
  
iki

  technorati tags start tags: [allergia](http://www.technorati.com/tag/allergia), [calcio](http://www.technorati.com/tag/calcio), [italia](http://www.technorati.com/tag/italia), [kevin](http://www.technorati.com/tag/kevin), [klaipeda](http://www.technorati.com/tag/klaipeda), [lithuania](http://www.technorati.com/tag/lithuania), [lituania](http://www.technorati.com/tag/lituania), [meteo](http://www.technorati.com/tag/meteo), [mondiali](http://www.technorati.com/tag/mondiali), [repubblica ceca](http://www.technorati.com/tag/repubblica ceca)



  technorati tags end 

