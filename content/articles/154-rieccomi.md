Title: Rieccomi!
Date: 2006-03-05
Category: Lituania
Slug: rieccomi
Author: Karim N Gorjux
Summary: Dopo avervi scioccato con il mio ultimo post, eccomi di nuovo qui. Ho aspettato qualche giorno prima di scrivere qualche cosa di nuovo per rendere più visibile il mio precedente "annuncio".La novità[...]

Dopo avervi scioccato con il mio ultimo post, eccomi di nuovo qui. Ho aspettato qualche giorno prima di scrivere qualche cosa di nuovo per rendere più **visibile** il mio precedente "annuncio".  
  
La novità che più mi rallegra annunciare è il mio ritorno in Italia, penso che atterrerò in patria il 23 Marzo, ma non è ufficiale dato che non ho ancora il biglietto in mano. Il mio intento è di rimanere almeno un mese nella provincia Granda, per poi tornare qui a Klaipeda, e stare vicino alla neo mamma nei momenti più difficili. In altre parole non tornerò in Italia per un bel po'.


 Tornare a casa sarà bellissimo, non dovrò subirmi 7-8 nevicate al giorno come mi capita qui, non farà freddissimo e finalmente rivedrò le mie belle, bellissime montagne. *Dulcin in fundo* ho il mio bel giocattolino che mi aspetta a casa. Il mio ritorno a Klaipeda per fine Aprile mi regalerà una sorpresa non da poco: *la primavera*.


 Il mese di Febbraio è stato per me un mese particolarmente duro, pian piano credo di aver risolto parecchi problemi, tra i quali un attrito personale che si è risolto brillantemente, ma questa è un'altra storia.   
  
adsense  
  


