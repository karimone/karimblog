Title: Dodici preziosi consigli per intraprendere l'attività di imprenditore senza perdere tempo e denaro
Date: 2008-02-08
Category: altro
Slug: dodici-preziosi-consigli-per-intraprendere-lattività-di-imprenditore-senza-perdere-tempo-e-denaro
Author: Karim N Gorjux
Summary: In base alla mia esperienza, vi propongo i consigli che avrei voluto sentirmi dire prima di iniziare la mia attività. Fatemi sapere se vi sono serviti perché a me, capire queste cose,[...]

In base alla mia esperienza, vi propongo i consigli che avrei voluto sentirmi dire prima di iniziare la mia attività. Fatemi sapere se vi sono serviti perché a me, capire queste cose, è costato molto denaro, tempo e stress.  
  
 3. Informarsi prima su internet! Il forum di [Giorgio Tave](http://www.giorgiotave.it/forum/) è veramente manna dal cielo per chi vuole iniziare a fare l'imprenditore in questo strano strano paese. Leggete anche se non capite e chiedete consiglio, vi farà risparmiare molti soldi.

  
 6. La partita iva serve solo quando fatturi. Fino a 5000€ l'anno potete anche farne a meno, vi basta far fare la ritenuta d'acconto, non impelagatevi, tutta la preparazione che c'è prima la potete fare senza prendere una partita iva
  
 9. Società? No. Grazie. Almeno non da subito, io ho fatto una snc ed è un casino perché si risponde con il proprio capitale, sarebbe meglio la SRL, ma è troppo costosa per chi inizia, troppo impegnativa. Meglio una partita iva semplice senza notaio tra le balle.

  
 12. Procuratevi un buon consulente. Chi mi ha aperto gli occhi sulla mia attività è stato Alessandro "[The Business Doctor](http://www.businessdoctor.it)" ovvero l'anello mancante di qualsiasi impresa. Districarsi nel marketing è la spina dorsale della vostra attività e per farlo dovete avere più che un consulente, vi serve un amico.

  
 15. Internet è l'autostrada verso il futuro. Usatela per un buon sito orientato sui contenuti, usate le nuove tecnologie!
  
 18. Eliminate i costi! Non fate contratti business, comprate materiale usato, non firmate un cazzo di niente in giro ed evitate il superfluo!
  
 21. Attenzione all'INPS. L'Istituto Nazionale Perdita Soldi è sempre alle calcagna, sappiate che vi chiedono 2500€ l'anno anche se fatturate 1 lecca lecca ad un bambino di quinta elementare e questo per **ogni** socio!
  
 24. Non fidatevi. Soprattutto del commercialista, verificate sempre su internet o da un altro commercialista
  
 27. Crescete gradatamente. Prima fate le cose che vi chiedono meno rischi e poi valutate. Nel mio caso avrei dovuto vendere computer, ma sarebbe stato assolutamente meglio appoggiarsi ad un negozio che già ne vende e chiedere una piccola percentuale. Meno casini, almeno all'inizio.

  
 30. Evitate il contratto business del cellulare. Si scala anche con il cellulare privato, ma il commercialista figlio di buona donna certe cose non le dice.

  
 33. Attenzione alle banche! Non sempre è necessario il conto business, ma loro non ve lo diranno mai! Usate un conto a basso costo come le [poste](http://www.poste.it/bancoposta/), [fineco](http://www.fineco.it) o [mediolanum](https://www.bancamediolanum.it/). Sarebbe meglio un conto a Lugano, ma questa è un'altra storia.  
 
34. Lasciate perdere il logo. Prima fatene uno semplice, piuttosto [compratene uno](http://www.pacificwebeffects.com/store/buylogotemplates.html), ma non commissionate ad un designer il lavoro. Per fare un buon logo aspettate, con il tempo avrete le idee chiare. E poi chi l'ha detto che il [logo non cambia mai](http://www.gotomedia.com/gotoreport/october2005/images/Apple.jpg)?


 Spero di esservi stato utile, se poi vi ho fatto risparmiare vari soldi, mandatemi l'offerta "pizza margherita" cliccando il pulsante donazione in alto a destra.


