Title: Distrutto
Date: 2005-08-03
Category: altro
Slug: distrutto
Author: Karim N Gorjux
Summary: Praticamente non ho un muscolo che non mi fa male, questo è normale dopo tanta semi-inattività. La mia vecchia infezione non si fa sentire e io cerco di far bene attenzione a[...]

Praticamente non ho un muscolo che non mi fa male, questo è normale dopo tanta semi-inattività. La mia vecchia infezione non si fa sentire e io cerco di far bene attenzione a non farla riapparire. Evito tutti gli esercizi che coinvolgono le spalle in modo dubbio.  
  
Oggi inizierò a lavorare a settori, spezzando i giorni con sedute di aerobica. Ho notato, con molto piacere, che è molto più facile mangiare da cristiano quando ci si allena che quando non ci si allena. Ovviamente questo vale per me, il mangiar di meno e bene è favorito dal fatto che tutta la fatica che faccio ad ogni seduta non voglio che venga sprecata inutilmente mangiando schifezze. I chili si prendono facilmente, ma perderli è tutta un'altra storia.


 Mi viene in mente la solita discussione sulle persone che mangiano e non ingrossano a differenza di quelli come me che devono fare attenzione a qualsiasi cosa. Dalla mia parte penso di avere solo vantaggi. E' vero che viviamo in un mondo cinico e superficiale quindi cosa conta è l'estetica, ma il non essere grassi non implica per forza l'essere sani. Il mio girovita è come un allarme, se aumenta so che non sono in regola; altre persone invece possono mangiare qualsiasi schifezza, distruggere il fegato e lo stomaco senza preoccuparsi tanto. Ciò che conta è il poter mostrarsi in spiaggia senza una panza imbarazzante.


 Niente di tutto ciò ha senso, essere magri è la meta di molte pazze anoressiche, ma io voglio stare bene e soprattutto piacermi. E' solo questione di tempo, l'importante è non smettere mai e non farne una malattia. Ringraziando il cielo c'è Daniela. :-)

