Title: Uomini italiani, qualcosa di buono c'è?
Date: 2007-07-28
Category: Lituania
Slug: uomini-italiani-qualcosa-di-buono-cè
Author: Karim N Gorjux
Summary: Interessantissimo questo post, davvero interessante, lo pubblico perché merita davvero una lettura, noi italiani ci lamentiamo delle donne italiane e non solo, ma noi uomini come siamo? Articolo preso dal blog: Le[...]

Interessantissimo questo post, davvero interessante, lo pubblico perché merita davvero una lettura, noi italiani ci lamentiamo delle donne italiane e non solo, ma noi uomini come siamo? Articolo preso dal blog: [Le ragazze russe e gli italiani.](http://ragazzerusse.blogspot.com/)   


 [Bambini vestiti molto bene?](http://ragazzerusse.blogspot.com/2007/07/bambini-vestiti-molto-bene.html):  
  
  
*Pubblico il parere di un uomo italo-slavo che mi pare interessante, e attendo i commenti degli uomini italiani.   
  
"Dico la verita': figlio di madre slava e padre italiano, riconosco benissimo la solfa, pur essendo vissuto sempre in Italia. C'e' un abisso tra me (che ho avuto un'educazione "slava") e gli altri italiani. A parte la loro effeminatezza, questo curarsi come se fossero delle superstar americane, e' proprio il fatto che crescono come bambini e quando si sposano cercano una seconda mamma che badi a loro. Loro non si sposano mai, quello che fanno e' cambiare mamma. Da una all'altra.  
  
Non si battono mai, fanno i giochini da donne sul lavoro, li guardi riuniti che sembrano donne che pettegolano, hanno sempre paura di tutto e di tutti. Non ti dicono le cose davanti perche' hanno paura, pero' te le dicono dietro come le donne.  
  
Mia moglie, italiana, dice che si stupisce per la mia durezza nel pensare, pero' ammette che e' meglio una persona che si batte e quando hai bisogno c'e', e non una persona che devi sempre sostenere perche' piagnucola come avere un figlio.  
  
Gli uomini italiani all'estero sono considerati effeminati piagnucolosi e viziati , quasi dappertutto, e' inutile che vi illudete, i latinos sono gli ispanici. Che sono molto diversi dagli italiani.  
  
Io qui faccio paura ai miei colleghi, dicono che sono troppo duro nel pensare, che sono sempre pronto allo scontro frontale, ma la verita' e' loro hanno paura di tutto e tutti e vogliono una donna che calmi la loro paura.  
  
la lettera ha ragione, e non per niente gli uomini italiani restano attaccati alla mamma per 30-35 anni. E non mi dite che e' difficile andare via di casa perche' manca il lavoro, perche' nell'est il lavoro manca di piu' e gli uomini se ne vanno e a 20-23 anni hanno figli.  
  
Siete bambini con vestiti molto belli e le mani delicate di una puttana. Deboli, paurosi e fighini".*  
  


