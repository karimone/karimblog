Title: Senza difetti
Date: 2007-04-17
Category: altro
Slug: senza-difetti
Author: Karim N Gorjux
Summary: Criticare o prendere in giro le persone per i loro difetti fisici è veramente un colpo basso. Ogni persona sulla terra ha qualche difetto fisico, sia esso stempiato, brutto, sproporzionato, ciccione, rachitico,[...]

Criticare o prendere in giro le persone per i loro difetti fisici è veramente **un colpo basso**. Ogni persona sulla terra ha qualche difetto fisico, sia esso stempiato, brutto, sproporzionato, ciccione, rachitico, nano... ne abbiamo a volontà, ma purtroppo nessuno è colpevole o quasi dei propri difetti fisici.


 Criticare i "coglioni" però ha senso, perché **coglione si diventa**, non si nasce.   
Altrimenti proverei solo pena.


