Title: Dopo 60 giorni di permanenza in Lituania, le 10 cose che mi mancano dell'Italia
Date: 2008-10-22
Category: Lituania
Slug: dopo-60-giorni-di-permanenza-in-lituania-le-10-cose-che-mi-mancano-dellitalia
Author: Karim N Gorjux
Summary: Due mesi sono tanti, fatichi a ricordarti com'era la routine quotidiana in Italia, tutto sembra così lontano... Greta si è ambientata in asilo, Rita ha nuove amiche e io mi sono adattato.[...]

Due mesi sono tanti, fatichi a ricordarti com'era la routine quotidiana in Italia, tutto sembra così lontano... Greta si è ambientata in asilo, Rita ha nuove amiche e io mi sono adattato. Personalmente però, ci sono dieci cose che mi mancano dell'Italia:  
  
  
 3. **I famigliari:** genitori, fratello, sorella, zii, cugini. Mancano un po' tutti, ma fortunatamente internet rende le distanze più brevi. Il lato positivo? Quando ci si incontra, si apprezzerà di più il tempo passato insieme e poi chi l'ha detto che devono sempre aspettarmi loro in Italia?
  
 6. **Gli amici:** ci sono amici e amici, ma alcuni sono veramente particolari e mi mancano parecchio. Non che li vedessi tantissimo, ma almeno li vedevo. Ora qui diventa difficile, ma rispetto all'ultima volta che ero in Lituania ho già 2 amici in più...

  
 9. **Le montagne:** da buon cuneese la [Bisalta](http://www.bed-and-breakfast.it/foto/2825/Bisalta%20vista%20da%20Cuneo.JPG "La Bisalta") mi manca tantissimo. Nelle brutte o nelle belle giornate lei è sempre la a vigilare su Cuneo, la città degli uomini di mondo. Ovviamente non posso citare la Bisalta senza dare spazio al [Monviso](http://digilander.libero.it/varallosesia/fotopie/monviso2.JPG "Monviso"), ricordo che nelle mattinate d'inverno era possibile vedere tutta la cintura montagnosa che circonda la provincia ad occhio nudo. Affascinante.

  
 12. **Il bar: **prendere il caffè al bar non è uno spreco di soldi, ma un rito. Entri nel bar, "un caffé espresso", ti metti al bancone, prendi un semidolce, dai un'occhiata alla stampa. Che è successo in Cuneo? Solite cavolate, a volte ti chiedi come facciano a riempire il giornale.

  
 15. **La cucina di mia madre**: alla faccia degli stranieri che ci etichettano come dei mammoni, ma a me la cucina di mia madre manca. In questo periodo poi iniziano a presentarsi alla tavola la polenta, la bagna caoda, la selvaggina... sono pietanze che scaldano il cuore nelle fredde domeniche d'inverno
  
 18. **Il dialetto**: non sono un asso a parlare il mio dialetto, ma mi piace da matti sentirlo parlare. Certamente il piemontese non è divertente come il calabrese di mia nonna, ma è pur sempre il dialetto che ascolto da più di 20 anni.

  
 21. **Le partite di pallone: **giocare a calcio in Lituania è veramente difficile. Qui tutti giocano a basket, mentre in Italia riuscivamo a organizzare delle partite a calcetto memorabili.

  
 24. **L'edicola**: in Italia entravo in un edicola e tra fumetti e riviste avevo solo l'imbarazzo della scelta. Sono riuscito ad ovviare al problema facendomi direttamente mandare i Dylan Dog per posta, ma alle volte vorrei potermi passare 10 minuti tra gli scaffali per scegliermi qualcosa di nuovo
  
 27. **Il ristorante: **il tipico ristorante italiano qui non esiste. La conduzione famigliare con i piatti della casa non è nella cultura lituana. Ho ovviato dandomi da fare ai fornelli e cercando di imparare qualcosa di buono. Appena torno in Italia vado dal mio caro amico [Gerardo](http://www.ristorantearvelo.com "Ristorante Arvelo a Passatore (CN)") a mangiare qualcosa di buono
  
 30. **Gli italiani**: ma non tutti. Di per sé il popolo italiano ha delle caratteristiche che qualsiasi altro popolo ci invidia, ma ben pochi di noi hanno queste caratteristiche nella misura giusta da non essere invadenti o rumorosi.

  
  
Ora toccherebbe fare una lista delle 10 cose che assolutamente **non mi mancano**.


 PS: il caffè non mi manca perché me lo sono portato su dall'Italia ;-)  
  
[![CIMG2461](http://farm1.static.flickr.com/31/62220184_8b42f1e3e6_m.jpg)](http://www.flickr.com/photos/kmen-org/62220184/ "CIMG2461 di karimblog, su Flickr")  


