Title: Ne gavariu pa-ruski
Date: 2006-10-31
Category: Lituania
Slug: ne-gavariu-pa-ruski
Author: Karim N Gorjux
Summary: Ieri, mentre guidavo da Genova per la provincia, ho avuto l'illuminazione per qualche post interessante, purtroppo, ora che sono davanti alla tastiera, le varie illuminazioni si sono offuscate fino a spegnersi.

Ieri, mentre guidavo da Genova per la provincia, ho avuto l'illuminazione per qualche post interessante, purtroppo, ora che sono davanti alla tastiera, le varie illuminazioni si sono offuscate fino a spegnersi.


 Pago lo scotto dovuto alla mia giovane età: stare in Lituania, a Klaipeda, sapendo il perché, ma continuamente assordato dai dubbi esistenziali ha reso le mie giornate lunghe, troppo lunghe. Mi dispiace non aver avuto la forza per dare il meglio di me, ma soprattutto mi dispiace **non aver deciso** subito di cambiare una situazione che dava (quasi) nessun frutto.  
  
Una costante assoluta nel mio disagio quotidiano era **l'impotenza sociale**. Parlare inglese in Lituania è ottimo se ci vai 15 giorni da turista, ma se abiti in Lituania ti devi adattare: il lituano è una lingua difficile, arcaica e ben poco diffusa, i libri per imparare il lituano si trovano su [amazon](http://www.amazon.com) e sono tutti su base inglese. Il russo è **l'inglese dell'est**, è una lingua parlata da 200 milioni di persone e ha una buona [documentazione](http://www.assimil.it/prodotto.php?isbn=8886968108) a basso costo su base italiana. Il russo è parlato da pochi, a Klaipeda sentivo poca gente parlare russo: qualche vecchietta sul bus e il vicino di casa russo che si ostinava a parlare in russo dopo 30 anni di residenza in Lituania. Il solito nazionalismo lituano indotto dalla moglie mi ha suggerito: "**Siamo in Lituania, studia il lituano!**". Purtroppo per imparare il lituano dai libri bisogna sapere bene l'inglese e io non sono una cima, conosco un buon **inglese tecnico** che è quello usato per scrivere i libri d'informatica, ma non ho una padronanza della lingua tale da studiare una terza lingua basandomi sull'inglese. In russo invece me la cavavo, purtroppo avrei dovuto studiare (anche senza arrivare necessariamente a livelli da master in lingue magari) il russo imperterrito senza guardare in faccia nessuno. Nel bene o nel male con l'inglese e il russo puoi parlare con il 95% della popolazione baltica.


 Ritornando al discorso sull'impotenza sociale, che già in passato ho abbondantemente discusso, la mia più grande preoccupazione riguardava la bimba. Mi ritrovavo in casa a fare il **mammo** ad un esserino di appena 2 settimane, da solo, con l'enorme preoccupazione di **non sapere cosa fare** nel caso succedesse qualcosa. La suocera a 100km, la mamma a 2400km, "solo" nel condominio sovietico a sperare che la bimba non avesse dei problemi. Avevo Enrico nel caso estremo d'emergenza, ma contare su Enrico era appunto un caso straordinario, volevo tornare in Italia, volevo riassaporare la sensazione di potere che si ha nel controllare la situazione.   
In Lituania, da turista, puoi farti un paio di risate nel cercare di farti capire, ma quando hai bisogno di cose importanti che sia ordinare la pizza o parlare con il dottore riguardo la tua salute o quella di tua figlia è importante sapere come muoversi.


 Ricordo Danilo, un imprenditore che ogni tanto risiede a Klaipeda un paio di settimane per curare i suoi affari. Lui parla solamente il russo e non ha nessun problema a farsi. Avrei dovuto continuare la mia strada e, una volta entrato in Lettonia, mi sarei risparmiato la strana sensazione di sentirmi più straniero di quanto già lo fossi in Lituania.


