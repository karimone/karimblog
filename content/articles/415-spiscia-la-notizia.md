Title: Spiscia la notizia
Date: 2008-01-30
Category: altro
Slug: spiscia-la-notizia
Author: Karim N Gorjux
Summary: Striscia la Notizia questa sera ha mandato in onda il video dell'incontro Cusumano - Strano. Il servizi si divertiva a contare gli insulti di Strano, tant'è che Ezio Greggio, una volta finito[...]

Striscia la Notizia questa sera ha mandato in onda il video dell'incontro [Cusumano - Strano](http://youtube.com/watch?v=zkg8V0q4ui8). Il servizi si divertiva a contare gli insulti di Strano, tant'è che Ezio Greggio, una volta finito il servizio, si è divertito a imitare Strano indossando gli occhialini scuri e lo scialle rosso e dando sfogo alla sua abilità di comico. Una vera perla di comicità degna della prima serata.


 Ma che c'è da ridere? Bisognerebbe vestirsi tutti di nero a lutto e lasciare le bandiere a mezz'asta perché dopo ciò che è successo dovremmo essere tutti consapevoli che dell'Italia ci rimane solo il Rinascimento e il campionato di calcio. Il mettere tutto sul ridere sembra un suggerimento venuto dal figlio di dio silvio berlusconi.


 Che gente.


