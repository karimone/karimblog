Title: Klaipedoj gyvena mergaite, vardu Dolce Gabana
Date: 2006-06-08
Category: Lituania
Slug: klaipedoj-gyvena-mergaite-vardu-dolce-gabana
Author: Karim N Gorjux
Summary: Si lo so, era da un po' che non scrivevo, ma non posso far sembrare questo blog un quotidiano altrimenti mi toccherebbe essere iscritto all'albo dei giornalisti per non infrangere la legge.Aggiorniamo[...]

Si lo so, era da un po' che non scrivevo, ma non posso far sembrare questo blog un quotidiano altrimenti mi toccherebbe essere iscritto all'albo dei giornalisti per non infrangere la legge.  
Aggiorniamo il diario del capitano, data astrale [4 8 15 16 23 42](http://en.wikipedia.org/wiki/Lost_%28TV_series%29#Discredited_theories)

   
Ieri ho fatto qualcosa di fantastico che volevo fare da tempo, ma che Skype purtroppo ancora non permette. Ho finalmente un numero di presenza in Italia! In pratica ho un numero che mi permette di essere raggiungibile al costo di una chiamata urbana o interurbana se chi mi chiama non è di Torino. Tutta questa smisurata fatica e dispendio di denaro è dovuto ad aiutare chi ancora vive nell'anacronistico mondo dei cellulari e dei fax, in altre parole mia madre non spende più una fortuna a chiamarmi.  
Se volete approfittarne anche voi e ottenere un nuovo numero di telefono fisso con prefisso a scelta tra 011, 02 e 06, andate sul sito di [Messagenet](http://www.messagenet.it), il numero è **gratis**.  
Ieri sera ho passato la serata al Relax e al Kurpiai, per la gioia dei baskettari ho avuto il piacere di conoscere [Donatas](http://www.liegebasket.be/v2/newsdetails.cfm?id=509), giocatore professionista che ora milita nella serie A1 della Lega Basket Italiana. (Figlioli, ci sono più di 30 cm di differenza!!)

 [![io-e-donatas](http://www.karimblog.net/wp-content/uploads/2006/06/162672895_23206b13ed_o-tm.jpg "io-e-donatas")](http://static.flickr.com/76/162672895_23206b13ed_o.jpg)

 “[I and Donatas](http://www.flickr.com/photos/kmen-org/162672895/)” by [karimblog](http://www.flickr.com/photos/72323144@N00/)

    
Nello stesso locale dove ho incontrato Donatas, ho potuto sentire l'hit del momento in Lituana, la canzone tamarrissima dei Raketa si chiama “Dolce Gabana”, scritta esattamente così, con una b sola, forse per nazionalismo Lituano o forse per errore di copiatura, non ho ben inquadrato la storia. Il ritornello mi fa scompisciare dal ridere: *Klaidėdoj gyvena mergaitė, Vardu - Dolce Gabana.* Per chi a scuola non era attento alle lezioni di lituano: *a Klaipeda vive una ragazza di nome Dolce Gabana.* ([ascolta](http://www.melodija.lt/mp3/raketa/raketa%20dolce%20gabana.mp3)).


   
Chiudo il post rammentando l**a mia fortuna tecnologica** nella terra dei cepelinai, da quando sono approdato qui ho rotto (o si sono rotti) le seguenti cose: iBook G4, mouse logitech, hub usb, batteria del MacBook Pro, iPod Shuffle e infine iPod Nano. I miei sospetti sono caduti su la linea elettrica della casa e sono corso ai ripari comprando uno stabilizzatore di corrente, ma la domanda sorge spontanea: *perché a Rita non si è rotto mai niente?*

  technorati tags start 

 tags: [basket](http://www.technorati.com/tag/basket), [dolce gabana](http://www.technorati.com/tag/dolce gabana), [dolce gabbana](http://www.technorati.com/tag/dolce gabbana), [donatas zavackas](http://www.technorati.com/tag/donatas zavackas), [klaipeda](http://www.technorati.com/tag/klaipeda), [lithuania](http://www.technorati.com/tag/lithuania), [raketa](http://www.technorati.com/tag/raketa), [snaidero](http://www.technorati.com/tag/snaidero)

   
 technorati tags end 

