Title: In partenza per il nord
Date: 2013-12-07
Category: Lituania
Slug: in-partenza-per-il-nord
Author: Karim N Gorjux
Summary: Ci abbiamo messo un anno, ma alla fine abbiamo deciso. L'occasione all'Italia l'abbiamo data, ma con il senno di poi è stata una scelta sbagliata. Si parte di nuovo, questa volta per[...]

Ci abbiamo messo un anno, ma alla fine abbiamo deciso. L'occasione all'Italia l'abbiamo data, ma con il senno di poi è stata una scelta sbagliata. Si parte di nuovo, questa volta per il nord. La nostra meta è la Svezia, ma ho avuto un colloquio di lavoro per la Finlandia che se va a buon fine farebbe comodo per spostarmi da solo almeno i primi mesi.


 Mi spiace per l'Italia, ma più passa il tempo e più le mie speranze scemano. Sento molti dire che ci sarà "la ripresa" come se a furia di sentire "crisi" pensano che sia dettato da una legge fisica non scritta che si ritornerà come prima se non meglio.


 Io sono meno ottimista. Per me l'Italia sarà sempre sempre peggio; nella zona dove vivo inizia a sentirsi ancora poco, la ricca Cuneo regge, ma per quanto durerà non lo so e non vogliamo esserne testimoni.


 Ovviamente la partenza ci porterà in Lituania che sarà il nostro trampolino di lancio per la Scandinavia.


