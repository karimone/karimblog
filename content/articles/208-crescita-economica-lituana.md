Title: Crescita economica lituana
Date: 2006-08-30
Category: Lituania
Slug: crescita-economica-lituana
Author: Karim N Gorjux
Summary: Si vocifera che la Lituania è uno dei paesi con il più alto tasso di crescita economica europea, uno dei partecipanti del forum di italietuva ha scritto che la crescita è di[...]

Si vocifera che la Lituania è uno dei paesi con il più alto tasso di crescita economica europea, uno dei partecipanti del [forum di italietuva](http://www.italietuva.com/forum/) ha scritto che la crescita è di 6,9% (penso il pil) e che i lituani stanno tornando nel loro paese a cercare lavoro. Secondo me è un eccesso di patriottismo lituano. Sinceramente penso che per qualche anno questo paese rimarrà "in crescita" e basta, ma ne passerà davvero tanta di acqua sotto i ponti prima di vedere questo paese diventare la nuova Irlanda.


 Ormai dilaga la moda di venire a vivere qui e di trovare lavoro da queste parti. Non vi chiedo chi ve lo fa fare, perché mi ritroverei i buoi che trainano il carro a cagarmi sulla tastiera, ma voglio rassicurarvi da subito: **qui c'è ben poco da fare**. In Irlanda si parla inglese, qua richiedono il lituano e a volte non basta nemmeno quello, viene richiesto il russo e il lituano. Gli stipendi sono bassi, gli orari di lavoro sono a dir poco strani. Se avete **tanti** soldi e potete permettervi di costruire qualcosa qui e mettere qualche lituano a dirigere il tutto allora siete arrivati nel posto giusto, per chi investe è una pacchia, ma tutti gli altri comuni mortali come me possono solo venire qui ad arrancare. Spero di essere contraddetto.  
 technorati tags start tags: [lavorare](http://www.technorati.com/tag/lavorare), [lituania](http://www.technorati.com/tag/lituania), [abitare](http://www.technorati.com/tag/abitare)



  technorati tags end 

