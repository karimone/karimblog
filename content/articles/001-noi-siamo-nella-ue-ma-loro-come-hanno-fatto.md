Title: Noi siamo nella UE, ma loro come hanno fatto?
Date: 2004-08-03
Category: Lettonia
Slug: noi-siamo-nella-ue-ma-loro-come-hanno-fatto
Author: Karim N Gorjux
Summary: Sono arrivato il 1 agosto, alloggio a Sarkandaugava che significa la daugava rossa, dalgava è il nome del fiume su cui è stata fondata la città. Come si può notare dalle foto,[...]

Sono arrivato il 1 agosto, alloggio a Sarkandaugava che significa la* daugava rossa*, dalgava è il nome del fiume su cui è stata fondata la città. Come si può notare dalle foto, l'appartamento non è l'excelsior hotel a 5 stelle, ma è molto interessante viverci per un pò perchè si capiscono molte cose riguardo alla vita della gente locale.


 **Ma come diavolo hanno fatto ad entrare nella comunità europea?**

 Il complesso di edifici dove abito io ha almeno 40 anni ed è completamente lasciato in balia di se setto. Muri scrostati, scale senza luce, cavi elettrici penzolanti, sembra di vivere come 50 anni fà. Il pungo nell'occhio e' dato dall'immagine delle ragazze che escono da questi edifici, belle ed eleganti. :-)

 Tutti i turisti sono nel centro di riga, ovvero *Old Riga*, si spostano in uno zoo immaginario che parte dall Hotel Latvia ed arriva al ponte di Dalgava, tutti gli italiani vivono in questo campo immaginario. L'ambasciata italiana per eccellenza è il **Roxy** una discoteca a prezzo variabile (dipende da quanto sembrati miei compaesani), la sera gli italiani sono tutti li e le donne che sono al roxy vogliono conoscere italiani... cosi' va' il mondo.


 In periferia e' tutto diverso, molto verde, pochi turisti e cose strane.... ubriachi alle 10 del mattino, macchine che vanno dove ci sono i binari del treno! .. il far west..


