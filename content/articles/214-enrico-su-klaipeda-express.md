Title: Enrico su Klaipeda Express
Date: 2006-09-21
Category: altro
Slug: enrico-su-klaipeda-express
Author: Karim N Gorjux
Summary: Una morte improvvisa ci ha portato via un giovane studioso, pedagogo, un amico sincero e collega Enrico... Lui era lettore della cattedra della Filologia Romana della facoltà delle scienze Umanitarie a Klaipeda,[...]

  
[![](http://static.flickr.com/92/248926540_fe7b408c7e_m.jpg)](http://www.flickr.com/photos/kmen-org/248926540/ "photo sharing")  
  
*Una morte improvvisa ci ha portato via un giovane studioso, pedagogo, un amico sincero e collega Enrico... Lui era lettore della cattedra della Filologia Romana della facoltà delle scienze Umanitarie a Klaipeda, insegnante della lingua italiana.  


 Enrico Quinto è nato a Milano. Dopo aver finito il ginnasio ha studiato giurisprudenza all’Università Cattolica di Milano. Durante i suoi studi ha fatto diversi corsi estivi all’Università di Vilnius, si interessava molto della storia e delle lingue dei paesi Baltici. Ha discusso la tesi del diritto internazionale “sull’occupazione ingiusta dei paesi Baltici”. Dopo aver terminato gli studi ha svolto il servizio civile al pronto soccorso, anche lavorato come volontario al tribunale per i minori di Milano.  
  
Nel 1999 ha iniziato a lavorare in qualità di assistente all’Università di Klaipeda, nella quale, fin agli ultimi giorni della sua vita, insegnava l’italiano. Nello stesso tempo approfondiva gli studi sul Baltico, nel 2004 ha preso la laurea specialistica in linguistica e ha discusso la tesi in lingua lituana sugli “aggettivi che esprimono il gusto nelle varie lingue del mondo”.  
  
Enrico era un uomo che s’interessava di molte cose: a parte il diritto e lo studio delle lingue lui era un appassionato di poesia e musica , era membro dell’associazione degli autori ed editori, scriveva i testi e suonava strumenti a percussione nella band “Soon”, con la quale hanno registrato due album musicali.  
  
I suoi studenti lo amavano – il suo corso della lingua italiana era uno dei più frequentati. A seguire le sue lezioni venivano anche gli studenti delle altre facoltà. Dopo aver imparato la lingua, tanti di loro hanno approfondito gli studi nelle università italiane. Con le proprie capacità, Enrico faceva stupore a tutti. Ha imparato tanto bene la lingua lituana, che riusciva a correggere gli errori degli stessi lituani, le sue conoscenze della lingua inglese sono state apprezzate all’Università di Cambridge, sapeva parlare il francese, lo spagnolo, ha anche studiato il lettone, l’estone e il russo.  
  
Enrico amava la Lituania di tutto il cuore, non rimaneva indifferente alla situazione della lingua lituana, si preoccupava del destino del popolo lituano, apprezzava molto il nostro folclore e si compiaceva delle canzoni popolari. Una simpatia particolare aveva per gli abitanti di Zemaitija... A molti faceva stupore la sua voglia di vivere in Lituania, dove le giornate solari sono molto meno frequenti che nella sua patria, e la gente non è sempre tanto allegra. Ma Enrico vedeva sempre la parte più positiva della vita, era allegro, comunicativo, aveva un buon senso di umorismo. Non si è mai annoiato a Klaipeda, tanti lo incontravano nelle diverse manifestazioni culturali, oppure a fare passeggiata con il cagnolino, l’avevano visto con dei colleghi ed amici nei jazz club. Molte volte aveva fato complimenti o critiche sulla Lituania in televisione, era perspicace, saggio. Non si lamentava mai della salute e non gli piaceva rattristare nessuno.  
  
Abbiamo perso un ottimo amico, studioso... Per quest’improvvisa perdita piange tutta la comunità dell’università ed esprime sincere condoglianze ai suoi genitori, al fratello e a tutti quelli che gli erano vicini. Che la terra italiana gli sia leggera.  
  
L’addio con il defunto – 21 settembre dalle 12 alle 20 nella chiesa di S. Kazimiero, sala funebre della chiesa.


*

