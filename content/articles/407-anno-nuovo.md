Title: Anno nuovo
Date: 2008-01-01
Category: altro
Slug: anno-nuovo
Author: Karim N Gorjux
Summary: Ho passato questi ultimi giorni a lavorare sul blog come non facevo da tanto tanto tempo. Ho preso un template pulito, leggero a tre colonne e l'ho modificato a mio piacimento, di[...]

Ho passato questi ultimi giorni a lavorare sul blog come non facevo da tanto tanto tempo. Ho preso un template pulito, leggero a tre colonne e l'ho modificato a mio piacimento, di lavoro da fare ne avrei ancora, ma per ora sono più che soddisfatto. Tra le altre cose ho anche convertito i vari tags (etichette) per essere sfruttate dalla nuova versione di wordpress, il risultato sarà, spero, in una maggiore indicizzazione degli articoli del blog da parte di Google.


 Questa vacanza che definire un incubo è a dir poco un [eufemismo](http://www.demauroparavia.it/41409) ha portato anche qualcosa di buono. Premettiamo che da tutto ciò che può capitarvi c'è sempre qualcosa da imparare, lasciamo perdere chi sta peggio o chi sta meglio che è solo un fugace tentativo di mascherare la percezione della realtà, ma dalla situazione che sto vivendo sto sfruttando il possibile.


 **Mai **lasciare niente al caso, meglio ancora: **mai fidarsi delle diverse culture**. Una moglie lituana ha i suoi vantaggi e di questo [ne ho già parlato](http://www.karimblog.net/2006/10/02/le-donne-lituane-qualcosa-di-buono-ce), ma i svantaggi sono sempre in agguato dietro l'angolo e appaiono sempre quando **meno te lo aspetti**. Intanto il rapporto con i suoceri è imbarazzante, nel mio caso non c'è un punto in comune, loro parlano lituano e russo e io inglese e italiano, in pratica **non ci conosciamo** e non possiamo conoscerci. La parte più dura è di conseguenza data dalle differenti usanze e modalità di vedere le stesse cose dettata dalla nostra differente cultura; non potendo spiegare il mio punto di vista vengo a sapere da Rita cosa c'è che non va in me, nel mio comportamento o nel mio modo di essere. A differenza mia Rita non mi difende, ma tende a modificare i miei comportamenti per evitare ripercussioni con i genitori.


 Rimanere in una casa piccolina, senza un angolo di tranquillità e per così tanto tempo non è umanamente accettabile per un italiano medio, a meno che non parta già mentalmente preparati a dover affrontare uno stile di vita del genere. Se scorrete indietro ai [primi articoli](http://www.karimblog.net/2004/08/03/noi-siamo-nella-ue-ma-loro-come-hanno-fatto) di questo blog potrete leggere di una mia avventura simile nei sobborghi di Riga, allora ero da solo, senza moglie e senza figlia ed ero **preparato** a passarmi un mese del genere. (A dire il vero dopo 15gg sono scappato per vivere in centro in un appartamento decente).


 Noi italiani abbiamo dei [proverbi sull'ospitalità](http://it.wikiquote.org/wiki/Ospitalit%C3%A0) che i lituani non hanno. Loro non riescono la capire come mai io mi senta così a disagio in una promiscuità così ristretta. Per loro è normale e a quanto ho capito, hanno vissuto per degli anni in queste condizioni tanto da viverla come una consuetudine.


 Invertendo i ruoli, non riuscirei mai ad ospitare della gente in casa mia per così tanto tempo, piuttosto gli pago io l'albergo, ma la propria intimità di casa, la propria privacy non ha prezzo.


 Ho notato anche che l'onestà non è apprezzata, preferiscono vedermi apparentemente felice e contento, anche se non lo sono, che turbato e indisposto quando realmente questo è il mio umore.


