Title: Giornata autunnale
Date: 2005-05-03
Category: Lituania
Slug: giornata-autunnale
Author: Karim N Gorjux
Summary: Sono stato svegliato (come al solito) dalla luce delle prime ore del mattino. Oggi piove, ieri era una giornata bellissima, gli abitanti di Klaipeda sono tutti felici nel vedere i giardini verdi[...]

Sono stato svegliato (come al solito) dalla luce delle prime ore del mattino. Oggi piove, ieri era una giornata bellissima, gli abitanti di Klaipeda sono tutti felici nel vedere i giardini verdi e gli alberi in fiore. In effetti qui sta arrivando la primavera ed escludendo la parentesi odierna, le giornate sono calde e soprattutto lunghe. Qui il sole tramonte alle 21 circa.


 Da questo mese ho l'abbonamento al bus, alla modica cifra di 3,47 Euro posso viaggiare su tutti i bus che voglio per tutto il mese. Mi spiace non poter lavorare con il Mac come volevo, le mie intenzioni erano di mettere dei filmati in linea e fare qualche bel lavoro con le foto, ma non mi riesce niente di tutto cio' con il pc, non continuo il discorso altrimenti divento noioso e ripetitivo.


 Che succede di bello qui? Nulla. Non ho ancora fatto nulla degno di nota, mi comporto come una perfetta massaia e cerco di imparare a cucinare le difficolta' maggior sono recuperare gli ingredienti. Quasi due settimane per trovare il basilico in bustina... che oltretutto fa' schifo!

 Ora mi gusto lo yogurt delle 10:30 fragola e cioccolato :-P

