Title: Lascio il testimone
Date: 2006-10-09
Category: Lituania
Slug: lascio-il-testimone
Author: Karim N Gorjux
Summary: Sono orgoglioso di presentare il blog di Sandro, un giovane studente cuneese in erasmus a Kaunas. I suoi post sono fantastici, ho avuto il piacere di incontrarlo direttamente a Klaipeda prima della[...]

Sono orgoglioso di presentare il blog di Sandro, un giovane studente cuneese in erasmus a Kaunas. I suoi post sono fantastici, ho avuto il piacere di incontrarlo direttamente a Klaipeda prima della mia partenza e vi assicuro che è una persona regolare che ha preso la sua avventura con il giusto spirito. Penso che lui sia la persona giusta per continuare a scrivere articoli sulla Lituania su un blog, i suoi scritti si leggono di un fiato, i suoi spunti sono piacevoli e originali. Da mettere in blogroll.


 Grande Sandro!

 PS: Anche Borja non è un personaggio niente male..


 Link: [Diary of a Baltic Man](http://kaunaslife.blogspot.com/)

