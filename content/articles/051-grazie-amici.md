Title: Grazie Amici
Date: 2005-03-08
Category: altro
Slug: grazie-amici
Author: Karim N Gorjux
Summary: Questa sera volevo guardarmi un bel film, dato che non ho voglia di andare da blockbuster e che la tessera del cinemastore me la sono fottuta da tempo immemorabile, ho deciso di[...]

Questa sera volevo guardarmi un bel film, dato che non ho voglia di andare da blockbuster e che la tessera del cinemastore me la sono fottuta da tempo immemorabile, ho deciso di guardarmi uno dei miei vecchi e divertenti DVD. Ovviamente appena mi sono messo a cercare "Lo chiamavano Trinità" mi sono reso conto di averlo prestato mesi e mesi addietro a qualcuno.


 Stupido e rincoglionito come sono, non mi sono nemmeno preso la briga di segnarmi da qualche parte a chi l'ho prestato. Ora non sò proprio a chi chiedere, inutile dire che nessuno si farà mai vivo per restituirmi 30€ di DVD originali, sicuramente aspetterà abbastanza tempo da farli diventare suoi. La stessa cosa per una scatola di divx che ho prestato in giro, li stò aspettando ancora adesso...


 Che bello... niente film stasera...


