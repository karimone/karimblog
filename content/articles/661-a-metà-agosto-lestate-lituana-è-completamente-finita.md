Title: A metà Agosto l'estate Lituana è completamente finita?
Date: 2014-08-14
Category: Lituania
Slug: a-metà-agosto-lestate-lituana-è-completamente-finita
Author: Karim N Gorjux
Summary: Molte volte mi sono ritrovato a dover dare consigli su quando andare in Lituania in vacanza. Il mese migliore in assoluto è Luglio e se ti piace il mare è meglio andarci[...]

![Tramonto a Klaipeda](http://www.karimblog.net/wp-content/uploads/2014/08/Bimbi_Klaipeda-225x300.jpg)Molte volte mi sono ritrovato a dover dare consigli su quando andare in Lituania in vacanza. Il mese migliore in assoluto è Luglio e se ti piace il mare è meglio andarci durante la [Juros Svente](http://www.jurossvente.lt/ "Sito ufficiale della Juros Svente") di Klaipeda. In questi giorni ho avuto un amico in visita, siamo andati a visitare il [castello di Trakai](http://it.wikipedia.org/wiki/Trakai "Pagina Wikipedia sul castello di Trakai") che sembra uscito dal set di un film medioevale, siamo stati a Kretinga a mangiare al [Vienkemis](http://www.vienkiemis.lt/ "Pagina ufficiale del Vienkemis") che preferisco di gran lunga rispetto al suo concorrente di Palanga l'HBH. Siamo stati a Siuliai a vedere la [collina delle croci](http://it.wikipedia.org/wiki/Collina_delle_Croci "Pagina Wikipedia della collina delle croci") che ha un suo fascino prettamente religioso e non è mancata la visita a [Nida](http://www.ilturista.info/guide.php?cat1=4&cat2=32&cat3=8&lan=ita#.U-xcV4CSydw "Nida su ilturista.net").


 La Lituania ha dei bellissimi paesaggi, si mangia "strano" (per non dire pesante) e quando fa caldo si può sempre fare il bagno al lago o al mare. Riguardo al mare purtroppo non ho mai avuto affinità con la balneazione lituana. Non ho mai amato fare il bagno al lago e le volte che sono stato in spiaggia a Klaipeda era più per il paesaggio con i suoi incredibili tramonti che per fare il bagno.


 Palanga merita una menzione particolare; molti la definiscono la "Rimini Lituana" io invece preferisco definirla la "Riccione Lituana" in quanto più verosimile con le dimensioni alla cittadina baltica. D'estate Palanga è invasa dai nuovi russi (che hanno molti soldi), gli hotel hanno prezzi decisamente europei e gli "affitta-camere" sulla strada Klaipeda-Palanga si sprecano. **Personalmente non punterei mai su Palanga per una vacanza in Lituania a base di spiagge e mare.** A parte il discorso bionde che ha più di un punto a suo favore per qualcuno, io opterei ancora per l'Italia. Cibo migliore, tempo (in teoria) migliore, ma soprattutto il mare è decisamente migliore. [Tramite Venere](http://www.venere.com/it/italia/riccione/ "Pagina su Venere.com") ho potuto constatare che i prezzi degli hotel sono persino leggermente più economici rispetto a Palanga.


 Anche il tempo poi non è più come lo ricordiamo. In Italia, dalle mie parti, il mese di Luglio ha regalato ben 27 giorni di pioggia. Qui in Lituania invece iniziamo solo ora, a metà Agosto, a vedere le prime piogge prolungate dopo temperature mediterranee per più di un mese. **L'estate è finita, ma almeno abbiamo avuto la fortuna di vivercela.**

