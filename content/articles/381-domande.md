Title: Domande
Date: 2007-11-08
Category: altro
Slug: domande
Author: Karim N Gorjux
Summary: Il porsi le domande giuste è la parte più difficile. Una volta che la domanda è stata posta ed è quella giusta, allora le risposte saranno veramente utili.

Il porsi le domande giuste è la parte più difficile. Una volta che la domanda è stata posta ed è quella giusta, allora le risposte saranno veramente utili.


 "Non esistono domande indiscrete, ma solo risposte indiscrete."

