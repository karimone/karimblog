Title: Pinocchio
Date: 2006-12-08
Category: altro
Slug: pinocchio
Author: Karim N Gorjux
Summary: Mi è capitato di assistere ad un'interpretazione di Pinocchio veramente geniale, mancava solo il naso rivelatore, ma per il resto ho assistito ad un'interpretazione leggendaria.

Mi è capitato di assistere ad un'interpretazione di Pinocchio veramente geniale, mancava solo il naso rivelatore, ma per il resto ho assistito ad un'interpretazione leggendaria.


 Il tutto è nato per caso, tra una battuta e l'altra; la naturalezza e la disinvoltura con cui si è calato nella parte, a dire il vero quasi schizofrenica, è semplicemente devastante: una doccia fredda nel Sahara. 

 Me lo ricorderò per tanto tanto tempo.


