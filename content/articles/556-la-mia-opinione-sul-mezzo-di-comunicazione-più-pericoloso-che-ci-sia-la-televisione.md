Title: La mia opinione sul mezzo di comunicazione più pericoloso che ci sia: la televisione
Date: 2009-07-24
Category: altro
Slug: la-mia-opinione-sul-mezzo-di-comunicazione-più-pericoloso-che-ci-sia-la-televisione
Author: Karim N Gorjux
Summary: Tra poco è un anno che non vedo la televisione come la intende la maggior parte degli italiani. E' un anno che non vedo tette e culi su studio aperto, che non[...]

[![Uccidi la TV!](http://www.karimblog.net/wp-content/uploads/2009/07/kill-tv-300x229.jpg "Uccidi la TV!")](http://www.karimblog.net/wp-content/uploads/2009/07/kill-tv.jpg)Tra poco è un anno che non vedo la televisione come la intende la maggior parte degli italiani. E' un anno che non vedo tette e culi su studio aperto, che non vedo la pubblicità stupida e ripetitiva e che non vedo inutili programmi sul calcio. A dire il vero la televisione la guardo, ma in altro modo. Guardo euronews in italiano con la sana abitudine di premere il tasto MUTE ogni volta che iniziano gli spot. Mi capita, di rado, di guardare la televione lituana per una decina di minuti soltanto perché mi stufa in fretta e mi stanca cercare di capire le cazzate che dicono. (Hai capito bene, al di là del mio livello di comprensione, sto solo dicendo che la tv lituana è schifosa almeno quanto quella italiana).  
  
Onestamente la tv la guardo come qualsiasi altra persona, ma a differenza dell'italiano medio che l'unico strascio di democrazia che gli è rimasto è il tasto OFF del telecomando, io attacco il portatile alla televisione e mi **guardo quello che scelgo** di scaricarmi da internet. In casa guardiamo soprattutto film in varie lingue, scarico parecchi telefilm consigliati e non dagli amici che vengono mandati in onda sui network americani e poi mi aiuto con i sottotitoli in italiano di [italiansubs](http://www.italiansubs.net/index.php).


 Sai qual è la grande differenza tra la televisione e gli altri mezzi di comunicazione come i giornali, internet e i libri stessi? La televisione è un [Kalašnikov](http://it.wikipedia.org/wiki/Michail_Timofeevi%C4%8D_Kala%C5%A1nikov) puntato diritto alla tempia, guardare la televisione è come provare per un breve periodo [il morbo di Alzheimer](http://it.wikipedia.org/wiki/Alzheimer). Quando sei davanti al televisiore, una cascata di merda ti arriva dritta dentro il cervello, i battiti delle palpebre rallentano, la respirazione rallenta, il cuore inizia a rallentare e tutto inizia ad avere un senso. In questo stato di trance qualsiasi porcata che la tv emette è degna di importanza ed ha un carattere autorevole per il semplice fatto che è in tv, il senso critico del cervello si spegne per poi andare, dopo svariate ore di televisione, direttamente in pensione a tempo indefinito. Solamente così si spiega perché programmi come studio aperto sono popolari e ampiamente accettati, si capisce anche perché il calcio invece di essere odiato per quella porcata inutile che è invece viene commentato e discusso come se fosse un punto cardine ed inconfutabile dell'evoluzione umana.


 I libri e i giornali sono molto diversi. Mentre con la televisione il fiume di merda è a **senso unico** per finire direttamente con una cascata nel cervello di chi guarda, con i libri, i giornali e le riviste il senso critico del cervello, a meno che non sia già spento, non si spegne. Mentre leggi sei **obbligato a pensare** e pensando puoi valutare o meno ciò che stai leggendo; purtroppo però la televisione ha danneggiato molti cervelli, altrimenti riviste come "novella 2000" o "chi" dovrebbero essere bandite per insulto all'intelligenza di chi legge, ma invece continuano ad essere acquistate con la conseguenza di distruggere preziosi alberi solamente per alimentare la stupidità umana.


 Internet è il mezzo più democratico che l'uomo abbia mai inventato. Su internet puoi scegliere, puoi leggere e puoi guardare la televisione. Attenzione però, il senso critico deve essere sempre presente e non bisogna mai e poi **mai pensare che se è scritto su internet allora è vero**. La grande rivoluzione in internet è la direzione della comunicazione. Rispetto alla televisione che l'unica cosa che puoi fare è spegnerla, con internet puoi **prendere un poco di quella merda che ti stanno propinando per poterla restituire indietro al mittente**.


 Concludendo, io penso che la televisione sia un mezzo straordinario, ma usato male e soprattuto abusato. Leggo su facebook i commenti e le discussioni dei ragazzi della generazione dopo la mia e noto con dispiacere che **la stupidità dilaga senza limiti**. Che ne sarà dell'Italia figlia di mediaset, grande fratello, isola dei famosi e studio aperto? Saremo circondati da tanti [Lorenzo](http://www.youtube.com/watch?v=4nxa-TEHZAU)?

 Sai una cosa che veramente mi da fastidio? Entrare in casa di qualcuno e avere per tutto il tempo **un ospite indesiderato**: la televisione accesa.


 Eddie Vedder canta "Society". Il video include i lyrics.  
  
  


