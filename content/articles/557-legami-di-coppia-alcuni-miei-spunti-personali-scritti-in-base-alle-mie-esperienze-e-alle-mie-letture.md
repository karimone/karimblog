Title: Legami di coppia: alcuni miei spunti personali scritti in base alle mie esperienze e alle mie letture
Date: 2009-07-28
Category: altro
Slug: legami-di-coppia-alcuni-miei-spunti-personali-scritti-in-base-alle-mie-esperienze-e-alle-mie-letture
Author: Karim N Gorjux
Summary: Prima di sposarmi ho avuto la fortuna di avere varie avventure con ragazze di varie nazionalità, niente di trascendentale ovviamente, ma il giusto numero di ragazze perse e conquistate che qualsiasi normale[...]

[![](http://farm1.static.flickr.com/51/189713200_87a87419af_m.jpg "Una coppia e i loro pensieri")](http://www.flickr.com/photos/51788543@N00/189713200/)Prima di sposarmi ho avuto la fortuna di avere varie avventure con ragazze di varie nazionalità, niente di trascendentale ovviamente, ma il giusto numero di ragazze perse e conquistate che qualsiasi normale ragazzo può avere.  
Ripercorrendo i miei errori, mescolandoli con le mie letture ed osservandomi attorno ho potuto notare che la psicologia che lega le persone è varia ed articolata.


 Ecco alcuni miei pensieri senza un preciso ordine, nel caso avessi qualche spunto in proposito sarei davvero felice che commentassi questo articolo. Ci tengo a sottolineare che **questi spunti sono personali e quindi da prendere come tali**.   


 **La coppia che funziona** esiste e ne abbiamo esempi tutti i giorni in tutte le case. Con coppia che funziona intendo due persone che riescono a vivere la loro vita insieme, amandosi, rispettandosi e litigando in termini civili. Una coppia che funziona ha alla base l'amore per se stessi e per il partner.


 **La mezza coppia**: esistono coppie che sono create dalle proprie nevrosi e non seguendo i classici canoni dell'amore. Ho avuto modo di riscontrare che esistono uomini e donne che scelgono il partner in base a ciò che secondo loro è accessibile e quindi è legato in modo particolare all'opinione di se stessi. In pratica sono coppie basate esclusivamente sul compromesso dove lui e lei sono appoggiati l'uno con l'altro per rimanere in piedi. Nel caso in cui uno dei due inizi a stare in piedi da solo, l'altro inesorabilmente cade.


 I motivi per cui una persona sceglie un partner possono essere vai, ma purtroppo molti sono legati dall'approvazione esterna, dai canoni sociali e dalla percezione della realtà. A Cuneo, da dove vengo io, la paura del rifiuto in qualsiasi approccio con l'altro sesso ha seriamente minato la stima di se degli uomini. La paura del rifiuto nel conoscere una ragazza fa si che si cerchi di scegliere un tipo di persona con cui siamo sicuri di avere alte probabilità di successo. 

 Mi è capitato diverse volte di sentire frasi come: *"Questa è troppo bella per me"*, *"Guarda come è vestita, non guarderà mai una persona insignificante come me"*. Questo tipo di approccio porta a delle conseguenze disastrose, una scelta del partner sbagliata, una rassegnazione nei propri confronti e una vita vissuta nella mediocrità. Analogamente puoi riscontrare questo tipo di mentalità nel combattimento. Quando praticavo il [JKD](http://it.wikipedia.org/wiki/Jeet_Kune_Do) la prima cosa da evitare era di **guardarsi con gli occhi dell'avversario** e la seconda era di evitare di **pensare all'esito dell'incontro**, ma far si che l'istinto facesse il suo corso. Il punto cardine è di dare il massimo delle proprie capacità.


 **La donna trofeo**. Esistono coppie basate sulla bellezza di lei e/o sui soldi di lui. L'uomo è più legato alla bellezza da mostrare ad altri perché avere una donna bella fa diventare bello anche l'uomo o almeno questa è la linea di pensiero. La donna invece è più materialistica, preferisce pensare alla sicurezza economica e tende ad avere un uomo che le permetta di avere una vita senza troppi pensieri. La donna trofeo serve a compensare un complesso di inferiorità dove il partner è scelto secondo i dettami sociali sui canoni di bellezza. Lo stesso approccio mentale dell'uomo lo si può notare con le macchine: possedere una bella macchina alimenta l'ego.


 **La coppia d'inerzia**. Esistono alcune coppie che sono insieme per abitudine e per evitare la solitudine. Una pacifica convivenza dove i compromessi sono semplici e le giornate tranquille portano ad avere coppie di lunga data dove il motivo dell'unione si è perso nel corso del tempo. Questo tipo di coppia continua ad esistere per comodità perché la paura di rimanere soli e le difficoltà ad esse annesse portano ad adeguarsi e accontentarsi ad una vita di coppia amara.


 La paura di rimanere da soli, la scarsa stima di se e le difficoltà che si possono incontrare nel cercare un nuovo partner fan si che un uomo possa stare con una donna e accettare qualsiasi cosa, anzi in alcuni casi modificando la realtà fino a modificare la linea di pensiero e vedendo un film 

 Qualche anno fa rimasi particolarmente addolorato per una relazione vissuta male, avevo circa 23 anni, ma allora quando il tuo si concluse, ci rimasi veramente male. La paura di rimanere da solo e dover ritrovare una partner mi lasciarono decisamente amareggiato e frustrato; il problema era dovuto ad una mancanza di obiettività è soprattutto alla mancanza di felicità. Secondo me per poter amare una persona e vivere con essa, **bisogna prima essere capaci ad amarsi e vivere da soli felicemente**. Questo è importante perché quando sarai di fronte alla scelta di un partner non scenderai a compromessi solamente perché sei solo.


 Le donne sono tante, davvero tante. Perché non cercare di scegliere la migliore per te? Qualche giorno fa, parlando in chat con un mio amico dicevo: *"E' inutile ragionare con gli stereotipi della società, se devi sceglierti una partner sceglitela bella, intelligente e brava. Se ti devi legare ad una persona tutta la vita, non scendere a compromessi!"*

 Link: [Suggerimenti utili su come far funzionare il rapporto di coppia](http://www.ilmiopsicologo.it/pagine/come_far_funzionare_il_rapporto_di_coppia.aspx)

