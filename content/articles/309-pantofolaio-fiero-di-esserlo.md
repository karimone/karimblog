Title: Pantofolaio (fiero di esserlo)
Date: 2007-04-02
Category: altro
Slug: pantofolaio-fiero-di-esserlo
Author: Karim N Gorjux
Summary: Io sono un pantofolaio. Lo ammetto e non me ne vergogno.

Io sono un pantofolaio. Lo ammetto e non me ne vergogno.


 Non mi piace bere e non mi piace fumare, non sopporto le discoteche e il sabato sera lo trovo noioso e conformista, non sopporto le feste "sociali" perché mi rompo altamente le palle, ma le trovo insopportabili in **modo biunivoco**, ovvero sia quando il festeggiato sono io e sia quando il festeggiato è qualcun'altro.


 Il massimo del godimento è quando, alle 21 di un canonico sabato sera, mi trovo in pigiama sul divano con la bambina in braccio. La ciliegina sulla torta è il caffé d'orzo preparato da Rita subito dopo cena.


 Il **bello della vita** è nelle cose semplici.


