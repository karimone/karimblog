Title: La donna lituana: un mistero
Date: 2006-09-29
Category: Lituania
Slug: la-donna-lituana-un-mistero
Author: Karim N Gorjux
Summary: Vivere con una donna lituana è un vero mistero, se siete in procinto di convivere con una ragazza lituana preparatevi a cose insensate per noi italiani.I lituani sono strani, non voglio giudicarli[...]

Vivere con una donna lituana è un vero mistero, se siete in procinto di convivere con una ragazza lituana preparatevi a cose insensate per noi italiani.  
  
I lituani sono strani, non voglio giudicarli perché hanno delle [buone attenuanti](http://it.wikipedia.org/wiki/Lituania#Storia), ma per noi certe cose sono davvero incomprensibili. Mia moglie è lituana e abbiamo un figlio insieme, ho vissuto parecchio tempo qui a Klaipeda e molti comportamenti di Rita che ho ritenuto strani sono diventati normali con il passare del tempo perché ho riscontrato un comportamento simile in altre coppie italo-lituane che conosco. Ecco cosa dovete aspettarvi:  
  
 * **Lingua lituana:** la donna lituana che vive con un italiano (e penso con qualsiasi straniero) non ti parla mai lituano, il motivo penso sia semplice, non hanno voglia e pazienza di spiegarti la lingua e di soffermarsi ogni volta che devono spiegarti termini astrusi. E' molto più facile parlare inglese e po diciamoci la verità, loro il lituano non lo sanno, lo hanno solo assorbito senza studiarlo.

  
 * **Traduzioni e aiuto sociale:** semplicemente si litiga per cose che in Italia si potrebbero fare più che semplicemente, dato che la situazione più comune è che l'italiano non conosce la lingua lituana anche fare le cose più elementari come andare in banca, chiamare il provider internet o interessarsi per aggiustare la macchina diventa un problema grosso. L'unica persona che può aiutarti è la tua partner lituana ed è qui arrivano i problemi, **sbuffi, occhiate verso l'alto**, rimandamenti, scuse, alternative. L'impotenza regna nella tua vita e non saprai come uscirne fuori, la lingua richiede tempo per essere imparata e tu devi arrangiarti subito. Litigio inevitabile. **Spiegazione:** i compiti di un uomo non li puoi fare, non sei lituano e in ogni caso sei sempre uno straniero. La donna non è assolutamente educata e culturalmente preparata a **sostituirsi a te** e quindi cercherà in tutti i modi di evitare di fare i compiti che dovresti fare tu.

  
 * **Chiedere informazioni:** le lituane non chiedono in giro informazioni, mi sono capitate situazioni stranissime in cui a ruoli invertiti avrei trovato naturale chiedere informazioni per Rita, ma qui non c'è stato verso. Le donne lituane sono timide e non chiedono ai loro connazionali. Sembra pazzesco ma è così.

  
Bene, sicuramente riceverò commenti contrari a ciò che ho scritto, ma mia moglie è giovane e timida, ma certe cose proprio non le capisco. Forse qualcuno di voi ha una donna lituana che si comporta diversamente, ma io per ora ho incontrato persone nella mia stessa situazione con gli stessi problemi. Brutta storia ragazzi. Prossimo post: le note positive. E vi assicuro che ce ne sono.


