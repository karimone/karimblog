Title: Periodo di matrimoni
Date: 2007-07-07
Category: altro
Slug: periodo-di-matrimoni
Author: Karim N Gorjux
Summary: Oggi come tante, tante, anzi tantissime persone, sono invitato ad un matrimonio. Oggi è il 7 7 2007 e la gente è praticamente impazzita per questa data, tutti si vogliono sposare il[...]

Oggi come tante, tante, anzi tantissime persone, sono invitato ad un matrimonio. Oggi è il 7 7 2007 e la gente è praticamente impazzita per questa data, tutti si vogliono sposare il 7 Luglio. Tra queste persone ci sono anche i miei cugini che hanno scelto in due questa data.


 Non avendo il dono [dell'ubiquità](http://it.wikipedia.org/wiki/Bilocazione), ho dovuto scegliere per l'unica cugina da parte di mio padre che tra l'altro ha deciso questa data con più di 1 anno di anticipo. Auguri e felicitazioni, oggi rinfresco, vestito elegante, messa e ben 200 parenti/amici a cena.


 Al mio matrimonio eravamo in 4. Io e Rita, due testimoni. Non c'erano nemmeno i parenti. Il tutto è durato circa 15 minuti, dopo la cerimonia ho offerto il pranzo ai testimoni, sono tornato a casa, ho dormito e poi la sera ho offerto la cena a mio cognato. Dovere di cronaca giusto per farvi capire come mi pongo verso le feste di matrimonio.


 Auguri!

