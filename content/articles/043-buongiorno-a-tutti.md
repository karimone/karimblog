Title: Buongiorno a tutti
Date: 2005-02-03
Category: Lituania
Slug: buongiorno-a-tutti
Author: Karim N Gorjux
Summary: Mi giungono notizie che in Italia il tempo è bello, qui invece è tutto meno che bello. Il giorno del mio rientro si avvicina a malincuore, tra circa due settimane prenderò il[...]

Mi giungono notizie che in Italia il tempo è bello, qui invece è tutto meno che bello. Il giorno del mio rientro si avvicina a malincuore, tra circa due settimane prenderò il volo per Copenaghen e poi per Milano, mi dispiace, ma è inevitabile, devo tornare a casa. Il tempo passa e le cose cambiano, sono venuto qui aspettandomi nulla e me ne vado lasciano qualcosa, ma questo mi rattrista fino ad un certo punto perchè sò che posso tornare e mi rendo conto di essere fortunato di vivere in questa era moderna di computer [superpotenti](http://www.apple.com/it/macmini/) dove le distanze si accorciano, dove posso svegliarmi in Lituania e addormentarmi in Italia.


 La vita di Klaipeda mi mancherà molto soprattutto perchè sotto certi aspetti si avvicina moltissimo al mio carattere. Klaipeda è una città molto molto tranquilla e alle volte persino noiosa. Adesso che è inverno il freddo limita molto la vita mondana, ma penso che d'estate questa ridente cittadina abbia molto da offrire, in fin dei conti si trova vicino al mare e di conseguenza il clima e più che godibile rispetto all'entroterra.  
Non ho mai accennato all'isola di [Neringa](http://www.neringainfo.lt/eng_fotogalerija/index_foto.html) dove si possono godere degli spettacoli naturali incredibili, vi consiglio di guardare le foto del link e spero di potermici recare presto per pubblicare delle foto personali.


 Stasera uscirò per locali in compagnia di amici, poco fà ho accompagnato Milady alla fermata del bus e mi sembrava di essere nudo, un manto di freddo mi penetrava nelle ossa. Come al solito il clima è il più grande amico del mio ozio. Il freddo, la neve e la pioggia mi tolgono qualsiasi voglia di uscire, se poi ci aggiungiamo che ho internet in casa... :-)

