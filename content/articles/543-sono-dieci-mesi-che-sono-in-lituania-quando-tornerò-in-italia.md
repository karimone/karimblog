Title: Sono dieci mesi che sono in Lituania, quando tornerò in Italia?
Date: 2009-05-19
Category: Lituania
Slug: sono-dieci-mesi-che-sono-in-lituania-quando-tornerò-in-italia
Author: Karim N Gorjux
Summary: Non tutti i giorni, ma quasi, mi chiedo se voglio continuare a vivere qui in Lituania. Essendo l'Italia il mio paese è ovvio che mi manca e che vorrei tornarci, ma vivere[...]

[![Italia vs Lituania](http://www.karimblog.net/wp-content/uploads/2009/05/italia-lituania-225x300.jpg "Italia vs Lituania")](http://www.karimblog.net/wp-content/uploads/2009/05/italia-lituania.jpg)  
Non tutti i giorni, ma quasi, mi chiedo se voglio continuare a vivere qui in Lituania. Essendo l'Italia il mio paese è ovvio che mi manca e che vorrei tornarci, ma vivere in Lituania è ciò che voglio per il (mio) nostro futuro?

 La soluzione ideale è, a mio avviso, sfruttare i vantaggi che i due paesi offrono cercando di vivere nei mesi caldi in Lituania e nei mesi freddi in Italia; ovviamente tutto questo mantenendo la residenza fiscale e burocratica in Lituania!

 Purtroppo è una soluzione poco fattibile se hai una famiglia, mia moglie non riuscirebbe per questioni di lavoro e la bambina ha bisogno di stabilità ed un punto di riferimento.  
  
Quindi la domanda rimane senza risposta. **Dove voglio vivere?** Se fossi ricco tanto da non avere più problemi per tutta la vita, me ne andrei a vivere nelle montagne cuneesi, ma dato che non sono ricco la domanda la devo riformulare: **Qual é il posto migliore dove posso vivere con la mia famiglia?**

 Per ora la **risposta migliore è la Lituania**, non tanto per il paese in sé, ma per la nostra particolare situazione. I vantaggi non sono indifferenti: l'asilo per la bambina economico ed efficiente; l'università per mia moglie che non solo non deve pagare, ma viene persino pagata; la semplicità fiscale e burocratica per il mio lavoro; la qualità dell'accesso ad internet e, dulcis in fundo, il vantaggio del cambio Euro / Litas.


 Mi è stato più volte chiesto quando tornerò in Italia, ma al momento **non ho una risposta certa e definitiva**. In questo momento io e la mia famiglia stiamo bene qui, ci manca l'Italia, il sole, il cibo, la gente, ma la Lituania fa parte di noi e quindi rimanere qui non ci dispiace. Per ora.


 Link: [Gli emigranti italiani e il cinema](http://www.didaweb.net/mediatori/articolo.php?id_vol=832)  
Link: [Emigrazione italiana su Wikipedia](http://it.wikipedia.org/wiki/Emigranti_italiani)

