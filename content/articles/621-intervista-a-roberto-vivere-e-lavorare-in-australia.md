Title: Intervista a Roberto: vivere e lavorare in Australia
Date: 2010-10-27
Category: altro
Slug: intervista-a-roberto-vivere-e-lavorare-in-australia
Author: Karim N Gorjux
Summary: Roberto è un ragazzo italiano che si è trasferito in Australia, come italiano residente all'estero anche lui desidera far conoscere la propria esperienza nel trasferirsi dall'altra parte del mondo ed ecco quindi[...]

[![](http://www.karimblog.net/wp-content/uploads/2010/10/australia.jpg "australia")](http://www.karimblog.net/wp-content/uploads/2010/10/australia.jpg)Roberto è un ragazzo italiano che si è trasferito in Australia, come italiano residente all'estero anche lui desidera far conoscere la propria esperienza nel trasferirsi dall'altra parte del mondo ed ecco quindi la sua intervista che sono onorato di ospitare qui sul mio blog.


 Buona lettura.


  **Come ti e' venuta in mente l'idea di andare a vivere in Australia?**

 La prima volta che mi sono entusiasmato all'idea di andare a vivere in Australia e' stato circa 5 anni fa quando lessi su una rivista che in Australia "non esiste stress" e che la parola "correre" aveva l'unica eccezione di "correre, per tenersi in forma". Rimasi folgorato da quell'articolo e dato che come ogni italiano che lavora, era particolarmente sotto pressione in quel periodo. Ad ogni modo poi i casi della vita mi hanno fatto rimandare per un paio d'anni. Poi una domenica pomeriggio, un po' avvilito dalla solita routine e da una situazione senza particolari prospettive mi sono messo su internet a cercare informazioni sull'Australia, ho trovato un forum ([tripaustralia](http://www.tripaustralia.com/forumaustralia/)), ho avuto uno scambio di email con un paio di ragazzi che stavano pianificando il loro viaggio e da li' e' partito tutto...


 **Ferma tutto! In Australia non esiste stress?!?**

 Beh, questo dipende da diversi fattori naturalmente... Ma diciamo che si respira decisamente un'aria piu' rilassata rispetto alla vita a cui siamo abituati nelle città italiane. Ad ogni modo gli australiani sono particolarmente fieri di questo: la chiamano "laid-back lifestyle", traducibile con "stile di vita rilassato" o meglio ancora secondo me tradurlo letteralmente "stile di vita sdraiato sulla schiena"... rende bene l'idea!

 Quando andavo alla scuola di inglese a Sydney mi hanno raccontato questo aneddoto: un po' di tempo fa alcuni operai tedeschi vennero a lavorare in Australia nei cantieri edili, fianco a fianco con gli operai australiani; quando gli operai australiani videro che i loro colleghi tedeschi lavoravano più alacremente di quello che secondo loro era dovuto, li prendevano da parte e gli spiegavano con le buone che non era proprio il caso di lavorare in quella maniera... anche questo rende l'idea!

 Al di la' di tutto, per fare un esempio, qui non e' particolarmente rivoluzionario prendersi, che so io, un paio di mesi per farsi un bel viaggio. I contratti, per quello che ho capito, prevedono 4 settimane di ferie all'anno come da noi, ma diciamo che sono più flessibili... Ed il venerdì pomeriggio non e' considerato eversivo finire prima per andare a bersi una birra e godersi l'inizio del week-end.


 **Torniamo a noi: allora hai trovato quel forum e poi?**

 I ragazzi che avevo contattato mi hanno detto di affidarmi ad un'agenzia: Go Study Australia. Come tutti gli italiani ho storto un po' il naso... gli italiani, ho scoperto in seguito, sono un po' tutti cosi': diciamo che rispetto ad altri popoli abbiamo più accennata l'aspettativa di venire fregati e quindi per natura siamo diffidenti... inoltre, grazie ad una ricca tradizione nell'arte di arrangiarci, preferiamo fare da soli. Ad ogni modo, in quel periodo, lavoravo 40 ore a settimana e in più avevo qualche altro lavoretto da fare la sera. Non avevo tempo e ho deciso di affidarmi all'agenzia.


 In realtà ho scoperto in seguito che non ci sono costi nell'affidarsi ad un'agenzia; ad esempio io sono partito per l'Australia iscrivendomi ad una scuola di inglese; le opzioni erano essenzialmente:  
  
 3. fare da solo tutte le cartacce per i visti, permessi, assicurazioni e pagare la scuola di inglese;
  
 6. affidarmi all'agenzia e pagare la stessa cifra.

  
  
Il sistema diffuso prevede che le scuole riconoscano una commissione al procacciatore, per questo esistono queste agenzie che si occupano di cacciare fuori clienti e come servizio aggiunto li aiutano in tutte le pratiche burocratiche.


 **Ma devi iscriverti per forza ad una scuola? Non ci sono altri visti?**

 Allora...


 Il più diffuso e' il [Working Holiday Visa](http://www.gostudy.it/australia/studiare-in-australia/working-holiday-visa/), dura un anno, ti permette di studiare e lavorare (ha giusto alcuni limiti) e può essere rinnovato un altro anno a determinate condizioni (devi andare a lavorare nei campi per 3 mesi). Peccato che lo puoi fare solo se hai meno di 31 anni. Non era il mio caso.


 [Student Visa](http://www.gostudy.it/lavoro/visto-di-studio/): ti permette in teoria di lavorare fino a 20 ore a settimana e devi frequentare (almeno l'80%) la scuola a cui ti iscrivi; la scuola può essere qualsiasi cosa dall'inglese al business, dalle terapie alternative al fitness, dal corso di cuoco a quello di parrucchiere; la pratica piu' diffusa e' quella di iscriversi ad una scuola che non occupi troppo tempo e lasci liberi di lavorare (oltre quelle 20 ore che in teoria non dovresti superare).


 Poi naturalmente c'e' il visto turistico, ma ovviamente non puoi lavorare col visto turistico... beh, puoi lavorare a nero, e' possibile ma non troppo diffuso (sicuramente non come da noi).


 C'è infine qualcuno che cerca subito una sponsorship, ovvero un contratto di lavoro dipendente che ti da' il diritto di vivere in Australia e lavorare full-time. Ma non e' assolutamente facile se non si hanno contatti e i contatti te li fai venendo qui.


 **E tu, dopo un anno e mezzo, hai una sponsorship adesso?**

 No, io sono ancora in Student Visa. Per come sono fatto io non mi piace la sponsorship: l'Australia mi ha dato la possibilità di fare quello che in Italia probabilmente non sarei riuscito a fare: diventare Freelance. E mi piace la mia vita da freelance! Peccato che in questo modo devo ancora scendere a compromessi ed iscrivermi ad una scuola per ottenere una Student Visa.


 Poi ora mi piace la scuola che ho scelto: fitness!

 Fra un anno forse le cose si sbloccheranno ma per adesso mi va bene cosi': tornare a scuola e' un po' come ringiovanire di 10 anni!

 **Cosa consiglieresti a chi vorrebbe provare a venire in Australia?**

 Questo direi che e' il piano migliore:  
  
 3. Metti da parte un po' di soldi per il biglietto (sola andata: potresti decidere di prolungare piu' del previsto), per una scuola di inglese (ne avrai bisogno) e per campare qualche mese senza lavorare (la scuola di inglese e' parecchio impegnativa e non sei venuto in Australia per stressarti).

  
 6. Appena ti senti un po' a tuo agio con la lingua inizia a cercarti un lavoro nel tuo settore.

  
 9. Se non trovi lavoro nel tuo settore, troverai sicuramente un "casual job" per andare avanti e cercare qualche altra opportunità... che alla fine arriverà di sicuro!
  
  
*Roberto, un amico australiano*



