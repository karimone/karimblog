Title: Opinioni
Date: 2006-10-27
Category: Lituania
Slug: opinioni
Author: Karim N Gorjux
Summary: Ho deciso di rimuovere dello "sporco" dal blog, non ho più voglia di perdere tempo ad amministrare e a gestire il blog come se fosse un lavoro. A me scrivere diverte e[...]

Ho deciso di rimuovere dello "sporco" dal blog, non ho più voglia di perdere tempo ad amministrare e a gestire il blog come se fosse un lavoro. A me scrivere diverte e deve rimanere un divertimento.


 Ho eliminato un articolo, ma mi sento in dovere di pubblicare il commento di Max, sia per i toni pacati e sia perché puntualizza come io non sono sempre l'esponente della verità assoluta. (In fondo in fondo voi lo sapevate...)

  *boh...  
a parte le discussioni personali, che sono cosa tua, Karim mi dispiace, ma devi dirla tutta la storia, se ne parli.  
in quel thread su italietuva di cui parli, mai nessuno, e dico mai nessuno ti ha detto che venendo in italia saresti stato male. Ti è stato solo detto di non aspettarti più del dovuto, ma che visto il tuo entusiasmo ed i problemi che avevi in lituania, avresti dovuto tentare. In parole povere di andarci con i **piedi di piombo**, soprattutto in considerazione del fatto che chi come noi di Italietuva aveva già un esperienza con compagne lituane si sentiva in dovere di dirti che forse tua moglie avrebbe incontrato delle difficoltà. *

 Questo era il consiglio, vieni, ma come si dice dalle mie parti, in campana, non sono tutte rose e fiori. Io voglio fare l'avvocato del diavolo, e difendere qualcosa che sul tuo blog pare a volte indifendibile. La mia idea è che tu, preso da tanti problemi, situazioni, ecc ti sia lasciato accecare e abbia travisato quasi tutti i **consigli** che la gente del forum ti ha dato, e poichè è giusto che si sentano entrambe le campane permettimi di allegare qualcosa a questo mio post.


 Ecco alcuni messaggi, dove mi devi dire onestamente se vedi iatture, malauguri o consigli per chi inizia una nuova avventura. Il mio punto di vista è logicamente evidente, ma la storia che racconti, è cominciata cosi' (per poi degenerare) con [questi messaggi](http://www.italietuva.com/forum/Me-ne-vado-da-Klaipeda-t6331.html) alla tua comunicazione:  

> "Te lo auguro di tutto cuore, ti auguro di avere la fortuna che non ho avuto io con tante cose da quando ho fatto il grosso errore di portare mia moglie e la bambina in una nazione che **non** è in grado di assicurare più un futuro sereno quasi a nessuno, se non ai soliti noti.  
> Buona fortuna Karim e condoglianze anche a te che hai condiviso tanti momenti con il nostro povero Enrico."

 ecco sempre dallo stesso thred l'augurio consiglio che ti ha dato la persona che citi come colui che ti odia:  

> "Ti quoto caro ......, anche se la lotta è dura....chi non lotta è......  
> Ad ogni modo, caro Karim, lascia da parte questo tuo "latente" odio per la LT che non ha colpe, stanne certo. Le prove che affronterai in ITALIA sono 1000 volte più dure, perchè **le dovrai affrontare tu** anche per tua moglie e tua figlia....e stanne certo, lo dico per esperienza, alla fine rimpiangerai la lituania, e spero invece che non sia tua moglie a dover dire le cose che anche tu hai detto, nei tuoi blog o in altri post....sarà ,molto ma molto più dura! Ad iniziare dalla reicerca di un lavoro per lei...fino al trovare un asilo per Greta che ti soddisfi....che chiude in luglio e agosto e settembre (cose inammissibili in LT e che susciteranno i suoi rimbrotti), alla pasta ogni giorno (come priam fra parentesi) alla burocrazia (come prima fra parentesi) al traffico (come prima fra parentesi) al freddo (come prima fra parentesi, anche s enon ci crederai!) ecc, ecc ecc...preparati le spalle larghe e qualche chiletto di cotone per le orecchie! :-) "

 ecco l'interesse e l'augurio di una donna lituana che vive in Italia:

 
> "Dal tutto il mio cuore ti auguro la felicita per la tua famiglia in Italia. Però preparati. Sarà duro. Prima per la tua moglie. Se stara male lei,starai male tu. Se trovera lavoro-gia bene. Se trovera lavoro che piace-molto bene. Ma ricordati che le scuole sono aperte solo fino le 15.30. Attivita di alto livello per i bambini nei paesi piccoli non ci sono tranne Milano e Roma. Conoscendo le mamme lituane ti posso dire che anche per questo avrai i problemi. Noi vogliamo attivita di alto livello per i nostri figli. Mio figlio frequenta la scuola di musica.paragono sua scuola con quella che freguentavo io-e come il giorno e la notte. non e livello che mi piace,ma voglio che il mio figlio si avvicina alla musica. Quando meglio non c e-va bene cosi. Tornando al discorso di lavoro. Trovare part-time qe quasi impossibile.E chi prende il bambino dopo scuola? I nonni? Ti assicuro che qua comminceranno i problemi tra i tuoi genitori e la tua moglie. Sento quello che parlano le mamme dei bambini  
>  che hanno tanto da fare con i nonni paterni...Meglio che non ti dico. Sembra che hanno qualcosa da fare con i mostri. Ma di solito queste sono le mamme italiane. Pero le donne sono le donne. Con ....... siamo molto organizzati nella nostra vita quotidiana, ho trovato interessante e ben pagato lavoro part-time 6 ore. Finisco alle 14.30 e alle 15.30 prendo il nostro prole dalla scuola .Ho tante amiche, ho il mio hobby-canto in un coro di alto livello. Ma quello e arrivato dopo tantissimi delusioni e con tanti sacriffici. Se ti raccontassi di tutto quello che abbiamo passato potrei scrivere due romanzi. Spero che tua moglie sia donna forte. In Italia vivere e difficile. Sopratutto quando hai i figli piccoli. Molto piu difficile che in Lituania. Amare Italia ho cominciato solo dopo tre anni di mia presenza in questo paese. Con l aiuto di ....... e con la mia forza di spirito. Spero che lo succedera anche alla tua moglie. A proposito-dove trasferirete, in quale reggione?"

 come vedi tanti, tantissimi consigli, ed esperienze di vita, non iatture...


 A Gennaro vorrei invece dire che la povertà "intellettuale" o "culturale" italiana non si misura da una bandiera del comune dove ci si sposa. Io mi sono sposato in provincia di roma e c'era la bandiera del luogo dove mi sono sposato, ed il sindaco ha fatto le foto con noi e la bandiera. vogliamo dire che è giusto? o che è sbagliato? sono opinioni.


 Povera italia va detto con fatti, e io lo dico perchè è difficile per tanti viverci dignitosamente con uno stipendio, è impossibile viverci con una pensione, perchè tanti trentenni stanno ancora a casa con la mamma perchè non hanno lavoro, casa, perchè le mamme sono nonne invece che mamme, perchè la scuole fanno schifo, perchè le strade sono sporche, ed i marciapiedi impraticabili, perchè se porto mio figlio in un giardino deve giocare allo "slalom" per evitare i "ricordini" dei cani abbandonati dai padroni, perchè il popolo italiano non sa piu' cos'è l'educazione, perchè vorrei attraversare sulle striscie senza il timore di essere investito, perchè vorrei arrivare in ufficio in tempo e non con mezz'ora di ritardo a causa di scioperi, ritardi e quant'altro, perchè vorrei uscire la sera con la famiglia in centro senza il timore di essere derubato da un fattone italiano o da un clandestino, perchè.. potrei continuare per ore. hai ragione, povera italia. 

 Belle parole... poi Gennaro, sul commento che poni, ti rispondo anche che la gente ha tempo voglia ed interesse (anzi, direi piu' aveva) a consigliare Karim perchè lo ha ritenuto un compagno di forum, un amico nel forum con cui condividere dei momenti, a cui dare dei consigli affinche venendo in italia stesse bene. Karim, quando è nata la sua bambina, ne ha dato immediata notizia sul forum di Italietuva, e tutti ci siamo congratulati con lui, **ne siamo stati felici, tutti!** e [qui](http://www.italietuva.com/forum/Club-dei-papa-t6251.html) puoi convincertene.


 Ti dico che ove possibile la nostra piccola comunità ha cercato di non rimanere solo un "ente" astratto sul web, e ci sono stati incontri, raduni, birrate, e lo stesso Karim credo abbia piu volte mostrato la sua voglia di far parte della comunità.  
Purtroppo ad un certo punto qualcosa si è rotto, tra Karim e la Lituania, e tanta gente che come me ama la lituania e l'italia ha cercato di far ragionare Karim su questo, cercando di confortarlo quando è stato necessario, e di "bastonarlo" quando scriveva cose che all'occhio di noi lettori apparivano "troppo brutte"

 Io ieri sera, poi chiudo questo interminabile post, ho comunicato a mia moglie lituana che secondo una statistica della UE la lituania è all'ultimo posto della scala europea sanitaria. Lei che vive qui con me mi ha risposto: "Oggi ho chiamato per avere l'appuntamento con il dermatologo per il bambino, ce l'hanno dato per fine dicembre." Per inciso è una visita di controllo dopo un piccolo intervento...


 Ora qualcuno dirà: beh, andatevene in lituania se pensate che è meglio. Beh, sbagliato: io che vivo in italia ed amo il mio paese, non vorrei che questo succedesse qui in italia. che mi frega della Lituania? perchè devo guardare chi sta peggio (ufficialmente?) perchè dovrei accettare un invito di Karim ad andare in lituania al suo posto? Io voglio vivere bene nel mio paese, e questo lo confermo a karim di nuovo, in italia non è davvero possibile. e non è solo colpa mia, quale cittadino italiano. Ma è colpa nostra. e allora, ripeto la domanda, cosa c'è di male nel far dire ad una persona che sta ricominciando la vita daccapo come Karim: "Karim attento, qui in italia non è meglio della lituania."

