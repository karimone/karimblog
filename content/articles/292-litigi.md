Title: Litigi
Date: 2007-01-28
Category: Lituania
Slug: litigi
Author: Karim N Gorjux
Summary: I miei rapporti con gli uomini lituano-disgraziati sono sempre stati molto attivi. Si cerca sempre di capire più o meno quali sono le nostre disgrazie e come possiamo salvarci.

I miei rapporti con gli uomini lituano-disgraziati sono sempre stati molto attivi. Si cerca sempre di capire più o meno quali sono le nostre **disgrazie** e come possiamo salvarci.


 Tempo addietro avevo capito che se abiti in Lituania, le donne lituane non hanno nessuna intenzione di fare le tue veci. Non importa se sei italiano e non conosci la lingua, **devi assolutamente fare tutto ciò che farebbe un uomo lituano**. Il problema è che non è molto semplice e quindi si litiga sempre per delle cazzate. Semplici cazzate.


 Anche qui in Italia è la stessa cosa, se si litiga è per le cazzate e da un lato mi fa veramente piacere perché almeno le cazzate si gestiscono bene, ma la domanda da porsi è "come mai"? Per ora ho capito solo una cosa: i lituani in generale, sono parecchio testoni... come direbbero a Roma... **sò de coccio!**

