Title: Dave e Kevin sono qui
Date: 2006-06-12
Category: Lituania
Slug: dave-e-kevin-sono-qui
Author: Karim N Gorjux
Summary: Non è stato quello che definirei il viaggio tranquillo, ma alla fine sono arrivati. Dave e Kevin sono a Klaipeda."Dave e Kevin" by karimblog

Non è stato quello che definirei il viaggio tranquillo, ma alla fine sono arrivati. Dave e Kevin sono a Klaipeda.  
[![Dave e Kevin](http://static.flickr.com/46/165580374_19b52af666_m.jpg)](http://static.flickr.com/46/165580374_19b52af666_o.jpg)  
  
"[Dave e Kevin](http://www.flickr.com/photos/kmen-org/165580374/)" by [karimblog](http://www.flickr.com/photos/72323144@N00/)

  
  
L'aereo ha ritardato di circa 40 minuti, il problema è che il ritardo è stato sufficiente a perdere il bus che da Riga porta a Klaipeda. Dave, nella sua somma esperienza di viaggiatore baltico, ha pensato bene di andare alla stazione dei bus e prendere un mikrobusas per Liepaja in modo di arrivare abbastanza veloce per prendere il bus per Klaipeda che aveva perso, il problema è che una volta arrivati a Liepaja, non c'era nessun bus fino al giorno dopo.


 Dave mi chiama e mi spiega la situazione e visto che per l'occasione dell'arrivo di Kevin mi sono motorizzato a spese del suocero, decido con Rita di farmi questa scappatella in Lettonia e andare a prendere i due sfollati... Sfollati?! Si, perché alle 10 di sera la stazione dell'autobus chiude e quindi erano entrambi fuori su una panchina a fare compagnia ai barboni, dovevo fare in fretta perché la situazione non era delle più felici e alle 23:30 avrebbe iniziato a fare buio.


 Primo problema: non ho la carta verde. Secondo problema: la carta verde la richiedono al controllo doganale (qua ci sono, non è come al colle di tenda). Sappiamo tutti che la Lituania è strana e ne ho avuto l'ennesima conferma sabato sera. Alle 21:30 all'akropolis di Klaipeda sono riuscito a comprare una carta verde valevole per 30 giorni al prezzo di 8€. Per rendere l'idea è come se andassimo al Bennet o all'Auchan a fare la carta verde alle 21:30 di sabato sera, penso che sia praticamente impossibile farla anche di mattino, pensa a quell'ora!

 100 km dopo ero a Liepaja, la seconda città più grande della Lettonia dopo Riga (o forse la terza), in ogni caso è più grande di Klaipeda e oltre ad avere strade da schifo è anche molto vasta. Finalmente, chiedendo e provando a "naso", trovo l'orribile stazione degli autobus, Dave e Kevino sono li ad aspettarmi con le loro 3 valigie, la stanchezza di ore di viaggio e tanta voglia di andare via da quell'orribile stazione. Mi aspettavano come se fossi il Messia, ma nell'attesa non si sono annoiati, qualche barbone andava ciclicamente a salutarli ;-)

 Un'ora dopo eravamo a Klaipeda. Dave ovviamente si è adrenalinizzato per andare a fare festa, Kevin ha mangiato cena con me prima di testare il divano.


 Stasera l'Italia..


  technorati tags start tags: [bus](http://www.technorati.com/tag/bus), [dave](http://www.technorati.com/tag/dave), [kevin](http://www.technorati.com/tag/kevin), [klaipeda](http://www.technorati.com/tag/klaipeda), [latvia](http://www.technorati.com/tag/latvia), [lettonia](http://www.technorati.com/tag/lettonia), [liepaja](http://www.technorati.com/tag/liepaja), [lithuania](http://www.technorati.com/tag/lithuania), [lituania](http://www.technorati.com/tag/lituania), [mikrobusas](http://www.technorati.com/tag/mikrobusas)



  technorati tags end 

