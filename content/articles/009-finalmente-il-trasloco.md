Title: Finalmente il trasloco
Date: 2004-08-16
Category: Lettonia
Slug: finalmente-il-trasloco
Author: Karim N Gorjux
Summary: Finalmente mi sono spostato di appartamento, vivo in Arsenala iela 6 nel centro della vecchia città. Ieri sera mi sono fatto la pasta con il tonno, la cosa più semplice e buona[...]

Finalmente mi sono spostato di appartamento, vivo in *Arsenala iela 6* nel centro della vecchia città. Ieri sera mi sono fatto la pasta con il tonno, la cosa più semplice e buona che abbia mai mangiato nelle ultime due settimante. Sono anche un pò malato, nel vecchio appartamento ho preso parecchio freddo, da notare che Svetlana, la ragazza che mi ha dato l'appartamento e' inaffidabile quanto la sua bellezza. Sempre in ritardo! Eppure mica la pago con dei sorrisi! :-D

 Sono andato al cinema, *il forum cinema*! Un complesso molto carino che offre delle sale all'avanguardia. (Meglio dei nostri cinelandia). Ho visto *"spiderman 2"*. Lingua originale sottotitolato in russo e in lettone. Comprensione del fim: 40%  
Prezzo per 2 persone: 4.90 lat ovvero 7.53€

 **Qualche nota interessante** dopo i miei primi 15 giorni in lettonia (da solo)... Ricordatevi di portarvi la moka del caffè se venite qui. Un consiglio molto prezioso, il caffè si trova al supermarket. Ho notato che se uscite una ragazza lei non pagherà mai niente. Nemmeno per sè. Solo una volta una mi ha pagato il gelato una mia amica (0.19 lat) quindi preparatevi a sborsare... il mio consiglio? Mangiate sempre a casa e se dovete uscire andate al [Lido](http://www.rigalatvia.net/lido/) dove mangerete cibo locale ed economico purtroppo con molto aglio e cipolla quindi non dimenticate di comprare i chewing gum.


