Title: karimblog in lutto
Date: 2006-11-15
Category: altro
Slug: karimblog-in-lutto
Author: Karim N Gorjux
Summary: Sono tutte cose che succedono in fretta: l'influenza, il trasloco e infine, ma molto più grave, la scomparsa della mia unica nonna. Capirete quindi che per qualche giorno il blog non sarà[...]

Sono tutte cose che succedono in fretta: l'influenza, il trasloco e infine, ma molto più grave, la scomparsa della mia unica nonna. Capirete quindi che per qualche giorno il blog non sarà più aggiornato, portate pazienza... tornerò presto.


