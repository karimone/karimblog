Title: Buon Lunedì a tutti quanti
Date: 2005-10-03
Category: Lituania
Slug: buon-lunedì-a-tutti-quanti
Author: Karim N Gorjux
Summary: Girovagando nella rete ho scoperto che posso leggermi 30 numeri della stampa a 16€ inclusa la sezione di Cuneo. Il costo di ogni stampa si aggirerebbe sui 0,53€. Oltre che costare quasi[...]

Girovagando nella rete ho scoperto che posso leggermi 30 numeri della stampa a 16€ inclusa la sezione di Cuneo. Il costo di ogni stampa si aggirerebbe sui 0,53€. Oltre che costare quasi la metà della versione cartacea si risparmia l'abbattimento di qualche albero.  
  
Il mio dilemma è che non so se spendere 16€ per leggere 30 numeri quando voglio fare l'abbonamento a 3 mesi di stampa al costo di 24€ che sono 0,27€ a quotidiano. Sono indeciso per il semplice fatto che non ho idea di come sarà leggersi la stampa su computer. A casa quando leggo sono sul divano, a tavola, al cesso... ok che ho il portatile, ma non è la stessa cosa. **Sono graditi suggerimenti** ;-)

 Ho ricevuto una mail straordinaria da uno dei personaggi che ha l'onore di trovarsi nella mia lista dei links, la mail è carica di energia e sincerità. Mi è piaciuta, l'ho ringraziato in privato e lo ringrazio anche qui, è uno dei personaggi più particolari che conosco in questo momento, da non sottovalutare.


 Fatemi sapere cosa ne pensate dell'eclissi solare anulare che dovrebbe essere visibile sopra la vostra testa in questo preciso momento, ma soprattutto fatemi qualche commento riguardo alla barra laterale. Ho aggiunto alcune informazioni riguardo al meteo di Klaipeda... poteri del php.


