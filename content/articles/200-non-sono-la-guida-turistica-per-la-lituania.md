Title: Non sono la guida turistica per la Lituania
Date: 2006-08-17
Category: Lituania
Slug: non-sono-la-guida-turistica-per-la-lituania
Author: Karim N Gorjux
Summary: Non fatevi prendere troppo dalle mie parole, la Lituania è un bellissimo paese se ci venite da turisti, si mangia bene, ci sono dei bei posti, si vedono delle belle ragazze. Insomma,[...]

Non fatevi prendere troppo dalle mie parole, la Lituania è un bellissimo paese se ci venite da turisti, si mangia bene, ci sono dei bei posti, si vedono delle belle ragazze. Insomma, il paesaggio è bello e l'estate è fresca. E' un paradiso.


 Soltanto che io mi rompo le balle a scoprire il **popolo lituano** che è tutt'altra cosa, forse pretendo troppo paragonando continuamente la gente di qui con gli Italiani, ma non dovete leggere tutto ciò che scrivo come oro colato. Le mie sono solo impressioni, scazzi di prima mattina e sfoghi serali.


 Se siete degli arditi sostenitori della Lituania come paradiso terrestre, non ve la prendete, sono d'accordo.  
 technorati tags start tags: [lituania](http://www.technorati.com/tag/lituania), [lituano](http://www.technorati.com/tag/lituano), [paradiso](http://www.technorati.com/tag/paradiso), [estate](http://www.technorati.com/tag/estate), [turismo](http://www.technorati.com/tag/turismo)



  technorati tags end 

