Title: La mia opinione
Date: 2004-08-24
Category: Lettonia
Slug: la-mia-opinione
Author: Karim N Gorjux
Summary: Ormai sono circa 20 giorni che mi trovo nella Parigi del baltico: Riga. Città fondata nel 1201 è uno dei borghi medioevali piu' vecchi d'europa ed e' anche la capitale piu' popolata[...]

Ormai sono circa 20 giorni che mi trovo nella Parigi del baltico: Riga. Città fondata nel 1201 è uno dei borghi medioevali piu' vecchi d'europa ed e' anche la capitale piu' popolata delle repubbliche baltiche (circa 1 milione di abitanti). Ecco i miei punti di vista dopo 21 giorni qui e 2 viaggi precedenti.


 Tutti vengono qui per le donne, sono belle bionde forse dotate di cervello e disponibili, ho in precedenza raccontato i miti e le leggende delle ragazze locali, ma ora provo a dare un'idea piu' precisa. Prima regola: *non innamorarsi*. Volete sposare una lettone o una russa procedete in questo modo:

   
 * Studiate il russo
  
 * Prendete un bel po' di soldi
  
 * Venite ad abitare qui per almeno un'anno
  
 * Vivete con loro non tra il turismo e in mezzo alla bambagia
  
 * Trovatevi qualcosa da fare
  


 E' l'unico modo per poter capire come funzionano le cose da queste parti. L'italiano nel 99% dei casi viene fino qui a Riga, frequenta solo old riga e i locali annessi, si fa' addescare da una bionda credendola di averla sedotta e perde la testa.


 Ricapitoliamo la vita dell'italiano turista a Riga:  
  
 3. Si organizza in branco di 3/6 persone 
  
 6. Affita appartamento nella Old Riga 
  
 9. Si gira tutte le discoteche di Old Riga frequentando giovani ragazze piacenti 
  
 12. Paga cene, discoteche alle suddette donne 
  
 15. Se ne torna a casa 
  


 Tra i punti 3-4 nel tempo libero senza la ragazza piacente l'italiano si butta nell'approccio per strada con scarsi risultati, ma ovviamente nessuno publicizza questi fatti spiacevoli.


 La ragazza lettone/russa (dai 18 anni ai 30) entra in un circolo vizioso dei seguenti punti:  
  
 3. Va' in discoteca alla ricerca dell'italiano
  
 6. Sorride all'italiano in discoteca
  
 9. Segue l'italiano nel suo appartamento
  
 12. Frequenta l'italiano durante la sua vacanza e si fa' pagare cene e discoteche in cambio di...

  
 15. ...sano sesso sicuro
  
 18. saluta l'italiano che se ne torna in Italia
  
 21. Torna al punto 1
  


 Come vi ho gia' spiegato se andate a vedere dove vivono queste ragazze vi rendete conto del perche' vivono questa vita. Ve la sentite di giudicarle?

 Ieri sera sono stato al Roxy, Kristina doveva andare a casa quindi mi sono preso la serata libera. Pioveva, ho passeggiato per la via centrale dove ho visto il mac donald chiuso con le striscie della polizia attorno all'edifico. Hanno ammazzato qualcuno? Tutta la piazza era piena di poliziotti.. boh! Dopo 15 minuti non c'era piu' nessuno e il mac donald era tornato nella piu' banale delle serate. Paese strano questo.   
Entro al Roxy alle 10:30, a differenza dell'Italia qui le discoteche aprono alla stessa ora degli anni '80. Biglietto: 5 lat (foreigner tax included), dopo essermi guardato in giro e aver individuato il 90% di ragazze come le sopra descritte faccio la conoscenza con una bionda (lettone) che spero faccia parte del 10% rimanente. Mi racconta un po' della sua vita e della sua fresca rottura con il boyfriend che guarda un po' dopo 10 minuti si presenta ubriaco al nostro tavolino bestemmiando in lettone contro di me e la ex ragazza. Qualcosa di lettone lo capisco e il russo pure, io me ne stavo zitto e non e' successo niente di che, il tipo se ne' andato e io dopo la chiacchierata pure. Meglio evitare problemi quando giochi fuori casa e in piu' sei da solo. Ti puoi risvegliare per la strada senza sapere a chi dire grazie.


 Ho seguito un caso di approcciamento in discoteca da parte di un ragazzo locale ad una ragazza locale (non sono un guardone!). Sala da ballo, il tipo si avvicina, le balla assieme, la tocca (non le orecchie e il naso)... effusioni amorose su un tavolino. Durata del tutto: 4 minuti.


 Penso di essere uno dei pochi italiani ad avere amici maschi in Lettonia, un paio di amici locali e altri arabi che vivono qui dall'epoca del big bang (grazie Mamma-Ghi per il nome arabo, non sapete quanto mi e' utile), ho raccontato un po' le mie idee e le mie opinioni e mi hanno fatto i complimenti per quante cose so' del posto (pavoneggio). Sintetizzo:  
  
 * La poverta' e' piu' diffusa dell'aria
  
 * La prostituzione e' diffusa quanto la poverta' e per prostituizione intendo non quella da marciapiede che qui non esiste
  
 * Nelle discoteche le donne ti scelgono e non sono loro a scegliere te
Se vuoi stare con una donna del posto devi vivere nel posto  
 * Le ragazze migliori si incontrano al supermarket, nei centri commerciali, nel bus e nella vita normale..

  
 * Le ragazze perbene non vanno sempre in discoteca, ma solo ogni tanto
  
 * Le ragazze perbene si notano subito :-P
  
 * Le donne sfioriscono ad una velocita' incredibile
  
 * L'italia e' molto piu' bella della Lettonia
  
 * La Lettonia **non** è old riga
  
  
Per oggi e' tutto, ho altre cose da raccontare, ho conosciuto un'italiano che ha un'attivita' qui dal '96. Molte cose che mi ha detto confermano cio' che ho gia' scritto e non ho scritto.


