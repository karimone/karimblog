Title: Sono stato influenzato, sono stato all'ospedale e ora sono di nuovo influenzato
Date: 2010-12-18
Category: Lituania
Slug: sono-stato-influenzato-sono-stato-allospedale-e-ora-sono-di-nuovo-influenzato
Author: Karim N Gorjux
Summary: Sono tre settimane che sono malato, la prima settimana, dopo una partita a calcio a -10 gradi centigradi, ho preso una bella influenza. Finita l'influenza ho iniziato ad avere dei fastidi sotto[...]

Sono tre settimane che sono malato, la prima settimana, dopo una partita a calcio a -10 gradi centigradi, ho preso una bella influenza. Finita l'influenza ho iniziato ad avere dei fastidi sotto l'ombelico, subito non ci ho dato peso, ma dopo due giorni ho avuto talmente male che Rita mi ha portato all'ospedale dovo sono rimasto cinque giorni.  
Esco dall'ospedale passando da una temperatura di 30 gradi nella stanza a -11 e cosa succede? La sera ho la febbre a 39.


 Tra i tanti lati positivi c'è da dire che ora ho l'esperienza diretta della sanità lituana da poter condividere con voi sul blog, datemi il tempo di riprendermi e scrivo.


 PS: Rita è stata intervistata oggi su [l'Altra Europa ](http://www.radio24.ilsole24ore.com/main.php?dirprog=L%27Altra%20Europa) di Federico Taddia.


