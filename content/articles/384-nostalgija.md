Title: Nostalgija
Date: 2007-11-15
Category: Lituania
Slug: nostalgija
Author: Karim N Gorjux
Summary: Bello il post di Henry (che linkerò tra poco), in effetti c'è qualcosa della Lituania che una volta che te ne vai ti manca, sicuramente viverci è difficile e stressante. La lingua[...]

Bello il post di Henry (che linkerò tra poco), in effetti c'è qualcosa della Lituania che una volta che te ne vai ti manca, sicuramente viverci è difficile e stressante. La lingua è sempre lo scoglio più difficile da superare. Ma c'è qualcosa che ti manca, anche Flavio ha provato una sensazione del genere.


  
Il commento che ho lasciato nel [post di Henry](http://www.henryblog.net/2007/11/14/subterranean-homesick-alien), potrà essere usato contro di me in tribunale. (lol)

  
In Kalku jela a Riga esiste un bar di tipico stampo ottocentesco che si chiama Nostalgija. Non so perché mi viene sempre in mente quel bar.


  


