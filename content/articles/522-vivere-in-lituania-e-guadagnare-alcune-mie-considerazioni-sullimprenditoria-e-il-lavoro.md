Title: Vivere in Lituania e guadagnare: alcune mie considerazioni sull'imprenditoria e il lavoro
Date: 2009-02-19
Category: Lituania
Slug: vivere-in-lituania-e-guadagnare-alcune-mie-considerazioni-sullimprenditoria-e-il-lavoro
Author: Karim N Gorjux
Summary: Sono tante le persone che mi contattano dal blog per sapere come vivere qui in Lituania. Il problema è sempre il solito, se vengo a vivere in Lituania, cosa faccio? come mi[...]

[![](http://farm3.static.flickr.com/2198/2223003068_4c1c3021eb_m.jpg "Money Island")](http://www.flickr.com/photos/combinedmedia/2223003068/)Sono tante le persone che mi contattano dal blog per sapere come vivere qui in Lituania. Il problema è sempre il solito, se vengo a vivere in Lituania, cosa faccio? come mi mantengo? Come me la cavo?

 Ho sentito molte storie leggendarie di emigrati italiani in Lituania motivati dalla rivoluzionaria idea di esportare il prosciutto, l'olio di qualità e la pizza fatta con il forno a legna, ma ne sono rimasti davvero pochi. Molti imprenditori dalle idee rivoluzionarie si sono mangiati i loro prosciutti, bevuti il loro olio e se ne sono tornati a casa quando avevano i soldi per comprare il biglietto di ritorno. Sono [veramente i pochi](http://www.italian-shop.net/) che hanno concluso qualcosa.  
  
La motivazione è sempre la stessa: due occhi azzurri contornati da uno splendido corpo di femmina baltica. A volte dopo un erasmus, a volte dopo una vacanza nei baltici e un incontro fortuito, alle volte semplicemente perché in Italia si è conosciuti una lituana sia in modo reale che in modo virtuale.


 Bene, dato che la motivazione è alta (mediamente 175 cm), si tende a vedere tutto con i prosciutti imprenditoriali ben appiccicati sopra gli occhi. Aprire un ristorante italiano in Lituania? Cercare lavoro in Lituania? Importare i formaggi?

 E' possibile cercare lavoro in Lituania ed è anche possibile ottenerne uno. **La paga minima è di 230€ (800 litas)** ed è necessario almeno sapere il lituano se non anche il russo. Considerando la difficoltà della vita in Lituania e le differenze rispetto all'Italia penso che sia il caso **riflettere obiettivamente** prima di scegliere di intraprendere questa strada.


 Se avete un'idea imprenditoriale, è meglio che la valutiate con molta attenzione. C'è gente che si siede al Cili Pica e schifato nel vedere una pizza meno mediocre, coperta di maionese e ketch-up, **ha l'illuminante idea di importare la "Bella Napoli"** a Vilnius o a Klaipeda. Sarà sicuramente un successo dico io, ma per il momento hanno chiuso in tanti. A Klaipeda ho già visto vari ristoranti italiani chiudere alla stessa velocità con cui hanno aperto...come uno dei tanti ristoranti di Bergamo, gestito da una coppia che si è traferita dal nord Italia un anno fà, che sembrava andare a gonfie vele. Il problema? Costo del prodotto finale e apprezzamento della qualità dello stesso da parte dei clienti. I lituani mangiano i loro cibi da decenni, fargli cambiare idea al doppio del prezzo di un piatto tipico locale è una scelta discutibile che per ora non ha avuto successo.


 E che dire di tutti gli altri tipici prodotti italiani che noi amiamo tanto? Il gelato, il prosciutto, l'olio, i formaggi... beh.. nessuno ne sente l'esigenza e se c'è un'esigenza da soddisfare **la risposta si chiama [Maxima](http://www.maxima.lt/)** dove si trova tutto, costa poco e nessuno si lamenta.


 Il problema principale è che si cerca di vendere ai lituani che sono persone poco abituate al marketing, che non hanno soldi da spendere e che difficilmente si fidano degli stranieri. Il made in Italy riscuote ancora successo nei mobili e nei vestiti, ma è un mercato ben gestito dai soliti noti.


 A mio parere c'è ancora modo di fare dei soldi in Lituania, ma usando uno dei principi che ha mandato in crisi economica il mondo, ovvero sfruttare la manodopera lituana per vendere nel resto del mondo. In pratica bisogna invertire la freccia: non dall'Italia alla Lituania, ma dalla Lituania all'Italia (o Europa).


 A Klaipeda oltre ad un porto ben attrezzato, esiste la [FEZ](http://www.fez.lt/) ovvero la Free Economic Zone che offre notevoli vantaggi a chi vuole investire oltre il milione di euro. Senza arrivare a certe cifre, i vantaggi e le semplicità fiscali rispetto all'Italia possono garantire un basso costo di produzione e una buona competitività in altri paesi. Siamo o non siamo nell'Europa Unita?

 Non sono solo vantaggi. Entrare in Lituania e lavorare con la Lituania richiede una grande dose di pazienza, dei **buoni collaboratori**, conoscenza della lingua, **persone fidate** e un portafoglio discretamente pieno. Come ogni attività imprenditoriale bisogna prendere le giuste cautele (e avere fiuto).


 Nel mio piccolo me la sto cavando con [la mia impresa](http://www.gorjux.net) online e appoggiandomi ad un [ottimo consulente di marketing](http://www.businessdoctor.it). Per ora funziona.


 Concludo dando qualche consiglio basato sulla mia esperienza personale:  
  
2. Non partite "sperando", c'è chi viene in Lituania con 2-3 mesi di stipendio sperando di cavare qualcosa dal buco entro lo svuotamento del portafoglio. **Iniziate a pianificare già in Italia** qualcosa, sfruttate la rete!


 - Evitate di fare i professori d'italiano. Ormai è un mercato inflazionato e l'italiano non è così richiesto se paragonato allo spagnolo.



 - Imparate la lingua. Se potete **datevi da fare per imparare il lituano**. Commercialmente è più indicato il russo, ma la Lituania ha una forte identità nazionalista e il russo non è così diffuso (o meglio "tollerato") come in Lettonia. (Riguardo l'Estonia non mi pronuncio, non la conosco)


 - Abbiate pazienza. La Lituania ha una sua storia e una sua cultura molto particolari. Non aspettatevi l'ospitalità siciliana, adattatevi senza dannare il paese o perdere tempo a giudicare ogni cosa. **Be water my friend**
  


