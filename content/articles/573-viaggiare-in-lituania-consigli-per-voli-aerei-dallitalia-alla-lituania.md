Title: Viaggiare in Lituania: consigli per voli aerei dall'Italia alla Lituania
Date: 2009-11-13
Category: Lituania
Slug: viaggiare-in-lituania-consigli-per-voli-aerei-dallitalia-alla-lituania
Author: Karim N Gorjux
Summary: Dopo l’avvento della crisi economica globale del 2008/2009, sono stati soppressi alcuni voli strategici e molte offerte di voli a Roma, tra cui quello diretto Roma-Vilnius, fornito da Airbaltic. A dire il[...]

[![Ragazze FlyLal](http://www.karimblog.net/wp-content/uploads/2009/11/flylal-300x225.jpg "Ragazze FlyLal")](http://www.karimblog.net/wp-content/uploads/2009/11/flylal.jpg)Dopo l’avvento della crisi economica globale del 2008/2009, sono stati soppressi alcuni voli strategici e molte offerte di voli a Roma, tra cui quello diretto Roma-Vilnius, fornito da Airbaltic. A dire il vero il collegamento era garantito anche dalla FlyLal, scalcinata compagnia di bandiera low cost, che è fallita, creando non pochi problemi per i collegamenti ai Lituani stessi.   
  
Pare che addirittura l’ex presidente della Repubblica Valdas Adamkus abbia dovuto rinunciare, per un contrattempo, ad un importante incontro internazionale.


 **Per chi parte da Roma, c'è la tratta Roma - Riga**, con collegamento a Vilnius, a volte però, risulta più agevole percorrere il tragitto Riga - Vilnius o Riga - Palanga direttamente in minibus, pullman o treno.  
**  
Allo stesso modo è possibile partire da Milano per arrivare a Riga** e poi entrare in Lituania con i mezzi già citati. In particolare la [Eurolines](http://www.eurolines.it/default.htm) offre un servizio economico e di alta qualità, sul pullman moderno e confortevole è persino possibile accedere ad internet tramite il wifi.  
   
**[Ryanair](http://www.ryanair.it) permette di costruirci un doppio scalo sfruttando la rotta Londra Stansted - Kaunas.** Sfruttando questa rotta, è possibile partire da molti aeroporti italiani e programmare la coincidenza per Kaunas. Gli aeroporti attualmente collegati a Londra Stansted sono: Brindisi, Cagliari, Cuneo, Lamezia, Bergamo, Lamezia, Milano, Olbia, Palermo, Parma, Perugia, Pescara, Pisa, Roma Ciampino, Rimini, Torino, aeroporto Trieste-Treviso, aeroporto Verona-Brescia.  
   
**Se scegliete la strada dello scalo con Ryanair, fate bene attenzione!** Ryanair non gestisce le coincidenze quindi sarete costretti a ripetere il checkin. Calcolate bene di avere il tempo per recuperare il bagaglio. Se scegliete questa strada è possibile che sarete costretti ad aspettare varie ore a Londra. Portatevi il bagaglio a mano e basta!  
   
**Con lo stesso principio, sempre con Ryanair, è possibile sfruttare la tratta Francoforte - Kaunas** e quindi partire dai vari aeroporti italiani che hanno la rotta con Francoforte.


 **Un'altra soluzione la offre la Czech Airlines** che a volte da la possibilità di fare uno scalo a Praga e di prendere un altro volo per Vilnius. Le partenze sono sempre da Roma e Milano.


 Per chi abita nel nord-ovest, **consiglio di valutare l'aeroporto di Nizza** perché ha un volo diretto con Riga.


 **I prezzi dei biglietti per andata e ritorno si attestano, in genere, attorno ai 300 euro,** se si ci affida alle compagnie low-cost. Valgono alcuni accorgimenti universali: è conveniente comprare i biglietti via Internet anziché all’aeroporto perché si risparmia, e se si prenota con largo anticipo, il prezzo cala. Inoltre consiglio di tenere d'occhio il sito della [Air Baltic](http://www.airbaltic.com) che spesso vende direttamente dal suo sito biglietti aerei fortemente scontati.


 **Alcune considerazioni mi sembrano doverose.** Se è vero che la crisi economica ha colpito dappertutto, non avrebbe certo mandato in dissesto le casse nazionali ripristinare, con finanziamenti statali appositi e in accordo con le compagnie aeree disponibili, i collegamenti strategici tra Vilnius e altre capitali e città europee.


 Tale operazione infatti non è da vedere solo come una spesa, ma come un investimento: **da sempre lo sviluppo del commercio passa per le vie di comunicazione, e i porti, le strade e gli aeroporti sono determinanti**.  
**  
E’ secondo me di una sciatteria allarmante la scelta dei politici Lituani di non fare nulla in merito: **il 2009, ormai agli sgoccioli, è l’anno di Vilnius, capitale europea della cultura. Da sottolineare anche che il turismo nella piccola repubblica baltica, in aumento, non è certo stato incoraggiato da questi disagi. Inoltre tale situazione dirotta i turisti italiani verso Riga, dove persistono alcuni collegamenti diretti, sottraendo turisti, almeno in parte, alla Lituania, in favore della vicina Lettonia (anche se la distanza tra Vilnius e Riga non è grandissima).


 **Se conosci altre tratte tra l'Italia e la Lituania che non sono qui menzionate, segnalacelo nei commenti!**

