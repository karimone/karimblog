Title: Regalo di compleanno
Date: 2006-11-07
Category: altro
Slug: regalo-di-compleanno
Author: Karim N Gorjux
Summary: Il 9 Novembre compirò ben 27 anni, mia madre direbbe buttati via, ma io ho un'opinone leggermente diversa.

Il 9 Novembre compirò ben 27 anni, mia madre direbbe **buttati via**, ma io ho un'opinone leggermente diversa.


 Bene, voglio fare un esperimento, voglio vedere se **riuscite a farmi il regalo di compleanno**. Visto che ho intenzione di fare un bel [podcast sui paesi baltici](http://www.karimblog.net/2006/11/06/podcast-baltico), ho pensato di acquistare un bel paio di cuffie semiprofessionali per fare i miei lavori da provetto dj. Le cuffie sono [queste](http://www.misco.it/store/index.aspx?sku=60277).


 Per fare il pagamento potete usare **paypal** cliccando direttamente sul pulsante qui sotto, basta una qualsiasi carta di credito e il gioco è fatto. Qualsiasi donazione và bene, **anche solo 1€ è ben accetto**. Da parte mia **vi terrò informati** sullo stato delle donazioni che riceverò e in ogni caso, anche se non ricevo nulla, mi comprerò le cuffie. In fin dei conti il regalo me lo voglio fare... (mica scemo! :smileycool\_tb: )

   
  
  
  
![](https://www.paypal.com/it_IT/i/scr/pixel.gif)  
  
  
  
**Nota:** secondo me non riceverò nemmeno 1€, ma provare non costa nulla.


