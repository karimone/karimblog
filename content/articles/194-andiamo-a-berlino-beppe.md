Title: Andiamo a Berlino, Beppe!
Date: 2006-07-05
Category: Lituania
Slug: andiamo-a-berlino-beppe
Author: Karim N Gorjux
Summary: Come durante Italia '90, sono tornato ad emozionarmi per gli Azzurri, i giocatori sono grandiosi e la squadra gioca bene. Ieri la partita l'ho vista, ma esultare al goal di Grosso è[...]

Come durante Italia '90, sono tornato ad emozionarmi per gli Azzurri, i giocatori sono grandiosi e la squadra gioca bene. Ieri la partita l'ho vista, ma esultare al goal di Grosso è un episodio isolato qui a Klaipeda.... purtroppo...


  


 Ho sofferto per tutta la partita sperando di non arrivare ai rigori, avete visto Germania - Argentina? Avete visto che rigori tirano i tedeschi? Avevo paura, l'Italia giocava bene, ma i rigori sono una (quasi) lotteria irrinunciabile dopo 120 minuti.  
Pirlo, Pirlo, Grosso, tiro a girare! GOAAAAAAAAAAAAAAAAAAAAAAL!!! Brividi per tutta la schiena, tachicardia amille , urlo liberatorio dopo 119 minuti di sofferenza e pensante insulto+bestemmia contro il der spiegel. Al secondo goal del **grandissimo** Del Piero, prendo a pugni Enrico (te l'avevo promesso!) l'Italia arriva alla finale, avrei voluto uscire per strada e urlare come un pazzo, ma ho dovuto accontentarmi di quei 5 minuti durante i due stupendi goal.


 Il brutto delle telecronache qui in Lituania è che dopo che l'arbitro ha fischiato passano 10 secondi di orologio e e si chiude il collegamento con la Germania. E' una doccia fredda, uno si aspetta un commento a caldo, le immagini dei giocatori in festa o dei tedeschi in lacrime, ma tutto finisce lì ricordandoti che oltre a subirti la telecronaca in Lituano, ti devi anche subire le loro regole.


 Se sei all'estero come me e vuoi goderti almeno i due goal con il commento in Italiano, ecco un regalino da parte di Karim (che si trova in Lituania e gode come un matto a vedere gli azzurri in finale). Spero di averti fatto piacere.  
  
  


  technorati tags start tags: [calcio](http://www.technorati.com/tag/calcio), [commento italiano](http://www.technorati.com/tag/commento italiano), [germania 2006](http://www.technorati.com/tag/germania 2006), [germania italia](http://www.technorati.com/tag/germania italia), [italia](http://www.technorati.com/tag/italia), [italia 90](http://www.technorati.com/tag/italia 90), [italia germania](http://www.technorati.com/tag/italia germania), [lituania](http://www.technorati.com/tag/lituania), [mondiali](http://www.technorati.com/tag/mondiali), [mondiali 2006](http://www.technorati.com/tag/mondiali 2006), [semifinale](http://www.technorati.com/tag/semifinale)



  technorati tags end 

