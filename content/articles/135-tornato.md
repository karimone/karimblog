Title: Tornato!
Date: 2005-12-09
Category: altro
Slug: tornato
Author: Karim N Gorjux
Summary: Il ritorno in Italia è stato strano, anzi stranissimo. Mi sono capitate cose che se me lo avessero detto prima non ci avrei mai creduto...All'aeroporto di Palanga, faceva freddo, aveva iniziato a[...]

Il ritorno in Italia è stato strano, anzi stranissimo. Mi sono capitate cose che se me lo avessero detto prima non ci avrei mai creduto...  
  
All'aeroporto di Palanga, faceva freddo, aveva iniziato a nevicare. Il papà di Rita ci ha accompagnati all'aeroporto con 2 ore di anticipo perché doveva assolutamente rientrare in tempo per tornare al lavoro; è stato tutto veloce, Rita mi ha dato un paio di baci, si è trattenuta dal piangere ed è andata via. 

 Ringraziando il cielo avevo da svagarmi, attacco il portatile alla prima presa di corrente e inizio a guardarmi un film. Dopo 1h circa chiama il megafono per il check-in, non immaginatevi un grande aeroporto, Palanga è internazionale, ma partiranno meno di dieci aerei tutto il giorno; il tabellone non è nemmeno elettronico. Nella sala d'attesa che dà sulla pista di atterraggio si forma il gruppo dei viaggiatori, tra tutte queste facce nordiche appaiono due ragazze brune, parlano italiano, butto due parole e rispondono in un modo così simpatico che avrei preferito ricevere un calcio nei coglioni, a Copenaghen andrà meglio...


 L'aereo è piccolo, ma parte in orario e senza problemi, una volta sopra le nuvole vedo un amico che mi mancava da tempo. Lo saluto e lui ringrazia con qualche raggio sfocato.. che bello rivedere il sole... A Copenaghen mi faccio l'ora di wifi per ingannare le due ore e mezza di attesa, ma ne ho già le palle piene, casa mia a Klaipeda è già un ricordo, Rita è lontana. Sposto di 1h indietro l'orologio.   
Una volta sull'aereo per Milano sento il peso di troppe emozioni, la partenza è sempre una ficata, ma mi sento stanco. L'aereo si stabilizza e ho voglia di smangiucchiare qualcosa, mi prendo un cioccolatino e da buon italiano non posso offrirlo al mio vicino di posto con cui attacco bottone. Mi trovo davanti finestrino a metà aereo sulla sinistra, la mia fila è composta da due file soltanto quindi siamo solo io e sto tizio. Iniziamo a fare due parole ed ecco che passano le due ore più veloci della mia vita, si ride si scherza, si mangiano i cioccolatini. Nel mio compagno di viaggio assaporo tutto ciò che l'Italia ha di fantastico da offrire e la resa famosa in tutto il mondo, dopo 1 ora di conversazione mi sembrava di parlare con l'amico delle ciucche, migliori ero quasi dispiaciuto che il viaggio finisse.


 Questa persona incredibile si chiama Corrado e ha già lasciato un commento sul sito (grazie!) lavora in Norvegia per una ditta, ma lui è sposato e vive Lecco. Corrado e sua moglie Cinzia (un ganza del web ;-) hanno messo sù un negozio online dove vendono maglioni Norvegesi ad un prezzo conveniente soprattutto se paragonato ai negozi delle balle che sparano queste particolarità a 200€ (bastardi!). L'indirizzo del negozio è [http://www.wooltime.com/](http://www.wooltime.com/ "Il negozio di Corrado") vi consiglio di visitarlo e di fare anche un giro sul forum dove potete avere informazioni sulla Norvegia nel caso vi interessasse farvi un giro nel paese delle renne...


 La seconda parte sul ritorno a casa la scrivo un'altra volta... -yawn-  
  
  
  
  
adsense  
  
  
  


