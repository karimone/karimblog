Title: Visita a Juodkrante
Date: 2005-10-29
Category: Lituania
Slug: visita-a-juodkrante
Author: Karim N Gorjux
Summary: La bellezza di 73 foto vi aspettano sulla mia galleria per raccontarmi il mio viaggio a Juodkrante, la novità di questo viaggio è stata l'andarci in macchina guidando per la prima volta[...]

La bellezza di 73 foto vi aspettano sulla mia galleria per raccontarmi il mio viaggio a Juodkrante, la novità di questo viaggio è stata l'andarci in macchina guidando per la prima volta per le strade della Lituania. Tanto freddo e tanto vento hanno accompagnato me e Rita nella collina delle streghe.... divertitevi e se volete farmi contento, votate e lasciate commenti.


 Le fotografie: [[Link](http://www.kmen.org/gallery/ "Le foto Joudkrante.")]

 Nota bene: per una buona volta le foto sono visibili **senza registrazione**.


