Title: I Blinì
Date: 2005-02-11
Category: Lituania
Slug: i-blinì
Author: Karim N Gorjux
Summary: Volete sentire il sapore della Lituania? Come già ho raccontato nei miei post precedenti, sia qui che in Lettonia (e da quanto ne sò anche in altri posti limitrofi) la patata è[...]

Volete sentire il sapore della Lituania? Come già ho raccontato nei miei post precedenti, sia qui che in Lettonia (e da quanto ne sò anche in altri posti limitrofi) la patata è l'alimento per eccellenza. Se volete assaggiare un piatto tipico, non è il caso che vi scomodiate a fare 3000 km per venire qui, vi basta preparare i blinì seguendo attentamente le mie istruzioni.


 I blinì (l'accento sulla ultima i) sono delle "frittelle" stranamente di patate con un ripieno a piacere, quindi preparatevi i seguenti ingredienti per due persone:  
  
 * Sei o sette patate pelate crude.  
 * Farina.  
 * 1 uovo.  
 * Del ripieno di carne tritata e altre cose che vi piacciono.  
 * Sale.  







 Ora per preparare i blini basta che seguite le istruzioni fotografiche che ho messo [](http://www.kmen.org/gallerie/blini/ "Preparare i Blinì")

