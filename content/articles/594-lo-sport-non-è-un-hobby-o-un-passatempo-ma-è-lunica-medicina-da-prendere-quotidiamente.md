Title: Lo sport non è un hobby o un passatempo, ma è l'unica medicina da prendere quotidiamente
Date: 2010-03-15
Category: Lituania
Slug: lo-sport-non-è-un-hobby-o-un-passatempo-ma-è-lunica-medicina-da-prendere-quotidiamente
Author: Karim N Gorjux
Summary: Quando sono arrivato la prima volta in Lituania, pesavo 74 kg, quando sono tornato in Italia con moglie e figlia, pesavo quasi 90 kg. Praticamente la Lituania mi ha regalato 15 kg[...]

![](http://www.karimblog.net/wp-content/uploads/2010/03/old_bodybuilder-300x276.jpg "Body Builder della terza età")Quando sono arrivato la prima volta in Lituania, pesavo 74 kg, quando sono tornato in Italia con moglie e figlia, pesavo quasi 90 kg. Praticamente la Lituania mi ha regalato 15 kg come souvenir. Ora che sono tornato qui, tra i miei obiettivi c'è anche quello di perdere peso, ma soprattutto tornare in forma prima che il mio metabolismo rallenti troppo a causa degli anni.


 Nel novembre scorso, dopo aver provato varie palestre, finalmente ho trovato [quella definitiva](http://www.herkausklubas.lt/), quella che non avrebbe chiuso per bancarotta, la palestra in cui fa piacere allenarsi e dove la gente è cordiale e simpatica. **L'inizio è stato duro**, rispetto a vari anni fa in cui mi allenavo praticamente tutti i giorni e soprattutto avevo una forza che mi permetteva di farlo, ora sono più debole ed inoltre ho anche un lavoro e una famiglia cui pensare.  
  
Ho iniziato ad allenarmi con difficoltà e senza superare le due volte alla settimana, il mese di dicembre è stato difficile anche perché le feste, gli esami di Rita e il tempo avverso non mi aiutavano a prendere bene al ritmo. Il 6 gennaio ho ricominciato ad allenarmi senza lasciare spazio a scuse o timori.


 Nel giro di un mese **le sedute in palestra sono passate da due a tre** e la voglia ha iniziato ad aumentare, non sentivo più quel sentimento avverso ogni volta che preparavo la borsa, anzi non vedevo l'ora di passare le mie due ore fuori dall'ufficio e di combattere il buio e il freddo sollevando ghisa. Quando inizi ad allenarti in palestra dopo tanto tempo, la cosa più brutta e sentirsi fiacchi e deboli e quindi ho deciso di registrare le mie sedute di allenamento in un diario. Non avrei mai creduto di arrivare a sollevare nuovamente 70 kg su panca per otto volte consecutive in così breve tempo.


 **Non solo il fisico ne ha giovato, ma anche la vita sociale** perché in palestra ho iniziato a chiacchierare, parlando il mio lituano maccheronico, un po' con tutti e così ho guadagnato sia degli amici che delle amiche. Ci sono stati dei momenti in cui mi sentivo quasi in Italia, persone che mi parlavano, gente che rideva e che scherzava, tutto questo mi fa sentire sempre più un cittadino di Klaipeda.


 Ora mi alleno quattro volte la settimana di cui una alla domenica, dopo ogni seduta faccio la doccia, a volte la sauna e sempre la barba. Il tutto in compagnia dei miei nuovi amici lituani e russi che siano. Dato che l'attività fisica è l'unica salvezza ed è anche la mia principale scuola di lituano, mi sono iscritto a un corso di Kung Fu Cinese, ovvero il [Wing Chun](http://it.wikipedia.org/wiki/Wing_Chun) che mi occupa due sere alla settimana oltre alla palestra.


 Quindi **sono ben tre mesi abbondanti** che sono tornato a dedicarmi religiosamente all'attività fisica. La differenza rispetto a tutte le volte che ho provato ricominciare in questi ultimi anni e che ho iniziato gradatamente, non ho cambiato il mio stile di vita dal giorno alla notte, ma mi sono imposto dei piccoli cambiamenti per un lungo periodo. Ora il più grande cambiamento che voglio fare è che voglio portare a termine entro l'estate, è la dieta, ma non la dieta intesa come nei giornali spazzatura femminili. Ma la dieta intesa come le intendevano i greci: il termine dieta deriva dal latino diaita, greco dìaita e significa *"modo di vivere"*. Quindi mangiare sano, non per un giorno o due o un mese giusto per perdere peso, ma per farlo diventare uno stile di vita.


