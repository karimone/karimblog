Title: E' morto il blog, viva il nuovo blog
Date: 2017-05-29
Category: altro
Slug: e-morto-il-blog-viva-il-nuovo-blog
Author: Karim N Gorjux
Summary: E finalmente, dopo tanto rimandare, ho recuperato il mio blog.

E finalmente, dopo tanto rimandare, **ho recuperato il mio blog**.


 L'ultimo post e' di circa 1 anno fa, nel mentre ho sempre rimandato di prendere mano il blog e rimodellarlo a mio piacimento in modo da avere nuovamente l'entusiasmo di scrivere come avevo qualche anno fa. I cambiamenti non sono radicali, anzi c'e' tanto da fare per riordinare gli articoli e rendere la lettura piu' piacevole, ma almeno il passo piu' grande e' stato fatto.


 Per chi e' interessato agli aspetti tecnici, ho mandato in pensione wordpress che era il "motore" dietro questo blog dal lontano 2004, al suo posto sto usando [Mezzanine](http://mezzanine.jupo.org) che e' fatto con la stessa tecnologia che uso quotidianamento.


 Il design del sito, soprattutto gli header in alto con me in versione "fumettora" non e' opera mia, ma di [Maurizio](http://maurizionoris.com)

 