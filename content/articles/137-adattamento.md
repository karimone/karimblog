Title: Adattamento
Date: 2005-12-11
Category: Lituania
Slug: adattamento
Author: Karim N Gorjux
Summary: E' passato qualche giorno e mi sto piano piano adattando all'Italia. La prima cosa che ho notato è che avevo sempre sonno...Sia oggi che ieri ho dovuto dormire il pomeriggio, anche il[...]

E' passato qualche giorno e mi sto piano piano adattando all'Italia. La prima cosa che ho notato è che avevo sempre sonno...  
  
Sia oggi che ieri ho dovuto dormire il pomeriggio, anche il ritmo colazione-pranzo-cena ha sconvolto la mia quotidianità, ma sinceramente mi piace, è una delle cose che più mi manca quando sono in Lituania.  
Oggi Rita mi ha detto che a Klaipeda c'è sempre il solito tempo: cielo grigio, freddo e anche un po' di neve, Cuneo invece si salva, oggi c'era il sole anche se fa freddo.


 Ieri sera sono stato in giro con gli amici, è stato bello rivedere i soliti posti, le solite facce, ma tutte le volte che rientro in Italia mi colpisce la differenza incredibile dalla Lituania. Quando sono stato nel locale "El Calor" per il mio compleanno, c'era poca gente e per lo più donne, ieri sera al Garage c'era troppa gente, veramente troppa. Il non potersi girare in un locale non ha senso, dovrebbero dare un po' di respiro alla gente che c'è altrimenti che gusto c'è a stare in un locale? Il fatto che poi erano 80% uomini è degno di nota rispetto alla Lituania, ma non ha importanza...


 Il fine serata lo abbiamo passato dal kebabbaro che purtroppo ha sempre i soliti prezzi. Mah.. 1,50€ per una mezza naturale... mah... (io non l'ho presa..)  
  
[![Deborah e Davide](http://www.kmen.org/wp-content/upload/Deborah%20e%20Davide-tm.jpg "Deborah e Davide")](http://www.kmen.org/wp-content/upload/Deborah%20e%20Davide.jpg)  
  
  
  
  
  
adsense  
  
  
  


