Title: Si è fortunato quando si è consapevoli di esserlo
Date: 2006-04-14
Category: altro
Slug: si-è-fortunato-quando-si-è-consapevoli-di-esserlo
Author: Karim N Gorjux
Summary: Mio cugino ha 20 anni, ha una famiglia semplice che gli vuole bene, un fratellino di 10 anni meno che lui, una bella ragazza che gli vuole bene, ha un lavoro fisso[...]

Mio cugino ha 20 anni, ha una famiglia semplice che gli vuole bene, un fratellino di 10 anni meno che lui, una bella ragazza che gli vuole bene, ha un lavoro fisso in una società con possibilità di partecipare come socio, ha la fortuna di avere la salute e la possibilità di avere una casa sua. Purtroppo non si rende conto di essere fortunato.


