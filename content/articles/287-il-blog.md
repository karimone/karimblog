Title: Il blog
Date: 2007-01-21
Category: altro
Slug: il-blog
Author: Karim N Gorjux
Summary: Prendendo spunto dall'articolo di Luca, vedo di dire la mia su cosa sia un blog.

Prendendo spunto dall'articolo di [Luca](http://www.pandemia.info/2007/01/13/tremila_post_e_la_divulgazione.html), vedo di dire la mia su cosa sia un blog.


 A mio parere il blog è **un'occasione**. Negli ultimi anni ho viaggiato nei paesi baltici e le mie varie avventure nell'ex unione sovietica sono tutte qui sul mio blog, se torno indietro con le pagine posso leggere cosa ho scritto quando ho conosciuto mia moglie o quando ho saputo che stavo diventando papà. Una volta si scriveva tutto su un diario, ma qui sul blog è tutta un'altra cosa: scrivo in modo molto diverso da come scriverei su un diario, le persone che mi conoscono commentano e dicono la loro, e i pensieri pubblicati sono molto più profondi.


 Il blog però non è solo questo, non è solo un modo univoco per comunicare, io tramite il blog ho conosciuto delle persone speciali che ho poi avuto la fortuna di incontrare. Ho conosciuto Massimo di Vilnius, [Sandro](http://kaunaslife.blogspot.com/) di Kaunas, [Alessandro](http://eiochemipensavo.diludovico.it/) di Cuneo, [Thierry](http://www.power-office.ch) di Ginevra. Soprattutto con quest'ultimo ho stretto un'amicizia a tal punto da diventare soci d'affari. Il blog mi ha dato delle possibilità non da poco, me ne rendo conto soprattutto quando penso a chi ha tante cose da raccontare, ma purtroppo non ha tempo per condividere i suoi pensieri su un blog.


 Aprire il mio blog, ha contribuito a far nascere altri blog o a migliorarne di già esistenti [Gaetano](http://blog.magogae.net/), [Daniela](http://www.danielaforconi.net/), [Matteo](http://blog.wastedthoughts.net/), [Massimo](http://www.massimoconsulting.com/) sono solo alcuni esempi di amici che come me hanno colto l'occasione del nuovo millenio: **il blog**.


 Altre persone hanno scritto sul blog: [Francesco](http://gaspatcho.blogspot.com/), [Stefano](http://www.stefanogorgoni.com/), [Davide](http://www.davidesalerno.net/), [Lele](http://www.leledainesi.com), [Luciano](http://www.lucianogiustini.org/), [Andrea](http://www.scintilena.com/), [Andrea](http://www.bloogs.com/cips/), [Filippo](http://www.sardanapalo.net/wpf/), [Tommaso](http://dontomsci.blogspot.com/), [Francesco](http://www.napolux.com/), [Miriam](http://miriambertoli.blogspot.com/), [Mauro](http://admaiora.blogs.com/maurolupi/), [Andrea](http://www.andreamartines.com), [Marco](http://blog.camisani.com/), [Matteo](http://www.totanus.net), [Matteo](http://www.jtheo.it), [Michele](http://www.micheleluconi.it/), [Filippo](http://www.tigulliovino.it/blog/index.html), [Antonio](http://www.simplicissimus.it), [Luca](http://www.lucalorenzetti.it), [Luca](http://www.blublog.it/), [Luca](http://www.lucazappa.com), [Marco](http://www.marcocattaneo.com), [Pierluigi](http://lnx.openetica.it/lotek/), [Riccardo](http://codewitch.org/), [Stefano](http://quinta.typepad.com/blog/), [Giorgio](http://blog.giorgiotave.it/), [Stefano](http://www.stefanoepifani.it/), [Marina](http://www.piublog.it), [Marica](http://bresaola.blogspot.com), [Mary](http://pastamista.blogspot.com/), [Cristian](http://www.disordine.com), [Emanuele](http://www.infospaces.it/wordpress/),

