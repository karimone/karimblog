Title: Le donne lituane: qualcosa di buono c'è
Date: 2006-10-02
Category: Lituania
Slug: le-donne-lituane-qualcosa-di-buono-cè
Author: Karim N Gorjux
Summary: Le donne lituane, beh per noi sono belle, noi italiani moriamo per le donne lituane, ma non tutti gli italiani e sembra che non siano pochi. Donne italiane non abbiatene a male,[...]

Le donne lituane, beh per noi sono belle, noi italiani moriamo per le donne lituane, ma non tutti gli italiani e sembra che non siano pochi. Donne italiane non abbiatene a male, sono solo gusti.  
  
Stare con una donna lituana ha dei vantaggi, per prima cosa hanno visto un passato molto diverso dal nostro quindi se **non le viziamo** sono umili, realiste e con i piedi per terra. **Non cercano** qualcuno che le ricopra d'oro o le compra tutti i vestiti che vuole, le donne lituane sanno essere riconoscenti all'uomo che le fa i complimenti, le fa sentire importanti e le rispetta. La cultura dell'uomo lituano spiana la strada per l'italiano seduttivamente palestrato, le donne lituane apprezzano i nostri sforzi nel corteggiarle.  
  
  
Non voglio generalizzare, ma anche in Lituania esistono le donnette e se siete un po' accorti avrete per le mani una bella e brava figliola, sappiate scegliere perché soprattutto nelle grandi città le ragazze hanno capito l'antifona. **Alcuni italiani vengono e trovano quello che vogliono, ma lo pagano caro.** Le donne lituane sono affettuose, ma non piace darlo a vedere, sono molto riservate e anche se ad occhi di estranei sembrano incazzate e fredde in realtà quando sono con voi sono delle **gattine premurose.** E poi dato che una donna dovete prendervela e dato che in ogni caso **di qualsiasi nazione sia vi farà girare le balle.**.. beh... tanto vale prendersela bella.


