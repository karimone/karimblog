Title: Fate molta attenzione agli italiani all'estero
Date: 2013-03-21
Category: Lituania
Slug: fate-molta-attenzione-agli-italiani-allestero
Author: Karim N Gorjux
Summary: In cinque anni che ho passato in Lituania, ho conosciuto moltissimi italiani. Di tutti questi italiani che risiedevano in Lituania ne ho conosciuti ben due che si salvano, entrambi si chiamano "Mirko"[...]

In cinque anni che ho passato in Lituania, ho conosciuto moltissimi italiani. Di tutti questi italiani che risiedevano in Lituania ne ho conosciuti ben due che si salvano, entrambi si chiamano "Mirko" mentre **tutti gli altri bisognava evitarli e farsi vedere il meno possibile in loro compagnia**. La mia esperienza ovviamente si può estendere, con i dovuti cambiamenti, a qualsiasi paese.


 In Lituania la maggior parte degli italiani, se non tutti, ci va per questioni di donne. In un secondo momento però si crea la componente "business" che è quella che viene poi aditata nel momento che incontrate questi italiani e gli chiedete perché sono in Lituania.  
  
"Ciao, come mai qui in Lituania?"  
"Importo dall'Italia il/le/i/ xxxxxx"

 Quel xxxxxx può essere qualsiasi cosa, dal prosciutto alle frizioni della Fiat Punto.  
### Il Manuel Fantoni

  
Manuel Fantoni è il personaggio interpretato da Angelo Infanti nel film "Borotalco" del 1982 ([video](http://www.youtube.com/watch?v=IbDSeyuu67U)), un tizio che vive nel lusso, ma in realtà è un povero pezzente che vive raccontando un sacco di balle o come dicono a Roma "un sacco de fregnacce"

 Il problema in questo tipo di persona che potete incontrare non è il business e se è vero o meno, **il vero problema è come si pone**. Intanto lo riconoscete subito perché il "Manuel Fantoni" dice di sapere e conoscere tutti inoltre cerca di sminuirti facendoti pesare l'importanza della sua presenza in Lituania rispetto alla tua presenza, menzionando posti visitati, anni in cui è in Lituania e donne che si è trombato. **Manuel è il caso peggiore che vi può capitare **perché se non siete esperti, vi potrà incantare e farvi credere a quello che dice, Manuel millanta di conoscere persone altolocate sia in Lituania che in Italia, di avere contatti importanti e di muovere ingenti somme di denaro.


 Mi sono capitati vari "Manuel" in Lituania e se aprite bene gli occhi e le orecchie li riconoscerai anche tu. Se li frequenterai abbastanza noterai che tra i loro racconti e le loro azioni ci sono un sacco di incongruenze, questo è il primo "sintomo" di chi racconta un sacco di balle e non riesce a ricordarsele tutte. Inoltre, una componente comune di queste persone è che non parla il lituano (o qualsiasi sia la lingua locale). Tutte le persone serie che ho incontrato e facevano affari in Lituania con una certa continuità, parlavano il lituano discretamente o almeno il russo.


 Non parlare lituano e raccontare un sacco di balle porta ad una conseguenza soltanto:** "Manuel" potrà fregare solo italiani**. Ho sentito un sacco di italiani che sono stati fregati da altri italiani e nei modi più disparati! Quest'inverno mi sono trovato in Lituania e mentre parlavo con alcuni italiani residenti nel centro della Lituania **saltava sempre fuori il nome "Franco Truffa**" per riferirsi ad un altro italiano che aveva fregato un po' di persone.


 A mio avviso, la caratteristica più importante è il "millantare". Il "Manuel" lo riconoscete subito da quello. Più millanta e più dovete fare attenzione.


 Parliamoci chiaro, se "Manuel" così importante, è a parlare a pranzo con la [Grybauskaite ](http://it.wikipedia.org/wiki/Dalia_Grybauskait%C4%97 "Presidentessa lituana Daila Grybauskaite")e non a rompere le palle a me che non sono nessuno.  
### Il succube della donna lituana

  
Altro dettaglio molto importante è la donna. Se volete sapere chi è l'uomo davanti a voi, dovete conoscere la donna che si è scelto. Da come veste e da come parla la sua donna capirete subito l'incantesimo a cui è sottoposto l'italiano di turno. Ci sono uomini che dalla donna sono spinti a fare ogni tipo di nefandezza verso se stessi e verso gli altri. Il problema è che ci sono tipi di persona che sono assolutamente adatti a questo tipo di donna, ma altri che ne sono completamente succubi.


 Mettetevi nei panni di un uomo che superati i 50 anni e con un divorzio alle spalle, si ritrova una ragazza lituana di 35 anni, bionda, alta, bella e che gli fa vivere (con il contagocce) un "paio di mezz'ore" di estasi al mese. Il povero italiano ne diventa succube.


 Questo tipo di relazioni sono costernate di difficoltà, le lituane non sono sceme e se vedono il pollo lo spennano come solo una donna sa fare. Ma un uomo di 50 anni che non è bello e prestante come un tempo, cosa può dare ad una donna? Il benessere.


 Purtroppo quelle "mezz'ore" di estasi, le cene romantiche ai ristoranti di [Juodkrante ](http://en.wikipedia.org/wiki/Juodkrant%C4%97)e l'illusione di un fidanzamento a 50 anni con una "bella gnocca" costano sempre di più e il nostro malcapitato italiano si vede costretto a trasformarsi in un "Manuel" pur di non perdere la sua droga.


 Questi tipi di italiani succubi sono sempre più frequenti. Ultimamente ho avuto modo di vedere uno di questi personaggi che potrebbe tranquillamente essere mio padre, rovinarsi reputazione e carriera e diventare un vero truffatore per inseguire una lituana che lo prendeva per il culo come mai non ho visto fare.


 Se frequenti una donna lituana che non vive con te una relazione normale dove il sentimento è al primo posto, ma tutto è sempre sotto ricatto di "beni e servizi" allora apri gli occhi, guardati allo specchio, non vedi che sei pieno di piume? Pollo!  
### Il cattivo

  
Io il "Franco Truffa" che ho menzionato poco sopra ho avuto modo di conoscerlo. Semplicemente è un truffatore sia che si trovi in Italia che si trovi in Lituania. "Franco Truffa" Millantava e c'era della cattiveria nei suoi occhi. Fate ben attenzione a tipi del genere.


 Concludendo, gli unici italiani con cui mi trovavo bene a Klaipeda, oltre al caro Mirko, erano i ragazzi erasmus. Gente amichevole, per bene e genuina.


 PS1: Mi piacerebbe davvero leggere i commenti di [Mirko](http://mirko.system-x.it/info/) e [Mirko](http://www.facebook.com/GelatoADomicilio) a questo articolo.


 PS2: L'incipit a questo articolo mi è venuto, oltre che ad alcune ultime esperienze personali, da questo [post ](http://www.italietuva.com/forum/italiani-a-vilnius-45-sono-morti-di-fame-che-fanno-i-boss-t8829.html "Italiani a Vilnius. Intervento su italietuva forum")su italietuva.


