Title: Non sopporto le persone che sporcano l'ambiente e purtroppo sia gli italiani che i lituani sporcano
Date: 2008-11-06
Category: Lituania
Slug: non-sopporto-le-persone-che-sporcano-lambiente-e-purtroppo-sia-gli-italiani-che-i-lituani-sporcano
Author: Karim N Gorjux
Summary: Mi ritrovo molto nelle parole di Enrico in questo post:...Ad un certo punto, la mano sinistra schiaccia ed espelle fuori dalla vettura un pacchetto di sigarette vuoto. Se avessi avuto i missili[...]

Mi ritrovo molto nelle parole di Enrico in [questo post](http://www.enrico.it/2007/04/27/voglio-andare-a-vivere-in-svizzera/):  
*  

> ...Ad un certo punto, la mano sinistra schiaccia ed espelle fuori dalla vettura un pacchetto di sigarette vuoto. Se avessi avuto i missili perforanti di Jeeg avrei fatto fuoco e senza alcuna esitazione!  
*  
  
Ho sempre odiato le persone che buttano i mozziconi, la carta o qualsiasi altra cosa per terra. Purtroppo questa abitudine è molto diffusa in Lituania infatti la maggior parte dei lituani che vedo in giro sono dei luridi zozzoni, ricordo ancora le spiagge di Klaipeda invase dalle bottiglie di birra, i mozziconi e le lattine. Peccato davvero!

 Con questo non voglio dire che gli italiani siano da meno, anche loro hanno il loro ben da fare a sporcare l'ambiente. Io sono sempre stato criticato perché in macchina buttavo tutta l'immondizia nella macchina e mai fuori, andate a cagare! Riciclate, spegnete le tv che sono sempre in standby, andate in bicicletta, non sprecate l'acqua e non sperperate. Il mondo è di tutti noi.


 Gli svizzeri sono veramente da prendere come esempio.


