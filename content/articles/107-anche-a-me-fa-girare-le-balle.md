Title: Anche a me fa girare le balle.
Date: 2005-10-21
Category: altro
Slug: anche-a-me-fa-girare-le-balle
Author: Karim N Gorjux
Summary: Mi capita alle volte di scrivere delle email e non ricevere alcuna risposta. Ad un certo punto mi sono chiesto: ma sto sul cazzo a qualcuno?

Mi capita alle volte di scrivere delle email e non ricevere alcuna risposta. Ad un certo punto mi sono chiesto: ma **sto sul cazzo a qualcuno?** 

 Sarebbe bellissimo, oltre che educato, ricevere una mail succinta con un veloce *"scusa non ho tempo per rispondere, grazie per la email"*, ma mettendo anche da parte l'educazione, mi renderebbe felice anche un *"vaffanculo"*. Una qualsiasi risposta è sufficiente, mi sentirei preso in considerazione e non sarei ripagato del peggiore dei mali: **l'indifferenza**.


 Pensavo di essere l'unico a farsi ste seghe mentali, ma il mio amico Luca Conti, si pone gli stessi dubbi. [[Link](http://www.pandemia.info/2005/10/21/ricevuta_la_mail.html "Articolo di Luca Conti su Pandemia")]

