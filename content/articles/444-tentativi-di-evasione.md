Title: Tentativi di evasione
Date: 2008-04-19
Category: altro
Slug: tentativi-di-evasione
Author: Karim N Gorjux
Summary: Ieri, Venerdì 18, compleanno di Rox ho provato per la prima volta ad evadere di casa e lavorare in un ufficio esterno.Ho la grande fortuna di poter fare il mio lavoro di[...]

Ieri, Venerdì 18, compleanno di [Rox](http://marcorossoni.com) ho provato per la prima volta ad **evadere** di casa e **lavorare** in un ufficio esterno.  
Ho la grande fortuna di poter fare il mio lavoro di consulenza dove voglio, [skype](http://www.skype.com) mi permette di essere presente in tutto il mondo e quindi, grazie al mio [MacBook Pro](http://www.apple.com/it/macbookpro/), posso fisicamente stare dove mi pare. Sembra tutto rose e fiori, ma in realtà non lo è. Vivo in un appartamento dove non ho una stanza adibita al solo uso ufficio e quindi sono sempre preda di scomode **interruzioni**.


 Immaginate di dovervi fare 200km di autostrada come per esempio Cuneo - Malpensa; personalmente quando mi tocca andare all'aeroporto di Malpensa, passo da Asti e una volta che sono sulla strada per Gravellona Toce mi fermo per il caffè ed una rinfrescata al primo autogrill. Provate per un attimo ad immaginare di fare la stessa strada e fermarvi 15 volte per fare 200km, in pratica in tutte le aree di servizio che incontrate! Arrivereste alla fine del viaggio con la stanchezza di chi ha fatto [l'Overland](http://www.overland.org/) in un giorno. Analogamente essere disturbati continuamente mentre si fa qualcosa stanca moltissimo e uccide la produttività

 Ieri sono riuscito a stare tutto il giorno in un ufficio provvisorio, ho portato avanti i miei lavori e sono riuscito a **concentrarmi** benissimo. Ero così concentrato che non ho nemmeno aggiornato il blog pur avendone tutto il **tempo**.


