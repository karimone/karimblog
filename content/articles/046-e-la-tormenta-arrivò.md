Title: E la tormenta arrivò
Date: 2005-02-10
Category: Lituania
Slug: e-la-tormenta-arrivò
Author: Karim N Gorjux
Summary: Dopo alcuni giorni di sole splendente e freddo pungente, siamo arrivati alla tanto preannunciata tormenta. Meno 20 gradi notturni, vento e neve. Cosa si può desiderare di più? Sono stato sgridato oggi[...]

Dopo alcuni giorni di sole splendente e freddo pungente, siamo arrivati alla tanto preannunciata tormenta. Meno 20 gradi notturni, vento e neve. Cosa si può desiderare di più? Sono stato sgridato oggi da mia cugina a causa del mio silenzio, ha ragione, non stò scrivendo nulla in questo periodo e perdo tempo prezioso in altre cose molto più interessanti.


 Ieri sera ero al Memelis per salutare Andrea, tra una cosa e l'altra non sono riuscito a fargli le foto che avrei voluto ed è un vero peccato. Andrea ha 24 anni, completamente rasato, piccolino come me ed estremamente simpatico. Studia lingue ed oggi ha ufficialmente finito il suo erasmus a Klaipeda, secondo i miei calcoli dovrebbe essere in Italia proprio ora che stò scrivendo. Andrea è una di quelle persone che vale la pena conoscere, estremamente simpatico e semplice, ha una dote naturale nel sintonizzarsi con le mie scurrilità e ridere delle mie stronzate. Più di una volta ha avuto la capacità di rendere le nostre serate culinariamente accettabili (memorabile il suo ragù in 30 minuti). Ciao Andrea, spero di vederti presto e che ti venga regalato un Mac un giorno ;-)

 [![Andreakarim](http://www.kmen.org/images/AndreaKarim.jpg)](http://www.kmen.org/images/AndreaKarim.jpg)



 La serata già particolare di suo per salutare Andrea, ma è successa l'ultima cosa che avrei mai pensato succedesse: abbiamo conosciuto degli Italiani! Di per sè questo non è così speciale, ma uno di loro è di Boves, e questo si che è strano. Sono giovani ragazzi lavoratori, conseguenza diretta delle direttive europee e sono qui per passare tre settimane al freddo. Diciottenni, spaesati, ma bravi ragazzi. Sono sicuro che sarà per loro un'esperienza indimenticabile, divertitevi, ma rispettate sempre gli altri, solo così rispetterete voi stessi.


