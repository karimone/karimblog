Title: Cambiamento
Date: 2005-09-15
Category: altro
Slug: cambiamento
Author: Karim N Gorjux
Summary: Dato che ora abbiamo quasi messo tutte le basi per poter creare il nostro Re Mida personale, penso che sia giunto il momento di dare un giro a questo blog. Il problema[...]

Dato che ora abbiamo quasi messo tutte le basi per poter creare il nostro Re Mida personale, penso che sia giunto il momento di dare un giro a questo blog. Il problema grafica dovrebbe avere trovato una soluzione, vediamo di fare qualcosa di ragionevole.  
  
Vorrei dare un'identità a questo sito. Dividerlo in canali tematici e togliere del personale che non frega a nessuno. Gente che si collega ce ne sempre di meno, dovrei mettermi a scrivere delle cazzate immani. Tipo... che ne sò... *- (void)setEntryDate:(NSCalendarDate *)date*... queste sì che sono cose interessanti.


 Il 27 Settembre alle ore 07:00 sarò da fiore a comprare le brioches alla nutella che mi porterò fino in Lituania, ovviamente non sono per me, saluterò Centallo e spero di non tornarci per tanto tantissimo tempo.


 Oggi ho ricevuto un cd, l'ultimo album dei Rolling Stones, segnerò la data sul calendario perché è raro negli ultimi tempi che qualcuno ti dà qualcosa senza chiedere niente in cambio. Come è raro ricevere una telefonata giusto per sapere come stai. In sostanza si è persa la concezione del "noi", ma è sempre più forte quella dell'IO. Troppe persone vivono la loro vita in funzione dei propri bisogni.


 Per fortuna il mondo gira sempre.


