Title: Dove sarà l'Italia tra 20 anni?
Date: 2013-09-10
Category: altro
Slug: dove-sarà-litalia-tra-20-anni
Author: Karim N Gorjux
Summary: Da un po' di tempo a questa parte mi chiedo che cosa ne sarà dell'Italia tra 20 anni. Le novità quotidiane non mi fanno sperare in bene, ma è d'obbligo prendere delle[...]

Da un po' di tempo a questa parte mi chiedo che cosa ne sarà dell'Italia tra 20 anni. Le [novità quotidiane](http://www.repubblica.it/politica/2013/09/10/news/riforme_camera_d_ok_comitato-66253075/?ref=HREA-1 "Riforme") non mi fanno sperare in bene, ma è d'obbligo prendere delle decisioni ponderate sul lungo periodo e non sulla propria situazione attuale.


 Vent'anni fa eravamo qui:

  ![Ragazze Drive In](http://cdn.blogosfere.it/teleipnosi/images/drive%20in%20ragazze.jpg)

 Ora invece siamo a "uomini e donne", "isola dei famosi", calcio in tutte le salse ed in tutti i canali... **in vent'anni c'è stato un netto miglioramento del livello di peggioramento culturale italiano**. Ora, seriamente, chi se la sente di dire che l'Italia sarà una potenza in Europa tra 20 anni? Se per un attimo pensiamo all'Italia come un'azione in borsa nel mercato, il grafico sarebbe un continuo discendere in vent'anni. Comprereste delle azioni dell'Italia? Sposeresti una donna che ha il vizio di tradire il suo compagno?

 Che ne sarà dei nostri figli?

