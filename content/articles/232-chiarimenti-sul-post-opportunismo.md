Title: Chiarimenti sul post "opportunismo"
Date: 2006-10-07
Category: altro
Slug: chiarimenti-sul-post-opportunismo
Author: Karim N Gorjux
Summary: Un paio di giorni fa ho scritto questo post. E' meglio che chiarisca bene la situazione altrimenti vengo scambiato per il tipo che ci prova con le lituane all'insaputa di mia mogle.[...]

Un paio di giorni fa ho scritto questo [post](http://www.karimblog.net/2006/10/04/opportunismo-o-timidezza). E' meglio che chiarisca bene la situazione altrimenti vengo scambiato per il tipo che ci prova con le lituane all'insaputa di mia mogle. (Donna Malonus, leggerissimamente maliziosa..)

 L'altro week-end mi trovavo a casa da solo, Rita era a Telsai e vista la partenza prematura di Enrico non sapevo con chi vedermi. Ho chiamato al telefono questa ragazza di cui non voglio assolutamente riferire il nome, ma mi è stata presentata da un mio amico italiano che tra l'altro è della mia stessa provincia. (Qui qualcuno potrebbe avere intuito). Chiamo questa ragazza per sapere se sabato o domenica aveva il tempo di vedermi per fare due chiacchiere, non per approfittare della situazione visto l'assenza di mia moglie, ma per fare due chiacchiere e prendere qualcosa in un locale in Klaipeda. Durante la breve telefonata, mi dice che si sarebbe fatta sentire per vederci, ma nel discorso salta fuori che devo andare a Vilnius Lunedì e lei pure. Perché non fare il viaggio insieme? (Sono quasi 4 ore di macchina).


 Non si è mai fatta sentire per uscire, ma solamente la domenica tramite messaggi per chiedermi del passaggio. Ora spero che abbiate capito il [post](http://www.karimblog.net/2006/10/04/opportunismo-o-timidezza).


