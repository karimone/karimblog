Title: Ciao Italy
Date: 2006-04-23
Category: Lituania
Slug: ciao-italy
Author: Karim N Gorjux
Summary: Dopo 1 mese in Italia, eccomi di nuovo in partenza per Klaipeda. La mattina del 29 Aprile sarò di partenza da Linate per Copenaghen e dopo 4 ore di attesa mi troverò[...]

Dopo 1 mese in Italia, eccomi di nuovo in partenza per Klaipeda. La mattina del 29 Aprile sarò di partenza da Linate per Copenaghen e dopo 4 ore di attesa mi troverò a Palanga verso le 14. Sono ansioso di tornare perché qui in Italia mi trovo perso, sento che mi manca qualcosa e capisco che la vita in Italia per il momento non mi appartiene.  
  
Non voglio dire che mi trovo male (internet a parte), ma che ciò che lascio in Lituania è molto più importante di ciò che trovo in Italia. Molte cose non hanno più importanza e altre invece sono diventate un punto di riferimento, ora l'unica cosa che sto aspettando è di tornare a casa dove Rita mi aspetta e potrò sentire i movimenti del piccolo che scalcia.


 Più passa il tempo e più mi rendo conto che questo "incidente" è la cosa più bella che mi poteva capitare. Sento dire che mi sto perdendo molte cose che non dopo potrò mai più fare, ad esempio le serate in discoteca o le ciucche con gli amici, ma io non sono un bevitore e del ballare non me ne frega molto, e poi, soprattutto ora, la mia testa è altrove, lontano... forse qualche mamma o papà che legge mi può capire.


