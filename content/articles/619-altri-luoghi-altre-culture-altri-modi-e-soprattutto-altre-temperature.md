Title: Altri luoghi, altre culture, altri modi e soprattutto altre temperature
Date: 2010-10-11
Category: Lituania
Slug: altri-luoghi-altre-culture-altri-modi-e-soprattutto-altre-temperature
Author: Karim N Gorjux
Summary: "Ciao, Viktor! Allora, Mercoledì giochiamo a calcio?""Si, ma alle 17:30 non più alle 18.""Bene! Ma senti una cosa, fino a quando giochiamo fuori a calcetto? Tra poco arriva l'inverno!""Tranquillo, anche quando c'è[...]

*"Ciao, Viktor! Allora, Mercoledì giochiamo a calcio?"  
"Si, ma alle 17:30 non più alle 18."  
"Bene! Ma senti una cosa, fino a quando giochiamo fuori a calcetto? Tra poco arriva l'inverno!"  
"Tranquillo, anche quando c'è la neve si gioca bene. L'anno scorso giocavamo a -15, è divertente!"  
"Ah."*

 Per un attimo mi sono tornate alle mente tutte le imprecazioni che tiravo quelle poche volte che d'inverno, prenotavamo la palestra del paese per giocare a calcetto e poi la ritrovavamo con il riscaldamento spento.  
Chissà cosa direbbe a riguardo il mio amico Viktor..


