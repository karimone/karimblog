Title: Dopo 1 anno di Italia
Date: 2012-04-27
Category: Lituania
Slug: dopo-1-anno-di-italia
Author: Karim N Gorjux
Summary: In questo ultimo anno ho scritto praticamente con il contagocce. Sono arrivato in Italia il 13 Aprile del 2011 quindi ormai è un anno che sono qui. La Lituania è solo più[...]

In questo ultimo anno ho scritto praticamente con il contagocce. Sono arrivato in Italia il 13 Aprile del 2011 quindi ormai è un anno che sono qui. La Lituania è solo più un ricordo sbiadito, alcune volte mi soffermo a pensare alla vita che facevo lassù rispetto alla vita che faccio qui. Le differenze sono molte, nei pro e nei contro, ma a mio modo di vedere le cose sono contento di essere qui anche se la situazione dell'Italia è sotto certi versi tragica.


 Dalle notizie che mi arrivano tramite mia suocera, i prezzi del cibo in Lituania continuano ad aumentare, la gente continua ad emigrare e le prospettive future non sono delle migliori. Non è solo una questione di tempo (meteorologico), ma di benessere sociale. La sanità locale è sempre un disastro e il lavoro si trova sempre poco a stipendi ragionevoli. Mi spiace per la Lituania, ma la crisi economica che sta investendo i paesi d'Europa più deboli, sta massacrando i paesi dell'ex unione sovietica.


 Ogni tanto vado a vedere le foto che pubblicano i locali che frequentavo in Klaipeda. La cosa che più mi ha stupito è di vedere facce nuove, ma non quelle che vedevo di solito io. Quando ero in Lituania, invece, guardavo le foto dei vari locali del cuneese e vedevo sempre le stesse facce.


 Intanto Klaipeda è passata a 177.823 residenti per l'anno 2011 contro i 190.000 abitanti circa del 2005.


 Mi piacerebbe davvero andare a dare un'occhiata per farmi un'idea.


