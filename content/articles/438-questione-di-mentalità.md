Title: Questione di mentalità
Date: 2008-04-11
Category: Lituania
Slug: questione-di-mentalità
Author: Karim N Gorjux
Summary: La differenza tra varie nazioni è, a mio parere, nella mentalità della popolazione. Rispetto comune, efficienza e precisione fanno la vera differenza.

La **differenza** tra varie nazioni è, a mio parere, nella mentalità della popolazione. Rispetto comune, efficienza e precisione fanno la vera differenza.


 In **Svizzera** ero rimasto piacevolmente colpito da quei curiosi box sul marciapiede dove chiunque poteva prendersi il quotidiano senza dover per forza trovare un'edicola. L'incredibile è nel box stesso che non ha nessun meccanismo per il controllo del pagamento, chiunque potrebbe prendersi 10 giornali e pagare con un bottone della camicia, ma in Svizzera continua tutto da anni e quindi vuol dire che la gente paga.  
  
Una mia amica tedesca che vive in Italia ormai da 8 anni, mi ha confessato che ancora oggi si meraviglia di alcuni piccoli particolari della nostra vita quotidiana. Molti fatti che qui in Italia sono **consuetudine**, in Germania non verrebbero ammessi nemmeno per 1 ora: condannati in parlamento, evasione fiscale continua, sporcizia nei fiumi e nelle città, programmi televisivi osceni... la lista potrebbe continuare ancora e sarebbe utile per dare corpo a questo articolo, ma preferisco intenzionalmente finirlo in poche righe.


 Esistono tanti esempi di altri paesi d'Europa che sono molto più civili del nostro grazie ad una mentalità di cui il rispetto comune è al centro di ogni **decisione**. Sia ben chiaro che tutti i paesi hanno le loro difficoltà e i loro difetti, ma **migliorarsi** deve essere una costante comune da perseguire con il **tempo**.


 Nel 1994 **Berlusconi** è sceso in campo, da allora Berlusconi è ancora in campo, la sinistra lo aiuta e l'Italia è quella che è. Sono passati **14 anni**, nel mentre paesi come la Norvegia, l'Irlanda, l'Estonia e anche la mia cara Lituania, hanno fatto dei passi da gigante pur con gravi difficoltà alla base.


   
  
  
  


