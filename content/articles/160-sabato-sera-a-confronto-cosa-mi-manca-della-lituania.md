Title: Sabato sera a confronto: cosa mi manca della Lituania
Date: 2006-04-09
Category: Lituania
Slug: sabato-sera-a-confronto-cosa-mi-manca-della-lituania
Author: Karim N Gorjux
Summary: L'inizio è dei più classici, il sabato sera si introduce con la tradizionale mangiata in pizzeria. Il gruppo è quello della palestra e quindi sia io che Matteo abbassiamo completamente l'età media[...]

L'inizio è dei più classici, il sabato sera si introduce con la tradizionale mangiata in pizzeria. Il gruppo è quello della palestra e quindi sia io che Matteo abbassiamo completamente l'età media della compagnia.  
  
Il casino è infernale, ricordo che Rita prendeva il mal di testa ad andare a mangiare fuori. La stessa cosa succede a me ora; in Lituania (e in tanti altri paesi del mondo) nei locali pubblici **le persone parlano sottovoce** in modo da non scassare le palle a dismisura. Da noi, invece, compensa la cucina, generalmente mangio meglio qui in Italia.


 La visita ai locali notturni della città di Cuneo, denota la prima grande differenza: l'affollamento. Nei locali che frequento sporadicamente in Lituania, la maggior parte delle volte, ti puoi sedere e puoi andare dove vuoi per il locale senza dover per forza capire cosa prova un maiale ammassato nelle gabbie con i suoi simili. Gli uomini sono di meno, ma bevono molto di più. Ogni locale ha la sua sala da ballo, piccola o grande che sia, che divide il locale per chi vuole stare tranquillo e chi invece vuole divertirsi. Ieri sera sono finito in un pub, dove un deejay spaccava le orecchie alle 2 di notte a 10 clienti in croce seduti ai tavolini. In Lituania se non c'è gente che balla, la musica si mette in sottofondo. Il kurpiai, ad esempio, ogni sera ha una band che suona dal vivo. Il concerto ha un paio di pause di 15 minuti che spezzano la serata e **durante queste pause la musica si sente appena.  
**  
In Lituania non ho mai aspettato per entrare in una discoteca, ho sempre pagato e **nessuno mi ha squadrato all'entrata dicendomi come mi devo vestire.** 

 Da incorniciare: ieri sera cerco di girare per il garage, mentre fatico a passare tra un maiale e l'altro, noto che una tipa è seduta in mezzo al corridoio su uno sgabello da banco. La guardo con la faccia tipo *"comoda la vita..."*, il pivello che le saltava intorno mi ammonisce subito con un "no no.." per stroncare un presunto mio corteggiamento.


 Ho solo due parole per lui: *Buona Fortuna. *  
  
  
adsense  
  


