Title: La dura vita dell'imprenditore
Date: 2007-10-03
Category: altro
Slug: la-dura-vita-dellimprenditore
Author: Karim N Gorjux
Summary: Chi crede che avere una società informatica equivalga a chiudersi in una stanza a sviluppare giorno e notte è completamente fuori strada. Una società informatica (software house) sviluppa un software che deve[...]

Chi crede che avere una società informatica equivalga a chiudersi in una stanza a sviluppare giorno e notte è **completamente** fuori strada. Una società informatica (software house) sviluppa un software che deve rispondere ad un bisogno; chi ha questo bisogno, nel 99% dei casi, non è un programmatore e sa ben poco di informatica.


  
La software house deve occuparsi di essere vicina agli utilizzatori e deve anche vendere il suo prodotto perché alla fine della fiera se non si vende, non si va da nessuna parte e questo vale per qualsiasi società a fini di lucro: **bisogna continuamente trovare nuovi clienti**.


  
Il programmatore deve **saper ascoltare** i clienti, capire le paure velate, sapere come esaudire i **bisogni** del cliente. Il programmatore deve anche **saper vendere**. In questo periodo telefono ai miei potenziali clienti e per poter avere degli appuntamenti in cui posso "dimostrare" il mio software. Generalmente le ditte hanno un commerciale che, grazie a laute commissioni, trova nuovi clienti per la ditta, ma il distacco cliente - commerciale - programmatore **penalizza il software** che si "allontana" dalle vere esigenze del cliente.


  
Questo periodo è per me **fondamentale** perché una volta che sarò un buon venditore potrò passare alla fase successiva: **reclutare**.


  
Caratteristiche del buon venditore: ottimismo, entusiasmo, buon incassatore, sorridente, motivato, intraprendente, astuto.


  
E' dura.


  


