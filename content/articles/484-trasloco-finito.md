Title: Trasloco finito
Date: 2008-09-10
Category: Lituania
Slug: trasloco-finito
Author: Karim N Gorjux
Summary: Per finire il trasloco mancava solo una cosa: la nostra roba. Dovessi fare nuovamente il trasloco ora, procederei in modo leggermente diverso, ma ce la siamo cavata egregiamente lo stesso, soprattutto grazie[...]

Per finire il [trasloco](http://www.traslochi365.it) mancava solo una cosa: la nostra roba. Dovessi fare nuovamente il trasloco ora, procederei in modo leggermente diverso, ma ce la siamo cavata egregiamente lo stesso, soprattutto grazie a mia madre che si è occupata di sbrigarmi le faccende "noiose" in Italia.


 Per fortuna che ho ascoltato Flavio e mi sono segnato tutto ciò che ho messo negli scatoloni. Ho scritto un piccolo programma su FileMaker per aiutarmi nella catalogazione.  
Per ogni pacco inserivo delle informazioni sul contenuto, il peso, il volume e una volta finito di riempire scatoloni ho potuto stampare le etichette autoadesive da applicare su ogni pacco, ho stampato la lista da consegnare al corriere con i totali e soprattutto ora posso sapere dove si trova la roba di cui ho bisogno prima di aprire il pacco.


 Come spedizioniere ho usato [Italbaltic](http://www.italbalticgroup.it/) che generalmente lavora con le aziende, ma non disdegna i privati. Per un totale di 390kg di roba per circa 2 m3 di volume ho pagato 400€. Ritiro in Italia il 4 Settembre, consegna a Klaipeda il 9 Settembre. Assolutamente fantastico, quando avevo spedito dalla Lituania all'Italia ho superato i 10 giorni di attesa.


