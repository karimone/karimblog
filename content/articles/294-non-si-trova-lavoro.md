Title: Non si trova lavoro...
Date: 2007-01-30
Category: Lituania
Slug: non-si-trova-lavoro
Author: Karim N Gorjux
Summary: La situazione qui in Italia è dura e me ne sono accorto, ma in ogni caso, prima di andare a fare il dipendente, avrei messo su la mia ditta. Se non lo[...]

La situazione qui in Italia **è dura** e me ne sono accorto, ma in ogni caso, prima di andare a fare il dipendente, avrei messo su la mia ditta. Se non lo faccio ora, non lo faccio mai più. Cosa mi fa ridere è che ho trovato un collaboratore, egregiamente laureato, con delle indiscusse capacità, ma assolutamente senza lavoro. Quando l'ho conosciuto mi chiedevo il perché non trovasse lavoro, poi quando ho iniziato a collaborare con lui ho capito tutto.


 Fired!

 Bisognerebbe mandarlo in Lituania a mettere la sua bella impresa a Vilnius.. vero [Max](http://www.massimoconsulting.com)?! :lol\_tb:

