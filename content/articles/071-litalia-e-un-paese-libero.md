Title: L'Italia e' un paese libero?
Date: 2005-05-11
Category: altro
Slug: litalia-e-un-paese-libero
Author: Karim N Gorjux
Summary: Col cazzo! Secondo la casa delle liberta' (quella vera, non quella di Berlusca), l'Italia e' al 74esimo posto della graduatoria sulle liberta' di stampa ed e' classificato come paese parzialmente libero. La[...]

Col cazzo! Secondo la casa delle liberta' (quella vera, non quella di Berlusca), l'Italia e' al 74esimo posto della graduatoria sulle liberta' di stampa ed e' classificato come paese *parzialmente libero*. La Lituania, dove mi trovo ora, e' un *paese libero* ed e' al 35esimo posto, persino la Grecia e il Ghana sono davanti a noi. Se volete verificare voi stessi controllate la graduatoria su [questo documento](http://www.freedomhouse.org/pfs2004/pfs2004.pdf) a pagina 18.


 [Freedom House](http://www.freedomhouse.org/) si presenta come: *a non-profit, nonpartisan organization, is a clear voice for democracy and freedom around the world.*

 Fonte: [beppegrillo.it](http://www.beppegrillo.it/archives/2005/04/fininvest_contr.html)

