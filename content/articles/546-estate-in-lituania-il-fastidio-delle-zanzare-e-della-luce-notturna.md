Title: Estate in Lituania: il fastidio delle zanzare e della "luce notturna"
Date: 2009-06-04
Category: Lituania
Slug: estate-in-lituania-il-fastidio-delle-zanzare-e-della-luce-notturna
Author: Karim N Gorjux
Summary: Non fa caldissimo, ci sono stati un paio di giorni dove la colonnina di mercurio ha toccato i 25 gradi, ma nel complesso si sta bene. C'è sempre un vento fresco e[...]

[![](http://farm4.static.flickr.com/3386/3594139623_5f780c9600_m.jpg "Molo di Palanga")](http://www.flickr.com/photos/39283264@N00/3594139623/)Non fa caldissimo, ci sono stati un paio di giorni dove la colonnina di mercurio ha toccato i 25 gradi, ma nel complesso si sta bene. C'è sempre un vento fresco e le temperature stanno rigidamente sotto i 20 gradi, ma purtroppo non è tutto rose e fiori.  
  
Solamente due cose mi danno fastidio dell'estate lituana: le zanzare e la "luce notturna".


 Le zanzare sono fastidiosissime, sono dappertutto e non ti lasciano un secondo. Per sopravvivere è d'obbligo premunirsi di anti-zanzare altrimenti visitare posti come Nida può diventare un impresa ardua.  
Domenica mi sono fatto una bella passeggiata nella foresta è stato un calvario vero e proprio, ancora oggi metto le pomate per combattere l'irritazione.


 La "luce notturna" in realtà non è un problema vero e proprio o almeno non lo sarebbe in Italia perché noi usiamo una delle più belle invenzioni dell'uomo: **le tapparelle**. Qui in Lituania che le tapparelle dovrebbero essere d'obbligo per legge, in realtà nessuno le conosce. Alle 11 di sera circa inizia a far buio per poi ricominciare la giornata alle 4 del mattino; se penso che a Luglio sarà ancora peggio, mi vien voglia di mettermi le tapparelle da solo.


 Mia suocera si difende con dei tendoni da circo a coprire le finestre. Nell'appartamento dove vivo io, il padrone di casa viveva con delle tendine "bianche" che coprono tutta la finestra. Che mente malsana può avere la brillante idea di mettere delle tendine bianche?

 Non voglio tediarti troppo con le mie lamentele, ieri pomeriggio pioveva e la temperatura é scesa fino a 9 gradi. Oggi invece il cielo è sporadicamente nuvoloso, ma la temperatura si avvicina ai 15 gradi; se penso che tra poco in Italia si arriverà all'afa dei 30 gradi, mi rallegro e non mi lamento. Sorrido, qui ho il mare, il fresco, tanti fiori sia su radici che su due gambe. Mi preparo per un estate di vera vacanza in Lituania.


 Link: [Stampa di Cuneo del 4 Giugno](http://rapidshare.com/files/240612449/La_Stampa_Cuneo_2009-06-04.pdf)  
Link: [Stampa del 4 Giugno Internazionale](http://rapidshare.com/files/240612447/La_Stampa_2009-06-04.pdf)

