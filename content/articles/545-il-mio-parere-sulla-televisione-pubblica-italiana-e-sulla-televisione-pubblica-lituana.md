Title: Il mio parere sulla televisione pubblica italiana e sulla televisione pubblica lituana
Date: 2009-05-27
Category: Lituania
Slug: il-mio-parere-sulla-televisione-pubblica-italiana-e-sulla-televisione-pubblica-lituana
Author: Karim N Gorjux
Summary: La televisione italiana è una delle cose dell'Italia che mi manca di meno. Ti suggerisco di guardare questi video, non è mia intenzione fare commenti o suscitare polemiche, hai una tua opinione[...]

[![Prova di trasmissione](http://www.karimblog.net/wp-content/uploads/2009/05/test-tv.jpg "Prova di trasmissione")](http://www.karimblog.net/wp-content/uploads/2009/05/test-tv.jpg)La televisione italiana è una delle cose dell'Italia che mi manca di meno. Ti suggerisco di guardare questi video, non è mia intenzione fare commenti o suscitare polemiche, hai una tua opinione e mi piacerebbe leggerla come commento a questo articolo.


 Il corpo delle donne [1](http://www.youtube.com/watch?v=_f_4M3ZFC6A "Il corpo delle donne parte 1") [2](http://www.youtube.com/watch?v=Y_Ne5vBp5L4 "Il corpo delle donne parte 2") [3](http://www.youtube.com/watch?v=Gpwpr2xuTuo "Il corpo delle donne parte 3"): documentario in tre parti sull'(ab)uso della donna nella tv italiana.  
  
[Estratto da Ciao Darwin](http://www.youtube.com/watch?v=sRQGyLHBooM): piccolo esempio di cosa si vede in televisione. Ho messo questo video perché **la modella in questione è lituana** e parla lituano. Notate le espressioni degli uomini in sala.


 [Alberto Sordi e la televisione](http://www.youtube.com/watch?v=6wj0kuBsLt0): Albertone dice la sua sulla televisione di "oggi" tratto dal film "Il comune senso del pudore" (1976).


 [Pasolini intervistato da Biagi](http://www.youtube.com/watch?v=A3ACSmZTejQ): l'Italia di altri tempi con Pasolini che esprime la sua opinione sulla televisione. 

 Ecco uno dei difetti della tv italiana quando dovrebbe essere seria: a "Porta a Porta" Donadi e Ghedini ["parlano" del caso Mills](http://www.youtube.com/watch?v=c6yAx8Jq_Vo). La discussione ha il potere di innervosire lo spettatore tanto da spingerlo a cambiare canale. Per capire qualcosa bisogna vedere il video [con i sottotitoli](http://www.youtube.com/watch?v=lf2prsonpSc).  
  
  
L'esempio non era il più calzante visto l'argomento trattato, ma la questione è che la TV italiana è solo una grande accozzaglia di rumore, tette, cazzate ed inutilità. Se c'è qualcosa di serio, in un modo o nell'altro, si fa di tutto per farlo diventare [trash](http://www.youtube.com/watch?v=I5Vafs0xZKs). Programmi come "la talpa", "il grande fratello" e varietà come "buona domenica" o rotocalchi come "studio aperto" sono **un insulto all'intelligenza**. La televisione è dannosa; per "par condicio" ad ogni programma stupido mandato in onda, dovrebbe esserci un "Piero Angela" in onda su un canale concorrente per marcare in modo inequivocabile la differenza tra stupidità ed intelligenza. Ma purtroppo ciò non accade, i programmi seri vengono mandati in onda nelle ore notturne e durante il giorno competono tra di loro solamente programmi stupidi.


 **Com'è la televisione lituana?**  
Monotona, noiosa e povera di contenuti. C'è sempre qualcuno che canta, un programma a premi o un telefilm sudamericano doppiato alla "paese dell'est". Il problema penso sia solo monetario, non hanno i mezzi qui in Lituania per creare contenuti di qualità per una popolazione che supera appena i 3 milioni di abitanti. Sinceramente io preferirei sottitolassero tutto come già succede con alcuni programmi importati dall'estero, il doppiaggio lituano rispetto ai nostri doppiaggi, secondo me, rovina il film; ma i lituani ci tengono molto alla loro cultura e sentire il film in lingua lituana è qualcosa per loro irrinunciabile.


 Durante lo zapping televisivo, a qualsiasi ora c'è uno spettacolo con [qualcuno che canta](http://www.youtube.com/watch?v=6EwwtOlgLDc). A me sinceramente questo tipo di programmi deprimono, sarà che il più delle volte sono stato costretto a sorbirmi questi programmi a casa dei suoceri, in un ambiente già difficile di per sé, ma la sensazione è davvero pessima. Degni di nota sono i programmi per i bambini che sono leggermente più curati e dai contenuti più ricchi. Forse la Lituania ci tiene all'istruzione del suo popolo?

 A volte bisognerebbe riuscire a prendersi un intervallo e ricordarsi che c'è un mondo vero al di fuori della televisione; anzi sarebbe meglio spegnere la televisione del tutto e dedicarsi ai libri e ad internet, il vero e unico media democratico del nuovo millennio.  
  
  


 Link: [Cosa manca alla televisione italiana?](http://www.youtube.com/watch?v=HjNttn6J1CA)  
Link: [La televisione e i danni sul cervello](http://www.disinformazione.it/televisione.htm)

