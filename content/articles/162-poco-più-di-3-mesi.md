Title: Poco più di 3 mesi
Date: 2006-04-13
Category: altro
Slug: poco-più-di-3-mesi
Author: Karim N Gorjux
Summary: Piano piano si concretizza nel mio cervello l'idea di aver aggiunto una foglia nel mio albero genealogico. La notte faccio i sogni più disparati dove naturalmente i protagonisti sono i bambini. Una[...]

Piano piano si concretizza nel mio cervello l'idea di aver aggiunto una foglia nel mio albero genealogico. La notte faccio i sogni più disparati dove naturalmente i protagonisti sono i bambini. Una cosa me la devo segnare subito, avrò tante cose da insegnare al mio bimbo, ma questa la deve capire subito: *"Se aspetti gli altri, stai fresco! Conta sulle tue forze!"*

