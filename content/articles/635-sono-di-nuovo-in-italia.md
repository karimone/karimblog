Title: Sono di nuovo in Italia
Date: 2011-04-26
Category: altro
Slug: sono-di-nuovo-in-italia
Author: Karim N Gorjux
Summary: Ci sono, è vero che è più di un mese che non scrivo nulla, ma ci sono. Il trasloco internazionale non è stata una passeggiata e rimettersi qui a posto con l'ufficio[...]

Ci sono, è vero che è più di un mese che non scrivo nulla, ma ci sono. Il trasloco internazionale non è stata una passeggiata e rimettersi qui a posto con l'ufficio ed il collegamento ad internet ha richiesto del tempo.


 Ora che sono in Italia il blog non morirà anche se la sua identità è identificata con la mia vita da italiano in Lituania però ho ancora molto da scrivere ed in più c'è la seria possibilità di fare un libro di tutto ciò che ho scritto.


 Ho varie cose in testa e molto da fare. 

 Abbi pazienza!

