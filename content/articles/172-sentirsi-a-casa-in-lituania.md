Title: Sentirsi a casa in Lituania
Date: 2006-05-04
Category: Lituania
Slug: sentirsi-a-casa-in-lituania
Author: Karim N Gorjux
Summary: Dover rinunciare a certe abitudini è problematico, in Italia si mangia tutti riuniti alle stesse ore canoniche, qui invece, ognuno fà quello che vuole. Le mie solite abitudini, come sentire i titoli[...]

Dover rinunciare a certe abitudini è problematico, in Italia si mangia tutti riuniti alle stesse ore canoniche, qui invece, ognuno fà quello che vuole. Le mie solite abitudini, come sentire i titoli di apertura del TG o ascoltare lo zoo di 105 in macchina, sono difficilmente rinunciabili e quindi, mi sono dovuto attrezzare.  
 technorati tags start Technorati Tags: [giornali](http://www.technorati.com/tag/giornali), [iptv](http://www.technorati.com/tag/iptv), [klaipeda](http://www.technorati.com/tag/klaipeda), [podcast](http://www.technorati.com/tag/podcast), [p2p](http://www.technorati.com/tag/p2p), [webradio](http://www.technorati.com/tag/webradio)

 technorati tags end   


 Ero indeciso se installare la parabola e guardare la tv italiana, ma una parabola costa (cara) e richiede anche il permesso del proprietario dell'appartamento. Ho preferito puntare tutto su internet, ho speso qualche € in più e ora posso vantarmi di avere un'adsl che raggiunge 40kb/s sul p2p e i 100kb/s su web. La differenza è dovuta allo strana politica di abbonamento dalla [balticum](http://www.balticum.lt/?c=1&p=3&t=1&tt=19&tid=3&sp=20) (l'equivalente funzionante della telecom).


 Riesco a leggere [La Stampa](http://www.lastampa.it) (inclusa la pagina di Cuneo), direttamente dal sito web. Ho comprato 30 numeri a 0,50€ l'uno (per un totale di 16€) che posso leggere quando e come voglio nell'arco di 1 anno, la stessa cosa l'ha fatta il mio mac-amico [rox](http://www.rossoni.org) con cui mi alterno nell'acquisto del giornale che prontamente ci scambiamo grazie a [pando](http://www.pando.com/beta).


 Le radio le posso sentire senza problemi grazie al widget di [melamangio.com](http://www.melamangio.com). Il funzionamento è fantastico, seleziono la radio che mi interessa e clicco su play, il resto lo fanno le orecchie :-) Come se non bastasse, ci sono i podcast, dove posso scaricare vari pezzi di [Radio Repubblica](http://repubblicaradio.repubblica.it/home_page.php) da ascoltare sull'ipod.  
  
![Widget-Radio](http://www.karimblog.net/wp-content/uploads/2006/05/Widget-Radio.jpg "Widget-Radio")  


 Il problema più grosso rimane la televisione. Su Mac ho qualche problema a guardare lo streaming delle televisioni italiane. Riesco a guardare senza problemi i telegiornali sul sito della [rai](http://www.rai.it), mentre i programmi su [raiclicktv](http://www.raiclicktv.it/raiclickpc/secure/homePage.srv) mi creano qualche fastidio. Forse il problema è la bandalarga, penso che per guardare la tv ci voglia almeno un [paio di mbit](http://www.telecom.lt/en/home/internet/plans/most_popular), servizio che qui a Klaipeda penso manco che ci sia.


 Al momento sono più che contento, uso [limewire](http://www.limewire.org) per scaricare film, telefilm, documentari o quello che passa il convento; nella maggior parte dei casi la qualità del materiale è pessima, ma visto che non esiste uno store online per i contenuti audiovisivi bisogna accontentarsi. Prossimo passo: ottenere il famigerato HighId con [amule](http://www.amule.org).


 Concludendo: *datemi una pizza e mi sento a casa* :-)  
  
  
adesense  
  


