Title: Il peggio è passato
Date: 2007-02-28
Category: altro
Slug: il-peggio-è-passato
Author: Karim N Gorjux
Summary: Ho da rimpiangere la burocrazia lituana, loro sono quattro gatti e hanno meno scartoffie da compilare qui è un macello, ma alla fine ce l'ho fatta. Ho la società "Naxia s.n.c.".

Ho da rimpiangere la burocrazia lituana, loro sono quattro gatti e hanno meno scartoffie da compilare qui è un macello, ma alla fine ce l'ho fatta. Ho la società "Naxia s.n.c.".


 Avrei risparmiato sicuramente 15-20 giorni se in Lituania mi fossi sposato con la divisione dei beni, invece qui mi è toccato spendere quattrocentocinquanta € di simpaticissimo notaio da aggiungere ai millecinquecento € di costituzione società. Quando sono uscito dall'ufficio del notaio, avevo voglia di piangere.... ma no! Ridiamoci su, **ora sono imprenditore!  
**  
  
  
  
[![CIMG5110](http://farm1.static.flickr.com/175/404400065_d6e8638f18_m.jpg)](http://www.flickr.com/photos/kmen-org/404400065/ "Photo Sharing")

