Title: Un paio di sere fa..
Date: 2007-09-26
Category: Lituania
Slug: un-paio-di-sere-fa
Author: Karim N Gorjux
Summary: Un paio di sere fa ero da un mio cliente che, oltre ad essere un mio cliente, è anche un mio coetaneo. A dirla tutta circa 20 anni fa era anche uno[...]

Un paio di sere fa ero da un mio cliente che, oltre ad essere un mio cliente, è anche un mio coetaneo. A dirla tutta circa 20 anni fa era anche uno dei miei compagni alle scuole elementari. (Quante botte che gli davo...)

  
Mentre pacioccavo sul suo computer, mi fa vedere le foto della sua ultima vacanza che manco a dirlo se l'è fatta proprio in Lituania. Vilnius, Trakei, Klaipeda... Klaipeda. A dire il vero di Klaipeda aveva poche foto, il battello di per Nida, un ponte, due case e il Kurpiai. Il Jazz Club più famoso della Lituania, il Kurpiai.


  
Le foto del Kurpiai mi confondevano, per prima cosa perché le foto le ha fatte di giorno e non c'era tanta gente, ma il senso di inquietudine che mi avvolgeva era dovuto al ricordo delle serate mie e di [Enrico](http://it.wikipedia.org/wiki/Enrico_Quinto) in quel locale. Mi succede occasionalmente, mentre guido o mentre guardo la foto del mio matrimonio sul comò, di pensare a com'era Klaipeda in sua compagnia. Mi vengono in mente cose buffe, i suoi racconti, la sua cagnolina [Baranka](http://www.flickr.com/photos/kmen-org/249162413/), i suoi litri di birra, la sua risata nascosta da quella mano che sembrava un badile, la sua insormontabile timidezza latente.


  
Pensare di tornare a Klaipeda mi angoscia. Appena penso a Klaipeda mi viene in mente quella passeggiata che stavo facendo da solo il pomeriggio del 22 Settembre 2006, giusto 2 giorni dopo la morte di Enrico. Dal Memelis camminavo verso Tilto Gatvé, a testa bassa, guardandomi le scarpe. Mancava solamente la pietrolina da calciare e, mentre camminavo, avevo chiaro in mente che la città dov'è nata mia figlia, per me, non sarebbe mai stata più la stessa.


  
  
 [![Matrimonio07](http://farm1.static.flickr.com/67/221912455_4e7b283e61_m.jpg)](http://www.flickr.com/photos/kmen-org/221912455/ "Condivisione di foto")  


