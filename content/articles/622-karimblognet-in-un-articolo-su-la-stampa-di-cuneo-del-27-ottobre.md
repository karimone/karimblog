Title: karimblog.net in un articolo su "La Stampa di Cuneo" del 27 Ottobre
Date: 2010-11-02
Category: altro
Slug: karimblognet-in-un-articolo-su-la-stampa-di-cuneo-del-27-ottobre
Author: Karim N Gorjux
Summary: Il 27 Ottobre 2010 sono apparso sulla stampa di Cuneo con un bell'articolo di mezza pagina comprensiva di foto e scopiazzatura del mio articolo sul problema del cognome. Il motivo della mia[...]

[![](http://www.karimblog.net/wp-content/uploads/2010/11/lastampa.gif "lastampa")](http://www.karimblog.net/wp-content/uploads/2010/11/lastampa.gif)Il 27 Ottobre 2010 sono apparso sulla stampa di Cuneo con un bell'articolo di mezza pagina comprensiva di foto e scopiazzatura del mio articolo sul [problema del cognome](http://www.karimblog.net/2010/10/14/follie-della-lituania-non-posso-dare-a-mio-figlio-il-mio-cognome/ "Articolo sul problema del cognome in Lituania"). Il motivo della mia apparizione è la rubrica "*Cuneesi nel mondo*", il giornalista che la cura, mi ha contattato grazie alla segnalazione di [un mio amico che vive a Zurigo](http://www.facebook.com/#!/michelecumar "Pagina Facebook di Michele Cumar"). Il giornalista mi ha intervistato al telefono in una chiacchierata di 15 minuti ed è rimasto colpito del fatto che lavoro con l'Italia dalla Lituania.


 Mi è piaciuta l'osservazione *"Lui, italiano con nome straniero, e lei, lituana con nome italiano."* e mi sono fatto una risata quando ho letto "*Mi adatto alle loro abitudini: l’in- verno scorso giocavamo a calcetto all’aperto con 30 gradi sottozero*" che è stato scritto più che altro per fare sensazionalismo dato che ho ben sottolineato che l'ultimo inverno abbiamo toccato i -30 di notte, ma siamo arrivati senza problemi ai -20 di giorno. Basta chiacchiere! Leggiti l'articolo in [pdf](http://www.karimblog.net/wp-content/uploads/2010/11/Karim.stampa.2010.10.27.pdf "Karim sulla Stampa del 27 Ottobre 2010 (pdf)") (624kb) o [direttamente nel browser](http://www.karimblog.net/wp-content/uploads/2010/11/Karim.stampa.2010.10.jpg "Karim sulla Stampa del 27 Ottobre 2010 (immagine)") (135kb).


