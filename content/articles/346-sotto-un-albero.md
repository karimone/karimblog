Title: Sotto un albero
Date: 2007-07-10
Category: altro
Slug: sotto-un-albero
Author: Karim N Gorjux
Summary: Oggi sono scappato da casa; subito dopo pranzo sono scappato da casa e famiglia per andare a sdraiarmi sotto un albero. Ci sono rimasto circa 1 ora e mi sono pure addormentato.[...]

Oggi sono scappato da casa; subito dopo pranzo sono scappato da casa e famiglia per andare a sdraiarmi sotto un albero. Ci sono rimasto circa 1 ora e mi sono pure addormentato. E' stato bello, il terreno era quasi soffice e gli uccelli cinguettavano, mi sembrava di essere collegato perennemente a [Twitter](http://twitter.com/home).


