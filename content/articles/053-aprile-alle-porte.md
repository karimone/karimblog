Title: Aprile alle porte
Date: 2005-03-28
Category: altro
Slug: aprile-alle-porte
Author: Karim N Gorjux
Summary: Ormai ho superato i 30 giorni di permanenza in Italia, tra poco sarà Aprile mese di cui vedrò la fine con la mia dolce metà. In questo momento sono scollegato, sono le[...]

Ormai ho superato i 30 giorni di permanenza in Italia, tra poco sarà Aprile mese di cui vedrò la fine con la mia dolce metà. In questo momento sono scollegato, sono le 04:28 di mattina, il cellulare è spento da ieri sera e quando mi collegherò ad internet userò il modem 56k come già facevo nel 1997, tutto procede normalmente. Ho da studiare fin sopra i capelli, ma dovrei risolvere tutti i miei problemi.


 Mi rendo conto che il tempo qui è passato più veloce di quanto pensassi, non sono ancora uscito un sabato sera e se non mi sbrigo rischio di non uscire mai. Al momento l'unico episodio emozionante è stato assistere in diretta al programma "Si O Si Si O" (spelling in inglese della parola cocco) a Radio Deejay, questo grazie a Cristian che è amico di Claudio Coccoluto, il deejay che cura il programma in questione. Esperienza strana perchè solo il giorno dopo mi sono reso conto di aver assistito a qualcosa che per qualcun'altro poteva essere particolarmente speciale, ma io, con tutto il rispetto per Claudio, non sapevo manco chi fosse.


 Ho visto **Donnie Darko** film particolare di cui mi sono permesso di fare una copia di backup ;-) è stato smontato dalla critica, ma a dire il vero è un film che mi ha smontato. Indefinibile nel genere, pazzesco nella trama, per poterci capire qualcosa bisogna sbattersi a capire le interpretazioni tramite forum, discussioni, faq e altro frugando in rete. Mi sento fortunato perchè penso a chi ha bocciato il film senza cercare nulla in internet. Ne parlerò in un altro post.


