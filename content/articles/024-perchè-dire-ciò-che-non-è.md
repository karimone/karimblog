Title: Perchè dire ciò che non è?
Date: 2004-11-25
Category: altro
Slug: perchè-dire-ciò-che-non-è
Author: Karim N Gorjux
Summary: Molti anni fà mi capitò di leggere "I Viaggi di gulliver" di Jonathan Swift (1667-1745), prete Irlandese che scrisse questo meraviglioso romanzo "per bambini", ma che in realtà era destinato, con incredibili[...]

Molti anni fà mi capitò di leggere "I Viaggi di gulliver" di Jonathan Swift (1667-1745), prete Irlandese che scrisse questo meraviglioso romanzo "per bambini", ma che in realtà era destinato, con incredibili metafore, a criticare il mondo degli adulti già allora dilaniato dall'egoismo e dalla superficialità. Se avete letto il libro vi ricorderete la diatriba dei lillipuziani su come dovevano essere rotte le uova, stupenda metafora-satira sui problemi anglo-irlandesi. In informatica la codifcazione little indian - big endian è un omaggio al grande Swift.


 Il libro In sintesi: Lemuel Gulliver, medico su una nave mercantile, fa naufragio sull'isola di Lilliput, dove tutto, a cominciare dagli abitanti, è grande un quindicesimo delle persone e degli oggetti quali li conosciamo noi. Nella seconda parte invece Gulliver visita Brobdingnag, dove il rapporto è rovesciato e dove il medico diventa trastullo della figlia del re, che lo tiene tra i suoi balocchi. Nella terza parte Gulliver visita Laputa e il continente che ha come capitale Lagado, dove la satira si rivolge contro filosofi, storici e inventori. Nell'isola di Glubdubdrib, poi, Gulliver evoca le ombre dei grandi dell'antichità e dalle loro risposte ne scopre i vizi e le meschinità; mentre presso gli Struldbrug, immortali, si accorge che la massima infelicità dell'uomo sarebbe la prospettiva di non porre mai fine al tedio di vivere. Nella quarta parte, infine, la virtuosa semplicità dei cavalli Houyhnhnm è messa a contrasto con la nauseabonda brutalità degli Yahoo, bestie dall'aspetto umano.


 Proprio di quest'ultimo viaggio vorrei puntualizzare alcuni particolari che allora, quando lessi il libro mi impressionarono molto. I cavalli Houyhnhnm parlano. Gulliver, vivendo con loro, impara la loro lingua e si rende conto che nel vocabolario dei cavalli non esiste una parola per difinire la bugia. Gli Houyhnhnm rispondono a questa loro mancanza dicendo: "Dato che la parola serve per comunicare qualcosa, a che scopo dovrei comunicare qualcosa che non è?". Più Gulliver cercava di far capire che il suo popolo di uomini viveva all'ombra della bugia e più i cavalli non capivano.


 Anche oggi mi capita ogni giorno di dover "sopportare" piccole bugie a cui ognuno di noi non dà importanza, dato che ormai sono ben radicate nel nostro animo umano, ma fate attenzione ad usare queste bugie o false promesse con i bambini. Loro sono ancora innocenti e puri, non sono ancora stati dilaniati dalla cruda e triste realtà del mondo che ci siamo creati. Abituateli poco a poco facendogli capire che le persone parlano tanto per parlare, esattamente come noi.


 Swift fu ordinato prete anglicano e vicario. Su un muro della cattedrale di san Patrizio - nella quale è stato sepolto si può leggere l'epitaffio che aveva scritto per sé: "Qui è sepolto il corpo di Jonathan Swift…, qui dove lo sdegno feroce non può più lacerare il suo cuore. O passante, vai ed imita - se puoi - uno chi si è sforzato con tutta la sua energia di difendere la libertà."

 Un'ultima cosa... ora sapete da dove hanno preso spunto per dare un nome al sito [Yahoo](http://www.yahoo.com)

