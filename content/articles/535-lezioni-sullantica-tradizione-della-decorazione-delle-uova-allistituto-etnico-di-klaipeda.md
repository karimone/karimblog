Title: Lezioni sull'antica tradizione della decorazione delle uova all'istituto etnico di Klaipeda
Date: 2009-04-10
Category: Lituania
Slug: lezioni-sullantica-tradizione-della-decorazione-delle-uova-allistituto-etnico-di-klaipeda
Author: Karim N Gorjux
Summary: Invece della solita lezione di lituano, ieri siamo andati al centro etnico di Klaipeda per disegnare sulle uova. La Pasqua lituana non è molto differente dalla Pasqua italian, qui le uova le[...]

[![Fernando e le Uova](http://www.karimblog.net/wp-content/uploads/2009/04/uova_1-225x300.jpg "Fernando e le Uova")](http://www.karimblog.net/wp-content/uploads/2009/04/uova_1.jpg)Invece della solita lezione di lituano, ieri siamo andati al centro etnico di Klaipeda per disegnare sulle uova. La Pasqua lituana non è molto differente dalla Pasqua italian, qui le uova le disegnano, in Italia le uova sono al cioccolato e si mangiano. Anche qui la domenica di Pasqua si mangia fino a scoppiare, ma della Pasquetta non so dirvi ancora nulla, vi farò sapere Martedì.


 L'istituto etnico lituano è situato nel centro storico di Klaipeda, tutto è nuovo, moderno e pulito. Alcune ragazze ed un'anziana signora, ci accolgono e accompagnano al piano superiore dove, in una stanza enorme, un grande telo di plastica viene usato da tappeto per un lunghissimo tavolo di legno.   
  
Ai fianchi del tavolo, per tutta la lunghezza, due panche di legno ci presentano quello che sarà il nostro piano di lavoro. [![Precisione](http://www.karimblog.net/wp-content/uploads/2009/04/uova_2-300x225.jpg "Precisione")](http://www.karimblog.net/wp-content/uploads/2009/04/uova_2.jpg)  
Sul tavolo una lunga tovaglia ospita dei piccoli lumini (quelli che vengono usati nei cimiteri) con sopra un piccolo rialzo di rete metallica, e sopra la rete, un piccolo contenitore con della cera calda. La prima cosa che mi è venuta in mente è di prendere una di quelle reti metalliche e portarmele a casa per farci la base della bagna caoda.[![Uova-3](http://www.karimblog.net/wp-content/uploads/2009/04/uova_3-300x225.jpg "Uova-3")](http://www.karimblog.net/wp-content/uploads/2009/04/uova_3.jpg)

 L'anziana signora che ci accoglie, ha delle mani enormi, capelli lunghi biondi raccolti dietro la nuca, occhi azzurri e un viso consumato dalle esperienze della vita. Non si capisce bene quanti anni possa avere, potrebbe avere 55 anni portati malissimo come 80 anni portati egregiamente, ma durante la spiegazione di cosa ci avrebbe aspettato e nel raccontarci le sue tradizioni si percepiva l'entusiasmo di una ragazzina. Si vedeva chiaramente dai suoi occhi che parlare delle tradizioni del suo paese a degli stranieri, la riempiva di gioia e di orgoglio.


 [![Le nostre uova](http://www.karimblog.net/wp-content/uploads/2009/04/uova_4-300x225.jpg "Le nostre uova")](http://www.karimblog.net/wp-content/uploads/2009/04/uova_4.jpg)Appena seduti, una ragazza dai lineamenti gentili, passa a raccogliere le nostre uova per scaldarle dentro un panno caldo perché l'uovo, per essere ornato dalla cera calda, deve essere caldo come le guance. Almeno così dice la signora.


 La cera deve essere passata molto velocemente e per farlo si usa un piccolo bastoncino di legno o una matita di legno con un piccolo chiodo piantato in punta. Non è facile, prima di prendere la mano con questa nuova arte ho dovuto passare 15 minuti a capire se il difetto sono io o se il difetto è la mia "matita". Dopo aver disegnato, o meglio scarabocchiato le uova, bisogna dare il colore all'ornamento. 

 [![uova_5](http://www.karimblog.net/wp-content/uploads/2009/04/uova_5-300x225.jpg "uova_5")](http://www.karimblog.net/wp-content/uploads/2009/04/uova_5.jpg)Per dare colore alla cera, le uova vengono immerse in un colorante per qualche minuto. E' possibile dare vari colori alle uova usando 3 o 4 colori diversi per ogni fase del lavoro. I risultati non sono eccezionali, ma da cosa ho potuto vedere in alcune foto in alcuni libri sull'argomento, c'è gente davvero brava in Lituania.  
  
  
Vedi anche:  
L'antica tradizione delle uova al Balzekas Museum di Chicago [[link](http://www.balzekasmuseum.org/Pages/workshop_easter_eggs.html)]

