Title: Finalmente mi hanno messo internet
Date: 2005-09-30
Category: Lituania
Slug: finalmente-mi-hanno-messo-internet
Author: Karim N Gorjux
Summary: Come al solito non sono fortunatissimo con internet, ma oggi tutto è stato risolto. Finalmente ho una connessione quasi decente, mi aggiro sui 256 kb/s sul web. Oggi sono andato al museo[...]

Come al solito non sono fortunatissimo con internet, ma oggi tutto è stato risolto. Finalmente ho una connessione quasi decente, mi aggiro sui 256 kb/s sul web. Oggi sono andato al [museo del mare](http://www.juru.muziejus.lt/).  
  
Il museo è composto dagli acquari e dalla piscina dei delfini dove ho potuto assistere ad uno spettacolo notevole. Ho scoperto che i delfini possono fare le seguenti cose: taxi nautico, giocatore di calcio, giocatore di basket, cantante, pittore, bagnino, giocoliere. Pazzesco :-) che animali straordinari...  
  
[![CIMG1243.JPG](http://www.kmen.org/wp-content/CIMG1243-tm.jpg "CIMG1243.JPG")](http://www.kmen.org/wp-content/CIMG1243.jpg)  


 Le foto dei delfini sono praticamente venute tutte sfocate, quindi godetevi l'immagine di questa foca (?) che ci salutava dai vetri dell'acquario.


