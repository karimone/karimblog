Title: Problemi di lingua
Date: 2006-02-10
Category: Lituania
Slug: problemi-di-lingua
Author: Karim N Gorjux
Summary: La maggior parte delle discussioni che ho con Rita riguardano l'incomprensione linguistica. Io non parlo il Lituano e mi è ben difficile impararlo per svariati motivi che elencherò in seguito.Sembra che il[...]

La maggior parte delle discussioni che ho con Rita riguardano l'incomprensione linguistica. Io non parlo il Lituano e mi è ben difficile impararlo per svariati motivi che elencherò in seguito.  
  
Sembra che il lituano sia una lingua pregiata e molto apprezzata dai linguisti per svariati motivi che non saprei enunciare, sappiate solo che che è così. Fidatevi. Vorrei ricordare, però, che la lingua lituana è parlata da tre milioni e mezzo di persone, volendo posso sbilanciarmi e arrotondare a 5 milioni di persone, ma la matematica è chiara ed in questo caso mi dice che sono più le persone che parlano il piemontese che il lituano, dubito, ma non troppo, che la stessa cosa valga per il calabrese.


 Rita continua a parlarmi in inglese, io le parlo solo in Italiano, ma la metà delle volte non capisce completamente il senso del discorso e soprattutto non percepisce le sfumature della mia lingua. Al contrario di me lei ha sia i libri per studiare l'italiano su base lituana e in più segue le lezioni dal solito Enrico.


 Io non ho nessun supporto, ho un corso di lituano senza nessuna base linguistica, è solamente lituano niente più. Ho un dizionario italiano-lituano e basta. Le lezioni da Enrico non le ho più prese, non perché avessi deciso di smettere, ma solamente perché Enrico mi ha sempre detto che prima o poi mi avrebbe seguito e invece niente, ha sempre rimandato ad ogni mia richiesta e di conseguenza mi sono stufato di chiedere, lascio perdere, sicuramente avrà le sue ragioni (che non sono pecuniarie, visto che ho sempre sottolineato il fatto che avrei pagato).


 Cosa mi resta da fare? Cercare su internet, ma le risorse sono poche. Si trovano dei manuali dall'inglese, qualcosetta dal francese, ma nient'altro. Mentre cerco mi chiedo se ne valga la pena. Tanto vale che Rita impari l'italiano o al massimo che io impari il russo. Si perché il russo è parlato da 150 milioni di persone, ho il corso che avevo già iniziato dell'[assimil](http://www.assimil.it) e mi piaceva pure, purtroppo Rita non ne vuole sapere di parlare il russo..


 'ndeve a caghè! Mi tacu a parlè piemunteis..  
   
  
  
adsense  
  


