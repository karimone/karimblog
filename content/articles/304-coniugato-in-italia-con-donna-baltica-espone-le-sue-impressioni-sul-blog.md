Title: Coniugato in Italia con donna Baltica espone le sue impressioni sul blog
Date: 2007-03-16
Category: Lituania
Slug: coniugato-in-italia-con-donna-baltica-espone-le-sue-impressioni-sul-blog
Author: Karim N Gorjux
Summary: Il titolo mi ricorda parecchio uno dei film di Sordi con la bellissima Claudia Cardinale, ma rende chiara l'idea di che argomento vuole trattare questo post.

Il titolo mi ricorda parecchio [uno dei film](http://italian.imdb.com/title/tt0066824/) di Sordi con la bellissima Claudia Cardinale, ma rende chiara l'idea di che argomento vuole trattare questo post.


 Intanto è necessaria una premessa, è molto difficile poter dare delle impressioni su mia moglie senza tenere conto del suo carattere. Rita è fatta a suo modo e questo è indipendente dal fatto che sia Lituana o meno, è così e basta. Ma alcune peculiarità del suo modo di aver affrontato la vita qui in questi tre mesi derivano espressamente dalla sua natura lituana.


 Il primo punto è un denominatore comune che ho riscontrato nel 99% delle donne lituane. Testimonianze dirette o indirette mi hanno preparato a non stupirmi più quando Rita non fa nessuno sforzo per frequentare i suo connazionali qui in Italia. A circa 500 metri da casa nostra abita un'altra lituana, ho provato, senza essere troppo invadente, a cercare di farle conoscere, ma senza ottenere il minimo risultato. Altre volte è capitata la possibilità di conoscere un'altra ragazza lituana, ma da parte di mia moglie **non c'è stata la minima intenzione di fare "comunella" **all'italiana.


 Il secondo punto è molto delicato. Ho notato che Rita è una madre molto premurosa, apprensiva e responsabile. Tiene ben pulita la casa e a fatica cerca di far da mangiare commestibile (qui centra ben poco il fatto che sia lituana). Rita ha appena 22 anni, è una testona incredibile, nazionalista e orgogliosa come tutti i suoi connazionali, ma date le qualità sopra elencate posso chiudere un occhio e portare pazienza.


