Title: Tra il dire e il fare...
Date: 2007-01-29
Category: altro
Slug: tra-il-dire-e-il-fare
Author: Karim N Gorjux
Summary: Ho aspettato parecchio prima di scrivere queste righe, ma dato che uso il blog come sfogo personale è meglio che le scriva senza troppi problemi.Vi è mai capitato di sentirvi dire frasi[...]

Ho aspettato parecchio prima di scrivere queste righe, ma dato che uso il blog come sfogo personale è meglio che le scriva senza troppi problemi.  
Vi è mai capitato di sentirvi dire frasi di rito come: "Se hai bisogno chiamami", "Puoi contare su di me" etc...? A me è successo non troppo tempo fa e ovviamente è risultata una frase buttata li per quello che è. Nient'altro.


 Sono veramente poche le persone di cui ti puoi fidare sulla parola, io cerco di circondarmi da questo tipo di persone a costo di avere pochissimi amici, ma vi assicuro che come filosofia ha in serbo delle grandi soddisfazioni.


