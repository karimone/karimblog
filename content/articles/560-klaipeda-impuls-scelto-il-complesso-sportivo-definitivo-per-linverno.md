Title: Klaipeda Impuls, scelto il complesso sportivo definitivo per l'inverno
Date: 2009-08-11
Category: Lituania
Slug: klaipeda-impuls-scelto-il-complesso-sportivo-definitivo-per-linverno
Author: Karim N Gorjux
Summary: Penso di aver trovato finalmente la palestra definitiva, non tanto per questioni di scelta personali e di gusto, ma gli eventi mi hanno portato a scegliere la palestra più vicina a casa[...]

[![](http://farm3.static.flickr.com/2027/1962236218_eec8372b55_m.jpg "Golds Gym")](http://www.flickr.com/photos/mchavs/1962236218/)Penso di aver trovato finalmente la palestra definitiva, non tanto per questioni di scelta personali e di gusto, ma gli eventi mi hanno portato a scegliere la palestra più vicina a casa e che è ancora aperta. Appena sono arrivato sono andato in una palestra spartana ed economica che purtroppo ha chiuso per restauri, quindi mi sono spostato in un'altra palestra abbastanza bella, con soli attrezzi, economica e pulita. Ha chiuso anche quella.  
  
Alla fine mi sono deciso di tornare nella palestra che frequentavo già nel 2005 quando arrivai qui a Klaipeda la prima volta, il club sportivo [Impuls](http://www.impuls.lt/lt/bendras/) che oltre alla sala pesi ha anche la piscina, l'idromassaggio e due saune. Il prezzo alla fine è di tutto rispetto. Con 99 litas, ovvero 28€ al mese, ho ben 12 ingressi mensili che per ora sono più che sufficienti. Conveniente economico e soprattutto salutare.


 A Klaipeda c'è il mare e le spiagge sono belle e spaziose, ma sarà che sono italiano, sarà che l'acqua del Mar Baltico è fredda e [molto inquinata](http://news.bbc.co.uk/2/hi/europe/3007228.stm), sarà quel che sarà, ma io alla fine non ho ancora fatto un bagno al mare. Ecco spiegato il perché ieri, le mie 10 vasche in piscina, sono state più che una semplice nuotata.


