Title: L'oro colato
Date: 2008-05-14
Category: Lituania
Slug: loro-colato
Author: Karim N Gorjux
Summary: Due giorni fa (è da un po' che non scrivo..) è venuto a trovarci Henry direttamente qui a Centallo, mi ha fatto piacere, abbiamo parlato del più e del meno, ma soprattutto[...]

Due giorni fa (è da un po' che non scrivo..) è venuto a trovarci [Henry](http://www.henryblog.net) direttamente qui a Centallo, mi ha fatto piacere, abbiamo parlato del più e del meno, ma soprattutto di Mac e Lituania.


 Henry è una di quelle poche persone che ha visto mia figlia il 23 Agosto 2006 ovvero il giorno della sua nascita, è anche una di quelle poche persone che capisce cosa significa Lituania e vivere con una Lituana. Lo prova il fatto che ora è fidanzato con una **ragazza italiana** ;-)

 Oltre la Lituania, la sua passione per il Mac è spropositata, come dicevo al mio amico Guido via email, chi usa il Mac non è solo invasato, ma un **pazzo invasato**. Odiamo le ventole, rumorose i cavi disordinati, i cassoni chiamati assemblati, gli antivirus, il bad quoting e soprattutto le procedure di disinstallazione. **Siamo Mac user!  
**  
Oltre questi fantastici discorsi che non smetterei mai di affrontare abbiamo discusso, anche se per poco, dell'**oro colato**. Ovvero quella sensazione che avevo a 15-20 anni dove qualsiasi cosa mi dicessero i miei genitori era oro colato. **Il cervello si spegneva** e qualsiasi cosa loro dicevano per me era giusta. Non proprio qualsiasi, ma almeno ciò che riguardava quel mondo di adulti allora a me sconosciuto.


 La verità è ben diversa. Ogni cosa che dicono i genitori **deve** essere sempre ben ponderata e verificata, niente va preso per oro colato semplicemente perché i nostri genitori appartengono alla [generazione del Carosello](http://blacknights1.blogspot.com/2007/02/la-generazione-di-carosello.html), della stampa tutte le mattine e del telegiornale alla mezza. Noi siamo molto diversi, siamo la generazione degli [sms](http://cristis80.blogspot.com/2008/02/generazione-sms.html), youtube, blog, rss, skype accediamo ad una mole di informazione in un solo giorno che un nostro genitore poteva ottenere **solo in mesi**.


 Siamo avanti. Ed è giusto che sia così.


 Questo articolo lo metterò in cassaforte e lo farò leggere a Greta nel 2028. Cercherò di sensibilizzarla e di avere pazienza con il **vecchio rincoglionito di suo padre**.


 Ultima news: Sandro ha [cambiato indirizzo.](http://www.balticman.net/2008/05/09/wwwbalticmannet/)

