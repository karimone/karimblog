Title: Maledetto euro!
Date: 2005-07-24
Category: altro
Slug: maledetto-euro
Author: Karim N Gorjux
Summary: E' offesa. Devo dire che ci vuole coraggio per comportarsi in questo modo, ma non voglio infierire, le persone orgogliose vogliono essere senza macchia, creano e ricreano il mondo attorno a loro[...]

E' offesa. Devo dire che ci vuole coraggio per comportarsi in questo modo, ma non voglio infierire, le persone orgogliose vogliono essere senza macchia, creano e ricreano il mondo attorno a loro in modo da farlo adattare alle proprie esigenze. Come potrei mai discutere con una persona del genere senza arrabbiarmi? Impossibile, tanto vale non cominciare nemmeno il discorso.  
  
Non c'è molta gente a Demonte, ma io non credo che sia colpa dell'Euro e della crisi. Non penso sia necessario leggere [Il sole 24 ore](http://www.ilsole24ore.com/) per capirlo, ma giusto pensarci bene... se davvero la gente non ha soldi da spendere e vuole fare una vacanza, chi ci rimette sono gli alberghi. Guardando su internet i prezzi degli alberghi ti rendi conto che sono proibitivi, converrebbe comprare una tenda e andare in campeggio... costa di meno e non si rinuncia alla vacanza... ahi! Davvero il problema è l'Euro... ma suvvia!

 Perchè lo scrivo qui? E' ovvio. Senti gridare qualcuno? :-)

