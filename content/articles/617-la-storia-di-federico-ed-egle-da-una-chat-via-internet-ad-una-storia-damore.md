Title: La storia di Federico ed Egle: da una chat via internet ad una storia d'amore
Date: 2010-09-16
Category: Lituania
Slug: la-storia-di-federico-ed-egle-da-una-chat-via-internet-ad-una-storia-damore
Author: Karim N Gorjux
Summary: Conosco molte persone che grazie a Skype e i siti specializzati conoscono ragazze lituane, ma non solo lituane, via internet. Poco tempo fa ho conosciuto un medico che grazie ad internet ha[...]

[![Loving Chat..](http://www.karimblog.net/wp-content/uploads/2010/09/loving-chat.jpg "loving-chat")](http://www.karimblog.net/wp-content/uploads/2010/09/loving-chat.jpg)Conosco molte persone che grazie a Skype e i siti specializzati conoscono ragazze lituane, ma non solo lituane, via internet. Poco tempo fa ho conosciuto un medico che grazie ad internet ha sposato una ragazza lituana e sta diventando padre praticamente nello stesso periodo in cui lo diventerò io. E' una storia particolare a cui voglio dedicare un articolo a parte.


 La storia che voglio raccontarti oggi è quella di un giovane ragazzo che ha conosciuto la Lituania via internet e l'ha portato a conoscere la compagna con cui convive attualmente in Italia. Ti propongo questa storia perché se anche tu sei in relazione virtuale con una ragazza lituana potrai trovare interessante l'esperienza di Federico.  
  
***Chi sei, cosa fai e dove vivi?***

 *Mi chiamo federico, ho 34 anni, sono un agente di commercio e vendo occhiali e lavoro molto con l'estero, soprattutto con l'Europa. Abito a Bassano del Grappa nella provincia di Vicenza.*

 ***Come hai conosciuto la Lituania?***

 *Me ne ha parlato la prima volta un amico come di un paese interessante da visitare e non nego che uno dei pensieri principali nell'immaginarla, fosse la bellezza delle donne descrittami. Io attraversavo un brutto momento sentimentale, diciamo che stavo vivendo da poco più di un anno il trauma di una relazione lunga e finita male. *

 ***E cosa è successo?***

 *Ho conosciuto Egle, di 26 anni, su un sito d'incontri lituano, dopo circa 4 mesi di telefonate, mail, chat sono andato a Vilnius per 3 giorni con quel famoso amico che me ne ha parlato la prima volta.*

 ***Come è stato il vostro incontro?***

 *Ne parlammo davvero molte volte in quei mesi di attesa sul come fare per incontrarci. All'inizio pareva venisse lei, ma giustamente non conoscendoci, mi face intendere di preferire che io andassi a trovarla. Lei studiava all'Università e le mancavano poco più di 2 anni per finire. Il problema per me però erano i soldi, in quanto al tempo vivevo coi miei ed ero appena laureato e lavoravo a poco più di 300 euro al mese in un call center per proporre dei corsi d'inglese.*

 *La svolta è stata quando ricevetti una proposta di lavoro a 1 ora e mezza da casa con uno stipendio normale. Cambiai vita, me ne andai di casa in affitto e finalmente iniziai a mettere da parte i soldi per andare in Lituania. *

 *Partii con il mio amico il 25 Marzo 2005 in auto per prendere l'aereo a Vienna, al tempo era più conveniente che partire da Venezia. Dormimmo in auto per risparmiare i soldi del pernottamento in un albergo. Come puoi immaginare, il sonno non fu dei migliori. Il giorno dopo atterrammo a Vilnius in preda all'entusiasmo nel sapere di conoscere di li a poco le ragazze, uscimmo dalla sala bagagli e una visione ci si stampò davanti agli occhi: due splendide e intimidite ragazze ci guardavano smarrite come se vedessero due alieni. L'imbarazzo fu molto, in quanto dopo aver visto Egle in webcam, dalla stanchezza ebbi qualche tentennamento nel distinguerla dalla sorella gemella. Avrò per sempre con me quello sguardo da ragazza dolce e perbene nel mio cuore.*

 *L'impressione che ebbi nel vedere Egle la prima volta è stata indescrivibile, la sua semplicità e i suoi modi educati ed imbarazzati, mi hanno colpito profondamente. Lei era un milione di volte migliore di quanto immaginassi. Il suo taglio d'occhi mi impressionò parecchi, in quanto puntava verso l'alto e ora, dopo alcuni anni di esperienza nei Baltici, posso dire che sia un tratto somatico molto caratteristico di quei luoghi.*

 ***Quali sono state le tue impressioni quando sei arrivato in Lituania?***

 *La Lituania mi ha affascinato sin dal primo giorno. Avendo girato molti paesi dell'ex blocco sovietico, conoscevo già la cultura che in alcuni casi li accomuna, ma la Lituania, come gli altri baltici, si trova non solo geograficamente, ma anche culturalmente, tra cultura nord europea e appunto post-comunista.*

 *Mi ha impressionato la facilità con la quale si consuma alcol. Mi ha impressionato la semplicità delle persone e soprattutto delle donne.*

 ***Quanto hai vissuto in Lituania, come ti sei trovato da straniero?***

 *Sono rimasto al massimo 2 settimane, ma sempre come turista. Mi sono sempre sentito trattato come un re. Lo straniero viene guardato con adorazione, ma spesso solo da donne. Mi sento più straniero nel mio paese che in Lituania.*

 ***Hai mai pensato di vivere in Lituania?***

 *Si, ma il maggior ostacolo sarebbe il clima. Ho necessità fisica e psicologica di sole. Da quando ho iniziato ad andare all'estero, avevo circa 9 anni, ho sempre subito il fascino di terre diverse dalla mia. Ho sempre desiderato provare a sentirmi in difficoltà in un paese che non è il mio, affrontare una vita nuova, ma non ho mai trovato la forza per farlo perchè di carattere fatico a prendere decisioni così radicali dato che ci penso e ripenso fino a lasciar perdere tutto.*

 ***Quali sono i vantaggi e gli svantaggi delle differenze culturali tra lituana e italiano?***

 *Con Egle non ho mai sentito questi problemi, lei si sente più cittadina del mondo che lituana e grazie a lei ho superato molto della mia provincialità. L'educazione e il carattere sono gli elementi che determinano o meno la stabilità di una coppia mista, l'amore aiuta all'inizio. La nostra fortuna è stato il carattere appunto di entrambi, io poi fortunatamente non sono per nulla un "problematico", mi adatto e recepisco qualsiasi cosa.*

 ***Quali sono le difficoltà di Egle nel vivere in Italia?***

 *Anche se il suo italiano è quasi da madrelingua, dentro resta il vuoto di non potersi esprimere nella propria lingua, una problematica che conosco bene seppur in misura assai minore. Egle vive le difficoltà e le limitazioni dovuti al doversi esprimere in una lingua che non le appartiene.*

 ***Che consigli daresti a chi sta seguendo un percorso simile al tuo?***

 *Emigrare per vivere in Italia è un passo arduo che non può essere affrontato da soli quindi **consiglio vivamente di essere sempre vicini alla propria compagna** ed aiutarla ad affrontare i momenti di difficoltà che scaturiscono dalla lontananza dalla famiglia, dagli amici e dalla propria terra.*

 *Il modo migliore per aiutarla e di capirla è **mettersi con tutta l'anima nei suoi panni**, non solo a parole. Fatela sempre sorridere e cercate il più possibile di alleggerire le difficoltà di tutti i giorni.*

 *All'inizio della nostra storia abbiamo passato 2 anni tra un volo e l'altro, fortunatamente ci si vedeva una volta al mese per incontrarci, ma verso la fine ero esausto per le difficoltà. In quei due anni **se non ci fosse stata una grande passione**, il progetto di stare insieme e molta comprensione, **non ce l'avremo mai fatta**.*

 *Una particolarità della nostra storia è che quel famoso amico di cui si parlava all'inizio è fidanzato con la gemella di Egle. Il tutto è iniziato con la più classica delle battute fatta da me a Egle: hai per caso una sorella da presentare al mio amico per sentirci meno in imbarazzo al nostro primo incontro? la risposta mi ha spiazzato completamente in quanto non solo sorella, ma per di più gemella! Da quel momento è iniziata la storia anche tra loro...*

 *Ma non solo... anche io sono gemello! Sia Egle con sua sorella, che io con la mia, siamo eterozigoti.*

 **Se anche tu hai una storia da raccontare, [contattami!](http://www.karimblog.net/contattami/)**

