Title: Mi mancava
Date: 2006-02-02
Category: Lituania
Slug: mi-mancava
Author: Karim N Gorjux
Summary: Sinceramente scapperei oggi, ma credo che sia solo una sensazione passeggera dovuta al mio ritorno, le cose si stanno evolvendo e la mia relazione con Rita è più solida che mai.Abbandonare (per[...]

Sinceramente scapperei oggi, ma credo che sia solo una sensazione passeggera dovuta al mio ritorno, le cose si stanno evolvendo e la mia relazione con Rita è più solida che mai.  
  
Abbandonare (per poco) la mia Italia, la palestra, i vari boja faus e gli amici per tornare qui non è facile, ma c'era qualcosa che mi mancava. Per prima cosa mi mancava il **non** avere la macchina. A Klaipeda mi sposto con il bus e spendo si e no 10€ al mese tra taxi, abbonamento e minibus; oltre al vantaggio economico dello spostarsi al costo di 8 litri di verde c'è il grandissimo vantaggio che pago pochissimo di telefono. Con il cellulare spenderò, se vanno bene 10€ al mese, tanto quanto andare in giro per la città.


 Mi mancava il cibo lituano, come ho scritto in precedenza è parecchio pesante e particolare, ma tra queste particolarità c'è anche la voglia di mangiare gli cepelinai o i guldunai ogni tanto. La prima sera a Klaipeda Rita si è subito buttata chiamano il 1822 e ordinando gli cepelinai al cili kaimas.


 Oltre la tranquillità, e la televisione sempre spenta, mi mancava il latte alla fragola. ;-)

