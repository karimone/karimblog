Title: Triplicati i casi di influenza in Lituania: il governo si è attivato contro rischio di pandemia
Date: 2009-11-21
Category: Lituania
Slug: triplicati-i-casi-di-influenza-in-lituania-il-governo-si-è-attivato-contro-rischio-di-pandemia
Author: Karim N Gorjux
Summary: Ieri, 20 novembre, ben 177 scuole sono state chiuse in Lituania come precauzione contro il diffondersi del virus Ah1n1. La decisione di chiudere le scuole avviene su base locale, eccetto il caso[...]

[![news](http://www.karimblog.net/wp-content/uploads/2009/11/news.jpg "news")](http://www.karimblog.net/wp-content/uploads/2009/11/news.jpg)Ieri, 20 novembre, ben 177 scuole sono state chiuse in Lituania come precauzione contro il diffondersi del virus Ah1n1.  
   
La decisione di chiudere le scuole avviene su base locale, eccetto il caso in cui la maggior parte delle 60 municipalità dichiari lo stato di emergenza. Se ciò dovesse accadere, il Governo avrà facoltà di chiudere le scuole.  
   
**Fino ad ora, sono 265 i casi accertati**, dopo che un ragazzo di 14 anni, due giorni fa, è morto a causa delle complicanze del virus.  
   
I territori considerati a rischio sono finora:Anykščiai, Utena, Marijampolė, Ukmergė, Alytus, Vilnius, Kėdainiai, Jonava, Kaišiadorys, Širvintos, Kaunas (municipalità), Kaunas (regione) , Švenčionys, Klaipėda (regione), Molėtai, Trakai, e Ignalina.  
   
Il portavoce del ministro della salute, Giedrė Maksimaitytė, ha specificato che l'uso delle mascherine può essere utile per chi è contagiato, in modo che protegga gli altri. Non è necessario, invece, per i non contagiati.  
   
L'Organizzazione Mondale della Sanità ha fatto sapere che non bisogna fare test clinici su tutta la popolazione. Essi vengono svolti solo sui ricoverati negli ospedali.  
   
Le medicine antinfluenzali sono disponibili nelle farmacie di tutto il territorio nazionale.


