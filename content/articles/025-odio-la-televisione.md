Title: Odio la televisione
Date: 2004-12-06
Category: altro
Slug: odio-la-televisione
Author: Karim N Gorjux
Summary: Ormai mi avvio verso il secondo anno senza televisione, sono uno dei pochi, non ho ancora trovato qualche "senza" tv come me. Ogni tanto mi capita di guardarla a casa di mia[...]

Ormai mi avvio verso il secondo anno senza televisione, sono uno dei pochi, non ho ancora trovato qualche "senza" tv come me. Ogni tanto mi capita di guardarla a casa di mia madre, ma ormai la mia sopportazione televisiva e' ai minimi storici, devo togliere il volume la maggior parte delle volte. Prima di continuare voglio marcare bene la distinzione tra cinema e televisione, quest'ultima e' quasi sempre spazzatura, mentre il cinema a volte puo' essere arte.


 Se accendo la tv mi capita spesso di vedere dei programmi assolutamente idioti. Non mi dilungo nel parlare del "Grande Fratello" che oltre ad essere una copiatura del mitico libro di Orwell (se non ricordo male si intitolava "1984") e' una schifezza immane, non ho voglia parlare delle conseguenze di questo programma dove ormai i ragazzini pensano che sia molto piu' facile ottenere qualcosa dalla vita facendo 3 mesi niente spiato da quell'elettrodomestico maledetto chiamato TV. (fonte Panorama)

 Ok il grande fratello e' quello che e', ma il calcio? Partite di qua, partite di la'. Ormai puoi guardare le partite in tutte le salse e tutti i giorni. Dalla Domenica al Mercoledi' si parla delle partite che hanno giocato, dal Giovedi' al Sabato quelle che giocheranno: uno schifo.   
Il fondo lo si tocca con i programmi con le letterine, veline e altre donne oggetto usate per dare lustro a programmi televisivi sempre piu' scadenti. La tv e' un'immondizia! Tutto cio' di interessante che si puo' vedere e' di notte quando ovviamente non "puoi" vederlo a meno che non soffri di insonnia. :-(

