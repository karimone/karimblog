Title: Una passeggiata nella foresta, alla ricerca di una spiaggia non troppo lontana
Date: 2009-04-09
Category: Lituania
Slug: una-passeggiata-nella-foresta-alla-ricerca-di-una-spiaggia-non-troppo-lontana
Author: Karim N Gorjux
Summary: Quando sono arrivato in Lituania ad Agosto, avevo preso in affitto un bell'appartamento a dieci minuti dal centro, un poco costoso, ma tutto nuovo e ben arredato. Abitavo al diciasettesimo piano e[...]

[![](http://farm4.static.flickr.com/3123/2716056335_7351db412f_m.jpg "Sentiero nella Foresta")](http://www.flickr.com/photos/you_rate/2716056335/)Quando sono arrivato in Lituania ad Agosto, avevo preso in affitto un bell'appartamento a dieci minuti dal centro, un poco costoso, ma tutto nuovo e ben arredato. Abitavo al diciasettesimo piano e si poteva vedere il mare dalla finestra, mi piaceva, era suggestivo, anche se, per godersi bene il panorama, bisognava [evitare di guardare troppo in basso](http://www.flickr.com/photos/kmen-org/3147155931/in/set-72157607088143393/).   
Dopo meno di sei mesi ci siamo trasferiti nuovamente in una zona fuori dal centro a nord di Klaipeda. Da dove viviamo ora al mare, ci separano meno di 3km di foreste intrecciate da sentieri e piste ciclabili.  
  
Finalmente, ieri sera, io, Greta e Rita abbiamo approfittato dei 15 gradi primaverili per avventurarci nella foresta. L'impressione è stata molto gradevole, guardando da un altro punto di vista il mio trasferimento in Lituania, posso dire di aver barattato le montagne con il mare e l'inverno temperato con un'estate mite, ventilata e poco afosa.


 [![](http://farm4.static.flickr.com/3594/3426265402_99ab3e8bbf_m.jpg "Palazzi colorati")](http://www.flickr.com/photos/kmen-org/3426265402/?edited=1)Purtroppo ieri non siamo riusciti ad arrivare fino alla spiaggia, Greta era stanca e tutto quell'ossigeno ha portato sonnolenza anche a me e a Rita. Siamo tornati che erano le 19, era ancora giorno, i bambini giocavano sulle giostre vicino al nostro condominio e i vari colori dei palazzi nuovi che compongono questo quartiere sembrano comunicare che i vecchi anni sovietici, stanno piano piano scomparendo per lasciare spazio ad una nuova Lituania. Ovviamente migliore.


 Mi spiace per la foto dei palazzi, ma l'ho fatta oggi che non è un giornata che definirei "stupenda".


