Title: Ma come hai fatto a capire che sono Italiano?
Date: 2005-11-05
Category: Lituania
Slug: ma-come-hai-fatto-a-capire-che-sono-italiano
Author: Karim N Gorjux
Summary: --> Nota: se stai leggendo questo articolo tramite RSS, non puoi vederlo, ma sul sito ho messo un sondaggio. Se vuoi farmi felice, vota. :-)

**--> Nota:** se stai leggendo questo articolo tramite RSS, non puoi vederlo, ma sul sito ho messo un sondaggio. Se vuoi farmi felice, vota. :-)

 Il *Boogie Woogie* è il locale a tema Rock-Pop che offre una tavola calda di tutto rispetto nel centro di Klaipeda. La vetrina esterna del locale punta dritto dritto sulla via principale *H. Manto Gatvè* che taglia Klaipeda in metà come un limone. Per un posto a sedere davanti alla vetrina si farebbero carte false, è come sedersi in prima fila ad una sfilata di moda. Uno spettacolo unico. (se sei un possessore dell'organo chiamato *prostata*)

 Il gruppo della serata era formato dai soliti ignoti: io ed Enrico, Rita, Gintare ed Emily (ragazza francese amica di Enrico). Eravamo tranquilli e beati che aspettavamo, da almeno 30 minuti, qualcosa da mangiare. Ad un certo punto Enrico "occhio di falco" nota alle mie spalle entrare due italiani *versione turista*. Il turista italiano è degno non di una puntata, ma di un'intera stagione di *Super Quark*. L'inconfondibile movimento rotatorio del collo a velocità regolare di 90 gradi a sinistra e a destra, le pupille dilatate gli occhi spalancati e mascella rigida ne fanno un esemplare unico nella sue specie. Il passo ipnotico e la *vestimenta* (mi sun piemunteis) da impiegato del catasto danno un tocco in più all'immagine.  
  
Li accantono in un angolo della mente finché non devo andare alla toilette (per i duri: il cesso). Scendo le scale che sembrano più un dirupo che delle scale e mi ritrovo nella piccola anticamera della contemplazione. Davanti a me le *spice girls* mi guardano sorridenti, l'evento più commerciale del secolo scorso è immortalato in una stampa di un metro per un metro sulla parete a fronte delle scale. Alla mia sinistra, appeso tra le due porte che danno alla *ritirata* delle signore e degli uomini, il sorriso sgargiante di *Ricky Martin* cerca di stimolare il motivo per cui ti trovi li. Penso che sia giusto che queste due grandi icone della musica pop trovino posto al cesso mentre al piano superiore si possono ammirare reperti dei più sconosciuti *Pink Floyd*, *Dire Strairs*, *Kiss*, *Queen*, *Status quo* e tanti tanti altri...


 Ero lì che stavo per aprire la porta della toilette che vedo questo ragazzone italiano incredibilemente indeciso e dubbioso sul da farsi. Camminava avanti e indietro guardando le due porte che davano ai bagni. Non aveva nessun tipo di disturbo interiore da mettere in dubbio la sua sessualità e non era nemmeno il protagonista di quei giochini che fanno agli scimpanzé per testarne l'intelligenza (ora che ho la banana, che porta apro?). Il suo problema era l'interpretazione dei strani simboli lituani, non riusciva a capire quale fosse il bagno degli uomini perché sulla porta non c'era scritto nulla né in inglese e tanto meno in Lituano. Solo due simboli: "^" "V" (sono persino difficili da fare con la tastiera.. il primo è una V rovesciata). Quale dei due simboli è quello dell'uomo? Contro ogni teoria de *Il codice Da Vinci* il bagno degli uomini è contrassegnato con la "V". Provate voi a spiegarmi il perché.


 *"La porta degli uomini e questa"*  
*"Grazie... ma come hai fatto a capire che sono Italiano?"*  
Sorrido e mentre il ragazzone si porta al lavabo io entro nel piccolo bagno dove sorpresa, c'è una turca!  
*"Vedi, dopo un pò che sei qua e vedi tanta gente diversa, ti fai l'occhio all'Italiano e anche alle ragazze..."*  
*"Figa! Io ho viaggiato in tanti posti, ma non ho mai visto tante fighe come in Lituania."*

 Continuo a sorridere, pensando che se ha viaggiato tanto allora lo ha fatto con gli occhi chiusi. Di belle ragazze se ne trovano in tutto il mondo, non solo in Lituania. Vomita parole di continuo, non aveva l'arroganza dei tipici italiani versione*"...è da tanto che sono qui, se voglio trombo qua e là.."*. Era più il tipo contento di aver trovato qualcuno a cui parlare in italiano per avere qualche notizia sul dove andare alla sera. Gli dò qualche dritta sul dove **non** andare e sul cosa **non** fare. L'impressione è quella tipica del turista italiano ormonalmente guidato, mi confessa che è contento della qualità delle donne, ma preferiva una Romania anni '90 cosa che **qui non è di moda da molto tempo**.


