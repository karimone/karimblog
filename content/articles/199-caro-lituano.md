Title: Caro lituano
Date: 2006-08-12
Category: Lituania
Slug: caro-lituano
Author: Karim N Gorjux
Summary: Ma perché quando io cerco di parlare la tua lingua antica, difficile, antipatica e parlata da così poca gente che a confronto il Piemontese è una lingua internazionale, non mi aiuti con[...]

Ma perché quando io cerco di parlare la tua lingua antica, difficile, antipatica e parlata da così poca gente che a confronto il Piemontese è una lingua internazionale, non mi aiuti con un sorriso e un po' di comprensione ad esprimermi? Perché non sorridi mai? Perché non saluti? Perché sporchi come un maiale il paese in cui vivi? Perché ti lamenti se le vostre donne scappano in cerca di uomini latini quando voi non avete mai una parola gentile per loro? Perché ogni scusa è buona per bere fino a rasentare il coma etilico? Perché non hai mai una parola gentile per il prossimo? Perché non chiedi per favore? Perché negli spogliatoi della palestra non parlate mai e rimanete in un imbarazzante silenzio anche per decine di minuti? Perché non pucciate i biscotti nel latte? Perché non posso fare una semplice modifica al menu del ristorante? Perché mangiate sempre patate? Perché in una città di 200.000 abitanti continuate ad aprire solo casinò e centri commerciali? Perché le vostre donne sono così belle e voi siete così brutti? Perché vi tagliate tutti i capelli come dei naziskin? Perché non cantate per strada? Perché mangiate quando cazzo vi pare e non vi sedete tutti a tavola a ore stabilite? Perché non togliete l'amianto dai tetti delle case? Perché andate a lavorare all'estero e quando siete nel vostro paese passate il vostro tempo a sperperare i vostri soldi in bere e feste? Perché siete inaffidabili ed è impossibile fare affidamento in qualsiasi cosa con voi? Perché siete grandi, grossi e grezzi? Perché non fate la raccolta differenziata? Perché non sottotitolate i film in tv come al cinema? Perché non mi affettate il prosciutto pur avendo l'affettatrice? Perché devo pagare la mazzetta al dottore dell'ospedale? Perché le donne si vestono bene in qualsiasi momento della giornata anche quando non è necessario? Perché il tassista spegne il tassametro quando salgo io a bordo? Perché siete ubriachi anche alle 10 del mattino? Perché è così duro integrarsi con voi maschi, ma è così facile integrarsi con le donne?

 Caro lituano, ma tutto ciò ha a che fare con il vostro **record mondiale assoluto di suicidi**? (81 ogni 100.000 abitanti).  
 technorati tags start tags: [lituano](http://www.technorati.com/tag/lituano), [perché](http://www.technorati.com/tag/perché), [lituania](http://www.technorati.com/tag/lituania), [suicidi](http://www.technorati.com/tag/suicidi)



  technorati tags end 

