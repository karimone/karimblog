Title: Una settimana piacevole con i parenti in visita dall'Italia
Date: 2010-11-29
Category: Lituania
Slug: una-settimana-piacevole-con-i-parenti-in-visita-dallitalia
Author: Karim N Gorjux
Summary: La scorsa settimana sono venuti a trovarci mia zia e mia madre. Siamo stati bene anche se ha fatto freddo e non sono riuscito a far vedere molto di Klaipeda. Le poche[...]

[![](http://farm4.static.flickr.com/3472/4565061357_018f8cf22f_m.jpg "Klaipeda dall'alto")](http://www.flickr.com/photos/ellennetcom/4565061357/)La scorsa settimana sono venuti a trovarci mia zia e mia madre. Siamo stati bene anche se ha fatto freddo e non sono riuscito a far vedere molto di Klaipeda. Le poche volte che siamo usciti, mia madre è congelata e mia figlia si è ammalata quindi abbiamo passato gran parte del tempo in casa.  
  
A me ha fatto piacere avere mia madre ai fornelli, non mangiavo così bene da quest'estate quando siamo stati in Italia. L'Italia... il paese stupendo con montagne, mare e cibo fantastico... mia moglie continua a sognare per un ritorno, ma non è così facile. Anticipo già che stiamo pianificando la nostra partenza, ma ne parlerò in un altro articolo.


 Ci sono alcuni aspetti della Lituania che hanno lasciato i miei parenti perplessi, cito a memoria quelli più importanti:  
  
 * **La sanità carente**, povera e poco professionale. Abbiamo dovuto portare mia figlia a controllare l'orecchio all'ospedale. Il dottore era un settantenne antipatico e scorbutico.

  
 * **La folla dai cassonetti a rovistare nell'immondizia**. Dalle parti di Cuneo si vede qualche vecchietto a mettere le mani nell'immondizia, ma qui a Klaipeda il via vai è continuo e di gente di tutte le età
  
 * **I sorrisi inesistenti**. Mia zia non ha certo l'aspetto del meridionale in cerca di intima dazione eppure è rimasta colpita da i visi glaciali in risposta al suo educato sorriso.

  
 * **Il riscaldamento del "mio" appartamento.** Qui fa freddo, ma noi non abbiamo ancora acceso il riscaldamento. Sembra impossibile, ma fuori ci sono stati per tutta la settimana 4-5 gradi, oggi siamo a -12, ma noi non abbiamo acceso il riscaldamento se non venti minuti per asciugare dei vestiti. Vedo di trovare un perché a questa fantasticheria
  


 Ci sono altre cose che sono piaciute: i locali, il centro commerciale (puah!), alcuni piatti della cucina lituana come i lietinai e i dolci che noi chiamiamo i "ricottini". Non hanno avuto modo di vedere l'asilo di Greta perché si è ammalata, ma dalle foto e dai filmati si sono rese conto che Greta sta bene e si trova in un bell'ambiente.


 I miei parenti hanno alloggiato al [Pajūrio Vieškelis](http://www.pajuriovieskelis.lt/) hotel nuovo, bello e dai prezzi contenuti. La mattina di sabato sono partite dall'aeroporto di Palanga senza troppi problemi e con tanta comodità da parte mia che mi sono risparmiato di doverle portare a Riga o a Vilnius facendo 600km tra andata e ritorno. Anche all'aeroporto la solita gentilezza e cordialità delle signore baltiche ha lasciato il segno. Al check-in sembrava di avere a che fare con un sergente che smistava i prigionieri. Al controllo dei bagagli la signorina che dettava istruzioni per passare ai raggi x sembrava colta da un irrimediabile paralisi facciale.


 Una volta andato a prendere la macchina parcheggiata davanti al minuscolo aeroporto, un signore sulla cinquantina con indosso la giacchetta fosforescente da soccorso stradale mi ha fatto notare che dovevo pagare il parcheggio. Erano le 5 del mattino di sabato quando ho parcheggiato la macchina e tutto ho pensato fuorché pagare il parcheggio. Ho chiesto quanto dovevo pagare e lui mi ha sorriso e mi ha detto: "*Non fa niente, pagherai la prossima volta*". 

 Peccato che mia zia e mia madre si siano perse l'unico sorriso della settimana.


