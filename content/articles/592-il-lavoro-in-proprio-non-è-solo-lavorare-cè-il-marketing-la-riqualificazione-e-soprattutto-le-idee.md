Title: Il lavoro in proprio non è solo lavorare c'è il marketing, la riqualificazione e soprattutto le idee
Date: 2010-03-02
Category: Lituania
Slug: il-lavoro-in-proprio-non-è-solo-lavorare-cè-il-marketing-la-riqualificazione-e-soprattutto-le-idee
Author: Karim N Gorjux
Summary: Io sono un imprenditore, almeno ci provo perché nel mio immaginario l'imprenditore è una persona che muove un sacco di soldi e ne fa anche un sacco. Se devo essere sincero la[...]

[![](http://farm1.static.flickr.com/137/345712029_345bf4ef2f_m.jpg "Lavorare in Ufficio alternativo")](http://www.flickr.com/photos/frijole/345712029/)Io sono un imprenditore, almeno ci provo perché nel mio immaginario l'imprenditore è una persona che muove un sacco di soldi e ne fa anche un sacco. Se devo essere sincero la mia idea di imprenditore italiano è anche legata a qualcosa di disonesto, ben velato e nascosto, ma questa è solo una mia impressione.


 Sono un freelancer, lavoro per conto mio e lo faccio all'estero, in Lituania. Non è facile, il problema non sono le competenze, ma riuscire a vendere le proprie capacità, trovare qualcuno disposto a pagarti per risolvergli un problema. Questo richiede marketing.


  In passato ho avuto un socio, bravissima persona, intelligente e capace, ma la sua idea di imprenditoria era inesistente, lui è il miglior dipendente che puoi avere, ma negato per essere imprenditore. Perché? Semplicemente perché gli mancava l'empatia che richiede il marketing per poter vendere.


 Tornando al discorso iniziale, io sono qui in Lituania e devo trovare i clienti che sono in Italia. In questi mesi sono riuscito a creare una presenza nel Belpaese pur essendo qui, ci sono riuscito aiutando un mio amico di infanzia ad aprire un laboratorio informatico nel paese dove vivevo: a Centallo in provincia di Cuneo.


 Gli ho permesso di usare il mio nome, di essere ospitato sul mio [sito web professionale](http://www.gorjux.net) ma soprattutto l'ho aiutato elargendo tutti i consigli possibili frutto dei miei errori passati (o figli della mia esperienza).


 Ora [ho una presenza fisica in Italia](http://www.gorjux.net/pc-doctor-apre-il-laboratorio-a-centallo-cuneo.html "PCDoctor a Centallo"), Emanuele vende i suoi servizi di tecnico informatico e vende anche i miei, questo mi permette di poter tornare in Italia non solo per le vacanze, ma anche per lavorare. Inoltre l'aver un rapporto lavorativo continuo con l'Italia è una porta sul retro, **una di quelle porte d'emergenza ad apertura antipanico** che si mettono negli edifici e nei negozi per scappare quando tutto prende fuoco.


 Tre anni fa ben pochi avrebbero immaginato che avremmo attraversato una crisi economica, che le fabbriche avrebbero chiuso e che i disoccupati sarebbero aumentati di colpo. Nessuno lo avrebbe immaginato e anche se per me le cose vanno non benissimo, ma nemmeno malissimo, faccio tesoro di questa esperienza per il futuro. Per prima cosa non è detto che io rimanga sempre in Lituania, certo **per ora non ho nessuna intenzioni di muovermi** e mi sto avvicinando ai due anni di residenza, ma il futuro è incerto e misterioso quindi la mia strategia di avere una presenza in Italia ha il duplice scopo di migliorare il mio business mentre sono qui, ma anche di **avere qualcosa di già avviato nel caso dovessi tornare in Italia con le valigie**.


 Oltre a questo sto impiegando tanto tempo per acquisire nuove abilità. Molte persone cercano lavoro, lo trovano e sono tranquille che non succederà più nulla. Ho sentito le interviste degli operai FIAT che rischiano di essere senza lavoro e trovarsi a 40 anni completamente fuori mercato lavorativo. Questo succede, soprattutto in Italia, perché i dipendenti non vengono formati a stare al passo con i tempi. Molte persone sono *"gratta palle"* per natura, ma a tante persone non viene data la possibilità di migliorarsi e questo è un male per un paese di *"vecchi"* come l'Italia. Inglese ed informatica sono i due ingredienti principali per formare una società efficiente, ma che in Italia sono tabù. Se tante cose in Italia funzionano male è proprio perché il dipendente non viene riqualificato e la meritocrazia nel posto di lavoro non esiste più da tempo immemore.


 In realtà i problemi sono davvero molti, io personalmente sono dipendente di me stesso, non ho orari e quindi devo tenere conto solamente dei risultati e l'unico modo per averne e di muovermi al passo con i tempi riqualificandomi ogni giorno. Vuoi vedere che alla fine tutte queste difficoltà che mi trovo ad affrontare qui in Lituania sono solo un bene?

