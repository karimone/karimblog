Title: Dottore...
Date: 2006-12-09
Category: altro
Slug: dottore
Author: Karim N Gorjux
Summary: Dottori... non solo per la salute e nemmeno per l'informatica, ma dottori negli affari perché le persone valide devono sempre essere un po' pazze.

Dottori... non solo per la salute e nemmeno per l'informatica, ma dottori negli affari perché le persone valide devono sempre essere un po' pazze.


 Le persone ordinarie mi annoiano e detto sinceramente, tra me e voi, ne sono anche timoroso. In fondo in fondo mi preoccupano.


