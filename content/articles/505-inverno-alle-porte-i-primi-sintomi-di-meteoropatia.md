Title: Inverno alle porte: i primi sintomi di meteoropatia
Date: 2008-11-18
Category: Lituania
Slug: inverno-alle-porte-i-primi-sintomi-di-meteoropatia
Author: Karim N Gorjux
Summary: Ieri ho assistito alla prima nevicata, ma non solo, il tempo sta decisamente cambiando per diventare sempre più invernale. Ad essere sincero qualche giorno fa ho persino patito il cambiamento, alle 16[...]

Ieri ho assistito alla prima nevicata, ma non solo, il tempo sta decisamente cambiando per diventare sempre più invernale. Ad essere sincero qualche giorno fa ho persino patito il cambiamento, alle 16 faceva già buio, il sole tramontava e io che mi trovavo seduto davanti al computer iniziavo ad accusare alcuni sintomi di meteoropatia.  
  
Non è stato facile e non lo è nemmeno ora. Non fa molto freddo perché Klaipeda è sul mare e quindi la temperatura regge bene, ma il vento, la pioggia e la neve rendono molto difficile fare una passeggiata in centro. Come si difendono i lituani da tutto questo? Penso che non si difendano o almeno non ne sono coscienti perché le difese sono nel loro modo di vivere. La sauna, i solarium, il cibo grasso, il bere in compagnia... è il naturale modo di vivere lituano che affronta egregiamente il lungo inverno.


 Domenica c'era un vento incredibile, ne ho approfittato e sono andato a Palanga con Greta e Rita nel parco pubblico dove si trova il museo dell'ambra. Il parco è stupendo, pulito, ordinato. Ho messo qualche foto su flickr e ho anche fatto un paio di filmati al mare burrascoso. Cosa mi colpisce tanto è il prato lituano, anche vicino alle case in campagna è sempre verdissimo e fine alle volte non sembra neanche vero.  
  
      


  
Le foto del parco le trovate [qui](http://www.flickr.com/photos/kmen-org/sets/72157609350544997/)

