Title: Ste (due) donne lituane
Date: 2006-12-13
Category: Lituania
Slug: ste-due-donne-lituane
Author: Karim N Gorjux
Summary: Ogni tanto mi leggo qualche post del mitico Sandro, lui si che se la spassa con l'altro fenomeno del Borja, donne, feste e ogni tanto si mette pure a parlare di scuola[...]

Ogni tanto mi leggo qualche post del mitico [Sandro](http://kaunaslife.blogspot.com/), lui si che se la spassa con l'altro fenomeno del [Borja](http://www.karimblog.net/2006/12/11/percezione-distorta-della-realta#comment-20727), donne, feste e ogni tanto si mette pure a parlare di [scuola](http://kaunaslife.blogspot.com/2006/12/sar-che-sono-gi-passati-tre-mesi-di.html) mettendo bene in mostra la foto di classe. Subito mi rendo conto di una cosa: l'occhio è tornato agli standard cuneesi.


 Bene, domani mattina andrò a prendere la mia signora e la mia signorina: le mie due donne lituane. La domanda che mi viene posta più frequentemente è questa: *"Ma Rita vuole venire in Italia?"*.  
Per "par condicio" Rita dovrebbe passarsi due anni qui in Italia, ma alle condizioni lituane, come ho passato i miei due anni a Klaipeda. Dato che non sono così meschino ho cercato di preparare la casa per il meglio, ma sono proprio tante le cose a cui bisogna pensare quando si passa dalla modalità **chissenefrega-del-domani** a **ho-una-famiglia-e-mi-tocca-diventare-adulto**. 

 In sostanza Rita ha "paura", ma non è la sola, anche io ho i miei timori e non sono pochi.


