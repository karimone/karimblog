Title: L'italia è la nuova Atlantide: stiamo sprofondando
Date: 2007-11-17
Category: Lituania
Slug: litalia-è-la-nuova-atlantide-stiamo-sprofondando
Author: Karim N Gorjux
Summary: L'Italia ha perso in ogni campo, siamo primi nel mondo solo nel calcio, ma per quanto riguarda tutto il resto, abbiamo perso. A me la provincia di Cuneo piace, la gente è[...]

L'Italia ha perso in **ogni campo**, siamo primi nel mondo solo nel calcio, ma per quanto riguarda tutto il resto, abbiamo perso. A me la provincia di Cuneo piace, la gente è cordiale, le nostre montagne riescono a mozzarti il fiato tanto che sono belle. La nostra cultura, il nostro cibo, la nostra lingua è qualcosa che fa parte di noi e che ci accompagna ovunque. Ci sono tante cose a cui non riesco a rinunciare dell'Italia.


  
Rita si trova bene qui, ma ci sono tante cose che non vanno e che ti fanno odiare in primis la nostra classe politica. **Le leggi sono troppe** e molte senza senso, le tasse sono elevatissime, i politici sono dei mafiosi che hanno stancato chiunque. Nel 1994 c'erano Berlusconi e Prodi, oggi, nel 2007 ci sono Berlusconi e Prodi. L'Italia è **ferma da 15 ann**i, ci siamo fatti battere dalla Spagna che non ha assolutamente le risorse che abbiamo noi; la Grecia fa a botte con noi per non essere all'ultimo posto nelle varie classifiche europee.


  
In Lituania la più grande difficoltà è stata la **lingua**, in Lituania non hanno le nostre risorse, ma sono sicuro che entro 10-15 anni faranno dei passi da gigante che andrà a finire che saremo noi italiani a emigrare lassù e non i lituani a venire da noi. Vi ricordate com'era l'Irlanda all'inizio degli anni 90?

  
Anche sull'immigrazione ho i miei dubbi, chi viene qui da noi? Romeni? Albanesi? Marocchini? Da noi arrivano per lo più persone che sanno del nostro sistema giuridico inefficiente. Sanno che Antonio Di Pietro, il magistrato in prima linea contro tangentopoli è il ministro delle infrastrutture mentre Mastella che era al matrimonio di [Campanella](Francesco%20Campanella), come testimone di nozze e in compagnia di [Cuffaro](http://it.wikipedia.org/wiki/Salvatore_Cuffaro) è ministro di Grazia e Giustizia. Sanno che l'Italia è il paese di [Bengodi](http://www.portoveneredidattica.it/ICPDID/Fabiano/paese_di_cuccagna.htm) e i delinquenti la fanno sempre franca.


  
L'Italia è morta. Le tasse superano il 43% dell'imponibile, scalare dalle tasse è una barzelletta. Ovviamente oltre le tasse bisogna tenere conto dell'INPS che non è nient'altro la pensione che non vedrai mai e poi c'è la ritenuta d'acconto, **lo studio di settore**. Mamma mia! Lo studio di settore! Una porcheria del genere può essere solo italiana: in pratica devi fare attenzione a quanto fatturi perché in base ad alcuni calcoli cretini, di cui nessuno conosce gli algoritmi, il ministero delle finanze sa quanto tu devi fatturare. Se fatturi meno o di più rischi di avere dei controlli. Se non vuoi i controlli devi pagare. In Italia le tasse le anticipi!

  
**Lo stato è un nemico**. Se hai una società, il tuo intento è lavorare in nero per pagare meno tasse. Il commercialista deve essere tuo alleato, ma il più delle volte **non vuole avere grane** e ti fa pagare tutte le tasse e quindi si fa del nero per guadagnare qualcosa.


  
In qualsiasi paese civile, le tasse vanno pagate, è un dovere sociale. Un paese dove non paghi le tasse, non ti da nessuna garanzia, ma in Italia dove paghi tanto, non hai garanzie! Mi chiedo alle volte come faccia questo paese ad andare avanti ed era, più o meno, la stessa domanda che mi facevo anche in Lituania. In [Svezia](http://it.wikipedia.org/wiki/Svezia) si paga il 50% di tasse. Ma la Svezia offre dei servizi ed è un paese in cui [vale la pena vivere](http://www.dioblog.it/2007/02/15/7-buoni-motivi-per-vivere-in-svezia.html). In Italia paghiamo le tasse svedesi per avere meno servizi di quanto ti offre la Lituania. Il medico lo pago, l'asilo per Greta lo pago, le medicine non le scali un cazzo di niente, l'autostrada la paghi, l'iva la paghi. **Si paga tutto!**

  
Ho ragione o no?

  


