Title: Inflazione in aumento: il prezzo del cibo sale e il lavoro è sempre pagato una miseria
Date: 2011-02-23
Category: Lituania
Slug: inflazione-in-aumento-il-prezzo-del-cibo-sale-e-il-lavoro-è-sempre-pagato-una-miseria
Author: Karim N Gorjux
Summary: 

[![](http://www.karimblog.net/wp-content/uploads/2011/02/inflazione.gif "inflazione")](http://www.karimblog.net/wp-content/uploads/2011/02/inflazione.gif)

 Christian è un italiano che vive in Lituania come me da tanto tempo. Parla il lituano, lavora in Lituania e vive in mezzo ai lituani. Oggi su facebook si discuteva dell'inflazione lituana che continua ad aumentare; il lavoro oltre a diminuire continua ad essere mal pagato e il prezzo del cibo continua ad aumentare. Ecco il suo commento che riporto integralmente.


 *Il problema è che la Lituania sta diventando una succursale dell'occidente. Non appartiene più ai Lituani. E' un'ottima meta turistica per un americano o un norvegese, i quali vogliono mangiare qualitativamente al ristorante con soli 9 euro, od affittare un appartamento al centro della capitale con "soli" 600 euro. Ma comparando il tutto con il potere d'acquisto lituano ci si accorge che e' pura follia.  
  
Il cibo secondo me ancora non è cosi inabordabile per le tasche lituane, perchè ha prezzi alti, ma comparati allo standard lituano (per esempio in Italia paghi 40 euro al ristorante, in Lituania 40 litas). Ma la benzina, il cinema, l'abbigliamento, il teatro, gli elettrodomestici e **soprattutto gli affitti sono semplicemente fuori da ogni logica**. E questo perche' tutti pretendono di guadagnare in Lituania quanto guadagnerebbero in un paese occidentale. Gli stipendi sono 3 volte meno copiosi, ma tutto costa esattamente quanto in Italia e a volte anche di piu' in valore assoluto! Sai qual'e' la cosa paradossale? *

 La soluzione per questo problema sarebbe l'unica cosa che ora i Lituani non farebbero mai, memori di un tragico passato: una rivoluzione nazional-comunista come a Cuba! Solo la nazionalizzazione porterebbe ad avere una Lituania lituana. A volte mi viene in mente un paradosso: i Lituani hanno barattato una schiavitù di fatto, quella dell'oppressione russa, con una schiavitu' ancora piu' sottile e subdola, quella del capitalismo selvaggio dei vari Akropolys, del consumismo, della povertà, della "minima sopravvivenza". Mi fa molto ridere sentire parlare i giovani Lituani, del fatto che ora è tutto molto meglio, ora si possono comprare macchine europee nuove, ora si può viaggiare dovunque, si ma con quali soldi? Se un giovane guadagna 1500 litas ed il solo affitto ne costa 1000? Quale macchina nuova? Quali viaggi? E soprattutto, quale sanità? E' tutto a pagamento come in America! Almeno quando c'era il comunismo tutti avevano un appartamento, tutti avevano diritto alla sanità (almeno teoricamente) e tutta la cultura era gratis! Cinema, teatro, concerti, tutto gratis, addirittura regalavano anche gli strumenti musicali a chi voleva imparare. Ora la gente fa fatica ad arrivare a fine mese solo con bollette e spesa, figuriamoci i diversivi vari... 

 **Non voglio dire che con il comunismo fosse meglio**, voglio dire solo che ad una condizione di svantaggio visibile, è subentrata una condizione di svantaggio mascherato. Ma e' comunque uno svantaggio. I Lituania risponderebbero alle mie parole: "si ma si puo' andare a lavorare all'estero, guadagnare una fortuna in 10 anni e poi tornare in patria", si, ma allora la Lituania-stato che ci sta a fare, se non puo' garantire il diritto al lavoro? Se non pul garantire la permanenza lavorativa in patria? 

 **Un altro paradosso**: durante il comunismo non si poteva andare all'estero, ora si è costretti ad andarci per vivere...


