Title: A cena con il professore di piemontese
Date: 2006-04-27
Category: altro
Slug: a-cena-con-il-professore-di-piemontese
Author: Karim N Gorjux
Summary: Ieri sera sono andato a cena con Jack, mi ha sorpreso rivederlo dopo tanto tempo, non me lo aspettavo. Jack è indiano, già sposato, ma senza figli. Ha 23 anni e da[...]

Ieri sera sono andato a cena con Jack, mi ha sorpreso rivederlo dopo tanto tempo, non me lo aspettavo. Jack è indiano, già sposato, ma senza figli. Ha 23 anni e da circa 10 viene in Italia a lavorare.  
  
Jack parla [l'hindi](http://it.wikipedia.org/wiki/Hindi) e il piemontese, soprattutto quest'ultimo lo parla come se fosse nato a Cuneo; è un piemontese disinvolto senza nemmeno una stonatura d'accento che ha imparato lavorando con i nostri contadini, badando a tori e a mucche (sacre).


 Al ristorante indiano, che è tutta un'altra cosa se ci vai con Jack, parlavo italiano con il cameriere, piemontese con Jack e ascoltavo l'hindi quando Jack parlava con il cameriere. Alla fine ha offerto cena Jack: *"Lasa perdi, ti las un fiol da manteni..."*  
  
[![Kappa-Jack](http://www.karimblog.net/wp-content/uploads/2006/04/Kappa-Jack-tm.jpg "Kappa-Jack")](http://www.karimblog.net/wp-content/uploads/2006/04/Kappa-Jack.jpg)  
  
Ovviamente Jack non è il suo vero nome, in realtà si chiama Pautassi. ;-)  
  
  
adsense  
  


