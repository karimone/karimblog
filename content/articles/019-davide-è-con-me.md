Title: Davide è con me
Date: 2004-08-28
Category: Lettonia
Slug: davide-è-con-me
Author: Karim N Gorjux
Summary: Siamo agli sgoccioli... partiro' il primo settembre nel pomeriggio e la sera saro' in centallo a fare due chiacchiere con gli amici. Ora mi trovo con davide che e' giusto arrivato ieri[...]

Siamo agli sgoccioli... partiro' il primo settembre nel pomeriggio e la sera saro' in centallo a fare due chiacchiere con gli amici. Ora mi trovo con davide che e' giusto arrivato ieri dall'italia e che stara' con me fino a Domenica...


 Non ho molte foto riguardo Auce perche' come ho gia' detto sono arrivato di notte e il giorno in cui stavo male ha piovuto sempre e non ho fatto foto. In casa non potevo fare foto perche' la mamma di Kristina non gradiva quindi... nulla! La stessa cosa mi e' capitata in un negozio di bevande in Auce dove sono rimasto meravigliato dalla quantita' di bottiglie alcoliche esposte, ho chiesto di fare una foto e mi hanno risposto con un NO. E' proprio vero che noi italiani siamo famosi per la nostra cordialità, dalle nostre parti queste cose non succedono.


 Dal centro di Auce a casa di Kristina ci sono 2km che abbiamo fatto a piedi tra strade asfaltate e campi. La casa l'avete vista nella foto, l'interno e' meglio di sarkandaugava sotto certi aspetti, ma sotto altri e' tale e quale. Prima cosa e' fredda, gli spifferi sono tanti e il fratello mi ha detto che d'inverno e' normale avere nelle stanze piu' fredde anche poco piu' di 0 gradi. (qui d'inverno si toccano i -15) I pavimenti non sono piastrellati, ma sono solo rivestiti di un tessuto che raffigura ogni tanto del palchetto in legno e altre volte delle piastrelle. I muri sono in pietra / mattoni e ogni tanto hanno delle asse di legno come rivestimento, il tutto fatto molto artigianalmente. Del bagno non dico nulla... penso solo alle mie amiche schizzinose.


 Sia colazione, pranzo e cena ho mangiato cibi tipicamente locali. Cibi ottimi, ma molto pesanti. La colazione e' normale: latte o the (sconsiglio il caffe') con biscotti, marmellata e miele. A pranzo ho assaggiato dei panzerotti fatti dalla mamma la mattina stessa, sono simili ai nostri ma molta pasta e poca carne e soprattutto fritti. Altra cosa buona l'insalata di riso, mais, polpa di granchi e maionese. La sera patate bollite e polpette di carne.   
La notte e' stata tranquilla, ho dormito sotto 4 coperte accompagnato da un silenzio assoluto e ogni tanto il tichettio della pioggia. Ho dormito nel letto del fratello, con il fratello. (letto matrimoniale.. che pensate!?)

 Il ritorno a Riga e' stato via bus, il prezzo meno di 2 lat. Essendo un servizio statale il bus e' molto vecchio, ma sempre piu' comodo del treno. Preparatevi all'effetto shake perche' le strade sono piene di buche e il bus ci mette del suo quindi passerete tutto il viaggio a saltellare pero' se vi impegnate riuscite anche a dormire.


 Due righe sul cimitero di cui ho messo le foto: si trova a 4km dal centro. E' un vecchio cimitero abbandonato e ha attirato la mia attenzione perche' ad una prima occhiata dal bus mi sembrava un parco poi... mi sono accorto che era un cimitero in mezzo a case abitate. Un pugno nell'occhio! Particolare e' la gente che passa in mezzo al cimitero per tagliare il quartiere e risparmiare 5 minuti. Di notte i ragazzi vanno a bere e a fumare tra le lapidi...


