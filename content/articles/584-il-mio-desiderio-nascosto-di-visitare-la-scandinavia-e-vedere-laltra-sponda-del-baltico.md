Title: Il mio desiderio nascosto di visitare la Scandinavia e vedere l'altra sponda del Baltico
Date: 2010-01-19
Category: Lituania
Slug: il-mio-desiderio-nascosto-di-visitare-la-scandinavia-e-vedere-laltra-sponda-del-baltico
Author: Karim N Gorjux
Summary: Recentemente ho scoperto il sito italiansinfuga gestito da un cuneese emigrato in Australia. Ho avuto il piacere e l'onore di concedermi per un'intervista che è stata pubblicata il 15 Gennaio con il[...]

[![](http://farm3.static.flickr.com/2121/2175609642_9a9a32b86f_m.jpg "Sweden")](http://www.flickr.com/photos/swedish_institute/2175609642/)Recentemente ho scoperto il sito [italiansinfuga](http://www.italiansinfuga.com/) gestito da un cuneese emigrato in Australia. Ho avuto il piacere e l'onore di concedermi per un'intervista che è stata pubblicata il 15 Gennaio con il titolo: [Emigrare in Lituania da chi l'ha fatto](http://www.italiansinfuga.com/2010/01/15/emigrare-in-lituania-da-chi-lha-fatto/ "Mia intervista su Italians in fuga").


 A parte i visitatori in più, ho avuto il piacere di conoscere (direttamente ed indirettamente) persone che vivono in Svezia e che, proprio come me, curano un blog sulle proprie esperienze. [Vivere in Svezia](http://vivereinsvezia.blogspot.com/ "Vivere in Svezia blog su blogspot") offre degli articoli interessanti esattamente come il blog [One Way to Sweden](http://onewaytosweden.blogspot.com/ "Blog a quattro mani sulla Svezia") che è scritto a quattro mani da dei ragazzi che risiedono nella terra degli Abba e dell'IKEA.  
   
Parlo di Svezia perché è uno dei paesi che prima o poi andrò a visitare. Il lato più Europeo del Baltico mi ha sempre affascinato, vedere un paese che si dice (e ripeto si dice) sarà il futuro dei paesi baltici e un'esperienza che voglio assolutamente fare al più presto. Nel 1990 sono stato in Danimarca, ero piccolo, ma conservo ancora un bel ricordo. La Norvegia ho sentito dire che circa 20 anni era molto simile alla Lituania di adesso, la Svezia è la patria degli Abba ed IKEA mentre la Finlandia è la casa della [Nokia](http://www.nokia.it) e di [Linux](http://it.wikipedia.org/wiki/Linux). 

 **Perché parlo di Svezia?** Perché se non fosse che ho una bimba, forse mi sarei già mosso per vedere altri nidi, non sono il tipo da basarmi sulle statistische come si potrebbe fare leggendo [questo commento](http://www.karimblog.net/2010/01/14/emigrare-e-una-scommessa-per-la-vita-sicuro-di-aver-fatto-la-scelta-giusta/#comment-53320) al mio post precedente, ma penso che sia opportuno verificare di persona la realtà di un paese. 

 Mia moglie tra qualche mese sarà infermiera professionista con laurea, in Italia dovrebbe passare circa 1 anno prima della conversione della sua laurea e poter lavorare come si deve, in Scandinavia il problema non si porrebbe, sarebbe assunta velocemente, avrebbe il corso di lingua locale (anche se tutti bene o male parlano inglese) e avrebbe uno stipendio dalle 3 alle 4 volte superiore allo stipendio lituano. Non è poi solo una questione pecuniaria, ma anche di professionalità. Qui in Lituania la situazione sanitaria è molto particolare, la professione è legata a gerarchie, amicizie e favoritismi, ma per ora non so ancora dirvi nulla di certo. A Maggio Rita sarà laureata e la prospettiva di tornare in Italia si fa sempre più remota.


 Dei miei visitatori residenti in Lituania, quanti consiglierebbero di venire a vivere in Lituania da salariati? E perché?

