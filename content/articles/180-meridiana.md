Title: Meridiana
Date: 2006-05-23
Category: Lituania
Slug: meridiana
Author: Karim N Gorjux
Summary: 

![Meridiana](http://static.flickr.com/52/151450719_cd0e461299_m.jpg)

"[Meridiana](http://www.flickr.com/photos/kmen-org/151450719/)" by [karimblog](http://www.flickr.com/photos/72323144@N00/)

  
  
La Meridiana è il simbolo di Klaipeda, ci sono stato ieri sera con Enrico e Rita, alle 10:30 c'era ancora un po' di sole e l'aria era calda, non avevo nemmeno la giacca, si stava da Dio.   


  
Ho scoperto che la Svyturis produce una linea particolare di birra solo per la Meridiana, con 4 litas ti puoi gustare una media nella stiva di una vecchia barca. Non è cosa da poco se pensiamo che il Boogie Woogie (caratteristico locale del centro), ha alzato i prezzi e ora un caffè costa più che in Italia.   


  
Alle 23 Rita era già stanca, siamo tornati a casa con il microbus. Bisogna portare pazienza.  


  
Oggi, porca miseria, piove.  




  technorati tags start tags: [boogie woogie](http://www.technorati.com/tag/boogie woogie), [caffè](http://www.technorati.com/tag/caffè), [euro](http://www.technorati.com/tag/euro), [inflazione](http://www.technorati.com/tag/inflazione), [lithuania](http://www.technorati.com/tag/lithuania), [lituania](http://www.technorati.com/tag/lituania), [meridiana](http://www.technorati.com/tag/meridiana), [svyturis](http://www.technorati.com/tag/svyturis)



  technorati tags end 

