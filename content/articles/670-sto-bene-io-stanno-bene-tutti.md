Title: Sto bene io, stanno bene tutti
Date: 2016-06-14
Category: altro
Slug: sto-bene-io-stanno-bene-tutti
Author: Karim N Gorjux
Summary: La maledizione che colpisce noi expat, soprattutto da quando facebook e' diventato cosi' popolare e' di essere rimossi da amici e persino da parenti quando scriviamo delle differenze tra il paese in[...]

La maledizione che colpisce noi expat, soprattutto da quando facebook e' diventato cosi' popolare e' di essere rimossi da amici e persino da parenti quando scriviamo delle differenze tra il paese in cui siamo e le nostre esperienze passate.


  
  
Quando ero in Italia mi ero reso conto che il *canto del mio lamento *era sulla bocca di piu' o meno tutti quelli che conosco. Sei vai al pub o parli di qualsiasi cosa che riguardi politica o vita quotidiana si finisce per entrare nei luoghi comuni e schifare l'Italia intera. Ultimamente va' molto di moda prendersela con i gli ingegneri, informatici, dottori, chimici che salgono sui gommoni per arrivare a Lampedusa a fregare il lavoro agli italiani. (nota l'ironia). Non e' dei lamenti in se' che voglio scrivere, ma del lamentarsi.


  
Lamentarsi e' un diritto se porta ad un'azione. Lamentarsi e basta e' fine a se stesso. Quindi ora che io mi meraviglio quotidianamente di come funziona l'Australia ed ogni tanto scrivo qualcosa su facebook, finisce che vengo criticato perche', testuali parole, "tiro merda sull'Italia".


  
Fatto curioso, chi mi ha accusato del puntare il dito sull'Italia, quando ero in Italia era tra quelli che erano in modalita' lamento, ma cosi' e', non mi stupisce.


  
La mentalita' italiana nella maggior parte dei casi, ma non tutti, e' ridotta in una semplice regola



 
>   
> Sto bene io, stanno bene tutti.

> 
>   
> Sto male io, devono stare male tutti.

> 
>   
>   


