Title: Due giorni di sole
Date: 2006-03-09
Category: Lituania
Slug: due-giorni-di-sole
Author: Karim N Gorjux
Summary: Sembra incredibile, ma la Lituania mi ha regalato due giorni di sole consecutivi senza nevicate o piogge intermedie. Sembra estate! :-)

Sembra incredibile, ma la Lituania mi ha regalato due giorni di sole consecutivi senza nevicate o piogge intermedie. Sembra estate! :-)  


 Come se la passano la vita i miei lettori? Tra 15 giorni mi imbarco sull'aereo a Palanga, saluto la sirenetta di Copenaghen e ritorno nel cuneese entro la serata di Venerdi' 24 Marzo. Rimarrò per 1 mesetto nella granda a vedere amici, parenti, montagne, palestra e a pianificare il futuro..


 Sembra che in quella panciona ci sia un mezzo terrone che si sente allo stretto, ha voglia di muoversi. A volte invidio Rita che può sentire tutto, mentre io sono estraneo ad ogni cosa e posso solo capirla tramite lei. Sono sicuro che quando partorirà, penserò tra me e me: *"Il poter pisciare contro un albero stando in piedi è il privilegio più bello del mondo"*  
  
  
adsense  
  


