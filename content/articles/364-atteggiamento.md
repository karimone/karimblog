Title: Atteggiamento
Date: 2007-09-04
Category: altro
Slug: atteggiamento
Author: Karim N Gorjux
Summary: "La fontana della gioia deve sgorgare dalla mente;e colui che conosce così poco la natura umanada cercare di ottenere la felicità,cambiando qualsiasi cosa tranne il proprio atteggiamento,sprecherà la sua vita in sforzi[...]

*"La fontana della gioia deve sgorgare dalla mente;  
e colui che conosce così poco la natura umana  
da cercare di ottenere la felicità,  
cambiando qualsiasi cosa tranne il proprio atteggiamento,  
sprecherà la sua vita in sforzi infruttuosi  
e moltiplicherà il dolo che si proponeva di eliminare"*

 