Title: Passiamo la palla?
Date: 2007-01-22
Category: altro
Slug: passiamo-la-palla
Author: Karim N Gorjux
Summary: Ma cosa ne pensano Daniela, Matteo, Massimo, Sandro, Gaetano, Cristian del blog? Come spiegherebbero il blog ad una persona che non sa niente di blog? Aspetto un bel post..

Ma cosa ne pensano [Daniela](http://www.danielaforconi.net), [Matteo](http://blog.wastedthoughts.net/), [Massimo](http://www.selfmarketing.wordpress.com), [Sandro](http://kaunaslife.blogspot.com/), [Gaetano](http://www.magogae.net/), [Cristian](http://www.cristianbarra.com/) del blog? Come spiegherebbero il blog ad una persona che non sa niente di blog? Aspetto un bel post..


 PS: Massimo... ora qual'è il tuo blog?

