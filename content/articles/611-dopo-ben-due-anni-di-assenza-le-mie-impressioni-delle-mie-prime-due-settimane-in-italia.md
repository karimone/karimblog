Title: Dopo ben due anni di assenza le mie impressioni delle mie prime due settimane in Italia
Date: 2010-07-13
Category: Lituania
Slug: dopo-ben-due-anni-di-assenza-le-mie-impressioni-delle-mie-prime-due-settimane-in-italia
Author: Karim N Gorjux
Summary: Ho lasciato l'Italia quando la parola crisi veniva usata per lo più per le coppie sopraffatte dalla monotonia, ora, dopoben due anni di assenza, è un paio di settimane che giro e[...]

[![](http://www.karimblog.net/wp-content/uploads/2010/07/Made-in-Italy-Fiat-500-04-07-07.jpg "Made in Italy Fiat ")](http://www.karimblog.net/wp-content/uploads/2010/07/Made-in-Italy-Fiat-500-04-07-07.jpg)Ho lasciato l'Italia quando la parola crisi veniva usata per lo più per le coppie sopraffatte dalla monotonia, ora, dopoben due anni di assenza, è un paio di settimane che giro e viaggio per la provincia Granda (Cuneo) a rivedere luoghi e a notare differenze in meglio o in peggio rispetto a quando sono partito.


 Siamo arrivati a Bergamo il 23 Giugno. Afa, caldo, cemento e un sacco di macchine. Se non sei mai stato in Lituania o nel nord Europa in generale non potrai mai capire l'impatto che può avere passare da un paese con densità popolare di 52 abitanti per chilometro ad uno con densità popolare 200 abitanti per chilometro come l'Italia. **Il cemento è dappertutto, le macchine sono tante e quasi tutte nuove grazie** agli ultimi incentivi statali. Non ci sono Audi 80 e soprattutto non ci sono Lexus che spuntano da tutti gli angoli.  
  
Il viaggio da Bergamo alla provincia di Cuneo è lungo, ma non noioso, dopo più di due ore attraverso le autostrade del nord d'Italia arriviamo a Centallo a pochi chilometri da Cuneo. La temperatura supera i trenta gradi, l'afa è insopportabile e** i cartelli "vendesi / cedesi" avvistati durante il viaggio sono incalcolabili**.


 Centallo è esattamente dove l'ho lasciata, non è cambiato molto del paese, ma la gente è sempre la stessa soltanto con qualche ruga o qualche capello bianco in più. Tutto è uguale e segue sempre lo stesso ritmo e abitudini che avevo lasciato due anni fa. La stessa gente nei negozi, le stesse persone sedute nei dehor del bar, gli stessi cartelli, le stesse vie, tutto sembra lottare per sopravvivere all'inesorabile passare del tempo. I bambini sono cresciuti, le ragazze diventano donne e gli stranieri sono sempre più numerosi.


 **Le montagne sono sempre le stesse: stupende e maestose**. L'afa purtroppo non mi permette di godermi il panorama come può succedere nelle mattinate d'inverno, ma mi è bastato un viaggio nelle nostre valli per abbuffarmi gli occhi di serpeggianti panorami montagnosi. Non parliamo poi del cibo che da solo vale la pena per un viaggio in Italia. In pochi giorni ho mangiato e gustato di tutto a discapito del mio metabolismo ozioso e addormentato; frutta, verdura, pizza, pasta, formaggi, vino, gelati... un festival di sapori e di genuinità che non ci godevamo da troppo tempo.


 **Ho potuto apprezzare nuovamente la lettura del quotidian**o comprato fresco di giornata in edicola, i Tex, i Dylan Dog e una di quelle decine di riviste d'informatica che non sfogliavo da tempo. Ho visto i centri commerciali e ho persino comprato un nuovo numero di telefono dato che quello che avevo mi è "scaduto" senza possibilità di recupero. Qui iniziano le note dolenti di questo paese che vive di contraddizioni continue.


 **Per avere due numeri di telefono della TIM ho dovuto aspettare ben 45 minut**i. La commessa del negozio mi chiede per prima cosa il codice fiscale e la carta d'identità, sparisce per una decina di minuti per tornarsene con le fotocopie fresche di stampa e poi inizia ad inserire dati al computer. Dopo 15 minuti si scusa dicendomi che il computer non funziona, che la linea è bloccata e che l'adsl fa le bizze. Mi chiede se voglio la tariffa a 2€ la settimana per chiamare un numero gratis e che se non l'attivo ora poi mi costerà 9€ attivarla... dopo una ventina di minuti ho due schede TIM da inserire nel telefono, Rita non ne poteva più di aspettare e la bimba era nervosa. Andiamo via notando che l'unico aspetto positivo è che il negozio aveva l'aria condizionata. Naturalmente ho dovuto passare un pomeriggio sul sito della TIM per capire quale tariffa era meno ladra di altre dopo aver speso 10€ di telefono in meno di un giorno.


 Quando sono arrivato in Lituania due anni fa, [arrivai in macchina](http://www.karimblog.net/2008/08/27/il-viaggio-in-macchina-italia-lituania/) ed entrai in territorio lituano a notte fonda. Alla prima area di servizio mi sono comprato un numero di telefono TELE2 senza dare nessun documento. Ho pagato meno di 1€ e ho atteso meno di un minuto. Non vorrei esagerare, ma **in due anni di permanenza in Lituania, penso di non aver speso più di 50€ di telefono.**

 A parte questo episodio ho notato che l'immutabilità dell'Italia non è una nota positiva. I giovani sono sempre più ignoranti, ma trasportano i loro culi su macchine più nuove. Internet è sempre inaccessibile e lenta uguale, i "negozietti" sono sempre di meno, il TG1 ha subito l'influenza di quell'aborto di telegiornale di Studio Aperto, la benzina è sempre venduta ad un prezzo folle, i politici dicono sempre le stesse cose, le persone si lamentano sempre delle stesse cose e i servizi che dovrebbe offrire lo stato ad un prezzo accessibile costano sempre di più.


 Ho anche guardato le offerte di lavoro, ho cercato per la mia categoria e ho notato che si cercano venditori con competenze d'informatica per vendere a cottimo. Mia moglie che è da poco diventata infermiera laureata, ha parlato con una nostra amica che vive e lavora qui ed ha una figlia da mantenersi. Il lavoro per le infermiere c'è, ma lo stato è poco presente. L'asilo costa caro, le tasse sono alte e i servizi non ci sono. Se vuoi crescere figli la vita qui è solo lavoro e tanto lavoro e se sei un lavoratore in proprio sei obbligato a fare il "furbo" altrimenti soccombi.


 Intanto ufficializzo il mio ritorno. Il 6 Agosto sarò nuovamente in Lituania.


