Title: Il viaggio in macchina: Italia - Lituania
Date: 2008-08-27
Category: Lituania
Slug: il-viaggio-in-macchina-italia-lituania
Author: Karim N Gorjux
Summary: Inizio come i programmi di Wrestrling: don't try this at home.

Inizio come i programmi di Wrestrling: **don't try this at home. **

 Si perché io mi sono fatto circa 30 ore di macchina e il viaggio è durato circa 40 ore. In pratica mi sono fatto 1300km al giorno, ma è stata una **vera stronzata**. Prendetevela con calma e se potete evitate di passare per la Polonia, fatelo! Piuttosto fatevi un bel giro per mare usando il traghetto per la tratta [Kiel - Klaipeda](http://www.lisco.lt/?en=1164293321 "Sito della Lisco") (che se volete prenotare dovete farlo con 10 giorni di anticipo). In pratica vi dovete fare solamente la Germania e vi evitate tutte le rogne del viaggio.


  **Premessa**  
Ho cercato su internet qualche articolo riguardo il viaggio in macchina tra l'Italia e la Lituania, ma a parte qualche accenno sui soliti forum, non ho trovato niente di concreto e quindi eccomi qua a colmare l'ennesima lacuna con la mia esperienza. Ecco cosa dovete assolutamente procurarvi o accertarvi prima di partire:  
  
 * Essere sicuri di poter effettuare delle chiamate dall'estero con il cellulare
  
 * Avere tutti i numeri di emergenza per i vari paesi in cui passerete, meglio ancora se aggiungete i numeri delle ambasciate italiane
  
 * Un atlante stradale aggiornato, se è vecchio buttatelo via, prendete **l'ultima edizione** che riuscite a trovare. **Non fidatev**i ciecamente del GPS
  
 * Una bussola: perché non sempre si riesce a capire dove si trova il sole e non sempre il GPS funziona (consigliato da [Flavio](http://flaepitta.blogspot.com/ "Blog di Flavio"))
  
 * Il GPS, che aiuta parecchio, ma solo se avete già verificato tutto su carta.

  
 * Tanta musica sull'ipod
  
 * Un bel po' di redbull ;-)
  
  
**Il viaggio**

   
[Visualizzazione ingrandita della mappa](http://maps.google.it/maps?f=d&saddr=Centallo+CN+%4044.503117,7.58503&daddr=A55+%4045.028070,+7.593100+to:Via+Lucmagn+%4046.651390,+8.851580+to:An+der+Zschopau%2FS202+%4050.932560,+13.028540+to:263+%4052.227940,+18.901290+to:263+%4052.249480,+18.911274+to:Objazdowa+%4052.210970,+19.369430+to:A11%2FE272+%4056.005460,+22.276470&hl=it&geocode=12628747536727542319,44.503117,7.585030%3B17038312636387213090,45.028070,7.593100%3B16992147176118538459,46.651390,8.851580%3B4610563751016814351,50.932560,13.028540%3B8651050450610580448,52.227940,18.901290%3B6973501319071329665,52.249480,18.911274%3B11744593704153328611,52.210970,19.369430%3B9325747192904045365,56.005460,22.276470&mra=ls&via=1,2,3,4,5,6&sll=47.791016,13.359375&sspn=3.845393,11.645508&ie=UTF8&ll=50.25428,15.714675&spn=19.717459,37.353516&z=4&source=embed)

 L'Italia la conoscete quindi problemi non ce ne sono, entrare in Svizzera non è un problema. Io mi sono fatto un sacco di paranoie riguardo al mio "bagaglio a mano" che occupava tutto ciò che era occupabile nella mia Station Wagon, ma alla frontiera l'unica cosa che mi è stata detta è: "Per il pedaggio subito a sinistra". Si perché per viaggiare in autostrada in Svizzera bisogna compare il bollino da applicare sul parabrezza, costa circa 30€ e vale tutto l'anno. Fatevi i vostri conti.


 Arrivato in Svizzera il mio gps mi ha avvertito che dato che dovevo passare io la strada era chiusa, mi ha fatto passare per Lucmagn. In quel momento ho capito di aver fatto una gran cazzata a fidarmi del GPS per farmi tutta l'Europa, mi sono fidato e mi sono caricato di ottimismo cercando di vedere il lato positivo della scelta "obbligata", mi sono goduto i paesaggi che la Svizzera ha da offrire. **Nota**: rispettate i limiti di velocità, la Svizzera è piena di autovelox e di pattuglie stradali, fatevi i 120 km/h anche in autostrada e portate pazienza, la Germania è vicina.


 Fiancheggio il Liechtenstein (che ci metti di meno a superarlo in macchina che a pronunciarne il nome) e mi ritrovo in Austria. Alla dogana non mi dicono nulla, mi fanno passare e basta ma è solo questione di una ventina di minuti che mi ritrovo in Germania. Destinazione Frankfurt sull'Oder ultimo baluardo tedesco prima dell'ingresso nella famigerata Polonia.


 La Germania ha delle autostrade fantastiche popolate da auto di tutte le marche: Audi, BMW, Mercedes, addirittura più belle le mercedes usate un po retro di quelle nuove, Skoda, Wolkswagen... :-) Ogni tanto vedi una Toyota e raramente anche una Fiat, ma ha più l'aria di essersi persa che essere li per un reale motivo.


 I limiti vanno rispettati perché non si scherza, se ti acchiappano non fanno sconti, se c'è un cartello che indica il limite dei 120 iniziano tutti a frenare e andare ai 120 km/h e per chi arriva dall'Italia sembra persino surreale. Al di là di questo ci sono molti tratti **senza limiti di velocità** e io che andavo ai 160 km/h venivo superato continuamente. Mi sono fermato un paio di volte, le aree di servizio iniziano ad essere popolate da biondi e bionde e nei panini mettono i cetrioli: l'Italia inizia davvero ad essere lontana.


 Cosa non mi è piaciuto della Germania sono proprio le aree di servizio. Il più delle volte sono al di fuori dell'autostrada quindi bisogna uscire per poi rientrare e gli incroci non sono ben illuminati quindi non si capiva molto, sarà perché ho beccato la pioggia e la visibilità era scarsa, ma io non mi sono trovato niente bene. Al di là di questo 

 Sono arrivato a Frankenberg alle 23 circa e ho alloggiato al [LandHotel](http://www.landhotel-frankenberg.de/ "Sito dell Landhotel di Frankenberg") dove ho preso una camera singola per 49€. Camera pulita e in ordine, colazione a buffet, ma internet a 8€ l'ora presente sono nella Hall d'ingresso.


 Alle 8 del mattino dell'indomani ero già in viaggio, volevo al più presto entrare in Polonia per poterla attraversare durante le ore diurne e così arrivare in Lituania prima del tramonto (verso le 21). Il problema della Polonia è che non ha una serie di autostrade che comunica con gli altri paesi ha solo dei tratti di autostrade come la E30 che inizia prima di Poznan e non arriva a Varsavia. L'autostrada è fatta benissimo, ma oltre ad essere corta ha anche il coraggio di avere dei pedaggi. Prezzo: 11 zloty ovvero circa 3€. **Non ho cambiato euro in zloty** per tutta la mia permanenza in Polonia, pagavo ai pedaggi in euro e mi veniva dato il resto in zloty che riutilizzavo al pedaggio successivo. Ad ogni modo accettano anche le carte di credito, ma ti fidi a lasciarle in mano a questa gente?

 Il grosso problema del GPS è arrivato dopo l'uscita dall'autostrada per Klodawa ha iniziato a farmi fare delle strade che non erano per niente rassicuranti e dato che mi trovavo nel bel mezzo della Polonia, fare la fine di [Gaetano Scirea](http://it.wikipedia.org/wiki/Gaetano_Scirea "Gaetano Scirea su Wikipedia") non mi andava per niente. Mi sono fermato ad un'area di rifornimento e ho fatto il pieno pagando in Euro e cercando di contrattare sul cambio da "rapina" che mi ha fatto il benzinaio, ma alla fine non ci ho ricavato molto.


 Grazie ad un ragazzo che sapeva qualche parola di inglese sono riuscito a capire quali erano le città e i paesi che dovevo attraversare per arrivare fino a Mariampole, mi aspettavano circa 400km di statali appresso ai camion prima di entrare in territorio lituano. Mi raccomando lasciate perdere il GPS in Polonia, prendete una cartina aggiornata e cercate di raggiungere Mariampole non seguendo la via più breve, ma seguendo la **strada più "larga" **perché più la strada è trafficata e migliori sono le condizioni! Nelle stradine che mi ha fatto passare il gps, mancava completamente la segnaletica orizzontale e il più delle volte persino quella verticale. E' stata un'esperienza che non voglio ripetere mai più.


 Arrivato a Rozan giro in direzione Ostroleka, ma appena intrapresa la strada vedo una piccola area di servizio dove mi fermo per fare fuori i zloty che mi rimangono e riposare un po'. In questa piccola area di servizio conosco [Patrizia](http://www.karimblog.net/2008/08/24/arrivato/#comment-50149) una ragazza di 18 anni di origini polacche che parla italiano. Patrizia è la classica ragazza acqua e sapone di queste terre, simpatica, bella e gentile.


 Trascorro una mezz'oretta da tranquillo, mangio e bevo qualcosa, chiacchiero un po' e sono pronto a farmi gli ultimi 250km che mi separano dalla Lituania. Riparto e intanto inizia a scendere il sole e a farsi scuro. Guidare in Polonia di notte non è il massimo, non sempre le strade hanno una segnaletica decente e soprattutto i camion sono onnipresenti e azzardare il sorpasso in strade come queste può avere delle conseguenze spiacevoli.


 Ho sentito Rita l'ultima volta alle 15, poi non mi ha più chiamato, ma mi sbagliavo lei continuava a chiamarmi, ma il roaming aveva completamente scaricato il mio telefono quindi oltre a non poter fare telefonate non potevo nemmeno riceverne. L'unica nota positiva è che avevo abbastanza benziona per uscire della Polonia ed una macchina che sapevo mi avrebbe portato a destinazione senza problemi.


 Alle 22 circa arrivo al confine lituano che a causa del [trattato di Schengen](http://it.wikipedia.org/wiki/Trattato_di_Schengen "Trattato di Schengen su Wikipedia") è solo una grande area di controllo fantasma, i camion e le macchine attraversano il confine senza accennare al minimo rallentamento. Subito dopo la vecchia dogana vedo una fila interminabile di camion parcheggiati sia sulla carreggiata di destra che di sinistra, la strada è larghissima e i camionisti sembrano godersi il meritato riposo dopo l'arrivo nella terra promessa. Nel classico stile lituano è possibile comprare Litas subito dopo il confine, sono presenti dei piccoli chioschi per il cambio sia sulla carreggiata destra che di sinistra.


 Il mio arrivo in Lituania dopo l'esperienza Polonia è paragonabile all'arrivo in Italia. Alla prima area di servizio sfodero il mio lituano embrionale e riesco a fare cena e acquistare una SIM per il telefono ed una ricarica senza nessuna difficoltà. Telefono a Rita che era quasi disperata dal non avere mie notizie per dirle che entro le 2 sarò a Telsiai e quindi di aspettarmi.


 **Conclusioni**

 Sono arrivato a destinazione senza conseguenze, ma solo molto stanco. Mi sono serviti due giorni per riprendermi, la sera del mio arrivo sono andato a dormire stanco morto e con le gambe a pezzi, il giorno dopo mi sono svegliato ancora stanco. Consiglio di affrontare un viaggio del genere con **molta più calma, fermandosi due notti** e non solo una come ho fatto io. Evitate di superare gli 800km al giorno. Consiglio anche di pianificare delle soste obbligate di almeno mezz'ora e di mangiare da tranquilli, senza fretta.


 Fate attenzione ai solchi in Polonia, le strade sono vecchie e per molti tratti i camion avevano lasciato il solco dei pneumatici alla "Zio Paperone". Prima di partire fate un **bel check-up alla macchina!**

 La mappa con alcuni dettagli e consigli la potete consultare [qui](http://maps.google.it/maps/ms?ie=UTF8&hl=it&msa=0&msid=100524896520482207968.000451cfb46a64abf4b24&ll=51.206883,21.269531&spn=14.359939,46.582031&z=5)

