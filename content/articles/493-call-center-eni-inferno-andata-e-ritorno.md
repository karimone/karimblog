Title: Call Center ENI: inferno andata e ritorno
Date: 2008-10-08
Category: altro
Slug: call-center-eni-inferno-andata-e-ritorno
Author: Karim N Gorjux
Summary: Cercare di fare la disdetta del servizio Eni Gas &amp; Power è una vera torturatortuta. Chiamo circa 20 giorni fa, per un giorno intero, il numero 800 900 700. Per due giorni[...]

Cercare di fare la disdetta del servizio Eni Gas & Power è una vera torturatortuta. Chiamo circa 20 giorni fa, per un giorno intero, il numero 800 900 700. Per due giorni consecutivi il numero è sovvraccarico.


 Funziona così:  
  
 * Chiama l'800 900 700.

  
 * Aspetti
  
 * Premi 1 per le offerte
  
 * Premi 2 per informazioni
  
 * Premi 3 per il tecnico
  
 * Premi 4 per...

  
 * Inserire il codice cliente
  
 * Non hai inserito il codice cliente. Attendi
  
 * Verrà messo in contatto con un operatore
  
 * Dopo 15 minuti....

  
 * Buongiorno sono Ramona di Bucarest, ma in realtà devo far finta di essere in Italia. Sono sottopagata e precaria, come posso esserle utile?
  
 * Cade la linea.

  
 * Richiamo.

  
  
Dopo vari tentativi riesco ad avere un operatore e chiedo l'intervento del tecnico per chiudere il contratto e leggere il contatore (cosa tutta italiana quella del contatore). Ovviamente ci sono problemi al computer e non riescono a leggere qualche dato strano, lascio il numero di mia madre e spero che la cosa si risolva.


 Passano i giorni e chiedo a mia madre riguardo all'ENI. Niente da fare. Non l'ha chiamata nessuno.


 Provo ad andare sul sito dell'ENI, ma non si riesce a fare nulla, la registrazione non va, il codice non lo accetta, questo non è valido e quell'altro pure. Oggi provo a chiamare nuovamente. Dopo quaranta minuti e 4 passaggi di collega consecutivi con la frase "Mi scusi, ma io tratto la ENI elettricità, le passo il collega per il GAS", mi ritrovo a sentire il solito gingle di attesa che chiude il sipario con un inatteso silenzio.


 Richiamo.


 Questa volta sembra rispondere qualcuno, ma il mio "Pronto" echeggia nel telefono accompagnato da rumori di dita impegnate sulle tastiere di un computer. E' l'inferno. Butto giù.


 Richiamo.


 Dopo una interminabile serie di squilli mi risponde "Luca", il precario di turno dall'accento romano. Gli spiego la mia situazione e mi informa che la terminazione del contratto è stata sospesa, ma non mi sa dire da chi. Perfetto. Ho sempre sognato di pagare il riscaldamento al nuovo inquilino del mio ex appartamento.


 Faccio notare a Luca che tutto ciò non ha assolutamente senso, non si può far interrompere la chiusura di un contratto da qualsiasi coglione che riesce a telefonare all'ENI e a parlare con un operatore, forse è il premio per essere riuscito nell'impresa, ma qui lo sponsor sono io e non mi sembra assolutamente il caso.


 Luca ha rimesso in moto la chiusura del contratto. Speriamo in bene.


 Lascio a voi i commenti

