Title: L'appartenenza, l'ideologia e il limite del pensiero
Date: 2010-02-04
Category: altro
Slug: lappartenenza-lideologia-e-il-limite-del-pensiero
Author: Karim N Gorjux
Summary: Vengo generalmente etichettato come simpatizzante di sinistra ogni volta che nomino i miei sentimenti rivolti al nostro pluricitato premier. A me non piacciono le etichette, capisco che per la logica umana poter[...]

[![](http://farm1.static.flickr.com/215/482472428_5f2f592b64_m.jpg "Acqua")](http://www.flickr.com/photos/snapr/482472428/)Vengo generalmente etichettato come simpatizzante di sinistra ogni volta che nomino i miei sentimenti rivolti al nostro pluricitato premier. A me non piacciono le etichette, capisco che per la logica umana poter etichettare qualcosa o qualcuno, catalogarlo e definirlo nel proprio archivio mentale semplifica enormemente i ragionamenti e le conclusioni, ma io mi oppongo a questo modo di interpretare la realtà.


 Non mi piace definirmi di sinistra e non mi piace definirmi di destra perché non ho bisogno di appartenere ad un gruppo per identificare me stesso. So benissimo a cosa stai pensando; secondo te ciò che ho scritto è di una banalità assurda, paragonabile alle sintetiche e finte frasi idealistiche tanto in voga tra gli adolescenti.  
  
**Non appartengo ad un credo religioso e non mi identifico in un credo politico**. Se appartieni ad una fazione politica ne devi sposare i modi di pensare e soprattuto rifiutare qualsiasi cosa che non appartenga alla tua fazione; l'appartenere alla sinistra o alla destra ti differenziam ti divide dalle altre persone, e questo è un limite. Nel momento in cui ti sei etichettato e ti sei aggregato ad un modo di pensare, ti sei limitato.


 Se sei cattolico, automaticamente pensi che la tua religione è nel giusto mentre le altre sono sbagliate. Ovviamente è la stessa cosa che pensano gli altri adepti nei confronti del tuo credo. Se invece fai parte di una fazione politica, devi sposarne il modo di pensare anche se non è completamente congruente al tuo.


 Ogni volta che affronto o propongo un discorso sull'Italia, le discussioni si dividono nelle due appartenenze destra e sinistra. Si litiga per capire chi ha ragione e si lotta per difendere la propria fazione mostrandola come la meno peggiore. **Mai nessuno si pone la domanda più sensata:** "è giusto o è sbagliato"? Il tutto si riassume a difendere la fazione a cui si appartiene che in realtà è come difendere se stessi, la propria identità ed il proprio essere.  
*"Quando non si ha una forma, si può avere qualsiasi forma; quando non si ha uno stile, si può avere qualsiasi stile."*

  
Preferisco la filosofia alla religione perché ha il coraggio di cambiare e rimettersi in discussione. Non siamo noi esseri frutti di un cambiamento costante? Non siamo noi il risultato di un'evoluzione durata centinaia di migliaia di anni con il solo intento di renderci una macchina biologica perfettamente funzionante e adattata all'ambiente che ci circonda? Perché le nostre idee non posso seguire lo stesso procedimento evoluzionistico con **l'obiettivo di perfezionarsi continuamente?**

 Secondo me, la filosofia da sposare è prendere cosa c'è di buono nelle idee, nei libri, nelle persone e farlo diventare proprio per costruire il proprio pensiero che è **indefinibile ed in continuo mutamento.**

 Cosa ne pensi?

