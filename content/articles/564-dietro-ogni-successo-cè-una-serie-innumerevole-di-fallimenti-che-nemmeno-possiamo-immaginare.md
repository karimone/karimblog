Title: Dietro ogni successo c'è una serie innumerevole di fallimenti che nemmeno possiamo immaginare
Date: 2009-09-12
Category: altro
Slug: dietro-ogni-successo-cè-una-serie-innumerevole-di-fallimenti-che-nemmeno-possiamo-immaginare
Author: Karim N Gorjux
Summary: Dietro ogni successo c'è una storia di fallimenti che viene semplicemente dimenticata. Purtroppo però, chi vuole avere successo, deve fare i conti quotidianamente con il fallimento.

[![](http://farm2.static.flickr.com/1133/619307160_019d96a443_m.jpg "Il successo")](http://farm2.static.flickr.com/1133/619307160_019d96a443_b.jpg)Dietro ogni successo c'è una storia di fallimenti che viene semplicemente dimenticata. Purtroppo però, chi vuole avere successo, deve fare i conti quotidianamente con il fallimento.


 Tempo fa ho iniziato la mia attività imprenditoriale, il mio primo tentativo è stato un fallimento ed uno sperpero di denaro inutile. Mi limitassi a questa frase per archiviare la vicenda, allora l'esperienza sarebbe davvero stata inutile, ma in realtà dietro ogni fallimento, o presunto tale, esiste una lezione da imparare. Non accade solo nel business, ma accade anche nella vita di tutti i giorni, con gli amici, la moglie e persino nelle semplici attività quotidiane.  
  
In passato ho fatto delle scelte sbagliate, ho dato fiducia a persone valide nel loro lavoro e nella loro vita, ma che non erano indicate per quello che stavo facendo io. Mi sono scelto un socio che aveva molte qualità, ma di queste qualità nessuna aveva che fare con l'imprenditoria. Ora, dopo più di un anno dalla chiusura di quell'episodio triste della mia vita, ho ben chiaro quali sono stati i miei errori e ragionandoci sopra ho capito che se ora sto seguendo una strada che secondo me è giusta, lo devo soprattutto a quelle esperienze.


 E' importante non darsi mai per vinti, ma questo da solo non è sufficiente. E' importante se non essenziale analizzare gli errori e trarne i giusti insegnamenti.  
  
* Capire quali sono state le cause del fallimento
  
* Capire qual é stata la nostra responsabilità che ha causato fallimento
  
* Trarre la giusta lezione analizzando obiettivamente le nostre convinzioni e le nostre ideologie che devono sempre essere messe in discussione.

  


 Pensavo che se una persona ha un titolo di studio ed è colta, allora è sicuramente un uomo pronto al successo e all'imprenditoria. Questo è errato per due semplici motivi:  
  
2. Se hai una laurea questo non fa di te automaticamente una persona di successo, una delle persone più intelligenti e istruite che conosca ha la terza media ma può prendere a calci nel culo, senza timori, un ingegnere.



 - Il secondo motivo si riassume con una domanda che non mi sono mai fatto: *"Se essere laureati e intelligenti porta automaticamente ad un successo imprenditoriale, perché negli atenei non è pieno di professori e ricercatori milionari?"*
  


