Title: Karim, come và?
Date: 2008-07-31
Category: altro
Slug: karim-come-và
Author: Karim N Gorjux
Summary: Un mio vecchio amico che non sentivo da tempo e che ho ritrovato su facebook, mi chiede come vanno le cose:

Un mio vecchio amico che non sentivo da tempo e che ho ritrovato su [facebook](http://www.facebook.com), mi chiede come vanno le cose:

 *Io sto abbandonando l'Italia. Per quanto non lo so esattamente, posso solo dirti che sarà per un **minimo** di 2 anni.*

 Rita per seguirmi ha abbandonato l'università al terzo anno, pensavamo che con l'UE si potesse facilmente trasferire gli esami di infermieristica all'università di Torino invece qui ha dovuto **ricominciare da zero**. E' durata poco, infatti ha smesso e ha deciso di tornare su in Lituania in modo da poter conseguire la laurea.


 Io qui in Italia ho prima fondato una piccola società uccisa dai troppi costi burocratici e le poche entrare, ora sono libero professionista e lavoro al 99% online ed è ciò che mi permette di rimanere in Lituania continuando a lavorare come se fossi in Italia.


 Devo confessarti che sono molto felice di andarmene, non tanto per i paesaggi, il cibo, la gente e il sole che **mi mancheranno come non mai**, ma sono felice di partire per poter vedere con distacco la situazione in cui è caduta la nostra Italia che, a mio avviso, è caduta in tempi molto bui (la storia si ripete).


