Title: La palestra e non solo..
Date: 2005-04-28
Category: Lituania
Slug: la-palestra-e-non-solo
Author: Karim N Gorjux
Summary: Il bello di avere una connessione ad internet permanente e' che puoi lavorare al computer e ascoltare Radio Deejay. Diamo qualche notizia di rilievo, riguardo al mio cane ho gia' detto tutto[...]

Il bello di avere una connessione ad internet permanente e' che puoi lavorare al computer e ascoltare [Radio Deejay](http://www.deejay.it/default.jsp?s=radio&l=onair). Diamo qualche notizia di rilievo, riguardo al mio cane ho gia' detto tutto in un altro post quindi sorvoliamo. Ieri sono andato nella palestra "superlusso" locale. Dato che il prezzo e' lo stesso della palestra davanti a casa, tanto vale, come dice Davide, viziarsi.


 Giusto qualche nota tecnica riguardo alla palestra, l'attrezzatura e' all'ultimo grido, le macchine sono tutte [technogym](http://www.technogym.it/gate/) della penultima generazione, l'unico atrezzo non technogym e' un tavolo da ping pong stiga che non ho ben capito cosa ci facesse li. Il locale e' ampio e il pavimento e' rivestito con una moquette delicata, i muri sono quasi completamente rivestiti da specchi e ogni tanto qualche pianta in un terreno artificale orna gli angoli della palestra. La sezione dei pesi liberi e' al fondo della palestra, dove regna un rack squat nuovo, bello e particolarmente curato.  
Gli spogliatoi sono ampi, puliti e le doccie hanno il sensore che erogano acqua solo se c'e' qualcuno. Sotto il sensore una manovella permette di scegliere la temperatura dell'acqua. Non ho potuto provare la piscina e l'idromassaggio perche' il costume che mi ha prestato Davide non mi andava.... grasse risate :-D mi tocca comprarne uno.  
Alla fine il prezzo: palestra, idromassaggio, sauna a 55 Euro al mese. Ingresso libero solo nei primi 5 giorni della settimana.


 Ieri sera sono uscito con Davide, Rasa e altri amici, tra cui il Re "Enrico Quinto" residente a Klaipeda dal 1998 e titolare della cattedra d'Italiano all'universita' di Klaipeda. Un personaggio da conoscere sia per la simpatia che la cultura. Grandissimo bevitore della birra locale [Svyturis](http://www.svyturys.lt/en) e padrone di una cultura musicale e cinematografica imbarazzante. Prima o poi metto qualche foto anche di lui...


 Una cosa ho notato ieri sera e a cui non ero tanto abituato, nei pub e nei discopub la percentuale di donne sul totale e' di circa il 70%... Maro'! 8-) Ho anche visto una ragazza che assomigliava a [Cristina Ricci](http://www.anidride.it/special/modelle/33.htm)!

