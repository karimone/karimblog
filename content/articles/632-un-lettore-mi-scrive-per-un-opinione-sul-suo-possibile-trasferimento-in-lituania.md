Title: Un lettore mi scrive per un opinione sul suo possibile trasferimento in Lituania
Date: 2011-03-09
Category: Lituania
Slug: un-lettore-mi-scrive-per-un-opinione-sul-suo-possibile-trasferimento-in-lituania
Author: Karim N Gorjux
Summary: Ciao, sono un tuo lettore, ho 38 anni, sono di Genova ma vivo in Sardegna dove ho un ristorante.Dal 2000 vivo con una ragazza di Klaipeda, ho un figlio di 6 anni,[...]

[![Punto interrogativo](http://www.karimblog.net/wp-content/uploads/2011/03/question_mark.jpeg "Domanda")](http://www.karimblog.net/wp-content/uploads/2011/03/question_mark.jpeg)*Ciao, sono un tuo lettore, ho 38 anni, sono di Genova ma vivo in Sardegna dove ho un ristorante.  
Dal 2000 vivo con una ragazza di Klaipeda, ho un figlio di 6 anni, nato a Klaipeda, ho anche una piccola abitazione lì, è da qualche mese che sto pensando seriamente di mollare tutto e trasferirmi lì, so fare un pò di tutto, soprattutto nel ambito del turismo e della ristorazione, secondo te si riesce a trovare qualcosa da fare?  
Di vivere in Italia non ne posso più, si vive praticamente per pagare i dipendenti e le tasse.  
Mi farebbe veramente piacere avere una tua opinione, visto che vivi li fisso, io ci vengo poco e non ho mai approfondito, anche perchè non mi sono mai impegnato ad imparare la lingua, ma ce la posso fare tranquillamente.  
Saluti*  
  
Carissimo lettore la tua domanda mi viene fatta molto spesso da italiani che vivono in Italia e per un motivo o per un altro vogliono venire in Lituania. **La risposta non è assolutamente facile e scontata** perché cosa posso vedere io con i miei occhi non è per forza ciò che vedresti tu, quindi posso dirti come la penso e motivare i miei giudizi, ma la decisione finale spetta a te dipende solo dalla tua situazione e dai tuoi gusti personali.


 ### Si riesce a lavorare in Lituania?

  
Lavorare in Lituania è ancora più limitante che lavorare in Italia. **Se sei un cuoco probabilmente troveresti lavoro subito qui a Klaipeda**, ma sappi che gli orari sono massacranti e la paga e bassa (500-600€ al mese se va bene), hai pochi vantaggi e per il fatto che sei straniero che non sa la lingua ti troveresti di fronte a possibili fregature che in Italia non si sognerebbero mai di propinarti. Altri lavori li potresti trovare facilmente se tu sapessi il Lituano che non è proprio una lingua semplice. Per avere un metro di misura, se non sai l'inglese o trovi difficile l'inglese allora il lituano ti sembrerà il cinese.


 ### E' semplice per un italiano vivere in Lituania?

  
Dipende. Klaipeda è molto piccola rispetto ad una città come Vilnius quindi anche le possibilità di lavoro sono nettamente ridimensionate e non solo per il lavoro.** Di per sé vivere qui in Lituania è alla lunga molto noioso**. L'inverno è triste, lungo e freddo. L'estate è troppo corta e non sempre così calda come si spera.  
Se hai una famiglia tendi ad annoiarti perché a parte i centri commerciali non ci sono molti svaghi, si fanno sempre le stesse cose e l'unica salvezza è avere delle amicizie locali che purtroppo sono difficilida consolidare se non sai la lingua. Ricordati che [alla fine sei sempre uno straniero](http://www.karimblog.net/2011/01/15/e-alla-fine-sei-sempre-uno-straniero-mio-caro-uzsienieti/) che si porta dietro gli stereotipi del proprio paese.


 Se poi scappi per questioni burocratiche o tasse,** non credere che qui in Lituania sia la manna dal cielo**. I servizi pubblici non sono paragonabili a quelli italiani ad esclusione della scuola pubblica e l'asilo e **le tasse sono alte soprattutto a causa della crisi economica**. La cosa peggiore però è che il grande esodo di questo popolo che già da solo dovrebbe far dubitare una permanenza in questo paese al di fuori del turismo, ha creato una generazione di professionisti incapaci.


 Mi spiego meglio. I lituani che emigrano sono per lo più gente che ha un mestiere e che è stufa di essere pagata 1/4 o 1/5 rispetto ad altri paesi d'Europa. Il problema è che generalmente sono i più bravi che scappano via, gente che ha qualità e che sa di avere la possibilità di farcela quindi qui in Lituania rimangono b**en pochi professionisti che sono in grado di fare qualcosa di decente** ed il problema è riuscire a trovarli. Almeno questo è quanto vedo a Klaipeda.


 Guarda, queste sono solo mie osservazioni e tieni conto che io vivo con la Lituania già dal 2005, ma questo non vuol dire niente, puoi sempre provare a venire qui e farti la tua opinione. Secondo me la Lituania cambia molto dalle proprie possibilità, gusti e filosofia di vita.


 Dimenticavo: ci sono tante **belle donne e sembra che la birra sia di ottima fattura** (io sono astemio), ma ti garantisco che sono **due qualità della Lituania che non sempre sono abbastanza motivanti per vivere qui**.


