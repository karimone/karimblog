Title: Per poter vivere bene in Lituania c'è bisogno di calcio (giocato)
Date: 2009-04-28
Category: Lituania
Slug: per-poter-vivere-bene-in-lituania-cè-bisogno-di-calcio-giocato
Author: Karim N Gorjux
Summary: Ti ricordi che avevo scritto dell'importanza del calciobalilla nella mia integrazione qui a Klaipeda? Bene, le cose sono un po' cambiate da allora, le giornate ora sono piene di sole e ora[...]

[![Campo da Calcetto](http://farm4.static.flickr.com/3557/3482750036_a5015ec371_m.jpg)](http://www.flickr.com/photos/kmen-org/3482750036/ "DSC_1389 di karimblog, su Flickr")Ti ricordi che [avevo scritto](http://www.karimblog.net/2009/02/16/integrazione-come-ho-iniziato-a-crearmi-una-vita-sociale-in-lituania/) dell'importanza del calciobalilla nella mia integrazione qui a Klaipeda? Bene, le cose sono un po' cambiate da allora, le giornate ora sono piene di sole e ora passo più tempo a giocare il calcio vero e proprio che quello da tavolo.


 Non molto lontano da dove abitavo prima, c'è una piccola scuola con dietro un campo da calcetto con le porte in ferro e il terreno sintetico. Ogni giorno c'è sempre qualcuno che gioca ed è una gran fortuna perché se provo ad organizzare qualcosa con i ragazzi del pub, molto probabilmente mi dicono tutti si, ma alla fine non si presenta mai nessuno ricordandomi un comportamento molto alla sudamericana.  
  
Al momento ho giocato circa 5-6 partite recuperando almeno un po' di fiato che è l'unica cosa che mi manca rispetto ai lituani. Se devo essere sincero i lituani non sono molto bravi a giocare calcio se non qualche raro caso di qualcuno che ha un minimo di senso di gioco. Se caso mai dovessi giocare a calcio con dei lituani, permettimi di darti un consiglio fai molta attenzione soprattutto ai lituani alti e spigolosi perché, anche se non lo fanno apposta, rischiano di farti molto, ma molto male.


 Oggi giocherò di nuovo alle 17:30 e, molto probabilmente, sarà l'ultima partita in cui giocherò con Andrea, uno dei tanti pazzi che è venuto a vivere qui a Klaipeda e che Venerdì riprenderà la macchina e se ne tornerà a casa in Italia. A fine partita gli faremo fare il saluto allo stadio come [Roberto Baggio](http://www.youtube.com/watch?v=5l5PJj_Gkb8) e se ci sarà un rigore, lo tirerà lui :-)

