Title: Foto di Juodkrante
Date: 2005-11-02
Category: Lituania
Slug: foto-di-juodkrante
Author: Karim N Gorjux
Summary: Mi dispiace, ma mi sono accorto che l'importazione da iPhoto non aveva portato anche la descrizione. Ho rimediato a mano dato che la descrizione spiega nella maggior parte dei casi la foto.

Mi dispiace, ma mi sono accorto che l'importazione da iPhoto non aveva portato anche la descrizione. **Ho rimediato a mano** dato che la descrizione spiega nella maggior parte dei casi la foto.


