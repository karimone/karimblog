Title: Gelosia
Date: 2007-11-26
Category: Lituania
Slug: gelosia
Author: Karim N Gorjux
Summary: All'università c'è già qualche studente che ha perso la testa per mia moglie, un professore la guarda sempre come un disperato e già ho saputo che in ospedale i dottori promettono mari[...]

All'università c'è già qualche studente che ha perso la testa per mia moglie, un professore la guarda sempre come un disperato e già ho saputo che in ospedale i dottori promettono mari e monti alle ingenue infermiere matricole pur di far **spuntare le corna** al sottoscritto.


 Il ragazzo di mia sorella è ultra geloso. Mia sorella è tutto sommato un bel tipo: mora, magra, bel viso, simpatica... un po' rincoglionita, ma accettabile. Il mio (forse) futuro cognato si **rode** completamente il fegato per mia sorella tanto da litigare ogni tanto per cose che ancora devono succedere.


 Io non me la prendo, penso di essere stato molto fortunato a trovare Rita, ha un senso della famiglia che è **impossibile da trovare** in una sua coetanea italiana e soprattutto è una brava mamma. 

 Ovviamente **metto le mani avanti**. Non è detto che prima o poi Rita se ne scappi con qualcuno, ma questo è un pericolo presente nella vita coniugale di ogni uomo e non è sempre solo colpa della donna adultera, come vogliono far credere i musulmani, sono gli uomini che ci mettono lo zampino portando a delle situazioni da tradimento.  
Sinceramente penso che **la gelosia ha poco senso**, essere possessivi non porta a nulla e cercare di controllare il partner in modo ostinato è solo una nevrosi distruttiva per entrambi.


 **Una domanda** ai vari estimatori di mia moglie: che ne pensate di **partire per la Lituania** e farvi tutto il mazzo che ho fatto io per trovare la mia anima gemella? Se non avete voglia di viaggiare, fatevi un giro per i night, ma poi non lamentatevi...


