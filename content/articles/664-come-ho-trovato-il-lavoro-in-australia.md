Title: Come ho trovato il lavoro in Australia
Date: 2015-03-11
Category: Australia
Slug: come-ho-trovato-il-lavoro-in-australia
Author: Karim N Gorjux
Summary: Dopo aver fatto le prime ed importantissime faccende burocratiche, dovevo fare ancora due cose. La prima più importante è quella di trovare un lavoro, la seconda di avere una copertura sanitaria.

[![Australia Money](http://www.karimblog.net/wp-content/uploads/2015/03/277757-money-stock-300x169.jpg)](http://www.karimblog.net/wp-content/uploads/2015/03/277757-money-stock.jpg)Dopo aver fatto le [prime ed importantissime](http://www.karimblog.net/2015/02/05/trova-le-differenze-con-litalia-burocrazia-australiana-per-un-cittadino-australiano/ "Trova le differenze con l’Italia: burocrazia australiana per un cittadino australiano") faccende burocratiche, dovevo fare ancora due cose. La prima più importante è quella di trovare un lavoro, la seconda di avere una copertura sanitaria. 

 Per sistemarmi in Australia ho iniziato a muovermi più che potevo già in Europa. Per prima cosa ho usato il più possibile facebook cercando di avere contatti con **persone valide** che potessero condividere la propria esperienza. I gruppi facebook sull'Australia sono così penosi che ho fatto prima a crearne uno io con dei collaboratori di alto livello che mi sono andato a pescare con la lente nei vari gruppi che frequentavo.   


 Circondarmi di persone valide è stato cruciale per il mio arrivo qui in Australia, grazie a loro ho potuto avere dei consigli che mi hanno aiutato a creare **la strategia per stabilirmi qui a Melbourne**. Purtroppo il mercato degli italo-sprovveduti è talmente vasto e ricco che è pieno di agenzie guidate da millantatori pronti a fare di tutto pur di fregarti qualche soldo. Un gruppo molto utile al riguardo è [truffe in Australia](https://www.facebook.com/groups/431731666992221/ "Gruppo Facebook Truffe In Australia").


 Il mio problema all'inizio era capire come stabilirmi da australiano e come trovare lavoro nell'informatica. Ho provato a chiedere nei vari gruppi facebook, ma dato che i moderatori lasciavano che le discussioni degenerassero, non riuscivo mai ad avere una risposta decente. Inizio a fare le prime amicizie e conosco Pierfrancesco con cui creo il gruppo [Australia Facile](https://www.facebook.com/groups/australia.facile/ "Gruppo facebook Australia Facile"), poi creo un gruppo solo per gli [italo australiani](https://www.facebook.com/groups/italo.australiani/ "Gruppo facebook rivolto agli italo australiani") e poi un gruppo rivolto agli [informatici italiani in Australia](https://www.facebook.com/groups/itexperts.in.australia/ "Informatici italiani in Australia"). 

 Fondare questi gruppi mi ha permesso di conoscere delle persone valide, ma molto valide. Da Italo-Australiano conoscere una persona come Robert che ha fatto i tuoi stessi passi prima di te, ti fa sentire meno solo. Sempre grazie a Facebook ho conosciuto Chris che è stato il programmatore che mi ha dato i primi consigli e mi ha aiutato a risolvere le prime difficoltà quando iniziavo ad impostare la mia strategia per cercare lavoro mentre ero ancora in Europa.


 Per trovare lavoro come informatico i passi cruciali sono stati: cambiare indirizzo del mio **profilo linkedin** e assumere un [resume writer australiano](http://www.rooresumes.com/ "RooResume") per riscrivere il mio curriculum vitae che qui viene chiamato "resume". Tramite linkedin ho ricevuto contatti a cadenza settimanale per proposte di lavoro. Il problema era solo rimandare al mio arrivo i possibili colloqui, ma prima di mettere piede in Australia avevo già una interview programmata che al momento è stata l'unica che ho avuto in quanto ho iniziato a lavorare quasi subito.


 Atterrato il 4 Novembre, ho avuto l'intervista il 6 Novembre e il 13 Novembre ho iniziato a lavorare nella startup dove lavoro ora. **Il passo più importante in Australia è trovare il primo lavoro** quindi ho accettato anche una paga al di sotto della media dei programmatori in Australia pur di iniziare a scrivere sul curriculum. Al momento sono 5 mesi che lavoro per una startup e mentre sono in attesa di rinnovo per altri sei mesi, continuo a ricevere proposte tramite linkedin questo non perché sono un fenomeno, ma semplicemente perché il mercato IT in Australia é molto attivo.


