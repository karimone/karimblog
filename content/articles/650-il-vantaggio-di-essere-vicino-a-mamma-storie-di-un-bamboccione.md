Title: Il vantaggio di essere vicino a mamma (storie di un bamboccione)
Date: 2011-11-23
Category: Lituania
Slug: il-vantaggio-di-essere-vicino-a-mamma-storie-di-un-bamboccione
Author: Karim N Gorjux
Summary: Ieri sera Tomas è caduto sullo spigolo del muro. Niente di grave, ma vederlo con la testa gonfia ed un taglio nel mezzo non è la migliore delle esperienze. Stava correndo in[...]

[![](http://www.karimblog.net/wp-content/uploads/2011/11/bamaboccioni4-300x225.jpg "Bamboccioni")](http://www.karimblog.net/wp-content/uploads/2011/11/bamaboccioni4.jpg)Ieri sera Tomas è caduto sullo spigolo del muro. Niente di grave, ma vederlo con la testa gonfia ed un taglio nel mezzo non è la migliore delle esperienze. Stava correndo in salotto quando è scivolato ed ha sbattuto la testa. In quel momento Greta era con la mamma e stava facendo il bagnetto, mentre io stavo pulendo i piatti.


 A Klaipeda era successa una cosa simile. Greta era caduta con delle forbici e si era tagliata, subito di corsa all'ospedale a farla cucire, tutto è andato bene, testimone dell'incidente è stato [Mirko](http://www.mirkobarreca.net/) che ci ha anche aiutato a superare il momento.  
  
Tornando a ieri, memori dell'incidente con Greta, abbiamo subito messo il ghiaccio. Rita era ovviamente sconvolta e Greta era nuda nella vasca da bagno. Da buon bamboccione ho subito chiamato la mamma che abita a 400 metri da dove siamo ora. Noi siamo andati all'ospedale e mia madre si è presa cura di Greta facendole finire il bagnetto e mettendola a letto.


 A Klaipeda mi è successo di avere dei forti mal di pancia e di dover andare all'ospedale. **Poco ci mancava che dovevo andarci da solo**. Ringraziando però siamo riusciti ad organizzarci e Rita quando poteva veniva a trovarmi portandosi entrambi i bambini con sè. Anche Mirko è stato d'aiuto portandomi qualcosa quando Rita non poteva. **In quei momenti ti rendi conto che essere da soli in un paese straniero è molto difficile** da gestire, sopratutto nei momenti difficili.


 Bamboccione, mammone alla fine sono solo parole. Nel mio [ultimo post](http://www.karimblog.net/2011/11/08/dopo-sei-mesi-di-italia/ "Dopo sei mesi in Italia") i commenti sono stati molto eloquenti allo stare "*vicino alla mamma*", questo video spiega il mio punto di vista **personale e soggettivo** della mia scelta.


   
  


