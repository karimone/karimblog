Title: Me stesso. Che onore
Date: 2006-11-03
Category: Lituania
Slug: me-stesso-che-onore
Author: Karim N Gorjux
Summary: Anche io sono passato in Radio. Grazie a Luca Conti ho partecipato allo skypecast in diretta su Radio Al Zero il 23 Ottobre 2006. La puntata era sperimentale, forse il primo tipo[...]

Anche io sono passato in Radio. Grazie a [Luca Conti](http://www.pandemia.info) ho partecipato allo skypecast in diretta su [Radio Al Zero](http://www.radioalzozero.net) il 23 Ottobre 2006. La puntata era sperimentale, **forse il primo tipo** di intervento di gruppo in una radio via internet.


 Dato che la trasmissione è durata quasi un ora, mi sono permesso di segnare i miei tentati interventi durante la trasmissione (notazione minuto:secondi). Primo intervento 11:40 non andato a buon fine per problemi tecnici. Il problema tecnico continua e a 17:50 **confermo via chat** che non sono timido. Finalmente il mio intervento riguardo il blog e la tecnologia a 29:00. Ovviamente ero emozionato, per me era la prima volta in radio.


 Link: [La puntata su Pandemia](http://www.pandemia.info/2006/10/31/web_20_a_noi_nel_mezzo.html)  
  
Link: [Noi nel mezzo sul web 2.0](http://www.radioalzozero.net/podcast/noinelmezzo/mezzo231006.mp3) mp3 (40.7 MB)  


