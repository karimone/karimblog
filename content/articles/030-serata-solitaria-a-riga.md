Title: Serata solitaria a Riga
Date: 2005-01-03
Category: Lettonia
Slug: serata-solitaria-a-riga
Author: Karim N Gorjux
Summary: Interessante la mia serata in Riga, ho incontrato Jana e il suo fidanzato "Virgilio", nostro compaesano di Roma. Con loro ho passato 2 splendide ore parlando del più e del meno dalle[...]

Interessante la mia serata in Riga, ho incontrato Jana e il suo fidanzato "Virgilio", nostro compaesano di Roma. Con loro ho passato 2 splendide ore parlando del più e del meno dalle donne russe ai prezzi degli aerei alle difficoltà linguistiche. In questo momento Virgilio dovrebbe essere nella nostra capitale, lo saluto sperando che abbia fatto buon viaggio.


 Ho rivisto vecchi amici: Rashan e Farhan. Fate caso ai nomi: Karim, Rashan, Fahran; sembrano i nomi di un'associazione a delinquere a sfondo terroristico! Con loro sono entrato GRATIS al Roxy. Il Roxy? Per chi di voi è stato qui posso solo dire che il Roxy è ormai la nuova ambasciata Italiana. :-) Pieno pieno pienissimo di Italiani. Chi di voi è intenzionato a venire da queste parti per valorizzare il fascino italiano e concretizzare i propri sogni erotici... beh... che dire, fate come volete, ma gli italiani hanno stroncato la carriera di Riga sul nascere.  
Le ragazze ormai si vedono al Roxy, in pieno sfogo ormonale a causa dell'abbondante presenza Italiana. Per lo più le ragazze sono brutte, mi spiace ma questi sono i rimasugli, la maggior parte delle ragazze (degne del nome) evita il Roxy e cerca posti dove l'italiano pappagallo non becca, non rompe, non c'è. Scene particolari degne di nota, almeno per me, non ne ho notate. Alcuni Italiani penso di averli già visti, nei miei precedenti tour, tutti in gruppo, festaioli, strafottenti.


 Dopo la serata passata in un'orgia di donne facili e stranieri opportunisti (non mancano le brave ragazze che vogliono solo ballare e non fanno altro che confermare la regola), sono uscito con Rashan a bere qualcosa al patio pizza vicino al Laima, le due cameriere Baiba e Inga allettano l'ultima mezz'ora prima del rientro con russo-inglese e qualche risata ed ecco che vicino a noi si presenta IVAN!. Ragazzi questo Ivan è un personaggio degno di un fumetto, penso di aver trovato il tipico stereotipo del turista sessuale italiano.


 Ivan, da me ribatezzato "Onan il Barbaro" è un ragazzo quasi 30enne che passa i suoi natali nella caotica Milano. Ivan frustrato dalle ragazze che non si prestano a dare sfogo ai suoi istinti in una serata di abbordaggio si trasferisce a lavorare in Polonia. Qui Ivan gode della scarsa concorrenza, del suo pessimo inglese e della sua posizione sociale, finchè gli Italiani invadono la polonia e lui è costretto ad andarsene. Tornato in Italia Ivan medita nuove mete, arruola un amico e dopo aver attraversato l'europa in macchina approda a Riga. Ieri sera quando l'ho conosciuto Ivan era parecchio incazzato, tormentato dalle sue esigenze, deluso dalle sue aspettative, schifato del suo hotel da 10€ al giorno che richiede l'antitetanica aggiornata.


 Ivan è l'incarnazione di una barzelletta, la parodia di un film di Mel Brooks, peccato non aver potuto parlare di più con lui, ma Rashan doveva andarsene o perdeva il tram. Le domande di Ivan erano molto interessanti: "Ma stè troie perchè non la danno?", "Questi Italiani, domani se ne vanno?", "Ma è sempre invaso così stò paese?", "Se le pago me la danno?". Devo dire che è un pò monotematico il ragazzo...


 Concludendo: credo sarò costretto prima o poi a dover esibire il passaporto francese e dire che NON sono Italiano anche se il mio modo di fare è molto tranquillo, non giro con la bava alla bocca. Ho molte amicizie, per lo più ragazze, che ho sempre rispettato. Il passaparola ha fatto il resto :-)

