Title: Un mondo alla rovescia
Date: 2005-10-09
Category: Lituania
Slug: un-mondo-alla-rovescia
Author: Karim N Gorjux
Summary: Oggi è Domenica, le giornate a Klaipeda sono micidiali, da quando sono qui ha piovuto furtivamente solo una sera. Stranamente fa sempre bello, c'è sempre il sole e pochissime nuvole, ma che[...]

Oggi è Domenica, le giornate a Klaipeda sono micidiali, da quando sono qui ha piovuto furtivamente solo una sera. Stranamente fa sempre bello, c'è sempre il sole e pochissime nuvole, ma che diavolo sta succedendo? Dovrebbe piovere, fare buio, dovrebbe fare schifo tutto! Invece mentre qui passi una settimana estiva, in Italia piove piove e piove. Un mondo alla rovescia.  
  
Qui ci sono solo donne, mentre **a Cuneo ci sono solo vecchietti**. Sono le stranezze demografiche in cui io ho sempre dato una grande responsabilità alle montagne. Guardate l'Olanda, tutta pianura, nemmeno una collinetta sperduta sul confine, anzi, Amsterdam scende sotto il livello del mare, ma guardate che paese **aperto e innovativo**. Ti puoi suicidare tranquillamente e le vetrine in centro interessano più gli uomini che le donne. Un paese da sogno.. :-)

 E voi lettori dove siete? Cosa fate? Dov'è il vostro blog che manda un trackback al mio e dice: *"Questo tipo racconta un mucchio di cazzate..."* Niente di tutto ciò. Non c'è nessuno.


