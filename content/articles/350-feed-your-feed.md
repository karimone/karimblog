Title: Feed  your Feed
Date: 2007-07-21
Category: altro
Slug: feed-your-feed
Author: Karim N Gorjux
Summary: Seguo l'esempio di Ivan e per la prima volta riporto una catena di RSS volte a far conoscere nuovi interessanti blog.---- inizio copia ----Copia e incolla la lista che trovi qui aggiungendo[...]

Seguo l'esempio di [Ivan](http://ivanmez.blogspot.com/2007/07/feed-your-feed.html) e per la prima volta riporto una catena di RSS volte a far conoscere nuovi interessanti blog.  
  
  
**---- inizio copia ----**  
  
  
  
2. Copia e incolla la lista che trovi qui **aggiungendo in cima** il tuo blog e il tuo feed RSS e eliminando l’ultimo.

  
4. Sfoglia i blog suggeriti. Se ti piacciono, se non li conoscevi, Clicca sugli RSS e **sottoscrivi** il loro feed.

  
6. Oltre al tuo blog segnala i blog e i feed di **altri 5 amici**, assicurati di mettere i link ai loro blog e soprattutto i link ai loro feed RSS.

  
8. **Diffondi, diffondi, diffondi.**
  
  
  
  
I cinque amici di [karimblog.net](http://www.karimblog.net) da sottoscrivere:  
  
 3. [Subscribe](http://www.pseudotecnico.org/blog/?feed=atom) to [Pseudotecnico](http://www.pseudotecnico.org/blog/)
  
 6. [Subscribe](http://www.sandro.madeinblog.net/?feed=atom) to [Baltic Man](http://www.sandro.madeinblog.net)
  
 9. [Subscribe](http://feeds.feedburner.com/zeligplanet) to [Zelig Planet](#)
  
 12. [Subscribe](http://www.soulsick.info/index.php/feed/atom/) to [iSleepy](http://www.soulsick.info)
  
 15. [Subscribe](http://feeds.feedburner.com/henryblog/fQMN) to [Henryblog](http://www.henryblog.net)
  
   


 The Original modified List:  
  
* [Subscribe](http://feeds.feedburner.com/karimblog) to [Karimblog](http://www.karimblog.net)
  
* [Subscribe](http://feeds.feedburner.com/Azzurro) to [Azzurro](http://ivanmez.blogspot.com/)
  
* [Subscribe](http://feeds.feedburner.com/manfrys/) to [Manfrys](http://www.manfrys.it/)
  
* [Subscribe](http://blogozine.blogspot.com/rss.xml) to [Blogozine](http://blogozine.blogspot.com/)
  
* [Subscribe](http://johncow.com/rss.xml) to [JohnCowdotCom](http://www.johncow.com/)
  
* [Subscribe](http://bobmeetsworld.com/rss.xml) to [BobmeetsWorld](http://bobmeetsworld.com/)
  
* [Subscribe](http://lifeisrisky.com/rss.xml) to [LifeisRisky](http://lifeisrisky.com/)  

  
* [Subscribe](http://www.thekingkongblog.com/rss.xml) to [TheKingKongBlog](http://www.thekingkongblog.com/)
  
* [Subscribe](http://www.mynewhustle.com/rss.xml) to [My New Hustle](http://www.mynewhustle.com/)
  
* [Subscribe](http://www.gadgettastic.com/rss.xml) to [GadgetTastic](http://www.gadgettastic.com/)
  
* [Subscribe](http://www.browie.info/rss.xml) to [Life of Browie](http://www.browie.info/)
  
* [Subscribe](http://www.disregardme.com/rss.xml) to [Disregard Me](http://www.disregardme.com/)
  
* [Subscribe](http://everybodygoto.com/rss.xml) to [Everybody Go To](http://www.everybodygoto.com/)
  
* [Subscribe](http://brianvaughan.com/rss.xml) to [Brian Vaughan](http://brianvaughan.com/)
  
* [Subscribe](http://fuery.com/rss.xml) to [Fuery](http://fuery.com/)
  
* [Subscribe](http://sambreadstone.com/rss.xml) to [Sam Breadstone](http://sambreadstone.com/)
  
* [Subscribe](http://www.blog-blond.blogspot.com/rss.xml) to [Blog-Blond](http://www.blog-blond.blogspot.com/)
  
* [Subscribe](http://www.mamamaui.blogspot.com/rss.xml) to [Who’s Yo Mama?](http://www.mamamaui.blogspot.com/)
  
* [Subscribe](http://www.theartoflivinganddying.blogspot.com/rss.xml) to [The Art of Living and Dying](http://www.theartoflivinanddying.blogspot.com/)
  
  
  
  
**---- fine copia ----**  
  
  
Se partecipi, per favore, fammelo sapere con un commento. Verrò a vedere il tuo blog, ti leggerò, sicuramente mi piacerai e ti aggiungerò alla lista. Sottoscriverò il tuo feed e tornerò poi a leggerti con piacere! (Ma tu scrivi qualcosa di interessante!) (lol)  
  


