Title: Delusione tricolore
Date: 2008-06-07
Category: altro
Slug: delusione-tricolore
Author: Karim N Gorjux
Summary: La sera, prima di andare a dormire, mi leggo questo libro:Tutto ciò che riguarda la mia attività professionistica assomiglia molto al titolo del libro di Stella e Rizzo: "La deriva". Rita si[...]

La sera, prima di andare a dormire, mi leggo questo libro:  
  
[![Immagine di La deriva](http://image.anobii.com/anobi/image_book.php?type=4&item_id=014303a264fa7d81c1&time=1209552846 "Per saperne di più su La deriva")](http://www.anobii.com/books/014303a264fa7d81c1/ "Per saperne di più su La deriva")  
  
  
Tutto ciò che riguarda la mia attività professionistica assomiglia molto al titolo del libro di Stella e Rizzo: "La deriva". Rita si chiede come sia possibile una cosa del genere, tasse, tasse e sempre tasse. C'è sempre qualcuno che chiede soldi e a volte fatichiamo anche a capire per cosa ce li chiedono, sono giunto alla conclusione che vivere onestamente, pagando le tasse, *non s'ha da fare in Italia*. Bisogna essere "furbi", bisogna assolutamente essere italiani in Italia.  
  
  


