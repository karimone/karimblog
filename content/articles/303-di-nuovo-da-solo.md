Title: Di nuovo da solo
Date: 2007-03-14
Category: altro
Slug: di-nuovo-da-solo
Author: Karim N Gorjux
Summary: Domani sarò a Malpensa a consegnare le mie donne alla Lithuanian Airlines, starò per un bel mesetto da solo a pensare ai miei affari e non solo a quelli. Il vantaggio o[...]

Domani sarò a Malpensa a consegnare le mie donne alla Lithuanian Airlines, starò per un bel mesetto da solo a pensare ai miei affari e non solo a quelli. Il vantaggio o svantaggio (dipende dai punti di vista) nell'aver sposato una donna straniera e che la signora deve o cerca di tornare in patria frequentemente dai parenti e dagli amici, secondo me è un grandissimo vantaggio. 

 La **seconda cosa** che voglio fare, in questi giorni di solitudine forzata, è di usare la mia bici di quarta mano parentale con ben 30 anni sul groppone. Le giornate sono belle ed è giusto approfittarne per iniziare una **guerra santa** contro i miei kg di troppo.


 La terza cosa che desidero fare è aggiornare più frequentemente il blog sperando di continuare anche quando non sarò da solo.


