Title: Ultimi giorni a Sarkandaugava
Date: 2004-08-14
Category: Lettonia
Slug: ultimi-giorni-a-sarkandaugava
Author: Karim N Gorjux
Summary: Ebbene sì.. la mia avventura a Sarkandaugava stà per concludersi, se tutto procede come accordato (i lettoni sono poco affidabili), mi sposterò a breve in Old Riga, dove potrò vivere serenamente senza[...]

Ebbene sì.. la mia avventura a Sarkandaugava stà per concludersi, se tutto procede come accordato (i lettoni sono poco affidabili), mi sposterò a breve in Old Riga, dove potrò vivere serenamente senza prendere il bus a notte fonda e potendomi fare da mangiare senza fare i salti mortali. 

 Tre giorni fà è sbarcata la nave San Giusto al porto per la celebrazione dell'ottocentesimo anniversario della marina lettone. Penso che Riga non sia mai invasa da così tanti italiani, purtroppo stiamo facendo una brutta figura, mi è capitato di vedere e sentire riguardo a svariati episodi. Si vocifera in Riga che siano stati picchiati alcuni marinai strafottenti dai Lettoni e personalmente sono contento perchè il comportamento della nostra marina è vergognoso, la maggior parte dei *seamen* sono strafottenti, arroganti e si muovono sempre in branco per darsi forza come se non bastasse la divisa è veramente brutta. Confermo il comportamento della marina italiana per esperienza personale.


