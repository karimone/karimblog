Title: Dopo sei mesi di Italia...
Date: 2011-11-08
Category: Lituania
Slug: dopo-sei-mesi-di-italia
Author: Karim N Gorjux
Summary: Sono ormai sei mesi che vivo nuovamente in Italia.

[![](http://www.karimblog.net/wp-content/uploads/2011/11/italians-gesture-214x300.jpg "italians-gesture")](http://www.karimblog.net/wp-content/uploads/2011/11/italians-gesture.jpg)Sono ormai sei mesi che vivo nuovamente in Italia.


 Mi sono riscontrato di nuovo con tutto: burocrazia, televisione, gerontocrazia...


 E' stato incredibile come** fare 2500km in macchina mi abbia trasformato da vecchio (in Lituania) a giovane (in Italia)** dove per essere credibile in banca devi farti accompagnare dai genitori a 32 anni. Perché ringraziando loro hanno "fatto qualcosa" nei tempi in cui si poteva.


 E' anche il posto delle montagne e del buon cibo. La realtà provinciale, così piccola e così italiana, non ti fa mai sentire solo. C'è sempre qualcuno con cui parlare, qualcuno che ti racconta qualcosa o ti saluta.


 **Dulcis in fundo, non sei più uno straniero**.


 Mi è stato chiesto se mi manca la Lituania. Sinceramente no. Tutto ciò che di buono mi ha dato la Lituania si trova a casa mia con me. Se mi manca qualcosa è forse una partita a calcio con gli "amici" lituani o qualche piatto tipico. Niente di così irraggiungibile, sicuramente avrò un assaggio di tutto ciò la prossima estate.


 Greta ha iniziato ad andare all'asilo, i primi giorni le mancavano gli amici che ha lasciato a Klaipeda, ora è tutto più semplice, si diverte si sente integrata e le maestre sono brave. Mia moglie si lamenta che alla mensa danno continuamente da mangiare la pasta.... **il dolce orgoglio nazionalista delle lituane è intramontabile!** Poco male, chiederò alla mensa di iniziare a servire la pizza ;-)

 Il più grande vantaggio nello stare qui in Italia **è avere la mia famiglia vicina**. Se c'è bisogno di qualcosa (e c'è sempre bisogno di qualcosa!) almeno non sei solo. Quando eravamo a Klaipeda non avevamo nessuno che ci poteva aiutare se non una zia di Rita a cui chiedevamo aiuto raramente. L'alternativa era trasferirsi a Telsiai nel centro della Lituania in mezzo al nulla, altrimenti tornare qui in Italia. Per ora sembra una mossa azzeccata. Anche se per poter sopravvivere qui bisogna essere furbi, furbi all'italiana. Roba di cui non andare fieri.


