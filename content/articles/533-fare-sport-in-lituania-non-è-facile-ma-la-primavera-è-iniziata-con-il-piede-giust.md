Title: Fare sport in Lituania non è facile. Ma la primavera è iniziata con il piede giust
Date: 2009-04-08
Category: Lituania
Slug: fare-sport-in-lituania-non-è-facile-ma-la-primavera-è-iniziata-con-il-piede-giust
Author: Karim N Gorjux
Summary: Ieri era la seconda volta che andavo a giocare a calcio. La prima volta è stata comica, sia io che Mirko ci aspettavamo qualcosa di più "italiano" come una palestra al chiuso,[...]

![Dischi in ghisa](http://www.karimblog.net/wp-content/uploads/2009/04/dischi-ghisa.jpeg "Dischi in ghisa")Ieri era la seconda volta che andavo a giocare a calcio. La prima volta è stata comica, sia io che [Mirko](http://www.mirkobarreca.net) ci aspettavamo qualcosa di più "italiano" come una palestra al chiuso, gli spogliatoi e le docce. Invece era tutto molto alla buona, un po' come quando giocavo da ragazzino: campo all'aperto in mezzo al parco e *"cerca di venire già cambiato perché la doccia te la fai a casa"*.  
  
E' stato bello, faticoso ma bello. La prima partita ha avuto delle conseguenze fisiche devastanti. Gambe distrutte per giorni e torcicollo, mentre la seconda non ha dato problemi di sorta. Mi è piaciuto tantissimo perché mi mancava molto giocare a calcio; non poter dare due calci al pallone enfatizzava ancora di più la lontananza dal mio paese e ha reso l'inverno ancora più difficile da sopportare.


 E' lo sport il problema più grosso negli inverni Lituani. D'inverno rimane solo la palestra e d'estate bisogna trovare le persone giuste con cui praticarlo. A me manca molto la mia palestra, sono dimagrito e ho perso qualche chilo di troppo, ma solo perché mangio di meno e non perché pratico dello sport. La palestra è, secondo me, il posto dove più si nota la differenza tra le persone del sud e del nord d'Europa e questa chiara distinzione viene ancora più marcata dall'assenza di birra e/o droghe che amplificano i già pochi stimoli sociali dei lituani. E' molto difficile trovare l'entusiasmo e la motivazione di andare in palestra, spogliarsi in silenzio, allenarsi in silenzio, fare la doccia in silenzio e tornare a casa in silenzio. E' in quei momenti che mi manca il mio Body Club (ora Body Shape), gli allenamenti da pazzo, le risate, le discussioni del nulla e miei carissimi amici che hanno condiviso centinaia di alzate di pura ghisa con me.


 Non mi faccio prendere dallo sconforto, la primavera penso prometta bene. Oggi per la prima volta un ragazzo lituano mi ha mandato un sms per chiedermi di giocare a basket, purtroppo ho dovuto rifiutare, ma domani pomeriggio andrò a pitturare le uova per Pasqua e poi vado andrò giocare a calcio. Speriamo che il tempo non mi tradisca.


