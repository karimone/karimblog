Title: Riga - Klaipeda
Date: 2005-01-13
Category: Lituania
Slug: riga-klaipeda
Author: Karim N Gorjux
Summary: Vi scrivo direttamente dal bus diretto a Klaipeda, per me sono le 16:43 il mio stupendo, incredibile e meraviglioso iBook segna ancora 5 ore di carica, quindi ho molto tempo per scrivere..[...]

Vi scrivo direttamente dal bus diretto a Klaipeda, per me sono le 16:43 il mio stupendo, incredibile e meraviglioso iBook segna ancora 5 ore di carica, quindi ho molto tempo per scrivere.. :-) Questo messaggio lo vedrete probabilmente domani, appena Davide riattiverà internet nell'appartamento, quindi non illudetevi di leggere mie notizie in diretta.


 Ogni minuto che passa Riga si allontana da me, per l'ennesima volta la grande città dall'energia positiva ha segnato qualcosa importante dentro di me. Ho vissuto nuovamente situazioni che in passato mi avevano profondamente toccato, ma che ora accetto, capisco, riconosco e supero senza soffrire o essere triste. In passato ho creduto di aver capito il perchè e il superare questi momenti mi ha fatto capire che le mie esperienze sono servite.


 Non posso entrare nei particolari perchè sarebbero molto personali, ma voglio solo dire a chi soffre per amore o per qualsiasi altra cosa di non sciupare il momento presente che la vita sta' usando per insegnargli qualcosa. Armarsi di pazienza, non esigere ed essere sinceri con se stessi sono ingredienti necessari per comprendersi e trovare la felicità dentro di sè.


 Non ho avuto quello che vorrei, ma sono contento, perchè qualcosa è cambiato...


