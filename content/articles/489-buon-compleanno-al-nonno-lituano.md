Title: Buon compleanno al nonno (lituano)
Date: 2008-09-21
Category: Lituania
Slug: buon-compleanno-al-nonno-lituano
Author: Karim N Gorjux
Summary: Ieri Vytautas ha compiuto 50 anni, siamo arrivati alle 11 circa a Telsiai, per l'occasione abbiamo persino pulito la macchina. Cielo grigio, 8 gradi stabili: autunno inoltrato.

Ieri Vytautas ha compiuto 50 anni, siamo arrivati alle 11 circa a Telsiai, per l'occasione abbiamo persino pulito la macchina. Cielo grigio, 8 gradi stabili: autunno inoltrato.


  Appena arrivati io e Rita abbiamo mangiato un pranzo velocissimo una classica (per la Lituania) bistecca e riso. Appena finito siamo scappati in paese per andare in alcuni negozi. Il sabato pomeriggio i negozi chiudono a metà pomeriggio, ma penso che sia una prerogativa solo dei piccoli negozi e non dei grandi magazzini che sono sempre aperti. Noi dovevamo comprare le tazzine da caffè che qui in Lituania sono praticamente **introvabili**, i fiori per nonno e fare una visita al negozio di elettronica.


 Telsiai è un paesino piccolo che se avesse strade e case in migliori condizioni, lo si potrebbe paragonare ad [Hill Valley](http://en.wikipedia.org/wiki/Hill_Valley_(Back_to_the_Future) "Hill Valley su Wikipedia"). Il negozio di *cose di casa* aveva un po' di tutto: tazze da thé, pentole, pirofile, mattarelli... guardo qua e là ed ecco una tazzina da caffè! La signora ci vende molto volentieri un set da 6 tazzine a 10 litas che erano in magazzino da mesi se non da anni perché, a sentire lei, è un articolo che non si vende nemmeno se regalato. Il prezzo originale era 43 litas, lo si leggeva da un'etichetta sbiadita appiccicata sopra la confezione. Era barrato a penna. Sotto il 43 un 38 barrato poi un 20 barrato e infine un bel 10. Se tornavo il giorno dopo forse lo trovavo nell'immondizia di fronte.


 Felice del mio acquisto (ora i miei caffè sembrano presi al bar del mio paese) andiamo nella via dei fiorai dove piccoli negozietti rustici sono messi uno di fila all'altro e vendono tutti fiori. Sembra di essere in una paese costruito con i Lego. La composizione floreale che acquistiamo ha dei colori scuri perché in Lituania si regalano i fiori agli uomini, ma devono avere dei colori più scuri. Il bianco è da evitare perché si usa nei cimiteri.


 Torniamo a casa dove una tavola gigante occupa tutto il salone, sono le 16 circa e sul tavolo c'è l'insalata che noi chiamiamo russa, il pesce, il pane, il vino e dei piatti già composti con bistecca, verdure e salse varie. Prima di mangiare ci ritroviamo tutti in piedi attorno al tavolo dove inizia il rito degli auguri. Niente tirate di orecchie o candeline da spegnere, Greta porta i fiori al nonno e avviene la consegna dei regali come se come se fossimo su un palcoscenico di teatro. Strette di mano, baci e abbracci e si inizia a mangiare.


 Non mi abituerò mai ai **non orari **lituani sui pranzi e le cene. Il nostro rito mediterraneo di pranzo e cena a ore prefissate ha conquistato anche Rita che, se non siamo a casa di altri, cerca di mantenere. La giornata è continuata con parenti e vicini che sono venuti a portare gli auguri e/o regali fino alle 21 quando siamo partiti per tornare a Klaipeda.


 E' stato divertente.  
[![Greta e Papi](http://farm4.static.flickr.com/3196/2875219416_ac8cacf857_m.jpg)](http://www.flickr.com/photos/kmen-org/2875219416/ "Greta e Papi di karimblog, su Flickr")



