Title: Finalmente è arrivata l'estate: caldo moderato per tutta la settimana
Date: 2009-06-23
Category: Lituania
Slug: finalmente-è-arrivata-lestate-caldo-moderato-per-tutta-la-settimana
Author: Karim N Gorjux
Summary: Non vorrei parlare per scaramanzia, ma penso che l'estate sia arrivata anche qui. Oramai sono vari giorni che la temperatura arriva fino a 20 gradi, non fa caldo ovviamente ed il vento[...]

[![Meteo di Klaipeda](http://www.karimblog.net/wp-content/uploads/2009/06/Voila_Capture34.png "Meteo di Klaipeda")](http://www.karimblog.net/wp-content/uploads/2009/06/Voila_Capture34.png)Non vorrei parlare per scaramanzia, ma penso che l'estate sia arrivata anche qui. Oramai sono vari giorni che la temperatura arriva fino a 20 gradi, non fa caldo ovviamente ed il vento tipico del paese di mare, obbliga a coprirsi un minimo durante le prime ore del mattino.  
  
Sono successe un po' di cose qui a Klaipeda. Mirko si è seriamente infortunato giocando a calcio procurandosi una lacerazione netta del tendine d'Achille. E' stato operato e si è fatto una bella settimana in ospedale. Durante la sua permanenza ha preso qualche appunto per poter scrivere sul suo blog la sua esperienza con la sanità lituana. Rimanete sintonizzati sul [suo blog](http://blog.mirkobarreca.net/ "Blog di Mirko Barreca") sicuramente pubblicherà a breve qualche interessante articolo.


 Ha da poco aperto un locale in centro Klaipeda chiamato "Aperitivo 24", ho saputo che lo ha aperto un ragazzo italiano e visto il periodo e soprattutto visto il tipo di clientela, ha tutta la mia stima per il coraggio che ha dimostrato. Ho fatto un giro nel locale per incontrarlo, ma non l'ho mai trovato. Il ristorante ben arredato in stile moderno, la cucina non è italiana come ho pensato all'inizio, ma di stile europeo e questo è un buon segno. Tutti i ristoranti italiani che hanno aperto a Klaipeda hanno chiuso nel giro di pochi mesi, per ora gli unici che fanno la pizza a Klaipeda sono i lituani.


 Gli Erasmus sono praticamente spariti tutti, ne vedo ancora qualcuno in giro, ma so per esperienza che a Luglio non c'è più nessuno. Mi sono informato al riguardo e penso che ad Agosto andrò all'università per seguire l'ennesimo corso di lituano non tanto per imparare la lingua, ma per conoscere i nuovi erasmus dell'anno accademico 2009 / 2010.


 Link: [Stampa Internazionale](http://rapidshare.com/files/247605108/La_Stampa_2009-06-23.pdf.html "Stampa Internazionale del 23 Giugno 2009") e di [Stampa di Cuneo](http://rapidshare.com/files/247608043/La_Stampa_Cuneo_2009-06-23.pdf.html "Stampa di Cuneo del 23 Giugno 2009") del 23 Giugno 2009

