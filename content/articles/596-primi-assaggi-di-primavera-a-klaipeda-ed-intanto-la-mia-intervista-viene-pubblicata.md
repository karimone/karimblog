Title: Primi assaggi di primavera a Klaipeda ed intanto la mia intervista viene pubblicata
Date: 2010-03-30
Category: Lituania
Slug: primi-assaggi-di-primavera-a-klaipeda-ed-intanto-la-mia-intervista-viene-pubblicata
Author: Karim N Gorjux
Summary: La mia intervista pubblicata su italiansinfuga.com ha dato i suoi frutti. Facendo qualche modifica, ho spedito l'intervista alla "Fedeltà", un quotidiano molto diffuso nelle zone da cui vengo io. L'intervista, corredata di[...]

[![](http://www.karimblog.net/wp-content/uploads/2010/03/centallo-informa1-300x217.jpg "centallo-informa")](http://www.karimblog.net/wp-content/uploads/2010/03/centallo-informa1.jpg)La [mia intervista](http://www.italiansinfuga.com/2010/01/15/emigrare-in-lituania-da-chi-lha-fatto/ "Emigrare in Lituania") pubblicata su [italiansinfuga.com](http://www.italiansinfuga.com) ha dato i suoi frutti. Facendo qualche modifica, ho spedito l'intervista alla "[Fedeltà](http://www.lafedelta.it "La fedeltà")", un quotidiano molto diffuso nelle zone da cui vengo io. L'intervista, corredata di fotografie, è stata pubblicata sia sulla Fedeltà che su "Centallo Informa", un giornale che viene distribuito a tutti i miei concittadini italiani.


   
L'esperienza è stata piacevole, qualcuno mi ha mandato un sms, altri mi hanno scritto su facebook, domani saprò se c'è stata qualche visita in più sul sito e forse ci sarà anche qualche contatto.


 La situazione a Klaipeda sta finalmente migliorando. **La neve è praticamente quasi tutta scomparsa**, non fa più freddo e stiamo timidamente superando i cinque gradi per entrare nelle decine. C'è il sole, vento e qualche pioggia, ma tirando le somme i sei mesi d'inverno sono alle spalle. Ora si sta bene, si può uscire, godersi le foreste ed il mare. Le giornate sono più lunghe e la voglia di "vivere" aumenta.


 Con l'arrivo della primavera **arrivano anche i turisti** e le persone che mi vogliono incontrare e/o conoscere. La settimana scorsa ho avuto modo di incontrare di nuovo [Andrea Russo](http://andrearusso1979.blogspot.com/ "Blog di Andrea Russo") a cui ho fatto un poco da cicerone per i due giorni che è rimasto a Klaipeda. Domani invece incontrerò Giuliano, altro frequentatore del blog che mi ha contattato e che vuole scambiare due chiacchiere con me. Pranzeremo in centro gustandoci la cucina lituana.


 Lavorativamente ho varie cose in testa, ma una cosa che ho imparato è che **non è possibile concentrarsi su troppe cose**, soprattutto quando si è da soli. Ho in mente di realizzare un progetto che richiede un'abilità che ho curato e allenato negli anni: scrivere. Vedremo come andrà a finire, intanto si continua ancora un po' con le solite cose

