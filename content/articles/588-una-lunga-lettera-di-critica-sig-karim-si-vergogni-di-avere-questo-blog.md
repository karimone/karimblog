Title: Una lunga lettera di critica: Sig. Karim si vergogni di avere questo blog
Date: 2010-02-05
Category: Lituania
Slug: una-lunga-lettera-di-critica-sig-karim-si-vergogni-di-avere-questo-blog
Author: Karim N Gorjux
Summary: Una signora, mi ha scritto una feroce e dura lettera di critica. L'ho letta un paio di volte e mi sento onorato di essere così importante e così valutato tanto da ricevere[...]

[![](http://farm1.static.flickr.com/173/407842334_b2cf2c38a6_m.jpg "Shame")](http://www.flickr.com/photos/robbrucker/407842334/)Una signora, mi ha scritto una feroce e dura lettera di critica. L'ho letta un paio di volte e mi sento onorato di essere così importante e così valutato tanto da ricevere una lettera così lunga. Sinceramente non lo merito.


 Lascio a voi i commenti, che sono sicuro saranno tanti tanti. La letterà e pubblicata così come mi è arrivata, ho solo fatto qualche modifica per migliorarne la leggibilità. Buona lettura!

 Oggetto: **Si richiede moderazione**

 *Gentile Signor Karim, sei Lei rispetta le donne di tutti i paesi a pari diritto, i blog su come conquistare donne lituane, non dovrebbero esistere.*

 *Premesso che una donna colta ed intelligente non ha mai a che fare con i tipi del genere, e' vergognoso da parte Sua ritenersi guru delle relazioni con le donne lituane, come se fossero un oggetto. gentile Signor Karim si dimentica di trovarsi nel paese che sta all’ottavo posto nel mondo, mentre la stessa Italia si ritrova ar 46 vicino ai paesi del terzo mondo per i diritti delle loro donne. (96% delle donne lituane lavoravano nel 2008, e 47 % delle italiane)*

 *Se dalle cinque famiglie lituane in tre la donna guadagna piu dell’uomo, e se nelle istituzioni in Lituania 75% sono dipendenti donne ci sara pure un motivo. “bisogna battere pugno sul tavolo” si rende conto che incita alla violenza? Non ho mai visto in mia famiglia LITUANA, ne da nessuna altra parte o famiglia, ne alzata di voce ne pugni. Anche se in Italia 81% delle violenze e stupri accadono tra le mura domestiche ed e' anche vero che 47% di tutte le italiane hanno subito abusi... (questi dati puo trovare in internet)*

 *Forse, le saranno capitate le persone che hanno vissuto vite traumatiche in Lituania? O e' stato influenzato dalla sua esperienza personale, o magari l'infanzia? Bisogna vedere con che tipo di donne e persone si frequenta per dire certe cose su TUTTE, e si ricordi sempre di non offendere il resto delle donne lituane che certamente non saranno sue amiche. Si ricordi anche che incitare alla violenza e' un reato, e che a certe persone questa frase puo essere di incoraggiamento per aggredire donne lituane. *

 *Personalmente io in Italia per questi atteggiamenti, direi, rurali, (pure aggressioni a parole, che voi chiamate complimenti) ho fatto cinque segnalazioni alle forze dell’ordine, perche nel mio paese Lituania ero abituata essere non disturbata per strada, cioe non offesa con "complimenti", e non ho potuto fare causa solo perche in Italia la legge che tutela maggiormente le donne e’ arrivata nel 2008. Ho sposato un italiano, un uomo meraviglioso, per i suoi valori morali, grande cultura, apertura mentale nei confronti delle donne, dovuta alla sua educazione e la sua famiglia di un certo tipo, naturalmente, quindi apprezzo molto Vostro popolo, ma non mi metterei mai a scrivere blog “come sfuggire dalle bugie all’italiana”, un tema trattato da molte ingenue anche sulle riviste nazionali in Lituania, storie delle ragazze che si sono arse le mani dopo i rapporti con gli italiani che in breve periodo dell'indipendenza sono diventati famosi... che, forse, non dovendo essere persone profonde, avranno cercato certi consigli da qualche parte... *

 *Come fa a stare li a dare consigli? Ha la pala magica? Non riuscirei a credermi superiore e permettermi dare giudizzi o di valutare un popolo, ancora peggio, DONNE di un popolo con tale superficialita’ ed arroganza. Siccome signor Karim, lei non ha 90 anni e non ha passato una vita felicemente sposato con una donna lituana, forse dovrebbe arricchire il suo mondo interiore fatto di presunzione di sapere la verita, dell’ignorare la cultura del posto in cui si trova (guardi matriarcato lituano e tradizioni pagane) e della superficialita nel distribuire i “consigli” che offendono la dignita di qualunque donna nel mondo. *

 *Si rende conto che isola “Donna lituana” come un fenomeno a parte ponendola come un oggetto di conquista e proponendo le techniche disdicevoli? Che cos'e'? caccia di un animale? Sono le techniche dell’inganno? Questa sarebbe una proposta plausibile del perché le donne lituane non si fidano piu degli italiani. Lo hanno detto anche gli italiani che vivono in Lituania ad Emilio Fede nella trasmissione del Sipario. Ci sarebbe da approfondire se in queste affermazioni non ci sono problemi di classismo razziale in quello che Lei dice. Io personalmente leggendo queste cose mi offendo. Ma io sono lituana che lei ha "etichettato" con leggerezza.*

 *Si rende conto che e' un blog maschilista e offensivo per qualunque donna del mondo? Mi pongo un altra domanda: con chi si frequenta per dedurre che le donne lituane hanno bisogno di un pugno sul tavolo o alzata di voce? Che bassezze... Ci sono tante donne che queste cose non hanno mai visto e vanno subito dai carabinieri. Io ho provato a farlo in italia, ma non avete le stesse leggi ancora riguardo a tutela delle donne... Quindi posso capire la sua cultura.*

 *Forse non ha conosciuto direttore delle pari opportunita, forse non vede chi governa Lituania oggi (essendo occupato a scrivere sul blog...), non si rende conto della realta in cui vive? Faccia allora un giornalista serio, presenti due facce della medaglia, magari quella faccia dove povere lituane ingenue di campagna devono evitare inganni e gravidanze indesiderate dai italiani, che si presentano single e in italia hanno una famiglia?*

 *Vivo in italia 9 anni e ne ho sentito un po di storie di vite distrutte delle ragazze, che ingenuamente hanno creduto alle assurdita di certi casanova. So di una lituana che e' stata segregata in italia per gelosia a casa di un napoletano. La obbligava a pulire casa ed impediva di uscire e mia amica l'ha vista con un livido in faccia. Lei laureata, lui no e gia questo mi farebbe pensare... era uno che ci sapeva fare un po di piu a prima vista di un semplice sincero maschio lituano, uno come mio padre per esempio che ha passato tutta la vita con mia madre felici e contenti..*

 *Perche non consiglia alle ragazze lituane di controllare tutti i documenti per iscritto, carta d'identita compresa prima di dire il proprio nome. So bene anche delle cause alle donne benestanti lituane per estrarre del danaro con ricatto dei figli. Ho visto le prove in persona.*

 *Si vergogni di avere questo blog.*

