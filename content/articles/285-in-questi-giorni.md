Title: In questi giorni
Date: 2007-01-17
Category: Lituania
Slug: in-questi-giorni
Author: Karim N Gorjux
Summary: Sono parecchio occupato, il 24 ho una consegna importante: il business plan della mia avventura imprenditoriale. Il 31 saprò se la commissione della provincia promuoverà il mio business plan e se da[...]

Sono parecchio occupato, il 24 ho una consegna importante: il business plan della mia avventura imprenditoriale. Il 31 saprò se la commissione della provincia promuoverà il mio business plan e **se** da Febbraio sarò ufficialmente imprenditore. Per ora posso solo dirvi questo.


 Segnalo alcune cose interessanti. Si è riaccesa la discussione sulle [donne lituane](http://www.karimblog.net/2006/10/02/le-donne-lituane-qualcosa-di-buono-ce), commentate che poi dirò la mia in un bel post. [Luca](http://www.pandemia.info/2007/01/13/tremila_post_e_la_divulgazione.html) e [Daniela](http://www.danielaforconi.net/2007/01/12/5-cose-che-non-sapete-di-mecredo/) mi hanno proposto due articoli, il primo riguardo ai blog; Il secondo riguardo a me. Ho intenzione di scrivere nel week-end e di pubblicare ad inizio settimana.


 Last but not least, il [blog di Gaetano](http://blog.magogae.net/) racconta il suo viaggio a Belgrado in "diretta". Ovviamente non è andato per musei, ma per seguire le orme di [Carlo](http://www.seduction.net) (mio amico).


