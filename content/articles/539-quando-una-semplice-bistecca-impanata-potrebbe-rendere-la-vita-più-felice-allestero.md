Title: Quando una semplice bistecca impanata potrebbe rendere la vita più felice all'estero
Date: 2009-05-06
Category: Lituania
Slug: quando-una-semplice-bistecca-impanata-potrebbe-rendere-la-vita-più-felice-allestero
Author: Karim N Gorjux
Summary: Mangiare una buona bistecca è un evento a cui partecipo generalmente quando vado a mangiare fuori in qualche locale. Fino ad ora non sono ancora riuscito a farmi in casa una buona[...]

[![](http://farm3.static.flickr.com/2067/2475617667_352f276670_m.jpg "Bistecca Impanata")](http://www.flickr.com/photos/panciapiena/2475617667/)  
Mangiare una buona bistecca è un evento a cui partecipo generalmente quando vado a mangiare fuori in qualche locale. Fino ad ora non sono ancora riuscito a farmi in casa una buona bistecca alla milanese e questo mi rattrista non poco. Se per caso vivi o hai vissuto in Lituania e hai qualche dritte su come italianizzare al meglio il mio "secondo" a pranzo. [Contattami!](http://www.karimblog.net/contattami/) Te ne sarei davvero grato.  
  
Nei supermarket puoi comprare il pollo già cucinato, non è come il nostro pollo allo spiedo, ma se la cava bene e ogni tanto mi concedo il vizio, ma proprio raramente. Oltre alla carne ci sono svariate insalate che noi volgari non esperti in materia chiamiamo "insalata russa", in realtà più che insalate sono esperimenti di statistica. **Quanti tipi di insalate riusciremo a creare con la roba che sta per scadere?**

 I due italiani che vivono a Klaipeda si pongono questa domanda, non perché non si fidano, ma semplicemente perché sono italiani. Come è possibile che "l'insalata russa" abbia così tanta maionese da non riuscire nemmeno a vedere il colore degli ingredienti? Un poco di malizia ci vuole.


 Allo stesso modo, i soliti due italiani che vivono a Klaipeda, si chiedono il perché le carni marinate in vendita nei supermarket hanno quantità spropositate di aglio e il colorito di Hulk. Eppure o si mangia questo o ci si arrangia in qualche modo e, non esistendo il macellaio, al bancone della carne nessuno si sognerà mai di tagliarti le tanto amate bistecchine. 

 Quindi, a parte rari casi, dopo 9 mesi di residenza continua in Lituania, ancora niente prosciutto e niente bistecchina alla milanese.   
Un giorno Rita si è avventurata al [mercato in centro](http://maps.google.it/maps?f=q&source=s_q&hl=it&geocode=&q=turgaus+gatve,+klaipeda&sll=41.442726,12.392578&sspn=22.760402,28.476563&ie=UTF8&t=h&z=16&iwloc=A) e armata di pazienza e tanto coraggio è riuscita a farsi fare delle fettine da un tal "macellaio". Impanate e cucinate, sono stato costretto a frullarle e farci un ragù perché impossibili da masticare, erano più dure dello stesso tavolo dove stavamo mangiando. Curiosamente il ragù aveva lo stesso sapore del ragù barilla che si trova negli scaffali dei supermarket di Klaipeda.


 Se riesco a risolvere questo problema culinario, il mio soggiorno in Lituania sarà ancora più piacevole, quindi se hai qualche consiglio, anche piccolo, commenta questo articolo o [scrivimi](http://www.karimblog.net/contattami/).


 PS: Dei due italiani che vivono a Klaipeda ovviamente uno è il sottoscritto, [l'altro](http://www.mirkobarreca.net) è un genovese con istinti fotografici.


