Title: Ma alla fine, questi italiani, sono puttanieri o no?
Date: 2008-12-15
Category: Lituania
Slug: ma-alla-fine-questi-italiani-sono-puttanieri-o-no
Author: Karim N Gorjux
Summary: A volte mi capita di sentire parlare italiano, in giro per Klaipeda, mi guardano in faccia perché sanno che sono italiano e fanno finta di non vedermi. Da parte mia anche io[...]

A volte mi capita di sentire parlare italiano, in giro per Klaipeda, mi guardano in faccia perché sanno che sono italiano e fanno finta di non vedermi. Da parte mia anche io non sono da meno, è già capitato che io e Mirko, in giro per l'akropolis, sentissimo parlare italiano vicino a noi. L'automatismo del "stai zitto e non farti riconoscere" scatta automaticamente.


 Quando parlo con un italiano che viene da queste parti, casualmente, a sentire lui, è l'unico "non puttaniere". Non è venuto qui per le bionde dagli occhi azzurri, ma sta girovagando per i Baltici alla scoperta di musei dell'ambra e paesaggi naturalistici. Sempre senza scomodare la probabilità e statistica, il discorso finisce sui migliori locali per incontrare ragazze poco vestite.


 Ma da cosa si riconoscono sti italiani puttanieri? Qualche caratteristica comune l'ho già notata:  
  
 3. Viaggiano e **vivono in branco**: non ne vedrete mai uno da solo girare da puttaniere e se lo vedete da solo e perché siete al locale dove si balla la lap dance.

  
 6. **Ostentano**: gli italiani da queste parti sono come gli italiani in tutto il resto del mondo. Ostentano ricchezza presunta in modo da sentirsi più forti sminuendo te che ascolti, ma anche loro sanno benissimo che sono in un paese straniero e per fortuna solo per 10 giorni. (E una parte di loro non vede l'ora di ritornare)
  
 9. **Urlano**: lo ammetto, anche io agli inizi tendevo a "urlare" nei locali. Purtroppo è una caratteristica tipia dello spagnolo e dell'italiano. Ora va un po' meglio e mi faccio riconoscere un po' meno, ma adesso sentire gli altri italiani che urlano impressiona anche me
  
 12. Sono **monotematici**: parlano solo di sesso e di donne. E' il loro unico argomento e generalmente salta fuori molto presto in una discussione. "Hai visto che fighe che ci sono qui? Ma a dirti la verità, sono stato a Riga e ne ho viste di migliori..."
  
 15. Si **vantano**: sapranno raccontarvi delle loro avventure nei più minimi particolari, le prestazioni, i locali, le conquiste. Forse sono fantasie, forse sono vere, ma sinceramente io non mi sentirei a mio agio raccontando la mia vita al primo che incontro.

  
 18. Avranno sempre visto più posti e donne di voi. E soprattutto conosceranno ambasciatori di ogni paese di cui voi non sapevate nemmeno l'esistenza.

  
  
Si, è incredibile quanto siano incredibili sti italiani, ma fortunatamente ho [tre cittadinanze](http://www.karimblog.net/2006/08/05/tre-cittadinanze/).


