Title: Di nuovo a Telsai
Date: 2006-05-07
Category: Lituania
Slug: di-nuovo-a-telsai
Author: Karim N Gorjux
Summary: Ieri ero a Telsai, sono andato con Rita in treno alle 6 del mattino. Il programma della giornata comprendeva: acquisto del tavolino per il sottoscritto, visita al lago della città e sabato[...]

Ieri ero a Telsai, sono andato con Rita in treno alle 6 del mattino. Il programma della giornata comprendeva: acquisto del tavolino per il sottoscritto, visita al lago della città e sabato sera a Kartena al compleanno di amici di Rita.


  Durante la mattinata, ho visitato alcuni negozi di mobili di Telsai. I prezzi sono bassi, dato che i mobili sono fatti in Lituania e mi sono comprato un bel tavolino ad un prezzo "amichevole" che ho montato appena arrivato a casa. Ci voleva, finalmente ho un angolo tutto mio.  
  
[![angolo-mio.jpg](http://www.karimblog.net/wp-content/uploads/2006/05/angolo-mio-tm.jpg "angolo-mio.jpg")](http://www.karimblog.net/wp-content/uploads/2006/05/angolo-mio.jpg)  
  
A me Telsai piace davvero tanto, ieri c'era il sole e faceva pure caldo, giravo in maniche corte e avrei voluto avere i pantaloncini al posto dei jeans. Purtroppo le foto che ho fatto erano in controluce e quindi devo "aggiustare a mano" tutti gli scatti.  
  
[![lago-telsai.jpg](http://www.karimblog.net/wp-content/uploads/2006/05/lago-telsai-tm.jpg "lago-telsai.jpg")](http://www.karimblog.net/wp-content/uploads/2006/05/lago-telsai.jpg)  
  
Kartenà si trova ad appena 20 minuti di macchina da Telsai sulla strada per Klaipeda. Kartenà è un piccolo paesino di 2000 anime sparse tra campagna e foresta, non esistono condomini, ma solo vecchie case di legno, c'è un solo bar e c'è un solo negozio di alimentari. Motto del luogo: *"Che palle!"*  
Il ritrovo è alle 20:00 davanti al negozio di alimentari, il motivo è semplice, ognuno si compra il bere che preferisce. A Kartenà sembra tutto vecchio e ad un passo dalla demolizione; le strade sono piene di buche, le case sono vecchie, il negozio di alimentari non è nient'altro che una vecchia casa di legno che si affaccia su una vecchia piazza polverosa di fronte alla strada principale. Kartenà è la versione moderna delle città di Sergio Leone, una sola strada che taglia in due il paese.


 Carichiamo donne e vivere e siamo pronti, tutti in sella, si parte. Destinazione: una delle tante vecchie case di Kartenà; Si perché questi ragazzi hanno una concezione molto diversa del compleanno che abbiamo noi. Nessun regalo al festeggiato, ma si sono messi dei soldi per affittare una tipica casetta del nord d'Europa attrezzata di sauna e posti letto. La serata è stata divertente, da unico straniero, devo dire di essermi sentito perfettamente a mio agio.  
  
[![svyturis.jpg](http://www.karimblog.net/wp-content/uploads/2006/05/svyturis-tm.jpg "svyturis.jpg")](http://www.karimblog.net/wp-content/uploads/2006/05/svyturis.jpg)  


 Le foto le potete vedere su [gallery](http://gallery.karimblog.net) e su [flickr](http://photos.karimblog.net).  
  
  
 technorati tags start tags: [compleanno](http://www.technorati.com/tag/compleanno), [lago](http://www.technorati.com/tag/lago), [kartena](http://www.technorati.com/tag/kartena), [sauna](http://www.technorati.com/tag/sauna), [spam](http://www.technorati.com/tag/spam), [telsai](http://www.technorati.com/tag/telsai)

 technorati tags end 

