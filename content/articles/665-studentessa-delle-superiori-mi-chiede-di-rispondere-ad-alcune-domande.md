Title: Studentessa delle superiori mi chiede di rispondere ad alcune domande
Date: 2015-06-01
Category: Australia
Slug: studentessa-delle-superiori-mi-chiede-di-rispondere-ad-alcune-domande
Author: Karim N Gorjux
Summary: Una studentessa delle superiori mi scrive in privato chiedendomi se posso rispondere ad alcune domande per una ricerca che sta facendo a scuola. Dato che di ispirazione ne ho poca in questo[...]

Una studentessa delle superiori mi scrive in privato chiedendomi se posso rispondere ad alcune domande per una ricerca che sta facendo a scuola. Dato che di ispirazione ne ho poca in questo periodo ecco che ne approfitto per rendere pubbliche le risposte.  


 **Quali sono state le tue prime esperienze di lavoro o studio all'estero?**

 La mia esperienza da migratore sono iniziate relativamente tardi. Per casualità ho iniziato a frequentare i paesi baltici nel 2003 quandi Lettonia, Lituania ed Estonia non erano ancora parte dell'Unione Europea. Avevo 22 anni.


 I miei primi viaggi sono da turista, sono anche l'occasione per aprire i propri orizzonti culturali. In passato ho viaggiato, ma per lo più con i parenti quindi i miei viaggi sono l'opprtunità per migliorare il mio inglese e per conoscere una realtà diversa che fino ad allora era rinchiusa tra quattro mura che possiamo chiamare "provincia granda"

 Nel 2005 un mio carissimo amico inizia la sua avventura con il programma Erasmus e mi chiede di andarlo a trovare a Klaipeda, in Lituania. Li, conoscerò quella che diventerà mia moglie e Klaipeda, oltre ad diventare la città natia dei miei figli, diventerà la mia città per circa 6 anni.


 In Lituania, vista la situazione economica non delle migliori, mi arrangio lavorando in proprio come sviluppatore e consulente IT online. Con l'avvento della crisi mi trasferisco in Italia con la famiglia, ma una volta che si è contagiato dal "virus migratore", la provincia granda risulta molto meno granda

 Nel 2014, dopo una visita a Stoccolma, prendo l'aereo per l'Australia, destinazione Melbourne. Attualmente lavoro per una startup australiana, ma mi trovo in Lituania dove la mia famiglia è in attesa dei visti per l'ingresso nel continente più antico della terra.


 nella tua scelta ha contato di più il desiderio di avventura e di scoperta di luoghi culture diverse o la necessità di realizzazione personale, intesa come formazione, acquisizione di competenze e collocazione lavorativa?

 Tutto è iniziato per "gioco", ma una volta che le cose sono diventate serie, emigrare è diventata per me un'esigenza. Vedere paesi come la Lituania che sono oggettivamente molto più poveri dell'Italia, ma che sono certi punti di vista molto più avanzati, ti fa capire che l'Italia è un paese che terrai nel cuore per ciò che di bello ti ha dato, ma che non è in grado di darti stabilità.


 In Australia la meritocrazia la fa da padrone, sei ti impegni e dimostri di essere capace vieni premiato e non ha nessuna importanza se sei "figlio di papà"

 Quando invece ho iniziato a fare i miei primi viaggi nei paesi baltici, il mio era più un desiderio di avventura, divertimento, festa. A poco più di 20 anni non avrei mai immaginato che quello sarebbe diventato l'inizio della mia avventura in giro per il mondo.


 **Quali ostacoli hai incontrato?  
**  
In inglese viene chiamato "culture shock". Esiste persino una collana di libri che ti aiuta a superare il culture shock quando emigri in un altro paese. Questa è la parte più difficile, ovvero capire profondamente che tu non sei quello "giusto" che importa in un altro paese il corretto modo di vivere, al massimo tu hai un punto di vista soggettivo, influenzato dalla tua cultura di origine, su come bisogna vivere la vita, ma non è l'unico modo giusto di vivere la vita. Quindi il paese che ti ospita non ha un modo "sbagliato" di vivere la vita, ma è semplicemente diverso. (o strano, da cui deriva la parola straniero)

 Per me all'inizio è stato accettare la cultura diversa. Una volta superato questo ostacolo tutto è diventato molto più semplice.


 Un'altra difficoltà è la lingua. Sapere l'inglese aiuta all'inizio ed è d'obbligo sapere l'inglese, ma se la lingua del paese non è l'inglese, ad un certo punto bisogna mettersi in testa che bisogna imparare la lingua locale. La lingua del posto è il ponte verso la cultura del paese che ti ospita, è impossibile far parte di una comunità se non parli la lingua locale.


 **Ne è valsa la pena?  
**  
Assolutamente si. Diventare cittadino del mondo è il più bel regalo che puoi farti. In questi anni ho avuto modo di viaggiare l'Europa, visitare molti paesi diversi e soprattuto conoscere molte persone diverse. L'apertura mentale che deriva da queste esperienze è impagabile. Viaggiare è un modo di confrontare se stessi, il mondo è davvero grande e ci sono un sacco di persone interessanti la fuori.


 Pensa che in Lituania ho conosciuto uno spagnolo che è partito dalla Spagna in bicicletta e sta andando in Australia (con la moglie). Sono persone ed esperienze che sarebbe impossibile vivere rimanendo dove si è nati.


 Per farti un'esempio, ti sto scrivendo da un paesino nel cuore della Lituania, fuori dalla finestra vedo il lago e giusto qualche ora fa ero in collegamento con Melbourne dato che ho avuto il meeting settimanale con i miei colleghi.


 **Quanto hanno inciso le tue esperienze all'estero sulla tua vita in generale?  
**  
A me l'esperienza all'estero ha completamente sconvolto il modo di vivere e di pensare. Essere a stretto contatto con culture diverse ti mette in una posizione in cui tutto è messo in dubbio, persino ciò che credevi fosse un punto cardine della tua vita.


 **Come immagini la tua vita oggi se non avessi avuto l'opportunità di uscire dai confini nazionali?  
**  
Limitata. Se non intraprendi un viaggio, sarai limitato. Non parlo solo lavorativamente. Io ho due figli di 9 e 4 anni. Entrambi sono bilingue, entrambi hanno vissuto in Italia ed in Lituania ed ora inizieranno la loro avventura in Australia. Immagina cosa significa per loro queste esperienze, come sarà diverso il loro modo di pensare rispetto a bambini che non si sono mai allontanati dal paese in cui sono nati.  

> "*Il vero viaggio di scoperta non consiste nel cercare nuove terre, ma nell’avere nuovi occhi.*"

 -- Marcel Proust

