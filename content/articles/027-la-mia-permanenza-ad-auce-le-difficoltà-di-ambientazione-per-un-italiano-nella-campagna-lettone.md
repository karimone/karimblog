Title: La mia permanenza ad Auce, le difficoltà di ambientazione per un Italiano nella campagna Lettone
Date: 2005-01-01
Category: Lettonia
Slug: la-mia-permanenza-ad-auce-le-difficoltà-di-ambientazione-per-un-italiano-nella-campagna-lettone
Author: Karim N Gorjux
Summary: Non sono più riuscito a collegarmi, mi sono completamente fregato i soldi della tim che mi permetteva internet in roaming e... ole! Fuori dal mondo. Sostanzialmente è una settimana che stò male,[...]

Non sono più riuscito a collegarmi, mi sono completamente fregato i soldi della tim che mi permetteva internet in roaming e... ole! Fuori dal mondo. Sostanzialmente è una settimana che stò male, all'inizio credevo fosse il freddo, poi la cosa ha cominciato a puzzarmi un pò, sono stato male sia quest'estate ad Auce in 2 giorni di permanenza che ora in una settimana di "villeggiatura". Alla fine ho capito: "**gli acari**"! Maledetti e stronzi, mi hanno fregato, in quella casa sono dappertutto: tappeti, cuscini, coperte... la mia allergia ha potuto dare il meglio di sè.


 Mi fà parecchio piacere aver ricevuto alcuni commenti, ringrazio di cuore perchè almeno non mi sento così abbandonato in questa terra dimenticata da Dio. Tornando al padre di Kristina... all'inizio mi incuteva persino timore, è il doppio (o il triplo) di me, per fare il giro della cassa toracica dovevo prendere la macchina o diventavo vecchio, trangugiava birra come se fosse acqua e zucchero... pero' quando rideva e mi parlava in inglese si capiva che era un tozzo di pane. Parlo al passato perchè la sera del 30 dicembre l'ho accompagnato all'aeroporto con la sua famiglia, ora si trova in Irlanda dove lavorerà per parecchi mesi su una nave mercantile. Aleksander guadagna all'incirca 3000€ al mese (in Lettonia sono una somma molto consistente) e gli permete di lasciare la moglie casa a fare la casalinga e inoltre a pagare gli studi ai figli. Per sè spende ben poco, circa 200€ tra pub e vodka, tutto questo ben di Dio gli costa all'incirca 300 giorni all'anno lontano dalla famiglia. Quest'uomo ha tutto il mio rispetto. :-)

 Tornando alla vita Lettone di periferia, ho capito perchè in questo paese i bambini sono tutti dei geni musicali, non c'è un'emerito "ci a due zeta o" da fare per tutto il giorno! Pochi hanno il lusso della macchina, se hanno il lusso della macchina è ancora più difficile avere la patente quindi generalmente chi si muove in macchina è il padre di famiglia o i figli per lavoro, ma raramente si muovono per svago. Nei paesini c'e' ben poco e d'in verno le strade sono ghiacciate, l'illuminazione pubblica è scarsa e i ritrovi sono pochi. Ho assistito a due concerti "casarecci" di cui uno di bambini. La sorella di Kristina suona il violincello, il piano e canta. Durante la "stagione delle scuole" i ragazzi tornano a casa per il pranzo, mangiano e vanno a scuola di musica. Tornano la sera, stanno in famiglia, guardano la tv, mangiano e via a nanna. Tutto qui. Altri svaghi? Beh, i laghetti d'inverno sono ghiacciati e qualcuno ci pattina sopra. Altri si sono costruiti le saune in casa, altri trombano. In fin dei conti c'e' ben poco da fare, ti metti a bere... 

 Le case sono praticamente artigianali in tutto e per tutto, dalle foto si nota qualcosa, ma ho evitato di fotografare i particolari. Non mi lamento (se non fosse per la salute) in fin dei conti fino ad ora ho speso esattamente 5€ :-) Quindi **devo essere contento**.  
Per concludere alcune cose degne di nota: ho notato che la famiglia di Kristina non ha mai litigato, sono sempre rimasto a casa e non è mai successo nulla tra fratelli e/o genitori. Episodio curioso: mi trovavo nel cortile nell'appartamento a Riga, quando ad un certo punto Aleksander si è affacciato alla finestra, si è messo a gridare ed è corso in cortile verso la macchina. C'era un tizio che stava ciucciando con la gomma la benzina dalla sua macchina! Io sono scoppiato a ridere, per loro è una cosa normale.


 Sono stufo di scrivere, non posso raccontare tutto ora.


