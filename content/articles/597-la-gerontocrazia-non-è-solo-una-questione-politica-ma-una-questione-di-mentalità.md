Title: La gerontocrazia non è solo una questione politica, ma una questione di mentalità
Date: 2010-03-31
Category: altro
Slug: la-gerontocrazia-non-è-solo-una-questione-politica-ma-una-questione-di-mentalità
Author: Karim N Gorjux
Summary: Oggi ho conosciuto una persona deliziosa, gentile e di cultura che ho avuto modo di contattare grazie a questo. Abbiamo mangiato insieme è ho dovuto subire i suoi elogi il mio blog[...]

[![Dinosauri Politici](http://www.karimblog.net/wp-content/uploads/2010/03/gerontocrazia_politica-300x223.jpg "gerontocrazia_politica")](http://www.karimblog.net/wp-content/uploads/2010/03/gerontocrazia_politica.jpeg)Oggi ho conosciuto una persona deliziosa, gentile e di cultura che ho avuto modo di contattare grazie a questo. Abbiamo mangiato insieme è ho dovuto subire i suoi elogi il mio blog nelle cose che scrivo, come le affronto e le conclusioni che traggo. Non mi piace auto elogiarmi, ma non posso eccedere di modestia o di presunzione perché in entrambi i casi sarei per bugiardo. La persona che ho conosciuto ha 51 anni ovvero la stessa età di mia madre.


 Ultimamente le persone che mi conoscono da anni e non dal blog, mi chiedono c**ome mai io viva così distante dalla mia famiglia** e perché, invece di stare qui in Lituania a scervellarmi su come fare del denaro, non torno invece in Italia per dedicarmi alle attività messe in piedi da mia madre che detto per inciso comprendono un campeggio, un hotel, un ristorante ed una baita in montagna.  
Sono sicuro che starai pensando che **solo un coglione non approfitterebbe di un'occasione del genere**. Io purtroppo, o per fortuna, dipende dai punti di vista, non sono coglione e non ne approfitto perché mia madre e il suo compagno (che ci tengo a precisare **non** abbbiamo nulla da spartire se non la lontana discendenza dalle scimmie) si cullano in uno dei più grandi mali della cultura italiana: la gerontocrazia.


 **La gerontocrazia è un male micidiale** perché non lascia spazio alla meritocrazia che, come diceva Enzo Biagi, in Italia si può trovare solo più nel calcio. Di conseguenza in passato mi sono ritrovato a lavorare con mia madre e il suo (discutibile) compagno di vita trattato come un sottoposto. Ho sempre avuto idee e nuove concezioni, parto delle mie letture e dei miei studi, da applicare alle attività, ma il risultato è stata sempre una bocciatura "ex ante", prima ancora di poter valutare se le mie idee fossero valide o meno.


 Non è mai successo che una volta mi venisse detto: "L'idea non è male, ci penso un poco e poi ti dico.", niente da fare, **decine di idee sono stato sempre bocciate a priori** andando contro ogni statistica. Per quanto sia scemo penso che su decine di idee forse, almeno una, avrei dovuto azzeccarla. Nulla, niente da fare, la base del pensiero che smonta qualsiasi mia idea è questa: "come fa mio figlio, 21 anni meno di me e che io ho creato e cresciuto, a poter essere meglio di me? Semplice, è impossibile".


 Purtroppo questo modo di pensare è aggravato da lacune culturali e da una gestione famigliare di stampo patriarcale e quindi il dialogo è pressochè nullo. Empaticamente capisco la situazione, la generazione dei miei genitori è la prima nella storia dell'umanità che **è riuscita a prendere calci nel culo dai propri genitori e successivamente dai figli**, ma io di questo non ho nessuna colpa. Non è colpa mia se l'accesso all'informazione è molto più facile ed immediato rispetto a 30 anni fa. E soprattutto non ho nessuna colpa se i miei genitori hanno avuto **la presunzione di essere sempre a passo con i tempi**.


 L'unico modo per mia madre di difendere il proprio status è fare leva sull'esperienza, sulla mole di lavoro e sui risultati. Il paragone tra me è lei è uno di quei paragoni semplicistici e di comodo di cui molti genitori si avvalgono quando vogliono avere la meglio: "*Quando avevo la tua età... ai miei tempi...*" chi non se lo è sentito dire? Persino mia madre l'avrà sentito dire da mia nonna quando era giovane, ma paragonare me a mia madre è un po' come paragonare l'Italia e la Lituania senza tenere conto della storia, della popolazione e delle risorse dei due paesi. Inoltre se non mi viene mai data una possibilità, non potrò mai dimostrare nulla e quindi è sempre facile poter giudicarmi a priori, prima ancora di poter agire e di dimostrare.


 Un mio amico consulente di quasi 40 anni si è trovato a discutere di affari con una persona che, forte dei suoi quasi 60 anni, gli dava del giovanotto e quindi dell'inesperto. Ma dico, la meritocrazia dove la mettiamo? **Come è possibile giudicare una persona solo dalla sua età?** Ogni storia è un caso a sé! Mozart ha iniziato a comporre a cinque anni, Steve Jobs ha creato la Apple e aveva meno di 30 anni, Gesù Cristo è morto in croce a 33; si può quindi usare l'età come valore degno di considerazione per valutare una persona e soprattutto le sue idee? In effetti **gli uomini si scelgono amanti novantenni e non le chiamano "vecchie", ma "esperte"**.


 In questi giorni ho avuto da discutere, non entro nei dettagli perché non mi piace parlare delle mie cose pubblicamente, ma all'ennesima richiesta di entrare nelle aziende di famiglia come "gestore", ho dettato delle condizioni precise che garantissero autonomia e libertà di poter creare e migliorare l'attività secondo le mie idee e permettondomi di esprimere anche sbagliando. Ovviamente, invece di ricevere un semplice "no", mi è stato svelato il vero proposito della richiesta che era di essere "assunto" come "sottoposto" naturalmente "sottopagato" ed inoltre **il rifiuto veniva argomentato dalle possibilità, sempre ex ante**, di miei ipotetici futuri e catastrofici fallimenti e rovine apocalittiche per la mia famiglia.


 Anche mia sorella è scappata via, vive a Ginevra e a sentire lei preferisce pane e cipolla sotto un ponte che lavorare nelle aziende dei miei genitori. Io concordo, **ma come dalla merda nascono i fiori penso che anche da questa amara esperienza di vita si possa trarre un insegnamento**. La prima lezione che ho tratto da tutto ciò è che da genitore è importante saper ascoltare i propri figli, puoi dimostrare l'amore materialmente come fanno tanti genitori indaffarati e con poco tempo, ma solo dei genitori che tengono davvero ai propri figli si siedono e dedicano l'attenzione a sentire cosa dicono.


 Vista la mia esperienza negativa ho agito di conseguenza con mia figlia Greta, mentre giochiamo parliamo molto, ci raccontiamo tante cose strane e mi piace tantissimo sentire i suoi discorsi e i suoi pensieri. Oltre a farmi un sacco di risate a sentire le cose buffe che dice mi diverto a farle delle domande per poi notare con il tempo i suoi miglioramenti nel parlare ed esprimere i pensieri.


 Inoltre ho imparato che i figli sono persone autonome con il proprio carattere, i propri pensieri e le proprie idee. Io non sono mai stato considerato da mia madre e dal suo compagno da passeggio una persona capace di esprimere concetti propri che possano essere qualitativamente migliori dei loro. Anzi, la concezione di figlio è materialistica, di possesso. La verità è che i figli vengono si attraverso i genitori, ma non gli appartengono. Il fatto che mi trovi a 2500km dal mio paese, ne è la prova concreta.


 Concludo perché sono andato ben oltre la tua sopportazione di leggere, ma ci terrei ad avere il tuo parere. Anche tu hai avuto, o hai un'esperienza dove le ali sono tarpate da una gerontocrazia diffusa, non solo nelle famiglie, ma in moltissimi ambiti della vita italiana? Se sei genitore, anche tu sei seduto sulla poltrona dell'anzianità e **ti prendi il diritto di avere ragione solo perché al compleanno spendi più soldi in candeline che di torta?**

 Link: [Lettera di Silvana Mura sulla proposta di legge: "Largo ai giovani in politica](http://www.antoniodipietro.com/2007/07/gerontocrazia_politica.html "Giovani in parlamento")"  
  
Link: [Antonio Albanese "Ivo Perego" e suo figlio](http://www.youtube.com/watch?v=7GYRrFJly5Q "Video di Ivo Perego")

