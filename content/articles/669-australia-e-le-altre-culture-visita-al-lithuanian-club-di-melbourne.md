Title: Australia e le altre culture: visita al Lithuanian Club di Melbourne
Date: 2016-05-12
Category: Australia
Slug: australia-e-le-altre-culture-visita-al-lithuanian-club-di-melbourne
Author: Karim N Gorjux
Summary: 

[![Lithuanian Club Melbourne]({static}/images/lithuanian_club.jpg)](http://www.lithuanianclub.com/)

 *Nota: questo articolo parla di integrazione culturale in Australia con riferimenti alle mie esperienze personali.*

 Trasferirsi in Australia e' considerato un "big shock", vero che si sta meglio che il sole ed il caldo rendono le giornate piu' piacevoli, ma e' sempre un paese lontano 20.000km da casa.


 Mentre per un italiano, basta entrare in un bar per trovare un connazionale, per realta' culturali minori come quella lituana e' un po' piu' difficile. A Melbourne esiste il Lithuanian club al 44 Errol Street in North Melbourne. Ovviamente e' stata una delle nostre prime mete appena arrivata Rita con i bambini.  


 Il club e' parecchio grande anche se si vede che e' molto vecchio. Un grande corridoio addobbato da vecchie foto dei primi migranti lituani, introduce a due sale enormi: sulla sinistra la sala da pranzo con tavolate, bar, televisione. Sulla destra la sala per i ricevimenti, balli e altre attivita'.


 Il pranzo lituano inizia alle 13, o come scrivono qui, alle 01:00am. Siamo tra i primi ad arrivare e, dato che e' Domenica, il posto per parcheggiare c'e' ed e' anche gratuito cosa che a Melbourne ti da la stessa gioia di una vittoria al gratta e vinci.


 La sala da pranzo e' molto grande e piano piano arrivano altre persone, che si parlano in un misto di lituano ed inglese. Molti sono gli anziani e c'e' anche qualche giovane tra i 20 e i 30 anni e qualche bambino.


 E' bello vedere, tra i quadri appesi, foto di paesi in Lituania dove sono stato. Klaipeda, Kaunas, Vilnius e anche altre citta' minori. Li dentro, avvolto dalla cultura lituana a ventimila km dal mar Baltico, mi rendo conto di quanto possa essere importante un posto del genere per Rita e tutti i lituani li' seduti che si scambiano saluti sorseggiando una [Svyturis](https://en.wikipedia.org/wiki/%C5%A0vyturys) confezionata a Klaipeda.


 Mentre mangiamo il [kugelis](http://ricette.donnamoderna.com/kugelis-lituania), vicino a noi si siede una signora di 84 anni con cui parlo i miei 10 minuti di sopravvivenza in lingua lituana. Aldona, questo e' il nome della signora, **e' arrivata in Australia a 18 anni** (1950) e il suo paese di origine e' lo stesso della nonna di Rita. La ragazza che mi serve il terribile caffe' lituano e' nata in Australia, parla bene il lituano e parla l'inglese con l'accento australiano, e' giusto tornata da un viaggio a Vilnius e ne parla come una seconda casa.


 Finito di mangiare e parlando un po' con i membri del club, Rita e i bambini sono invitati a partecipare alla scuola lituana. Il fatto che Greta e Tomas parlino il lituano senza accento perche' migrati da poco li rende preziosi alla scuola perche' possono parlare con altri bambini in lituano. L'insegnante piu' volte si e' raccomandata di portare i bimbi alla scuola e partecipare alle attivita' lituane.


 Melbourne e' una delle citta' con piu' etnie differenti al mondo e come mi disse una mia amica, essendo tutte le culture ospiti di questo paese, c'e' un'armonia che difficilmente si trova in altre parti del globo. Ogni cultura si ritrova e si preserva negli anni, mantenendo lingua e tradizioni ed e' affascinante visitare il Queen Victoria Market durante l'estate per poter assaporare il cibo di diverse parti del mondo in un unico posto.


 Una lista, molto parziale dei club italiani, e' presente [qui](http://www.onlymelbourne.com.au/italian-clubs-melbourne), ma tenendo d'occhio il sito [onlymelbourne](http://www.onlymelbourne.com.au/) ogni settimana e' possibile partecipare ad un evento di carattere etnico/culturale.


 Si, siamo lontani dall'Europa, ma se [l'Australia partecipa all'Eurovision](http://www.eurovision.tv/page/history/year/participant-profile/?song=33703), un motivo ci sara', no?

  

