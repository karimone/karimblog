Title: Passeggiata
Date: 2006-05-01
Category: Lituania
Slug: passeggiata
Author: Karim N Gorjux
Summary: Il primo maggio è ben poco sentito qui in Lituania, a parte le scuole e gli uffici, sia negozi di alimentari che i grandi centri commerciali sono aperti rispettando l'orario abituale. Sono[...]

Il primo maggio è ben poco sentito qui in Lituania, a parte le scuole e gli uffici, sia negozi di alimentari che i grandi centri commerciali sono aperti rispettando l'orario abituale. Sono uscito a fare due passi verso le 19, il vento era abbastanza forte, ma ringraziando il cielo faceva caldo. Il termometro segnava 14 gradi.


 Ho aggiunto qualche foto su [flickr](http://photos.karimblog.net), godetevi quel poco di primavera che, per ora, riesce a dare la Lituania.


