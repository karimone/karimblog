Title: Non è un paese per i vecchi
Date: 2008-07-07
Category: altro
Slug: non-è-un-paese-per-i-vecchi
Author: Karim N Gorjux
Summary: Ecco, la settimana scorsa hanno scoperto una coppia, in California, che affittava camere ai vecchietti, poi li ammazzava, li seppelliva in giardino, e intascava le loro pensioni. Ah, e prima li torturava,[...]


> Ecco, la settimana scorsa hanno scoperto una coppia, in California, che affittava camere ai vecchietti, poi li ammazzava, li seppelliva in giardino, e intascava le loro pensioni. Ah, e prima li torturava, non so perché, forse il televisore si era guastato. E la cosa è andata avanti finché, testuali parole, «i vicini si sono allarmati quando hanno visto un uomo scappare con indosso solo un collare per cani». È impossibile inventarsi una notizia così, provaci, non ci riesci. Questo c'è voluto per attirare l'attenzione di qualcuno: scavare fosse in giardino era passato inosservato.  
>   
> ###### Sceriffo Bell
> 
> 

 Ho visto il film [Non è un paese per vecchi](http://it.wikipedia.org/wiki/Non_%C3%A8_un_paese_per_vecchi_(film)) tratto dall'omonimo libro. Ho aspettato che uscisse la versione in DVD per potermelo scaricare, l'ho visto e ora, diligentemente, cancellerò la prova ;-)

 E' un film violento, strano, pieno di quelle scene silenziose che mi piacciono tanto; è ambientato nel 1980, tra il Texas ed il Mexico e lo stile lo si può definire "western moderno".


 Mi è piaciuto.


