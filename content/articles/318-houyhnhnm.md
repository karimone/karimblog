Title: Houyhnhnm
Date: 2007-04-19
Category: altro
Slug: houyhnhnm
Author: Karim N Gorjux
Summary: Conoscete persone che vi dicono una cosa e non la fanno? Mi è capitato nuovamente sabato con un recidivo, me lo aspettavo, ma questa volta non so perché ci penso più del[...]

Conoscete persone che vi dicono una cosa e non la fanno? Mi è capitato nuovamente sabato con un recidivo, me lo aspettavo, ma questa volta non so perché ci penso più del solito.


 Mi disse che mi avrebbe chiamato per le 22 e io gli risposi: *"So già che non mi chiami, ma non preoccuparti, mi basta il pensiero"*. Lui di riflesso obbiettò: *"No, stai tranquillo, ti chiamo, ci sentiamo dopo"*.


 Naturalmente sono passati due giorni e non si è ancora fatto sentire. Una volta mi sarei arrabbiato, mi sarei offeso, ora che sono più vicino ai trenta che ai venti mi chiedo sempre prima se ne vale la pena. Vale la pena arrabbiarsi con qualcuno che si sa esattamente come è fatto, si sa esattamente come potrebbe reagire a date situazioni? Penso di no. Perché prendersela? La domanda più consona è: *"Quanto tempo ci vorrà, per questa persona, prima che le sue parole abbiano un minimo valore alle orecchie di chi lo ascolta?"*

 Recentemente mi è capitato qualcosa di strano, non ho intenzione di spiegare in dettaglio cosa mi è accaduto, ma visti i protagonisti e vista la vicenda definire strano l'accaduto è semplicemente riduttivo.   
Di una situazione del genere, tempo addietro, ne avrei fatto un dramma, ma ora è diverso, ho altre priorità e tra queste priorità, in primis, c'è il mio tempo. E' una delle più grandi lezioni che insegna la vita: **il tempo è tiranno, non bisogna sprecarlo**.


 Il titolo del post? Gli Houyhnhnm erano i cavalli parlanti incontrati da Gulliver nel suo ultimo viaggio e non avevano una parola per definire la "bugia". **Che senso ha dire ciò che non è?**

