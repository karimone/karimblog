Title: Back to the normality
Date: 2008-07-06
Category: altro
Slug: back-to-the-normality
Author: Karim N Gorjux
Summary: Stasera tornerò a casa da Demonte, ma a dire il vero ero già tornato a casa, ma non ho avuto voglia e tempo di tenervi aggiornati. Sono alle prese con due progetti[...]

Stasera tornerò a casa da Demonte, ma a dire il vero ero già tornato a casa, ma non ho avuto voglia e tempo di tenervi aggiornati. Sono alle prese con **due progetti** che mi prendono la maggior parte del tempo che rimango seduto davanti al computer. 

 Ho anche gradatamente abbandonato l'abitudine di essere sempre su [skype](http://www.skype.com) e su [google talk](http://www.google.com/talk/) per non ricevere continui disturbi da parte dei miei contatti; ho impostato Apple Mail di controllare la posta ogni 15 minuti invece di ogni 5 minuti.


 Da domani sarò per tutta la settimana tranquillo a casa senza Rita e Greta, potrò studiare indisturbato senza nessun elemento di disturbo riportando back to the normality this blog.


