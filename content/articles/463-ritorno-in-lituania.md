Title: Ritorno in Lituania
Date: 2008-07-09
Category: Lituania
Slug: ritorno-in-lituania
Author: Karim N Gorjux
Summary: Partirò per la Lituania nella settimana tra l'11 Agosto e il 17 Agosto. Ci andrò in macchina e sarò da solo quindi ho un posto libero tra le varie cose che devo[...]

Partirò per la Lituania nella settimana tra l'11 Agosto e il 17 Agosto. Ci andrò in macchina e sarò da solo quindi ho **un posto libero** tra le varie cose che devo portare su per Rita e Greta. 

 **Chi si aggrega con me?**

 Sono alla ricerca di qualcuno che mi accompagni nel viaggio, ma mi serve qualcuno che sappia arrangiarsi e che sia ben temprato per la situazione.


 [Sandro](http://www.balticman.net), [Enrico](http://www.henryblog.net/), [Massimo](http://www.massimoconsulting.com/), [Gaetano](http://www.magogae.net), [Davide](http://www.facebook.com/profile.php?id=553918104), fatevi avanti!

 In ogni caso:  
  
 3. La scelta finale spetta a me e secondo i miei criteri
  
 6. Non sono un **agenzia turistica**, chi viene con me una volta arrivato in Lituania si deve arrangiare
  
 9. Valutate la possibilità di **tornare in aereo** visto che la mia data di ritorno non è assolutamente certa
  
 12. Si dividono i costi a metà
  


