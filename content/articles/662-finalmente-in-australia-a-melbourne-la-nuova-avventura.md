Title: Finalmente in Australia a Melbourne: la nuova avventura
Date: 2015-01-24
Category: Australia
Slug: finalmente-in-australia-a-melbourne-la-nuova-avventura
Author: Karim N Gorjux
Summary: Melbourne Southbank

[![Melbourne Southbank](http://www.karimblog.net/wp-content/uploads/2015/01/melbourne-300x200.jpg)](https://www.flickr.com/photos/alanandanders/236302157/in/photolist-mT7p8-7Uy7S-2sHhs-8Xn8HU-4WARAu-4LuqUv-kmn4p-kyND5-bzdRUr-dvXEi-iqqCx5-9k7Voi-66tc26-5oj4tV-5bSkqE-4Zcd5B-4YkrLH-4Xfysr-4zYvzP-4qixi5-CMsvE-nr2fob-bs9vKr-jnDPc-m4ypho-9TJPp3-9dEUm7-5KXRBX-5apXTq-59f952-4LMafq-4nSfMG-47SPTQ-5qpJGU-4XvZ91-9agHfR-5EuhL-95Ss3V-5aZ8gb-51DmzF-4nF1hd-3LbZXX-bzNSok-5BdEt6-EKKyq-buv3Uv-9jkJ4A-7U6Uxy-6i5a2W-5FyJD6) Melbourne Southbank

 Dopo un volo Dubai-Melbourne da 13 ore, ho finalmente messo piede in Australia il 4 Novembre 2014. Era il giorno della [Melbourne Cup](http://www.melbournecup.com/ "Official Melbourne Cup") e le persone andavano in giro per la città vestite come negli anni '60, il centro era semi deserto e, nonostante ciò, sono riuscito a fare una delle cose più importanti per chi arriva in Australia: acquistare una SIM telefonica.  


 Dato che è da un po' che non scrivo più niente, meglio fare un passo indietro e **spiegare cosa è successo**. Tornare in Italia è stato un passo falso, mi ha fatto piacere sotto molti punti di vista dato che l'Italia è pur sempre il mio paese, ma purtroppo non è il paese in cui posso far crescere i miei due bambini; a parte le solite persone che si arricchiscono sempre di più, la classe media arranca e arranca. Mancano tante cose, si stanno perdendo altre, ma non è dell'Italia che voglio scrivere.


 **La prima volta che ho pensato all'Australia** come meta è stato nel 2010, i motivi per cui avevo incluso l'altra parte del mondo tra le mete papabili sono tanti, ma il più importante di tutti è il fatto di essere cittadino australiano per discendenza. Ho sempre saputo che è una fortuna essere australiani, ma da quando sono qui a Melbourne, sono andato oltre la conoscenza; ho avuto modo di parlare con altri immigrati e sentire raccontare in prima persona le **difficoltà** di chi cerca di rimanere in questo paese.


 Nonostante battessi l'argomento Australia, mia moglie è sempre stata restia. La capisco, non è facile andare dall'altra parte del mondo, significa tagliare i ponti con tutto ciò che conosci ed affidarti ò della vita stessa. Mia moglie preferiva di gran lunga rimanere in Europa e quindi abbiamo lasciato l'Italia e ci siamo diretti per la Lituania dove mia moglie è rimasta con i bambini. Io nel mentre ho avuto modo di vedere la Svezia.


 **La Svezia è un paese incredibilmente bello** nonostante l'abbia visto poco. Richiede molto tempo preparare il trasferimento in un paese del genere, un po' come l'Australia è facile entrare, ma **rimanerci come residenti è molto complicato**. Inoltre c'è un problema: "la lingua". In questi anni ho sempre battuto sul discorso "lingua", anche se puoi lavorare in qualsiasi paese senza sapere la lingua locale, il farlo ti isola e rende l'integrazione a "*pain in the ass*". Dopo aver visitato Stoccolma ho capito che la cosa giusta da fare è di andare in Australia da cittadino e con il vantaggio di sapere la lingua più che discretamente.


 Da Marzo ad Ottobre sono rimasto in Lituania preparando la partenza per Melbourne. **Nonostante io sia Australiano, la preparazione mi ha chiesto molto tempo.** Non è facile informarsi su come ci si deve muovere in un altro paese e ai giorni nostri oltre google c'è Facebook con i suoi gruppi. Mia moglie non è mai stata d'accordo con la mia decisione *palesemente antidemocratica* di andare in Australia, ma io non potevo vedere il mio passaporto australiano prendere polvere senza provare ad usarlo e quindi ho iniziato a preparare la mia strategia su come andare in Australia e anche a migliorare le mie skills per poter ottenere la professione che volevo.


 Nei primi mesi ho frequentato i gruppi facebook sull'Australia rendendomi conto che gli italiani sono alla frutta, la gente è nervosa, risponde con cattiveria soprattutto a chi è impacciato e pone le domande più elementari; il più delle volte si pone la domanda e poi si finisci a litigare nelle risposte.


 A quel punto, stufo di questi gruppi mal gestiti, ho riunito le persone più capaci che ho conosciuto nei vari gruppi e ne ho fondato uno in collaborazione con un altro ragazzo il nostro gruppo. Attualmente ["Australia Facile (se sai come fare)"](https://www.facebook.com/groups/australia.facile/ "Gruppo Facebook Australia Facile") e al momento in cui sto scrivendo questo post conta 5000 iscritti circa. Grazie al gruppo e alla mole di informazioni che ho trovato in rete sono riuscito ad arrivare a Melbourne preparato. Ora sono tre mesi che sono in Australia.


 Questo mio post è solo per prepararti ai prossimi articoli che arriveranno: "Storie di un italo-australiano in Australia"

 *Viso gero Lietuva!*

