Title: Lo sport, il cibo e i pensieri sono i segreti per una vita felice e piena di soddisfazioni
Date: 2009-07-10
Category: Lituania
Slug: lo-sport-il-cibo-e-i-pensieri-sono-i-segreti-per-una-vita-felice-e-piena-di-soddisfazioni
Author: Karim N Gorjux
Summary: E' qualche giorno che sono a casa da solo, ma già da domani dovrei riavere le mie due donne in casa. La settimana è passata ottimamente, il tempo è stato variabile con[...]

[![Cibi Vivi](http://farm4.static.flickr.com/3015/3707332444_2a2d84004a_m.jpg)](http://www.flickr.com/photos/kmen-org/3707332444/ "Cibi Vivi di karimblog, su Flickr")E' qualche giorno che sono a casa da solo, ma già da domani dovrei riavere le mie due donne in casa. La settimana è passata ottimamente, il tempo è stato variabile con qualche giornata di sole ventosa e un po' di pioggia che mi ha rinchiuso in casa e mi ha permesso di dedicarmi full time al lavoro.  
  
In questi giorni mi sono spostato e continuo a spostarmi esclusivamente in bicicletta; da casa mia al centro dove vado a giocare a calcio sono circa 10km e quindi mi ritrovo a partire verso le 17 da casa per dover abbandonare il campo verso le 21 quando il freddo mi ricorda prepotentemente che in Lituania il sole estivo sembra immortale e la fresca arietta serale non ha nulla da invidiare alle nostre villeggiature montane.


 Tra bicicletta e calcio mi sembra di essere ringiovanito, ho perso peso, migliorato il fisico, ma soprattutto ho ritrovato quell'amore per lo sport e la fatica che avevo a 20 anni. Ricordo ancora quando prendevo la bicicletta da Centallo per andare in palestra a Cuneo (14km) e dopo 1 ora e mezza di allenamento me ne tornavo a casa. **Lo sport è una pratica quotidiana** che non dovrebbe mai essere messa da parte.


 Lo sport è solo una parte del cambiamento. E' importante abbinare l'attività fisica ad una buona dieta, siamo o non siamo ciò che mangiamo? Ho iniziato a privilegiare i cibi vivi ai cibi morti, ho diminuito drasticamente il consumo di formaggi, pasta, pizza e bibite gasate; sorrido di più, controllo le emozioni e cerco di controllare i miei pensieri evitando di pormi delle limitazioni.


 Forse non sei pratico di questi argomenti, quindi cerco di essere più dettagliato. Con il mangiare i cibi vivi intendo che preferisco mangiare frutta e verdura e non "cadaveri". I cibi vivi sono quelli che hanno un colore vivo; se ci pensi bene è molto più gratificante mangiarsi un pomodoro o una mela che la chiappa di una mucca; con questo non intendo dire che bisogna diventare vegetariani, ma solo di cambiare le dosi. A me le bistecche piacciono, ma soprattuto in questo periodo estivo cerco di mangiarne molto meno.


 Riguardo ai pensieri invece sono convinto che noi siamo il risultato delle nostre esperienze e le nostre esperienze derivano dalle nostre scelte. La nostra mente è come un paio di occhiali per vedere la realtà, ovvio che la realtà una sola, ma l'interpretazione che diamo ad essa è ciò che permette di crescere.


 L'ideogramma cinese usato per identificare la parola "crisi" è composto da altri due ideogrammi: "pericolo" e "opportunità". Quando sono venuto qui, in Lituania, dopo le mie esperienze precedenti, ho cercato di **vuotare la tazza** e riprovare. Ho messo da parte tutti i miei **preconcetti** e il risultato è stato oltre ogni aspettativa; ieri pomeriggio mi trovavo al campo da calcio a chiacchierare con i ragazzi del più e del meno e per un attimo, per la prima volta da quando sono qui, **mi è sembrato di essere a casa**.


 La Lituania, il blog, la famiglia, la vita in sé è stata e continua ad essere una continua lezione, ma proprio come a scuola c'è chi viene promosso a pieni voti e c'è chi invece viene bocciato. Io ho imparato che le avversità della vita, le difficoltà di tutti i giorni, devono essere affrontate con una profonda introspezione. **Bisogna essere onesti con se stessi** e capire il perché succedono, trovare le energie per reagire e imparare la lezione per migliorare.


 Tempo addietro frequentavo una persona molto intelligente, ma vestiva sempre di nero, guardava tutto e tutti con diffidenza, affrontava i problemi in modo tragico e soprattutto non sorrideva mai; il suo modo di pensare e di agire era contagioso e ti garantisco che non è un bel modo di vivere; per fortuna ho smesso di frequentare questo ragazzo, le nostre strade si sono divise e anche da questa storia ho tratto degli utili insegnamenti. La vita deve essere vissuta pienamente, umilmente e senza porsi continui e stupidi limiti, ma soprattutto bisogna pulire la propria mente dai pensieri cattivi.


 Ora dopo questo piccolo sfogo, mi preparo per 12km di bici, vado dritto al campo da calcio dove sono stato invitato per un'amichevole con altri ragazzi lituani.  


  
*"Vivere è la cosa più rara al mondo. La maggior parte della gente esiste, e nulla più."*  
* Oscar Wilde*



