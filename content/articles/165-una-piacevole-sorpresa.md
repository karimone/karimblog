Title: Una piacevole sorpresa...
Date: 2006-04-19
Category: altro
Slug: una-piacevole-sorpresa
Author: Karim N Gorjux
Summary: Oggi ho passato la giornata in palestra e tra una chiacchiera e l'altra con il mio socio si è fatto parecchio tardi.. Dopo la palestra sono andato all'Ippogrifo (la mitica libreria di[...]

Oggi ho passato la giornata in palestra e tra una chiacchiera e l'altra con il mio socio si è fatto parecchio tardi.. Dopo la palestra sono andato all'Ippogrifo (la mitica libreria di Cuneo) a fare scorta di libri.


 Il commesso dell'Ippogrifo è un bravo ragazzo barbuto con la folta chioma nera legata a coda di cavallo. A prima vista Giampy sembra un barbone ;-) capitato li per sbaglio, ma se vado all'ippogrifo e non da altre librerie è solo perché c'è lui. In questi anni tra un libro e l'altro mi sono sempre fatto qualche risata al punto di fare una visita in negozio giusto per salutarlo. 

 Il 28 notte partirò per la Lituania fino a data incerta e quindi mi sono preparato a comprare libri per neo babbi sprovveduti come me, Giampy mi ha consigliato e ordinato *La guida del giovane papà*. Oggi passo da lui a ritirarlo e... sorpresa! Giampy mi ha regalato il libro. Non me lo sarei mai aspettato, sono davvero contento, un regalo talmente inaspettato che mi ha lasciato senza parole.


 Già che c'ero ho comprato una guida per non disperarsi con il nascituro... fino ai 6 anni d'età sono teoricamente a posto.  
  
[![Il libro](http://www.karimblog.net/wp-content/uploads/2006/04/Foto%20187-1-tm.jpg "Il libro")](http://www.karimblog.net/wp-content/uploads/2006/04/Foto%20187-1.jpg)  


 Grazie Giampiero! Spero che visiterai il mio blog spesso! :-)

