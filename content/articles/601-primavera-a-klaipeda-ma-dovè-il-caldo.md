Title: Primavera a Klaipeda, ma dov'è il caldo?
Date: 2010-04-21
Category: Lituania
Slug: primavera-a-klaipeda-ma-dovè-il-caldo
Author: Karim N Gorjux
Summary: Hai sentito la mia mancanza? Dopo Pasqua mi sono praticamente dimenticato del sito web, mi sono occupato di altre cose, ma soprattutto mi sono goduto questo inizio di primavera. A dire il[...]

![](http://farm1.static.flickr.com/52/151450719_cd0e461299_m.jpg "Meridiana Klaipeda Barca")Hai sentito la mia mancanza? Dopo Pasqua mi sono praticamente dimenticato del sito web, mi sono occupato di altre cose, ma soprattutto mi sono goduto questo inizio di primavera. A dire il vero primavera è un parolone che qui in Lituania è da usare con le pinze perché attualmente di giorno c'è il sole, e non sempre, ma non manca mai il vento a rendere le giornate fredde, anzi freddissime!  
  
Quando io Greta andiamo a giocare nei giardini del quartiere, scendiamo ancora vestiti in tenuta antisommossa con cappellino, cappotto, guanti e occhialini. Fa freddo ed il vento è lo scotto da pagare quando si vive sul mare e senza montagne a difenderti; non andiamo mai allo scivolo davanti a casa nostra, ma un poco più lontano c'è uno scivolo ed un altalena che sono sempre baciati dal sole e stiamo li a giocare insieme per un oretta, purtroppo però, se ci sono nuvole, duriamo ben poco e dopo meno di mezz'ora torniamo a casa.


 A parte la pseudo primavera, sono tornato a giocare a calcio, questa volta però ho un gruppo di amici della palestra che gioca due volte alla settimana. Sono andato a giocare un Mercoledì ed oltre ad avere continuamente il fiatone, mi sono anche preso una bella storta alla caviglia sinistra. Ora mi tocca giocare con due cavigliere per entrambi i piedi. Come se non bastasse Greta mi ha graffiato un occhio e ho dovuto prendere degli antibiotici per eliminare l'infezione che stava avanzando. A coronare il tutto c'è una novità, sono diventato un poco presbite e ora mi tocca usare gli occhiali al computer e a leggere.


 Alba alle 6 e tramonto alle 21 :-)

