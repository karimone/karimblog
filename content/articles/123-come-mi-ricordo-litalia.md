Title: Come mi ricordo l'Italia.
Date: 2005-11-23
Category: Lituania
Slug: come-mi-ricordo-litalia
Author: Karim N Gorjux
Summary: ...ti ringrazio, l'idea di questo articolo è tua... :smile_tb:

*...ti ringrazio, l'idea di questo articolo è tua... :smile\_tb: *

 La Lituania vista da me, terronaccio di origine vissuto e cresciuto nel cuneese, terra di tartufi, castagne e vino. Ecco cosa noto in Lituania, terra delle patate, del freddo, delle pianure e dalle ragazze dagli occhi di ghiaccio.  


 **Il ketchup sulla pizza.** Nelle pizzerie della Lituania ad ogni tavolo potete trovare il ketchup e la maionese con cui fare un bel make-up alla vostra pizza. Per un Italiano è peggio di un film di Lino Banfi doppiato in tedesco.


 **I biscotti nel latte.** Se pucci il biscotto (se pensi quella cazzata che si dice in prima media vai su un altro blog) nel latte, ti guardano tutti con un'espressione schifata e vergognata. Tu ovviamente ci rimani male, pensi... ma che cazzo ho fatto? In Lituania chi bagna i biscotti sono i nonni che non hanno i denti.


 **Le scarpe in casa.** Se entri in casa d'altri (e anche nella tua se hai la morosa Lituana), sei obbligato a girare scalzo. Tutti lo fanno e le mogli evitano di pulire continuamente il pavimento. Sinceramente sono d'accordo.


 **Colazione, pranzo e cena.** Semplicemente non esistono. Ognuno mangia quando vuole e i ristoranti si adattano. Vuoi mangiarti le tagliatelle alle 4 del pomeriggio. Nessun problema. Una nota: i lituani sono molto sbrigativi nel mangiare, per noi è quasi un rito.


 **Il minibus.** Molta gente non ha la patente e in città medie come Klaipeda (200.000 abitanti) la soluzione è il minibus o taxi collettivo. Sono dei furgoni che fanno le stesse tratte dei bus pubblici, ma si fermano e caricano a richiesta. Alla stazione dei bus altri piccoli minibus portano un po' ovunque per la Lituania.


 **Gli ubriachi.** Li vedi dappertutto e qualsiasi ora del giorno. Al mattino può incrociare giovani ragazzi con la bottiglia di birra sul bus.


 **Le donne.** La maggior parte delle donne si veste elegante ogni giorno e inizia già da giovane. Le unghie sono sempre curatissime e in molti casi sono finte e ornamentate.


 **Gli uomini.** La maggior parte di essi sembra uscito dalla fabbrica. Alti, spessi, occhi azzurri e capelli corti. Autoritari, duri e poco inclini al sorriso.


 **Gli anziani.** Le donne sono sempre cicciose e vestite da unione sovietica, i denti d'oro sembrano quasi una bandiera, un'onoreficenza. Gli uomini vestono il bacco e giacche vecchie e consumate. Su alcuni si possono vedere tatuaggi, di epoche non troppo felici, impressi sul polso. A volte guardandogli negli occhi sembra di leggere la loro vita di sofferenze.


 **Il chiasso.** I lituani tendono a non urlare nei locali pubblici, non esiste il gran chiasso che abbiamo nei nostri bar o ristoranti. 

 **Le strade.** L'autostrada è come la nostra, ma non si paga. Ce ne sono due Vilnius - Klaipeda e una molto più grande che attraversa tutti e tre gli stati baltici. Gli autogrilli sono piccoli e tra un autogrill e l'altro ci sono solo foreste o campagna. Rarissime le case.


 **Le macchine.** Le macchine Lituane sono generalmente vecchie, dire lituane non è corretto perché la Lituania non produce macchine, le importa. Vecchie Audi, Golf, Renault sono qui a fare la crescita economica.


 **I bus.** Sono vecchi e trasandati, a parte a Vilnius che conta la maggior ricchezza del paese, ma in altri posti come a Klaipeda i bus sono i rifiuti di altri stati. A Klaipeda ho già letto *Non parlare al conducente* in danese, svedese, spagnolo, e tedesco. I bus italiani dell'ATM di Milano si trovano a Kaliningrad.


 **Le case.** Tutte uguali. I condomini sono essenzialmente costruiti allo stesso modo in tutti i paesi della Lithuania, ci sono quelli rossi, quelli grigio chiaro o quelli grigio scuro. Alti o medi. E' la prima cosa che salta all'occhio appena arrivati in Lituania.


 **Il cesso.** Capita di aver a che fare con il cesso sovietico. Ne ho parlato in post precedenti, è quello con lo scalino in modo che dovete dare la spinta per mandare tutto chissà dove.


 **La cucina.** Le cucine degli appartamenti sono minuscole, penso che sia dovuto al tempo sovietico quando i lavoratori mangiavano nelle fabbriche, sinceramente non ne sono sicuro.


 **L'inquinamento.** I lituani sporcano, buttano tutto in giro e il verde ne risente. Spero che un giorno se ne rendano conto, perché la flora e la fauna lituana sono stupende.


 **Il latte fresco in busta.** Se andate a comprare nei supermercati, trovati i prodotti derivati dal latte fresco in buste di plastica ben poco gestibile, infatti in casa ho la bottiglia del latte dove riverso tutta la busta. Come negli anni '50.


 **Il grietine.** Ovvero la panna acida. Qui è adorata, non gliela toccate. A me fa vomitare!

 **Il culto dei morti.** Anche a questo ho dedicato un post in particolare, i cimiteri sono molto belli e ben tenuti. Ci si va anche di sera tardi.


 **Il tramonto.** D'inverno alle 16 è già buio e quando c'è luce il cielo e grigio.


 **Le montagne.** Semplice, non ci sono. Quanto mi manca la Bisalta..


 **Il meteo.** Penso sinceramente che sia impossibile fare un meteo decente ad un paese in Lituania. Dato che non ci sono montagne la mattina può esserci il sole e dopo mezz'ora nevicare. Succede, e succede più spesso di quanto si creda.


 **I sorrisi.** Può capitare e non di rado, di ricevere un gentile e ospitale sorriso femminile. Se ricevi un sorriso da un ragazzo e perché ti ha appena pestato.


 **I taxi.** Dato che la gente non ha la patente e/o la macchina, i taxi possono essere davvero utili. Sono praticamente dappertutto, ma non fatevi fregare. Chiamatene uno al telefono, costano di meno.


 **I fiori.** Gli uomini si regalano fiori al compleanno e anche in altre festività. In Italia sarebbe un gesto quantomeno equivoco.


 **Depressi.** Più gli uomini che le donne, parlano male di loro stessi. Mai capito il perché.


 **Puntualità.** Qua è peggio che in Italia, se dai un appuntamento a qualcuno puoi anche arrivare mezz'ora dopo. Se ti dicono che richiameranno stai sicuro che non succede.


 **Senso civico.** Manco per il... All'HyperMaxima (la nostra ipercoop) fecero il grave errore di lasciare i carrelli senza gestione "mettimoneta-prendi-riporta-ripigliamoneta" i carrelli te li ritrovavi disseminati per il parcheggio. Mitica la scena della donna colta da altruismo che ha lanciato il carrello dalla metà del parcheggio verso il centro commerciale. Peccato che il parcheggio era leggermente in discesa. (Derio e Sam ne sono stati testimoni...)

 Per ora basta... se ne aggiungo altri vi avverto..  
  
  
adsense  
  


