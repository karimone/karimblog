Title: L'uso dell'italiano in un rapporto di coppia internazionale
Date: 2009-01-15
Category: Lituania
Slug: luso-dellitaliano-in-un-rapporto-di-coppia-internazionale
Author: Karim N Gorjux
Summary: L'inglese è semplice ed utile. Escludendo l'Italia dove la gente fatica a parlare in italiano, negli altri paesi l'inglese è una seconda lingua ben radicata, soprattutto nei giovani. Quando ho conosciuto Rita[...]

L'inglese è semplice ed utile. Escludendo l'Italia dove la gente fatica a parlare in italiano, negli altri paesi l'inglese è una seconda lingua ben radicata, soprattutto nei giovani. Quando ho conosciuto Rita parlavamo in inglese ed è stata la lingua che ci ha uniti, ma ora è solo più un lontano ricordo, ora parliamo sempre in italiano e questo è, secondo me, indispensabile.  
  
Non fraintendetemi, non dico che bisogna parlare necessariamente l'italiano, ma come mi suggerì tempo addietro mio padre, "*è meglio parlare una delle due lingue di cui uno dei due è madrelingua*". Ho scartato l'inglese perché io lo conosco, ma non così bene da parlarlo quotidianamente e Rita lo sa peggio di me. Il problema più grave è che usando una lingua "ponte" come l'inglese non si riescono a comunicare quelle che io chiamo "*le sfumature della vita*".


 **Lituano, si problem!** Ho riscontrato nei miei amici e nel mio caso personale che **è molto più facile insegnare a lei l'italiano che imparare il lituano facendosi aiutare da lei**. Rita non mi ha mai parlato in lituano più di un giorno e questo non mi ha mai aiutato a trovare la motivazione per studiare la lingua locale quotidianamente. Stranamente però Rita parla in lituano da sempre con nostra figlia e ora che siamo qui in Lituania, Greta parla più lituano che italiano (soprattutto grazie ai bimbi dell'asilo).


 **Italiano, no problem!** Ho iniziato a parlare solamente italiano dal giorno alla notte, come se niente fosse. La decisione è nata perché Rita non studiava l'italiano e, nel caso ci fossimo trasferiti in Italia (che è successo), mi sarei ritrovato a gestire le difficoltà di ambientazione di Rita in Italia che, fresco delle mie esperienze in Lituania, volevo assolutamente evitargliele.


 Nel quarto mese di gravidanza di Rita ho iniziato a parlare **solamente in italiano** sbattendomene altamente le palle se lei aveva già delle difficolta con la nostra piccolina che stava arrivando. Per i primi due mesi è stato difficile, dovevo prima dire le cose in italiano e poi spiegarle in inglese, ma non mi sono rassegnato, Rita si incazzava e io continuavo giorno per giorno, come se niente fosse.


 Circa 8 mesi dopo Rita e Greta sono venute in Italia. Rita parlava italiano più che discretamente anche se le mancava la pratica sul campo. Dopo 1 anno era quasi italiana: poteva andare in giro per il mercato da sola, andare nei negozi, in posta, in banca, all'asilo per Greta, parlare con i miei parenti, fare telefonate e guardare i film. I vantaggi non sono da poco perché insegnare l'italiano a Rita le ha permesso in Italia:  
  
 * Di non sentirsi una "russa"
  
 * Di litigare e/o discutere facendosi capire al 100%
  
 * Di avere nuovi amici
  
 * Di essere indipendente
  
 * Di poter fare una vita sociale senza limitazioni
  
  
Nel prossimo post parlerò del mio rapporto in Lituania con la lingua lituana.


