Title: Record! 4 giorni di sole!
Date: 2006-03-11
Category: Lituania
Slug: record-4-giorni-di-sole
Author: Karim N Gorjux
Summary: Ieri, visto l'evento, ho persino stappato la bottiglia per festeggiare. Il Bracchetto d'Acqui è il vino preferito sia per mio che di Rita ed è riservato per le grandi occasioni. Quattro giorni[...]

Ieri, visto l'evento, ho persino stappato la bottiglia per festeggiare. Il *Bracchetto d'Acqui* è il vino preferito sia per mio che di Rita ed è riservato per le grandi occasioni. **Quattro giorni consecutivi di sole**, in questo periodo, sono un grande evento in Lituania.  


 Finalmente le lezioni di Lituano hanno preso il ritmo che speravo, sto imparando a cavarmela nelle situazioni di tutti i giorni. Purtroppo però, dovrò interrompere tra 10 giorni, ma avrò tutta l'estate per mettermi alla prova.


 Il tempo continua a migliorare e gli effetti sono molto piacevoli, mi sento meglio e il mio umore è migliore, i tempi bui di fine Gennaio saranno, per qualche mese, solo un ricordo. L'estate Lituana è letteralmente una figata e consiglio a tutte le persone che pensano di venire da queste parti di **evitare** l'inverno come periodo di visita. L'estate regala al turista tutto ciò che ha di bello la Lituania da mostrare: prati, laghi, foreste, ma in particolare [Neringa](http://www.visitneringa.com/en). Venire in Lituania senza visitare Neringa è come andare a Napoli e non mangiare la pizza. (non è il migliore dei paragoni, ma penso che renda l'idea). Quindi venite a trovarmi a Klaipeda che vi spiego tutto... Neringa è a 10 minuti di traghetto da qui.  
  
  
adsense  
  


