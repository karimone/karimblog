Title: Una strana sensazione (sempre tricolore)
Date: 2008-06-11
Category: altro
Slug: una-strana-sensazione-sempre-tricolore
Author: Karim N Gorjux
Summary: Gli Europei di calcio.

Gli Europei di calcio.


 La nazionale, gioca (male) e perde. Spettatori in TV: 18 Milioni d'italiani.


 Siamo il paese del calcio, se l'Italia vince si scende in piazza, ma nessuno scende in piazza per i diritti che ci spetterebbero come paese civile.  
  
Leggete [questo](http://www.attivissimo.net/me/motivi_italia_uk.htm) se non avessi una moglie ed una figlia, penserei seriamente di andarmene in un altro paese. In Inghilterra o in Danimarca o in qualsiasi paese europeo che mi dia una sensazione di rispetto reciproco. Anche la Spagna non è da disdegnare, non conosco bene la situazione, non ho mai vissuto in Spagna, ma rispetto a 20 anni fa hanno fatto davvero dei passi avanti. E noi?

 All'Italia manca la concezione di vivere per il bene di tutti, non nel senso comunista del termine, ma nel senso civile. Ci sono piccole cose che sicuramente migliorerebbero la vita a tutti, ma la mentalità italiana è egoista e superficiale. Noi siamo degli stranieri nel nostro paese perché i monumenti, le opere d'arte, il territorio non appartiene a noi, ma al massimo ai nostri antenati, abbiamo perso tutto.


 Io sono libero professionista, il mio obbiettivo e migliorare la vita informatica dei miei clienti tramite le consulenze. Se fatturo 100€ + IVA dei 100€ imponibile il 20% lo devo versare subito come tassa tramite un F24 a nome del mio cliente. Un altro 30% lo devo mettere da parte per eventuale INPS e altre tasse. Il resto me lo posso tenere e usare per pagare il commercialista, la benzina alle stelle, il canone rai, la tassa sui rifiuti, il baby parking per Greta...  
Se poi qualcuno non ti paga, tu l'IVA la devi versare lo stesso. Se ad esempio ho una fattura di 2000€ + 400€ di IVA e non viene pagata, io devo versare 400€ di IVA allo stato che non ne vuole assolutamente sapere nulla che io non sono stato pagato e soprattutto non offre nessuna garanzia per i piccoli imprenditori. In pratica, oltre a non essere pagato, bisogna versare 400€ che non è nient'altro l'affitto di casa mia. Tu cerchi di lavorare, vieni inchiappettato e paghi. Se poi provi a fare valere i tuoi diritti, tra avvocati ed altro ti conviene stare zitto e farti furbo. Intanto il tuo "cliente" si scarica la tua fattura...


 La tassazione è la stessa della Svezia, ma i servizi statali svedesi (a quanto ne so) sono tutt'altra cosa rispetto ai nostri. A tal proposito se qualcuno ha qualche notizia, sarei proprio curioso di saperne di più.


 Io sono d'accordo che pagare le tasse sia un dovere civile, ma il mio commercialista è il primo a dirmi che se voglio salvarmi devo evadere. Non c'è nulla da fare perché il sistema tassativo è fatto in base a questo presupposto, quindi se vuoi essere onesto ti viene impedito dallo stesso stato. E' incredibile.


 Il 14 Marzo, ho chiuso la società che avevo prima per poter diventare libero professionista, ma ancora adesso quel rincoglionito del mio ex commercialista mi cerca perché devo pagare delle tasse. Io ho chiuso la società in perdita, ma allo stato non interessa, io devo pagare le tasse. E come le pago?

 Come dicevo prima, le tasse sarei contentissimo di pagarle, ma solo se ci fossero dei servizi alla pari di ciò che pago. E soprattutto vorrei che le pagassero tutti. Come diceva Crozza in una copertina di Ballarò: "E' giusto pagare le tasse, se vivi in Svizzera". Forse la Svizzera non è così perfetta come si dice, ma la mentalità è molto diversa dalla nostra, il rispetto comune è la forza di questo paese che ha tantissime risorse in meno di noi.


 Le case costano carissime, gli affitti non hanno senso, la benzina è alle stelle... che futuro ci aspetta? Che futuro riserba l'Italia a mia figlia? 

 Ma che cazzo di paese è questo?

