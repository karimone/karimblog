Title: Dio Denaro
Date: 2004-08-15
Category: Lettonia
Slug: dio-denaro
Author: Karim N Gorjux
Summary: Poche sere fa' sono stato testimone di un fatto molto curioso.Aspettavo il bus numero 5 e avevo 30 minuti di attesa, ho pensato bene diandarmi a prendere un gelato al mac donald[...]

Poche sere fa' sono stato testimone di un fatto molto curioso.  
Aspettavo il bus numero 5 e avevo 30 minuti di attesa, ho pensato bene di  
andarmi a prendere un gelato al mac donald non poco distante (costo: 19 santimi ovvero  
29 centesimi di euro), mentre facevo l'interminabile fila (era l'una e un quarto  
di notte) ascoltavo due ragazzi francesi che parlavano davanti a me. La fila era  
interminabile, ma non si trovava all'interno del locale, eravamo tutti davanti  
allo sportello che da' sulla piazza centrale perchè dopo l'una il locale e'  
chiuso e rimane aperto solo lo sportello esterno fino alle 2 di notte.


 I ragazzi davanti a me ordinano e uno di loro riceve subito il classico  
sacchetto di cartone stampato Mc Donalds con all'interno l'hamburger e le  
patatine e si mette da parte per aspettare l'amico. Nell'attesa si avvicina un  
signore sulla trentina biondo, occhi azzurri... un viso tipicamente russo e  
chiede al ragazzo se gli vende cio' che ha appena comprato per 20 lats. Il  
ragazzo rimane sbigottito pensando a uno scherzo, poi si rende conto che  
questo strano signore era dannatamente serio.   
Accetta con un cenno con il capo sintetico e automatico, gli stringe la mano,   
lo saluta e intasca i soldi. Un decimo di secondo dopo chiede  
subito all'amico, ancora allo sportello, di ordinargli di nuovo l'hamburger e le  
patatine.


 Ho fatto due parole con i due ragazzi francesi (di Parigi)... e gli ho spiegato  
che qui la via di mezzo non è molto ampia: o sono particolarmente ricchi o sono  
particolarmente poveri. I benestanti non sono molti. 20 lats per un'hamburger e  
due patatine E' un'enormita', il prezzo normale E' circa 1 lat, quindi 20 volte  
tanto... fate due conti.. se spendete 3 euro in Italia significa che il tipo ha  
sborsato l'equivalente di 60 euro... Una mia amica mi ha detto che per  
guadagnare 5 lat deve lavorare tutto il giorno (piu' di 8 ore). Cosa significa  
tutto questo? Che sei hai tanti soldi per te il mondo non è lo stesso degli  
altri, basta pagare. Qui in lettonia è anche molto facile corrompere la polizia.  
Guidi ubriaco? Se ti fermano sei nei guai, ritiro patente! Ma se sganci 50 lat è  
come se non ti avessero fermato.


 Non c'e' nulla di nuovo direte voi. E' vero, in tutto il mondo funziona cosi', ma  
qui e' tutto cosi' plateale..


