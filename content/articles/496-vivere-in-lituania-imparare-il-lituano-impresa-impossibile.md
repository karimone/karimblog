Title: Vivere in Lituania: imparare il lituano, impresa impossibile?
Date: 2008-10-18
Category: Lituania
Slug: vivere-in-lituania-imparare-il-lituano-impresa-impossibile
Author: Karim N Gorjux
Summary: Parlare il lituano significa padroneggiare bene la lingua non a livelli di un madre lingua, ma abbastanza bene da farsi capire e capire senza problemi.

Parlare il lituano significa padroneggiare bene la lingua non a livelli di un madre lingua, ma abbastanza bene da farsi capire e capire senza problemi.


 Parlare il lituano come Tarzan è un livello di padronanza della lingua che si ottiene facilmente senza nemmeno studiare tanto, ma il risultato è stressante. Sapere la lingua a questi livelli mi ha aiutato a togliermi dagli impicci, ma è come andare in macchina con le ruote bucate. Insostenibile.


  Per imparare il lituano bisogna ascoltarlo, parlarlo, leggerlo e scriverlo. I bambini imparano la lingua in questo preciso ordine, mentre noi adulti facciamo esattamente il contrario. I miei consigli per imparare il lituano decentemente sono:  
  
 * **Obbligare** le persone a parlarti in lituano, moglie, amici e chiunque possa aiutarti.

  
 * **Ascoltare** la televisione lituana aiuto. Non solo ci si informa del paese, ma si migliora l'accento.

  
 * Frequentare dei **corsi** con un insegnate madrelingua. Purtroppo la lingua di base è sempre l'inglese in questi casi. Consiglio: rivolgetevi alle università. Le scuole private, dato che siete uno straniero, vi faranno pagare il triplo.

  
 * Munirsi di un **dizionario**
  
 * **Non vergognarsi** di fare degli errori mentre si parla
  
 * Fare un figlio in Lituania e imparare con lui la lingua **(facoltativo)**
  
 * Acquistare dei supporti cartacei e/o corsi, qui in Lituania se ne possono trovare molti, ma anche [amazon è d'aiuto](http://www.amazon.com/Colloquial-Lithuanian-Complete-Beginners-Multimedia/dp/0415121051)
  
 * **Non perdersi mai d'animo**
  
  
Non bisogna avere fretta, il lituano è una lingua difficile, non si impara in pochi mesi, tutt'altro! Ma imparare questa lingua così antica e affascinare è **l'unica strada** per scoprire un popolo particolare come il popolo lituano.  
  
[![Lithuanian language](http://farm4.static.flickr.com/3282/2950516917_f956c03250_m.jpg)](http://www.flickr.com/photos/kmen-org/2950516917/ "Lithuanian language di karimblog, su Flickr")  


