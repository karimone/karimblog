Title: L'italiano a Riga (e non solo)
Date: 2004-08-15
Category: Lettonia
Slug: litaliano-a-riga-e-non-solo
Author: Karim N Gorjux
Summary: Descrizione del tipico italiano a Riga

**Descrizione del tipico italiano a Riga**

   
 * *Il ciccione con i soldi:* tipicamente vestito bene, appesantito da secchiellate di gel, indossa occhiali firmati, rolex e straborda di lipidi da qualsiasi punto del corpo. Spesso accompagnato da belle ragazze con il solo scopo di fare dimagrire non il nostro connazionale, ma il suo portafoglio. Età: dai 40 in sù. Segni particolari: ride sempre.



  - *Il branco affamato:* schiera di giovani ragazzi dai 20 ai 30 anni che si muove sempre e solo in branco. Si possono notare passeggiare per la città e si riconoscono dall'abbigliamento tipicamente sportivo, accento tipicamente meridionale e grandi praticatori dello stretching cervicale. (movimenti sincronizzati della testa a destra a sinistra casualmente atti all'ammirazioni di stupendi corpi femminili). Approccio alle ragazze in gruppo, molte prede in questo periodo evitano di passare per la via principale proprio per evitare questi cacciatori. Segni particolari: Se è da solo non ride.



  - *La famigliola: *con bambini o senza sono i turisti che non sperperano e viaggiano per la città per soddisfare la propria curiosità rispettando il luogo e la gente. Sono cordiali e simpatici. Segni particolari: Zaini e macchine fotografiche.



  - *Karim* .... pecco di immodestia?
  


