Title: Novita' novita' e ancora novita'
Date: 2005-05-11
Category: Lituania
Slug: novita-novita-e-ancora-novita
Author: Karim N Gorjux
Summary: Domani arrivano i due gladiatori: Derio e Sam. Approderanno all'aeroporto di Vilnius per la loro prima esperienza nordica. Ad aspettarli ci saranno il Kappa e il Dave. :coolhmm_ee:Vi avverto subito ragazzi, se[...]

Domani arrivano i due gladiatori: Derio e Sam. Approderanno all'aeroporto di Vilnius per la loro prima esperienza nordica. Ad aspettarli ci saranno il Kappa e il Dave. :coolhmm\_ee:  
Vi avverto subito ragazzi, se siete fortunati avrete l'acqua calda. Io e Davide rimarremo senza fino al 16 Maggio.Normale amministrazione in una citta' con il passato comunista come Klaipeda. 

 Per quanto riguarda il mio iBook purtroppo dovro' aspettare ancora, a Milano sono in attesa del pezzo di ricambio e quindi mi tocca pazientare, nel mentre sono riuscito a rendere (con una strada tutta in salita) il mio sito degno di essere visitato. Ho altre cose per la mente, ma voglio aspettare ancora, prima di fare tutto cio' che desidero.


 Intanto... :smoke\_tb:

 Tra circa 1 ora dovrebbe arrivare il tecnico della [balticum](http://www.balticum.lt) per fare l'upgrade di internet. I dettagli: **100 kb/s internet e 200 kb/s per il protocollo http (www)** il tutto con i 25 canali televisivi internazionali (nemmeno 1 italiano :down\_tb: ). Il prezzo? Poco piu' di 20 Euro al mese.


 Per chi come me ha comprato un Mac sull'[apple store](http://www.apple.it) capira' benissimo la situazione in cui si trova [Matteo](http://www.diveintomydelirium.it): *Processing order*.


 Benvenuto Matteo nel grande mondo dei Mac! Mi raccomando, aspetto le foto! Non mi (ci) deludere!

