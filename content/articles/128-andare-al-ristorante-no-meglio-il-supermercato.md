Title: Andare al ristorante? No.. meglio il supermercato.
Date: 2005-12-02
Category: Lituania
Slug: andare-al-ristorante-no-meglio-il-supermercato
Author: Karim N Gorjux
Summary: L'ultima volta che ho parlato di cucina lituana, vi ho parlato dei vari ristoranti tipici locali, esistono altri tipi di ristoranti simili al Cili. A Vilnius ho notato dei ristoranti Italiani "Da[...]

L'ultima volta che ho parlato di cucina lituana, vi ho parlato dei vari ristoranti tipici locali, esistono altri tipi di ristoranti simili al Cili. A Vilnius ho notato dei ristoranti Italiani *"Da Antonio"*, ci sono entrato in buona fede, senza pensare che il nome Antonio dovrebbe essere coperto da ©...  
  
In pratica "Da Antonio" il pizzaiolo è Lituano, che delusione. Ma qualcuno di quelli che mi legge, sa se esiste una pizzeria con forno a legna attizzato da terrone in tutta Vilnius? Mi manca davvero un tarun da pizza.


 L'alternativa al ristorante è farsi da mangiare a casa, ovviamente vi tocca andare al supermercato, non potete mica farvi l'orto sul balcone e allevare i maiali in bagno. Quindi andiamo tutti insieme a braccetto al supermarket. A Klaipeda ho contato tre tipi di supermarket: iki, Maxima e Leader Price. Quest'ultimo è abbastanza economico rispetto ai primi due, ma parlare di economico qui ha poco senso soprattutto i primi giorni. Vi capiterò di andare a fare la spesa e con 20€ tornarvene a casa con due borsate piene di roba da mangiare per tutta la settimana in Italia questo non succede anche se per paragonarsi all'italia bisogna moltiplicare per 3 (forse meno) e quindi i 20€ diventano 60€.


 Come sopravvivere alla spesa lituana? Bene iniziamo con qualche avvertimento per l'italiano che si accinge a far la spesa e non sa come orientarsi. Alcuni cibi che devono essere evitati come la peste bubbonica indiana; il grietine lasciatelo dov'è e non fatevi fregare dall'acqua, esisteranno si e no 25 marche d'acqua e alcune sono peggio dell'acqua del rubinetto di Napoli. A mente ricordo la *Vytautas* che è uno schifo d'acqua incredibile, poi anche la *Magnezina*. Una che sembra buona è la *Mona* va giù sia gasata che naturale, diffidate anche della *Bonacqua* che non è altro che l'acqua della cocacola... bleah!

 Altra cosa importantissima da non acquistare è il tonno in scatola, io lo so' che voi vi aspettate di aprire la scatola e trovarvi i filetti alla rio mare che si tagliano con il grissino, in realtà quando aprite la scatola vi ritrovate un composto di tonno predigerito adatto alle mandibole dei soldati russi che entrarono a Berlino nel 1945. In pratica il tonno è già masticato e in più fa schifo. Se abitate vicino ad un grande ipermercato e trovate il tonno rio mare, il parmigiano reggiano e l'olio d'oliva, andate a casa e cucinatevi due spaghetti barilla, accendete un cero e cantate l'inno italiano. Vi metterete a piangere, soprattutto dopo 2 mesi di pseudopizza lituana.


 Tornando a **cosa non comprare** in Lituania, non posso non parlare della pasta. I Lituani non hanno la minima cognizione di come si cucina la pasta, per loro mettere due spaghetti nel piatto o appiccicati al muro per attaccare i poster non fa alcuna differenza. Noi Italiani invece siamo molto critici e quindi appena vediamo una marca di pasta con il tricolore ci fiondiamo sopra con orgoglio sperando di aver salvato il palato... ascoltate me, non uscite dalla trincea, al massimo acquistate *barilla*, *agnesi* o *de cecco*. La cottura qui è leggermente più lunga rispetto alla confezione (penso sia dovuto ad una questione di pressione atmosferica, lo avevo studiato alle medie).


 Fate anche attenzione a comprare lo scatolame se non avete un apriscatole decente a casa, io sono costretto ad usare un pugnale per aprire i pelati e quando mi metto all'opera sembra che sto girando un provino per *cast away*. Non sono riuscito a trovare un apriscatole degno del nome in tutta la Lituania, quelli che ho trovato fanno schifo e tutte le volte dimentico di portarne uno dall'italia. Quindi segnate sul taccuino cosa dovete portare: **moka del caffè** (qui ci sono ma costano care), *apriscatole*, *1 kg di parimigiano* :D eh.. *ricette da cucina*. La carne qui è molto buona quindi andate sul sicuro, riguardo l'olio da cucina comprate l'olio d'oliva, se comprate quello spagnolo costa una barda di soldi in meno rispetto a quello italiano, ma è sempre meglio dell'olio che si trova in Lituania. Una sorpresa sono le bustine per fare la purea di patate. Rispetto alle nostre sono ottime, provare per credere.


 Per oggi basta...  
  
  
  
  
adsense  
  
  
  


