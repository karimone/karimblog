Title: Quattro semplici consigli per trovare un buon commercialista e risparmiare molto denaro e tempo
Date: 2008-03-05
Category: altro
Slug: quattro-semplici-consigli-per-trovare-un-buon-commercialista-e-risparmiare-molto-denaro-e-tempo
Author: Karim N Gorjux
Summary: Come trovare un buon avvocato? Un buon dottore? Un buon commercialista? Me lo sono chiesto anche io e forse ho anche trovato una risposta, ma nulla di definitivo, mettetevi il cuore in[...]

Come trovare un buon avvocato? Un buon dottore? Un buon commercialista? Me lo sono chiesto anche io e forse ho anche trovato una risposta, ma **nulla di definitivo**, mettetevi il cuore in pace.


   
 3. Per prima cosa non è possibile fidarsi del "sentito dire", la professionalità in Italia non vive di **meritocrazia**. Enzo Biagi diceva: *la meritocrazia in Italia esiste solo nel calcio*; il problema è quindi nella **valutazione** di un buon commercialista che si può solo fare a posteriori e non a priori a meno che non si cerchi di **notare qualsiasi piccolo dettaglio.**


  - Il commercialista deve farvi guadagnare tempo e denaro. **Il tempo lo si guadagna utilizzando la tecnologia**: email, skype, fax sono solo un paio di esempi. Non ha più senso doversi fisicamente recare nell'ufficio del commercialista ogni volta che si ha una mezza domanda, meglio una mail, permette l'asincronia della risposta. Prima regola d'oro: il commercialista deve essere al passo con i tempi, ma direttamente! Non la schiava segretaria.



  - **Informatevi**. Non fate l'errore di delegare tutto a occhi chiusi, informatevi e scassate le palle a chi pagate per fare il loro lavoro. Informarvi [costa davvero poco](http://www.altroconsumo.it/map/src/99841.htm) ed è l'unico modo per capire se il commercialista vale qualcosa, **fate delle domande di cui sapete già la risposta**, dalle risposte potrete farvi qualche idea.



  - **L'attitudine** è anche un buon metro di misura. Se mentre parlate il commercialista prende appunti non è solo segno di organizzazione, ma di **considerazione**. E' prassi comune in Italia di non cambiare mai il commercialista se fa delle cazzate, non abbiate paura di mandarlo a **fare in culo** nel caso sia un incompetente! Non capisco perché si può divorziare dalla moglie e non dal commercialista! E' l'unico modo per trovarne uno valido nel tempo.

  


