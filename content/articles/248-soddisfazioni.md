Title: Soddisfazioni
Date: 2006-10-28
Category: Lituania
Slug: soddisfazioni
Author: Karim N Gorjux
Summary: In questo periodo mi occupo della nuova casa, sarà che sono in un paesino, sarà che qui ci si conosce tutti, ma è tutta un'altra storia rispetto all'Italia, alla fine mi sono[...]

In questo periodo mi occupo della nuova casa, sarà che sono in un paesino, sarà che qui ci si conosce tutti, ma è tutta un'altra storia rispetto all'Italia, alla fine mi sono chiesto se le persone che parlano tanto male dell'Italia quando sono all'estero lo fanno solo per giustificare una scelta. Ma c'è bisogno di giustificare una propria scelta?

