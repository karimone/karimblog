Title: Il lavoro nobilita...
Date: 2008-01-22
Category: altro
Slug: il-lavoro-nobilita
Author: Karim N Gorjux
Summary: Da poco è tornato Dario dall'Australia, dopo un'assenza di quasi 1 anno. La sua avventura purtroppo non è stata condivisa, ma da quel poco che mi ha raccontato deve essere stato uno[...]

Da poco è tornato Dario dall'Australia, dopo un'assenza di quasi 1 anno. La sua avventura purtroppo non è stata condivisa, ma da quel poco che mi ha raccontato deve essere stato uno di quei viaggi che ognuno di noi dovrebbe fare, purtroppo (o per fortuna) non posso più permettermi di andare in giro a cazzeggiare come facevo un tempo, ma se potessi tornare indietro cazzeggerei il doppio.  
Mi sono scelto un lavoro che mi diverte e che si avvicina il più possibile a quello che facevo già prima, non c'è da vergognarsi nell'essere pagati a fare ciò che si diverte e soprattutto il lavoro fa male, non quanto tale, ma quanto concezione.


 In un libro che mi stato regalato da un [caro amico](http://www.businessdoctor.it), l'autore racconta di come aveva notato che durante una partita di football americano gli spettatori si divertissero come dei matti e gli addetti alla sicurezza che non facevano nient'altro che assistere alla partita esattamente come gli spettatori non si divertissero affatto, ma tutt'altro! Erano imbronciati e incarogniti come se fosse qualcosa di estremamente spiacevole. La concezione di "lavoro" inculcata dalla società è dannosa e non lo penso [solo io](http://www.simonecristicchi.it/mp3/agosti.mp3 ).


 Non penso che sia il caso di incazzarsi come Tiziano Lugaresi in [questo filmato](http://www.youtube.com/watch?v=pJegEcWj10w), di cui sconsiglio la visione alle persone più sensibili dato che è infarcito di bestemmie e di insulti contro il governo, ma penso che sia più saggio fare come il mio amico Lello che ha avuto il coraggio di rimettersi in gioco a sessant'anni e partire per [Boca de Yuma](http://evm.vr-consortium.com/titres/repdominicaine/zze/zzon4/yuma3.htm) e vivere come guida turistica "indipendente" per i temerari che vorranno vivere a stretto contatto con gli abitanti dominicani.


 Ieri mentre uscivo di palestra ho incontrato un mio vecchio compagno di università che andava al lavoro in un piccolo ufficio a Cuneo. Otto ore con straordinari, 1000€ al mese forse 1200. Ma ha senso? Alla fine Dario è quello che ha capito più di tutti, a quando la sua prossima ripartenza? 

 Smetto qui altrimenti parto con i miei deliri.


 PS: Se Rita a Pasqua va in Lituania, io vado a trovare Lello. (Gli devo fare il sito..)

