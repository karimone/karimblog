Title: Torno a parlare di Lituania
Date: 2007-06-26
Category: Lituania
Slug: torno-a-parlare-di-lituania
Author: Karim N Gorjux
Summary: Dato che ora vivo in Italia i miei post sono diventati un variegato di articoli incentrati sui miei interessi e le mie avventure, ma cosa ha reso questo blog interessante è stata[...]

Dato che ora vivo in Italia i miei post sono diventati un variegato di articoli incentrati sui miei interessi e le mie avventure, ma cosa ha reso questo blog interessante è stata la mia **permanenza in Lituania**.


 Qualche sera fa mi chiama su Skype la mia amica di Klaipeda "Rasa", era davanti al computer con "Tolvaida", un'altra mia amica che per la cronaca doveva sposarsi con un tipo di Reggio Calabria, ma alla fine è tutto andato in brodo e la mia amica è tornata a vivere su mar Baltico.


 Entrambe le mie amiche hanno vissuto in Italia, amano l'Italia e parlano l'italiano. Entrambe **hanno un fidanzato lituano**. Entrambe hanno avuto le loro esperienze al sud d'Italia.


 Cambio discorso. Alcuni miei amici tornano in vacanza in Lituania, precisamente saranno a Vilnius per una decina di giorni durante il mese di Agosto. Il loro obiettivo è rivedere i locali notturni, ma soprattutto le ragazze che li frequentano, nel centro della capitale. Mi chiedevano consigli, ma non ho molto da dire, vediamo se in questo **non molto** ne esce qualcosa di utile.  
  
* **Siate onesti:** sia con voi stessi che con le altre persone. La Lituania è meta di turismo sessuale (all'acqua di rose) da ormai vari anni e sappiate che la gente non è scema, le ragazze sanno perché da vari anni ormai sono invasi da italiani, inglesi, tedeschi, americani etc. etc. 


 - **Viaggiate:** non esiste solo Vilnius, la Lituania si attraversa in 5 ore di macchina, è un piccolo stato ideale per le vacanze da 7-10 giorni. Non tutti parlano inglese, ma se non si esce dal sentiero turistico non dovrebbero esserci problemi. L'ideale sarebbe conoscere il russo e l'inglese.



 