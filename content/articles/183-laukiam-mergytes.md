Title: ...laukiam mergytes!
Date: 2006-05-29
Category: altro
Slug: laukiam-mergytes
Author: Karim N Gorjux
Summary: Lo schermo in bianco e nero mostrava degli strani disegni grigiastri, i cerchi nel monitor si ingrandivano e si restringevano ad ogni movimento della sua mano e io non ne capivo nulla.[...]

Lo schermo in bianco e nero mostrava degli strani disegni grigiastri, i cerchi nel monitor si ingrandivano e si restringevano ad ogni movimento della sua mano e io non ne capivo nulla. Lei segnava dei piccoli punti al computer e continuava a muovere la cloche come se dovesse raderle la pancia con un rasoio elettrico.


 Poi qualcosa si mostrò timidamente al centro del monitor, e in quell'attimo, le sensazioni di un momento, diventano un ricordo indelebile e irripetibile. Il suo piccolo cuoricino era lì, al centro di quella piccola insignificante finestra elettronica piena di tubi e di fili elettrici inanimati. Quel piccolo schermo mi mostrava la vita, una piccola vita nata attraverso me.


 Ora ecco un piedino, poi una manina, poi la schiena e poi un sorriso (questa volta della mamma): *Mergyte!*.


 E' una bambina.


 (Diciamo che è probabile all'80%)  
 technorati tags start tags: [baby](http://www.technorati.com/tag/baby), [ecografia](http://www.technorati.com/tag/ecografia), [bimba](http://www.technorati.com/tag/bimba), [lithuania](http://www.technorati.com/tag/lithuania), [lituania](http://www.technorati.com/tag/lituania), [mergyte](http://www.technorati.com/tag/merghite)



  technorati tags end 

