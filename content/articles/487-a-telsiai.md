Title: A Telsiai
Date: 2008-09-15
Category: Lituania
Slug: a-telsiai
Author: Karim N Gorjux
Summary: Questo weekend era dedicato ai suoceri, ma ho avuto la grande fortuna di arrivare solo la domenica e rimanere solo una notte. In Lituania ormai fa freddo, le giornate arrivano al massimo[...]

Questo weekend era dedicato ai suoceri, ma ho avuto la grande fortuna di arrivare solo la domenica e rimanere solo una notte. In Lituania ormai fa freddo, le giornate arrivano al massimo a 15, 16 gradi e di notte si toccano i 4 gradi. Ovviamente sono raffreddato e non sono l'unico.


 Per fortuna che mi sono portato il mio vecchio Mac Mini che ho collegato al posto del computer cesso assemblato del suocero. Ho fatto un po' di manutenzione e riesco almeno a gestire le email e fare qualche lavoro di routine.


 La mattinata prevede incontro con la Kirpykla (parrucchiere) e inizio procedure per prendere la residenza in Lituania. Vedremo.


   
[![Mac Mini](http://farm4.static.flickr.com/3141/2858148943_2620863377_m.jpg)](http://www.flickr.com/photos/kmen-org/2858148943/ "photo sharing")  


