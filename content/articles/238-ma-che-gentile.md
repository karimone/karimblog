Title: Ma che gentile
Date: 2006-10-12
Category: Lituania
Slug: ma-che-gentile
Author: Karim N Gorjux
Summary: Sono giusto due giorni che sono in Italia che un mio amico si presenta a casa mia per installarmi l'adsl via radio. Il mio comune non è provvisto di adsl telecom quindi[...]

Sono giusto due giorni che sono in Italia che un mio amico si presenta a casa mia per installarmi l'adsl via radio. Il mio comune non è provvisto di adsl telecom quindi abbiamo dovuto arrangiarci come potevamo appoggiandoci ad altri operatori. Purtroppo dalla firma del contratto all'installazione dell'adsl passano all'incirca 20 giorni, ma grazie a questo amico ho potuto contattare mia moglie da subito e anche aggiornare il blog.


 Mentre ero a Klaipeda ho ricevuto due pacchi da due miei amici carissimi a nome di mia figlia Greta. Due pensierini stupendi per festeggiare la mia primogenita.


 A casa avevo un sacco di roba fatto a mano dalle signore del mio campeggio, tutto per Greta.


 Oggi in palestra parlando di traslochi un mio amico mi offre gratis il suo furgone, basta che metta il gasolio.


 A Klaipeda la mia vicina di casa mi ha regalato due mele, è stato l'unico regalo che ho ricevuto da quando ero a Klaipeda, per inciso nessuno lituano mi ha mai offerto nemmeno un caffè. La signora che mi ha regalato le mele si chiama Oksana, siberiana, madre di due figli abita a Klaipeda da tre anni con il marito lituano. Ovviamente prima di andare via le ho regalato una bella bottiglia di bracchetto di Acqui. 

 Questi episodi non vogliono dire molto riguardo alle due culture (quella di Klaipeda e quella di Cuneo), ma vogliono solo dimostrare come la mia scelta di tornare in Italia sia stata più che ponderata.


