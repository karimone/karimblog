Title: John il gallese
Date: 2008-07-11
Category: altro
Slug: john-il-gallese
Author: Karim N Gorjux
Summary: John ha sessant'anni, vive a Mold nel Galles a pochi km dall'Inghilterra. La sua passione è lo sport: va in bici e corre in montagna. John trova la pianura noiosa e quindi[...]

John ha sessant'anni, vive a Mold nel Galles a pochi km dall'Inghilterra. La sua passione è lo sport: va in bici e corre in montagna. John trova la pianura noiosa e quindi si fa km e km correndo tra le montagne del cuneese.  
  
L'ho conosciuto nel campeggio di mia madre, ho rispolverato il mio inglese e abbiamo parlato per tutto il pomeriggio, John è un temerario una di quelle persone che affronta la vecchiaia a viso aperto e non si lascia intimorire. A sessant'anni atterra a Nizza con la sua bicicletta e vaga per le nostre zone abbuffando gli occhi di spettacolari paesaggi montani.


 John mi ha raccontato del suo paese e sono rimasto piacevolmente impressionato dal sapere che esistono paesi che funzionino funzionano. La polizia rimane di pattuglia sul ciglio della strada e tramite una telecamera collegata ad un computer legge tutte le targhe delle macchine che passano ed in tempo reale sono a conoscenza di tre cose: se hai pagato il bollo, se hai pagato l'assicurazione e se hai fatto il collaudo. Lo sanno subito e senza fermarti.


 Se non paghi il bollo e ti fermano, la macchina va a finire dritta dal demolitore.


 Le tasse sono al 33%.  
  
[![Io e John](http://farm4.static.flickr.com/3187/2657448891_9f67a63130_m.jpg)](http://www.flickr.com/photos/kmen-org/2657448891/ "Io e John di karimblog, su Flickr")  
  
  


