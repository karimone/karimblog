Title: Spotorno
Date: 2005-07-31
Category: altro
Slug: spotorno
Author: Karim N Gorjux
Summary: La spiaggia di pietraLa città di Spotorno è bella, ma ha una spiaggia orrenda. E' quasi impossibile entrare in acqua senza recitare un rosario. Sono quasi le 9 del mattino e mi[...]

La spiaggia di pietra  
  
La città di Spotorno è bella, ma ha una spiaggia orrenda. E' quasi impossibile entrare in acqua senza recitare un rosario. Sono quasi le 9 del mattino e mi trovo nella hall dell'albergo collegato in internet via wifi. Non ho capito il perchè, ma non posso collegarmi direttamente dalla camera, l'unica rete accessibile tra quelle disponibili si trova nella hall. Penso di aver ricevuto una maledizione tecnologica, non me ne va mai bene una.


