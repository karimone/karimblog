Title: Sveiki! Il portale sulla Lituania
Date: 2008-09-03
Category: Lituania
Slug: sveiki-il-portale-sulla-lituania
Author: Karim N Gorjux
Summary: Ho registrato il dominio sveiki.net, per chi non lo sapesse sveiki significa "Salve", per ora all'indirizzo principale ho messo un'installazione standard del CMS Joomla, ma all'indirizzo forum.sveiki.net ho già messo il forum[...]

Ho registrato il dominio sveiki.net, per chi non lo sapesse sveiki significa "Salve", per ora all'indirizzo principale ho messo un'installazione standard del CMS Joomla, ma all'indirizzo [forum.sveiki.net](http://forum.sveiki.net "Il forum di Sveiki") ho già messo il forum per chi volesse iniziare a partecipare alle discussioni.


 Il progetto è ha a lungo termine, richiede la partecipazione di persone che vogliano contribuire a creare un luogo comune dove dibattere e un portale dove pubblicare e/o leggere informazioni sulla Lituania.


 Divertiamoci!

