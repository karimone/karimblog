Title: La miglior cosa è prendere il meglio dall'Italia e dal paese che ti ospita
Date: 2010-05-15
Category: Lituania
Slug: la-miglior-cosa-è-prendere-il-meglio-dallitalia-e-dal-paese-che-ti-ospita
Author: Karim N Gorjux
Summary: Greta è passata dalla varicella all'otite. Praticamente è sempre in casa, e dato che io in casa ci vivo e ci lavoro, le cose si fanno un po' difficili.

[![](http://farm2.static.flickr.com/1352/4607998349_75a49f9838_m.jpg "Greta")](http://www.flickr.com/photos/kmen-org/4607998349/)Greta è passata dalla varicella all'otite. Praticamente è sempre in casa, e dato che io in casa ci vivo e ci lavoro, le cose si fanno un po' difficili.


 Non tutto il male vien per nuocere, dato che la macchina la usiamo per lo più per portare Greta all'asilo, ci siamo finalmente decisi a cambiare le targhe alla macchina. Niente più assicurazione stratosferica e bollo da rapina, si passa alla gestione facilitata "Made in Lithuania", prossimamente scriverò come funziona il passaggio di targhe e come si trasferisce una macchina dall'Italia alla Lituania.  
  
Questo periodo è quindi stato un po' particolare, Greta malata, macchina da cambiare, meno palestra e anche il lavoro ha subito qualche modifica. Intanto confermo [la situazione della Lituania con le tasse](http://www.karimblog.net/2009/01/20/misura-anti-crisi-del-governo-lituano-le-tasse-per-i-piccoli-imprenditori-dal-20-al-50/) che fatti due calcoli mi ha portato a chiudere la mia attività con sede in Lituania. Se l'Italia ha un rapporto tasse/servizi assolutamente squilibrato, la Lituania, dopo questa manovra, ha portato il rapporto a valori incredibilmente squilibrati.


 Giocando a calcio, è capitato che mi sono fatto male più volte, le cure mediche fornite dallo stato sono sempre a pagamento, ma la professionalità lascia al quanto a desiderare. Qui, esattamente come in Italia, se si vuole un servizio decente bisogna pagare e andare dal privato, in Italia però il servizio pubblico gratuito è decisamente migliore di quello lituano.


 La morale della favola è che mi darò da fare per prendere il meglio dei due paesi, con la Lituania lo sto già facendo, ora è giunto il momento di prendere il meglio dall'Italia.


