Title: Crisi, lavoro, opportunità e motivazione. Riflessioni di un informatico che si diverte lavorando
Date: 2009-11-03
Category: Lituania
Slug: crisi-lavoro-opportunità-e-motivazione-riflessioni-di-un-informatico-che-si-diverte-lavorando
Author: Karim N Gorjux
Summary: La crisi, il lavoro e le opportunità dei momenti bui. Ho scritto questo articolo raccontanto qualcosa del mio lavoro e riflettendo sulla crisi e le motivazioni e le opportunità che ne possono[...]

[![Weiji](http://www.karimblog.net/wp-content/uploads/2009/11/weiji-300x214.jpg "Weiji")](http://www.karimblog.net/wp-content/uploads/2009/11/weiji.jpg)La crisi, il lavoro e le opportunità dei momenti bui. Ho scritto questo articolo raccontanto qualcosa del mio lavoro e riflettendo sulla crisi e le motivazioni e le opportunità che ne possono nascere. Se sei demotivato o pensi che la crisi sia l'origine di tutti i mali, forse questo mio articolo può esserti d'aiuto.  
  
Lavoro con il computer e ho solo clienti italiani; era così in Italia e lo faccio tutt'ora che vivo in Lituania. Quando incontro qualche italiano qui a Klaipeda, inesorabilmente, tra i tanti discorsi che ho già fatto migliaia di volte, mi chiedono che lavoro faccio. Quando capiscono che guadagno in Euro e spendo in Litas, mi guardano con occhi colmi di ammirazione.


 **Ma è apparentemente tutto così semplice?** Hai ragione, è una domanda retorica.


 I problemi da affrontare sono tanti, in primo luogo bisogna trovare dei nuovi clienti e dato che vivo a 2500km circa dal mio target non è così facile. I miei rudimenti di marketing iniziarono con le telefonate, se ci penso rido ancora adesso, ma da qualche parte dovevo pur iniziare. Ora invece mi appoggio molto al passaparola che mi ha tenuto occupato vari mesi, adesso che ho meno clienti e meno lavoro da sbrigare mi sto dedicando ampiamente al marketing attraverso il mio sito web.


 Un servizio o prodotto, per valido che sia, deve avere un buon marketing di supporto altrimenti è come mettere le insegne luminose in un paese di ciechi, non porta da nessuna parte.


 Il secondo passo difficile da gestire è la burocrazia nel paese straniero. Io sono arrivato in Lituania in un momento difficile, cosa avrei dovuto fare in passato lo so e ne ho anche parlato in vari articoli, ma rivangare il passato è da disperati senza futuro. Come diceva Walt Disney **continua ad andare avanti.** Dopo svariati mesi non ho ancora ben chiaro quante tasse dovrò pagare e quando, i vari uffici sono discordanti e poco preparati perché La Lituania affronta le cose da grandi senza esperienza e senza mezzi pagandone le conseguenze. Di chi è la colpa? La colpa è della crisi.


 La crisi [ha mandato a gambe all'aria molti](http://www.karimblog.net/2009/06/05/nei-paesi-baltici-puoi-vedere-molte-di-macchine-di-lusso-ma-non-e-tutto-oro-quello-che-luccica/) dei sogni nel cassetto lituani, la crisi ha messo e continua a mettere a dura prova i paesi con un'economia solida come la nostra cara Italia. La crisi ho imparato ad apprezzarla perché in tutto ciò vedo delle opportunità e soprattutto una lezione; guardandomi attorno e un po' per esperienza personle, ho imparato che **vivere sugli allori non è saggio, il posto fisso non esiste**, le cose non sono immutabili e sempre come vorremmo, gli imprevisti non sono solo un mazzo di carte nella scatola del Monopoli, gli imprevisti sono sempre in agguato e bisogna accoglierli come maestri di vita e di saggezza.


 **Non pretendiamo che le cose cambino se facciamo sempre la stessa cosa.** La crisi è la migliore benedizione che può arrivare a persone e Paesi, perché la crisi porta ad una trasformazione. 

 
>   
>  La creatività nasce dalle difficoltà, nello stesso modo che il giorno nasce dalla notte oscura.  
>   
>  E' dalla crisi che nasce l'inventiva, le scoperte e le grandi strategie.  
>   
>  Chi supera la crisi, supera se stesso senza essere superato.  
>   
>  Chi attribuisce alla crisi i propri insucessi e disagi, inibisce il proprio talento e ha più rispetto dei problemi che delle soluzioni.  
>   
>  La vera crisi è la crisi dell'incompetenza. La convenienza delle persone e dei Paesi è di trovare soluzioni e vie di uscita.  
>   
>  Senza crisi non ci sono sfide, e **senza sfida la vita è una routine**, una lenta agonia.  
>   
>  Senza crisi non ci sono meriti.  
>   
>  E' dalla crisi che affiora il meglio di ciascuno, poiché senza crisi ogni vento é una carezza.  
>   
>  Parlare della crisi significa promuoverla e non nominarla vuol dire esaltare il conformismo. Invece di ciò dobbiamo lavorare duro. Terminiamo definitivamente con l'unica crisi che ci minaccia, cioé la tragedia di non voler lottare per superarla  
> 

 Non ne sono sicuro, ma questa citazione è attribuita ad Albert Einstein, in queste poche righe c'è molta saggezza e soprattutto molta motivazione per andare avanti.


 Molti mi chiedono come sta andando il mio lavoro, (io in realtà non lo chiamo lavoro perché ciò che faccio lo facevo anche quando aveva 12 anni ed ero chinato, nelle buie notti d'inverno, sul mio glorioso [Amiga 4000](http://it.wikipedia.org/wiki/Amiga_4000)). Semplicemente vado avanti con tutte le difficoltà che ne conseguono, questo è un periodo di magra? Meglio, **posso dedicarmi a tutte le cose che sono sempre stato costretto a rimandare**, sto leggendo libri, sto studiando alcune molto interessanti di informatica, ma soprattutto sto lavorando al mio sito che quando sarà pronto segnalerò sul mio blog.


 Concludo parlando del posto fisso tanto citato negli ultimi giorni. Il posto fisso esiste, è una realtà, **basta andare in qualsiasi cimitero e vedrete tutti i posti fissi che volete**. Con questo non voglio dire che tutti devono essere imprenditori e creare un loro business, ma voglio dire che bisogna essere imprenditori di se stessi perchè "imprendere" è una parola nata dall'unio di due altre parole: **movimento e presa**. Il movimento è inteso come azione a fare qualcosa e il prendere si riferisce alle occasioni che tutti i giorni la vita propone.


 La morale è semplice: **il movimento, sia fisico che mentale, è sinonimo di vita!** Per stare immobili c'è sempre tempo al cimitero.


 PS: Hai capito cosa significa l'immagine all'inizio dell'articolo?

