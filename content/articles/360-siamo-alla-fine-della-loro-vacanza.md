Title: Siamo alla fine della Loro vacanza
Date: 2007-08-25
Category: Lituania
Slug: siamo-alla-fine-della-loro-vacanza
Author: Karim N Gorjux
Summary: Ieri ho guidato per circa 300km. Siamo partiti da Centallo per finire a Spotorno, poi Ventimiglia e infine ritorno a casa passando dal colle di Tenda. Mare e montagna, caldo e freddo.[...]

Ieri ho guidato per circa 300km. Siamo partiti da Centallo per finire a Spotorno, poi Ventimiglia e infine ritorno a casa passando dal colle di Tenda. Mare e montagna, caldo e freddo. Spaghetti allo scoglio, trofie al pesto, gelato. I miei suoceri sono contenti, l'Italia è un gran bel paese. Le nostre montagne sono fantastiche, il nostro mare è bellissimo.


 Oggi è in programma una giornata tranquilla: giretto a Cuneo, visita da mia zia. Domani partenza per Milano Malpensa, destinazione: Vilnius. Vera destinazione: Telsai, cuore della Lituania.


 Sono sicuro che i genitori di Rita torneranno, forse non la prossima estate, ma quando decideranno di tornare mi organizzerò per il meglio: affitterò un camper. :-)

