Title: In partenza. Rientro in Italia per metà Aprile
Date: 2011-03-19
Category: Lituania
Slug: in-partenza-rientro-in-italia-per-metà-aprile
Author: Karim N Gorjux
Summary: Si, tra circa tre settimane partirò da Klaipeda per tornare a Cuneo, in Italia. La decisione non è stata sofferta, ma molto ponderata. Perché rientrare in Italia? Perché non rimanere qui o[...]

[![](http://www.karimblog.net/wp-content/uploads/2011/03/new-life-245x300.jpg "Nuova vita")](http://www.karimblog.net/wp-content/uploads/2011/03/new-life.jpg)Si, tra circa tre settimane partirò da Klaipeda per tornare a Cuneo, in Italia. La decisione non è stata sofferta, ma molto ponderata. Perché rientrare in Italia? Perché non rimanere qui o andare in un altro paese?

 Di rimanere qui non ne abbiamo intenzione. La Lituania, ora come ora, non è il paese che si può permettere di dare un futuro a me o ai miei figli specialmente per noi che viviamo a Klaipeda, forse a Vilnius, la capitale, sarebbe diverso? Non ho idea, ma qui in Lituania, se si vuole essere felici la strada è tutta in salita.


 Personalmente in Italia abbiamo più possibilità di quante ne abbiamo qui in Lituania, inoltre ho molti parenti più vicini che ci permetterebbero di gestirci un po' meglio con i bambini mentre qui siamo da soli, senza parenti e con due bambini da gestirci. Nel nostro caso quindi, abbiamo pensato che sia meglio così, soprattutto nel lungo periodo. Forse nel tuo sembra una pazzia, ma nel nostro è decisamente meglio tornare in Italia.  
  
Lasciare la Lituania dopo tanto tempo, (in pratica sono a Klaipeda da 5 anni dopo due esperienze di residenza), mi lascia un po' malinconico. Klaipeda è una piccola città che anche se conta 180 mila abitanti in realtà sembra una città di campagna un po' allargata.   
Mi mancheranno molte cose che ho imparato ad apprezzare in questi anni: il mare, le passeggiate nella vecchia città, le partite a calcio con i lituani, i locali con lo sproporzionato rapporto uomini/donne, i sorrisi dei fiori lituani, la lingua lituana stessa... penso che mi mancherà persino [l'akropolis](http://www.akropolis.lt/) che è uno dei centri commerciali più belli che ho visto e che ho imparato subito ad odiare dato che **è l'unica attrazione invernale a Klaipeda**.


 Delle tante belle cose che lascerò della Lituania ci sono dei lati negativi con cui convivo tutt'ora dato che sono qui, ma sono felice di lasciarmi alle spalle.


 E' molto difficile vedere persone felici attorno a te mentre vivi qui a Klaipeda. In tanti anni **non ho mai sentito una persona fischiettare** e gli unici sorrisi che ho visto sono di donne, ma non sono sinonimo di felicità. E' vero che la felicità è in ognuno di noi, ma ricordo ancora come nei Caraibi la gente fosse estremamente povera, ma incredibilmente sorridente e felice. Forse è il sole che aiuta?

 Un paio di settimane fa è venuto un mio amico da Caraglio (Cuneo) a trovarmi, a parte il fatto che è stato l'unico ad avere il coraggio di farlo in tre anni che sono qui permanente, ma mi ha fatto subito notare delle cose che la lontananza con l'Italia mi ha fatto dimenticare. I Lituani non si aggregano facilmente, puoi vedere degli agglomerati di case e di appartamenti, ma **nessun vero luogo di ritrovo che faccia in modo che le persone si frequentino**. Non ci sono bar, chiese, bocciofile, una piazza o qualsiasi altro tipo di cosa che permetta di fare comunità.  
Sembra che i paesi si formino per interesse del singolo a non essere completamente da solo, ma poi effettivamente c'è un senso di solitudine e la mancanza di illuminazione rende tutto più cupo.


 A Klaipeda che conta ben 180.000 anime, c'è un minimo di ritrovo nel centro, ma il più lo fanno i centri commerciali che da noi tendono ad essere in periferia, mentre qui servono a riempire il vuoto del centro. In periferia invece non c'è nulla, ma proprio nulla che porti ad una vita "di paese" come la intendiamo noi.


 Prendiamo ad esempio "Don Camillo", **la vita del piccolo paese dove tutti si conoscono e si frequentano nel bene o nel male, qui non esiste**. Esiste qualcosa di simile nei giovani che si aggregano, ma i loro discorsi sono per lo più rivolti alla felicità futura quando andranno via dalla Lituania subito dopo gli studi.


 Il paese di mia moglie, Telsiai, conta ben 30.000 abitanti ovvero la metà di Cuneo, eppure non c'è nulla. Non c'è un cinema, un teatro, un oratorio, un ritrovo comune, il mercato fisso un giorno alla settimana, i panettieri o l'edicola. In sostanza la Lituania è molto più provinciale dell'Italia se non più campagnola, ma ognuno è chiuso in sé stesso e il massimo della vita è un supermarket.


 Conoscendo i lituani, non mi stupisco di questo modo di vivere solitario e xenofobo. Anzi, da un lato mi aiuta ancora di più a comprendere come mai ci sia quel muro verso gli stranieri che in tanti anni non mi ha portato mai ad avere un'amicizia con un lituano.


 Qualche giorno fa scrivevo con una ragazza di 20 anni di un paesino piccolino vicino a Klaipeda. La sensazione è stata che lei **aspetta la fine della settimana pur di divertirsi un po' in Klaipeda in qualche locale e lasciarsi alle spalle la tristezza del piccolo paesino**. E' un po' triste, ma è così. Vedere la Lituania solo da turisti e per di più d'estate come fanno molte delle persone che leggono il mio blog, non rende l'idea di cosa sia veramente la Lituania. La Lituania non è solo Vilnius o gli euro che qui sembrano oro rispetto alla moneta locale, ci va molto tempo per capire i lituani e la Lituania.


 A me la Lituania miancherà, ma ho la cinica ed egoistica intenzione di **frequentare la Lituania prendendo solo ciò che c'è di bello** che allo stato ottimale significa viverci Giugno e Luglio da turista e godendosi il poco d'estate che questo paese ha da offrire. Senza pensieri.


