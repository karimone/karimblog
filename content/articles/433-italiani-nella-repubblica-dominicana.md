Title: Italiani nella repubblica dominicana
Date: 2008-03-31
Category: altro
Slug: italiani-nella-repubblica-dominicana
Author: Karim N Gorjux
Summary: Di italiani che vivono qui ne ho trovati di vari tipi, di cui tutti generalmente motivati da tre cose: il carro dei buoi, l'esilio volontario, il freddo.

Di italiani che vivono qui ne ho trovati di vari tipi, di cui tutti generalmente motivati da tre cose: il carro dei buoi, l'esilio volontario, il freddo.


 Alle volte esistono altri motivi, ma i tre che ho menzionato sono onnipresenti in percentuali diverse sepur minime. Raramente il carro dei buoi non c'entra.


 La prima parola che mi viene in mente pensando a Boca de Yuma è **fancazzismo**, subito dopo l'associazione, il mio stupore per la vasta presenza di italiani in un paesino di 1500 abitanti viene meno. Di italiani ne ho conosciuti già una decina e ne mancano ancora! Dalle mie stime, sembra che ce ne siano almeno il doppio.   
  
L'età media dell'italiano supera i 50. I puttanieri pensionati vanno per la maggioranza, poi c'è chi lavora qui, c'è chi vuole dimenticare l'Italia, c'è invece chi vuole fare assolutamente nulla, nemmeno trombare. Alcune coppie pensionate **svernano** qui sei mesi l'anno, investono la liquidazione in immobili (e proprio per questo i prezzi delle case in vendita poi aumentano in posti come nel sud della Spagna ad esempio)e si godono la vita senza troppe pretese. 

 Chi ha deciso di stare qui a **fare niente** a mio parere è chi ha fatto la scelta migliore. Chi tenta disperatamente di trarre uno stipendio vendendo qualcosa al dominicano mi ricorda tanto l'italiano in Lituania che si mangiava il parmigiano e il prosciutto che invano tentava di vendere ai lituani. 

 I dominicani hanno già pochi soldi per loro, non ci va molto a capire che qui si possono fare soldi solo con il **turismo** e chi li poteva fare, li sta già facendo. Forse si può fare qualche soldo sfruttando la mano d'opera e le materie prime, ma i dominicani sono inaffidabili e anche la repubblica dominicana lo è. Meglio, a mio parere, lasciare perdere.


 C'è un vecchietto di 80 anni che viene qui a svernare e a consumare la sua pensione in **donne**. Quello che non spende in riscaldamento in Italia, lo spende per riscaldarsi tra le lenzuola qui a Boca de Yuma. I costi si aggirano intorno ai 10 o 20 Euro a consumazione, malattie incluse, ma a 80 anni è lecito sbattersene altamente delle malattie. A cosa servirebbe? Le preoccupazioni bisogna lasciarle ai giovani.


