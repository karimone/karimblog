Title: Finalmente non sono il solo
Date: 2005-06-02
Category: Lituania
Slug: finalmente-non-sono-il-solo
Author: Karim N Gorjux
Summary: Escludendo Davide, sono contento di non essere il solo a provare la sensazione demoralizzante del rientro in Italia. Il sentirsi scazzati e' la normale conseguenza che rincoglionisce per un mese ed oltre[...]

Escludendo Davide, sono contento di non essere il solo a provare la sensazione demoralizzante del rientro in Italia. Il sentirsi *scazzati* e' la normale conseguenza che rincoglionisce per un mese ed oltre al rientro in patria. Tutto e' diverso, immensamente noioso e monotono, mi spiace, ma non ci sono molte possibilita': o cosi' o pomi'.


 Il viscido e' andato a festeggiare il 2 Giugno a Vilnius. Io rimango qui a Klaipeda ad approfondire le mie conoscenze di SQL. Intanto comunico una possibile data di rientro che dovrebbe essere il **19 Giugno**. Non sono ancora sicuro, ma quando avro' il biglietto in mano lo comunichero' sul sito. La mia permanenza a Centallo sara' al quanto breve, non piu' di 2 o 3 giorni. Giusto il tempo di salutare la nonna e andare a [Demonte](http://www.campingdemonte.it) a schiavizzarmi. :thumbdown\_tb:

 In Italia il sole e il caldo sono ormai all'ordine del giorno, qui si gira con la giaccavento. Ieri e' approdata una nave da crociera al porto e ne hanno parlato sia la radio che i telegiornali, sono cose che mi fanno sorridere e capire come la Lituania sia un grande paesone di 3 milioni di abitanti. 

 Gli ultimi due giorni di Maggio li ho passati in spiaggia, ho preso il sole e ho anche fatto il bagno nel Baltico, l'acqua era gelida e poco salata, le onde non troppo grandi. Le spiaggie sono particolari, non esistono ombrelloni o sdrai e' tutto pubblico e quindi invaso da una marea di gente. Domenica sono stato a Palanga che e' la Rimini lituana, era il 29 Maggio, c'era talmente tanta gente che sembrava fosse Ferragosto. Rita mi ha spiegato che in alta stagione e' impossibile trovare posto seduti, l'unico neo, secondo me, riguarda il rispetto dell'ambiente. I lituani fanno schifo da quel punto di vista :nono\_tb:

