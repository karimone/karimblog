Title: Gioco con una piccola squadra di calcio amatoriale di Klaipeda
Date: 2009-12-03
Category: Lituania
Slug: gioco-con-una-piccola-squadra-di-calcio-amatoriale-di-klaipeda
Author: Karim N Gorjux
Summary: E' ormai 1 mese che gioco indoor a calcetto con una squadra amatoriale di calcio di Klaipeda. Tutte le domeniche ci troviamo verso le 16 alla palestra che affianca lo stadio comunale,[...]

[![](http://farm3.static.flickr.com/2506/4155559884_ebb992a0ba_m.jpg "Passa la palla!")](http://www.flickr.com/photos/kmen-org/4155559884/)E' ormai 1 mese che gioco *indoor* a calcetto con una squadra amatoriale di calcio di Klaipeda. Tutte le domeniche ci troviamo verso le 16 alla palestra che affianca lo stadio comunale, per intenderci è lo stadio dove gioca [l'Atlantas](http://www.atlantas.lt/naujienos/ "Pagina ufficiale della squadra di calcio Atlantas di Klaipeda")

 La squadra dove gioco si chiama FK Klaipeda e i giovani ragazzi fondatori sono sia russi che lituani. Chi mi ha introdotto è un ragazzo libanese con cui giocavo quest'estate. Non mi trovo male, ma il giudizio non è obiettivo, sono talmente contento di giocare a calcio almeno una volta alla settimana che non me ne frega nulla se sono simpatici o meno.  
   
**A parte l'ultima Domenica, dove mi sono preso un calcione al piede** che mi fa male ancora oggi, mi sono sempre divertito. A volte non capisco nulla di quello che dicono perché gridano tra di loro in russo mischiato al lituano, ma io me ne frego, gioco e basta.


 **Il livello di gioco è conseguenza della diffusione come primo sport nazionale del basket**. C'è qualche ragazzo che è davvero in gamba, ha una buona tecnica e gioca bene, ma la maggior parte di loro prende la palla, guarda in basso e attacca. Non ci sono movimenti senza palla, scatti, triangolazioni, ma soprattutto non c'è gioco di prima intenzione. C'è un ragazzo, piccolo, tarchiato, con la faccia che ricorda un po' Alvaro Vitali mischiato ad un pizzico di [Sloth dei Goonies](http://lacrudarealidad.blogsome.com/uploads/lacrudarealidad/sloth-goonies.jpg "Foto di Sloth dei Goonies"). Lui è il principe della tattica "sfondamento a testa bassa".


 Il piede mi fa davvero male, il giorno dopo l'infortunio pensavo persino di essermelo rotto, ma ora riesco a camminarci abbastanza bene sopra, se riesco domani vado a farmi un giro in palestra che è già da troppo che manco. Il fatto di passare queste giornate tristi e buie sempre davanti al computer non mi aiuta davvero, in questi frangenti è meglio saper dedicare il proprio tempo alla cura di se stessi.


 Domenica giocherò di nuovo (piede permettendo) e poi vedremo se mi faranno giocare anche al campionato di calcio che qui, dovrebbe incominciare a Marzo. Sinceramente non mi dispiacerebbe giocare, ma [le partite di quest'estate erano insuperabili](http://www.karimblog.net/2009/07/10/lo-sport-il-cibo-e-i-pensieri-sono-i-segreti-per-una-vita-felice-e-piena-di-soddisfazioni/ "Vita felice in Lituania (ma solo d'estate)").


