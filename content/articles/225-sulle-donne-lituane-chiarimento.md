Title: Sulle donne lituane: chiarimento
Date: 2006-10-03
Category: Lituania
Slug: sulle-donne-lituane-chiarimento
Author: Karim N Gorjux
Summary: Non sputo sul piatto dove mangio, scrivo esattamente le mie esperienze e soprattutto scrivo dopo aver avuto conferma da più parti che il comportamento di mia moglie è una costante comune delle[...]

Non sputo sul piatto dove mangio, scrivo esattamente **le mie esperienze** e soprattutto scrivo dopo aver avuto conferma da più parti che il comportamento di mia moglie è una costante comune delle donne baltiche. Non voglio lamentarmi, osannare o criticare, ma mi limito a **documentare**. E' ovvio che poi essendo il blog mio, la minima opinione ce la metto. Non sono un giornalista.


 Prima di scrivere sul blog informazioni utili a chi vuole aggiungersi al club dei disperati (in senso buono) alla ricerca di moglie baltica, aspetto di essere sicuro di non scrivere una stronzata. Sulle donne ho scritto, ma non esiste la donna perfetta quindi se siamo qua a 2000km da casa o continuamo a restare con una donna baltica in qualsiasi altra parte del mondo significa che **le note positive sono più di quelle negative**

