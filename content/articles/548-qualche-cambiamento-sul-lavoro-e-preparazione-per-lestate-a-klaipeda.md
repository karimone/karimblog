Title: Qualche cambiamento sul lavoro e preparazione per l'estate a Klaipeda
Date: 2009-06-12
Category: Lituania
Slug: qualche-cambiamento-sul-lavoro-e-preparazione-per-lestate-a-klaipeda
Author: Karim N Gorjux
Summary: Sono un po' stanco di stare seduto davanti al computer, ormai le mie giornate sono computer, calcio e passeggiate. Fortunatamente riesco a giocare a calcio almeno tre volte alla settimana e questo[...]

[![](http://farm3.static.flickr.com/2432/3594060513_233c9ebcf0_m.jpg "Spiaggia di Palanga")](http://www.flickr.com/photos/39283264@N00/3594060513/)Sono un po' stanco di stare seduto davanti al computer, ormai le mie giornate sono computer, calcio e passeggiate. Fortunatamente riesco a giocare a calcio almeno tre volte alla settimana e questo è diventato il mio punto cardine della vita sociale in Lituania.  
  
I ragazzi Erasmus sono partiti, il corso di lituano è finito, Fernando è andato via e, alla fine della fiera, a parte qualche piccola eccezione periodica, chi rimane sono sempre i soliti: io e [Mirko](http://blog.mirkobarreca.net).   
Rita ha finito il suo terzo anno di università ed è in vacanza, molto probabilmente non ci muoveremo da Klaipeda se non qualche viaggio turistico baltico, ma non mi lamento. Ho il mare e mi si prospetta davanti un'estate dalle temperature primaverili. Perché dovrei lamentarmi?

 Intanto mi sto dedicando a fare dei consistenti cambiamenti al mio lavoro. Per prima cosa **ho iniziato a delegare** parte dei miei progetti creando una vero e proprio ufficio virtuale, manco a dirlo, ho conosciuto [questa persona](http://www.digit-ale.it "Il sito di Alessandro") proprio grazie al blog. Sono molto fiducioso e sono sicuro che questa nuova avventura porterà a grandi soddisfazioni.  
Delegare è molto importante, fare tutto da soli può sembrare una soluzione ottimale perché tieni per te tutti i guadagni e prendi da solo tutte le decisioni, ma delegare ti permette di concentrarti e ottenere risultati migliori. Nell'informatica poi, il delegare dovrebbe essere una sana abitudine, fare tutto da soli porta a risultati scarsi e molta frustrazione.


 Sempre riguardo al lavoro, voglio segnalare un blog molto interessante che leggo ormai da qualche settimana: [efficacemente.it](http://www.efficacemente.com/ "Il blog di efficamente.it"). Qualsiasi tipo di lavoro tu svolga, ti consiglio di leggere questo blog da cima a fondo perché è ricco di saggezza per migliorare il proprio lavoro, le proprie abitudini e i risultati.


 Se poi sei pigro e tendi a procrastinare come me, ti suggerisco di leggere questa pagina: [Come superare la procrastinazione](http://www.ilmiopsicologo.it/pagine/come_superare_la_procrastinazione.aspx)

 PS: Avrei voluto scrivere delle elezioni, di Mr. B, ma soprattutto della fantastica legge sulle intercettazioni, ma perché farsi il sangue amaro?

