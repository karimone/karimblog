Title: Di nuovo papà, ben arrivato Thomas
Date: 2010-10-09
Category: altro
Slug: di-nuovo-papà-ben-arrivato-thomas
Author: Karim N Gorjux
Summary: In questi giorni non ho scritto nulla, ma ho tutt'altro da fare. La notte del 4 Ottobre è nato Thomas, un maschietto di 4kg e 56cm. Il parto è stato lungo e[...]

[![](http://www.karimblog.net/wp-content/uploads/2010/10/noi_tre.jpg "Noi tre")](http://www.karimblog.net/wp-content/uploads/2010/10/noi_tre.jpg)In questi giorni non ho scritto nulla, ma ho tutt'altro da fare. La notte del 4 Ottobre è nato Thomas, un maschietto di 4kg e 56cm. Il parto è stato lungo e difficile, ma sembra volgere tutto per il meglio.


 L'esperienza con la sanità lituana è sempre abbastanza tragica, gli ospedali rispecchiano esattamente le scarse possibilità finanziarie del governo e non è raro trovare apparecchiature che vengono donate da altri paesi. Ad esempio il cardiofrequenzimetro usato per monitorare il battito cardiaco del bambino all'interno della pancia della mamma era una gentile donazione della Confederazione Svizzera.  
  
Al momento sia il bimbo che la moglie sono ancora all'ospedale e se tutto va bene dovrebbero ritornare Lunedì, io nel mentre mi occupo della casa, di Greta e pure del lavoro e il blog ne risente.


 Un grazie a [Flavio](http://www.terracelodge.com) per l'editing della foto

