Title: Il Tram.
Date: 2005-12-16
Category: Lituania
Slug: il-tram
Author: Karim N Gorjux
Summary: Si è vero, non scrivo più tanto. Ieri ho beccato Max su skype e mi ha fatto il culo: "non scrivi più niente!", mi ha detto; beh in effetti la cosa che[...]

Si è vero, non scrivo più tanto. Ieri ho beccato Max su skype e mi ha fatto il culo: "non scrivi più niente!", mi ha detto; beh in effetti la cosa che mi trattiene di più è la 56k e soprattutto il tempo...  
  
Ieri sono stato a Torino, il viaggio mi ha stancato, ma la città mi ha demoralizzato. Mi chiedo come facciano i Torinesi a vivere in una città del genere, grigia, sporca, caotica. Il tutto è così artificiale, mi sembrava quasi di essere in quel romanzo di Asimov in cui la gente viveva sottoterra, lo avete letto? (Non ricordo il titolo..)

 Quando ero sul tram e vedevo tutti quei cantieri, tutto quel casino, mi è mancata Klaipeda. Sui bus di Klaipeda incrociavo lo sguardo dei bambini che rispondevano sempre al mio sorriso. Ricordo quel bambino; era piccolo, magrolino, con i capelli castano chiaro tutti in fila ordinati, quasi come se fossero finti. Gli enormi occhi azzurri risplendevano su quel visettino bianco porcellana e mi davano l'impressione che fosse un bambino vivace, ma allo stesso tempo molto gentile. Se ne stava seduto in mezzo ai grandi con lo sguardo sperduto, sulle gambe teneva   
una cartella in cartone rattoppata con il nastro adesivo da pacchi.  
   
Rispose al mio sorriso e al mio occhiolino, era un bimbo molto simpatico. Non mi parlò perché si rese conto che non ero del posto ed è proprio in quei momenti che mi dispiace non sapere il Lituano, purtroppo non potevo dirgli niente e la situazione non era delle migliori, il bus era pieno di gente. 

 Ricordo ancora il momento in cui scese, la fermata era giusto quella prima di casa mia, la porta del bus si apriva su un parco; era buio. Ricordo ancora il bimbo che con la sua cartella di cartone si avviava per il suo sentiero, da solo, in mezzo ai verdi prati che circondano la città di Klaipeda.


 Ma io sono sul tram a Torino e oltre che non vedere la natura, non vedo nemmeno un bambino.  
  
  
  
  
adsense  
  
  
  


