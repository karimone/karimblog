Title: C'è una cosa che se in un paese manca non la si può importare. E' la felicità
Date: 2011-03-22
Category: Lituania
Slug: cè-una-cosa-che-se-in-un-paese-manca-non-la-si-può-importare-e-la-felicità
Author: Karim N Gorjux
Summary: Depressione, la si sente con il freddo, il cielo color grigio e il vento freddo tagliente che trasforma una passeggiata in un calvario. La depressione è la vera e unica piaga di[...]

[![](http://www.karimblog.net/wp-content/uploads/2011/03/depressione.jpeg "depressione")](http://www.karimblog.net/wp-content/uploads/2011/03/depressione.jpeg)Depressione, la si sente con il freddo, il cielo color grigio e il vento freddo tagliente che trasforma una passeggiata in un calvario. La depressione è la vera e unica piaga di questo paese e la si sente, a pelle.


 La radio più famosa della Lituania si chiama M-1 che si pronuncia letteralmente "em vienas", in pratica è la "Radio Deejay" lituana con programmi storici seguiti un po' da tutti in tutte le città. Al mattino in particolare è possibile seguire un programma sulla felicità dove gli ascoltatori intervengono raccontando qualcosa di felice che è successo loro. C'è quello che racconta che ha vinto al Lotto, quello che è felice di essere diventato genitore o essersi innamorato, c'è chi ha trovato l'amante o quello che sta aspettando un parente all'aeroporto e che non vedeva da tanto tempo.  
  
Ieri ha chiamato una ragazza perché era felice di non essersi suicidata. Questa ragazza aveva praticamente la testa incravattata ad un ramo, ma all'ultimo momento qualcuno l'ha aiutata a reagire e chiamava felice perché non si era abbandonata al gesto estremo. Il conduttore del programma non sapeva cosa dire. Annuiva, non faceva domande, era completamente spiazzato dalla chiamata pur sapendo che il proposito di un programma del genere è infondere buon umore ad un popolo che si suicida sempre di più e che affida le sue ore di gioia al più potente depressivo legalizzato in circolazione: "l'alcol".


 Questa continua tristezza che sfocia in gesti estremi o in lento suicidio è un lato della Lituania che un turista non può assolutamente capire. Se non vivi qui per mesi o meglio anni, non puoi capire cosa vive o meglio "affronta" quotidianamente il popolo lituano. Per capire, e non giudicare, bisogna vivere con queste persone a stretto contatto; bisogna prendere i bus con loro, andare nei loro quartieri, frequentare i loro posti, sentire i loro discorsi, sentire come vivono la vita e l'amicizia, a cosa danno importanza e conoscere la loro storia per capire come vedono la vita le differenti generazioni.


 **In un paese dove 8 mesi l'anno fa freddo e per la restante parte dell'anno si spera che non piova**, per divertirsi, svagarsi o comunque coltivare i propri interessi è necessario avere i soldi. Detto fatto, la gente lavora, ma la paga e bassa e quindi si passa il più del tempo a lavorare per qualche spicciolo che purtroppo non basta mai dato che i prezzi non sono allineati agli stipendi. Il risultato tangibile è un cerchio ristretto, molto esclusivo di persone che si può permettere di fare 3 giorni a Milano per comprarsi dei vestiti ultimo grido ed un club affollato di persone che aspetta gli aiuti dallo stato per mettere insieme il pranzo con la cena.


 
> *"Jean-Loup pensò che le priorità della vita, tutto sommato, sono abbastanza semplici e ripetitive, e in pochi posti al mondo come quello era possibile quantificarle. La caccia al denaro al primo posto. Alcuni ce l'hanno e tutti gli altri lo vogliono. Semplice. Un luogo comune diviene tale per la dose di verità che cela al suo interno. **Forse il denaro non dà la felicità**, ma aspettando che la felicità arrivi è un bel modo per passare il tempo."*  
> *Giorgio Faletti in "Io uccido"*
> 
> 

 **Chi può scappa e lo fa in fretta**. Essere costretti ad abbandonare il proprio paese per inseguire i propri sogni è triste e non tutti sono in grado di sopportarlo. Forse su questo i lituani sono avvantaggiati perché la mia impressione è che** i lituani siano degli stranieri nel loro stesso paese** e il loro continuo nazionalismo ne è la dimostrazione più lampante. La Lituania conta sulla carta circa 3 milioni e mezzo di abitanti, ma entro la fine dell'anno, grazie alla presidentessa [Dalia Grybauskaitė](http://it.wikipedia.org/wiki/Dalia_Grybauskait%C4%97) un censimento nazionale aggiornerà questo dato e sapremo quanti sono effettivamente i lituani che, volenti o nolenti, sono rimasti nel loro paese.   
Secondo alcune statistiche sembra che ci siano un milione di lituani all'estero ed il fenomeno, grazie anche alla crisi economica, non ha nessuna voglia di diminuire. Persino io mi sono accorto che Klaipeda sta diventando sempre più un buco di campagna e la gente scompare nel nulla facendo diventare la terza città lituana quasi una città fantasma.


 Ma i lituani all'estero, vogliono tornare? Mi verrebbe di dire di no, ma è solo una mia impressione. Al massimo tornano per spendere le sterline o gli euro guadagnati all'estero, ma non per vivere di nuovo in Lituania e affrontare ogni giorno tutte le difficoltà che questo paese può offrire. Su questo punto sarebbe interessante davvero avere delle opinioni "di prima mano". Quindi se sei un lituano/a all'estero e hai qualcosa da dire al riguardo, scrivi senza indugi ricordandoti di indicare almeno la tua età.


 Le possibilità di avere una vita felice è inesistente. E' vero che la felicità deve essere trovata in ognuno di noi, ma io continuo a sostenere che senza sole diventi davvero difficile. E penso di non sbagliarmi se affermo che i lituani sono molto più cordiali e gentili durante i mesi estivi.


 
> *Quando Alessandro Magno giunse ad Atene, tutti andarono ad ossequiarlo, eccetto Diogene il Cinico. Fu così che, ritenendolo degno di stima, il re macedone decise di andare a fargli visita. Lo trovò seduto dentro la botte. Parandosi davanti a lui, Alessandro Magno gli disse:  
> - Chiedimi tutto quello che desideri e lo avrai!  
> - Togliti dal sole, che mi stai facendo ombra – rispose Diogene, cui niente serviva.*

 **In un luogo ostile dove il freddo tempra gli animi, mancano le possibilità e la felicità è poco più di un miraggio**, tutt'intorno si vedono volti corrucciati e bellissime donne che risaltano come fiori nel deserto accentuando ancora di più la stranezza di questo posto. Vedi macchinoni da decine di migliaia di euro con ragazzi alla guida vestiti di tutto punto che non sanno nemmeno dove andare, nel mentre ti giri e vedi autobus vecchi e decrepiti che trasportano anziani a cui è rimasta solo la dignità di uscire per fare una spesa centellinando persino sul pane.


 I vecchi quartieri sovietici sono sciatti e abbandonati; le auto imperversano in ogni spazio libero facendosi beffe di un vecchio regime che non le ha mai considerate degne per il popolo. I marciapiedi sono rovinati e le case cadono a pezzi, ma nei quartieri nuovi dove le macchine sono numerose e ordinate negli ampi parcheggi e le case sono nuove e belle da vedere, si sente nell'aria lo stesso "odore" di tristezza perché i visi delle persone sono pensierosi nello stesso modo sia nei poveri che nei ricchi. 

 **Il cielo, per fortuna, non ha parenti e tratta gli uomini tutti allo stesso modo.**

 Link: [Epidemia dei suicidi nel mondo](http://en.wikipedia.org/wiki/Epidemiology_of_suicide) (inglese)

