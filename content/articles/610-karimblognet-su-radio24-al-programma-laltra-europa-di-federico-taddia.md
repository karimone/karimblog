Title: karimblog.net su Radio24 al programma L'altra Europa di Federico Taddia
Date: 2010-06-21
Category: Lituania
Slug: karimblognet-su-radio24-al-programma-laltra-europa-di-federico-taddia
Author: Karim N Gorjux
Summary: Sabato mattina sono stato intervistato in diretta su Radio 24 durante il programma l'Altra Europa in veste di esperto sulla Lituania (ehm..). L'intervista è durata appena 9 minuti quindi non si è[...]

[![](http://www.karimblog.net/wp-content/uploads/2010/06/micradio.jpg "micradio")](http://www.karimblog.net/wp-content/uploads/2010/06/micradio.jpg)Sabato mattina sono stato intervistato in diretta su Radio 24 durante il programma [l'Altra Europa](http://www.radio24.ilsole24ore.com/main.php?dirprog=L%27Altra%20Europa) in veste di esperto sulla Lituania (ehm..). L'intervista è durata appena 9 minuti quindi non si è parlato di grandi cose, ma il tempo è tiranno. Per fare le cose per bene bisognerebbe parlarne almeno due ore, ma ringraziando è possibile informarsi qui su karimblog.net  
  
E' stata un'esperienza emozionante e divertente. Emozionante perché Radio24 è una radio nazionale seguita da milioni di persone e divertente perché è qualcosa fuori dall'ordinario che si aggiunge ai tanti episodi divertenti che mi ha regalato il blog.


 I temi che hanno destato la curiosità a Radio24 sono stati l'articolo su [Lady Gaga dei So So Bad](http://www.karimblog.net/2010/06/18/e-anche-i-giovani-lituani-sono-capaci-di-stupire-il-successo-della-canzone-lady-gaga-so-so-bad/) e [Caro Lituano](http://www.karimblog.net/2006/08/13/caro-lituano/) (sob).


 Potete direttamente scaricare l'intervista in mp3 [qui](http://www.karimblog.net/wp-content/uploads/2010/06/intervista-Karim.mp3).


