Title: Mi sono svegliato ad un'ora decente :-)
Date: 2005-01-17
Category: Lituania
Slug: mi-sono-svegliato-ad-unora-decente-
Author: Karim N Gorjux
Summary: Finalmente ho iniziato la giornata svegliandomi ad un'ora decente: le 7 del mattino! Ora sono le 10, Davide è andato all'università e io in compagnia di Our Shangri-La dall'album "Shangri-La" di Mark[...]

Finalmente ho iniziato la giornata svegliandomi ad un'ora decente: le 7 del mattino! Ora sono le 10, Davide è andato all'università e io in compagnia di Our Shangri-La dall'album "Shangri-La" di[ Mark Knopfler](http://www.google.com/search?q=%2522Mark%20Knopfler%2522), lavoro al computer, mi bevo un the e pulisco i piatti.  
E' una mattinata particolarmente tranquilla, tutto è calmo e naturale. Alle 13 andrò in centro e mi vedrò con Milda, mi porterò la macchina fotografica e cercherò di proporvi qualche foto interessante anche se oggi il cielo è nuvoloso, il vento è freddo e pungente. Ogni volta che apro la finestra mi pento dopo appena 10 secondi che clima palloso!

 Ieri sera da noi erano ospiti due sorelle: Yasha e Raima. Entrambe sono lituane ed entrambe parlano italiano grazie a loro precedenti esperienze lavorative e di studio in Italia. La loro presenza ha reso piacevole una serata che per me si è conclusa alle 10, dato che ero molto stanco e la mia intenzione era di svegliarmi presto, sono andato a coricarmi come un vecchietto. Con noi c'era anche Andrea, un personaggio molto particolare di cui parlerò dettagliatamente in un prossimo articolo.


