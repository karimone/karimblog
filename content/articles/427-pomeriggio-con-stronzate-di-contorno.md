Title: Pomeriggio con stronzate di contorno
Date: 2008-03-13
Category: altro
Slug: pomeriggio-con-stronzate-di-contorno
Author: Karim N Gorjux
Summary: Sono felice che il tempo sia cambiato. Io sono un metereopatico fino all'osso e quando le giornate sono grigie e piovose, ho solo voglia di dormire; ho notato che anche Greta è[...]

Sono felice che il tempo sia cambiato. **Io sono un metereopatico** fino all'osso e quando le giornate sono grigie e piovose, ho solo voglia di dormire; ho notato che anche Greta è come me, quando piove e le giornate sembrano in bianco e nero, lei dorme quasi il doppio.


 Oggi la giornata era bellissima, ma ho dovuto passarmela in casa, ho dovuto fare cose che non mi piacciono e mi sono ritrovato in una di quelle situazioni spiacevoli in cui non sai come comportarti perché tanto **alla fine combini un casino**. E' una di quelle situazioni in cui si organizza una cena: noi e loro. Io invito anche una persona a me molto vicina, ma appena loro lo sanno, **storgono il naso**. Non posso entrare nei dettagli, ma il motivo per cui storgono il naso non è perché la persona a me cara ha qualche difetto, ma perché la sua presenza **può causare** in qualche modo qualcosa di spiacevole come già era successo a loro.


 Per fare un esempio e come se io bucassi la gomma al casello dell'autostrada perché la pressione nella ruota è troppo alta e invece di dare la colpa alla gomma do la colpa all'autostrada e decido di non prendere più l'autostrada (così non buco più la gomma!)

 So che è un post strano e forse ne capite poco o niente, ma la faccenda mi ha talmente scoglionato che avevo voglia di scriverne qualcosa.


