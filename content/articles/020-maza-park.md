Title: Maza Park
Date: 2004-08-29
Category: Lettonia
Slug: maza-park
Author: Karim N Gorjux
Summary: Ieri sono andato al maza parks con Kristina, ho affittato una bici e mi sono fatto in giro nel bosco. Ragazzi, mi ripetero', ma i boschi di Riga sono fantastici. Per andarci[...]

Ieri sono andato al *maza parks* con Kristina, ho affittato una bici e mi sono fatto in giro nel bosco. Ragazzi, mi ripetero', ma i boschi di Riga sono fantastici. Per andarci ho dovuto prendere il bus e andare nella zona est di Riga che era l'ultima che mi mancava. Inutile parlare del degrado appena a 2km dal centro, ormai non e' piu' una novita'. Penso che ci tornero' oggi perche' c'e' un bello zoo che ieri sera era chiuso. Ho assaggiato una prelibatezza Siberiana: il "*cebureki*"... buono!

 In questi giorni sono stato con Dave, ho visitato il Peter doma (che purtroppo stanno restaurando) e ho fatto altre bellissime foto. Mi rimane solo piu' domani e poi saro' in italia, sono un po' triste perche' lascio qui a Riga delle persone stupende, ma sono certo che le reincontrero' presto. In fin dei conti Riga ormai la conosco davvero bene, potrei persino fare da guida turistica per i miei amici che sono bramosi di venire qui.


