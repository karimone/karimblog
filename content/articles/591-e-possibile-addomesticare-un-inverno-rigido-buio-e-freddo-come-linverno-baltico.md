Title: E' possibile addomesticare un inverno rigido, buio e freddo come l'inverno baltico?
Date: 2010-02-18
Category: Lituania
Slug: e-possibile-addomesticare-un-inverno-rigido-buio-e-freddo-come-linverno-baltico
Author: Karim N Gorjux
Summary: Devo ammettere che l'inverno baltico mi sta mettendo in difficoltà. Stamattina ho cercato delle immagini di spiagge caraibiche da mettere nello sfondo del computer in modo da contrastare tutta questa neve che[...]

[![](http://farm1.static.flickr.com/166/427249380_7e7bd1a7da_m.jpg "Girls in Vilnius")](http://www.flickr.com/photos/dodland/427249380/)Devo ammettere che l'inverno baltico mi sta mettendo in difficoltà. Stamattina ho cercato delle immagini di spiagge caraibiche da mettere nello sfondo del computer in modo da contrastare tutta questa neve che da mesi monopolizza il panorama di Klaipeda.


 Il freddo è il grande nemico della quotidianità, si esce poco e quando si esce non sai cosa fare. Ecco, forse il problema è proprio questo, quando esci non hai nulla da fare se non **le solite cose fatte e strafatte.**

 Da quando sono qui ho iniziato ad andare in palestra la domenica. Non era mai successo nella mia vita, ma ora approfitto degli orari incredibilmente flessibili della [mia palestra](http://www.herkausklubas.lt/ "Herkaus Manto Sporto Klubas") per allenarmi praticamente in qualsiasi giorno della settimana. Se non vado in palestra, sono a casa nell'ufficio a lavorare o studiare o al massimo guardare l'ultimo serial televisivo torrentizzato a dovere.


 Quando Greta è a casa, veramente non si hanno tante possibilità di svago. A Klaipeda abbiamo l'Akropolis il centro commerciale della felicità sintetica dove puoi andare al cinema, giocare al bowling o mangiare tutti i cibi più strani.


 Consumato e abusato del centro commerciale, rimane la passeggiata in centro Klaipeda, che a causa dell'Akropolis si sta trasformando sempre di più in una **città fantasma**. Rimangono la passeggiata al mare, la passeggiata nella foresta o la visita a Telsiai dai suoceri nell'appartamento sovietico propedeutico per la cura dell'agorafobia, ma sempre chiusi in casa a causa del freddo gelido.


 In pratica c'è ben poco da fare e le giornate diventano alla lunga noiose perché in palestra c'è un limite fisiologico, lavoro, tv e libri vanno bene, ma alla fine stufano e poi la bambina ha bisogno di qualche svago che non siano i soliti cartoni.


 L'altra Domenica sono andato a Palanga (la piccola Rimini lituana) con una nostra amica e il suo piccolo bimbo, faceva un freddo senza senso e la passeggiata si è trasformata in un continuo pellegrinaggio in locali a bere il solito "Arbata" (The).


 C'è da dire che siamo quasi a Marzo e le giornate si stanno allungando velocemente, alle 8 del mattino c'è già la luce e dura ormai fino alle 18. Se tutto và bene, tra 1 mese non ci sarà più la neve e si inizierà ad uscire la sera, andare al mare, godere del sole, giocare a calcio, andare in bicicletta etc. etc.


 Si dice che a differenza dell'Italia, il periodo freddo sia, da queste parti, più lungo di ben due mesi. Quest'anno sono quasi riuscito a "farcela", ma se penso al prossimo inverno mi viene già male... dato che tra i miei lettori ci sono tanti "nordici", chiedo proprio a te, **hai qualche suggerimento**?

