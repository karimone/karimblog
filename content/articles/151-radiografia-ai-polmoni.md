Title: Radiografia ai polmoni
Date: 2006-02-22
Category: Lituania
Slug: radiografia-ai-polmoni
Author: Karim N Gorjux
Summary: Oggi mi è toccato servirmi del servizio sanitario lituano. Al modico prezzo di 2€ ho potuto fare una radiografia ai polmoni in un istituto infestato dai malati di tubercolosi.Sarà che le infermiere[...]

Oggi mi è toccato servirmi del servizio sanitario lituano. Al modico prezzo di 2€ ho potuto fare una radiografia ai polmoni in un istituto infestato dai malati di tubercolosi.  
  
Sarà che le infermiere stanno meglio degli ospiti, ma sono tutte allegre e sorridenti e tutto ciò è alquanto inusuale. Mentre si parlava dei miei polmoni sanissimi e bellissimi senza l'ombra di un solo *cough*, Rita si è fatta mostrare, per didattica infermieristica, la radiografia di un malato di tubercolosi. 

 Sembrava una di quelle immagini dal satellite che mostrano l'uragano di turno. 

 E' proprio bello essere sani.  
  
  
adsense  
  


