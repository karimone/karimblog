Title: Esperienza
Date: 2008-08-13
Category: Lituania
Slug: esperienza
Author: Karim N Gorjux
Summary: La mia partenza si avvicina, sono solo in casa, ho molto lavorò da fare e piano piano, giorno dopo giorno smembro ogni più piccolo ricordò di quella ché è stata la nostra[...]

La mia partenza si avvicina, sono solo in casa, ho molto lavorò da fare e piano piano, giorno dopo giorno smembro ogni più piccolo ricordò di quella ché è stata la nostra casa in questi ultimi due anni. 

 È faticoso. Non è solo il peso degli innumerevoli scatoloni e nemmeno la demotivazione nell'affrontare tutto questo lavoro da solo. È faticoso dover ricominciare, crearsi un nido, abituarsi al nido e vivere nel nido. Non è solo faticoso, ma difficile. 

 Rammarichi. 

 A volte penso ché sarebbe stato meglio non venire in Italia. Avrei sicuramente risparmiato molto denaro e le cose sarebbero sicuramente molto diverse ora.


 Forse sarebbe andata meglio o forse peggio, ma sono sicuro ché ora, dopo l'esperienza italiana, sarà tutto molto diverso in Lituania. Gli errori commessi e l'esperienza maturata mi permettono di vedere con molta chiarezza il percorso da seguire e soprattutto quello da non seguire.


 Ho ancora dèi dubbi, ma il dubbio è l'inevitabile genesi dell'azione, l'interrogativo universale davanti all'incertezza del domani. È solo questione di tempo: ore, giorni e, se necessario anche settimane e inevitabilmente i dubbi si dissolveranno. 

 "Per quanto possa essere lungo e freddo l'inverno, prima o poi, inesorabilmente, arriverà la primavera."

