Title: Riflessioni di chi parte
Date: 2008-07-16
Category: Lituania
Slug: riflessioni-di-chi-parte
Author: Karim N Gorjux
Summary: Guardo un paio di commenti, rispondo alle email e vedo che è uscito wordpress 2.6, ma non ho voglia di aggiornarlo. Stamattina mi sono allenato con un mio amico in palestra, è[...]

Guardo un paio di commenti, rispondo alle email e vedo che è uscito wordpress 2.6, ma non ho voglia di aggiornarlo. Stamattina mi sono allenato con un mio amico in palestra, è stato carino, la mia palestra mi spiace e sarà la cosa che mi mancherà di più quando sarò in Lituania.


 Oggi è il 16 Luglio e significa che tra circa 20 giorni Rita partirà con Greta, solo al pensiero di tutte le cose che dovrò fare prima di trasferirmi, mi viene male, ma c'è una cosa dalla mia parte: l'esperienza. Ho già vissuto in Lituania e questa è una seconda chance, non sembrerebbe, ma mi sento ottimista.


