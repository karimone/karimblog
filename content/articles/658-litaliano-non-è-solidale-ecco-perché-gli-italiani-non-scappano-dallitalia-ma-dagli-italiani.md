Title: L'italiano non è solidale, ecco perché gli italiani non scappano dall'Italia, ma dagli italiani
Date: 2013-12-19
Category: altro
Slug: litaliano-non-è-solidale-ecco-perché-gli-italiani-non-scappano-dallitalia-ma-dagli-italiani
Author: Karim N Gorjux
Summary: In questi giorni si parla tanto dei "Forconi" nelle varie piazze italiane. E' buono che se ne parli, ma mi spiace perché non porterà a molto; purtroppo gli italiani sono solidali solo[...]

In questi giorni si parla tanto dei "Forconi" nelle varie piazze italiane. E' buono che se ne parli, ma mi spiace perché non porterà a molto; purtroppo gli italiani sono solidali solo con loro stessi, quindi le piazze sono piene di gente disperata, ma chi ha i soldi punta il dito e se ne frega senza capire che **quando tutti intorno a te sono malati, essere l'unico sano non è una cosa piacevole.**  
  
Prendiamo ad esempio Torino che è una delle città più colpite dalla crisi a causa dell'indotto Fiat. Poi prendiamo Cuneo che è dove vivo io. A Cuneo si sta bene e in piazza non c'era praticamente nessuno, si certo, qualche scappato da casa mischiato con gli studenti c'era, ma i poliziotti sono venuti a fare presenza con gli infradito altro che caschi! Questo semplicemente perché Cuneo è una zona molto ricca e quindi si sta ancora troppo bene, **i problemi dell'Italia non riguardano ancora Cuneo.**

 Secondo il mio modesto parere il problema dell'Italia è semplicemente negli italiani stessi. Non sono un popolo unito come i francesi o i tedeschi, sono semplicemente un popolo che è unito ogni 4 anni quando 11 rappresentanti prendono a calci un pallone (il prossimo anno in Brasile, tra l'altro). Altrimenti non c'è nessuno che ha da spartire qualcosa con un altro italiano. **Che cosa hanno in comune un valdostano ed un palermitano?** Perché dovrebbero combattere per gli stessi ideali? L'italiano guarda il suo orto, **se sta bene lui, stanno bene tutti**.


 Ed intanto molti emigrano, non perché l'Italia fa schifo, **ma scappa dall'italiano stesso** che al momento è il suo più grande nemico.


