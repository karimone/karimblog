Title: Andre Zappa: vi chiedete perché ovunque andrete sarete perseguiti dagli stereotipi italici?
Date: 2010-06-16
Category: Lituania
Slug: andre-zappa-vi-chiedete-perché-ovunque-andrete-sarete-perseguiti-dagli-stereotipi-italici
Author: Karim N Gorjux
Summary: L'ultima segnalazione che ho ricevuto è la canzone di Andre Zappa, cantante italiano sconosciuto in patria, ma che gode di un modesto successo qui in Lituania. La sua ultima canzone mescola la[...]

[![](http://www.karimblog.net/wp-content/uploads/2010/06/andre_zappa-300x300.jpg "andre_zappa")](http://www.karimblog.net/wp-content/uploads/2010/06/andre_zappa.jpg)L'ultima segnalazione che ho ricevuto è la canzone di [Andre Zappa](http://www.andrezappa.com/), cantante italiano sconosciuto in patria, ma che gode di un modesto successo qui in Lituania. La sua ultima canzone mescola la politica lituana osannando la presidentessa [Dalia Grybasuskaite](http://it.wikipedia.org/wiki/Dalia_Grybauskaitė) con la frase "Dalia Grybauskayite è la numero 1" e tutti i simboli italiani che sono conosciuti nel mondo: la pizza, i maccheroni, pinocchio, berlusconi, capuccino... e persino il padrino!  
  
Purtroppo, canzoni come ["Mano Daila Grybauskaite"](http://www.youtube.com/watch?v=s1WRDciIgeQ), sono molto diffuse. Sono canzoni che fanno leva sui nostri stereotipi italici rendendoli indenni di fronte al passare del tempo. Ti sei mai chiesto come sia possibile che appena arrivate in un paese straniero al primo "hello" vi verrà ricordato il padrino, i maccheroni, la pizza etc. etc.? Beh, è anche grazie ad uno come Andre Zappa.


 Il fenomeno degli italiani famosi all'estero e sconosciuti in patria è abbastanza diffuso, un esempio è [Gianmaria Testa](http://it.wikipedia.org/wiki/Gianmaria_Testa) che è famoso più all'estero che in Italia, ma almeno è un cantautore di qualità. Forse tu ne conosci altri?

