Title: Il business in Lituania con i lituani può essere più difficile di quanto sembri
Date: 2009-05-08
Category: Lituania
Slug: il-business-in-lituania-con-i-lituani-può-essere-più-difficile-di-quanto-sembri
Author: Karim N Gorjux
Summary: Io posso vantarmi di aver creato il mio business in Italia e di goderne i frutti stando in Lituania. Non dico che sia stato facile, tutt'altro! Ad essere sincero, in questo periodo[...]

[![](http://farm3.static.flickr.com/2338/2072535972_15cdf86659_m.jpg "Business a Vilnius")](http://www.flickr.com/photos/johnas/2072535972/)Io posso vantarmi di aver creato il mio business in Italia e di goderne i frutti stando in Lituania. Non dico che sia stato facile, tutt'altro! Ad essere sincero, in questo periodo faccio fatica a concludere qualche affare, ma bene o male riesco a cavarmela. Penso che non sarei mai stato capace di fare un business con i lituani, non ho abbastanza coraggio e pazienza per scontrarmi con **una mentalità che, dal mio punto di vista, ha delle lacune di logica notevoli**.  
  
Conosco delle persone che hanno creato il loro business con i lituani e, dai loro racconti, ne ho sentite di cotte e di crude. La mentalità di vecchio stampo del lituano è ben poco propensa a nuove idee. La **xenofobica repulsione verso la straniero** poi, rende la vendita ancora più difficile di quanto essa già normalmente sia. 

 Se entri in un negozio in Lituania, mai un commesso verrà a chiederti qualcosa o cercherà mai di venderti qualcosa. **Il marketing in Lituania è all'età della pietra** e questo è preoccupante. Inoltre anche l'affidabilità del lituano lascia il tempo che trova, sono sicuro che qua fuori esiste gente che se da la sua parola la rispetta, ma generalmente ho potuto constatare che le promesse e gli appuntamenti non vengono rispettati anche se ci sono di mezzo dei soldi.


 Un'ultima cosa che ho cercato sempre di evitare, è il ragionamento in Litas. Se si inizia a ragionare in Litas si rischia di svalutarsi e di lavorare per pochi soldi, io ho evitato il business con i lituani principalmente per questo discorso. Se devo lavorare con qualcuno, è giusto che lavori per chi mi offre di più.


 Se devo invece comprare, ragiono in Litas. E' un buon esercizio di ambientamento locale e di risparmio casalingo.


