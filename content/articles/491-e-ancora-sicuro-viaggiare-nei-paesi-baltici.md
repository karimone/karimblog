Title: E' ancora sicuro viaggiare nei paesi baltici?
Date: 2008-09-29
Category: Lituania
Slug: e-ancora-sicuro-viaggiare-nei-paesi-baltici
Author: Karim N Gorjux
Summary: Sabato sera sono stato all'Akropolis di Klaipeda e ho provato per la prima volta in vita mia il ristorante giapponese. Sono stato completamente io e Rita abbiamo approfittato della permanenza di Greta[...]

Sabato sera sono stato all'Akropolis di Klaipeda e ho provato per la prima volta in vita mia il [ristorante giapponese](http://www.hanabi.lt/). Sono stato completamente io e Rita abbiamo approfittato della permanenza di Greta dai nonni per andare a mangiare fuori da tranquilli. Non mi dilungo sulla descrizione dei piatti o del prezzo, ma vi assicuro che come ristorante esotico non ha nulla a che fare con i nostri ristoranti-catena cinesi.  
  
Uscendo dall'Akropolis ho intravisto tre italiani. Turisti, casinisti, **importunatori**. I barbarici italiani si spingono ormai ovunque perché le capitali baltiche sono ormai completamente rovinate dai turisti e non solo quelli italiani. Riga, la capitale baltica per eccellenza, è assolutamente da evitare e non sono io a dirlo, ma il prestigioso Baltic Times in [questo articolo](http://www.baltictimes.com/news/articles/21110/) in inglese.


 Il mio **consiglio** per sopravvivere come turista nei paesi baltici è per ora uno solo: **conoscere la lingua locale**. Se dovete difendervi in inglese siete spacciati, ma se capite e vi fate capire nella loro lingua è tutto un altro paio di maniche. Quando intendo parlare la lingua locale, intendo parlare il lituano in Lituania, il lettone in Lettonia e l'estone in Estonia; perché anche se il russo è molto diffuso e, sopratutto a Riga, c'è ancora una grande presenza di russi, sono convinto che è meglio essere straniero parlando inglese che essere straniero parlando russo.


