Title: Italia, un mese dopo
Date: 2011-05-11
Category: Lituania
Slug: italia-un-mese-dopo
Author: Karim N Gorjux
Summary: Non stiamo a perdere tempo con i difetti di questo grande paese rappresentato da questa piccola città. Ci limitiamo a vivere tutto ciò che di bello l'Italia ci riserva. C'è un sole[...]

Non stiamo a perdere tempo con i difetti di questo grande paese rappresentato da questa piccola città. Ci limitiamo a vivere tutto ciò che di bello l'Italia ci riserva. C'è un sole ed una campagna stupenda e le montagne influiscono sul nostro umore tanto sono belle da vedere.  
*  
"Greta, vuoi tornare a Klaipeda?"  
"No, voglio stare in Italia tutto il giorno!"*  
  
Centallo è un paesino vicino a Cuneo abitato da cinquemila anime. C'è un po' di tutto e si può fare tutto senza prendere l'auto. Da buon italiano che vive in un piccolo borgo, tutti mi conoscono e io conosco praticamente tutti. All'ora di pranzo prendo la bici, metto Greta sul seggiolino e andiamo per le strade di campagna a vedere le mucche, il fiume e il vecchio mulino. Per la strada raccogliamo i fiori da portare a mamma e prima di arrivare a casa la bimba si è già addormentata poggiando la testa sulla mia schiena.


 Il Lunedì mattina c'è il mercato nella piazza vicino a casa nostra. Spesso compriamo i formaggi fatti da un pastore di montagna e a pranzo ci gustiamo frutta, verdura e formaggi freschi mentre il paese fuori dorme, non una mosca vola, non una persona passeggia.


 I telegiornali non li guardo, i giornali li leggo approssimativamente, il mio ufficio sta prendendo piede e tutte le cose negative che possono fare tanto arrabbiare e penare non le prendo nemmeno in considerazione.


 Per ora va bene. Ed è un mese che ho lasciato Klaipeda.


