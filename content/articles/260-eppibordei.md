Title: eppibordei
Date: 2006-11-09
Category: altro
Slug: eppibordei
Author: Karim N Gorjux
Summary: Ho 27 anni, sono sposato, ho una bimba, 14kg oltre il peso forma, tanta simpatia, felicità e anche una società informatica in partenza. Và tutto bene!

Ho 27 anni, sono sposato, ho una bimba, 14kg oltre il peso forma, tanta simpatia, felicità e anche una società informatica in partenza. Và tutto bene!

 **Tanti auguri a me!** 

 Tanti auguri anche ad [Alex Del Piero](http://www.alessandrodelpiero.com/) che compie gli anni il mio stesso giorno, ma per 5 volte in più.


 PS: Il titolo del post è in lingua [daninglese](http://www.danielaforconi.net) (©).


