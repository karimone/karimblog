Title: Sono schifato
Date: 2005-06-13
Category: altro
Slug: sono-schifato
Author: Karim N Gorjux
Summary: Toglietevi dalla testa di poter cambiare le cose. I soldi rovinano tutto, oramai siamo fregati. Cosa pensate delle persone che ci governano? Puo’ veramente una persona che guadagna svariati miliardi preoccuparsi delle[...]

Toglietevi dalla testa di poter cambiare le cose. I soldi rovinano tutto, oramai siamo fregati. Cosa pensate delle persone che ci governano? Puo’ veramente una persona che guadagna svariati miliardi preoccuparsi delle persone insignificanti che lavorano per 900 euro al mese? In fin dei conti chi siamo per loro? Assolutamente niente, meno di un numero. Possiamo esserci o meno e non fa’ nessuna differenza.


 Silvio Berlusconi e’ ricco da fare schifo, ha tutto, controlla il paese tramite televisioni, giornali e radio. Persino le riviste e i libri li fa lui. Ma dove vuole arrivare? A conquistare il mondo? Vi ricordate qualcosa di buono e significativo che ha fatto per gli altri? Mettiamolo a confronto con un altro personaggio che ha fatto la differenza: Martin Luther King. Sono sicuro che costui non aveva soldi come il Berlusca, ma sicuramente, mentre il b. un giorno sara’ cibo per i vermi con tutti i suoi soldi (e quelle zoccole delle veline), noi ricorderemo ancora Martin Luther King. Ve lo vedete s.b. a fare lo sciopero della fame come Gandhi per i diritti degli italiani?

 A noi manca della gente con i coglioni, che pensi un po’ meno ad arricchirsi e di piu’ a fare l’interesse degli altri. Il tronchetti provera con 44 miliardi di euro di debiti, non si interessa neanche minimamente a permettere agli italiani di comunicare (vedi spot di Ghandi etc..), ma vuole arricchirsi continuamente alle nostre spalle.


 Cosa ci rimane da fare? Come possiamo difenderci? Con la conoscenza e la sua piu’ saggia conseguenza: la scelta.


 Spegniamo la tv.  
Leggiamo le notizie su internet e da piu’ fonti.  
Prima di comprare un servizio o prodotto ci informiamo sull’azienda che lo fornisce. Questo e’ l’unico voto che veramente importa. Se oggi 10mln di televisioni si spengono le cose cambiano, se 10mln di persone disdicono il contratto telecom, le tariffe cambiano.


 Serve davvero andare a votare destra o sinistra? No. l’unico voto che abbiamo e’ dettato dalla nostra saggezza.


 Inculiamoli!

