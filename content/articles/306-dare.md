Title: Dare
Date: 2007-03-30
Category: Lituania
Slug: dare
Author: Karim N Gorjux
Summary: Vivere in Lituania è una palla. Ok, se si fa la vita da erasmus, ci si diverte, si fa finta di andare a scuola, si va con le bionde e può essere[...]

Vivere in Lituania è una palla. Ok, se si fa la vita da [erasmus](http://www.sandro.madeinblog.net), ci si diverte, si fa finta di andare a scuola, si va con le bionde e può essere estremamente divertente, ma vivere in Lituania da straniero è una vera palla. [Qualcuno](http://www.karimblog.net/2007/03/23/italiettuva#comment-33608) ha provato l'esperienza, con la moglie italiana, e senza pensarci due volte sta preparando la fuga (come ho fatto io d'altronde). 

 Senza internet vivere in Lituania è un completo suicidio, l'unica persona contenta di vivere in Lituania che ho conosciuto era Enrico e ancora adesso non ho mai capito come potesse piacergli vivere a Klaipeda. A parte Enrico di cui ho imparato apprezzare sia i difetti che i pregi e ho voluto un sacco di bene, ho conosciuto virtualmente un sacco di persone. In particolare con un paio ho stretto un forte legame d'amicizia. Ma in particolare con una persona avevo molto disgrazie in comune: donna in Lituania, fuga dall'Italia per amore, problemi con la popolazione Lituana.


 Questa particolare amicizia è nata, come tutte le altre, dal mio blog. Una mail, due email, tre email, skype... e poi la proposta di condividere il mio hosting per creare i suoi siti web. L'ho aiutato a personalizzare i siti, a gestirli, a scrivere degli articoli "da web" in pratica ho usato un po' del **parecchio** tempo disponibile per aiutar**ci**.


 Forse avrei dovuto andare a vivere Vilnius e non rimanere a Klaipeda, forse se fossi stato nella capitale avrei resistito qualche mese di più o forse me ne sarei scappato prima. Di fatto in quel periodo di clandestinità Lituana ho coltivato amicizie importanti, una mi ha permesso di diventare imprenditore, l'altra mi ha permesso di imparare molte cose e soprattutto mi ha aiutato in molti momenti difficili passati a Klaipeda. 

 Conosco **troppe persone diffidenti** che non darebbero mai aiuto ad una persona che non conoscono, non passerebbero del tempo "gratis" per aiutare qualcuno, io l'ho fatto. Il poter aiutare qualcuno nello stesso modo in cui vorrei essere aiutato mi faceva stare bene, non ho mai pensato che un giorno quel tempo speso "per nulla" mi sarebbe tornato indietro. 

 Inizierò un corso molto particolare, se doveste seguirlo vi costerebbe dai 1500 ai 2000 Euro, ma io avrò la fortuna di seguirlo **gratis**, da casa, via skype, dalla Lituania.


 Da molto tempo ho un consulente, via skype, gratuito, con cui ho un rapporto bellissimo che mi da consigli, spunti, aneddoti e molti aiuti preziosissimi che senza di lui avrei imparato solo dopo **anni e anni **di lavoro. 

 E tutto è partito da un sorriso.


