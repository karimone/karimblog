Title: Emigrare è una scommessa per la vita, sicuro di aver fatto la scelta giusta?
Date: 2010-01-14
Category: Lituania
Slug: emigrare-è-una-scommessa-per-la-vita-sicuro-di-aver-fatto-la-scelta-giusta
Author: Karim N Gorjux
Summary: Gli italiani emigrano come i nostri nonni emigravano in cerca di fortuna, è un fatto sintomo di una realtà che pochi vogliono affrontare. In Italia il potere è legato a persone vecchie,[...]

[![](http://farm5.static.flickr.com/4020/4273184773_6040aeb029_m.jpg "Emigranti italiani a Manhattan")](http://www.flickr.com/photos/kmen-org/4273184773/)Gli italiani emigrano come i nostri nonni emigravano in cerca di fortuna, è un fatto sintomo di una realtà che pochi vogliono affrontare. In Italia il potere è legato a persone vecchie, incompetenti e raccomandate. In Italia il furbo, quello che riesce a trovare una scappatoia a qualsiasi vincolo, è tutelato, mentre l'onesto è omertosamente deriso da chi delle regole si rallegra come dopo aver sentito una barzelletta.  
  
Io sono emigrante, sono un italiano che vive in Lituania, ma gli italiani all'estero sono davvero molti. Tempo addietro ho sentito un voce, mai confermata, che faceva notare che ci sono più calabresi in Canada che in calabria; una legge non scritta, ma **empiricamente dimostrabile**, cita: *"Ovunque tu vada, ci sarà sempre un italiano"*. A mia esperienza personale, è stata una legge infallibile. (Piccola diversione matematica: se non si esclude dalla regola se stessi nel caso si sia italiani, la regola diventa un paradosso perché nel momento che vai in un posto un'italiano c'è: ovvero tu!)

 Se un italiano emigra dall'Italia per altri nidi, alla base di questo importante e coraggioso gesto, c'è **una sfiducia nel proprio paese**. Emigrare è un messaggio implicito che sfiducia l'Italia e pone la propria vita, perché di questo si parla, il proprio lavoro, le proprie risorse e le proprie speranze in un paese straniero che ci accoglie come stranieri e dove le persone, la lingua, i cibi, la storia, le usanze e persino il modo di pensare è differente da ciò che è parte di noi.


 Quasi **ogni giorno mi chiedo se la Lituania sia la scommessa giusta**. Un po' come i trader comprano azioni e quindi danno fiducia a delle aziende o comprano valute e quindi danno fiducia ad un paese, io sto investendo il mio tempo, il mio denaro ed in pratica la mia vita sulla Lituania ed è quindi una preoccupazione lecita domandarsi se è una scelta giusta.


 Il litas (la moneta locale) vale poco meno della carta straccia, se esci dalla Lituania con dei litas in tasca, sei fregato, ti conviene tenerli per il prossimo viaggio perché difficilmente troverai una banca disponibile a cambiarti il litas in euro. Oltre al fatto che il litas vale poco anche la retribuzione lituana è coerente al contesto lituano.  
Non ho idea di quanto ammonti la retribuzione media lituana, su [NationMaster](http://www.nationmaster.com/index.php) non sono riuscito a trovare una voce a proposito, ma in base alla mie conoscenze so che una commessa di un negozio prende 1300 litas al mese netti (376€) mentre la responsabile dello stesso negozio prende 1800 litas al mese sempre netti (522€).


 Nel contesto lituano sono stipendi normali e ti permettono di vivere **senza concederti troppi sfizi**. Il discorso è diverso quando si parla invece di creare una famiglia, comprare casa, godersi delle vacanze o fare degli investimenti. Il litas è sempre il litas e la Lituania è solo un angolo del vasto mondo, quindi a vivere qui o si guadagna come si deve o ci si rassegna a vivere alla *"spera in Dio"*.


 Ho avuto modo di discutere a tal proposito con chi vive questa realtà di lavoratore lituano. Evito di riportare i soliti paragoni che tentano di trovare un moltiplicatore (o divisore) che diano un senso alle differenze di valore, ma a mio avviso ci sono alcuni punti che dovrebbero essere approfonditi.  
  
Per vivere bene, senza figli e possibilmente senza affitto, **è necessario guadagnare almeno 600€ al mese**.


 **Il prezzo delle case è elevato** in confronto agli stipendi. Un'alternativa è l'acquisto del vecchio appartmento sovietico o una casa di legno meno, ma in questo ultimo caso non saprei dire quanto costa realmente di meno. A questo proposito mi viene da pensare che una casa di legno la puoi comprare qui è fartela costruire dove vuoi, anche in un paese dove lo stipendio è in Euro.


 **Ci sono meno spese:** gli alimenti costano meno, l'assicurazione costa meno e la burocrazia è più semplice, ma le macchine hanno gli stessi prezzi europei, telefoni e computer anche. Uno stipendio svalutato tende a incrinarsi sotto il peso di una global economy.


 Che io sappia ci sono due paesi in Europa che erano come la Lituania e ora sono delle potenze economiche di tutto rispetto. La Norvegia (soprattutto grazie al petrolio) e ora un paese ricco e fiorente, l'Irlanda è un altro esempio di paese in crescita e che attualmente ospita un nutrito gruppo di italiani.


 Dato che il tempo è una risorsa non acquistabile e usufruibile solamente fino ad esaurimento scorte, è necessario **valutare bene se la Lituania è il posto ideale per trascorrere la vita di emigrante**. Senza riassumere tutto alla infinita diatriba Italia - Lituania, sarebbe necessario esaminare gli altri 26 paesi europei e chiedersi: "è forse meglio "scomettere" su un paese più promettente?"

