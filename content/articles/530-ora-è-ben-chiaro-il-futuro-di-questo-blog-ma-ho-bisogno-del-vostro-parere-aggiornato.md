Title: Ora è ben chiaro il futuro di questo blog, ma ho bisogno del vostro parere (Aggiornato)
Date: 2009-03-26
Category: altro
Slug: ora-è-ben-chiaro-il-futuro-di-questo-blog-ma-ho-bisogno-del-vostro-parere-aggiornato
Author: Karim N Gorjux
Summary: Dopo svariate riflessioni ho capito cosa devo fare di questo blog, come scriverci, quanto scriverci e cosa scrivere. Fino a poco tempo fa ero confuso perché non avevo più chiara l'utilità del[...]

[![Novità](http://www.karimblog.net/wp-content/uploads/2009/03/novita-300x271.jpg "Novità")](http://www.karimblog.net/wp-content/uploads/2009/03/novita.jpg) Dopo svariate riflessioni ho capito cosa devo fare di questo blog, come scriverci, quanto scriverci e cosa scrivere. Fino a poco tempo fa ero confuso perché non avevo più chiara l'utilità del mio blog, ma più cercavo risposte complicate e più mi allontanavo dalla risposta che è sempre stata davanti ai miei occhi. 

 Il mio blog è karimblog.net quindi non deve far nient'altro che raccontare (con le dovute riserve) di me. Tutto il resto sarà delegato ad altri blog che sto progettando con cura e con aiuto di altre persone.  
  
Karimblog.net continuerà ad essere il mio diario raccontando le mie giornate e i miei pensieri e dato che abito in Lituania, il tema ricorrente sarà la Lituania. Articoli tecnici, computer e altre cose particolari saranno pubblicati su altri siti che mi impegnerò prontamente a segnalarvi.


 **Molti articoli che ho scritto e che non rispecchiano questa nuova visione del blog, verrano cancellati.** Al momento ho circa 950 articoli pubblicati, vedremo alla fine della sfoltitura che cosa ne rimarrà. Un'ultima questione non ha ancora trovato risposta: **Cosa devo cambiare del design del blog?** Il sondaggio lo trovate sulla destra. Grazie per la vostra collaborazione!

