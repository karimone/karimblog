Title: Qualche news e indiscrezioni su Riga
Date: 2004-08-14
Category: Lettonia
Slug: qualche-news-e-indiscrezioni-su-riga
Author: Karim N Gorjux
Summary: Ieri sera sono stato invitato a mangiare a casa di Kristina nella zona di riga chiamata Imanta 3. Ho mangiato una zuppa di verdure e formaggio che non era affatto male. Durante[...]

Ieri sera sono stato invitato a mangiare a casa di Kristina nella zona di riga chiamata **Imanta 3**. Ho mangiato una zuppa di verdure e formaggio che non era affatto male. Durante la serata ho incontrato Ivars, il cugino di Kristina che io chiamo amichevolmente Arnold (è immenso) e la sua ragazza Indra essi hanno passato la serata a bere vodka lettone e mangiare cioccolatini guardando la tv russa e lettone. Dopo un'oretta si sono aggiunti aggiunti a noi un'altro Ivars, amico di Alona, che lavora nella polizia militare accompagnato da un'altro ragazzo particolarmente rinc... quest'ultimo lavora al Vietsica Hotel come recezionista e mi ha spiegato chi sono quelle tipe bionde, giovani e belle che parcheggiano sempre il mercedes dalle parte dell'hotel e sono vestite da stragnocche.... ebbene si... i turisti facoltosi accedono alle loro prestazioni per la modica somma di 100 euro l'ora.


 Ho visto una macchina degna della scenografia di FANTOZZI, una moscovita 412 chiusa con il luchetto!!! Roba da non credere, il colore mi ricorda la macchina di Mr. Bean :)

 [![La moscovita](http://www.kmen.org/images/moscovita.jpg)](http://www.kmen.org/images/moscovita.jpg)



 Altre novità degne di nota... ho incontrato gia' una decina di pazzi per le strade e uno l'ho anche filmato. Gente che si ferma a fare facce strane e altri che gridano di essere Dio in mezzo alla strada (ho avuto la traduzione sul momento). Il filmato lo trovate [qui](http://www.kmen.org/movies/pazzo_milda.3gp)

 Un paio di curiosità, non ho ancora capito il perchè, ma ogni tanto si aggirano sulla città delle nuvole di puzzo incredibile. Mi capita spesso di sentire queste ventate tossiche quando passo davanti al mc donald, non penso che sia colpa loro anche perchè l'odore è di fogna...  
Una nota di dovere: **il cambio**! se vi aggirate per old riga, non cambiate i soldi nei vari **Valutas Maina** perchè vivono di quello e offrono un cambio degno di un ladro, il cambio migliore lo avete nei centri commerciali perchè preferiscono lucrare sull'acquisto nei negozi che su qualche euro con il cambio.


