Title: Prendere residenza a Melbourne: auto, affitto e arredo
Date: 2016-05-09
Category: Australia
Slug: prendere-residenza-a-melbourne-auto-affitto-e-arredo
Author: Karim N Gorjux
Summary: Atterro il 12 Marzo 2016 a Melbourne. Ringraziando l'universo intero e tutta la collaborazione dei santi del calendario, un mio amico si offre di ospitarmi per il periodo in cui dovro' preparare[...]

Atterro il 12 Marzo 2016 a Melbourne. Ringraziando l'universo intero e tutta la collaborazione dei santi del calendario, un mio amico si offre di ospitarmi per il periodo in cui dovro' preparare l'arrivo della moglie e dei bambini prevista per il 12 Aprile 2016. Ho esattamente trenta giorni.


  Il mio amico Andrea e' gentilissimo sia nell'ospitarmi che nell'aiutarmi nelle piccole cose. Ho internet, un letto, spazio a sufficienza anche per lavorare. Le cose da fare in ordine sono:  
  
 3. Comprare una macchina
  
 6. Affittare una casa
  
 9. Arredare la casa
  
  
La macchina
-----------

  
Comprare una macchina e' una cosa che puo' richiedere mesi anche se all'atto pratico l'acquisto e' questione di un'ora. Purtroppo io non conosco il mondo dei motori e quindi ho chiesto a mio zio australiano di aiutarmi e soprattutto di portarmi a vedere dei rivenditori. Qui le macchine vengono vendute un po' come nei film americani, a Melbourne la zona piu' famosa e' quella di Moorabin, entro in uno dei venditori sulla statale. Mio zio mi chiede quanto voglio spendere e lui, assolutamente distante da qualsiasi problematica vissuta dagli europei mi suggerisce di spendere almeno 7/10 MILA EURO per una macchina. Io ne scelgo una e me la cavo con 1700€ chiavi in mano. Ok, non e' niente di speciale, ma fino ad ora mi ha portato in giro senza problemi. La macchina e' una Hyundai Excel del 2000 con 160.000km e mi e' stata venduta con il RoadWorthy (il nostro collaudo), tax stamp (passaggio di proprieta') e rego fino a Giugno (il nostro bollo).


 Avessi avuto un po' di esperienza, avrei comprato la macchina con il gancio traino (tow bar), ne parlero' dopo.  
La casa
-------

  
Ora che ho una macchina posso muovermi per andare a cercare la casa. Il mercato immobiliare in Australia e' uno degli elementi cardine dell'economia interna. I prezzi delle case aumentano da anni e tendono a raddoppiare ogni 7 anni [senza fonte], di conseguenza anche il mercato degli affitti e' in continuo fermento. Il problema non e' tanto trovare la casa, **ma dove trovare la casa.**

 Per scegliere la zona ho dovuto fare alcuni ragionamenti e scegliere tenendo conto di alcuni criteri:  
  
 * Costare massimo 400$ alla settimana (circa 1000€ al mese) con 3/4 stanze da letto
  
 * Essere servita dalla metro che porta in Melbourne (max 2km dalla stazione)
  
 * Avere centrelink, mall, scuole primarie, palestra e un minimo di centro
  
 * Avere dei parchi a portata di una passeggiata
  
 * Essere a 10/15km da un grande centro per questioni di lavoro/universita'
  
  
Tenendo presente questi criteri ho iniziato a usare un sito come realeastate.com.au per mostrare le case che fossero nel budget richiesto. Dato che Melbourne e' davvero immensa l'ho divisa in zone. Ho escluso la parte nord perche' e' parecchio congestionata se si vuole raggiungere il centro. Ho anche escluso la zona sud ovest perche' la conoscevo meno e non avevo tempo di visitarla. Alla fine la zona che conoscevo di piu' e che potevo permettermi di esplorare in meno tempo era la zona sud est.


 Per facilitarmi il compito ho creato una mappa su Google e ho iniziato a segnarmi le ispezioni delle case che volevo visitare. Purtroppo quando non si conosce una zona, e' davvero difficile capire dove andare a prendere casa, quindi ho iniziato a guardare le case a Cranbourne cercando di rimanere il piu' possibile fedele ai criteri da me scelti.  
### Il processo per affittare una casa

  
In Australia funziona come in altri paesi anglosassoni. Le case da affittare sono aperte a certi orari e disponibili per un'ispezione. Dopo aver visto la casa e' possibile mandare l'applicazione per l'affitto. Il modo piu' veloce per farlo e' usando un servizio a cui varie agenzie di renting si appoggiano ovvero [1form](http://1form.com).


 Usando 1form e' possibile mandare l'applicazione allegando tutti i documenti una sola volta. I documenti piu' importanti sono ovviamente quelli d'identita' e ovviamente una prova che hai uno stipendio o, come nel mio caso, basta avere un documento scritto dal datore di lavoro.


 Io ho mandato varie applicazioni e alla fine ho optato per la prima casa che mi ha dato l'ok. Le difficolta' del processo di scelta sono i vari viaggi da fare e saper scegliere la zona giusta rispetto ad altre zone. Non e' una cosa semplice se non si e' vissuti per un po' da queste parti. Una volta che ti danno l'ok per l'affitto bisogna pagare un mese in anticipo e versare un bond che viene gestito da un'azienda governativa quindi ne' il proprietario e nemmeno l'agenzia tengono i soldi del bond che generalmente equivale ad 1 mese di affitto.


 L'agenzia qui in Australia si occupa di tutte le comunicazioni con il proprietario. In alcuni casi, se l'agenzia e' buona, la soluzione e' ottimale. Io ad esempio non avevo la cassetta della posta e l'antenna della tv e mie stato installato tutto mandando una mail e senza pagare nulla.  
Utenze
------

  
Fare le utenze ha richiesto una telefonata di un'ora. Esiste un servizio che ti allaccia tutte le utenze chiamandoti direttamente. L'agenzia generalmente la propone e se avete fretta conviene, io ho fatto cosi' e ha funzionato. Luce e gas li ho fatti con la stessa azienda (AGL) e internet fibra ottica l'ho fatta con Dodo. Purtroppo avevo fretta e non ho potuto scegliere il meglio, ma va bene cosi'  
Arredare casa
-------------

  
Arredare la casa richiede tempo e costa. Io avevo bisogno del minimo indispensabile, ma alla fine sono riuscito a fare meglio grazie all'aiuto di una famiglia italiana che mi ha aiutato tantissimo nel portare la roba. Si, il problema piu' grande e trasportare la roba ed e' per quello che se potessi tornare indietro ora comprerei l'auto con il gancio traino. Ci sono tante persone che vendono mobili di tutti tipi ed e' molto facile affittarsi un carrello da agganciare alla macchina per andare in giro a caricare mobili usati.


 Io ho avuto fortuna e grazie a Francesca e Flavio sono riuscito a portarmi a casa in meno di 20 giorni:  
  
 * Frigo e lavatrice usati garantiti
  
 * Tavolo in legno con 8 sedie
  
 * Due letti singoli e un letto matrimoniale con materassi
  
  
Su facebook e [gumtree](http://www.gumtree.com.au/) e' possibile trovare roba usata a poca distanza da casa, ma la difficolta' e caricare. Molto spesso e' anche facile contrattare sul prezzo in quanto le persone hanno bisogno di fare fuori i mobili veloce in quanto hanno la consegna della roba nuova in giornata. (Si vede che qui non c'e' crisi). Inoltre non sempre la gente puo' rimanere in Australia, molte volte ci sono persone che devono far fuori tutto l'arredamento della casa perche' devono tornare in Europa a causa del visto che sta per scadere o non e' stato rinnovato. Se non hai fretta, puoi farti davvero dei buoni affari.


 Ora mi tocca trovare un divano e un mobile per la tv.  
Riassumendo
-----------

  
In 18 giorni sono riuscito a comprare macchina, affittare la casa, fare le utenze e arredare la casa per accogliere moglie e figli. Il bello dell'Australia e' che la burocrazia e' snella, ma molto snella, omparato all'Italia sembra di stare ai Confini della realta'.


  

