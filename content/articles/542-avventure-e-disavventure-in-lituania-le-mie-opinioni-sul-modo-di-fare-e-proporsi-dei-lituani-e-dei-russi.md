Title: Avventure e disavventure in Lituania: le mie opinioni sul modo di fare e proporsi dei lituani e dei russi
Date: 2009-05-18
Category: Lituania
Slug: avventure-e-disavventure-in-lituania-le-mie-opinioni-sul-modo-di-fare-e-proporsi-dei-lituani-e-dei-russi
Author: Karim N Gorjux
Summary: La mia più grande conquista qui in Lituani è giocare a calcio. Se mi riesce organizzo le partite trascinando Mirko altre volte invece vado da solo, tanto alla fine c'è sempre qualcuno[...]

![Sofia Loren che ride](http://www.karimblog.net/wp-content/uploads/2009/05/ridere-214x300.jpg "Sofia Loren che ride")[](http://www.karimblog.net/wp-content/uploads/2009/05/ridere.jpg)La mia più grande conquista qui in Lituani è giocare a calcio. Se mi riesce organizzo le partite trascinando [Mirko](http://www.mirkobarreca.net "Il blog di Mirko Barreca") altre volte invece vado da solo, tanto alla fine c'è sempre qualcuno che gioca e così facendo conosco anche nuove persone.


 Non riesco a capire il perché, ma tutte le volte che vado finisce che mi faccio male o **mi fanno male**. Una volta mi sono quasi distrutto un dito del piede, un altra volta ho preso un calcio, poi ho preso una storta alla caviglia e l'ultima volta mi sono preso nuovamente una storta alla caviglia (la stessa). Sia Rita che Julia, la fidanzata di Mirko, ormai si raccomandano: *almeno questa volta, torna a casa sano.*  
  
Mi è capitato di giocare con tanta gente diversa, alcuni sono davvero simpatici, altri invece riescono ad aprire il vaso di Pandora che c'è in te. Tu penserai, grazie tante! Anche in Italia è così. C'è chi ti sta sui coglioni e c'è invece chi ti è simpatico e ti trovi bene.  
Hai ragione, ma ho notato che chi mi sta simpatico generalmente è russo ed invece chi mi sta veramente sulle balle è... russo!

 Suona strano, vero? Invece è così, se dovessi usare dei termini "chimici", il lituano è basico. Non esprime emozioni, sta sulle sue, non capisci se gli stai sulle balle o se è timido, mentre il russo o lo odi o lo ami. Sarà che lui, rispetto al lituano esprime ciò che prova, ma con uno stile tutto nordico. Il russo o è acido o è basico. 

 I saluti: quando arrivi e quando te ne vai
------------------------------------------



 Il salutare i lituani mi spiazza sempre. Se entra un lituano nello spogliatoio della palestra i casi sono due:  
  
 3. ti guarda e non ti saluta
  
 6. ti guarda, ti saluta e obbligatoriamente ti stringe la mano
  
  
Al primo caso ci sono abituato, tutte le volte che succede mi manca sempre un po' di più l'Italia. Nel secondo caso invece mi sento spiazzato perché non capisco se dopo avermi stretto la mano la conversazione deve continuare o finisce tutto li. Poi tutto sto stringersi le mani non riesco a "farlo mio". Succede alle volte che mi salutano e ricambio il saluto, ma non mi preparo a stringere la mano e vengo colto alla sprovvista creando una situazione di imbarazzo tragicomica che denota ancora di più quanto io non faccia parte di questa cultura.


 Stranamente i lituani sono così anche tra di loro, all'inizio pensavo che il comportamento fosse una reazione spontanea allo straniero, ma invece è così sempre. Quando giochiamo a calcio, qualcuno se ne va a casa, ma nessuno lo saluta e lui non saluta nessuno. La situazione ha delle conseguenze anche per me perché quando me ne vado via io, provo a salutarli e ben pochi riescono a gestire il **miracoloso evento** ricambiando il saluto.


 Con i russi è diverso. In palestra sono diventato amico di un'istruttrice. Abbiamo iniziato a chiacchierare ed ora, tutte le volte che mi vede, si chiacchiera. Si chiama Alina ed è russa.


 Al campo da calcio, un ragazzo russo mi ha chiesto il numero di cellulare e ci sentiamo ogni volta che andiamo a giocare. Quando arriva al campo mi saluta "all'italiana" e mi saluta anche quando vado via. Anche i suoi amici russi sono amichevoli e gentili come lui, ma purtroppo c'è anche qualche stramaledetto che è totalmente l'opposto.  
Mi è capitato di giocare varie volte con gente molto violenta in campo dove l'unico obiettivo è vincere in qualsiasi modo, anche facendo del male. L'ultima partita che ho fatto, mi sono preso una storta dolorosissima a causa di un russo che mi ha spinto facendomi cadere. Gli ho detto di tutto, cinque minuti incessanti di insulti a lui e suoi consanguinei, oltre che ad una collaborazione di tutti i santi del calendario e divinità. Anche se non capiva nulla di quello che dicevo, **ha capito il senso di quello che stavo dicendo**. Non ha detto nulla, non si è scusato e ha fatto finta di niente, come se tutto fosse normale.


 Quando sono tornato in campo, avrei avuto voglia di spaccargli una gamba alla Hagi - Conte, ma alla fine il buon senso di un quasi trentenne ha avuto la meglio. Ho lasciato perdere perché, alla fine della fiera, qui gioco fuori casa, e dando una lezione ad un ragazzino, rischio di ritrovarmi 10 russi che la volta dopo mi aspettano al campo di calcio per usarmi al posto del pallone.


 I russi sono così, ho avuto delle belle esperienze e delle brutte esperienze. Dai lituani, a parte qualche rara occasione, ho sempre avuto le stesse reazioni. SMS non risposti, appuntamenti saltati, ma per fortuna qualcuno si salva. Una manciata di amici che sento regolarmente ci sono e penso che in futuro andrà anche meglio sia con loro sia con altri.


 Conclusione. Ho capito perché gli italiani all'estero (ma anche i cinesi, giapponesi, russi...) tendono a creare la mafia e stare uniti. Non che sia la migliore soluzione, ma vivere all'estero da straniero non è facile e posso capire anche se non approvo i metodi.  
Ho anche capito che quella che non chiamiamo "freddezza" in realtà è solo onestà. I lituani sono fatti a loro modo, i russi in un altro. Sinceramente a me manca molto l'Italia per questo, gli italiani, come gli spagnoli, riescono a dare un sorriso alle giornate. Non mi piace l'Italia politica, burocratica e ignorante, ma la nostra cultura nel cibo, nelle usanze e nel modo di fare è qualcosa che mi mancherà sempre e che,**secondo me**, ci contraddistingue in meglio rispetto ai modi di porsi dei popoli nordici.


 *Il riso è sacro. Quando un bambino fa la prima risata è una festa. Mio padre, prima dell'arrivo del nazismo, aveva capito che buttava male; perché, spiegava, quando un popolo non sa più ridere diventa pericoloso.  
Dario Fo*  




