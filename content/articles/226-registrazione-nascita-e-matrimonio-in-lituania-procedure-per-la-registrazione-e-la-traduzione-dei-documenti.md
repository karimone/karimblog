Title: Registrazione nascita e matrimonio in Lituania: procedure per la registrazione e la traduzione dei documenti
Date: 2006-10-03
Category: Lituania
Slug: registrazione-nascita-e-matrimonio-in-lituania-procedure-per-la-registrazione-e-la-traduzione-dei-documenti
Author: Karim N Gorjux
Summary: Scrivo questo articolo per aiutare quelle persone che come me hanno dovuto registrare il matrimonio e/o la nascita di un bambino/a in Lituania. La procedura nel caso più fortunato dura due giorni[...]

Scrivo questo articolo per aiutare quelle persone che come me hanno dovuto registrare il matrimonio e/o la nascita di un bambino/a in Lituania. La procedura **nel caso più fortunato** dura due giorni e si può fare solo a Vilnius. Io sono partito il Lunedì mattina (alle 4!) pensando di finire il Martedì, in realtà sono stato più fortunato e sono riuscito a concludere in un giorno solo: ecco come ho fatto.  
  
Partiamo dall'occorrente, dovete avere il certificato di nascita, il certificato di matrimonio e oltre al vostro passaporto anche quello di vostra moglie. Ho sentito dire che in realtà non è possibile custodire un passaporto di un'altra persona, ma in questo caso, in nome della semplicità, è meglio chiudere un occhio.


 Il certificato di nascita e di matrimonio devono essere apostillati. [L'apostilla](http://www.meltingpot.org/articolo1476.html) non è nient'altro che un allegato che verrà applicato ai certificati che rende il certificato valevole in tutti quei paesi che hanno aderito alla convenzione di Le Haye del 1961, tra cui ovviamente sono presenti l'Italia e la Lituania. Per far apostillare i documenti dovete andare prima a pagare la tassa in banca, ovvero 10€ circa per certificato, poi presentarvi a al ministero degli affari esteri lituano con la ricevuta della banca e i certificati da apostillare. Nel migliore dei casi vi ritroverete i documenti apostillati in giornata, nel peggiore dei casi dovete tornare la mattina dopo.


 I documenti apostillati dovete farli tradurre da un traduttore **ufficialmente riconosciuto dal'ambasciata italiana.** Questo significa che dovete mandare una mail o chiamare l'ambasciata e richiedere la lista dei traduttori riconosciuti e poi scegliervi qualcuno che vi traduca i documenti. Nel mio caso ho trovato Teresa tramite il mio amico [Massimo](http://www.massimoconsulting.com), ma per chi abita lontano da Vilnius come me il problema non è cosa da poco, a Klaipeda non c'era nessuno, nemmeno [Enrico](http://it.wikipedia.org/wiki/Enrico_Quinto) era ufficialmente riconosciuto dall'ambasciata per tradurre documenti ufficiali. Dato che io **non sonnecchio** avevo già mandato una copia via email dei documenti da tradurre a Teresa quindi una volta arrivato a Vilnius bisognava solo aggiungere alla traduzione il numero di serie delle due apostille.


 L'ultimo passo e il notaio. Ricordatevi di portare **due copie** per ogni certificato dal notaio che dovrà ufficializzare entrambi come copia conforme e allegarne la traduzione. In pratica i documenti vengono ufficializzati e solo dopo possono essere consegnati all'ambasciata italiana. L'ufficializzazione mi è costata 10€ per entrambi i documenti. (Quindi 4 copie)

 Arrivati a questo punto dovete vedere se avete ancora tempo per passare in ambasciata, io grazie a Teresa e alle scansioni mandate in anticipo sono riuscito a fare tutto nella mattinata e nelle prime ore del pomeriggio, cosa **possibile solo da queste parti dove non hanno orari rigidi come in Italia**, mentre l'ambasciata che Italia è, mantiene fede alle nostre tradizioni. Grazie a Teresa sono riuscito ad entrare all'ambasciata e a consegnare i documenti, alla fine non c'è molto da fare all'ambasciata. Consegnate le copie ufficializzate dei certificati e una bella fotocopia dei due passaporti, io ho anche fatto fotocopiare la carta d'identità per l'indirizzo di residenza perché l'ambasciata manderà tutto **al comune in cui siete residenti**. Dovete anche scrivere di vostro pugno due righe in cui richiedete la registrazione in Italia del matrimonio e della prole. Fatto questo potete andare a casa. Ricordatevi di portare anche i certificati originali che ovviamente vi **verranno restituiti.  
**  
Alcune note sono d'obbligo: la Lituania fa parte dell'Unione Europea, ma a mio parere solo politicamente e poco culturalmente. Una nazione che **non permette la doppia cittadinanza** ha ben poca voglia di "unirsi". Mia moglie avrà sicuramente problemi a diventare italiana senza perdere la cittadinanza lituana, ma mia figlia invece ha diritto alla cittadinanza italiana e **nessuno**, per legge (italiana), può toglierlela. Di fatto mia figlia ha due cittadinanze.


 Riguardo alle sovvenzioni lituane sui nascituri, per i bambini nati da padre o madre straniero **è necessario** un documento che provi che non si ottengono sovvenzioni "dall'altro" stato. Nel mio caso avrei dovuto richiedere all'ambasciata un documento che dichiara che io non ricevo nessuna sovvenzione dall'Italia per la bimba, questo documento ovviamente non esiste, ma esiste una procedura ovviamente più complicata e lunga che è ugualmente accettata dal governo lituano. Ho lasciato perdere, per 285€ più 28€ al mese per un anno posso anche chiudere un occhio (anche due visto che parto per l'Italia a breve).


 Concludo il post indicando il sito dell'[ambasciata italiana a Vilnius](http://www.italian-embassy.org.ae/ambasciata_vilnius). Per i contatti vi consiglio di scrivere ad Isabella Starnoni che tra l'altro è una persona gentilissima, siate pazienti che è sempre piena di lavoro. ;-)

