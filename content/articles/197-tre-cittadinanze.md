Title: Tre cittadinanze
Date: 2006-08-05
Category: altro
Slug: tre-cittadinanze
Author: Karim N Gorjux
Summary: Rompo il mio lungo silenzio per fare un piccolo appello, sono successe tante cose in questi giorni di cui vorrei informare i miei lettori, ma una in particolare ha fatto in modo[...]

Rompo il mio lungo silenzio per fare un piccolo appello, sono successe tante cose in questi giorni di cui vorrei informare i miei lettori, ma una in particolare ha fatto in modo che postassi di nuovo. Alcuni di voi sapranno che sono italo-francese visto che i mia madre è italiana e mio padre è francese e, grazie ad una convenzione tra le due nazioni, posso avere entrambi i passaporti e inoltre ho potuto anche evitare di fare il militare.


 Dato che mio padre ha ottenuto negli anni '70 la cittadinanza australiana vivendo per 13 anni nel continente dei canguri, ho approfittato per richiedere la cittadinanza per discendenza all'ambasciata australiana di Roma. Dopo quasi due anni hanno accettato la mia richiesta e mi hanno conferito la cittadinanza per discendenza. Il mio appello è quindi molto semplice: **esistono altre persone con più di due cittadinanze?** C'è qualcuno da qualche parte che ha **tre, quattro o più cittadinanze?** Fatemi sapere! [Contattatemi](http://www.karimblog.net/info/)!

  technorati tags start tags: [australia](http://www.technorati.com/tag/australia), [cittadinanza](http://www.technorati.com/tag/cittadinanza), [cittadinanze](http://www.technorati.com/tag/cittadinanze), [francia](http://www.technorati.com/tag/francia), [italia](http://www.technorati.com/tag/italia), [discendenza](http://www.technorati.com/tag/discendenza)



  technorati tags end 

