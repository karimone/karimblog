Title: Due mesi in Italia, la Lituania è ormai un ricordo lontano. Cosa fare del blog?
Date: 2011-06-06
Category: Lituania
Slug: due-mesi-in-italia-la-lituania-è-ormai-un-ricordo-lontano-cosa-fare-del-blog
Author: Karim N Gorjux
Summary: Ormai sono quasi due mesi che sono in Italia.

[![Punto interrogativo](http://www.karimblog.net/wp-content/uploads/2011/03/question_mark.jpeg "Domanda")](http://www.karimblog.net/wp-content/uploads/2011/03/question_mark.jpeg)Ormai sono quasi due mesi che sono in Italia.


 La Lituania inizia ad essere un ricordo che si sta affievolendo piano piano, il tempo è tiranno e quindi riesco a stare in contatto poco tempo con i miei amici (italiani) a Klaipeda mentre le conoscenze lituane invece le sento praticamente mai.  
  
Ogni tanto scrivo tramite facebook, ma sono sempre io a fare la prima mossa, ho ricevuto un messaggio da un ragazzo con cui giocavo a calcio e mi ha fatto piacere perché significa che non proprio tutti mi hanno dimenticato, ma con il tempo sarà inevitabile perdersi totalmente. Già era molto difficile vedersi o sentirsi quando ero a Klaipeda, ora che sono in Italia è inesorabile cadere nel dimenticatoio.


 Non è un problema, non mi da nemmeno fastidio, anzi, sapevo già che sarebbe andata a finire così basandomi sulla conoscenza del popolo lituano. Forse ci sarà qualche miglioramento a Settembre, quando i lituani passeranno più tempo con le luci accese che a respirare e quindi si ritroveranno con molto tempo da spendere in casa davanti al computer. Forse allora riceverò qualche messaggio in più, ma adesso è meglio lasciare i lituani godersi quel poco di sole che la natura concede loro.


 Ora il mio dubbio principale è cosa fare di questo blog. Ora che non sono più in Lituania non ho molto da scrivere sul tema principale che ha reso "famoso" karimblog.net. In realtà ho ancora almeno 5-6 articoli da scrivere su cose che ho visto e che non ho avuto voglia e tempo di riportare, ma finiti quei 6 articoli di cosa posso scrivere?

 In fin dei conti prima o poi la fine arriva a tutto. E' una legge naturale. E' giunto il momento di cambiare il blog e scrivere di qualcos'altro? Che ne pensi?

