Title: Descrivere la Lituania: il popolo lituano nel bene e nel male
Date: 2008-10-16
Category: Lituania
Slug: descrivere-la-lituania-il-popolo-lituano-nel-bene-e-nel-male
Author: Karim N Gorjux
Summary: Si è ormai detto di tutto dei lituani.

Si è ormai detto di tutto dei lituani.


 Gli uomini non ridono e sono grossi e brutti. Se dovessi dare un nome ed un cognome ad un lituano lo chiameresti "Massimo Pestaggio"

 Le donne sono belle, sensuali, provocanti, ma soprattutto inaffidabili.


 I lituani...


  ...sono inaffidabili, poco puntuali

 ...sono sempre ubriachi

 ...non sanno vendere, non hanno idea di come funzioni il commercio, ma soprattutto non gli interessa

 ...sono rozzi

 ...sono dei fenomeni nel basket

 ...sanno tante lingue

 ...sono ultra nazionalisti

 ...sono antipatici

 ... [non si interessando di politica](http://www.balticman.net/2008/10/14/luomo-meno-potente-del-mondo/ "Politica e lituani")

 ... etc. etc..


 Penso che alla fine **etichettare** il popolo lituano sia un impresa ardua e tutto ciò che ho scritto qua sopra può solo dare un idea vaga dei lituani rimasti in Lituania. Ricordate che molti lituani sono all'estero per lavorare, le cifre parlano di 1 milione di emigrati in un paese che conta 3,5 milioni di abitanti.


 Riguardo agli uomini i migliori, a **sentire le mie amiche**, sono tutti all'estero ed essendo veramente tanti fuori dal paese in pratica è rimasto solo il peggio.


 Forse ho un amico lituano, dico forse perché è vero che le persone sono inaffidabili, Martinas è uno di quei ragazzi miti e gentili che poco hanno a che fare con una frase del tipo "gli uomini lituani sono...". I lituani sono nordici ed è forse l'unica cosa che si può dire e come ogni popolo ha le sue sfumature. Comunque è vero che avere un amico lituano maschio è un [evento di proporzioni epiche](http://www.balticman.net/2007/06/02/le-stanze-della-mente/ "Sandro ha un amico lituano").


 Devo dire che la classica tendenza delle persone di etichettare e definire qualsiasi cosa crea solo atrito nella vita di tutti giorni. I lituani hanno una cultura particolare, **molto diversa dalla nostra** ed è bene conoscerla per non andare incontro a sofferenze inutili. Se devo, in piena obiettività, stilare una lista delle caratteristiche dei lituani (non completa), sarebbe questa:

 **In base alla percezione personale della mia vita lituana, i lituani sono, con le dovute eccezioni, così:**

 Gli **uomini** sono generalmente più imponenti di noi e tendono ad avere i capelli corti, si vedono raramente persone con i capelli lunghi. Diffidare dai gruppi di lituani che girano con una Svyturis in mano. I lituani in branco sono pericolosi. Esistono anche gli uomini d'affari e gli studenti, parlano inglese e sono socievoli, ma il nostro carattere estroverso, tipico dei mediterranei, li spiazza non poco.


 Delle **donne** ho [parlato e straparlato](http://www.karimblog.net/2006/10/02/le-donne-lituane-qualcosa-di-buono-ce/ "Donne Lituane") in passato. Bisogna cercarle bene e fidarsi moderatamente. **Tutto il mondo è** paese, non fatevi abbagliare dalla bellezza.


 **I lituani non sanno vendere**. Se entrate in un negozio il commesso raramente vi chiederà qualcosa o cercherà di vendervi qualcosa. Il commesso prende il suo stipendio e pensa che la sua unica funzione sia di non far diventare un self service il negozio. "Vuole questa borsa?" Bene, sono 100 litas, resto e scontrino. Visogaro. Commercianti italiani, fate bene attenzione prima di venire qui, portatevi delle commesse dall'Italia!

 I lituani **sono inaffidabili**. E chi non lo è? Avete mai provato a dare un appuntamento ad un sudamericano? Pensate davvero che gli Svizzeri siano tutti precisi? I Lituani possono stupirti perché comprano il regalo di compleanno 5 minuti prima di andare alla festa, ma possono essere puntuali e precisi. Dipende tanto da chi avete conosciuto, ma diffidate, almeno all'inizio.


 I lituani sono **diffidenti**. Si lega molto al loro patriottismo. I colori rosso, verde e giallo sono la loro vita, la loro identità. Sapete come si abbatte la diffidenza di un lituano? **Parlando in lituano**. 

 I lituani **non sono antipatici**, alcuni di loro sono simpatici e socievoli (soprattutto dopo il secondo bicchiere di vodka). 

 I lituani sono **fenomeni** del basket. E' vero. Mia moglie, quando eravamo in Italia, è andata all'oratorio del paese per vedere un allenamento di basket. L'ho convinta a fare due canestri e allora si è messa a fare tiri da tre. (da fuori area). Nove canestri su dieci. Italiani basiti... i lituani hanno il basket nel sangue.


 Sanno anche tante lingue, l'inglese lo sanno anche i bambini e sempre parlando di bambini arrivano a 7 anni parlando lituano, russo ed inglese. Il mio vicino di casa, quando abitavo a Kretingos Gatvé, aveva 8 anni e mi parlava in inglese, in casa parlava russo e con gli amici parlava lituano. Fantastico.


 Non ho altro da scrivere per ora, ma penso di aver fatto un po' di chiarezza sulla posizione Karim - popolo lituano. Mi piacerebbe però che i miei amici bloggers che hanno avuto significative esperienze lituane commentassero questo post. E' giusto che ci sia una pagina su internet che descriva il popolo lituano al di sopra delle proprie esperienze personali. Ammetto però che è veramente difficile.


