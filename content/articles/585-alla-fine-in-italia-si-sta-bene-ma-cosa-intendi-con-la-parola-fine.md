Title: Alla fine in Italia si sta bene! Ma cosa intendi con la parola fine?
Date: 2010-01-27
Category: Lituania
Slug: alla-fine-in-italia-si-sta-bene-ma-cosa-intendi-con-la-parola-fine
Author: Karim N Gorjux
Summary: Ieri chiacchieravo con un mio caro amico usando skype e si è entrati nella solita discussione "in Italia si vive bene o in Italia si vive male". Ormai facendo qualche calcolo posso[...]

[![Scappare!](http://farm4.static.flickr.com/3624/3506856206_e573b47561_m.jpg "Scappare!")](http://www.flickr.com/photos/joschmaltz/3506856206/)Ieri chiacchieravo con un mio caro amico usando skype e si è entrati nella solita discussione *"in Italia si vive bene o in Italia si vive male"*. Ormai facendo qualche calcolo posso dire di aver quasi passato 4 anni della mia vita in Lituania suddiviso in due soggiorni di cui quest'ultimo senza interruzioni.


 Ho notato, nel mio soggiorno all'estero, che il tempo ha aggrovigliato la mia cultura italico-cuneese al pensiero e alla cultura del paese che mi ospita. La prima naturale conseguenza di questa strana metaformosi è di realizzare che "**l'Italia è il paese in cui nasci, ma che nessuno ti obbliga anche a morirci**".  
  
Davide (che non ha un blog da linkare) vive a Bruxelles da circa 1 anno. Lui ha viaggiato molto più di me e ha visto tanti paesi più di me, lui è il primo a dire che l'Italia non è il paese che soddisfa appieno le sue esigenze ed esattamente come lui la penso io e tanti altri italiani che sono emigrati all'estero.


 Chi vive in Italia invece difende con le unghie la sua posizione e con tutti gli argomenti possibili ed immaginabili. "*Alla fine in Italia non si sta peggio di altri paesi*", "*Ho sentito una mia amica che vive in ... e mi ha detto che non si vive così bene...*"  
Se difendo la mia posizioni di emigrante mi metto nella stessa posizione di chi difende la sua posizione di italiano in Italia. Il principio non è difendere agli occhi degli altri le proprie scelte in modo da non realizzare consciamente il proprio fallimento, il principio alla base è: "**Ho il coraggio di fare delle scelte che possono rendere la vita migliore per me e per la mia famiglia?**"

 In [un mio articolo](http://www.karimblog.net/2009/11/18/ma-e-vero-che-i-giovani-hanno-difficolta-a-lasciare-il-proprio-paese-a-conoscere-nuove-culture-e-ad-aprire-la-propria-mente/) sui giovani italiani che non si muovono mai verso nuovi nidi, ho affrontato l'argomento in modo discretamente approfondito, ora ci ritorno per condividere alcuni miei pensieri con te che hai la pazienza e il coraggio di leggermi.


 Io e mia moglie stiamo meditando di espatriare nuovamente. Quando sono arrivato in Lituania era il 24 Agosto 2008 e pensavo di rimanere giusto il tempo necessario perché mia moglie finisse gli studi universitari. La convinzione era parto della memore ed epica mia prima esperienza in Lituania. Ora le cose sono decisamente cambiate. A me vivere qui piace e ci sto bene anche se oggi **a mezzogiorno** il termometro della mia macchina segnava -20 gradi. (Apro una parentesi: a differenza dell'Italia, qui nessuno fa una tragedia se è da 1 mese che il termometro non vede i numeri positivi, anzi tutti lavorano, vivono e si vedono anche i bambini uscire di giorno. Chiusa la parentesi.)

 La Lituania è un paese che ancora soffre, dopo vent'anni, le conseguenze del lungo regime sovietico. Stipendi bassi, disoccupaziopne, stato con poche disponibilità di mezzi ed è per questi e altri motivi che stiamo discutendo se spostarci e vivere in Svezia. Rita mi ha parlato dell'Italia come possibilità, ma mia moglie quando parla dell'Italia ne parla solo in modalità turistica perché sa (anche se non lo ammette esplicitamente) che tutto quello che riguarda burocrazia, tasse e litigate con i call center pesa sulle mie spalle. No grazie dico io.


 Un'altra cosa che mi trattiene molto dal venire in Italia è **la situazione tragica che i giovani stanno vivendo**. Ho avuto modo di parlare con una coppia dove lui è di Torino e lei è Lettone, ma viveva a Torino da ben 5 anni parlando persino un poco di piemontese. Entrambi lavoravano percependo un "buon" stipendio, da poco hanno una bambina e la loro vita si riduceva all'affanno nel superare la fatidica ultima settimana del mese.  
Hanno deciso di muoversi e, armati di coraggio e bagagli, si sono trasferiti a Stoccolma. Non è un caso isolato, gli italiani che si sono stufati di adeguarsi all'Italia dei furbi sono molti e mentre i Lituani sono 3 milioni e mezzo in Lituania e circa 1 milione all'estero, l'Italia può contare una "Italia in Italia" e una Italia sparsa per il resto del mondo.


 Se dovessi tornare in Italia, tutto sarebbe sulle mie spalle e il nostro futuro sarebbe in mano alla burocrazia italiana. Affittare un appartamento, arredarlo, diventare nuovamente residenti, cercare l'asilo per mia figlia, convertire la laurea di Rita come infermiera professionista che sia valevole in Italia (tempio richiesto 1 anno), allacciare internet, gas, elettricità. Tutte cose normali che in Italia diventano costose in termini di salute, denaro e tempo.


 C'è anche una cosa da sottolineare, chi mi ha detto che in Italia si sta bene (e che ora mi sta leggendo), ha 30 anni e un buon lavoro, vive in casa con i genitori, ha le sue donne e i suoi amici, non sa l'inglese e io capisco benissimo la sua visione dell'Italia come piccolo paradiso che rappresenta il mondo che si è creato. Il mondo è grande e senza dover andare in Australia, l'Europa è facilmente accessibile ed è praticamente il vaso di Pandora delle opportunità che aspetta solo di essere aperto.


 **Concludendo:** secondo il mio amico, a cui voglio bene anche se passo più tempo ad insultarlo che a parlarci, se una persona ha successo all'estero puo' avere successo anche in Italia. Permettetemi di obiettare con un pensiero molto personale: "L'Italia è un paese che **tutela il disonesto**, se sei furbo sei premiato, se righi dritto sei scemo e persino deriso. L'Italia non è il paese della meritocrazia, ma della mediocrità dei furbi." Una prova è lo [scudo fiscale](http://it.wikipedia.org/wiki/Scudo_fiscale) varato poche settimane fa che in pratica dimostra che **se paghi le tasse al 50% da onesto sei uno povero scemo**, se evadi e metti i tuoi soldi in Svizzera, lo stato ti premia con una mazzetta da ricettatore affiliato alla Caritas pagando solo il 5% di commissione.


 Link interessanti:
------------------

  
  
 * [Antonio Caporale](http://antonello-caporale.bcdeditore.it/) parla del suo libro "[Peccatori](http://www.bol.it/libri/Peccatori.-italiani-dieci/Antonello-Caporale/ea978886073605/)" nella puntata del 25 Gennaio 2010 di Melog. [Ascoltala qui](http://www.radio24.ilsole24ore.com/radio24_audio/100125-melog.mp3).



  - [Mollo Tutto](http://nuke.mollotutto.com/): sito per chi ha deciso di dare una svolta alla sua vita


  - Il blog di [Simone Perotti](http://www.simoneperotti.com/), autore del libro "[Adesso Basta!](http://www.simoneperotti.com/wp/?page_id=408)"
  


