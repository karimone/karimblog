Title: Può lo straniero lamentarsi del paese che lo ospita? Certo che può! Anzi, deve!
Date: 2010-11-08
Category: altro
Slug: può-lo-straniero-lamentarsi-del-paese-che-lo-ospita-certo-che-può-anzi-deve
Author: Karim N Gorjux
Summary: La risposta alla domanda varia molto dall'interlocutore. Se andiamo da Bossi e gli chiediamo se un senegalese in Italia può lamentarsi dell'Italia, la risposta è scontata: "Che se ne torni al suo[...]

[![](http://www.karimblog.net/wp-content/uploads/2010/11/straniero.jpg "straniero")](http://www.karimblog.net/wp-content/uploads/2010/11/straniero.jpg)La risposta alla domanda varia molto dall'interlocutore. Se andiamo da Bossi e gli chiediamo se un senegalese in Italia può lamentarsi dell'Italia, la risposta è scontata: "*Che se ne torni al suo paese se non gli va bene!*"

 Mi sono ritrovato varie volte a lamentarmi dell'Italia o della Lituania in svariate occasioni della mia vita. Se lo scrivo sul blog o su un forum la maggior parte delle volte vengo attaccato come se io fossi il senegalese davanti a Bossi. Non ti piace l'Italia?! Vattene. Non ti piace la Lituania!? Tornatene a casa! Che senso ha una risposta del genere? Se una persona ha scelto di vivere in un posto nel mondo, farci crescere la famiglia, lavorarci, **pagare le tasse** e così facendo contribuire alla crescita del paese che lo ospita, non ha anche lui il diritto di lamentarsi se c'è qualcosa che non gli piace?  
  
Qualche giorno fa ho fatto qualcosa di cui mi sono pentito amaramente, ovvero riportare l'articolo sui [problemi burocratici del mio cognome](http://www.karimblog.net/2010/10/14/follie-della-lituania-non-posso-dare-a-mio-figlio-il-mio-cognome/) sul famoso forum di intreccio culturale "ita-lituanico". Tra le varie risposte è arrivata subito la cretinata nazionalista di una lituana che si fa chiamare "Estate":  

>   
> *Perchè non torni a vivere in Italia?  
> Anche nel mio cognome lituano ci sono 2 lettere lituane : Č e Ė ( nei documenti italiani vengono scritte come C e E), mica anche io devo fare una guerra burocratica contro l'Italia?*  
> 

 A parte il fatto che "Estate" non ha capito la differenza tra un semplice carta d'identità che ti viene data in Italia come straniero e una trascrizione alle anagrafe di un nascituro, ma ciò che mi lascia perplesso è il "*Perché non torni a vivere in Italia?*"

 Non mi è possibile fare nessuna critica senza che qualcuno mi consiglia di tornarmene "*a casa*", come se fosse la cosa più semplice del mondo. Sbaracca tutto l'appartamento, prepara moglie, figlio, figlia, vai prima in Italia a preparare la casa, cerca un lavoro, l'asilo, le pratiche burocratiche... tutto questo perché non è mio diritto da italiano residente in Lituania di lamentarmi per una legge stupida.


 Qui in Lituania pago le tasse anche se la Lituania ha fatto la porcata di alzare le aliquote e la sodra (ovvero il nostro inps) in corso d'opera. Mia figlia va all'asilo e mia moglie ha studiato qui e io spendo i soldi che guadagno qui in Lituania contribuendo a fare a girare l'economia. Mi devo per forza sentire un cittadino di serie b solo perché sono straniero?

 "Estate" continua con i suoi suggerimenti filosofici per una vita più serena:

 
>   
> *Anche per me in Italia non piacciono tante cose, ma non mi permetto mai insultare il Paese che mi ospita.  
> *

 "*Non mi permetto di insultare il paese che mi ospita*". E come io andassi a casa di un mio amico e dato che sono ospite non posso lamentarmi se mi fuma in faccia, tocca il culo di mia moglie o parla male di mia sorella. Non potrei mai lamentarmi perché non mi "permetterei mai di insultare l'amico che mi ospita".   
Ma questi sono ragionamenti inculcati dal regime sovietico o c'è un velo di masochismo sotto?

 
>   
> *Se ti trovi così male sia in Lituania, sia in Italia, perchè non torni in Francia o in Algeria?*  
> 

 Il mondo è mio quanto tuo e il sole sorge per me quanto per te. Quindi io "*non torno*" in Francia e solo perché mi chiamo Karim non significa che abbia qualche relazione con l'Algeria (ma poi perché l'Algeria e non l'Egitto). Se poi ho voglia emigrare nuovamente e lasciare questa valle di lacrime che ha le stesse tentazioni suicide dei suoi abitanti, vado dove **cazzo** mi pare esattamente come i lituani che si spargono per tutto il mondo fregandosene beatamente delle sorti del loro paese.


 La lamentela, la critica è un diritto irrinunciabile e io consiglio vivamente agli stranieri in Italia, ma agli stranieri in generale di non tacere di fronte alle ingiustizie solo perché *"ci si sente"* cittadini di serie b come la ragazza lituana qui sopra citata che vive in Italia. 

 E' inutile inoltre fare continue comparazioni con il paese di provenienza, altrimenti un somalo che si è fatto a piedi l'Africa per arrivare in Europa non potrebbe mai aprire bocca vista la situazione della Somalia. 

 Su questo blog, la comparazione con l'Italia viene intavolata ad ogni critica ed osservazione che si fa alla Lituania come se la comparazione fosse una risposta valida a compensare, ma soprattutto a legittimare qualsiasi anomalia.


 E un po' come se [Angela Merkel](http://it.wikipedia.org/wiki/Angela_Merkel) si permettesse di sparare ad un ebreo visto che Hitler aveva fatto molto di peggio bruciandone sei milioni nei forni crematori.


 Cosa ne pensi? Ti senti un cittadino di serie b? Anche tu ti lamenti dell'Italia o del paese che ti ospita?

