Title: E alla fine sei sempre uno straniero mio caro uzsienieti!
Date: 2011-01-14
Category: Lituania
Slug: e-alla-fine-sei-sempre-uno-straniero-mio-caro-uzsienieti
Author: Karim N Gorjux
Summary: Eh si, sei sempre uno straniero in Lituania e lo posso garantire sulla mia pelle. Forse non è nemmeno colpa mia, sarà che non parlo bene la lingua e non capisco proprio[...]

[![Straniero](http://www.karimblog.net/wp-content/uploads/2011/01/wall3.jpg "uzisienieti")](http://www.karimblog.net/wp-content/uploads/2011/01/wall3.jpg)Eh si, sei sempre uno straniero in Lituania e lo posso garantire sulla mia pelle. Forse non è nemmeno colpa mia, sarà che non parlo bene la lingua e non capisco proprio tutto, **ma se potessi quantificare lo sforzo per integrarmi tra i lituani con un valore** sarebbe almeno un centinaio di volte superiore all'apertura dei lituani verso di me.


 Inizio ad essere stanco di essere uno straniero o come dicono qui un *uzsienietis* che tradotto letteralmente significa al di là del muro.   
Probabilmente è la stessa parola con cui era identificato lo straniero nel medioevo ed è concettualmente interessante riflettere proprio sulla derivazione di questa parola nelle due lingue, se per noi uno straniero deriva da *strano* che non ha un'accezione negativa a priori, in lituano invece indica una vera e propria separazione culturale. Al di là del muro, quel muro che a differenza di quello di Berlino non vuole saperne di cadere.  
  
Ti starai chiedendo sicuramente com'è il rapporto con mia moglie, in fin dei conti anche per lei sono un uzsienietis. A dire il vero con lei è diverso, perché da anni ormai sto mettendo in atto un piano per portarla *"al di quà del muro*". Ho iniziato ad insegnarle l'italiano, farle vedere i film italiani, farle conoscere i sapori e la cultura italiana ed è questo l'unico modo per integrarsi con una moglie di queste parti.


 Lungi via da me l'idea di giudicare i lituani, ma alcuni loro comportamenti sono davvero strani. Ti faccio qualche esempio per darti un'idea di cosa significhi essere italiano in Lituania. Intanto molti sanno più o meno chi sono a Klaipeda, uno con la mia faccia che si accorge che sta piovendo in leggero ritardo grazie ai suoi 15cm in meno della media nazionale non passa inosservato in un paesino come Klaipeda. (Paesino? In realtà conterebbe 180.000 abitanti, ma le facce sono sempre le stesse)

 Mi è capitato di conoscere persone, nei pochi posti di svago di Klaipeda, che **sapevano chi fossi ancora prima di essermi presentato**. Delle amicizie che mi sono fatto in questi anni e che infestano la lista dei miei amici su Facebook praticamente nessuno lo sento con una certa frequenza e con nessuno di essi ho un rapporto di amicizia.


 Fino ad un mese fa giocavo a calcio con un gruppo di amici di tutte le età, sto aspettando che faccia un po' più bello e farò forse qualche altra partita con loro prima di partire, ma non mi stupisco più di niente e se non li vedessi mai più non mi meraviglierei. Questo gruppo di amici si trova due volte alla settimana ad un piccolo campo da calcio per giocare cinque contro cinque; il campo è gratuito, si trova adiacente ad una scuola pubblica e quindi si può giocare senza pagare. Da mesi queste persone si trova il Mercoledì alle 18 e la Domenica alle 10, non si organizzano mai, i giorni sono fissati e a volte sono in 6 e altre volte in 15. Organizzare una cosa del genere e farla durare così tanto con i miei amici in Italia penso sia un'autentica utopia.


 Questa è la prima stranezza. La seconda a cui non mi abituerò mai e che praticamente quando è finita la partita, se ne vanno praticamente via tutti senza nemmeno salutarsi. Io non dico che bisogna scambiarsi effusioni come al gay pride, ma giusto due parole, due commenti, forse parlarsi nel dopo partita davanti ad una birra in un pub. Assolutamente no, s**aluto imborghesito lituano con la stretta di mano e via, senza voltarsi, verso la macchina**.


 Delle 20 persone che ho conosciuto a giocare a calcio, ho il numero di telefono di una persona soltanto che mi ha persino mandato gli auguri a capodanno ed, mi spiace scriverlo, ma è russo. Della metà di queste persone non so nemmeno il nome.


 Altra stranezza è che i rapporti che più si possono considerare d'amicizia li ho con le donne. Non che possa frequentare agevolmente donne con una moglie e due figli, il tempo libero è quello che è, ma soprattutto non posso farlo perché inevitabilmente ed inesorabilmente la natura tende sempre a fare il suo corso, ma alla fine sono le lituane le persone con cui ho stretto amicizia e con cui potrei stringere rapporti di amicizia degni di questo nome.


 I russi invece sono una categoria a parte. Sono mentalmente più vicino agli italiani, ovvero sono allegri, sorridenti e pericolosamente falsi, mentre **i lituani dispensano sorrisi come se fosse merce proibita dalla DEA** e sono completamente trasparenti nel mostrarti il ribrezzo o la stima che hanno verso nei tuoi confronti. Con i russi mi sono fatto delle risate, ma anche con loro diventa difficile avere rapporti. Molti russi non parlano lituano e io di iniziare a parlare il russo per fare un piacere a loro non ne ho proprio la voglia (anche se sarebbe estremamente più utile del lituano).   
Mi è capitato di incontrare un ragazzo che mi ha chiaramente chiesto di parlare in inglese perché il lituano non lo capiva e mi è anche capitato di ritrovarmi con un signore di 60 anni che non riusciva a parlare con me perché non sapeva il lituano. (E' uno dei portieri nel gruppo che gioca a calcio)

 Parlando con i lituani mi è successo che l'interlocutore abbandonasse la chiacchierata senza apparente motivo, mi è successo più volte e con persone diverse. All'inizio pensavo che fosse colpa mia, forse parlo troppo male e non si capisce cosa dico e il lituano preferisce dare forfait invece di tentare di capire cosa dico. L'ultima volta mi è capitato con mia suocera e da li ho capito che noi italiani siamo dei chiacchieroni favolosi e i lituani non sono mentalmente a parlare e parlare per troppo tempo. Il lituano preferisce farsi beffe di qualsiasi imbarazzo godendosi il suo meritato e filosofico silenzio in qualsiasi occasione e contesto.


 Circa un mese fa sono stato una settimana in ospedale, nella camera che mi ospitava c'erano due letti e quando ho preso posto ero da solo. Dopo un giorno è arrivato un signore sulla cinquantina, ha posato la sua roba, si è sdraiato sul letto e non ha detto niente. Abituato agli autoctoni ho rispettato il silenzio per più di un'ora. Alla fine ho rotto il ghiaccio altrimenti penso sarebbe andata avanti per giorni.  
Con il secondo ospite ho ripetuto l'esperimento, ho parlato solo dopo un paio d'ore, questa volta era un ragazzo di 28 anni che doveva essere operato. La sera parlavamo del più e del meno, ma facevo sempre attenzione a non sforare i limiti di sopportazione lituana congedandomi in rispettoso silenzio dopo non più di venti minuti.


 Tornando al discorso sul rapporto di amicizia dei lituani con i stranieri ho parlato un ragazzo libanese che vive da qualche anno a Klaipeda e che è sposato con una russa da cui ha da poco avuto una bambina. Gli ho chiesto se le sue esperienze con i locali sono simili alle mie ed il mio amico che parla bene russo mi ha detto che al di fuori dei tre ragazzi che sono amici di famiglia e li frequenta più attivamente, tutti quelli che frequenta giocando a calcio od uscendo la sera sono dei perfetti "quasi sconosciuti".


 Quindi conosco un sacco di persone, ci scambiamo il numero, cerco sempre di parlare lituano, ma alla fine non si instaura niente. A calcio mi chiamano italas, maccaronai e quando segno un gol esce fuori il discorso mafia, Corleone, Vito. C'è niente da fare, ma lo stereotipo è la prima cosa che i lituani vedono e io capisco profondamente l'essenza delle varie Little Italy sparse per il mondo. Infatti qui a Klaipeda alla fine della fiera, l'unico amico che ho ha il passaporto come il mio.


 A te l'infausto compito di giudicare questo popolo.


