Title: Consigli Lituani
Date: 2006-07-19
Category: Lituania
Slug: consigli-lituani
Author: Karim N Gorjux
Summary: Ho ricevuto una mail da un ragazzo che a Dicembre si trasferirà a Kaunas per passare il suo anno da Erasmus. Mi lusinga sapere che Google mi considera come un probabile "esperto"[...]

Ho ricevuto una mail da un ragazzo che a Dicembre si trasferirà a Kaunas per passare il suo anno da Erasmus. Mi lusinga sapere che Google mi considera come un probabile "esperto" della Lituania proponendomi tra i primi posti nelle ricerche di curiosi esploratori dei Baltici. Io non voglio deludere google e nella maggior parte dei casi rispondo pure (gratis).  
  
Kaunas ha la fama di essere una città con alto tasso di criminalità, queste sono voci che mi sono giunte e non le ho mai potute verificare, Kaunas è una città che non ho mai visto, non ci sono mai stato. Prima o poi però dovrò sopperire a questa mia lacuna perché Kaunas è la seconda città più grande della Lituania ed è stata anche la capitale di questo piccolo e grazioso stato. Di consigli quindi ne posso solo dare in generale e non mirati ad alcune città in particolare, Vilnius è molto bella, ma anche li ci sono stato giusto un paio di giorni... Klaipeda è bella, ma è noiosa. Palanga... è bella, ma è affollata. Statevene a casa, che è meglio ;-)  
Cosa venite a fare in Lituania adesso che ci sono appena 25 gradi con il sole, un bel vento rinfresca le giornate e di pioggia non se ne vede da giorni, non state bene in Italia con i vostri 35 gradi all'ombra? :-D  
Ok. Basta scherzare, i miei consigli per chi vuole venire qui sono semplici, ma basiliari. Per prima cosa non portatevi la macchina targata Italia, sicuramente sparirà e non grazie al Mago Silvan, conviene comprarsi un catorcio qui. Prendetevi un bell'alloggio in affitto e non è il caso che sia in centro, prendetelo anche un pò fuori, in periferia, informatevi solo che la zona sia tranquilla. Ultimo consiglio: **imparate la lingua Lituana**. Lasciate perdere il russo o l'inglese, se volete avere a che fare con questa gente e apprezzarne ogni lato è meglio che iniziate a scoprirne la cultura partendo dalla lingua. Solo così vi integrerete e capirete sin da subito che le ragazze chiamano gli amici e i fidanzati con la stessa parola: draugas.  
Già sapere questo è tutto dire.


  technorati tags start tags: [caldo](http://www.technorati.com/tag/caldo), [integrazione](http://www.technorati.com/tag/integrazione), [kaunas](http://www.technorati.com/tag/kaunas), [lingua](http://www.technorati.com/tag/lingua), [lithuania](http://www.technorati.com/tag/lithuania), [consigli](http://www.technorati.com/tag/consigli)



  technorati tags end 

