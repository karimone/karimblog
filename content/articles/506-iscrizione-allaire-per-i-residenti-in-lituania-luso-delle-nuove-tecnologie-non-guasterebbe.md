Title: Iscrizione all'aire per i residenti in Lituania: l'uso delle nuove tecnologie non guasterebbe
Date: 2008-11-20
Category: Lituania
Slug: iscrizione-allaire-per-i-residenti-in-lituania-luso-delle-nuove-tecnologie-non-guasterebbe
Author: Karim N Gorjux
Summary: Senza entrare nei dettagli particolari e noiosi, ieri ho dovuto fare una telefonata per chiedere semplicemente un codice. Un semplice codice di trasferimento per un dominio, il signore interpellato, titolare di una[...]

Senza entrare nei dettagli particolari e noiosi, ieri ho dovuto fare una telefonata per chiedere semplicemente un codice. Un semplice codice di trasferimento per un dominio, il signore interpellato, titolare di una piccola azienda informatica, mi ha chiesto di fargli mandare un fax. Un fax? Deve mandare un codice al mio cliente e vuole che gli si mandi una richiesta via fax? Mai sentito parlare della posta elettronica? Se vuole le posso far portare un documento timbrato con la ceralacca e potrei venire a cavallo fin da lei... Incredibile!  
  
**Iscrizione all'aire**  
Essendo residente in Lituania e dato che sono libero professionista lituano, ho il dovere-diritto di iscrivermi all'aire. Ok. Sono d'accordo, facciamo questa cosa e togliamoci definitivamente dall'Italia. Mi informo tramite l'ambasciata Italiana di Vilnius e scopro che devo andare fino a Vilnius per fare una firma, si perché devo compilare un modulo e firmarlo.


   
  
[Visualizzazione ingrandita della mappa](http://maps.google.it/maps?f=d&saddr=Minijos,+klaipeda&daddr=Vytauto+Gatve+1+,+vilnius&hl=it&geocode=&mra=ls&sll=55.18574,23.201205&sspn=1.659043,4.54834&ie=UTF8&ll=55.185141,23.203125&spn=3.764745,6.591797&z=6&source=embed)  




 Quindi per una semplice firma devo farmi 616km e perdere praticamente tutta la giornata. Non è molto più semplice mettere sul [sito dell'ambasciata](http://www.ambvilnius.esteri.it/) tutti moduli pronti da scaricare e compilare? Se cliccate sulla sezione "modulistica" non è presente nulla, niente di niente. Non contento, sono andato a farmi un giro sui sito delle altre ambasciate europee e quasi tutte permettono di iscriversi all'aire compilando un modulo scaricabile e mandando il tutto per posta. Se poi guardiamo i siti delle tre repubbliche baltiche, il sito dell'ambasciata a Vilnius è quello messo peggio.


 Usare il computer fa risparmiare tempo, carta ed è soprattutto ecologico. Mi basta scaricare il documento, firmare il tutto, scansionare e mandare via posta elettronica allegando tutte le scansioni di qualsiasi altro documento e senza uscire di casa, senza spendere soldi della benzina e senza perdere tutta la giornata.


 Morale della favola: **investire** nell'informatica e nella telecomunicazione non sono soldi buttati via. Quando il nostro paese inizierà ad investire nell'informatica e nel formare il personale all'uso delle nuove tecnologie?

 Ho scritto [un articolo](http://www.gorjux.net/2008/11/19/sito-web-davvero-conviene-e-a-cosa-mi-puo-servire/) sul mio [blog professionale](http://www.gorjux.net) riguardo l'investimento sui siti web.


