Title: La dura vita del blogger
Date: 2011-06-18
Category: altro
Slug: la-dura-vita-del-blogger
Author: Karim N Gorjux
Summary: Ne hai sempre una a scrivere su un blog. Arriva un commento dove ti si riempie di merda e di insulti, la gente ti contatta in continuazione per avere informazioni, ma soprattutto[...]

Ne hai sempre una a scrivere su un blog. Arriva un commento dove ti si riempie di merda e di insulti, la gente ti contatta in continuazione per avere informazioni, ma soprattutto la gente pensa di poterti leggere la vita e giudicare in base ai tuoi scritti. Questa è la dura vita del blogger, se ti esponi lo devi accettare, non c'è santo che tenga.


 **L'ultimo mio articolo ha permesso a qualcuno di sfogarsi su di me accusandomi di incoerenza.**  
Parlo male di una cosa, poi ne parlo bene, poi ne riparlo male... e il problema dove sta? Se la cosa che ti sembra giusta prima poi ne parlo per i suoi difetti allora non va più bene a te, ma può andare bene a qualcun'altro. Dove sta la verità, ma siamo sicuri che tra tutto ciò che scrivo ci sia una verità o forse sono tante verità o forse niente di tutto ciò che scrivo è vero?  
  
**Come si fa a sapere cosa è vero o cosa non è vero?** Se nell'arco di questi anni ho rivalutato e rivisto tante cose è solo una questione mia personale e profondamente soggettiva. Quando Gianluca Nicoletti mi ha intervistato su Melog 2.0 io ho subito premesso che la mia visione è soggettiva, è solamente un punto di vista perché posso esprimere solo quello che conosco e non lo esprimo per far conoscere. L'ho scritto e riscritto, non sono un giornalista. 

 Come se tutto questo non bastasse, ti ritrovi i tuoi articoli su un sito di informazioni su chi vuole andare in giro per trombare. Non che la cosa mi dispiaccia, ma non avere nemmeno il link back ai miei articoli profondamente sudati, **a me sembra una vera vigliaccata**.


 Per prima cosa ho chiesto di mettere il link back al mio articolo originale e il titolare del sito ha aggiunto il mio nome e cognome come se scrivessi per il loro sito. Dato che io cono loro non ho niente a che fare, vorrei che rimuovessero il tutto, ma sembra che la gente possa fare davvero quello che vuole.


 Non è che per caso sei un avvocato che vuole fare un'opera buona per il sottoscritto? :-)

