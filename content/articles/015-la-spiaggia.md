Title: La spiaggia
Date: 2004-08-23
Category: Lettonia
Slug: la-spiaggia
Author: Karim N Gorjux
Summary: La spiaggia è gratuita, poco affollata con sabbia finissima, non ci sono pietre e l'acqua e' bassa per decine di metri (oltre che essere fredda). Il posto dove sono stato io e'[...]

La spiaggia è gratuita, poco affollata con sabbia finissima, non ci sono pietre e l'acqua e' bassa per decine di metri (oltre che essere fredda). Il posto dove sono stato io e' molto bello, non e' il centro di Jurmala, ma leggermente in periferia. Ho avuto conferma guardando le persone in spiaggia di un'altra piaga che con l'alcolismo e la prostituzione colpisce questo paese. La malnutrizione: uomini e donne tendono a sfiorire gia' dopo i 30. Chi si può permettere una palestra qui? Chi si può permettere di spendere i soldi per mangiare come Dio comanda? :-/

 [![Spiaggia di Jurmala](http://www.kmen.org/images/spiaggia.jpg)](http://www.kmen.org/images/spiaggia.jpg)



