Title: Mini post di un sabato pomeriggio invernale qualunque
Date: 2009-01-24
Category: altro
Slug: mini-post-di-un-sabato-pomeriggio-invernale-qualunque
Author: Karim N Gorjux
Summary: La lontananza si fa sentire, sapete cosa più mi manca di Cuneo? Le montagne. Oggi per me sarebbe stata una di quelle giornate da passare in montagna con Greta e Rita, prendersi[...]

La lontananza si fa sentire, sapete cosa più mi manca di Cuneo? Le montagne. Oggi per me sarebbe stata una di quelle giornate da passare in montagna con Greta e Rita, prendersi una cioccolata calda e divertirsi per la neve. Senza fare grandi viaggi, mi sarebbe bastato fare un giro a [Limone](http://it.wikipedia.org/wiki/Limone_Piemonte), a due passi dalla Francia, e mi sarai passato un bel sabato da vero piemontese :-)

 Venticinque anni passati con le montagne attorno a te, non si dimenticano e infatti mi mancano.  
[![Cuneo e le montagne](http://farm4.static.flickr.com/3511/3221981445_61d0462881_m.jpg)](http://www.flickr.com/photos/kmen-org/3221981445/ "Cuneo e le montagne di karimblog, su Flickr")



