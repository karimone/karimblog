Title: A casa mia vigono leggi lituane
Date: 2007-08-22
Category: Lituania
Slug: a-casa-mia-vigono-leggi-lituane
Author: Karim N Gorjux
Summary: Ormai sono 6 giorni che i miei suoceri sono ospiti a casa mia e lo rimarranno fino a Domenica 26 Agosto. I miei suoceri sono persone gentili e a modo, o almeno[...]

Ormai sono 6 giorni che i miei suoceri sono ospiti a casa mia e lo rimarranno fino a Domenica 26 Agosto. I miei suoceri sono persone gentili e a modo, o almeno questo sembra, perché non riesco a seguire più del 10% dei discorsi che fanno e, dopo che ci siamo fatti qualche ora di macchina, ho un mal di testa lancinante che mi ricorda il perché sono scappato dalla Lituania. Rita purtroppo non capisce, ha avuto un marito che con **tanta pazienza** le ha insegnato l'italiano e questo tipo di problemi non fanno parte del suo mondo.


 **Imparare il lituano?** Neanche per idea, non ha nessuna utilità, al massimo mi [studio il russo](http://www.assimil.it/prodotto.php?isbn=8886968116), ma dubito che mi servirebbe per capire i miei suoceri dato che tra di loro **parlano sempre solo lituano**. Tanto vale che loro imparino l'inglese (dato che serve) o ancora meglio l'italiano che è rinomato ed importante (e io posso oziare come la mia indole mi suggerisce).


 Sinceramente non vedo molte soluzioni al problema, a parte quella dello studio del russo che può sempre essere utile nei paesi dell'ex [blocco sovietico](http://it.wikipedia.org/wiki/Paesi_ex-comunisti).


