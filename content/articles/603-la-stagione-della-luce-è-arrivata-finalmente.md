Title: La stagione della luce è arrivata, finalmente!
Date: 2010-05-03
Category: Lituania
Slug: la-stagione-della-luce-è-arrivata-finalmente
Author: Karim N Gorjux
Summary: Ormai è primavera inoltrata, le giornate sono calde e superano i dieci gradi, non piove e c'è il sole tutto il giorno. L'alba si presenta già alle sei del mattino, la luce[...]

[![](http://farm4.static.flickr.com/3090/3161512924_aa3e8f4771_m.jpg "L'alba")](http://www.flickr.com/photos/jamesjustin/3161512924/)Ormai è primavera inoltrata, le giornate sono calde e superano i dieci gradi, non piove e c'è il sole tutto il giorno. L'alba si presenta già alle sei del mattino, la luce entra prepotentemente dalla finestra e dei tre io sono l'unico che si sveglia a causa del sole. L'appartamento che affittiamo non ha delle tapparelle, ma ha delle piccole tendine bianche a vetro che servono giusto per non far vedere ai vicini di casa cosa stai facendo.  
  
All'asilo di Greta c'era un'epidemia di varicella, Greta sembrava essere l'unica bambina a non aver preso la malattia, ma alla fine anche lei ha ceduto, è da Giovedì che la teniamo in casa per tenere sotto controllo le pustole che vanno e che vengono. Ovviamente di tutti i periodi che Greta poteva ammalarsi, questo è il peggiore perché Rita ha gli ultimi due esami prima della tesi. Venerdì ha dato un esame e tra qualche giorno avrà l'ultimo, dato che siamo tornati in Lituania principalmente per farle finire gli studi, ufficialmente dal 4 Giugno siamo svincolati dal pretesto di stare qui.


 Lungi via da me l'idea di preparare le valigie ed andarmene, ma tra 1 mese le cose saranno molto più semplici da gestire in casa dato che Rita sarà meno impegnata e di conseguenza meno stressata.


