Title: Foto panoramiche di Klaipeda con la nuova Kodak M1063
Date: 2009-01-14
Category: Lituania
Slug: foto-panoramiche-di-klaipeda-con-la-nuova-kodak-m1063
Author: Karim N Gorjux
Summary: Per Natale io e Rita abbiamo deciso di spendere pochi soldi in regali, Greta è troppo piccola per apprezzarli e l'abbiamo accontentata con un Puzzle nuovo. Con i parenti abbiamo deciso di[...]

Per Natale io e Rita abbiamo deciso di spendere pochi soldi in regali, Greta è troppo piccola per apprezzarli e l'abbiamo accontentata con un Puzzle nuovo. Con i parenti abbiamo deciso di tenerci i soldi in tasca o al massimo di farci qualche pensierino.  
  
Il Natale però è stata l'occasione per mandare in pensione la nostra vecchia compatta [Casio Exilim EX-Z40](http://www.exilim.de/it/exilimzoom/exz40/) dopo 4 anni di onorato servizio. L'idea iniziale era di acquistare qualcosa di "più serio", ma un po' per il costo e soprattutto per il vero utilizzo che facciamo della macchina fotografica in casa, abbiamo optato per una compatta della Kodak in offerta: la [Kodak EasyShare M1063](http://wwwit.kodak.com/global/it/service/products/ekn035308.jhtml?pq-path=13050).  
Dato che io e Rita siamo davvero due totali inesperti di fotografia e che il soggetto principale di ogni nostra foto è Greta, i motivi che ci hanno portato a scegliere la Kodak sono stati:  
  
 3. I 10 Mega Pixel (contro i 4 della Casio)
  
 6. Il colore rosso
  
 9. L'uso della stessa scheda di memoria della vecchia macchina fotografica
  
 12. Il costo contenuto (poco più di 100€)
  
  
Con la nuova compatta ho fatto due foto panoramiche dal palazzo in cui vivo: Una sulla città e una sul porto. A voi giudicare il risultato.  
  
  
[![Behind me](http://farm4.static.flickr.com/3222/3147988620_274bbfa69c.jpg)](http://www.flickr.com/photos/kmen-org/3147988620/ "Behind me di karimblog, su Flickr")  
  
  
[![From my window](http://farm4.static.flickr.com/3196/3147155931_2cfdfb700e.jpg)](http://www.flickr.com/photos/kmen-org/3147155931/ "From my window di karimblog, su Flickr")

