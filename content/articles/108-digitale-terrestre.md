Title: Digitale Terrestre
Date: 2005-10-23
Category: altro
Slug: digitale-terrestre
Author: Karim N Gorjux
Summary: Cosa penso del digitale terrestre? E' una cazzata, ma una grandissima idea di B. ovvero fare la sua televisione con i soldi degli Italiani, praticamente una RAI targata Mediaset. Qual'è il futuro?[...]

Cosa penso del digitale terrestre? **E' una cazzata**, ma una grandissima idea di B. ovvero fare la sua televisione con i soldi degli Italiani, praticamente una RAI targata Mediaset. Qual'è il futuro? Ovviamente la televisione su internet che oltretutto c'è già! Basta avere la banda larga e si possono già fare alcune cosette interessanti. Vediamo in dettaglio.  
  
Sul sito [CoolStreaming](http://www.coolstreaming.it/ "Coolstreaming.it") potete vedervi (quasi) qualsiasi **cosa in TV gratis**. Questo perchè mentre il derby della Madonnina qui è a pagamento e anche criptato sul DT in Cina ovviamente è gratis quindi basta prendere lo streaming dalla Cina e ciao ciao B. **Il tutto ovviamente è legale**

