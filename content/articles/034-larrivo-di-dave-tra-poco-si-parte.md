Title: L'arrivo di Dave, tra poco si parte
Date: 2005-01-10
Category: Lettonia
Slug: larrivo-di-dave-tra-poco-si-parte
Author: Karim N Gorjux
Summary: Potete vedere nuove foto cliccando qui. Vi consiglio di usare il tasto "Next in Set" sulla destra per vedere le foto nell'ordine in cui vorrei mostrarle.

Potete vedere nuove foto cliccando [qui](http://www.flickr.com/photos/kmen/). Vi consiglio di usare il tasto "Next in Set" sulla destra per vedere le foto nell'ordine in cui vorrei mostrarle.


 Davide è arrivato, il cesso era intasato e ci siamo fatti due risate a cercare di farlo tornare al vecchio e tanto amato funzionamento. Purtroppo ogni cosa che usavamo, dal lavandino alla doccia peggiorava la situazione. Ci ha salvati la proprietaria utilizzando un prodotto Irakeno che ha stappato i tubi in meno di un minuto. Qualcosa di favoloso: apre una bottiglia di alluminio, la mette sullo scarico della doccia, preme un pulsante e tutto torna normale. Siamo contenti e profumati!

 Mercoledì lascerò Riga con Davide. Oggi incontro una mia vecchia amica e domani Ilga. Kristina l'ho vista oggi. Come tutti quelli che conosco avranno intuito, ritorno (o rimango) single ed è tutto normale. Il difficile sarà andarsene da qua, ma oramai sono talmente abituato che non è un problema. Sono sempre tornato e il futuro riserva un mucchio di sorprese...


 Il sito ha un mucchio di contatti e ne sono felice, ricevo qualche commento dal solito Matteo e vari amici. Ricevo molte e-mail. Ho cambiato il layout del sito per dare un po' di aria nuova, spero che siate sempre contenti dei miei articoli, posso vantarmi, pavoneggiarmi dell'esperienze che sto vivendo che ho vissuto e che spero di vivere in futuro, sono insegnamenti che nessuna scuola ti può dare. La mia esperienza (ricordo sempre l'aforisma di Wilde: l'esperienza è il nome che gli uomini danno ai propri errori), mi aiuta a passare dei momenti che se fosse la prima volta che vengo qui sarei giù di morale tanto da toccare il fondo della [Fossa delle Marianne](http://it.wikipedia.org/wiki/Fossa_delle_Marianne) (il link è per Matteo), invece il mio umore è molto alto grazie anche alla presenza di Davide che ha rotto la mia solitudine temeraria nel porto baltico per eccellenza.


