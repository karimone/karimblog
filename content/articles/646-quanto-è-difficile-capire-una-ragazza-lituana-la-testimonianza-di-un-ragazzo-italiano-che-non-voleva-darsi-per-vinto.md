Title: Quanto è difficile capire una ragazza lituana: la testimonianza di un ragazzo italiano che non voleva darsi per vinto
Date: 2011-07-25
Category: Lituania
Slug: quanto-è-difficile-capire-una-ragazza-lituana-la-testimonianza-di-un-ragazzo-italiano-che-non-voleva-darsi-per-vinto
Author: Karim N Gorjux
Summary: Ricevo una lettera interessante da un ragazzo che mi chiede delle risposte che proprio non so dare. Cosa può esserci alla base delle stranezze nel comportamento di una ragazza lituana? A volte[...]

[![](http://www.karimblog.net/wp-content/uploads/2011/07/donna-arrabbiata.jpg "Donna Arrabbiata")](http://www.karimblog.net/wp-content/uploads/2011/07/donna-arrabbiata.jpg)Ricevo una lettera interessante da un ragazzo che mi chiede delle risposte che proprio non so dare. Cosa può esserci alla base delle stranezze nel comportamento di una ragazza lituana? A volte ti aspetti un sorriso ed invece è imbronciata, offesa, arrabbiata per qualcosa che non hai la minima idea di cosa sia. Cerchi di indagare ponendo domande, cercando di indovinare qual é il problema e senza pensare alla stupidità della situazione. Ma la soluzione non c'è e l'unica cosa da fare è aspettare [senza far niente](http://it.wikipedia.org/wiki/Wu_wei "Wu Wei"). Almeno questo è ciò che farei io al posto di questo ragazzo.


   

> Sembra stupido, in effetti lo é...da giorni leggo il tuo blog...a dire il vero mi sento un po' sedicenne e da tanto che non mi sentivo così...  
> Magari ti spiego che così sembro un folle che blatera cose folli.


 Vivo a Londra da un anno e per otto mesi ho convissuto con la mia attuale ragazza, che é tornata in Italia e lì resterà ed abbiamo intrapreso questo complesso rapporto a distanza, nell'ultimo periodo di "convivenza" con la mia ragazza ho conosciuto una ragazza lituana, un semplice rapporto di amicizia che consideravo tale, finché un giorno ci trovammo a parlare per ore in un pub e finimmo per baciarci, due volte.  
Non pensavo di piacerle...non pensavo che mi sarebbe piaciuta, ma è stata così accattivante, intelligente, turbolenta...come un fulmine nella mia attuale vita alquanto annoiante e stressante.


 Abbiamo intrapreso una cordiale relazione di amicizia visto che lei non voleva immischiarsi nei miei affari...  
da quel giorno è diventata schiva, giudiziosa nei miei confronti, pesante...io tutto quello che volevo era magari riuscirla a vedere...ma a volte si negava pure...o magari mi dava appuntamenti ai quali non si presentava...


 Sai come siamo fatti noi Italiani...passionali cocciuti a volte stupidi...e niente...ho iniziato a pressarla ad avere ragioni motivi...siamo andati avanti così per un mese, un mese in cui l'avrò vista si e no due volte...senza baciarla una volta...senza sfiorarla nemmeno...  
a dire il vero tutta questa freddezza mi uccideva, mi faceva sentire rifiutato, snobbato...magari è parte del suo carattere...non sono riuscito a spiegarmelo ed ad accettarlo...ed ho dato di matto varie volte...nella migliore tradizione melodrammatica partenopea...sceneggiata. Ero fuori di me...  
Ergo...morale della favola è tutto finito terminato estinto...e sinceramente non mi do molta pace...


 Ora sinceramente non so perchè ti scrivo questa mia...sembra davvero di un sedicenne che parla di problemi di cuore ad un amichetto...a dire il vero, nemmeno ti conosco magari mi deriderai, era giusto per sapere...in realtà loro sono così? Fredde macchinose, testarde?  
Sul serio non mi do pace...ma in fin dei conti non è altro che un altro post fra blogger...


