Title: Ho poco tempo
Date: 2006-08-24
Category: altro
Slug: ho-poco-tempo
Author: Karim N Gorjux
Summary: Purtroppo le foto della bimba non le metto pubbliche, solo qualcuna ogni tanto e spero che comprendiate. Vi informo che stiamo tutti e tre bene, la bimba è alta 54cm e pesa[...]

  
[![](http://static.flickr.com/85/223590016_564087ad00_m.jpg)](http://www.flickr.com/photos/kmen-org/223590016/ "photo sharing")  
  
Purtroppo le foto della bimba non le metto pubbliche, solo qualcuna ogni tanto e spero che comprendiate. Vi informo che stiamo tutti e tre bene, la bimba è alta 54cm e pesa 3.7kg, sta bene è in perfetta salute e siamo tutti felicissimi di quanto ci sta capitando. Vi ringrazio di cuore per i vostri commenti.




