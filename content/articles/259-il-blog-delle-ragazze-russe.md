Title: Il blog delle ragazze russe
Date: 2006-11-08
Category: altro
Slug: il-blog-delle-ragazze-russe
Author: Karim N Gorjux
Summary: Molto interessante! Elena Ivanova, l'autrice del sito ragazzerusse.org, ha anche un suo blog. Tra l'altro ha anche scritto un libro che vende online.

Molto interessante! Elena Ivanova, l'autrice del sito [ragazzerusse.org](http://www.ragazzerusse.org), ha anche un suo [blog](http://ragazzerusse.blogspot.com/). Tra l'altro ha anche scritto un [libro](http://www.ragazzerusse.org/libro_donne_russe.html) che vende online.


