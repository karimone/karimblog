Title: Dall'aeroporto
Date: 2008-01-04
Category: Lituania
Slug: dallaeroporto
Author: Karim N Gorjux
Summary: Un saluto dall'aeroporto di Vilnius, dove il wifi internet è gratis e il pranzo al "Kavine" costa il doppio che in qualsiasi altro posto. Siamo qui, tutti e tre, con 45kg di[...]

Un saluto dall'aeroporto di Vilnius, dove il wifi internet è gratis e il pranzo al "Kavine" costa il doppio che in qualsiasi altro posto. Siamo qui, tutti e tre, con 45kg di valigie, un passeggino e Greta che dorme profondamente. Se tutto va bene tra 1 ora facciamo il checkin. Ci si rivede in Italia.


