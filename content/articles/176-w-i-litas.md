Title: W i Litas!
Date: 2006-05-16
Category: Lituania
Slug: w-i-litas
Author: Karim N Gorjux
Summary: Oggi l'UE ha deciso che la Slovenia adotterà l'euro, ma la Lituania no. Inutile dire che qui sono tutti contenti, visti i precedenti negli altri stati. La bocciatura sembra sia dovuta al[...]

Oggi l'UE ha deciso che la Slovenia adotterà l'euro, ma la Lituania no. Inutile dire che qui sono tutti contenti, visti i precedenti negli altri stati. La bocciatura sembra sia dovuta al livello dell'inflazione, un solo e misero 0,01%. W i Litas!  


  
  
Link: [Ansa](http://www.ansa.it/main/notizie/awnplus/economia/news/2006-05-16_1166748.html)  


  
 technorati tags start tags: [economia](http://www.technorati.com/tag/economia), [euro](http://www.technorati.com/tag/euro), [litas](http://www.technorati.com/tag/litas), [lithuania](http://www.technorati.com/tag/lithuania), [inflazione](http://www.technorati.com/tag/inflazione)

  
  
 technorati tags end 

