Title: Impresa in Lituania
Date: 2007-04-21
Category: Lituania
Slug: impresa-in-lituania
Author: Karim N Gorjux
Summary: Mettere su un'impresa in Lituania è relativamente facile, la burocrazia lituana è uno scherzo paragonata alla nostra. Il problema non è però mettere su l'impresa, ma farla andare. Gli imprenditori lituani che[...]

Mettere su un'impresa in Lituania è relativamente facile, la burocrazia lituana è uno scherzo paragonata alla nostra. Il problema non è però mettere su l'impresa, ma farla andare. Gli imprenditori lituani che conosco io, gioiscono a far fuori le loro società e a tornare in Italia. Chi rimane lo fa a scapito della sua tranquillità perché di guadagni se ne vedono pochi.


 Ancora non capisco perché mi aggredivano tanto quando dicevo queste cose. L'unico modo per far soldi in Lituania è andare lassù con 1 o 2 milioni di € da investire nella produzione di qualcosa, sfrutti la loro mano d'opera e vendi in Italia. In quali altri paesi si fa cosi'?

