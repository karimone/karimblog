Title: Grazie Teresa
Date: 2006-10-02
Category: Lituania
Slug: grazie-teresa
Author: Karim N Gorjux
Summary: Se non fosse stato per questa ragazza piccolina che corre come un treno, non sarei riuscito a fare così veloce. Ho registrato il matrimonio e la nascita di Greta all'ambasciata italiana, se[...]

Se non fosse stato per questa ragazza piccolina che corre come un treno, non sarei riuscito a fare così veloce. Ho registrato il matrimonio e la nascita di Greta all'ambasciata italiana, se tutto va bene tra 20 giorni verrà tutto registrato nel comune di Centallo.  
  
Ringrazio [Massimo](http://www.massimoconsulting.com) per l'ospitalità, ma soprattuto per il contatto di Teresa che è riuscita nell'impresa burocratica **ad ambasciata chiusa** il Lunedì! Ad essere sincero sono stato parecchio fortunato nel trovare una segretaria gentile all'ambasciata italiana che mi ha permesso di entrare anche se le visite erano chiuse per il pubblico

 A Vilnius ho avuto modo di fare visita al *Sole Luna* il famoso locale-ritrovo degli italiani. Ho anche visitato il dipartimento linguistico dell'università di Vilnius, scuola di Teresa e di Ieva, la ragazza di Massimo. Il dipartimento è molto carino, soprattutto la gente giovanile rendeva il posto molto ospitale, ma anche la cucina della mensa ha fatto la sua bella figura. 

 Ora sono stanco e penso che mi metterò a sonnecchiare.


