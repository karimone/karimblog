Title: Quella strana sensazione che l'estate stia finendo pur non essendo mai cominciata
Date: 2009-08-08
Category: Lituania
Slug: quella-strana-sensazione-che-lestate-stia-finendo-pur-non-essendo-mai-cominciata
Author: Karim N Gorjux
Summary: Luglio è il mese più caldo da queste parti, il sole tramonta dopo le 11 di sera e fa abbastanza caldo da far sudare e lamentare i lituani. Ieri anche ha fatto[...]

[![](http://farm3.static.flickr.com/2656/3777801829_9c0ed1ff21_m.jpg "Una tela su Klaipeda")](http://www.flickr.com/photos/kmen-org/3777801829/)Luglio è il mese più caldo da queste parti, il sole tramonta dopo le 11 di sera e fa abbastanza caldo da far sudare e lamentare i lituani. Ieri anche ha fatto caldo, il termometro ha superato i 25 gradi e in alcuni locali hanno persino acceso l'aria condizionata, anche Rita ha sofferto, ma sinceramente, abituato ad altri tipi di afa, io sto più che bene.  
  
A dire il vero io non ho ancora sentito afa, non ho mai avuto la necessità di un ventilatore e non mi sono mai lamentato. Secondo i miei canoni e penso anche i tuoi, qui l'estate non è mai arrivata del tutto, è stata semplicemente una primavera calda che ora sta volgendo al termine, ma senza mai esagerare.


 Mi è capitato di andare in spiaggia più volte, ma raramente ho fatto il bagno; ricordo invece che durante le mie visite estive in Liguria, il bagno in acqua non era una scelta, ma un obbligo. Era l'unico modo per trovare un po' di sollievo.


 Un esempio: ieri sera sono andato in un locale tipico che si trova in mezzo alla campagna non tanto lontano da dove abito, siamo andati verso le 19, ma mi sono portato il maglione che ho indossato prontamente dopo le 21. A me piace, mi ricorda le serate in villeggiatura in montagna e detto molto sinceramente preferisco difendermi dal freddo che dal caldo. Hai capito ora perché non ho nessuna intenzione di andare in Italia ora? :-)

