Title: E' la Lituania meta di turismo sessuale? Per molti italiani si
Date: 2011-06-09
Category: Lituania
Slug: e-la-lituania-meta-di-turismo-sessuale-per-molti-italiani-si
Author: Karim N Gorjux
Summary: Anche se continuo a ribadire che le donne lituane sono istruite, indipendenti e caratterialmente molto forti, c'è chi ancora confonde la Lituania con i paesi dell'est nella situazione sociale degli anni '80.[...]

[![](http://www.karimblog.net/wp-content/uploads/2011/06/un-sacco-bello.png "Un sacco Bello")](http://www.karimblog.net/wp-content/uploads/2011/06/un-sacco-bello.png)  
Anche se continuo a ribadire che le donne lituane sono istruite, indipendenti e caratterialmente molto forti, c'è chi ancora confonde la Lituania con i paesi dell'est nella situazione sociale degli anni '80. Lo stereotipo mi ricorda molto il film [Un sacco bello](http://it.wikipedia.org/wiki/Un_sacco_bello) di Carlo Verdone dove in un dei tre personaggi del film, interpreta Enzo un ragazzo sulla trentina che sta organizzando un tour del sesso a Cracovia e non riesce a trovare un compagno di viaggio.  
  
Illuminante una frase di "Enzo" all'amico che non vuole più partire:  

>   
> E poi scusa,  
> avemo fatto il passaporto  
> avemo fatto il visto  
> avemo comprato le carze de seta  
> avemo comprato le biro!  
> 

 Il film è del 1980, quindi i ragazzi di ora che vogliono visitare la Lituania, trasportati da vampate ormonali insoddisfatte, sono al corrente di leggende e millantate varie dei nostri connazionali che **sono il più lontano possibile dalla realtà.**

 **Le donne lituane battono culturalmente un uomo medio italiano 5 a 0** quindi vuol dire che le grandi fighe che si sbattono i turisti italioti non sono nient'altro che quella percentuale di peripatetiche **presenti in qualsiasi angolo della terra**. Ma allora perché un italiano turista sessuale non si prende una prostituta da 200€/ora e si avanza tutti i fastidi di un viaggio? Perché andare in Lituania, fare la "pesca facilitata" e sentirsi un novello casanova, vale molto più di qualsiasi scopata. **Niente ha più valore del rafforzamento del proprio ego**.


 Allora cosa si potrebbe rispondere a una richiesta di informazioni, doverosamente riportata senza modifiche come questa? "Te sei portato le biro e le carze de seta?"

 
>   
> Siamo un gruppo di amici ci siamo permessi di scriverti una mail per capirci di più. 

 Noi vorremo venire a fare un soggiorno dove tu conosci benissimo. Vorremo sapere innanzi tutto dove si prende l'aereo con prezzi modici possibilmente consigliarci un albergo dove eventuaslmente possiamo portare le ragazze che abbordiamo e se le fanno entrare.


 Poi spiegaci se le ragazze si abbordano facilmente e se nei nait club ci sono spettacoli interessanti e se le ragazze che ci piacciono quando costano per portarle a letto.


 Penso che hai capito il tutto parlaci nella risposta a questa mail del turismo sessuale sai siamo un grppo di amici e ci vogliamo divertire per 5 6 giorni come dobbiamo organizzarci e se ne vale la pena.


 Scusami se approfitto ma se abbiamo modo di conoscerci ti faremo una bellissima impressione.


 Cari saluti **Enzo**.  


