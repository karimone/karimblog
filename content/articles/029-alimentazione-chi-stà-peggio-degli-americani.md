Title: Alimentazione: chi stà peggio degli americani?
Date: 2005-01-03
Category: Lettonia
Slug: alimentazione-chi-stà-peggio-degli-americani
Author: Karim N Gorjux
Summary: Per me Auce è stata una settimana di strazio alimentare, i Lettoni e forse tutto il nord europa mangiano in un modo da far venire il voltastomaco. Il loro fisico è sottoposto[...]

Per me Auce è stata una settimana di strazio alimentare, i Lettoni e forse tutto il nord europa mangiano in un modo da far venire il voltastomaco. Il loro fisico è sottoposto ad un continuo sconvolgimento dell'apparato digerente e i loro cesso parla pure in calabrese :-)

 I lettoni tendono a mangiare a ore non prefissate: quando hanno fame mangiano. Forse questo puo' sembrare anche intelligente a primo acchito, ma per svariati motivi non lo è. La conseguenza a mio avviso peggiore,è che tendono a togliersi l'appetito mangiando qualsiasi cosa. E' facile notare come i giovani siano snelli e slanciati, ma questo non dura con il tempo. Le giovani fanciulle dagli occhi blu in portano con sè i primi sintomi di sfioritura già dopo i 20 anni, questo per le fortunate ragazze che hanno un metabolismo che le permette di metabolizzare velocemente le fritture, la maionese, il ketchup, il pane, le patate... etc. Ho conosciuto alcuni cugini di Kristina, i figli di sua zia sono 3: un ragazzo di 14 anni (quasi obeso), una ragazza di 15 anni (obesa) e l'ultima ragazza una diciasettenne da sfilata di moda. Lei è fortunata spero che duri.


 Non esiste la pasta (e questo è forse un bene), poca verdura (penso a causa dei prezzi), giusto un po' di frutta. La tendenza è quella di mangiare molta carne, per lo più molto salata e questo penso dipenda dalla qualità della stessa. La carne è cucinata saltata con delle uova a fare delle sottospecie frittelle o cucinata a polpetta "mastodontica" naturalmente anch'essa fritta. Sono molto goloso delle polpette della mamma, ma quelle lettoni non mi vanno giù: troppo salate, troppo grasse, troppo pesanti. Sulla tavola non mancano mai le fette di pane bianco e nero confezionati, la maionese e il ketchup. Non esiste il nostro (invidia mondiale) olio extravergine di oliva e questo è molto male! I formaggi locali lasciano a desiderare, basta dire che la fontina e il parmigiano che ho portato dall'italia è durato ben poco, lo mangiavano come se fosse un biscotto. :-)

 Non esisteva l'acqua, loro bevono sempre il the. All'inizio mi ha lasciato un pò perplesso, usano uno speciale bollitore elettrico e bevono the tutto il giorno a tal punto da far passare un'inglese per un disidratato. Il the ostituisce qualsiasi bevanda a noi cara (coca-cola?), niente succhi di frutta, svariati dolci. Una sera Kristina mi ha cucinato gli spaghetti, è un peccato non aver portato i poster perchè con una colla del genere sarebbero rimasti attaccati al muro fino alla prossima era glaciale, a parte gli scherzi, li ho mangiati solo per disperazione. Cotti e stracotti sono stati messi in padella e farciti di formaggio (per fortuna con il "mio" parmigiano e la "mia" fontina) e poi serviti con la MAIONESE e il KETCHUP. Per loro e normale, adesso capite perchè il loro cesso parla calabrese? :DDD

 Ora che sono in peldu street torno a seguire i consigli del "Barry Sears", qui il salmone non manca ed è meglio approfittarne.


