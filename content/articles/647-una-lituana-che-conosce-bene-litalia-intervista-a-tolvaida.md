Title: Una lituana che conosce bene l'Italia: intervista a Tolvaida
Date: 2011-09-07
Category: Lituania
Slug: una-lituana-che-conosce-bene-litalia-intervista-a-tolvaida
Author: Karim N Gorjux
Summary: Trovare una lituana che conosca bene gli italiani non è facile, ma tra le varie amicizie che ho coltivato nella mia permanenza in Lituania, una ragazza in particolare potrebbe dare una buona[...]

[![](http://www.karimblog.net/wp-content/uploads/2011/09/CIMG8197.jpg "Tolvaida")](http://www.karimblog.net/wp-content/uploads/2011/09/CIMG8197.jpg)Trovare una lituana che conosca bene gli italiani non è facile, ma tra le varie amicizie che ho coltivato nella mia permanenza in Lituania, una ragazza in particolare potrebbe dare una buona visione di come le lituane vedono e vivono l'Italia.


 Insomma un cambio di prospettiva completamente diverso da quello che sei abituato a leggere su karimblog.net. Tolvaida ha 28 anni, ha un bimbo di pochi mesi e vive in Calabria. Parla benissimo l'italiano faceondo particolare attenzione al congiuntivo e condizionale, conosce così bene la nostra lingua da capire persino il dialetto calabrese.  
  
### Ciao Tolvaida, da quanto tempo frequenti l'Italia e perché?

  
L'Italia la conosco da ben 12 anni. La prima volta ci sono venuta nel 1999 con il gruppo folkloristico dei miei genitori. Un anno dopo c'e stato un'altro festival ed è capitata la classica storia tra la ragazza lituana e il ragazzo italiano; il primo bacio, il primo amore grande grande che ti fa fare pazzie e perdere la testa :)

 Ricordo ancora oggi l'immenso dolore e lacrime quando dovevamo partire, pregavo Dio che il nostro pullman si rompesse. Avevo 17 anni. Dovevo ancora finire la scuola e mi aspettavano gli esami di maturità; una volta tornata in Lituania passavo il giorno a preparare gli esami e la notte imparavo l'italiano.


 Il ragazzo che mi ha fatto perdere la testa era molto testardo, mi chiamava in continuazione e piangendo ci giuravamo di stare insieme, di mettere su famiglia, ma non pensavo che il Signore avesse già deciso per noi. La vita ci ha separati per molti anni. Dopo scuola ho iniziato l'Universita, dopo di che ho cominciato a lavorare e quindi a fare la "donna in carriera". Ad ogni modo, in tutti questi anni con l'Italia ho sempre avuto dei bei raporti: mia sorella viveva e vive tutt’ora in Italia, per un'anno ho studiato all’Universita di Messina grazie al programma Erasmus senza parlare di tutti i viaggi e le vacanze che ho trascorso nella bella penisola durante questi anni.  
### Rispetto alla Lituania (escludendo il meteo) cosa ti piace di più dell'Italia?

  
E' proprio il meteo che dell'Italia non mi piace, adoro il tempo lituano, sono abituata ad avere le 4 stagioni che per me sono stupende. Le mie stagioni preferite sono l'autunno e la primavera proprio quelle che ormai in Italia non esistono più. Amo la natura pura, profonda, selvatica e molto molto verde oltre i meravigliosi colori che la natura ci regala durante l'autunno.  
Anche le primavere lituane sono uniche, non ho trovato da nessuna parte in Italia una primavera simile a quella Lituania.


 Ed il tanto odiato inverno lituano? C'é qualcosa di piu bello di quando con gli amici vai a sciare o semplicemente ti fai una passeggiata sulla costa ghiacciata del Mar Baltico, o nel bosco? Le viste che ti regala la natura d'inverno sembrano una cartolina, **tu dici il freddo, ma il freddo non c'é ne più**. Le case, gli uffici, le macchine e i locali sono ben riscaldati; se non c'e troppa umidità poi tranquillamente stare anche fuori a -15 gradi.


 **Sembrerà un paradosso, ma l'anno più freddo della mia vita è stato l'anno scorso in Calabria**. Le loro case tradizionali sono fatte per non sentire il caldo d'estate e quindi con i materiali freddi come marmo, pietra e metallo. Le finestre lasciano passare gli spifferi quindi d'inverno si muore di freddo e d'estate si muore lo stesso dal caldo.


 Dell'Italia mi piace (una pausa lunga lunga ed un sospiro) **il cibo**. Adoro cucinare e qui posso trovare dei prodotti di alta qualità, **mi piace la loro cultura paesana**, la vita nei piccolo paesi gira intorno alla piazza centrale, qui ci si parla, si discutte, si prende il caffé, si leggono i giornali, si saluta e si sorride. Hai la garanzia del buon umore per tutto il giorno. Quando qualcuno mi chiede cosa dell'Italia mi piace di piu, io rispondo "mio marito!". **Mi sono trasferita in Italia non per il paese ma per la persona che amo.**  
### Cosa ti piace di meno dell'Italia?

  
Ovviamente la mentalità. Il loro passato è stato molto piu "semplice" rispetto al nostro e questo si sente tantissimo. I bambini della mia generazione sono cresciuti da soli o nei casi migliori con i nonni; i nostri genitori lavoravano tanto, veramente tanto. Purtroppo non avevano altra scelta,** la vita allora era veramente dura**. Non dimenticherò mai le lacrime di mia mamma quando lasciava me e mia sorella, ancora piccoline, all’asilo nido settimanale. Ricordo quando d'estate tutti i noi bambini del quartiere aspettavamo fuori fino a mezzanotte che tornassero dal lavoro i nostri genitori.


 **A 3 anni gia lavavo i pannolini della mia sorellina**, a 5 anni gia sapevo da sola friggermi un’uovo, a 6 anni senza problemi attraversavo la citta per andare a scuola. la vita ci ha fatto crescere una pelle dura, sembra che non ci sia una cosa che ci può far perdere l’equilibrio, le donne lituane sono molto emancipate, gli uomini, uno su due, a 25 anni già ha la sua attività. Sapere 5 lingue, avere 2 lauree e una carriera di successo alle spalle, per un lituano è solo il punto di partenza.


 **Un’italiano di oggi invece perde tante possibilità di questa vita già solo perché si aspetta qualcosa, ma nel frattempo la mamma gli sbuccia la pera e gli lava le mutande.**  
Le condizioni di vita, l’educazione, le virtù formano la propria qualità di vita. Inutile nominare i loro punti deboli, leggendoli non sarebbero d'accordo! Noi invece li sappiamo bene. Non è ne colpa loro ne nostra che siamo molto diversi. Potrei scrivere un libro sulle cose che non mi piaciono dell'Italia, ma a cosa serve? Io vivo in Italia, la persona che amo è italiana e persino mio figlio è italiano, devo imparare ad accettare la terra che alla fine sono stata io a scegliere.


 La mia fortuna è che io vivo con un uomo che non rientra in nessuno stereotipo italiano, è di mentalità aperta oltre che autonomo e maturo. Sa gestire perfettamente la mia integrazione in Italia e grazie a lui, l'Italia è diventata ancora più piacevole. Di difetti ne ha anche molti la Lituania, ma la cosa importante è concentrare il proprio punto di vista sulle cose positive, e lasciare perdere le cose che non puoi cambiare; per ora io mi sono concentrata sulla mia famiglia, come meglio crescere mio figlio, essere una buona mamma e moglie. I problemi dell’Italia li lascio risolvere a Silvio :)  
### Attualmente vivi in Italia, sei contenta della tua scelta, credi di poter costruire un futuro per la tua famiglia in Calabria?

  
**Oggi come oggi non c'è posto migliore per crescere un piccolo bambino**: l'aria pura di montagne, niente traffico, il mare a 2 passi, il cibo sano dalla campagna dei miei suoceri... cosa ci può essere di meglio? Per quanto riguarda il futuro, non voglio proprio pensarci ora, cerco di vivermi la vita quotidianamente, altrimente impazzirei. Le condizioni del mondo academico italiano sono molto tristi.


 Oggi vedo mio figlio crescere circondato da tanto tanto amore, sto cercando di regalargli una vita piena di sorrisi e di buoni valori facendogli vedere il bell’esempio della sua famiglia cosa che nessuna scuola o Università potrà mai dargli.  
### La tua integrazione come sta avvenendo? Ti senti straniera? Vieni stereotipata come donna dell'est?

  
**La mia vita in Italia è molto più lituana di quanto lo era la mia vita in Lituania**. A casa guardo solo la televisione lituana e non solo quello! Ascolto la radio lituana e leggo le riviste e il libri che porto o mi faccio spedire dal mio paese. - Con mio figlio parlo solo nella mia lingua, i prodotti quatidiani della mia patria li compro nei supermercati ucraini o tedeschi e tra poco finiremo la nuova casa costruita tutta "alla lituana" con il pavimento in legno, termocamino, lenzuola europee e prese multiuso.


 **Le mie origini e la mia cultura per me sono molto importanti**, crescerò mio figlio secondo le tradizioni di entrambi i paesi. Tutte le volte quando mi capitava di essere in Italia, mi sentivo stereotipata come la donna dell’est, ma non so se poi di fatto era così o era solo la mia convinzione. A Motta san Giovanni, ovvero dove abito ora, c’e tanta invasione di stranieri ma solo degli immigrati rumeni, quindi la gente riesce a capire che io non sono "una di loro". Per fortuna mio marito e tutta la sua famiglia a Motta San Giovanni è ben conosciuta e tanto rispettata, quindi automaticamente hanno accettato anche la loro nuova concittadina. Noi non siamo ancora sposati civilmente, abbiamo il figlio eppure siamo considerati una famiglia a tutti gli effetti.  
### Com'è il rapporto con le donne italiane?

  
Quando ancora studiavo all'Universita di Messina, ho capito che **con le ragazze italiane non avrei mai avuto rapporti. E' come se venissimo da due pianeti diversi.** Per i miei gusti sono troppo viziate. Quando parli con loro, sembrano di essere principesse intorno al quale gira o almeno dovrebbe girare tutto il mondo intero. Anche il rettore dell'università sapeva se qualcuna aveva il mal di testa e quando venivano le "loro cose" lo sapevano anche i parenti piu lontani; hanno la patente, ogni giorno guidano la macchina fino all'Università, ma se devono spostarsi 250 metri fuori, chiamano qualcuno per accompagnarle; la sera hanno paura di uscire di casa da sole come se qualcuno volesse rapirle. Sembra che abbiano sempre febbre, mal di testa, freddo, ciclo, tumori.


 **Sono esauste, stanche e stressate**. Per andare insieme da qualche parte dovevo aspettare ore e ore che fossero pronte e poi alla fine si rompeva qualche unghia e non si usciva più, quando iniziava a piovere, la stessa cosa, perche i capelli si sarebbero rovinati. Gli argomenti di discussione? Viktoria Beckham, Costantino o se hai la fortuna di stare con una ragazza più acculturata, ti raccontera l'ultima puntata del Grande Fratello. La donna italiana si lamenta davanti a te per l’enorme stanchezza per il volo che ha fatto, quando tu invece la settimana prima hai fatto il trasloco dalla Lituania comprensivo di 19 ore di pullman, 2 voli e svariati km di macchina, tutto questo al ottavo mese di gravidanza. Siamo veramente 2 pianeti diversi.


 **In queste situazioni mi odio, perche divento una snob tremenda**. No, una donna italiana non potrebbe mai diventare mia amica. Poco fa ho letto un articolo su una famosa ciclista lituana che da parecchi anni vive in Italia, ad un certo punto inizia a parlare della sua migliore amica in Italia. Mi viene un leggero colpo alla testa, "ma com'é possibile!?" e poi diventa tutto chiaro, l'amica ha il nome lituano.  
### Com'è il rapporto con gli uomini italiani in italia?

  
Ma quali uomini italiani? Sono chiusa a chiave! :)  
### Com'è il rapporto con gli altri lituani in Italia?

  
Veramente ottimo. ci siamo uniti in un gruppo che si chiama *"i lituani del Sud d'Italia"* e stiamo facendo delle diverse attività. il gruppo è ancora nuovo ma abbiamo gia organizzato 2 grandi incontri, stiamo creando un'associazione, programmando altri incontri, cerchiamo di unire i lituani, le famiglie italo-lituane del Sud (e non solo) per vivere qui in Italia almeno un po' della nostra amata terra.


 Ho degli ottimi rapporti con altre donne lituane che abitano in Calabria e Sicilia; ci sentiamo spesso, discuttiamo dei diversi problemi che affrontiamo ogni giorno. Sinceramente queste amicizie per me sono essenziali; avere una piccola parte della mia cosi amata Lituania qui a 3000 km è meraviglioso.  
### Torneresti a vivere in Lituania con tuo marito?

  
**Definitivamente si.** Giacomo si è innamorato della Lituania, e poi, lui non ha quello stretto legame con la propria patria (o la mamma) come tanti altri italiani. Non escludiamo la possibilità di trasferirci li un giorno, abbiamo tanti progetti, vedremo come andranno avanti. Ma almeno per i prossimi 5 anni, la nostra casa sarà in Italia, qui stiamo bene e se un giorno decideremo di trasferirci in Lituania, sarà solo per stare ancora meglio.  
### Che consigli dai ad una ragazza lituana che ha conosciuto un italiano? Ricordi la canzone che diceva *"gli italiani fanno gli spaghetti a tutte"*, che consigli dai per non farsi raggirare dal solito "provetto Casanova"?

  
Gli spaghetti li fanno anche i lituani! Grazie a Dio e a questi 12 anni del mio rapporto con la solare isola, ho capito bene gli italiani. **Prima di tutto, non fare mai e poi mai caso a quello che lui ti sta dicendo** a meno che le sue parole non coincidano con le proprie azioni; non mettere tanta speranza alle amicizie che sono cominciate quando lui era venuto in Lituania; se lui è veramente innamorato si capisce subito senza alcun dubbio. Se hai dei dubbi, di te a lui non importa niente.


 **Un italiano innamorato diventa un caso patologico classico quindi è molto facile capirlo**. Da un uomo forte e sicuro che hai conosciuto prima, non rimane niente se non un piccolo cagnolino che ti chiama 34 volte al giorno cercando di venire ogni settimana in Lituania. Ti presenta la sua famiglia, promette di amarti anche nelle vite successive, diventa paranoico e stressato per tutti i messaggi ai quali tu non rispondi e di solito, per la classica isterica gelosia, finisce in un manicomio. Quindi, il mio consiglio è **capire se lo ami veramente TU**, ma questo è solo il classico stereotipo italiano che, a mio parere, non esiste più e se esiste, allora ci sono tante bellissime eccezioni, in di queste la sto vivendo io.


