Title: Visita a Jurmala
Date: 2004-08-22
Category: Lettonia
Slug: visita-a-jurmala
Author: Karim N Gorjux
Summary: Sono andato fino a Jurmala in treno per vedere la zona balneare, mi soffermo a parlare dei treni anche se penso che la foto sia piu' che esplicativa. Sedili duri e scomodi,[...]

Sono andato fino a Jurmala in treno per vedere la zona balneare, mi soffermo a parlare dei treni anche se penso che la foto sia piu' che esplicativa. Sedili duri e scomodi, senza poggiatesta. Hanno un controllore per ogni vagone e il biglietto e' molto simile ad uno scontrino fiscale. Prezzo: 40 santimi per fare 15km. (0.60 euro circa)

 [![Treno in Lettonia](http://www.kmen.org/images/trenolatvia.jpg)](http://www.kmen.org/images/trenolatvia.jpg)



