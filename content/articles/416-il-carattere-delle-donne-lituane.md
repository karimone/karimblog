Title: Il carattere delle donne lituane
Date: 2008-02-08
Category: Lituania
Slug: il-carattere-delle-donne-lituane
Author: Karim N Gorjux
Summary: Torno a parlare di donne lituane uno degli argomenti che più caratterizzano il mio blog. Ci tengo a rammentare che io di donne lituane in casa ne ho due: ho la moglie[...]

Torno a parlare di [donne lituane](http://www.karimblog.net/2006/10/02/le-donne-lituane-qualcosa-di-buono-ce) uno degli argomenti che più caratterizzano il mio blog. Ci tengo a rammentare che io di donne lituane in casa ne ho due: ho la moglie e la figlia.  
Le donne lituane sono cocciute come dei muli e tendono a **non esternare** le cose che non le vanno. Avevo già notato questo particolarissimo lato del carattere anche nelle ragazze dei miei amici e sono parecchio rodato dal comportamento di mia moglie, ma sinceramente non ho ancora trovato una soluzione efficace.


 Può capitare che succeda qualcosa in una coppia o ci sia un particolare episodio che crei della tensione, ad esempio un piccolo e banalissimo litigio o qualcosa di più velato come una critica da parte di noi uomini. Le conseguenze con una donna lituana sono molto diverse da un bel vaffanculo da parte di una donna nostrana e il conseguente "mal di testa" sospetto ai nostri prossimi afflussi di ormoni. La donna lituana macina tutto in quel cervello strano che si porta sopra il collo, **non dice nulla**, ma ogni cosa da quel momento sarà un pretesto per litigare e rimproverarti. La donna lituana si **vendica**.


 Se la sera prima è successo qualcosa, il giorno dopo la donna lituana si tramuta in [cattivik](http://erestorfolletto.altervista.org/_altervista_ht/Cattivik.jpg) in gonnella e si attacca a qualsiasi cosa per rimproverarti. Noi uomini, soprattutto italiani, siamo terribilmente logici e dibattiamo i vari rimproveri **apparentemente sensati** perché è nella nostra natura di uomini, purtroppo però la nostra mente ha già archiviato nei meandri della sua memoria l'evento scatenante che in questo preciso momento alimenta il fiume d'orgoglio che scorre nelle vene delle donne lituane.


 Cosa si può fare? **Essere acuti**. Non chiedete "che cos'hai?", avete mai visto un uomo lituano che chiede che cos'hai alla sua donna? Mai. Non esiste la domanda e quindi non esiste nemmeno la risposta. L'unica è capirlo da se e rimediare, anche se nel 90% dei casi abbiamo ragione. Avrei qualche caso da citare come esempio, ma essendo aneddoti della vita privata dei miei amici preferirei lo facessero direttamente loro commentando il post anche anonimamente.


 Mia figlia che sarà una futura donna semi lituana è già testona adesso: vuole fare tutto da sola e piange se non la lasciamo fare. E' semplicemente carattere o è quel 50% di Lituania che le scorre nelle vene?

