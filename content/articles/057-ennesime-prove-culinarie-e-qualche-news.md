Title: Ennesime prove culinarie e qualche news
Date: 2005-04-22
Category: Lituania
Slug: ennesime-prove-culinarie-e-qualche-news
Author: Karim N Gorjux
Summary: Il tempo e' stranissimo. Ieri alle 10 del mattino nevicava, alle 14 c'era il sole, il vento pungente e' l'onnipresente contorno in questo cocktail di eventi atmosferici. Davide dice che qui a[...]

Il tempo e' stranissimo. Ieri alle 10 del mattino nevicava, alle 14 c'era il sole, il vento pungente e' l'onnipresente contorno in questo cocktail di eventi atmosferici. Davide dice che qui a Klaipeda hai l'onore di provare le 4 stagioni in una giornata, forse e' esagerato, ma non si allontana molto dalla realta'.


 Per ora non ho ancora preso un taxi, ho sempre girato usando minibus o bus, quest'ultimi sono fantastici, ogni bus ha una provenienza diversa, le varie scritte di sicurezza sono in lingue diverse, ne ho trovata qualcuna anche in Italiano. In pratica qui in Lithuania (e non solo) recuperano i bus mezzi morti dagli stati piu' sviluppati e li rimettono a nuovo. Niente di sorprendente.


 Ieri ho provato di nuovo a fare la carbonara e questa volte e' venuta stupendamente bene, fluida al punto, salta al punto giusto... secondo me non mi ricapita mai piu' :-D

 Per il resto tutto bene, la salute c'e', il computer no.. :-| ma non e' un problema. Ho winzozz! :-(

