Title: Dieci giorni
Date: 2006-12-03
Category: Lituania
Slug: dieci-giorni
Author: Karim N Gorjux
Summary: Tra 10 giorni il blog diventerà una versione di Casa Vianello italo-lituana, le preoccupazioni ci sono perché sto facendo in modo che lei si trovi a suo agio sia con le persone[...]

Tra 10 giorni il blog diventerà una versione di Casa Vianello italo-lituana, le preoccupazioni ci sono perché sto facendo in modo che lei si trovi a suo agio sia con le persone che la circonderanno sia in casa. Si fa il possibile e si cerca di fare anche un po' d'impossibile, spero che la signora apprezzi.


 La grande preoccupazione che ogni tanto folgora la mia mente è che la signora si trovi in una situazione simile alla mia in Lituania, il vantaggio di Rita è di conoscere abbastanza bene l'Italiano, conoscere i miei amici e i miei parenti, avere una macchina e la patente per andare in giro e soprattutto avere molta più disponibilità di quanta ne ho ricevuta io nei pressi del mar Baltico. Ho già pensato di spedirla in perlustrazione in Lituania prima che ne senta la nostalgia, ringrazio Dio di vivere nell'era di internet e dei voli low cost.


 Venerdì sera ho finalmente giocato a calcetto dopo più di 1 anno. **Ho segnato**, mi sono sentito morto, ma soprattutto mi sono divertito. Ovviamente c'è sempre quello che non la passa mai! Disgraziato! :-)

