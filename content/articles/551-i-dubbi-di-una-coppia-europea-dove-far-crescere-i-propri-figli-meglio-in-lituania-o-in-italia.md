Title: I dubbi di una coppia europea: dove far crescere i propri figli? Meglio in Lituania o in Italia?
Date: 2009-07-06
Category: Lituania
Slug: i-dubbi-di-una-coppia-europea-dove-far-crescere-i-propri-figli-meglio-in-lituania-o-in-italia
Author: Karim N Gorjux
Summary: Oggi a pranzo Rita mi ha messo la pulce nell'orecchio: dove andremo a vivere quando avrò finito l'università? Per capire la domanda bisogna fare un piccolo passo indietro: il motivo principale che[...]

[![Bambini lituani](http://www.karimblog.net/wp-content/uploads/2009/07/Vaikai.jpg "Bambini lituani")](http://www.karimblog.net/wp-content/uploads/2009/07/Vaikai.jpg)Oggi a pranzo Rita mi ha messo la pulce nell'orecchio: dove andremo a vivere quando avrò finito l'università? Per capire la domanda bisogna fare un piccolo passo indietro: il motivo principale che ci ha spinti a tornare in Lituania è stata la carriera universitaria di Rita.   
  
Quando nel 2006 siamo partiti dalla Lituania per tornare in Italia, l'intenzione era di permettere a Rita di continuare a Cuneo la sua carriera universitaria iniziata a Klaipeda. Purtroppo non è andata come speravamo; problemi con l'università, problemi miei di lavoro e ci siamo decisi a tornare nella piccola Repubblica Baltica.


 Tra poco è un anno che sono nuovamente qui, tutto è molto diverso dalla prima volta. La Lituania non è molto differente, ma la nostra situazione è completamente diversa e quindi la domanda sorge spontanea. Quale sarà il nostro punto di riferimento in futuro? Su che basi possiamo scegliere dove continuare a vivere?

 Il primo pensiero che ti verrà in mente presumo sia l'inverno. Si è vero, la piaga del freddo infinito che avvolge la Lituania da Settembre a Marzo è un deterrente difficile da superare. Il sole, la pioggia, la neve e il freddo possono essere piacevoli, ma alla lunga portano a delle conseguenze spiacevoli sull'umore e sul quotidiano. Detto questo è solo una questione di **priorità**; il nostro primo pensiero e la nostra bimba Greta, qual è il miglior posto per far crescere e studiare Greta? E' meglio che rimanga qui a Klaipeda o frequenti le scuole in un paesino della provincia di Cuneo?

 Non ho una risposta decisa e sicura, ma è chiaro che la nostra scelta è incentrata sulla nostra bambina a costo di sacrifici personali. La domanda è semplice: un bambino ha migliori opportunità di istruzione e crescita a Klaipeda o a Cuneo?

 La mia esperienza personale mi dice che l'istruzione fino alle superiori è di qualità superiore rispetto all'Italia, mentre l'università non è paragonabile alle università del nord d'Italia. Non è una domanda dalla risposta facile, ma per fortuna abbiamo ancora tempo. Io da ex studente italiano posso solo dire che l'istruzione in Italia, e non solo quella, è una **botta di culo**; un mix tra insegnanti, mezzi dell'istituto e genitori a casa.


 Cosa ne pensi?

 Link: [Educazione e nazismo](http://www.komisija.lt/en/body.php?&m=1150465846 "The International Commission for the Evaluation of the Crimes of the Nazi and Soviet Occupation Regimes in Lithuania") (EN)   
Link: [Studiare in Lituania](http://europa.eu/youth/studying/index_lt_it.html "Link utili per studiare in Lituania")

