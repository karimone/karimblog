Title: Le esperienze non sono critiche e le opinioni non sono verità assolute
Date: 2011-01-19
Category: Lituania
Slug: le-esperienze-non-sono-critiche-e-le-opinioni-non-sono-verità-assolute
Author: Karim N Gorjux
Summary: Il mio ultimo articolo ha destato vari commenti, c'è chi ha scritto che la colpa è mia che non sono simpatico, un paio di lituani mi hanno scritto comparando le loro problematiche[...]

[![](http://www.karimblog.net/wp-content/uploads/2011/01/cultdiff.jpeg "Differenze culturali")](http://www.karimblog.net/wp-content/uploads/2011/01/cultdiff.jpeg)Il mio [ultimo articolo](http://www.karimblog.net/2011/01/15/e-alla-fine-sei-sempre-uno-straniero-mio-caro-uzsienieti/) ha destato vari commenti, c'è chi ha scritto che la colpa è mia che non sono simpatico, un paio di lituani mi hanno scritto comparando le loro problematiche nel vivere in Italia da stranieri. Ho ricevuto anche una lamentela perché dipingo male il popolo lituano, come se fossero dei mostri.


 Vorrei chiarire alcuni punti con te che con tanta pazienza leggi il mio blog. Sono sette anni che scrivo e dal mio primo articolo mi sono successe molte cose e come tutti nella vita sono cambiato nel modo di scrivere e nel modo di pensare. Mentre prima tendevo ad etichettare in base alle mie esperienze, ora mi limito a scrivere la mia esperienza, ma senza esprimere giudizi che siano insindacabili. **A volte scrivo articoli senza nemmeno avere un'opinione al riguardo perché semplicemente non ce l'ho.**  
  
Nel mio ultimo articolo mi sono guardato bene da esprimere un giudizio tanto da concludere con questa frase:  

> A te l’infausto compito di giudicare questo popolo.


 Io non ne voglio sapere niente sull'etichettare questo popolo con termini come "freddo", "antipatico", "scorbutico", "violento" o ancora altri termini inflazionati che sento quando altri italiani in visita parlano della Lituania. Io mi limito semplicemente a scrivere le mie esperienze e se esprimo un giudizio lo faccio con le mie dovute riserve e con il ragionevole dubbio di porre un giudizio sbagliato.


 **Non esprimendo un giudizio, divento inevitabilmente responsabile delle conclusioni tratte dal lettore**. C'è il difensore avvocato della Lituania che subito commenta ridimensionando la mia esperienza paragonando all'Italia o a esperienze indirette. C'è il lituano che se la prende come se fosse un attacco personale alla sua identità e mi richiama in pubblico o insulta in privato. Un mio carissimo amico tempo fa mi ha scritto:

 
>   
> [...] comunque sia, osservando in blog e forum questi comportamenti ed altri simili come flames, attacchi personali ed insulti e chi più ne ha più ne metta. Mi ha portato a considerare il fatto che tutto sommato Internet, l'accesso illimitato alle informazioni, conduce alcuni tipi di persone a sopravvalutare il fatto che possiedono un'opinione su qualcosa. Spesso su qualunque cosa.


 Nella realtà queste opinioni che manifestano nei loro commenti e interventi non sono vere opinioni, non sono frutto di riflessione, di scambio. Sono semplicemente pensieri istantanei che in qualche modo si materializzano con il loro intervento. E' chiaro che poi vengono fuori le stronzate.  


