Title: Klaipeda... ri-ambientarsi
Date: 2006-02-02
Category: Lituania
Slug: klaipeda-ri-ambientarsi
Author: Karim N Gorjux
Summary: Per ora è parecchio difficile riambientarsi, sono partito da casa che nevicava e pioveva, arrivo a Klaipeda che c'è il sole e fa "caldo", non è brutta come accoglienza, però c'è qualcosa[...]

Per ora è parecchio difficile riambientarsi, sono partito da casa che nevicava e pioveva, arrivo a Klaipeda che c'è il sole e fa "caldo", non è brutta come accoglienza, però c'è qualcosa che non va. Ho nostalgia della mia palestra..  
  
Ringrazio Matteo e Claudia per il loro continuo supporto p2p a distanza, grazie a loro ho parecchio materiale didattico su cui lavorare e studiare, ma mi ci vorrà un po' per ambientarmi alla vita lituana nuovamente. Penso che domani me ne andrò in palestra qui a Klaipeda e inizierò a vivere in Zona come si deve.


