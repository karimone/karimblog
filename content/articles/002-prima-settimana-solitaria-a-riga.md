Title: Prima settimana solitaria a Riga
Date: 2004-08-03
Category: Lettonia
Slug: prima-settimana-solitaria-a-riga
Author: Karim N Gorjux
Summary: Primo sabato sera senza compagnia a Riga, sono uscito e ho incontrato due ragazzi italiani di Padova (Daniele e Debora) che vivono in una ex caserma russa non troppo distante dove abito[...]

Primo sabato sera senza compagnia a Riga, sono uscito e ho incontrato due ragazzi italiani di Padova (Daniele e Debora) che vivono in una ex caserma russa non troppo distante dove abito io. Sono turisti di passaggio, la loro intenzione è visitare le repubbliche baltiche in lungo e in largo.


 Ho assistito ad una rissa, sono andato al Roxy... che come al solito è pieno di ragazze carine ed una vagonata di italiani. Due giorni fa' ho conosciuto il mio vicino di casa: un nonno di 75 anni che mi ha parlato in russo per 15 minuti. Ho capito una parola ogni 184, ma gli rispondevo in russo e mi ha preso in simpatia. Il nome??? Non me lo ricordo..


 Mi spostero' per la lettonia tra poco se tutto andra' bene, penso che andro' fino in lituania, ma niente e' sicuro. Il locale da dove chatto, scrivo email e dove naturalmente aggiorno questo piccolo sito. Un grazie alla apple per la facilita' di uso del tutto. Per i tecnici: il collegamento e' via wifi ad una velocita' di 60 kb/s, pago 1 lat all'ora, ma non capisco il perche' ho comprato una card che mi sta' durando da circa 2 giorni. Forse un bug?? 

 Ieri NON sono andato con la barca sul fiume e penso che ci andrò oggi. Il tempo è fresco, si stà bene, un pò come il nostro settembre. Sabato 14 Agosto mi sposterò in old riga in un'appartamento di vecchia conoscenza mia e del mio compagno di avventure Davide "Viscius" Chiaramello (la porta di fianco all'appartamento di quest'inverno).


 Prendo tutti i giorni il bus che costa 20 sentimi (ovvero 30 centesimi di euro) e percorre quasi tutta Riga. Sul bus si incontra la gente più disparata: ubriachi, barboni etc... e a volte si possono vedere delle belle cose ;-)  
Ho notato è che non ti puoi fidare troppo delle persone, prendono gli appuntamenti molto sottogamba ed intendo appuntamenti d'affari, non galanti! L'alcolismo è la piaga di questo paese (con l'alta percentuale dei divorzi penso...) a differenza dell'estonia che è uno dei paesi più informatizzati del mondo, la Lettonia è l'ultimo gradino della scala europea. La patente è anche a punti (10) e se guidi in stato di ubriachezza te ne vengono tolti 8. E' vietato circolare per le strade con una bottiglia di conenuto alcolico APERTA! (non scherzo!) ed è anche vietato fare foto nei centri commerciali. Nessuno è riuscito a spiegarmi il perchè.


