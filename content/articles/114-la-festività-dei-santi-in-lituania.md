Title: La festività dei Santi in Lituania
Date: 2005-11-01
Category: Lituania
Slug: la-festività-dei-santi-in-lituania
Author: Karim N Gorjux
Summary: Sono rimasto letteralmente stupefatto da come i Lituani vivono il primo novembre, sono riuscito a fare qualche foto, ma data l'ora tarda vi lascerò un piccolo assaggio rimandando a domani la visita[...]

Sono rimasto letteralmente stupefatto da come i Lituani vivono il primo novembre, sono riuscito a fare qualche foto, ma data l'ora tarda vi lascerò un piccolo assaggio rimandando a domani la visita sulla mia galleria. Per ingannare l'attesa, vi racconto cosa ho vissuto oggi, rimandando a futuri post le mie visite ai laghi di Telsai.  
  
In preparazione al primo Novembre ho notato un notevole fermento nei supermarket di cianfrusaglie da "mano sul cavallo" come candele rosse o nere da loculo, la cosa mi ha stupito non poco dato che le cianfrusaglie in questione erano presenti a quintali, nello stile barattoli di pomodoro uno-sopra-l'altro tipico dei grandi discount Italiani.


 Premettendo che Telsai è una città su piccole colline anche il cimitero, come le chiese, tende a essere posizionato su di una collina in modo da *"avvicinarsi"* di più al cielo. La mia prima visita è stata all'incirca alle 11 del mattino. Io, Rita e i suoi genitori ci siamo recati armati di fiori e candele al "camposanto" (come lo chiamano dalle nostre parti). Dato che io me ne partivo con il mio stereotipo da cimitero fuori città con loculi a grattacielo e fila di macchine parcheggiate, sono rimasto stupito quando mi sono reso conto che il cimitero si trova in centro città. Duecento metri a piedi ed eccoci arrivati di fronte ad un vecchio cancello malandato che introduce una stradina asfaltata che si arrampica sulla collina ricoperta di croci e lapidi. Il tutto è ornato da centinaia di fiori di vari colori e da candele bianche, rosse e verdi. 

 La giornata era come al solito fredda, ma non essendoci una nuvola, il sole splendeva in tutta la sua bellezza. C'era un mucchio di gente. Passeggiando tra una lapide e l'altra sono rimasto incuriosito da una grande differenza rispetto ai nostri cimiteri: **le foto sono presenti in rarissimi casi**. La gente preferisce le lapidi con ornamenti e scritte per ricordare i cari e in fin dei conti se ci pensiamo la foto rappresenta un momento della vita che ne dovrebbe riassumere l'intero corso, senza una foto il ricordo diventa qualcosa di molto intimo che altre persone non possono nemmeno percepire.


 I disegni che ho visto scavati nel marmo erano delle vere opere d'arte. Non ho potuto fare foto durante il giorno perché mi sembrava una mancanza di rispetto ai presenti (vivi) quindi mi sono trattenuto, ma la sera non sono riuscito a fermarmi, trascinato dal fatto che non ero il solo, mi sono scatenato nel provare ad immortalare al meglio il cimitero di notte. Si, avete capito bene, le visite continuano anche a notte fonda e le luci di tutte quelle candele rendono lo spettacolo unico.


 Per ora accontentatevi di questo scatto:  
  
[![Gravenight](http://www.kmen.org/wp-content/upload//CIMG1638-tm.jpg "Gravenight")](http://www.kmen.org/wp-content/upload//CIMG1638.jpg)  


 Durante il viaggio di ritorno, ci siamo fermati a *Kartenà* per lasciare due candele sulla tomba del bisnonno di Rita. Erano quasi le 9 di sera e dato che Kartenà è una cittadina di poche anime e poche colline, il cimitero si trovava fuori dal centro (sempre che Kartenà abbia un centro). A differenza di Telsai le tombe e lapidi si estendevano in piano e davano un sorprendente effetto di vastità a tutto il camposanto. L'illuminazione emanata dalle candele era talmente tanta che si poteva distinguere qualsiasi angolo del campo. Le luci fioche e i giochi d'ombra non incutevano il minimo timore, la bellezza era talmente tanta che soffocava qualsiasi paura. Eravamo gli unici vivi a sentire il freddo pungente che sembrava quasi entrare nelle ossa.


 Presto vi farò avere le altre foto, tra cui quella di una lapide con relativo disegno inciso sul marmo. **In questo caso solo per i registrati alla galleria..**

