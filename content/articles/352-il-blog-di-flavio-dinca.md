Title: Il blog di Flavio D'inca
Date: 2007-08-01
Category: altro
Slug: il-blog-di-flavio-dinca
Author: Karim N Gorjux
Summary: Di Flavio vi ho già parlato in un mio post precedente. Ora Flavio si è finalmente deciso e ha aperto il suo Blog (rss) dove potete avere notizie dirette dal Cile. Imperdibile!

Di Flavio vi ho già parlato in un mio [post](http://www.karimblog.net/2007/06/27/flavio-dinca) precedente. Ora Flavio si è finalmente deciso e ha aperto il suo [Blog](http://fladinca.blogspot.com/) ([rss](http://fladinca.blogspot.com/feeds/posts/default)) dove potete avere notizie dirette dal Cile. Imperdibile!

 Sei un grande Flavio!

