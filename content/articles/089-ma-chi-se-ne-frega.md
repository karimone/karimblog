Title: Ma chi se ne frega!
Date: 2005-08-14
Category: altro
Slug: ma-chi-se-ne-frega
Author: Karim N Gorjux
Summary: Il mondo cambia continuamente, si formano nuovi pensieri, nuove mode, nuove ideologie. Naturalmente succede anche alle persone.Non avrei mai pensato un anno fa di ritrovarmi ora in questa situazione. Mi dispiace di[...]

Il mondo cambia continuamente, si formano nuovi pensieri, nuove mode, nuove ideologie. Naturalmente succede anche alle persone.  
  
Non avrei mai pensato un anno fa di ritrovarmi ora in questa situazione. Mi dispiace di essere stato snobbato, ma è un susseguirsi di vicende che delineano il cambiamento. Inutile essere offesi o toccati, c'è solo incompatibilità, non è colpa mia né colpa loro. Ho altre cose a cui pensare ora, ho da fare qualcosa di importante e non ho tempo da perdere. Devo fare la differenza.


