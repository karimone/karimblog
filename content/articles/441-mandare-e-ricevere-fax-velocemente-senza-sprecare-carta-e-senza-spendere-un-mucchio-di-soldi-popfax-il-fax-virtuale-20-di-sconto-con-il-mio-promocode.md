Title: Mandare e ricevere fax velocemente, senza sprecare carta e senza spendere un mucchio di soldi. Popfax il fax virtuale (20% di sconto con il mio promocode)
Date: 2008-04-14
Category: altro
Slug: mandare-e-ricevere-fax-velocemente-senza-sprecare-carta-e-senza-spendere-un-mucchio-di-soldi-popfax-il-fax-virtuale-20-di-sconto-con-il-mio-promocode
Author: Karim N Gorjux
Summary: Sono quasi due anni che uso popfax e ne sono molto soddisfatto. Avere al giorno d'oggi un vero fax non è più necessario, è molto meglio il fax virtuale.

Sono quasi due anni che uso [popfax](http://www.popfax.com) e ne sono molto **soddisfatto**. Avere al giorno d'oggi un vero fax non è più necessario, è molto meglio il fax virtuale.


 Ho un fax con numero di telefono con prefisso locale e per ogni fax che ricevo mi viene inviata una mail con **allegato**. Per mandare i fax invece è possibile farlo direttamente sul loro sito inviando pdf, doc, jpeg al costo di **5 centesimi** a pagina. Popfax si impegna a fare le chiamate al destinatario e a restituire la **ricevuta** della consegna direttamente via email.


 Stampo solo una piccola percentuale dei fax che ricevo, non ho una linea dedicata per il fax e se voglio mandare un fax, lo scrivo direttamente con [Pages](http://www.apple.com/it/iwork/pages/) e me lo salvo in pdf. Per chi usa Windows esiste il [driver di stampa](http://sourceforge.net/projects/popfax-printer/) che manda il fax direttamente, senza dover fare login al sito.


 Se vi interessa, approfittate del promocode [2452 5365 0618 7318] per avere lo sconto del 20%

 [Aggiornamento]

 Ho avuto più di una volta a che fare con l'assistenza tecnica via email. Mi hanno sempre risposto in italiano ed in meno di 12 ore lavorative. Per ora di note negative non ne ho viste.


