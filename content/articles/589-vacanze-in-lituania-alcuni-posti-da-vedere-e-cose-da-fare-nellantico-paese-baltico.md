Title: Vacanze in Lituania: alcuni posti da vedere e cose da fare nell'antico paese baltico
Date: 2010-02-11
Category: Lituania
Slug: vacanze-in-lituania-alcuni-posti-da-vedere-e-cose-da-fare-nellantico-paese-baltico
Author: Karim N Gorjux
Summary: Molti mi chiedono della Lituania nell'ambito del turismo, se ci siano posti da vedere, cose da fare, se fa freddo etc. etc.

[![](http://farm4.static.flickr.com/3196/2947182660_9e992525b1_m.jpg "Trakai")](http://www.flickr.com/photos/losk/2947182660/)Molti mi chiedono della Lituania nell'ambito del turismo, se ci siano posti da vedere, cose da fare, se fa freddo etc. etc.


 La mia opinione da italiano residente in Lituania è molto rigida. La Lituania è una terra stupenda nel periodo che va da Maggio a Settembre, discreta nei mesi di Aprile ed Ottobre e molto difficile da godere nel periodo autunnale ed invernale.


 Klaipeda, dove vivo, è un bellissimo posto con il mare, con un bel centro storico e tanta cose da fare nel periodo di Giugno e Luglio. Ad Agosto, ma dipende dall'annata, inizia già a fare freddo, quindi sconsiglio caldamente di sfruttare Agosto per venire in Lituania, soprattutto le ultime settimane.


  Un ente italiano adibito alla promozione in Italia della Lituania è [l'Ente del Turismo lituano](http://www.turismolituano.com/ "l'Ente del Turismo lituano") che è una fonte piena di informazioni per chi ha deciso di visitare la Lituania.


 In particolare ho trovato un [video](http://www.c6.tv/archivio?task=view&id=3089&special=22 "video"), girato nel periodo in cui Vilnius era la capitale europea della cultura, e dove vengono elencati i 15 motivi per visitare la Lituania. Purtroppo il pessimo audio non permette di capire, molto quindi elenco io i miei personali pro nel visitare l'antica terra baltica (in estate).  
  
 * [Vilnius](http://it.wikipedia.org/wiki/Vilnius "Vilnius"): la capita della Lituania può vantare un centro storico affascinante che mostra il passato della Lituania e soprattuto mostra come popoli di perse culture e religioni abbiano abitato insieme per centinaia di anni.

  
 * Castello di [Trakai](http://it.wikipedia.org/wiki/Trakai "Trakai"): suggestivo ed affascinante, si trova a pochi km da Vilnius. Vale la pena farci visita perché è talmente bello che sembra finto.

  
 * **La collina delle croci di Šiauliai**: posto suggestivo immerso nello spirito religioso, Šiauliai ospita una collina dove negli anni sono state [piantate migliaia di croci](http://www.youtube.com/watch?v=0ugPy_UrOTg "piantate migliaia di croci").

  
 * [Nida](http://en.wikipedia.org/wiki/Nida,_Lithuania "Nida"), il deserto e la [penisola curoniana](http://en.wikipedia.org/wiki/Curonian_Spit "penisola curoniana"): protetta dall'Unesco come patrimonio dell'umanità, la penisola curoniana è un posto affascinante che ospita un deserto naturale che non ha eguali in tutta Europa. Assolutamente da vedere.

  
 * **Palanga e Klaipeda**: Palanga è la Rimini lituana, d'estate è la città più attiva della lituania con feste, concerti, spettacoli e ovviamente il mare. Klaipeda invece è la città portuale dove vivo che organizza il Jazz festival e la festa del mare.

  
 * **Le spiagge**: tutta la costa lituana è pubblica, non esistono spiagge private. L'acqua non è caldissima, ma si fa il bagno senza problemi. Non è la Sardegna, ma sempre mare è.

  
 * **Le foreste da percorrere in bicicletta**: la Lituania è immersa nella natura, non ci sono montagne, ma in compenso è piena di foreste praticabili in bicicletta per svariati km. Attenzione che d'estate le zanzare sono davvero fastidiose!
  
 * **Il cibo particolare e gustoso**: la Lituania anche se piccola ha parecchi piatti tipici che sono degni di nota. I nazionali [cepelinai](http://en.wikipedia.org/wiki/Cepelinai "cepelinai"), la Saltibarsai e tanti altri [piatti particolari](http://ausis.gf.vu.lt/eka/food/potatoes.html "piatti particolari")
  
 * **L'estate fresca e luminosa**: giornate da 25-30 gradi accompagnate da un sole che tramonte solo dopo le 23. Una brezza fresca quotidiana rende l'estate molto più piacevole dell'afa italiana a cui siamo abituati.

  
 * [Vente](http://en.wikipedia.org/wiki/Ventė_Cape "Vente"): una delle prime stazione d'Europa dove si studiano gli uccelli e le loro migrazioni.

  
 * **I lituani**: che se ne parli male o che se ne parli bene, i lituani sono da conoscere. Da uomo posso dire che la bellezza delle donne lituane vale la pena di essere ammirata esattamente come si entra in una galleria d'arte a vedere dei quadri. Indimenticabile.

  
  
Sicuramente ho dimenticato luoghi, posti, cibi e tante altre cose che sono degne di essere viste, ma so benissimo che ci siete voi per questo e che direte la vostra direttamente nei commenti.


 Qualcuno di Kaunas che può dare qualche suggerimenti? Qualcuno di altri paesi più piccoli che conosce dei posti?

