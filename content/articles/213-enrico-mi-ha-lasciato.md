Title: Enrico mi ha lasciato
Date: 2006-09-18
Category: Lituania
Slug: enrico-mi-ha-lasciato
Author: Karim N Gorjux
Summary: Questo è un periodo a dir poco strano. Enrico è partito, è andato via. Mi mancherà tantissimo, le sue stranezze, la sua amicizia, la sua timidezza. Era l'unico amico che avevo qui[...]

Questo è un periodo a dir poco strano. Enrico è partito, è andato via. Mi mancherà tantissimo, le sue stranezze, la sua amicizia, la sua timidezza. Era l'unico amico che avevo qui a Klaipeda. Addio Enrico. Ti voglio bene.  
  
  
![Karim e Enry](http://static.flickr.com/82/226404128_8b9d432430_m.jpg)

