Title: Articolo sul giornale del mio blog?
Date: 2007-05-26
Category: altro
Slug: articolo-sul-giornale-del-mio-blog
Author: Karim N Gorjux
Summary: Al Blockbuster di Cuneo c'è una ragazza che conosco da tempo immemorabile. Proprio ieri mi trovo da lei per cambiare il dvd dei Pirati dei caraibi 2 perché dava dei problemi nel[...]

Al Blockbuster di Cuneo c'è una ragazza che conosco da tempo immemorabile. Proprio ieri mi trovo da lei per cambiare il dvd dei Pirati dei caraibi 2 perché dava dei problemi nel vederlo (non si rippa!) e mentre parlavamo mi ha chiesto del mio blog. 

 "Sai sono capitata sul tuo blog, ho visto l'articolo su un giornale..." :huh\_tb:

 La mia amica ha letto un articolo, riguardo al mio blog, su uno dei nostri giornali locali, ma purtroppo non ricorda quale. Da una piccola analisi sembra che sia uno di questi: [La Fedeltà](http://www.lafedelta.it), Piazzagrande, La Guida.


 L'articolo, a quanto sembra, non ha più di 2-3 settimane. Mi potete aiutare a recuperarlo? Mi piacerebbe leggerlo.


