Title: Buona Pasqua!
Date: 2010-04-04
Category: altro
Slug: buona-pasqua-1
Author: Karim N Gorjux
Summary: Ammetto di non aver nessuna voglia di fare il giro di chiamate a parenti e amici per fare gli auguri di buona Pasqua, quindi approfitto del blog e ti incarico di estendere[...]

[![](http://www.karimblog.net/wp-content/uploads/2010/04/buona-pasqua-281x300.jpg "buona-pasqua")](http://www.karimblog.net/wp-content/uploads/2010/04/buona-pasqua.jpg)Ammetto di non aver nessuna voglia di fare il giro di chiamate a parenti e amici per fare gli auguri di buona Pasqua, quindi approfitto del blog e ti incarico di estendere questo mio messaggio a tutte le persone che conosco.


 Ai mei cugini chiedo di fare gli auguri ai miei zii da parte mia, ai miei amici di estendere gli auguri agli amici che non mi leggono. Auguri anche a tutti i miei parenti che se proprio volete sentire la mia voce, non avete altro da fare che telefonarmi.


 In particolare voglio fare gli auguri ai miei lettori, persone che mi conoscono per i miei scritti e che non mi hanno mai incontrato se non qualche raro caso. Auguri anche a chi non commenta mai e legge sempre in silenzio e auguri anche a chi commenta ogni mio articolo nel bene e nel male, criticando od elogiando.


 Insomma, penso che il messaggio sia stato recepito. Auguri!

