Title: Impressioni del Lunedì mattina
Date: 2005-02-07
Category: altro
Slug: impressioni-del-lunedì-mattina
Author: Karim N Gorjux
Summary: Anche se sono iscritto al dipartimento di informatica di Torino e sono diplomato come perito informatico, i miei interessi divagano in campi molto diversi dall'informazione automatica. Ho letto vari libri e passo[...]

Anche se sono iscritto al dipartimento di informatica di Torino e sono diplomato come perito informatico, i miei interessi divagano in campi molto diversi dall**'informa**zione automa**tica.** Ho letto vari libri e passo molto tempo a leggere articoli su internet, questa mattina cercavo approfondimenti su una frase che il leggendario Bruce Lee disse ai suoi allievi:

 *"Non insegnate ciò che io vi ho insegnato, ma ciò che voi avete imparato"*

 