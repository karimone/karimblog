Title: Strutter
Date: 2005-01-05
Category: Lettonia
Slug: strutter
Author: Karim N Gorjux
Summary: I vantaggi di non muoversi in branco per un a città come Riga sono notevoli. Tutti gli italiani cercano di fare colpo dicendo che sono diversi dagli altri italiani, ma in realtà[...]

I vantaggi di non muoversi in branco per un a città come Riga sono notevoli. Tutti gli italiani cercano di fare colpo dicendo che sono diversi dagli altri italiani, ma in realtà sono tutti esattamente uguali. Ieri sera ho di nuovo incontrato IVAN ovvero Onan il barbaro! ([per chi non sapesse cos'è l'onanismo](http://it.wikipedia.org/wiki/Onanismo)). Ivan era in compagnia di uno strano personaggio di nome Raffaele.  
Raffaele ha i tipici tratti somatici dell'italiano del nord, ma con un accento tipico di chi è sbarcato dal Congo l'altro ieri. Devo dire che stonava molto la combinazione. Nei 300 secondi che ho passato con Raffaele, ho scoperto una lingua particolare, un italiano sintetico dove ogni parola deve essere obbligatoriamente formata da queste parole: "scopare", "hotel" e "troie". raffaele (rigorosamente minuscolo) ha dato il meglio di sè accennando ad una sua relazione con una ragazza di Praga che lo ha gentilmente ospitato in casa e che lui ha ripagato uscendo la sera da solo alla ricerca di altre ragazze. Non avendo nessuna medaglia e non conoscendo l'inno nazionale svizzero non ho potuto premiare Raffaele.  
Ivan e Raffaele hanno il merito di essere i massimi esponenti dell'associazione: "Pene a presa anatomica". 

  Non c'è molto da fare a Riga in settimana, l'unico locale aperto è l'onnipresente Roxy. Ieri sera ci sono andato volentieri, ero molto contento. Nel pomeriggio ho passato due ore molto piacevoli con una mia vecchia conoscenza: Jana. La classica e inconfondibile ragazza 100% lettone alta 175 cm, bionda, occhi azzuri. . Jana ed io abbiamo parlato del più e del meno, bevuto qualcosa e passeggiato in old riga. Ci siamo salutati alla fermata del bus. Coltivare amicizie in una città come Riga è molto piacevole, tutte le volte che torno da queste parti non mi sento mai così solo, ho sempre qualche numero da chiamare e qualcuno da incontrare.


 Ieri sera al Roxy ho conosciuto il barista. Un lettone molto simpatico che dato la poca gente presente il Lunedì sera aveva parecchi tempi morti, abbiamo parlato del più e del meno, mi ha fatto vedere le sue acrobazie con i bicchieri. Questo ragazzo (di cui non ricordo il nome: troppo difficile!!), faceva volare i bicchieri così velocemente che sembrava avesse 8 braccia come il Dio Visnù! Ora oltre ad avere un amico in più ho anche qualche dritta sui locali a italiani tasso zero. :-)

 Ovviamente gli Italiani nella discoteca sono gli stessi della sera prima, le cesse ingresso libero (apertura a spinta) sono anche loro sempre le stesse. Ormai ho affinato l'occhio e riesco a distinguere le brave ragazze che sono in discoteca con la sola intenzione di ballare, non sono tante, ma si trovano. Probabilmente a prima vista non sono le bellezze a cui Riga abitua, ma vi assicuro che è molto piacevole parlare con loro. Il classico identikit della brava ragazza me lo ha confermato ieri sera Marina: piccolina, castana, occhi marroni. Marina non fuma, beve succhi di frutta, le piace ballare, ha 19 anni, parla un ottimo inglese. Nell'ambiente in cui vive è persino particolare. Non è facile trovare ragazze con gli occhi marroni e i capelli castani che non siano tinti. Marina è Lettone - Ucraina, spero di vederla presto e fare altre quattro chiacchiere :-)

 Prima di tornare a casa sono passato in un locale molto carino dove lavora una mia amica: Laura. Mi sono fatto preparare il cappuccino della buonanotte e ho chiacchierato dalle 3 del mattino alle 5. Il locale è aperto 24 su 24 e a quelle ore l'affluenza è veramente bassa. Laura è veramente una ragazza stupenda, anche lei 100% lettone, è intelligente e di cuore oltre che molto bella. E' la classica ragazza acqua e sapone che studia e lavora, difficile da incontrare al Roxy... di lei spero vivamente di poter pubblicare una foto.


 Per ora e tutto, arrivederci al prossimo articolo, spero di ricevere qualche commento. :-)

 Un'ultima cosa, se per caso venite da queste parti, non fate come tutti che dicono: *"Io non sono come gli altri Italiani*", io mi limito a non fare come gli altri (non ho ancora conosciuto qualcuno che è andato ad abitare a Sarkandaugava!) saranno poi le persone a dirvi: "*Ma tu non sei come gli altri italiani!*"

 Cosa mi rimane da fare? Pavoneggiarmi (la mitica canzone dei [Kiss](http://www.lyricsfreak.com/k/kiss/79870.html))

