Title: Partenza per Auce
Date: 2004-08-24
Category: Lettonia
Slug: partenza-per-auce
Author: Karim N Gorjux
Summary: Partiro' alle 17 ora italiana per Auce che (si legge auze con la a aperta) chesi trova ai confini con la Lituania. In questi giorni non sono stato benissimo,continuo a sentirmi debole[...]

Partiro' alle 17 ora italiana per Auce che (si legge auze con la a aperta) che  
si trova ai confini con la Lituania. In questi giorni non sono stato benissimo,  
continuo a sentirmi debole e con mal di stomaco, non so' da cosa sia dovuto, ma  
mi sto' riprendendo anche se sensibilmente. (e anche se sono ingrassato).


 In questi giorni ho avuto a che fare con i bambini (maschi) che chiedono  
l'elemosina dandoti fastidio finche' non gli dai qualcosa pur di farli andare  
via. Il loro motto: "pleaaaaseee santimiii". Sono particolarmente fastidiosi e  
incredibilmente maleducati, io non gli do' soldi per principio, ma c'e' chi  
casca nella trappola e sgancia il money (ovvio, altrimenti avrebbero gia' smesso  
da un pezzo di chiedere soldi).


 In old riga non ci sono i cassoni della spazzatura quindi alcuni angoli delle  
strade sono adibite alla raccolta dei sacchi neri. L'odore e' insopportabile,  
eppure c'e' sempre qualcuno che rovista nel mezzo alla ricerca di qualcosa di  
utile o commestibile. Sono stato anche al mercato di Riga che e' il luogo dove  
la gente comune trova vestiti e alimenti a prezzi comuni. Sono rimasto  
impressionato dalla sporcizia, soprattutto vicino ai banchi della frutta dove le  
angurie (in vendita) sembravano il ritrovo per mosche ed api. Il mercato si  
trova vicino alla stazione dei bus, vi sconsiglio vivamente di portarvi  
portafogli o cellulari.


 Altre informazioni che ho avuto della vita lettone/russa: (quasi tutta) old  
riga, come la parex banka e' in mano alla mafia russa quindi mentre la  
burocrazia della lettonia e' in mano ai lettoni i soldi sono in mano ai russi.  
Delle tre repubbliche la lettonia e' quella che ha piu' russi (sono il 50%  
circa), mentre in lithuania sono circa il 30%. Aprire un locale in old riga e'  
un po' come aprirlo a napoli, bisogna pagare la tassa alla mafia, niente di  
nuovo. Nel '96 il metro quadro in old riga costava 300 dollari, ora costa 3000  
dollari. Averlo saputo prima... ma io penso che Riga sara' solo per qualche anno  
solo old riga.  
Ho scoperto che e' possibile comprare 1 litro di vodka a 1 lat (1,5 euro) e' una  
vodka illegale, particolarmente dannosa per l'organismo che viene venduta da  
gente comune nelle case o negli appartamenti. Il costo e' 1/4 della vodka legale  
ed e' appannaggio di moltissima povera gente. Ricordatevi che qui bere ogni sera  
vodka e' pura normalita'.


