Title: Karimblog intervista Karim (parte 1)
Date: 2012-10-12
Category: altro
Slug: karimblog-intervista-karim-parte-1
Author: Karim N Gorjux
Summary: Karim intervista Karim, dopo più di un anno di lontananza dall'Italia e un buon anno sabbatico per il blog ecco che si ricomincia. Non sapevo come cominciare e da dove cominciare, dopo[...]

[![](http://www.karimblog.net/wp-content/uploads/2012/10/Interview.jpg "Interview")](http://www.karimblog.net/wp-content/uploads/2012/10/Interview.jpg)Karim intervista Karim, dopo più di un anno di lontananza dall'Italia e un buon anno sabbatico per il blog ecco che si ricomincia. Non sapevo come cominciare e da dove cominciare, dopo aver lasciato la Lituania ed essere il titolare di uno dei blog più letti sull'argomento Lituania (penso di essere l'unico) che cosa potevo fare? Mi sono concentrato su altre cose e mi sono preso una lunga vacanza.


 Ora è il momento di ritornare. E ho pensato che la cosa migliore fosse di farlo intervistandomi sulle mie impressioni da (ex) expat.


   
### "Ciao Karim, benvenuto sul tuo blog. Come stai?"

  
Bene, va tutto bene. E' strano rispondere alle proprie domande. Mi sento un po' mr B.  
### "Allora, ti manca la Lituania?"

  
Si e no.  
**Mi mancano alcune cose della Lituania** che una volta che le hai viste e vissute ti sembrano normali. Quando torni in Italia ti chiedi perché non sia anche così.


 Ti faccio alcuni esempi, i primi che mi vengono in mente. L'orario delle banche in Lituania è continuato dal mattino fino alla sera e nei grandi centri commerciali di solito c'è uno sportello aperto anche la Domenica. In Italia l'orario è stranissimo persino mia moglie non lo capisce, sembra che passino il tempo a contare soldi a porte chiuse.  
Quando volevo andare a mangiare fuori, non guardavo l'orario, a parte che costa molto meno di qua, ma questo è un altro discorso, se volevo andare a mangiarmi una pizza alle 18 ci andavo ed era più semplice trovare locali con uno spazio per i bambini, qui l'unica cosa che si può fare quando hai dei figli e rimanere a casa.


 Se devi acquistare una SIM per il telefono cellulare vai in un negozio e in due minuti hai il numero, **qui c'è la burocrazia di mezzo e 9 volte su 10** la povera commessa ti dice che hai dei problemi con il computer che si connette al server centrale. In Lituania non mi è mai successo.  
### "Allora ti manca! Vuoi tornare lassù?"

  
L'Italia è un paese che a mio avviso è **destinato ad affondare come il Titanic**. Sta affondando e mentre i passeggeri di prima classe si stanno imbarcando sulle poche scialuppe, la terza classe viene rassicurata che tutto va bene. La seconda si sta buttando in mare cercando di centrare le scialuppe della prima classe.


 Detto questo conviene andare via dall'Italia, ma la Lituania non è affato la soluzione,** i paesi Baltici devono essere presi dal lato giusto ovvero da quello scandinavo**. A meno che si abbiano dei ganci particolari con il lavoro o si ami particolarmente il posto è ben dura andare a vivere in Lituania come alternativa all'Italia.


 Forse la cosa che mi manca di più è la mentalità e in subordine **il livello culturale** e noi italiani siamo completamente fottuti paragonati ad altri popoli. A meno che non si verifichi il proverbiale riscatto italiano ovvero quando il livello di merda è abbondantemente sopra il livello di guardia, noi siamo destinati a soccombere. Gli italiani sono un popolo mediamente ignorante e soprattutto diviso. Le cose intelligenti sono fuori dalla mentalità italica e la futilità e all'ordine del giorno, questo ovviamente in media, poi ci sono degli elementi di spicco, ma ormai sono veramente pochi e quei pochi che valgono fanno le valigie e scappano. **L'Italia non ha più niente da dare agli italiani può solo prendere finché c'è qualcosa da prendere**.  
### "Cavolo, non è molto ottimista come visione dell'Italia"

  
Non è questione di ottimismo, è questione di realtà. Le cose le vedo e non sono rassicuranti. L'italiano è completamente tartassato al minimo movimento che fa e il lavoro non ha perso ogni dignità e rispetto. Bisogna pagare tutto; la benzina aumenta e non si sa bene il perché, le tasse aumentano e le aziende scappano, la televisione che ha una qualità culturale bassa la bisogna pagare e in Lituania che secondo me fa ancora più schifo che qui, almeno è gratis.


 Si paga il bollo auto, l'autostrada, internet cara e di qualità pessima, l'istruzione ha dei costi assurdi e la qualità cala sempre di più anno dopo anno. Intanto i politicanti (parola non usata a caso) dicono sempre le stesse cose insensate. **Le interviste ai soliti noti sono un insulto all'intelligenza** e i vari programmi di attualità che trasmettono in televisione hanno la stessa utilità di leggere il volantino che si trova sul sedile anteriore dell'aereo mentre stai precipitando.  
### "Presumo quindi che la tua scelta di tornare in Italia sia stata sbagliata"

  
Non la penso così. **Gli errori, se insegnano qualcosa non sono più errori**. Io il futuro non lo conosco e preferisco vedere le cose nel suo insieme, ogni evento ha una sua ragione ben precisa di esistere.


 L'esperienza in Lituania mi ha dato molto, questa esperienza in Italia anche. Sinceramente me ne andrei di nuovo, ma ora non devo solo più rendere conto a me stesso, ho una moglie, ma soprattutto dei figli che richiedono stabilità. Greta ha iniziato le elementari ed ha già fatto troppi giri per l'Europa secondo me. Detto questo, potessi fare le valigie, andrei in Australia, ho la cittadinanza e partieri già avvantaggiato, purtroppo non è una cosa che fai su due piedi.  
### "Ti sposteresti di nuovo?"

  
Ciò che ho scritto fin'ora farebbe venire voglia di scappare a chiunque, ma l'Italia ha ancora dei pregi anche se sono ben pochi. Il primo in assoluto è il semplice fatto di **essere nel proprio paese senza essere considerato uno straniero**, il secondo forse è il piacere e l'utilità di avere parenti e amici vicini, poi volendo possiamo mettere anche il cibo e il meteo. Tolto questo l'Italia è una paese da cui scappare e non sono l'unico a dirlo.


 Di Lituani in Italia ne vediamo pochi, **la maggior parte si sposta in Inghilterra**, Germania o nei paesi scandinavi e i lituani non sono scemi, l'Italia è una scelta più amorosa che lavorativa e di opportunità. In questo anno e mezzo ho anche conosciuto persone straniere che hanno deciso di andarsene e di tornare nel proprio paese perché qui si sta peggio che da loro e più parlo in giro e più sento persone che se ne vogliono andare.


 C'è una crisi economica, forse qualcuno non la sente, nel cuneese ci sono ancora tanti ricchi che sembrano nemmeno vederla la crisi, ma la maggior parte delle persone sopravvive alla situazione come può e cerca di difendersi tenendosi un lavoro sottopagato e difendendosi da uno stato che gli è nemico. Si perché** l'Italia (stato) è il peggior nemico degli italiani** seguito solo dalle banche. Purtroppo l'italiano non ha memoria storica, all'italiano medio basta solo che inizi la Serie A poi tutto va bene. Forse un risveglio ci sarà, ma il livello di merda, come ho già scritto sopra, sarà già molto oltre la punta dei capelli.  
### "Ed in Lituania come sta andando?"

  
Anche in Lituania sta andando male, il PIL aumenta, è vero, ma il PIL è un valore in percentuale, è giusto una cifra (a volte due) che non possono riassumere la vita di un paese. La differenza è che la Lituania ha una mentalità migliore della nostra ed un'istruzione molto più seria quindi anche se adesso la Lituania ha i suoi problemi **è molto probabile che nel lungo periodo li risolva** in meglio, mentre noi nel lungo periodo siamo spacciati. Dalla sua parte la Lituania ha una cultura mediamente più elevata della nostra e soprattutto un'unità nazionale. I lituani si sentono un popolo, cosa che noi siamo mai riusciti a fare se non nel finale del film [La Grande Guerra](http://www.youtube.com/watch?v=-tavJaJMtU8 "Finale del film La Grande Guerra").


 Concludendo **la Lituania non è un paese in cui andare a vivere a meno che non si abbia qualche garanzia**, un lavoro da almeno 1000€ al mese e non si conosca un minimo la lingua.  
### "Cosa hai fatto in questo anno e mezzo?"

  
Ho aperto un piccolo negozio di informatica e ho fornito consulenze. Come mi ha detto [un ragazzo del mio paese che viaggia il mondo](http://moltosimonebernardi.blogspot.it/ "Il blog di Simone Bernardi") "...sei venuto a fare il lavoro che facevi in Lituania guadagnando di meno e sbattendoti di più..."

 Ora sto ridimensionando il tutto, mi tolgo il negozio e ho conosciuto persone in gamba con cui collaborare per qualcosa di più interessante. Mi ritengo molto fortunato dopotutto le prospettive sono buone e spero che si realizzi tutto ciò che aspiriamo a raggiungere.


 **-Fine Prima Parte-**

