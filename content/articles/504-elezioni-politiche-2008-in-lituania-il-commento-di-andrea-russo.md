Title: Elezioni politiche 2008 in Lituania: il commento di Andrea Russo
Date: 2008-11-12
Category: Lituania
Slug: elezioni-politiche-2008-in-lituania-il-commento-di-andrea-russo
Author: Karim N Gorjux
Summary: Andrea Russo ha scritto un articolo di facile lettura, ma estremamente veritiero sulla politica lituana e le reazioni dei lituani. Bravo Andrea!Da pochi giorni si sono concluse le elezioni politiche in Lituania.Il[...]

Andrea Russo ha scritto un articolo di facile lettura, ma estremamente veritiero sulla politica lituana e le reazioni dei lituani. Bravo Andrea!  
  
*Da pochi giorni si sono concluse le elezioni politiche in Lituania.  
Il Risultato è stata la vittoria della coalizione liberale, a danno dei partiti del governo uscente.  
A Gediminas Kirkilas succede Andrius Kubilius, che il 28 Ottobre ha ricevuto dal Presidente della Repubblica Valdas Adamkus il mandato di formare il governo.  
Il sistema elettorale lituano prevede il sistema a "doppio turno" ovvero si vota due volte a distanza di 15 giorni.*

 Queste elezioni hanno visto la comparsa sulla scena di un nuovo partito, fatto di attori e personaggi televisivi.  
Nella prima tornata l'affluenza è stata del 40% circa, nella seconda solo del 30%.  
Il popolo lituano non fa un mistero di essere disgustato dalla politica, ma il fattore più preoccupante è la convinzione che c'è in tutti, anche nei giovani, di non poter cambiare le cose, e c'è una rassegnazione che va ben oltre il realismo e le possibilità di cui una democrazia può godere.  
A distanza di 18 anni circa dalla loro "strappo" con l'Unione Sovietica, i Lituani non hanno ancora piena coscienza della loro capacità di cambiare le sorti del proprio paese, anche con la partecipazione popolare alla vita politica. C'è ancora una testardo scoramento come ai tempi dell'ex Urss, e una lontananza dalla percezione di libertà e autonomia che come stato la Lituania gode.  
Eppure non manca un forte senso di nazionalismo in una certa parte della popolazione.


 E non mancano neppure dei segnali di cambiamento.negli ultimi anni, l'economia capitalistica ha fatto capolino nel piccolo paese baltico, e recentemente, nel 2007, la Lituania è stata incoronata come il paese europeo con la crescita economica più alta (7,5%). Questo exploit si spiega con due fattori principali: il primo è che c'è stata un certo afflusso di fondi da parte dell'Unione Europea, e in un paese di 3 milioni e mezzo di abitanti (una popolazione pari a quella di una regione italiana medio-grande a fronte di un territorio pari a 4 regioni circa), anche poche decine di milioni di euro si fanno sentire in maniera forte nel loro apporto all'economia.


 Il secondo motivo è che molti, soprattutto i giovani, sono emigrati all'estero e il loro gettito fiscale è derivante dai guadagni ottenuti all'estero.  
Dalla parte opposta , però, si tenga conto che molte persone in Lituania percepiscono uno stipendio di non oltre 400 500 euro al mese, cifra con cui a Vilnius, la capitale, a malapena si paga l'affitto. Questa disparità tra stipendi e costo della vita fotografa bene la situazione del paese.  
Inoltre, negli ultimi mesi si è avuto un forte incremento dell'inflazione: sono aumentati i prezzi di tutti i prodotti, sia di quelli di uso quotidiano che dei beni più costosi.


 C'è già inoltre, chi critica l'utilizzo dei fondi dati dall'unione europea: essi non verrebbero sfruttati al meglio da imprenditori che non sono ben abituati a confrontarsi con il libero mercato, e detentori di una mentalità spartana derivante dal passato sovietico: in poche parole, non ci sarebbe coraggio negli investimenti, e in alcune nuove attività, si continuerebbe, come prima, a comprare attrezzature già vecchie, senza guardare al futuro e senza la fiducia che un investimento più qualitativo possa portare migliori frutti.


 Tracciato un quadro generale, ciò che è doveroso dire, al di là di tutto, è questo: è dannoso, per un paese ormai da parecchi anni affrancato dai regimi stranieri, non prendere coscienza della propria libertà e della capacità di cambiare il proprio destino.  
Sono ancora molti i giovani che, sebbene cresciuti per buona parte nella Lituania libera, non credono al cambiamento.


 Snobbare i politici con un silenzioso dissenso, più che dare loro un messaggio critico, comporta che le voci di dissenso si attutiscano ancora di più, e chi vuole la "cattiva politica", beneficiando di un disinteresse generale, può a maggior ragione delinquere e governare male perchè trova un terreno propizio.Anche l'argomentazione che molti adducono: qui governaqno ancora, in buona parte, personaggi che comandavano ai tempi dell'Unione Sovietica, non può avere scusanti: spetta ai Lituani mandarli a casa e promuovere l'ascesa di politici di diversa caratura.   


