Title: Iscrizione all'aire per i residenti in Lituania: epilogo
Date: 2009-01-12
Category: Lituania
Slug: iscrizione-allaire-per-i-residenti-in-lituania-epilogo
Author: Karim N Gorjux
Summary: Mi era stato detto che non era possibile, che dovevo per forza presentarmi in ambasciata con mia figlia e che non era ammesso fare tutto per posta. Anche i miei precursori che[...]

[Mi era stato detto](http://www.karimblog.net/2008/11/20/iscrizione-allaire-per-i-residenti-in-lituania-luso-delle-nuove-tecnologie-non-guasterebbe/) che non era possibile, che dovevo per forza presentarmi in ambasciata con mia figlia e che non era ammesso fare tutto per posta.   
  
Anche i miei precursori che abitano a Vilnius mi avevano confermato che bisognava andare a Vilnius. Dato che sono pigro di natura e di farmi 600km in macchina non ne ho nessuna voglia, ho chiesto [al giramondo](http://flaepitta.blogspot.com/) più giramondo che conosco consiglio su come procedere e il risultato è stato questo:

   
2. Ho scaricato **dal sito dell'ambasciata italiana di Riga** il documento da compilare per richiedere l'iscrizione all'Aire.

  
4. Ho stampato il documento e l'ho compilato, poi l'ho scannerizzato
  
6. Ho scannerizzato il permesso di soggiorno e il certificato di residenza lituano
  
8. Ho scritto un email allegando carta d'identità, passaporto mio e di mia figlia e i vari documenti lituani scansionati. Ovviamente ho anche incluso il documento di iscrizione all'aire.

  
10. Nell'email ho scritto di avvalermi dell'autocertificazione (art. 76 d.p.r. 445 del 28/12/2000)
  


 La risposta dell'ambasciata è stata questa:  

>   
> Gentile Signor Karim,

 questo Ufficio ha provveduto ad iscrivere Lei e Sua figlia alla Anagrafe Consolare dell'Ambasciata e a trasmettere la pratica al Comune di Centallo che provvedera' a trascriverLa nell'elenco AIRE del Comune.  


