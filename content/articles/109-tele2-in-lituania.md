Title: Tele2 in Lituania
Date: 2005-10-25
Category: altro
Slug: tele2-in-lituania
Author: Karim N Gorjux
Summary: Ho comprato una ricarica da 10€ per il cellulare il 28 Settembre. Oggi è il 25 Ottobre e non ho ancora finito il credito. Chiamo, mando messaggi eppure non ho l'impressione che[...]

Ho comprato una ricarica da 10€ per il cellulare il 28 Settembre. Oggi è il 25 Ottobre e non ho ancora finito il credito. Chiamo, mando messaggi eppure non ho l'impressione che i soldi mi vengano *bruciati*, per magia, come accade in Italia con la Tim o sorelle.  
  
In Italia con il cellulare non mi bastava una ricarica da 50€ per 3 settimane, i soldi non ho ben capito come me li fotte, ma me li fotte. Almeno in Lituanio gli utenti della telefonia mobile non sono visti come limoni.


 Le giornate si susseguono all'insegna del freddo della pioggia, l'autunno colora i parchi di rosso e giallo, il vento soffia inesorabile ogni giorno, al mattino la temperatura arriva a toccare i 0 gradi. Questa è la Lituania.


 Per il week-end non sarò a casa, ma andrò in giro per la Lituania, probabilmente nella capitale, ma cosa mi entusiasma e che forse ho trovato una macchina per spostarmi nei posti più disparati.


 Vedremo..


