Title: ZML: butteremo i dvd?
Date: 2007-11-15
Category: altro
Slug: zml-butteremo-i-dvd
Author: Karim N Gorjux
Summary: Marco mi ha gentilmente segnalato, tramite delicious, il sito della zml. Mi è bastato dare un'occhiata per capire da subito che è un sito rivoluzionario; ZML permette di scaricare film in divx[...]

[Marco](http://www.marcorossoni.com) mi ha gentilmente segnalato, tramite [delicious](http://del.icio.us), il sito della [zml](http://www.zml.com). Mi è bastato dare un'occhiata per capire da subito che è un sito rivoluzionario; ZML permette di scaricare film in divx o in altro formato compatibile con i palmari e l'iPod a prezzi estremamente contenuti. L'unica pecca? E' **solamente** in lingua inglese.


 In questa [pagina](http://www.zml.com/movie/simpsons-movie-the-99261.htm) del loro catalogo, il film dei Simpson costa **2,99$** in versione DivX e **1,99$** in versione PDA/iPod, è possibile leggersi la trama e avere informazioni varie sul film. Il download è disponibile istantaneamente.


 Non l'ho provato, ma se mantiene la promessa, questo sarà **il vero concorrente **di [iTunes](http://www.apple.com/it/itunes/)?

