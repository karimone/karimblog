Title: In spiaggia
Date: 2008-09-07
Category: Lituania
Slug: in-spiaggia
Author: Karim N Gorjux
Summary: Dov'è la Lituania? Molte persone hanno grandi difficoltà a rispondere a questa domanda. C'è chì pensa ché la Lituania sia in Africa o vicino alla Romania, in realtà la Lituania è nel[...]

Dov'è la Lituania? Molte persone hanno grandi difficoltà a rispondere a questa domanda. C'è chì pensa ché la Lituania sia in Africa o vicino alla Romania, in realtà la Lituania è nel nord est europeo e io ho la fortuna di vivere sulla costa baltica. 

 La spiaggia è completamente libera e accessibile a chiunque, piccoli bar, spogliatoi e toilette sono presenti un po' ovunque. Oggi 25 gradi circa, ventilato con sole a sprazzi. Qualcuno fa anche il bagno.   
[![](http://www.karimblog.net/wp-content/uploads/2008/09/p-640-480-6d83c7f8-f961-413b-929a-4fa91edf2ca8.jpeg)](http://www.karimblog.net/wp-content/uploads/2008/09/p-640-480-6d83c7f8-f961-413b-929a-4fa91edf2ca8.jpeg)



