Title: Piacevolmente sorpreso: l'asilo in Lituania
Date: 2008-09-10
Category: Lituania
Slug: piacevolmente-sorpreso-lasilo-in-lituania
Author: Karim N Gorjux
Summary: Sono quasi 20 giorni che sono qui in Lituania; ieri sono arrivate la roba le nostre cose e tutti e tre ci sentiamo meno profughi e più residenti. A dire il vero[...]

Sono quasi 20 giorni che sono qui in Lituania; ieri sono arrivate la roba le nostre cose e tutti e tre ci sentiamo meno profughi e più residenti. A dire il vero abbiamo solo l'essenziale, molte cose sono rimaste nella tavernetta di mia madre in Italia, ma tra quelle poche cose che ho portato non poteva mancare la **macchina del caffè** che, come potrà confermare ogni informatico professionista, è uno strumento di lavoro **insostituibile** alla pari della sedia e il mouse.  
  
Sono contento, non avrei mai creduto di trovarmi bene, ma così è. La Lituania è cambiata molto in questi due anni che sono stato in Italia e la sensazione che ho tutti i giorni, quando passeggio per la città o prendo la macchina per spostarmi è che la Lituania continua imperterrita a migliorarsi. Le strade cambiano, i palazzi crescono come funghi e le innovazioni tecnologiche sono un po' dappertutto.


 Sono stato piacevolmente sorpreso da molte cose, alcune utili e altre meno. Personalmente mi tocca molto da vicino il discorso **asilo**. In Italia la scelta possibile era solo una: l'asilo privato. Quando Rita frequentava l'università tutti i giorni dalle 8 del mattino alle 17 di sera, eravamo obbligati a portare Greta tutto il giorno all'asilo. Non le davano da mangiare quindi dovevamo preparare tutto noi il giorno prima o portare qualcosa durante la giornata, i costi si aggiravano sui 300€ mensili (trecento euro) e per qualche periodo il comune elargiva dei contributi che allegerivano la retta.


 In Lituania l'asilo è statale, non so bene da che età possono entrare i bambini, ma Greta che ha appena compiuto **due anni**, fa già parte di un gruppetto di 10 bambini chiamato "la coccinella" e le maestre a loro disposizione **sono due**. Il costo dell'asilo è all'incirca di 40-50€ al mese, gli orari sono elastici per andare incontro alle esigenze dei genitori. Il mattino i bambini devono entrare per le 9 se vogliono fare la colazione e la sera possono rimanere all'asilo fino alle 18. Inoltre, dato che Rita è studente, la rata mensile è **dimezzata**.


 A voi i commenti, ma una domanda mi pare opportuna: perché ci si soprende tanto quando [l'istat](http://demo.istat.it/bil2007/index.html "Bilanci demografici dell'ISTAT per l'anno 2007") ogni anno ci informa che in Italia la natalità è meno di zero? (Per l'anno 2007 è stata di -6868)

   
[![Asilo 4](http://farm4.static.flickr.com/3258/2824652605_18e100c125_m.jpg)](http://www.flickr.com/photos/kmen-org/2824652605/ "Asilo 4 di karimblog, su Flickr")  
  
  


