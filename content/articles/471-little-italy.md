Title: Little Italy
Date: 2008-07-31
Category: altro
Slug: little-italy
Author: Karim N Gorjux
Summary: Visto il notevole bagaglio di esperienza maturata nella mia precedente permanenza in Lituania, ho studiato come dovrebbe essere organizzata la mia nuova permanenza in modo da non soccombere dalla tristezza e dalla[...]

Visto il notevole bagaglio di esperienza maturata nella mia precedente permanenza in Lituania, ho studiato come dovrebbe essere organizzata la mia nuova permanenza in modo da non soccombere dalla tristezza e dalla malinconia.


  In primo luogo deve esserci una mentalità positiva, la Lituania è com'è e non ci si può fare niente, le persone sono come sono e il tempo (meteorologico) non lo si può cambiare. L'accettare l'inevitabile è un modo saggio e tranquillo di vivere.  
*  
"Non serve a niente discutere con l'inevitabile: l'unico argomento possibile con un vento maligno è mettersi il cappotto."*  
James Russell Lowell

  
Il più grande errore nella mia precedente permanenza è stato non creare la mia Little Italy. E' importantissimo che la propria casa sia un posto di rilassamento e pace dove tutti i problemi del paese che ti ospita (non esiste il paese perfetto) rimangono al di fuori di quelle quattro mura. La Lituania deve rimanere fuori dalla porta di casa e per farlo ho deciso di attuare in questo modo:



   
 * Prendere una casa **nuova** in stile europeo: niente vasca - lavandino, niente cucina di grande puffo o scale condominiali che puzzano di falce e martello. Lungi via da me la vista di una qualsiasi casa [Krusciov](http://it.wikipedia.org/wiki/Krusciov "Presidente Krusciov"). Costi quel che costi è necessario che la casa sia accogliente perché non è una spesa, è un investimento.

  
 * Portarsi il **decoder** [Sky](http://www.sky.it/): è vero che la nostra tv è veramente uno schifo ed è un insulto all'intelligenza di chi guarda, ma su sky ci sono alcune cose che è interessante poter seguire e aiuta soprattutto a sentirci meno lontani da casa. Volete mettere il piacere di guardarsi il telegiornale, annozero o la Juventus come se fossimo a casa?
  
 * Il **caffè**: dato che ho la macchina del caffè me la porto dietro con tanto piacere. Nelle giornate grigie, piovose e fredde una bella tazza di caffè nostrano aiuta a rissolevarsi e a concentrarsi sul lavoro
  
 * Il **lavoro**: è molto importante avere qualcosa da fare che impegni tantissimo in modo da far diventare l'inverno lungo è grigio l'occasione per potersi buttare sulla propria attività
  
 * I **libri**: ringraziando è possibile acquistarne online e farseli spedire direttamente a casa senza spendere un patrimonio, ma è meglio portarsi subito qualcosa da leggere sulla poltrona
  
 * l'**affettatrice**: in Lituania non esiste la cultura dell'affettare la carne. Non hanno il prosciutto crudo di Parma da mangiare con il melone, ma almeno hanno delle belle fettone di altre carni che si possono affettare tranquillamente.

  
 * Il **computer e internet**: per me è obbligatorio, ma per altri forse no. L'unico modo per non sentirsi isolati è avere internet ed usare email, web, skype per sentirsi vicini agli amici e ai parenti.

  
 * Il **telefono voip**: io lo uso per lavoro, ma costa poco ed è utilissimo. Se vi registrate su [euteliavoip](http://www.euteliavoip.it) è possibile ottenere un numero fisso da poter utilizzare collegato ad internet così chi vi chiama paga un urbana e voi rispondete in Lituania. Io ho fatto di meglio, ho chiesto di portare il mio vecchio numero di telefono in Voip quindi userò lo stesso numero di telefono che avevo qui in Lituania. Geniale.

  
 * La **macchina**: Klaipeda, dove andrò abitare io, è ben servita da Mini Bus, Bus e Taxi, ma una macchina fa la vera differenza. Andare a Vilnius o a Telsiai o diretamente tornare in Italia non è un grosso problema se si ha una macchina. Non è indispensabile, ma è altamente consigliato.

  
 * La **Wii**: non è proprio necessaria, ma sapete com'è... sono un bambinone.

  
  
Sarebbe anche opportuno fare dei rientri frequenti nel bel paese almeno ogni quattro mesi, ma io voglio essere più specifico. E' molto importante combattere il buio che è la più grande causa di depressione in quei posti e lo si può fare in tanti modi:  
  
 * Facendo dei **solarium**. Sembra uno scherzo, ma non lo è, farsi dei solarium di tanto in tanto aiuta ad essere più "felici" e aiuta ad affrontare il buio.

  
 * **Socializzare**: è molto difficile socializzare in Lituania, soprattutto con gli uomini. Si socializza sempre con le donne e il confine del doppio fine è sempre in vista.

  
 * Fare **dell'attività fisica**: oltre che aiutare il corpo e la mente è una buona possibilità di socializzazione
  
 * **Scappare** dalla Lituania, di tanto in tanto: non è detto che bisogna sempre tornare a casa in Italia, soprattutto d'inverno che anche in Italia non scherza. Se è possibile è buono partire per 4-5 giorni in paesi caldi come la Gran Canarie che non sono tanto distanti dall'Europa.

  
  
Un ultima cosa, forse la più importante, organizzazione! Sapere dove si sta andando rende le cose più chiare e la motivazione più alta mentre si affrontano le giornate. Sapere che ci sono obiettivi, mete, vacanze aiuta a non perdersi d'animo.


 Non è finita qui, scriverò ancora sulla realizzazione della Little Italy.


