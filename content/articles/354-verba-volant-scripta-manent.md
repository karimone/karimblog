Title: Verba volant scripta manent
Date: 2007-08-03
Category: altro
Slug: verba-volant-scripta-manent
Author: Karim N Gorjux
Summary: Il passaggio da cazzone perditempo spensierato a padre di famiglia imprenditore comprende varie lezioni di vita incluse nel prezzo. Il bello di queste lezioni è che in teoria sono gratis, ma in[...]

Il passaggio da cazzone perditempo spensierato a padre di famiglia imprenditore comprende varie lezioni di vita incluse nel prezzo. Il bello di queste lezioni è che in teoria sono gratis, ma in pratica passi il tempo a spalmarti il voltaren sulle chiappe.


 Le parole valgono nulla, sono aria fritta, cazzate. Il mio [gestionale](http://www.naxia.it/poweroffice) oltre a migliorare la vita lavorativa impone un metodo di lavoro che ho adottato da subito. Un metodo di lavoro segue la filosofia [verba volant scripta manent](http://it.wikipedia.org/wiki/Verba_volant,_scripta_manent) ovvero *le parole volano, gli scritti rimangono*.   
Ai miei clienti faccio delle offerte, loro ne accettano una, discutiamo sul pagamento e sui tempi di consegna e poi gli faccio avere una **conferma d'ordine** che mi devono restituire firmata. Senza quel foglio firmato io non accendo manco il computer. 

 Quando l'idea imprenditoriale ha iniziato a concretizzarsi un amico che tale non è, mi ha promesso mari e monti, un altro mi ha promesso un lavoro per un sito comunale un altro... tutte cavolate. Se non c'è niente di scritto, se non c'è una firma, non illudetevi.


 Tra l'altro ci sono persone con cui **dovrei comunicare solo per iscritto,** un po' come con i log di skype che tra l'altro non cancello mai, questo non tanto per rivendicare le cose dette-non-fatte, ma soltanto per ricordarsi con chi stiamo parlando.


