Title: Perdonatemi, sono pigro e scrivo poco
Date: 2005-01-19
Category: altro
Slug: perdonatemi-sono-pigro-e-scrivo-poco
Author: Karim N Gorjux
Summary: Non posso scrivervi molto oggi, ho avuto da fare con il computer e non ho avuto molto tempo per aggiornarvi con le mie avventure. Ho passato la serata con Milda, forse è[...]

Non posso scrivervi molto oggi, ho avuto da fare con il computer e non ho avuto molto tempo per aggiornarvi con le mie avventure. Ho passato la serata con Milda, forse è l'ultima volta che la vedrò perchè torna a casa dai genitori dove rimarrà per 2 settimane. Non voglio dare un'indicazione per una mia ipotetica rimpatriata, perchè a dire il vero non ne sono sicuro, forse rimarrò abbastanza per rivederla.


 La mia serata è stata particolarmente piacevole. Il ragù di Andrea e gli spaghetti Barilla sono un connubio semplice ed elegante, se poi sono accompagnati con della buona musica e un buon ambiente, rendono la serata memorabile. Non mi dilungo oltre con dettagli e pettegolezzi, zittisco subito le malelingue, tra me e Milda è nata una tenera e dolce amicizia. Lei mi ha aiutato a orientarmi in Klaipeda e mi ha dato informazioni interessanti sulla Lituania, inoltre mi ha fatto riscoprire il piacere di conoscere una persona sincera e buona con dei valori nobili come la famiglia e l'amicizia.  
Milda mi ha confessato che il suo paese non le garantisce un futuro anche se dovesse ottenere la laurea a pieni voti. Ma lei proprio non se la sente di lasciare la sua casa. Non se la sente di dividersi da coloro che l'hanno messa al mondo e che nei suoi 19 anni di vita, nel bene e nel male, le hanno dato l'amore necessario per sopravvivere ad una realtà locale molto diversa dalla nostra. Scrivo queste righe, perchè mentre l'ascoltavo mi sono venuti alla mente dei discorsi che purtroppo sento spesso e sono molto diversi dalle parole di Milda.


 Ho iniziato a scrivere questo articolo con tutt'altre intenzioni, ma come al solito divago e mi perdo nei pensieri. Milda questa sera mi ha detto che ogni tanto sembro un bambino. Sarà che ho una dote naturale a essere buffo, ma nel momento in cui me lo ha detto ho avuto un flash. Volevo scriverne giorni addietro, ma ho sempre rimandato e ora colgo l'occasione grazie a Milda.  
Mi trovavo a Riga e dovevo telefonare in Italia, come di consueto evito di usare il cellulare-sanguisuga e compro una scheda per il telefono pubblico (quelle casse da morto verticali che in Italia non sono fashion e che se ci vedi qualcuno dentro ti chiedi se sei tornato indietro nel tempo). Inizio a chiamare, ma ho dei problemi, non prende la linea. Al quarto o quinto tentativo suona libero. Inaspettatamente mi risponde una vocina esile, innocente e simpatica con un buffissimo: "Proooooooonto?".


 Ho un attimo di esitazione, non mi aspettavo una risposta da parte di un bambino, i miei neuroni tentano di dare velocemente una spiegazione e concludo che non poteva altro che essere un mio cuginetto o qualche altro piccolo conoscente che aveva risposto prima di un adulto. Di conseguenza è per me naturale la più banale e sintetica delle risposte:

 "Ciao. Chi sei?"  
"Ciao, io sono Fabrizio. Tu come ti chiami?",  
"Karim",  
"Ciao Karim, che bel nome."

 Mi sentivo sempre più spiazzato, la voce del bambino era simpaticissima, avrei voluto chiacchierarci un pò, ma una voce adulta in sottofondo disse:

 "Fabrizio, chi è?"  
"E' Karim"  
"Karim????? Passa a me per favore"

 Si conclude il disguido, avevo sbagliato numero, la persona è gentilissima e mi saluta e augurandomi buone feste. Non saprei perchè mi è rimasto impressa questa telefonata, forse la voce innocente, il modo di fare amichevole o quel "E' Karim", come se mi conoscesse da anni. Probabilmente il piccolo Fabrizio, con la sua semplicità e l'incontaminato piacere di apprezzare ogni sorpresa della vita, mi ha ricordato come i bambini non sono ancora macchiati dalle stranezze adulte, dove stranezze è un ovvio eufemismo.


