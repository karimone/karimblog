Title: Con l'arrivo dell'anno nuovo approfitto per pensare a idee nuove per questo blog
Date: 2009-01-06
Category: Lituania
Slug: con-larrivo-dellanno-nuovo-approfitto-per-pensare-a-idee-nuove-per-questo-blog
Author: Karim N Gorjux
Summary: Finalmente le feste sono finite, in salotto abbiamo ancora l'albero di Natale, ma entro un paio di giorni finirà nello scatolone in attesa delle prossime feste.

Finalmente le feste sono finite, in salotto abbiamo ancora l'albero di Natale, ma entro un paio di giorni finirà nello scatolone in attesa delle prossime feste.


 Se me lo avessero detto un anno fa, non ci avrei mai creduto. Ed invece eccomi qui, sono più di quattro mesi che sono in Lituania e tutto va bene, ho persino mangiato il **pandoro** che ho comprato al Maxima per la bellezza di 10€.  
  
In questi giorni fa veramente freddo, siamo scesi a -10 toccando punte di -15 durante la notte, ma il peggio è dovuto al vento che qui a Klaipeda la fa da padrone. Uscire di casa per fare due passi è un'impresa, sembra di essere in mezzo ad una bufera, quindi passiamo il più del tempo in casa e se usciamo ci rinchiudiamo in qualche locale.


 Durante le feste ho avuto modo di pensare al mio blog e ho deciso di limitare se non addirittura **eliminare totalmente** i miei interventi nell'area commenti.  
Qualche motivazione della scelta:  
  
2. Ho un punto di vista e voi avete il vostro
  
4. Io scrivo gli articoli e mi sembra già più che sufficiente
  
6. Se voglio rispondere ai commenti dei lettori, posso farlo direttamente in un nuovo post
  
  
Non rispondere ai commenti mi permetterà di avere più tempo per scrivere gli articoli e rendere questo blog ancora più ricco.


 Staremo a vedere.


