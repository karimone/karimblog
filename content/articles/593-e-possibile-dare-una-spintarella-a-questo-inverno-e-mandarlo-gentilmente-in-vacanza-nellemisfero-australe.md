Title: E’ possibile dare una spintarella a questo inverno e mandarlo gentilmente in vacanza nell’emisfero Australe?
Date: 2010-03-09
Category: Lituania
Slug: e-possibile-dare-una-spintarella-a-questo-inverno-e-mandarlo-gentilmente-in-vacanza-nellemisfero-australe
Author: Karim N Gorjux
Summary: Oggi è il 9 Marzo e fuori nevica. Il sole sorge alle 6:30 del mattino per tramontare intorno alle 18, si sente nell'aria che l'inverno ci sta lasciando, ma tende a farlo[...]

[![](http://farm5.static.flickr.com/4029/4271999372_9f64c72341_m.jpg "Il mare che sembra la giasa")](http://www.flickr.com/photos/kmen-org/4271999372/)Oggi è il 9 Marzo e fuori nevica. Il sole sorge alle 6:30 del mattino per tramontare intorno alle 18, si sente nell'aria che l'inverno ci sta lasciando, ma tende a farlo molto lentamente ed infatti fa freddo e giusto per infierire, quando c'è il sole fa ancora più freddo. Ieri è stata una giornata bellissima, sole e cielo azzurro, ma al mattino alle 8 il termometro segnava -13.


 **Si aspetta la primavera, la gente è stanca del freddo e quest'inverno è stato molto più duro dell'inverno passato.** Sabato ero in Klaipeda con Rita e Greta, la piazza era piena di bancarelle che vendevano prodotti tipici della Lituania tra cui un tipico bastone ornato di fiori che rappresenta l'arrivo della primavera. Penso che ci siano stati almeno 5 o 6 gradi sottozero, dopo 20 minuti a girare per la piazza e a comprare cianfrusaglie, ci siamo rifugiati in un locale a bere the e caffe bollenti. Stavamo congelando.  
  
**Nel pomeriggio di Domenica, abbiamo pensato di farci una passeggiata nella foresta**. La Domenica è sempre una giornata un po' particolare perché la mattina la passo in palestra, per poi farmi la sauna e la barba dopo la doccia. Una volta arrivato a casa, consumiamo il pranzo prima di fare la pennicchella pomeridiana. Dopo le 14/15 usciamo tutti e tre, ma Domenica faceva davvero tanto freddo e quindi abbiamo deciso di fare una passeggiata nella foresta qui vicino.


 **La foresta è completamente bianca,** l'ultima settimana le temperature erano salite miracolosamente sopra lo zero portando di prepotenza i gradi nell'insieme dei numeri positivi e quindi la neve si stava sciogliendo, le strade erano sempre bagnate e si iniziava ad intravedere l'erba nei prati. Purtroppo è bastatata una nevicata per riportare tutto alla "normalità", facendo rientrare Klaipeda prepotentemente in questo lungo inverno che sembra non finire mai.


 E' strano camminare attraverso il percorso che taglia la foresta, vedere tanti e maestosi alberi bianchi, il sole e il cielo azzurro, ma essere imbacuccati e morire dal freddo. **C'è persino gente che invece di fare la passeggiata si diverte a fare sci di fondo** e quindi bisogna prestare un po' di attenzione alla bambina per evitare brutti scontri. E' bello e rilassante, ma d'estate è molto meglio perché dopo la foresta c'è il mare e cosa c'è di meglio di passare l'estate al fresco e al mare?

 **Quando c'è il sole in casa si muore dal caldo, letteralmente**. Il balcone del nostro appartamento è di quelli chiusi con una vetrata per renderlo utilizzabile anche d'inverno, ma appena c'è un po' di sole basta tenere la vetrata chiusa per fare l'effetto serra e farsi la sauna abbronzante in casa. E' strano rimanere sul balcone in maniche corte e vedere la gente fuori vestita di piumone, sciarpa berretto e guanti che porta a passeggio il cane.


 Oggi passo la giornata in ufficio, spezzando le ore di lavoro con tisane e caffè, ma stasera alle 18 andrò al corso di [Wing Chun](http://it.wikipedia.org/wiki/Wing_Chun), ormai è quasi un mese che lo frequento e tra il mio lituano maccheronico, le giornate in palestra, il corso di Wing Chun e le partite di calcio d'estate, mi sento davvero un Klaipedese.


