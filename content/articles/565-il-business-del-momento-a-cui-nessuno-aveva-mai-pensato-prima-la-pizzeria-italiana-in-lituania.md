Title: Il business del momento a cui nessuno aveva mai pensato prima: la pizzeria italiana in Lituania
Date: 2009-09-20
Category: Lituania
Slug: il-business-del-momento-a-cui-nessuno-aveva-mai-pensato-prima-la-pizzeria-italiana-in-lituania
Author: Karim N Gorjux
Summary: Vado a farmi un bel giro a TelŠiai (si legge telshiai) dai suoceri e scopro, con molto stupore, ma ancora più sorpresa che qualcuno ha avuto il coraggio di aprirci una pizzeria.[...]

[![](http://farm4.static.flickr.com/3171/3679501981_fcd5894ebe_m.jpg "Pizza!")](http://www.flickr.com/photos/ludmila_tavares/3679501981/)Vado a farmi un bel giro a TelŠiai (si legge telshiai) dai suoceri e scopro, con molto stupore, ma ancora più sorpresa che qualcuno ha avuto il coraggio di aprirci una pizzeria. Ora,dubito che tu sappia [dove si trovi TelŠiai](http://it.wikipedia.org/wiki/Telšiai), ma sappi che è uno di quei paesi piccoli piccoli dove il cinema non esiste e se esci di casa non sai che fare.  
  
La pizzeria asseconda una delle leggi non scritte della Lituania: **non ha importanza dove si trovi il tuo ristorante, se fai bene da mangiare sarà come essere in centro paese**. Così è che Nunzio ha avuto il coraggio, secondo la classica mentalità italiana, di aprire in un posto sperduto ed introvabile nel piccolo paese di TelŠiai. La pizzeria è fatta nel classico stile lituano dove tutti i mobili sono in legno, il prezzo è alla portata delle tasche lituane in piena concorrenza con i vari cili e mambo pizza, mentre la musica è rigorosamente tricolore.


 Dopo più di un anno di permanenza in lituania mi ha fatto davvero piacere mangiare una pizza che assomigliasse più alla definizione italiana che alla definizione lituana, mi ha fatto piacere conoscere il già citato Nunzio, padrone del locale e vistosamente originario delle nostre terre del sud. Un piccolo disappunto, ma niente di più, me lo ha dato lo scambio di opinioni con un altro connazionale presente nella pizzeria. Gli argomenti sono stati i classici luoghi comuni che, come direbbe [Pino Scotto](http://it.wikipedia.org/wiki/Pino_Scotto), "mi hanno abbondantemente abbuffato le palle", i lituani qua, la pizzeria la, il lavoro, i fondi europei, noi siamo più... argomenti triti e ritriti che si scontrano inesorabilmente con una visione tipica dell'Italiano legato alla *"cultura più bella del mondo".*

 Non ho approfondito i discorsi più di tanto, ma il signore in questione vive in un altro piccolo paese da circa 1 mese e mezzo, la solita saccenza, le buone speranze di fare il business definitivo e soprattuto le varie conoscenze di cui si vantava spero gli siano di buon auspicio, ma intanto la mia personalissima teoria che gli italiani che vivono all'estero hanno un "qualcosa di strano" sta prendendo sempre più fondamento.


 Per concludere: a Klaipeda, da quando ci bazzico io, sono stati aperti ben tre pizzerie ristoranti di pura cucina italiana, hanno chiuso tutti e tre, mentre i vari Cili, Mambo, Bambola Pizza sono ancora tutti li. Auguro buona fortuna, anche perché a me la pizza è davvero piaciuta.


