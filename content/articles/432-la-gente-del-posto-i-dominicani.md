Title: La gente del posto: i dominicani
Date: 2008-03-29
Category: altro
Slug: la-gente-del-posto-i-dominicani
Author: Karim N Gorjux
Summary: Boca de Yuma è una piccola favelas. Tanti bambini scalzi e seminudi, strade sporche e impolverate e qualche piccolo ristorante "nel centro" che non ha i problemi di visite da parte dell'ASL[...]

Boca de Yuma è una piccola favelas. Tanti bambini scalzi e seminudi, strade sporche e impolverate e qualche piccolo ristorante "nel centro" che non ha i problemi di visite da parte dell'ASL locale.


 I dominicani sono persone gentilissime, non sono violenti anche se vederli girare con il macete può fare un po' effetto; sorridono sempre e, spesso, sono loro a salutarti per primi. Tutta questa gentilezza però può avere uno scopo: il bianco qui è visto come un'opportunita sia dagli uomini che dalle donne. Gli uomini sono sempre disponibilissimi a venderti qualsiaisi cosa o ancora peggio a chiederti dei soldi in prestito.  
  
Il primo giorno che ero qui un tizio è venuto a chiedermi 50 pesos (1€ circa), non gli ho dato nulla, ma non perché sono tirchio fino al midollo osseo, ma semplicemente perché se inizi a regalare soldi a chi capita, ti fai la fama di "banca per poveri" e poi ti ritrovi la fila di gente davanti a casa dalla mattina alla sera.


 Le donne invece si vendono per 10 o 20 euro, rispetto agli uomini che ti chiedono soldi per nulla, le donne danno qualcosa in cambio. Dato che il tipico italiano da queste parti è il PP (Pensionato Puttaniere), andare con la nera di turno oltre che squallido può essere molto pericoloso per la salute.


 A sentire loro, i dominicani sanno tutto e sanno fare tutto. Nessuno ti dirà mai di no. Se vai da un ragazzino che a malapena sa leggere e gli chiedi di metterti a posto il computer lui ti risponderà: "No problema, amigo!" e tu sei nella merda fino al collo. L'unica persona istruita del posto è "l'ingegnero" e ha un'istruzione equivalente alla nostra terza media, ma cosa trovo sorprendente è che sto ragazzo è realmente un ingegnero con la laurea. E' una brava persona, umile e onesta, ma non capisce una sega.


 Lello che per chi non lo avesse capito è colui che mi ha fatto venire qui, è di cuore troppo buono. Tutti i bambini che gli chiedono qualcosa lui è sempre pronto ad esaugire i loro desideri; persino i cani e i gatti che vengono a battere cassa davanti alla porta, sono diventati i nostri inseparabili compagni di viaggio ovunque andiamo.


 I bambini ad essere sincero non li sopporto più anche se all'inizio mi erano simpatici. Sono troppo invadenti e maleducati. Ti entrano in casa, toccano tutto, ti seguono perfino nel cesso e se non fai attenzione si portano via qualcosa di nascosto.


 Per fortuna c'è un sole ed un mare bellissimo.


