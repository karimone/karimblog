Title: Si parte per l'Italia, grazie per gli auguri
Date: 2006-10-06
Category: Lituania
Slug: si-parte-per-litalia-grazie-per-gli-auguri
Author: Karim N Gorjux
Summary: Dato che Domenica partirò definitivamente per l'Italia, non potrò scrivere tante cose riguardo la Lituania, forse potrò cambiare il mio blog in: "come sono riuscito a vivere da signore in Italia con[...]

Dato che Domenica partirò definitivamente per l'Italia, non potrò scrivere tante cose riguardo la Lituania, forse potrò cambiare il mio blog in: "come sono riuscito a vivere da signore in Italia con la moglie e la figlia lituana", sperando di contrastare i malefici auguratomi da un [forum](http://www.italietuva.com/forum) di gente che bene o male ha intrallazzi con italiani e lituani. Guardate voi stessi: [Me ne vado da Klaipeda](http://www.italietuva.com/forum/Me-ne-vado-da-Klaipeda-t6331.html).


 Sono sempre stato criticato di come parlo della Lituania, ma molti in silenzio e soprattutto in privato **confermano ciò che scrivo**, la Lituania **non turistica** è dura, molto dura. Non si lavora, si vive male e in rapporto agli stipendi gli affitti sono molto più cari dei nostri, il popolo è molto chiuso tra di loro e ancora di più con gli stranieri. Queste sono tutte cose che ho visto e che non mi invento. Sul famoso forum invece la gente ama la Lituania, ma la ama stando in Italia e guadagnando in Italia, lamentandosi di come l'Italia abbia questo e quell'altro difetto, ma come potrete leggere alla mia semplice domanda: **Perché non vieni ad abitare qui?** Non ha mai avuto risposta. Le persone che frequentano quel forum ed abitano in Lituania lavorando in Lituania e vivendo come un lituano sono davvero poche e hanno tutta la mia stima anche se abbiamo idee contrastanti.  
  


 Sostanzialmente quel forum è popolato da persone che decantano la Lituania per le donne, vorrebbero abitare qui, ma come fossero in Italia e con tutte le comodità italiane. Ne ho piene le balle di sentire parlare bene della Lettonia o della Lituania da gente che è andata 15 giorni in vacanza a Vilnius o a Riga pensando solo di divertirsi con le ragazze e basta! Ma soprattutto ne ho piene le balle di sta gente che pensa di avere capito la realtà locale. Basta! Avete rotto le balle! 

 Poi leggo i problemi delle donne lituane di quel forum, donne che abitano in Italia e hanno grandissimi problemi di integrazione. Si lamentano di come le patate non permettono di cucinare i cepelinai o che l'inverno è troppo freddo o la luce è troppo chiara o l'estate è troppo calda, le serrande non servono, le banche sono chiuse all'una. Certo che con la puntualità è l'affidabilità del lituano medio le banche dovrebbero essere aperte 24 ore al giorno, ma la domanda è sempre la stessa: **ma perché non tornate ad abitare in Lituania? **Io me ne vado dopo aver vissuto due anni in un casa sovietica dove la vasca da bagno in una toilette di 1 metro per 1 metro è usata come bidè, lavabo e doccia. Ovviamente di persone che frequentano quel forum che abitano in Lituania in una casa sovietica ne ho contanto solo uno... su 464. 

 **Rinnovo la mia offerta.** A fine Novembre lascio il mio appartamento di Klaipeda a 5 minuti dal centro, è completamente arredato. Affitto e spese sono 700 litas circa (200€), l'appartamento è composta da una grande sala con divano letto e televisione, bagno sovetico con lavatrice, camera da letto con armadio e cucina moderna. Il tutto con balcone vista amianto. Se uno di quei quegli sbruffoni decantatori della Lituania ha voglia di venire a godersi la vita in questo paradiso ha solo da contattarmi che gli lascio l'appartamento per fine Novembre. In quel forum gli iscritti sono 464, vuoi che per la legge dei grandi numeri almeno uno mi contatterà? Ne dubito.


 **Avviso alla scema che scrive in italiano come Tarzan:** non sono arabo e non metto il burka a mia moglie o mia figlia. Mia moglie legge il forum e anche mia suocera, il post riguardo [l'opportunismo](http://www.karimblog.net/2006/10/04/opportunismo-o-timidezza) della gente merdosa che ho avuto a che fare qui in Lituania non l'hai nemmeno capito. Sei una persona ignorante, maleducata e soprattutto razzista. **Pussa via dal mio blog!**

