Title: Lettonia - Galles 0:2
Date: 2004-08-23
Category: Lettonia
Slug: lettonia-galles-02
Author: Karim N Gorjux
Summary: Ieri hanno giocato la partita amichevole Latvia - Wales e in questi giorni Riga e' stata invasa dai supporters gallesi. Ho fatto la conoscenza di alcuni di loro che oltre essere stati[...]

Ieri hanno giocato la partita amichevole Latvia - Wales e in questi giorni Riga e' stata invasa dai supporters gallesi. Ho fatto la conoscenza di alcuni di loro che oltre essere stati amichevoli si sono prestati per fare un paio di foto. I festeggiamenti si sono inoltrati la sera sia da parte dei gallesi che da parte dei lettoni nei locali della citta'. Il calcio e' un motivo come un'altro per bere in compagnia.


 Ho fatto un giro in barca sul daugava per 1 ora in una specie di grandissima zattera con sala da ballo (costo: 2 lat), non sono riuscito a fare delle belle foto perche' era notte fonda. Interessante, soltanto un po' freddo. I marinai italiani se ne sono andati e piano piano anche molti turisti se ne vanno, Riga e' meno popolata delle prime due settimane di Agosto. Meglio cosi'

