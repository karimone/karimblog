Title: Le nostre credenze
Date: 2006-08-22
Category: Lituania
Slug: le-nostre-credenze
Author: Karim N Gorjux
Summary: Non parlo di mobili, ma di convinzioni. Ieri mattina ero nello spogliatoio della palestra, il Lunedì è un giorno speciale per la palestra, generalmente c'è più gente del solito. A tutte le[...]

Non parlo di mobili, ma di convinzioni. Ieri mattina ero nello spogliatoio della palestra, il Lunedì è un giorno speciale per la palestra, generalmente c'è più gente del solito. A tutte le ore.


   
Ero di buon umore, mi stavo mettendo la mitica maglietta *Camping Demonte* grigia che mi accorgo di averla messa al contrario. Dico qualcosa tra me e me ad alta voce ed un tizio che era nello spogliatoio mi chiede "kas?" ovvero "Cosa?" (usato come "scusa che hai detto?"). Gli rispondo in lituano che non parlavo in lituano, ma in italiano on inglese. Non ha fatto una smorfia, **è stato zitto ed è andato via senza salutare.  
**  
Traete le conclusioni che volete. Il mio umile pensiero è che indipendentemente dall'essere lituano o meno, quel tizio è uno stronzo e come ben sapete di stronzi ne è pieno il mondo. L'errore di base è che se sei convinto o vuoi convincerti che i lituani sono *bla bla *puoi considerare o non considerare un episodio del genere. Mi sono fatto due risate pensando a che vita di merda può fare un tizio del genere, sono salito in sala pesi e me lo sono lasciato dietro le spalle. Era il giorno dei dorsali.  
[![CIMG3174.JPG](http://static.flickr.com/82/221437488_548c7ac8cf_m.jpg)](http://www.flickr.com/photos/kmen-org/221437488/ "Photo Sharing")

  
 technorati tags start tags: [klaipeda](http://www.technorati.com/tag/klaipeda), [lituani](http://www.technorati.com/tag/lituani), [lituania](http://www.technorati.com/tag/lituania), [lituano](http://www.technorati.com/tag/lituano), [palestra](http://www.technorati.com/tag/palestra), [italiani](http://www.technorati.com/tag/italiani)



  technorati tags end 

