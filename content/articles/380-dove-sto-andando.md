Title: Dove sto andando?
Date: 2007-11-04
Category: Lituania
Slug: dove-sto-andando
Author: Karim N Gorjux
Summary: Come vedete fatico molto ad aggiornare il blog, non ho nessuna intenzione di chiuderlo, ma la mia vena artistica è decisamente scemata da quando sono tornato dalla Lituania. Sicuramente quando sarò di[...]

Come vedete fatico molto ad aggiornare il blog, non ho nessuna intenzione di chiuderlo, ma la mia vena artistica è decisamente scemata da quando sono tornato dalla Lituania. Sicuramente quando sarò di nuovo tra le foreste incontaminate di Telsiai, mi tornerà la vena creativa, ma per il momento non riesco più a scrivere.


 Qualcuno di voi mi puà aiutare? [Henry](http://www.henryblog.net/2007/11/01/post-ordinati-grafica-rinnovata/) si è messo di buona lena a cambiare il template del blog e a correggere i vecchi articoli. Io ho più di 700 post, ci riuscirei mai?

