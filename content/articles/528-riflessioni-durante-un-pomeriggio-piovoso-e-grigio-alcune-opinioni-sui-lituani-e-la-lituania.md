Title: Riflessioni durante un pomeriggio piovoso e grigio: alcune opinioni sui lituani e la Lituania
Date: 2009-03-17
Category: Lituania
Slug: riflessioni-durante-un-pomeriggio-piovoso-e-grigio-alcune-opinioni-sui-lituani-e-la-lituania
Author: Karim N Gorjux
Summary: I ragazzi dai 20 ai 25 anni sono esattamente come i nostri ragazzi ad esclusione del piccolissimo dettaglio che qui sanno quasi tutti tre lingue: lituano, russo, inglese. Mi riservo di dire[...]

[![](http://farm1.static.flickr.com/124/328434036_417ce496c7_m.jpg "Klaipeda sotto la neve")](http://www.flickr.com/photos/i_am_neuron/328434036/)I ragazzi dai 20 ai 25 anni sono esattamente come i nostri ragazzi ad esclusione del piccolissimo dettaglio che qui sanno quasi tutti tre lingue: lituano, russo, inglese. Mi riservo di dire quasi tutti, perché al momento ho conosciuto solamente una ragazza che parlava unicamente il lituano ed il russo.


 Se facciamo un piccolo passo avanti di 10-15 anni avanti, si torna all'URSS e le persone sono totalmente differenti, la sensazione è che in Lituania esistano due tipi di lituani: il vecchio e il nuovo lituano. Mentalità diverse, storie diverse.  
  
Essere stranieri non è una bella sensazione, ma con i "nuovi lituani" è una sensazione che non mette a disagio quanto con i "vecchi lituani", anche se basta semplicemente provare a parlare la lingua locale per abbattere svariati muri che sembrano insormontabili ai più. Fortunatamente non ho fretta e Greta mi aiuta ad imparare nuove parole ogni giorno.


 Se invece parliamo del tempo, qui siamo mal messi. So che in Italia c'è un bel sole mentre qui il sole l'abbiamo visto di sfuggita un paio di giorni fa, oggi invece nevica e piove. Se si vuole maledire la Lituania, questo è uno dei tanti motivi da cui iniziare.


 In ogni parte del mondo ci sono paesi come la Lituania con persone difficili da capire per noi italiani e con usanze che non riusciamo a comprendere. Ma confrontare ogni differenza con il proprio paese è una comparazione impari, per il semplice motivo che solamente nel proprio paese non si è stranieri. Io dopo sette mesi che sono qui non mi sono mai lamentato, o meglio, non mi sono mai lamentato come facevo tempo addietro. Mi limito a vedere le cose come sono, non lamentarmi di ciò che non posso cambiare e migliorare ciò che è nelle mie possibilità.


 Un'altra cosa a cui ho rinunciato è l'etichettatrice. Evito di etichettare e suddividere ogni cosa in modo definitivo, senza la possibilità di ricredermi. Mi faccio un'idea in base alle mie esperienze e la condivido sul blog, ma essendo la verità viva, non è detto che cambi idea con il tempo e che sia la stessa verità di chi legge. Una cosa è certa, lamentarsi che piove è come lamentarsi del fatto che siamo destinati a morire, non cambia nulla.


