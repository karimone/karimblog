Title: Una mail da un ragazzo che lavorava in banca a Vilnius
Date: 2011-07-22
Category: Lituania
Slug: una-mail-da-un-ragazzo-che-lavorava-in-banca-a-vilnius
Author: Karim N Gorjux
Summary: Un ragazzo che lavorava in Lituania con lo stipendio in Euro mi ha scritto qualche email. Lui lavorava in banca a Vilnius e si occupava di finanziamenti. Non ha resistito ed è[...]

Un ragazzo che lavorava in Lituania con lo stipendio in Euro mi ha scritto qualche email. Lui lavorava in banca a Vilnius e si occupava di finanziamenti. Non ha resistito ed è scappato. Mi ha mandato questa email tanto tempo fa.


 Sinceramente non ho mai avuto grandi contatti con la banca in Lituania, ho un conto con la Swedbank che ho aperto in poco tempo ed ho usato mentre ero su a Klaipeda, è stato facilissimo aprirlo e non ho mai pensato di chiuderlo. Il sito online è semplice da usare e non ha spese.  
  

> "Pronto Gianni?"  
> "Sì chi è?"  
> " Ciao sono Enzo...scusa se ti disturbo"  
> " Dimmi"  
> " Avremmo bisogno del tuo aiuto: siamo a Vilnius. Potremmo incontrarci?"  
> "Di cosa si tratta?"  
> "Io e Giovanni siamo due ragazzi della provincia di Napoli, avremmo bisogno di un finanziamento, vorremmo vendere i chebureki sulla spiaggia di Palanga"  
> "....tu tu tu tu tu tu"  
>    
> Dei due volenterosi ragazzi non ne ho avuto più notizia ma sembra che abbiamo trovato il Garante. Dei chebureki non so: dicono siano buoni ma io non mangio la carne.  
> Non è il solo aneddoto, la lista sarebbe lunga. Per non parlare del contadino di Tauragè, me lo ricordo ancora. Era venuto in gran pompa: voleva fondare una società di Trading con sede nel centro di Vilnius. So che è rimasta aperta giusto il tempo per fare arieggiare i locali dopo l'imbiancatura. Che simpatica era la signora Ausra: voleva ammodernare la sua azienda agricola vicino Elektrenai. Peccato che i 800.000 litas usati per comprare trattori e ringiovanire le mandrie non le sono bastati per pagare la prima bolletta della luce.


 E le esporpriazioni: ah sì un vero business. C'era un agenzia immobiliare che comprava terreni nelle vicinanze di opere pubbliche oggetto di futura espropriazione: si vede che avevano una clientela a cui piaceva addormentarsi cullata dalla melodia della A2.   
   
Karim, tu hai parlato degli stipendi delle commesse di Akropolis. Ma come ti permetti??? E la par condicio dove la metti? Dovresti aggiungere i benefits degli executives di Snoras Bankas, Ab Seb, Swedbank etc...non ci vuole molto, basta aggiungere qualche 0 finale e i loro nomi!!!  
   
Una scrittrice francese, di cui ora mi sfugge il nome, scriveva in un libro: "*Il denaro è un ottimo valletto e un pessimo maestro*" .  
Credo che i lituani non l'abbiano letto!

