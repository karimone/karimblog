Title: A volte mi verrebbe voglia di distruggere il blog
Date: 2006-10-27
Category: Lituania
Slug: a-volte-mi-verrebbe-voglia-di-distruggere-il-blog
Author: Karim N Gorjux
Summary: Non sono uno di quei blogger famosi in tutta Italia, ma dato che ho vissuto in Lituania per quasi due anni ho un certo numero di visitatori. (Mediamente arrivo a 15.000 visite[...]

Non sono uno di quei blogger famosi in tutta Italia, ma dato che ho vissuto in Lituania per quasi due anni ho un certo numero di visitatori. (Mediamente arrivo a 15.000 visite al mese).  
Prima di vivere in Lituania, ho fatto una manciata di viaggi a Riga da turista e l'attaccamento per quella città è lo stesso che può avere un qualsiasi ragazzo italiano per i paesi baltici: **non volevo più tornare a casa.**  
  
Da turista il mondo è più bello, potersi alzare la mattina solo per passeggiare per le vie del centro di una bella città ha il suo fascino. Le donne lituane come quelle lettoni sono incantevoli ed il loro modo di fare ci lascia praticamente stregati.


 La mia visione dei paesi baltici è cambiata gradualmente quando ho iniziato a viverci, la mia situazione è però sempre stata particolare, lavorando qualche mese in Italia potevo stare tranquillamente a Klaipeda nella Krushiov's house senza troppe pretese. Non avevo la macchina e il vitto e alloggio costavano poco, me la cavavo egregiamente senza pretendere troppo. Tutti i miei post riguardo la Lituania non sono scritti principalmente per sfogarmi, ma la forma dell'articolo tendeva ad essere del genere **cronaca + commento** a differenza dei quotidiani che dovrebbero essere solo cronaca. Mi succedeva qualcosa e io lo scrivevo sul blog commentandolo in base al mio parere.


 Purtroppo però, la mia esperienza è molto diversa da chi ha una ragazza in lituania e vive in Italia. Se sei innamorato di una lituana, ma vivi in Italia, tendi a difendere la Lituania pensando che se parlo negativamente del suo paese parlo negativamente anche della donna stessa. Ma dico io, una donna lituana me la sono sposata, ha tanti difetti, ma anche tanti pregi e pure io non sono perfetto, ma quando scrivevo del paese, della gente, delle strutture della Lituania enunciandone sia i difetti che i pregi **ho tirato le mie somme e ho deciso di vivere in Italia** affrontando l'avventura di trasferirci tutta la famiglia. L'Italia, per quanto male se ne possa parlare, **è un paese dove si vive ancora bene** e nel mio caso ha molto più da offrirmi che la Lituania.


 Sono ovviamente esperienze personali, ma conoscendo altri italiani che vivono in Lituania sia di persona che virtualmente possò dire che italiani che vivono bene in Lituania sono pochi, molto pochi, anzi pochissimi. Al momento **ne ho conosciuti due** di cui sono certo che vivono bene in Lituania, uno di questi poi è quasi nazionalista quanto un lituano, ma è giusto così, non significa che io dica delle cose non vere e lui si, ma significa solamente che a lui piace la Lituania e che per molti degli aspetti negativi che vedevo io, lui ha una chiave di lettura meno negativa se non positiva. (e sinceramente per questo lo invidio pure)

 Al di là delle donne che sono bellissime, dolci, affettuose e alle volte pure simpatiche, la Lituania è un paese che consiglio vivamente come meta turistica, ma poco per una svolta alla propria vita. Questo solo per il momento, per il futuro non posso garantire.


