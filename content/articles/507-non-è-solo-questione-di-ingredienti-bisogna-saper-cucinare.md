Title: Non è solo questione di ingredienti, bisogna saper cucinare!
Date: 2008-11-25
Category: Lituania
Slug: non-è-solo-questione-di-ingredienti-bisogna-saper-cucinare
Author: Karim N Gorjux
Summary: Lezione di lituano. Argomento: cibi e bevande.

Lezione di lituano. Argomento: cibi e bevande.


 Ieri sera eravamo solo in quattro alla lezione di lituano. Io, Fernando (ragazzo spagnolo di cui vi parlerò in futuro) e due signore bielorusse. Non è stato facile tradurre ogni cibo o bevanda di cui abbiamo parlato, alcune spezie o verdure non le ho ancora tradotte e altre che sono molto comuni in Italia qui non si trovano.


  Sapete cosa non si trova e che è veramente utile? La panna da cucina, qui usano la panna acida (grietine), ed è possibile acquistarla a varie percentuali di grasso, ma l'equivalente della nostra panna da cucina non esiste e tanti piatti italiani che cucino ne avrebbero davvero bisogno.


 Durante la lezione si facevano domande sugli ingredienti: ti piace, non ti piace, conosci o non conosci, e, parlando con l'insegnante, io e Fernando abbiamo scoperto che tante cose non piacciono per motivi assolutamente banali (per noi). Il tacchino non mi piace perché è troppo "duro", il riso non sa di niente, le zucchine non sono buone. In realtà qui in Lituania la cucina è molto semplice, oltre i piatti tipici locali con base di patate e salse grasse, esiste il classico piatto riso bollito, verdure grigliate, bistecca "carbonata" e patate. Esiste qualche altra variante, ma niente di più e quindi si perdono tantissimo dalla cucina. Noi abbiamo una cultura sul mangiare, esattamente come gli spagnoli, mentre in Lituania si mangia quando si pare e si mangia solo per vivere non per il piacere. Prendiamo ad esempio il riso, nessuno qui lo cucinerebbe in un modo diverso di quello bollito, ovvio che alla lunga il riso fa schifo e non ne vuoi più sapere.


 Saltiamo anche il discorso ketchup e maionese sulla pizza perché non voglio spenderci più tempo del necessario è una questione di abitudine che trovo stupido commentare. Loro fanno così e se gli piace, che continuino. Una nota sulla carne. Le migliori bistecche della mia vita le ho mangiate qui, e se lo dico io che arrivo da Cuneo potete credermi, qui la carne è favolosa, persino migliore della [Coalvi](http://www.coalvi.it/ "Coalvi").


 Non sono un grande cuoco e quindi la mia opinione è assolutamente discutibile, ma ho sempre pensato che se qualcuno aprisse una scuola di cucina, invece che del solito ristorante, farebbe una discreta fortuna nell'esportare la nostra cultura culinaria.


