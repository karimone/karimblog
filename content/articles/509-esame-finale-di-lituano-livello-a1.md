Title: Esame finale di lituano: livello A1
Date: 2008-12-12
Category: Lituania
Slug: esame-finale-di-lituano-livello-a1
Author: Karim N Gorjux
Summary: Ieri sera abbiamo dato l'esame finale di lituano. Con un bel 9/10 mi porto a casa il certificato che attesta che ho un livello base, ma molto base di padronanza del lituano.A[...]

Ieri sera abbiamo dato l'esame finale di lituano. Con un bel 9/10 mi porto a casa il certificato che attesta che ho un livello base, ma molto base di padronanza del lituano.  
A Gennaio si parte con il livello "avanzato", andiamo avanti... piano piano, tanto io non ho fretta! :-)

