Title: Ventidue...
Date: 2007-04-15
Category: altro
Slug: ventidue
Author: Karim N Gorjux
Summary: Il ventidue finalmente arriveranno le mie belle, intanto mi godo le foto fatte da Rita in Lituania. (Al costo di 1/10 di quelle italiane).

Il ventidue finalmente arriveranno le mie belle, intanto mi godo le foto fatte da Rita in Lituania. (Al costo di 1/10 di quelle italiane).  
  
[![Le mie belle](http://farm1.static.flickr.com/246/460607160_357de8b260.jpg)](http://www.flickr.com/photos/kmen-org/460607160/ "Photo Sharing")  


 Mi sento fortunato. Molto fortunato. Ma un mio caro amico continua a dire che la **fortuna non esiste**, in fin dei conti non ha tutti i torti, la famiglia che ho creato non è caduta dal cielo, è qualcosa che ho voluto ed è quindi la conseguenza di una mia scelta che sono contento di aver fatto.


 Avrei potuto fare scelte diverse in passato, più canoniche: laurearmi, lavorare da impiegato a Torino in qualche azienda dell'IT, passare i sabati sera in discoteca. Su certi versi pensavo di aver sbagliato, ma poi una volta che mi sono sposato, una volta che ho preso la mia bimba in braccio ho pensato: **la vita inizia solo adesso.**

