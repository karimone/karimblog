Title: Il passato scolpito nel marmo
Date: 2004-12-10
Category: altro
Slug: il-passato-scolpito-nel-marmo
Author: Karim N Gorjux
Summary: Herbert George Wells e' il padre della fantascienza, per chi non lo conoscesse ha scritto dei veri capisaldi della letteratura: "l'uomo invisibile" e "la macchina del tempo" sono tra i piu' famosi.[...]

Herbert George Wells e' il padre della fantascienza, per chi non lo conoscesse ha scritto dei veri capisaldi della letteratura: "l'uomo invisibile" e "la macchina del tempo" sono tra i piu' famosi. Sono libri affascinanti, ben scritti e soprattutto hanno dato vita a varie trasposizioni cinematografiche. 

 La macchina del tempo ha visto una sua prima trasposizione cinematrografica nel 1960; fedelissimo in tutto e per tutto al libro, "la macchina del tempo" e' un film da vedere. Mi e' capitato di vederlo tanti anni fa', quasi per caso, quindi penso che non sia di facile reperibilita'.   
Un remake moderno di questo bellissimo film ha visto la luce nel 2002, rispetto al libro sono presenti degli spunti particolarmente moderni e interessanti che non hanno sconvolto la trama originale ideata da Wells. Di questi spunti, uno in particolare mi aveva colpito, ed e' proprio di questo che vorrei scrivere alcune mie riflessioni.


 Il protagonista, un giovane e brillante scienziato, perde in un tragico incidente la sua amata fidanzata. Essendo lui uno scienziato e spinto da una tragica sofferenza, decide di costruire una macchina del tempo per poter tornare nel passato e salvare la sua ragazza. Dopo qualche anno di intenso lavoro la macchina del tempo e' terminata, lo scienziato torna indietro nel tempo e salva la fidanzata, ma purtroppo, subito dopo averla salvata muore a causa di un'altra fatalita'. In pratica, se lo scienziato tornasse indietro nel tempo 100 volte per salvare la fidanzata, lei morirebbe altre 100 volte.


 Perche' lo scienziato non riesce a salvare la sua fidanzata? Forse perche' il passato non si puo' cambiare, ma in realta' la risposta giusta la otteniamo ponendoci la domanda giusta: perche' lo scienziato ha costruito la macchina del tempo? L'ha costruita perche' la ragazza e' morta, quindi se lei non muore lui non costruisce la macchina del tempo e quindi non puo' viaggiare a ritroso nel tempo.


 Sembra un paradosso (e lo e'), ma metaforicamente parlando noto che ognuno di noi tenta di costruire macchine del tempo. Ognuno di noi vorrebbe tornare nel passato per modificare le proprie scelte, dimenticandole o nascondendole a causa del dolore e dell'infelicita' che ci hanno causato. Eppure noi siamo il prodotto delle nostre esperienze e come scrisse Oscar Wilde: "l'esperienza e' il nome che gli uomini danno ai propri errori" quindi rinnegare i nostri errori, rinunciare alla nostra esperienza e sognare un'utopica vita fatta di scelte sempre azzeccate equivale a rinnegare cio' che siamo.


 In conclusione, l'unico modo per cambiare il passato e' giudicarlo per cio' che ci ha donato, ovvero, la possibilita' di costruirci un futuro migliore.  
Lo scienziato, dopo aver compreso di non poter cambiare il passato, viaggia alla scoperta del futuro, l'unico tempo che poteva cambiare.


