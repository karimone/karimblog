Title: Citazione su "Diario"
Date: 2006-08-27
Category: altro
Slug: citazione-su-diario
Author: Karim N Gorjux
Summary: Sono stato citato su diario di questa settimana, più precisamente a pagina 22. Su flickr potete vedere direttamente una scansione della pagina in questione (vi consiglio di cliccare sul pulsante all sizes).

Sono stato citato su [diario](http://www.diario.it) di questa settimana, più precisamente a pagina 22. Su flickr potete vedere direttamente una scansione della pagina in questione (vi consiglio di cliccare sul pulsante **all sizes**).


  
Il mio nome è sotto i piedi dell'uomo nella vignetta, esattamente dove viene segnalato nella nota.  
  
[![Diario pag 22](http://static.flickr.com/85/225900529_a267117ccd_m.jpg)](http://www.flickr.com/photos/kmen-org/225900529/ "Photo Sharing")  
  
  
Grazie anonimo segnalatore!  
 technorati tags start tags: [blog](http://www.technorati.com/tag/blog), [diario](http://www.technorati.com/tag/diario), [giornale](http://www.technorati.com/tag/giornale)



  technorati tags end 

