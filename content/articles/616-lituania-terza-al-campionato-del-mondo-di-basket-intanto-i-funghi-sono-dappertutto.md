Title: Lituania terza al campionato del mondo di basket, intanto i funghi sono dappertutto
Date: 2010-09-13
Category: Lituania
Slug: lituania-terza-al-campionato-del-mondo-di-basket-intanto-i-funghi-sono-dappertutto
Author: Karim N Gorjux
Summary: In questi giorni si è svolto in Turchia il campionato mondiale maschile di basket, le macchine erano (e lo sono ancora) addobbate con bandierine lituane con la scritta "vinceremo!".

[![Monumento Basket a Vilnius](http://www.karimblog.net/wp-content/uploads/2010/09/Monumento-Basket-197x300.jpg "Monumento-Basket")](http://www.karimblog.net/wp-content/uploads/2010/09/Monumento-Basket.jpg)In questi giorni si è svolto in Turchia il [campionato mondiale maschile di basket](http://turkey2010.fiba.com/eng), le macchine erano (e lo sono ancora) addobbate con bandierine lituane con la scritta "vinceremo!".


 Così è stato, la Lituania ha vinto tanto, anzi tantissimo perdendo solo con i soliti USA che si sono aggiudicati la finale contro i padroni di casa per 81-64. La Lituania si è piazzata terza battendo la Serbia 99-84 nella finale per il terzo posto quarto posto raggiungendo un traguardo storico per una nazionale che rappresenta un paese di 3 milioni e mezzo di abitanti.  
  
I Lituani amano il basket quanto noi italiani amiamo il calcio e sono entusiasti ed orgogliosi della loro nazionale che seguono, amano e odiano esattamente come noi con la nostra nazionale di calcio. Il bello è che la loro nazionale di basket, pur essendo il paese piccolo e con lo stesso numero di abitanti della provincia di Milano, vanta di essere una delle nazionali di basket **più forti e rinomate al mondo**. La cosa è talmente sentita che a Vilnius, vicino al palazzetto dello sport, è possibile vedere un monumento dedicato allo sport nazionale. (lo puoi ammirare nella foto di questo articolo)

 Tutto questo succede in attesa degli [Europei di basket del 2011](http://en.wikipedia.org/wiki/EuroBasket_2011) che si terranno proprio qui in Lituania e dove ovviamente la Lituania stessa è tra le favorite per la vittoria.


 Intanto il tempo impazza tra sprazzi di giornate estive e autunnali, alternando la pioggia e il sole e **portando la crescita dei funghi a livelli record**. Le foreste sono piene di gente che cerca funghi, la mia vicina di casa ottantenne che fatica a fare due piani di scale, è tornata con un sacco dell'immondizia piena di funghi presi chissà dove; mio suocero segue la scia cercando i funghi nei posti che conosce lui e trovano nuovamente altri funghi tornandoci dopo un paio di giorni. **Avremo funghi per tutto l'inverno** e la cosa davvero non mi dispiace, anzi riempie la casa del profumo delle montagne Cuneesi e questo non può far altro che farmi piacere.


