Title: Mercatino delle pulci di Savigliano
Date: 2007-05-26
Category: altro
Slug: mercatino-delle-pulci-di-savigliano
Author: Karim N Gorjux
Summary: Domani mattina sarò presente al mercatino delle pulci di savigliano in compagnia di Davide. Tra me e lui avremo a disposizione per i clienti centinaia di fumetti in condizioni ottime dai Bonelli[...]

Domani mattina sarò presente al mercatino delle pulci di savigliano in compagnia di Davide. Tra me e lui avremo a disposizione per i clienti centinaia di fumetti in condizioni ottime dai [Bonelli](http://www.sergiobonellieditore.it) ai Manga, vari libri, cianfrusaglie, palmari etc. etc. Metterò in vendita anche la mia collezione musicale per un totale circa di 250 CD.


 Che dire? Passate a trovarci, siamo di fianco al municipio. :smile\_tb:

