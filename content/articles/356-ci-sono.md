Title: Ci sono!
Date: 2007-08-18
Category: altro
Slug: ci-sono
Author: Karim N Gorjux
Summary: In questi giorni sono ospiti a casa mia i suoceri lituani. Il tempo è quello che è, quindi non ho idea di quanto scriverò in questo periodo. Devo dire che è veramente[...]

In questi giorni sono ospiti a casa mia i **suoceri lituani**. Il tempo è quello che è, quindi non ho idea di quanto scriverò in questo periodo. Devo dire che è veramente un peccato avere così poco tempo perché ho alcune cose interessanti da scrivere e tra tutte queste mirabolanti vicende che continuamente mi capitano, ho da raccontarvi un incontro che ho fatto **grazie al mio blog**. Un incontro che non dimenticherò molto facilmente.


 Una cosa ho notato: le persone che incontro tramite il blog **mi conoscono** di più di quanto io conosca loro. Mi è già capitato di ricevere una **fiducia incondizionata** che nasce solamente da ciò che scrivo e dall'idea che il lettore si fa di me. E' interessante, ma allo stesso tempo preoccupante, c'è da fidarsi? Parlo degli altri nei miei confronti, ovviamente.


