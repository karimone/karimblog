Title: E anche i giovani lituani sono capaci di stupire: il successo della canzone Lady Gaga So So Bad
Date: 2010-06-18
Category: Lituania
Slug: e-anche-i-giovani-lituani-sono-capaci-di-stupire-il-successo-della-canzone-lady-gaga-so-so-bad
Author: Karim N Gorjux
Summary: Un giovane ragazzo lituano rimane colpito, durante il suo viaggio negli Stati Uniti, come gli americani siano letteralmente pazzi per Lady Gaga e tutto ciò che ruota attorno al suo personaggio.

[![](http://www.karimblog.net/wp-content/uploads/2010/06/lady-gaga-so-so-bad.jpg "lady gaga so so bad")](http://www.karimblog.net/wp-content/uploads/2010/06/lady-gaga-so-so-bad.jpg)Un giovane ragazzo lituano rimane colpito, durante il suo viaggio negli Stati Uniti, come gli americani siano letteralmente pazzi per Lady Gaga e tutto ciò che ruota attorno al suo personaggio.


 Tornato in Lituania, il mio "quasi compaesano" lituano ha la brillante idea di divertirsi creando una canzone ed un video su Lady Gaga **scherzando sul personaggio in modo spinto e provocante**. Il risultato è una canzone molto orecchiabile ed un video a dir poco stupendo creato con appena 100 litas, ovvero 29€, che secondo la mia modesta opinione sono stati spesi solamente per comprare da bere e da mangiare.  
  
**Da uno scherzo, come spesso accade su internet, è nato inaspettatamente il successo.** Il video è tra i più visti di Youtube e la canzone, [in vendita su iTunes](http://itunes.apple.com/it/album/the-lady-gaga-song/id376448523) e amazon mp3 è così cliccata che i ragazzi che l'hanno creata sono stati contattati dalla [Interscope records](http://www.interscope.com/) per accordarsi sulla distribuzione della canzone negli Stati Uniti.


 La canzone ha già un [sito ufficiale](http://www.thegagasong.com/), un profilo [twitter](http://twitter.com/TheGagaSong), la fan page su [facebook](http://www.facebook.com/pages/So-So-Bad-The-Lady-Gaga-Song-creators/130597083632335?ref=ts) e un bel video su [youtube](http://www.youtube.com/watch?v=0AC0uLvxCyY).


 Io posso solo garantirvi che **il video mostra in modo molto genuino la campagna lituana**, le case, ma soprattutto le feste che vengono organizzate dai lituani a base di birra, vodka e carne cotta alla brace.


 PS: In campagna, il cesso in mezzo al giardino è dura realtà...


