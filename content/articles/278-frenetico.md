Title: Frenetico
Date: 2006-12-23
Category: altro
Slug: frenetico
Author: Karim N Gorjux
Summary: In Lituania mi spaccavo altamente le balle, qui non ho il tempo nemmeno di sbadigliare, soprattutto ora che siamo in tre. Greta è piccola e richiedere un'attenzione particolare, continua e costante. Rita[...]

In Lituania mi spaccavo altamente le balle, qui non ho il tempo nemmeno di sbadigliare, soprattutto ora che siamo in tre. Greta è piccola e richiedere un'attenzione particolare, continua e costante. Rita non è nel suo paese, ma per fortuna se la cava più di quanto me la cavavo io in Lituania i primi tempi.


 All'appello manca il lavoro, ma anche quello lo sto portando avanti. Ho scelto di mettere in piedi una società di servizi informatici e purtroppo, ma è giusto così, **i primi anni sono quelli più duri**. Bisogna investire dei soldi, del tempo e non è sempre detto che si riesca ad avere dei grossi risultati, sono giovane e se non lo faccio ora non lo faccio mai più. A tal proposito ho letto un [interessante articolo](http://www.lastampa.it/_web/cmstp/tmplRubriche/editoriali/grubrica.asp?ID_blog=41&ID_articolo=205&ID_sezione=56&sezione=) sulla stampa qualche giorno addietro, la vita del dipendente in Italia è diventata ardua, molto ardua. Siamo sicuri che mettere in piedi un'impresa sia veramente **la scelta più coraggiosa**?

