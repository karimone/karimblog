Title: Un lettore mi scrive un messaggio su facebook
Date: 2014-05-13
Category: altro
Slug: un-lettore-mi-scrive-un-messaggio-su-facebook
Author: Karim N Gorjux
Summary: Ricevo un messaggio su Facebook nella sezione "altro" quindi come al solito l'ho visto molto in ritardo. Purtroppo l'account non esiste più e non ho potuto rispondere.

Ricevo un messaggio su Facebook nella sezione "altro" quindi come al solito l'ho visto molto in ritardo. Purtroppo l'account non esiste più e non ho potuto rispondere.


 Il messaggio:

 
> Flavio: le lituane non sono strane come scrivi nel tuo blog......  
> anche io vivo in lituania sai.... e poi senza offesa... il tuo nome e cognome non mi sembrano affatto italiani

 Caro Flavio che le lituane che incontri tu, non siano le stesse che ho descritto nel blog è più che plausibile. Come ho già scritto più volte in passato **le mie esperienze sono soggettive**. I miei articoli non sono universali regole scritte sulla tavole della legge e consegnate a me dal divino mentre scendevo dalla montagna del sapone. Quindi qui abbiamo tutti ragione o tutti torto o se ci scrolliamo di dosso il contorto e limitato giudizio torto-ragione abbiamo tutti delle esperienze di vita uniche ed insindacabili.


 Riguardo invece al mio nome è cognome, esse non sono altro che due parole. Come puoi notare ho una certa padronanza dell'italiano dovuta al fatto che sono nato in Italia e cresciuto in Italia, non sono un professore, ma non faccio nemmeno così schifo nell'esprimermi. Non sono i nomi e cognomi che fanno l'italianità.


 Un caro saluto.


