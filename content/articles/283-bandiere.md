Title: Bandiere
Date: 2007-01-02
Category: altro
Slug: bandiere
Author: Karim N Gorjux
Summary: Ma la vuoi la cittadinanza italiana?!No, altrimenti perdo quella lituana.Beh, allora ci vai tu a fare la fila per avere il permesso di soggiorno.Va bene, ci vado io.Ho capito... mi toccherà fare[...]

*Ma la vuoi la cittadinanza italiana?!  
  
No, altrimenti perdo quella lituana.  
  
Beh, allora ci vai tu a fare la fila per avere il permesso di soggiorno.  
  
Va bene, ci vado io.  
  
Ho capito... mi toccherà fare la fila con te... che palle!  
*

 Non capisco perché la Lituania non accetta la doppia cittadinanza, entrano in Europa (da ieri siamo 27!), accedono ai fondi europei stanziati per le zone baltiche, emigrano come non mai e poi... niente **doppia cittadinanza**! Sinceramente questo nazionalismo estremo proprio non lo capisco, a cosa serve? Greta ha la doppia cittadinanza, semplicemente perché essendo mia figlia la ottiene per diritto **iuri sanguinis** e quindi nessuno può toglierle la sua cittadinanza italiana.


 Questa mattina ero all'asl di Cuneo, ho registrato Greta per 3 mesi in attesa di certificato di residenza definitivo, ho fatto una bella autocertificazione e per ora è tutto ok. Per Rita sarà ben diverso se non vuole la cittadinanza italiana... mah! Staremo a vedere. Intanto mi godo il sorrisino del mio amore.  
  
[![CIMG4483](http://farm1.static.flickr.com/154/342388844_583243f064_m.jpg)](http://www.flickr.com/photos/kmen-org/342388844/ "Photo Sharing")  


