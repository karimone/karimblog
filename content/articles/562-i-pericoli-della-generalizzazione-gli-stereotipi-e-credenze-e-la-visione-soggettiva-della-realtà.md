Title: I pericoli della generalizzazione: gli stereotipi e credenze e la visione soggettiva della realtà
Date: 2009-09-02
Category: Lituania
Slug: i-pericoli-della-generalizzazione-gli-stereotipi-e-credenze-e-la-visione-soggettiva-della-realtà
Author: Karim N Gorjux
Summary: E normale generalizzare. Anzi è più che normale, è utile per il sano equilibrio psichico della nostra mente; se il nostro cervello non generalizzasse rischierebbe di impazzire nel giro di 15 giorni.

[![Generalizzare](http://www.karimblog.net/wp-content/uploads/2009/09/generalization-300x279.jpg "Generalizzare")](http://www.karimblog.net/wp-content/uploads/2009/09/generalization.jpg)E normale generalizzare. Anzi è più che normale, è utile per il sano equilibrio psichico della nostra mente; se il nostro cervello non generalizzasse rischierebbe di impazzire nel giro di 15 giorni.


 Se ti trovi davanti una porta sai come si apre, dove si mette la chiave e in pratica sei in grado di usare una porta anche se è la prima volta che la vedi. Il cervello generalizza e comprende le similitudini di porte anche molto diverse tra loro; prova ad immaginare se ad ogni porta dovessimo cercare di comprendere come funziona perché è leggermente diversa dalla porta di casa nostra. Semplicemente folle!  
  
La generalizzazione aiuta a comprendere il mondo che ci circonda, ma purtroppo ognuno di noi ha **una visione soggettiva di un'unica realtà**. Le persone che vanno dai sensitivi sono predisposti a credere a tutto quello che gli si dice e non fanno mai attenzione alle affermazioni sbagliate anzi, non se le ricordano nemmeno. Quando non c'erano i cellulari e la gente si chiamava a casa sui telefoni fissi succedeva ogni tanto che qualcuno rispondeva dicendo: *"ma guarda il caso, stavo giusto pensando a te!"*. Se hai vissuto quel periodo forse sarà capitato anche a te di avere queste tipiche premonizioni telefoniche che non hanno niente di straordinario a meno che non ci si dimentichi di tutte le volte che si è risposto al telefono senza indovinare chi ha chiamato.


 Gli esempi sono davvero tanti, ma alla base di tutto c'è il focus. Quando ci si innamora di una donna, ogni difetto viene sminuito o persino visto come un pregio, le altre donne non esistono ed esiste sempre solo lei. Molte persone che sono venute qui in Lituania dicono che **le lituane sono tutte belle**, ma non è affatto vero! Vuoi mettere una ragazza bionda, alta, ben vestita con tutte le curve al posto giusto al confronto con una bassa, bruttina che sembra mangiare solo cepelinai? Le donne belle in Lituania sono tante e più delle brutte, ma il fatto che queste siano di meno oltre a non essere degne di note, fa si che non esistano proprio. Prova tu stesso, quando sarai da queste parti, fatti un giro in paese e per 1 ora cerca di notare le ragazze che secondo te sono brutte.


 La generalizzazione ha anche dei risvolti negativi. Se ti dico albanese, rumeno o marocchino, tu cosa pensi? Puoi essere razzista o meno, ma se non sei ipocrita, devi ammettere che dietro queste tre cittadinanze è legato uno stereotipo di persona poco per bene. Eppure [John Belushi](http://en.wikipedia.org/wiki/John_Belushi "Pagina di John Belushi su Wikipedia") era albanese, gli [Enigma](http://en.wikipedia.org/wiki/Enigma_%28musical_project%29 "Gli Enigma su Wikipedia") sono romeni e ovviamente anche il Marocco sia in passato che attualmente ha dato i natali a persone di spicco che purtroppo non so citare. Non esistono dottori, scienzati, avvocati o ingegneri albanesi? Certo che si! 

 Allo stesso modo i lituani possono essere generalizzati in base alle proprie esperienze come è stato scritto nei [commenti](http://www.karimblog.net/2009/08/24/impressioni-e-riflessioni-del-mio-primo-anno-in-lituania-senza-mai-tornare-nel-bel-paese/#IDComment31821602) del mio articolo sul mio [primo anno in Lituania](http://www.karimblog.net/2009/08/24/impressioni-e-riflessioni-del-mio-primo-anno-in-lituania-senza-mai-tornare-nel-bel-paese/), ma per ogni generalizzazione che ho sentito, io ho sempre trovato un'eccezione. Esistono donne e uomini pericolosi, ma ho anche trovato donne e uomini gentili, educati e rispettosi. Esistono persone egoiste come ho trovato persone altruiste. Sono stato a compleanni, matrimoni, feste di compleanno, pranzi di Pasqua e di Natale, ho anche visto tanti bambini di tutte le età e in tutto ciò io ho notato che i lituani sono semplicemente umani.


 Evito di generalizzare e non ho **nessuna pretesa** dai lituani dato che non sono in affari con loro. Se esco per un appuntamento, mi preparo sempre un piano B nel caso in cui venga "bidonato": mi porto un libro, un diario per ordinare alcuni appunti, mi faccio una passeggiata... Non ho nemmeno pretese con le "amicizie" e filtro molto le persone da frequentare.


 Quanti amici sono davvero amici? In Italia io ho un sacco di amici, ma quanti sento frequentemente? Quanti mi chiamano o chiamo? Ben pochi! E perché qui in Lituania dovrebbe essere differente?

 Concludo il discorso sulla generalizzazione sottolineando un punto molto importante. **Generalizzare equivale ad etichettare** le persone e i luoghi, dato che il tutto nasce da una valutazione soggettiva, bisogna essere sempre prontia rimuovere l'etichetta per metterne una nuova. Se poi si evita di mettere etichette si risparmia la fatica di doverle cambiare.


