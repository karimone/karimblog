Title: Cogito Ergo sum
Date: 2005-02-05
Category: altro
Slug: cogito-ergo-sum
Author: Karim N Gorjux
Summary: Cogito Ergo Sum ovvero, cosa spinge una persona a scrivere? Che cos'è un blog? A volte mi viene chiesto perchè scrivo in un sito, perchè spendo il mio tempo in internet a[...]

[Cogito Ergo Sum](http://en.wikipedia.org/wiki/Cogito_ergo_sum) ovvero, cosa spinge una persona a scrivere? Che cos'è un blog? A volte mi viene chiesto perchè scrivo in un sito, perchè spendo il mio tempo in internet a battere meccanicamente come un drogato i tasti sul mio [portatile](http://www.apple.com/it/ibook). A mio giudizio internet è una delle più importanti invenzioni dell'uomo perchè permette di essere liberi, nel bene e nel male rende il nostro mondo piccolo come una palla da basket, la maggior parte delle persone che conosco e che usa internet non ne sfrutta nemmeno il 10% del potenziale e non si rende conto di che mezzo ha a disposizione. In rete abbiamo accesso a praticamente tutto lo scibile umano, possiamo contattare un mucchio di persone in ogni angolo del globo, parlare di qualsiasi argomento senza pregiudizi, senza discriminazioni di razza o religione.


 Troppo spesso sento dire che Internet uccide la vita sociale, mi spiace che la grande rete, la madre di tutte le reti sia tirata in causa tutte le volte che c'è qualcosa che non và. I mass media usano collegare internet alla pedofilia o al terrorismo come se eliminando la rete si chiudesse il [vaso di Pandora](http://www.vasodipandora.it/pandora.html). Mi dispiace contraddirli, ma non è così, avrei mai potuto scrivere liberamente senza internet? Avrei mai potuto conoscere così tante persone senza internet? Anche se non ho lo stesso numero di visitatori, ho la stessa importanza del sito della [NASA](http://www.nasa.gov/) e posso dire quello che voglio, posso esprimere ogni mio pensiero senza dover sbattere il muso contro l'indifferenza. Internet è libertà, internet è conoscenza perchè solo nella conoscenza vi è la libertà.


