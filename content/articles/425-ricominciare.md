Title: Ricominciare
Date: 2008-03-01
Category: altro
Slug: ricominciare
Author: Karim N Gorjux
Summary: Il 14 Marzo finalmente chiuderò questa sfortunata parentesi burocratica della mia società. Sono partito male, mal consigliato, senza esperienza e con tanta confusione. Ora posso ripartire facendo tesoro dei miei errori.Cosa più[...]

Il 14 Marzo finalmente **chiuderò questa sfortunata parentesi** burocratica della mia società. Sono partito male, mal consigliato, senza esperienza e con tanta confusione. Ora posso ripartire facendo tesoro dei miei errori.  
Cosa più mi è dispiaciuto in questa triste avventura e che** tutta l'energia** che in un paese normale bisognerebbe spendere sul lavoro, l'ho spesa per stare dietro alla burocrazia o per ritagliarmi il mio spazio in casa. Lavorare in casa, se non si ha lo spazio, è veramente difficile. Se poi hai una figlia ed una moglie giovane, lo è ancora di più.


 Sono in modalità **mumble mumble** (il rumore che fanno i personaggi quando sono assorti, quando riflettono)

