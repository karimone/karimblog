Title: Berlusconi e berlusconiani: perché? Aiutatemi a capire
Date: 2008-11-08
Category: Lituania
Slug: berlusconi-e-berlusconiani-perché-aiutatemi-a-capire
Author: Karim N Gorjux
Summary: Mi chiedo il perché molte persone seguano berlusconi, lo difendano e trovino sempre una scusa per tutte le critiche che gli vengono rivolte. Ieri sera discutevo via email con un mio amico[...]

Mi chiedo il perché molte persone seguano berlusconi, lo difendano e trovino sempre una scusa per tutte le critiche che gli vengono rivolte. Ieri sera discutevo via email con un mio amico pro berlusconi ed elettore di berlusconi. Io avrei tante domande da porre a lui e a chiunque sostenitore dell'attuale presidente del consiglio che personalmente non sopporto, ma sono sempre disposto a cambiare idea.


 Prima di porre le domande voglio dettare un paio di regole:  
  
2. Non valgono le risposte comparative. Non si menziona la sinistra, prodi e si giustifica qualsiasi cosa che fa o abbia fatto berlusconi paragonandolo alla sinistra e tornando sull'argomento trito e ritrito del destra e sinistra
  
4. Non si risponde con una domanda
  
6. Si risponde e basta, niente giri di parole
  
8. "Se tu fossi al suo posto faresti la stessa cosa". Nei manuali di medicina questa risposta è sintomo della sindrome di Alzeihmer, quindi usatela solo con cognizione di causa.

  
  
  
Premessa:  
L'informazione in Italia è morta. I telegiornali sono una farsa ed umiliano l'intelligenza di chi guarda e l'assuefazione che dilaga in Italia alla corruzione, alle leggi ad personam, alla violenza e soprattutto all'omertà hanno ucciso completamente l'opinione pubblica. Perché dico omertà? Secondo voi i telegiornali che non danno le notizie cosa fanno?

 Ecco una lista di domande e di varie cazzate che mi vengono in testa:  
  
2. Obama ha 47 anni ed è al senato da 3. E' stato eletto presidente degli Stati Uniti dopo svariati confronti televisivi con il suo diretto concorrente alla casa bianca McCain. Il nostro Mr. B. che tutto il mondo ci invidia ha 72 anni, è a Roma dal 1994 ed è stato eletto senza nemmeno confrontarsi 1 minuto con Veltroni. Secondo voi, è un esempio di democrazia?
  
  
  
6. Mr B. si permette di farci fare delle figure di merda dalla Nuova Zelanda alla Lituania. Mr B. dice che è un comico incompreso e che il suo humor non viene capito. Io invece dico che a 72 anni si può permettere un ospizio con i fiocchi. Un presidente di un paese come l'Italia a cui viene chiesto di esprimere un commento sul neo eletto presidente degli Stati Uniti non può rispondere con "bello, simpatico e abbronzato" sopratutto vista la nazione e il presidente tirato in causa. Provo a dare io un suggerimento a Mr B. anche se sono un semplice blogger quasi sconosciuto: "Un commento su Obama? Gli Stati Uniti sono un grande paese e una grande democrazia. Se gli americani hanno scelto Obama allora Obama è l'America e sarà sicuramente all'altezza del suo incarico." Ho detto tutto e ho detto niente, ma non ho fatto nessun riferimento al fatto che è nero
  
  
  
10. Le gaffè di Mr B. sono volontarie o involontarie? Preferisce che la stampa parli di quanto sia scemo invece di parlare delle sue leggi ad personam? Leggi ad personam? Ma esistono?
  
  
  
14. Mangano santo subito? Mangano eroe? Dell'Utri mafioso? Se si toccano questi argomenti la reazione è dare colpa ai magistrati estremisti comunisti reazionari incazzati che solo per essere precisi si limitano ad applicare le leggi. In un paese del primo mondo, persone come Mr B. e i suoi amici sarebbero in qualsiasi posto, ma non in parlamento
  
  
  
18. Mills? Sembra una favola, ma non lo è? Complotto pure quello? Il lodo alfano? Assolutamente indispensabile, una priorità del governo. Vero?
  
  
  
22. Alitalia: è un bel magna magna con i soldi degli italiani, anche quello è ammesso? Si può fare?
  
  
  
26. Io sono convinto che nei libri di storia, un giorno, berlusconi verrà paragonato a mussolini. Non scherzo, ne sono davvero convinto. La differenza è che con mussolini le cose si sapevano: "manganello e silenzio". Mentre con berlusconi si vive una favola, un incantesimo. Studio aperto è il telegiornale dei giovani o di chi non ha nessun dvd di Tinto Brass in casa, gli italiani non leggono più e la critica sta svanendo, ma è tutto normale
  
  
  
30. La scuola: riforme si riforme no. Ma non ve lo ricordate il governo delle tre i? Internet, Inglese, Impresa. AHAHHAHAHAH ridiamo. Tu che mi leggi, sai l'inglese? Qui in Lituania che è un paese povero da fare schifo l'inglese lo parlano anche i bambini di 8 anni, ma lo parlano davvero mica "au ar iu?" e "bai bai"
  
  
  
34. Lega Nord e Partito delle Libertà: il meglio che l'Italia si poteva permettere. Da una parte un razzista che brucia la bandiera italiana e dall'altra un mafioso miliardario che tiene in scacco l'informazione e le menti degli italiani. Forza Milan! Forza Juve! W le veline! W "buona domenica" e il grande fratello! Gli Italiani meritano tutto questo, ma rimane sempre qualcuno che non sopporta niente e scappa, si scappa perché non ne può più. Sono pazzi questi che scappano, vero?
  
  
  
38. Digitale terrestre?! AHHAHAHAH in Lituania internet arriva a tutti e la tv digitale la si può ricevere tramite internet. Questo è avanzare, questa è tecnologia. In Italia vi hanno per le palle con sto digitale terrestre, ADSL per pochi e SIM telefoniche ricaricabili a prezzi assurdi
  


 Questi sono vari pensieri che mi sono passati per la testa alle 9 del mattino, mi faccio un giro fino al mare dalle parti del hotel Cattolica sulla riviera romagnola, in passato una della poche regioni d´Italia puramente "rossa" per apprezzare quanto ha da offrirmi il paese che mi ospita. Mi rilasso.  
  


