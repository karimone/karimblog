Title: Notizie dal grande freddo
Date: 2006-01-24
Category: Lituania
Slug: notizie-dal-grande-freddo
Author: Karim N Gorjux
Summary: Avendo Rita in casa, mi giungono notizie fresche fresche dalla Lituania. Il freddo imperversa su tutto il nord-est europeo con temperature che toccano i -35 nella capitale sovietica. Queste notizie si possono[...]

Avendo Rita in casa, mi giungono notizie fresche fresche dalla Lituania. Il freddo imperversa su tutto il nord-est europeo con temperature che toccano i -35 nella capitale sovietica. Queste notizie si possono avere tranquillamente in un qualsiasi telegiornale-rotocalco mediaset, ma quello che voi non sapete è...  
  
... che nel paese natale della mia consorte il riscaldamento centralizzato si è guastato quindi tutta la città è rimasta al freddo per dei giorni. In altre parole... per strada ci si gode la bellezza dei -25 gradi dell'entroterra lituana, mentre in casa si toccano temperature tropicali di 6 gradi sopra lo zero.


 Le conseguenze più ovvie sono tragiche. La prima cosa che pensa di fare chi sta morendo di freddo è di accendersi un fuoco; peccato che in casa sia poco sicuro ed in tutta la Lituania si contano già un centinaio di case devastate dalle fiamme accese da chi non voleva morire di freddo. Oggi la situazione dovrebbe essere tornata alla normalità, ma ancora molte case sono fredde e non si sa ancora quando torneranno a scaldarsi, la visita del politico-pezzo-grosso di turno è imminente.  
  
  
adsense  
  


