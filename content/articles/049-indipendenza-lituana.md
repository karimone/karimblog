Title: Indipendenza Lituana
Date: 2005-02-16
Category: Lituania
Slug: indipendenza-lituana
Author: Karim N Gorjux
Summary: Oggi è festa in Lituania, teoricamente tutti i negozi dovrebbero essere chiusi, ma io ci ho provato lo stesso. Mi sono svegliato alle 8:30 e dopo una sostanziosa colazione mi sono armato[...]

Oggi è festa in Lituania, teoricamente tutti i negozi dovrebbero essere chiusi, ma io ci ho provato lo stesso. Mi sono svegliato alle 8:30 e dopo una sostanziosa colazione mi sono armato di giubotto e sciarpa, ho preso l'immondizia da cestinare e sono uscito. Non nutrivo quelle grandi speranze di trovare il market aperto, ma ho dovuto ricredermi, era un giorno come tutti gli altri. L'orario era sempre lo stesso: 8-23. Passeggiando tra i condomini ho notato le bandiere lituane sventolare, se non fosse per il rosso sembrerebbero i colori della Giamaica, ma non ho niente da ridire, solo noi possiamo confondere la nostra bandiera con quella del Messico... a proposito.. qual'e' la differenza? ;-)

 Non conosco abbastanza la storia locale e trovare informazioni riguardo alla festa nazionale. Ho cercato su internet, ma trovare informazioni esaurienti in Italiano non è facile. Quindi ammetto la mia ignoranza storica e piuttosto che dire una cazzata stò zitto. Mi limito a dirottare la vostra barca da qualche [parte](http://it.wikipedia.org/wiki/Lituania).  
Riguardo l'articolo su wikipedia, i dati demografici confermano le mie impressioni, Riga è molto più Russa di Klaipeda. A Riga ho notato la tendenza bilingue, qui il Russo è celato, nascosto, non ho trovato molti volantini bilingue, anzi, a dire il vero nessuno. Solo sporadicamente ho sentito parlare in Russo le persone. Le nuove generazioni studiano la lingua della Regina.


 Sono stato sgridato tempo addietro riguardo alla mia scarsa inclusione di immagini sul sito, vero.. tutto vero, * Mea Culpa*. Chi di voi sà dove si trova la Lituania? Avete guardato sulla cartina dove sono? Ma non sulla cartina per farsi i cannoni, quella geografica! Ma che gente frequento?? :-)

   
[![Cuneo Lituania](http://www.kmen.org/images/cn-kl.png)  
](http://www.kmen.org/images/cn-kl.png)



 La distanza è opinabile, Andrea mi ha riferito che in macchina da Trieste a Klaipeda sono 2800km, ma in linea d'aria non saprei proprio dirvi. Suppongo che la distanza sia 3000km centinaio più centinaio meno, in ogni caso dovete attraversarvi tutta l'Europa per venirmi a trovare. :-) (Al massimo potete aspettare Lunedì :-()

   
[![Lituania](http://www.kmen.org/images/lt.png)  
](http://www.kmen.org/images/lt.png)



 Se per caso siete così rinco da venire davvero a trovarmi, questa è una mappa grezza e brutta (come chi ha scritto questo articolo) che indica dove abito.


   
[![Ma dove abiti Kappa?](http://www.kmen.org/images/doveabito.jpg)  
](http://www.kmen.org/images/doveabito.jpg)



 Spero di esservi stato d'aiuto

