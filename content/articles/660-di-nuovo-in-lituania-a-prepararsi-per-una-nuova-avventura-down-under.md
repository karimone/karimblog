Title: Di nuovo in Lituania a prepararsi per una nuova avventura down under!
Date: 2014-07-31
Category: Australia
Slug: di-nuovo-in-lituania-a-prepararsi-per-una-nuova-avventura-down-under
Author: Karim N Gorjux
Summary: Eccomi di nuovo qui, appena approdo in Lituania, la prima cosa che voglio fare, è scrivere. Sono a nel cuore della bassa Lituania da un bel po' di mesi, sono arrivato in[...]

[![australia0](http://www.karimblog.net/wp-content/uploads/2014/07/australia0-198x300.jpg)](http://www.karimblog.net/wp-content/uploads/2014/07/australia0.jpg)Eccomi di nuovo qui, appena approdo in Lituania, la prima cosa che voglio fare, è scrivere. Sono a nel cuore della bassa Lituania da un bel po' di mesi, sono arrivato in Italia ad Aprile 2011 per tornare in Lituania a Febbraio 2014. Andare in Italia mi è servito per capire che è un paese da cui bisogna stare lontani **per quanto riguarda il mio stile di vita**.


 A parte l'ondata di caldo anomalo che sta investendo attualmente il paese baltico, non c'è molta differenza nella Lituania da quando l'ho lasciata. A Gennaio arriverà l'Euro, il cibo costa leggermente di più di qualche anno fa e i fondi europei sono stati investiti per restaurare le strutture pubbliche. Nonostante tutto questo c'è sempre un bel via vai di lituani verso l'estero specialmente nei paesi scandinavi.


 Parlando di paesi Scandinavi, ho avuto modo di visitare la Svezia, rimanendo per circa una settimana a Stoccolma. L'idea una volta partito dall'Italia, era quella di stabilirsi in Svezia, ma mia moglie ha avuto un'opportunità di lavoro in Lituania e ha preferito rimanere nel suo paese. Quindi ora siamo in Lituania, ma solo per usarla come trampolino di lancio infatti il mio intento è andare in Australia appena possibile.


 Perché in Australia? Sono cittadino australiano per discendenza, ho il passaporto e quindi ho tutti i vantaggi ad andare in Australia. Già nel 2010 avevo questa idea di partire, ma la scelta però è sempre stata rifiutata per svariati motivi che solo chi ha figli e una famiglia può capire. Ora però ho deciso di provare questa carta senza stare troppo a rimuginare e vedere gli anni che mi scorrono davanti e quindi, di conseguenza, il blog cambia vestito e inizierà a raccontare una nuova avventura nel buon vecchio stile del Karim Blog.


 Intanto il libro sugli articoli che ho scritto fino ad ora sta prendendo forma...


