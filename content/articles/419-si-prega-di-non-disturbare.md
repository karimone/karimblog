Title: Si prega di non disturbare
Date: 2008-02-12
Category: altro
Slug: si-prega-di-non-disturbare
Author: Karim N Gorjux
Summary: In questi giorni sto pensando a troppe cose contemporaneamente. Più hai cose a cui pensare e meno fai, l'ho notato ed è così; penso che se ci si mette un attimo a[...]

In questi giorni sto pensando a troppe cose contemporaneamente. Più hai cose a cui pensare e meno fai, l'ho notato ed è così; penso che se ci si mette un attimo a ragionarci su, è possibile trovare un legame direttamente proporzionale che lega la quantità di cose da svolgere e l'effettivo svolgimento finale.


 Casa, lavoro, soldi, salute, famiglia... sono tutti argomenti che presi uno alla volta ti spaventano, nell'insieme ti sotterrano. Ho bisogno di leggere qualcosa che mi permetta di evadere la mente.


 Si, ma cosa?

