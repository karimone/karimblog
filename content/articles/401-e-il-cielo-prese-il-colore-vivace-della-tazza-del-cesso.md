Title: E il cielo prese il colore vivace della tazza del cesso
Date: 2007-12-26
Category: Lituania
Slug: e-il-cielo-prese-il-colore-vivace-della-tazza-del-cesso
Author: Karim N Gorjux
Summary: Saper affrontare situazioni come queste non e' facile, ma mettendoci un po' di umorismo qualcosa si può fare. La situazione e' questa: siamo in un appartamento sovietico stretto stretto, uno di quei[...]

Saper affrontare situazioni come queste non e' facile, ma mettendoci un po' di umorismo qualcosa si può fare. La situazione e' questa: siamo in un appartamento sovietico stretto stretto, uno di quei appartamenti che ben conosco in cui si può vivere a malapena in due, purtroppo qui siamo in cinque senza contare Greta. **Non dico questo per lamentarmi**, mio padre su skype mi ha chiesto come stava andando e io ho risposto "male", ma non lo dico per lamentarmi lo dico solamente perché è la verità. Cerco di resistere e ne approfitto per dedicarmi a leggere o smanettare sull'ipod e sul Nintend ds.


 Non ci sono molte cose positive, non possiamo muoverci perché non abbiamo la macchina, ci basterebbe prendere un minibus o un bus per andare dove vogliamo, ma Greta sta passando un brutto febbrone che dura dalla sera di Natale e non possiamo muoverci da qui. A Telsiai c'è poco o niente, anche se abbiamo un'ora o due nel pomeriggio per uscire, non sappiamo dove andare, i marciapiedi sono talmente ghiacciati che rischi di spaccarti le gambe scivolando, il freddo e' talmente pungente che quando sei a passeggio per il paese non vedi l'ora di tornare a casa. Una vacanza invernale da queste parti deve essere impostata molto diversamente.


   
 3. Si affitta un **alloggio **o si va **albergo**, non è tanto per noi che siamo ospitati dai parenti, ma per i parenti che non hanno più una vita normale da quando noi siamo ospiti. Non la prendano a male i soliti che dicono che bisogna accettare la "cultura" che ci ospita, se si parte all'avventura è una cosa, ma qui siamo in tre e ci sono certe esigenze da soddisfare.



  - La **macchina**: i lituani hanno dei mezzi che sono molto lontani dalla definizione di macchina. Ho fatto un viaggio dall'aeroporto a Telsiai che non auguro a nessuno, vero che non serve molto la macchina ai miei suoceri, ma quando noi siamo qui serve tantissimo. Tra bambina, passeggino, bagagli e seggiolino una buona macchina è d'obbligo! Il mio auto-consiglio per il prossimo viaggio è affittare una macchina! Imperativo assoluto!


 