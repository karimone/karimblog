Title: Vivere all'estero non è facile, ma la giusta attitudine e un poco di filosofia, rendono tutto più semplice
Date: 2009-05-07
Category: Lituania
Slug: vivere-allestero-non-è-facile-ma-la-giusta-attitudine-e-un-poco-di-filosofia-rendono-tutto-più-semplice
Author: Karim N Gorjux
Summary: La prima volta che sono stato in Lituania, non l'ho vissuta bene, mi incazzavo come una bestia su tutto ciò che non mi andava a genio e scrivevo tante cose usando parole[...]

[![](http://farm4.static.flickr.com/3362/3496558410_51459b0b3a_m.jpg)](http://www.flickr.com/photos/kmen-org/3496558410)La prima volta che sono stato in Lituania, non l'ho vissuta bene, mi incazzavo come una bestia su tutto ciò che non mi andava a genio e scrivevo tante cose usando parole sprezzanti che ben descrivessero il mio personale punto di vista nei confronti di questo paese.


 Ora sono nuovamente qui e le mie opinioni non sono cambiate, perché la Lituania, a parte i miglioramenti alle strutture e i servizi, è sempre lo stesso paese, con la stessa cultura, lo stesso clima e gli stessi abitanti.  
  
E' vero, i lituani hanno sempre le solite facce serie quasi incazzate. Se la prima persona che incontri al mattino è il tuo vicino di casa, non solo ti viene voglia di affidarti alla pratica del masochismo pur di sentirti vivo, ma nella tua mente allenata nella ricerca di nuovi business in Lituania, balena l'idea di dedicarti a delle allegre attività ispirate da quel volto gioioso e pieno di vita. Ti immagini quanto sarebbe bello fare il becchino, aprire un'agenzia di pompe funebri o costruire delle bare di qualità.


 A parte gli scherzi, **se ti illudi di trovare la felicità nei lituani** e/o nel clima Baltico allora parti con il piede sbagliato. Il tempo è quello che è, e non lo si può cambiare, i lituani sono quello che sono e se sono lituani vuol dire che non sono latini quindi è inutile cercare di trovare l'estroversia di un calabrese in un lituano. 

 **Se ti fai risucchiare dal vortice critica - lamentela** per finire con la comparazione a vantaggio dell'Italia per qualsiasi cosa che vedi allora è giunto il momento di andartene. Io che sono nove mesi che sono qui, ho trovato la mia Lituania e non perdo tempo nelle critiche e nelle lamentele, ma cerco di trovare e sfruttare il lato positivo della situazione.


 Ad Agosto mia figlia compirà 3 anni e a Novembre compirò trent'anni avvicinandomi così alla metà (statistica) della mia vita. Sono convinto che farsi il sangue amaro e lamentarsi di cose che non posso cambiare sia stupido ed inutile. Se ti trovi in una situazione che non ti piace, che ti cambia l'umore in peggio, che puoi fare? **O cambi la situazione o cambi il tuo umore.**

 Oggi sono stato per una mezz'oretta al pub in paese. Era da circa 10 giorni che non ci mettevo piede. Ho visto Monica, una delle bariste che senza nemmeno salutarmi mi ha chiesto semplicemente cosa volevo, senza nemmeno chiedermi come stavo visto che erano dieci giorni che non mi vedeva. Dato che in passato ci siamo fatti qualche chiacchierata, il suo comportamento mi ha lasciato perplesso.  
In passato, un episodio come questo sarebbe stata altra merda da sparare sui lituani campi gonfi di humus e poveri di vacche, ora invece penso che molto probabilmente Monica aveva i "cazzi" suoi per la testa.


 **Bisogna prendere la vita come i bambini.** A me sembra che Greta si trovi parecchio bene qui in Lituania, a te non sembra?

