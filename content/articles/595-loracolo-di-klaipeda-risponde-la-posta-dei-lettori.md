Title: L'oracolo di Klaipeda risponde: la posta dei lettori
Date: 2010-03-29
Category: Lituania
Slug: loracolo-di-klaipeda-risponde-la-posta-dei-lettori
Author: Karim N Gorjux
Summary: Vengo contattato, sempre più frequentemente, da persone che mi contattano ponendomi le domande più bizzarre come se io potessi dare delle risposte soddisfacenti e soprattutto competenti. Io non sono l'oracolo di Klaipeda,[...]

[![](http://farm3.static.flickr.com/2058/1600621244_15c44b25de_m.jpg "Bocca della verità")](http://www.flickr.com/photos/uncle-leo/1600621244/)Vengo contattato, sempre più frequentemente, da persone che mi contattano ponendomi le domande più bizzarre come se io potessi dare delle risposte soddisfacenti e soprattutto competenti. Io **non sono l'oracolo di Klaipeda**, non ho tutte le risposte a tutte le domande che mi porrete semplicemente perché sono italiano e vivo a Klaipeda.


 Se mi volete contattare, leggetevi prima le mie risposte a queste email

 Nota: tutte le email che ho pubblicato sono dei **copia ed incolla, non ho apportato nessuna modifica.**

 **Il ristoratore  
**  

> ***Sconosciuto**: sono un ristoratore romagnolo.Ci sono possibilita di investire in qualche attivita nel settore?????? Grazie*  
> ***Karim**: Fammi capire una cosa, se ti dico che ci sono possibilità, inizi a preparare le valigie?*  
> ***Sconosciuto: **le valige semprepronte.Piu info GRAZIE *  
Imparati la lingua (possibilmente partendo dall'italiano), vieni qui in Lituania due mesi, prova tutte le pizzerie, commissiona un'indagine di mercato e trai da te le tue conclusioni.


 **Mia opinione:** Le pizzerie lituane sono aperte da decenni e continuano a fare affari senza dare segni di cedimento. Le uniche pizzerie e ristoranti che ho visto chiudere sono quelli aperti da italiani convinti di diffondere il verbo.


 **Problemi con la ragazza**  

> *[...] Secondo te c'e' il rischio che questa stia cercando di "sistemarsi" con il pollo di turno?  
> Potresti darmi qualche consiglio su come relazionarmi con questa ragazza?*  
Se chiedi ad uno sconosciuto come relazionarti con la tua ragazza, è meglio se frequenti uno specialista. Le donne lituane non sono tanto differenti dalle italiane se non per l'aspetto. Poi ogni donna è un individuo ed ha le sue particolarità. Rispettati e fatti rispettare.


 **Un nuovo stile di vita**  

> *Ciao! sono un dentista di Torino e volevo complimentarmi per il tuo blog e per la tua scelta, soprattutto.  
> Io amo molto i paesi baltici ( ci sono già stato 4 volte...ho 30 anni) e vorrei chiederti un parere:  
> conosco una ragazza estone che mi piace moltissimo e vorrei sapere se secondo te è praticabile la starda di un mio trasferimento lì.  
> E se venisse lei? secondo te si adatterebbe alla nostra società e stile di vita?  
> Attendo risposte e ancora complimenti: lì sì che la vita vale la pena di essere vissuta!!!*  
E' solo una questione di soldi, se non hai da affrontare la dura vita che tocca a tante persone, vai dove più ti piace, tanto potrai sempre farti una vacanza appena senti un poco di malinconia. Esiste però anche la possibilità di trasferirsi in un paese "neutro" che non deve per forza essere la Svizzera. Opta per qualcosa di caldo :-)

 **Lavorare in Lituania**  

> *io sono un musicista, sono un docente nella scuola statale e mi occupo di musica. scrivo, dirigo, arrangio.  
> ma non cerco lavoro solo come musicista  
> cerco un lavoro in una biblioteca, in un museo, il massimo sarebbe in un faro..  
> lo so che chiedo la luna  
> ma se sai dirmi cosa posso fare e se posso fare qualcosa...*  
Una email del genere ha ragione di esistere se il soggetto è il paese con il record di suicidi al mondo. Vieni a farti un giro da queste parti e portati il dizionario.


 **Aprire un negozio**  

> *Ciao, sono Francesco di roma... Avrei intenzioni di aprire un negozio di intimo donna... Puoi darmi qualche consiglio e più informazioni possibili????*  
Questa non l'ho capita. Vuoi dei consigli di business? Come aprire il negozio, cosa fare, come fare? In qualche modo c'entra la Lituania? Parliamone pure, ma sono 200€ l'ora e cerca di farmi ridere!

 **La fiera erotica**  

> *Per il mio discorso di organizzare fiera erotica*

 * dovrei venire a **Klaipedia** prossimamente,  
per tentare di organizzarla quest'estate.*

 *Eventualmente tu sei disponibile da fare  
da contatto,x tradurre, **capire la mentalità** etc.?  
Se ok dovresti dirmi i costi ad ora o mezza giornata.*

 *Tu sai se c e volo Vilnius-Klaipedia ?*

 *Servirà ****se parte il progetto****, trovare tipografia x stampare  
manifesti, quali radio etc.*

