Title: Il vaso di Pandora
Date: 2006-10-19
Category: altro
Slug: il-vaso-di-pandora
Author: Karim N Gorjux
Summary: Davide mi ha segnalato un servizio stupendo che può aiutarti ad uscire dal classico stallo musicale dovuto all'ascolto delle stesse canzoni. Ti piace un artista? Lo dici a Pandora e lei in[...]

Davide mi ha segnalato un servizio stupendo che può aiutarti ad uscire dal classico stallo musicale dovuto all'ascolto delle stesse canzoni. Ti piace un artista? Lo dici a Pandora e lei in base ai tuoi gusti ti trova le canzoni che più ti piacciono. Semplicemente geniale.


 Link: [Pandora](http://www.pandora.com/)

