Title: Flash News: alcuni incontri con i commentatori del blog
Date: 2009-08-07
Category: Lituania
Slug: flash-news-alcuni-incontri-con-i-commentatori-del-blog
Author: Karim N Gorjux
Summary: In questi giorni ho incontrato persone con cui sono diventato amico grazie a karimblog.net. Per la seconda volta ho visto Alessandro che è venuto a visitare Klaipeda e Nida e poi ho[...]

[![Meridianas durante la festa del mare](http://farm4.static.flickr.com/3563/3778613850_fc0f427bd1_m.jpg "Meriadanas")](http://www.flickr.com/photos/kmen-org/3778613850/)In questi giorni ho incontrato persone con cui sono diventato amico grazie a karimblog.net. Per la seconda volta ho visto Alessandro che è venuto a visitare Klaipeda e Nida e poi ho incontrato Stefano, buon commentatore del blog che starà qui per circa quindici giorni con due suoi amici.  
  
Io non vado in ferie, ho molto lavoro da fare sui miei siti, tra cui anche karimblog.net, ho un paio di progetto da finire e poi spero che le cose vadano in meglio. In Italia come và? Da quanto vedo e da quanto so, sembra che Agosto sia il mese dello stop, l'Italia si ferma. Qui in Lituania invece è tutto normale, mia figlia ha ricominciato ad andare all'asilo da ben due giorni. Più ci penso e più mi sorprende

 Oggi sono 349 giorni che sono in Lituania. :-)

