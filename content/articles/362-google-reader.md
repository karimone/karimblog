Title: Google Reader
Date: 2007-08-30
Category: altro
Slug: google-reader
Author: Karim N Gorjux
Summary: Dopo aver utilizzato per parecchio tempo gli aggregatori RSS sul computer, ho deciso di provare Google Reader. All'inizio ero scettico anche perché tra email e reader Google sta diventando sempre più padrona[...]

Dopo aver utilizzato per parecchio tempo gli aggregatori RSS sul computer, ho deciso di provare [Google Reader](http://www.google.it/reader/). All'inizio ero scettico anche perché tra email e reader Google sta diventando sempre più padrona delle mie "cose", tralasciando questo particolare degno di [George Orwell](http://it.wikipedia.org/wiki/George_Orwell), devo dire di essere soddisfatto. Google Reader è un ottimo aggregatore RSS, semplice ed essenziale.


 Approfittando del trasferimento delle mie sottoscrizioni ho rimosso alcuni siti che non aggiornano più o non mi piacciono più. Uno in particolare l'ho rimosso perché *"la vita è come uno specchio e se la guardi sorridendo essa ti sorride".*

