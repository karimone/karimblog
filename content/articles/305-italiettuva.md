Title: Italiettuva
Date: 2007-03-23
Category: Lituania
Slug: italiettuva
Author: Karim N Gorjux
Summary: Quando dicevo la verità sulla Lituania, sono stato insultato dalla testa ai piedi e ho anche rischiato la diffamazione.

Quando dicevo la verità sulla Lituania, sono stato insultato dalla testa ai piedi e ho anche rischiato la diffamazione.


 Sulla Lituania, la figa, la birra e il divertimento abbiamo tante parole da spendere, ma quando si parla di vivere, lavorare, fare una vita normale, non si spende una parola. Ma l'obbiettività non è di casa in Lituania?

 Prima di andare lassù a fare una pseudo vita normale, aspettate che abbiano almeno fatto sparire tutto l'ethernit dai tetti! (Una cinquantina d'anni).


 Un'ultima cosa: l'unica persona che ho visto fare qualcosa di decente lassù era Enrico. Il grande Enrico, alla fine della fiera, si è dimostrato sapere il lituano e della Lituania meglio degli stessi autoctoni. Da quanto mi risulta, al momento, nessuno è ancora riuscito ad avvicinarsi minimamente alle sue magnifiche competenze.


 Amen.


