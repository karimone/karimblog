Title: Quando la risposta ad un commento nel blog diventa troppo lunga, è meglio pubblicare un nuovo articolo
Date: 2009-10-01
Category: Lituania
Slug: quando-la-risposta-ad-un-commento-nel-blog-diventa-troppo-lunga-è-meglio-pubblicare-un-nuovo-articolo
Author: Karim N Gorjux
Summary: Un commento sul mio blog mi ha fatto scrivere una risposta lunga, ma molto lunga. All'inizio volevo pubblicarla direttamente tra i commenti, ma poi ho deciso di scrivere un post a parte[...]

[![](http://farm3.static.flickr.com/2033/2351373247_cf51ff1c1b_m.jpg "Creative Comment")](http://www.flickr.com/photos/trimmer741/2351373247/)Un commento sul mio blog mi ha fatto scrivere una risposta lunga, ma molto lunga. All'inizio volevo pubblicarla direttamente tra i commenti, ma poi ho deciso di scrivere un post a parte per evitare così di scrivere un altro post. Il commento originale lo trovate [qui](http://www.karimblog.net/2009/09/27/italiano-che-vuoi-lavorare-in-lituania-o-nei-paesi-baltici-hai-fatto-i-conti-prima-di-partire/#IDComment36731243), ma non è necessario andarlo a leggere perché ho incluso i passi più salienti a cui do risposta.


 Carissima "Signorina". Ti ringrazio di aver commentato il mio blog, mi fa piacere ricevere dei commenti interessanti e soprattutto scritti in buon italiano. Ti prego di non smettere, la tua opinione arricchisce il blog con nuovi e originali punti di vista.  
  
Tornando al tuo commento, rispondo ad alcune tue affermazioni molto interessanti.


 
> La situazione di cui parli te non mi sembra una "particolarità" dei paesi baltici bensì un problema che riguarda TUTTI i paesi.  
Può essere vero, ma io vivo in Litunia e a parte alcune considerazioni soggettive che mi giungono dall'Italia e i soliti canali di informazione, non ho idea realmente di come si viva in altri paesi europei. Non cadere nella falsa idea che io sia un giornalista e che quello che scrivo sia tutto vero. Io vivo a Klaipeda e passo la maggior parte del tempo in casa davanti ad un monitor, ciò che scrivo nasce dalle mie esperienze e dalle mie conoscenze

 
> Ciò che vedo io in Italia è una stagnazione totale...  
In entrambi i tuoi commenti tendi a fare qualcosa che io trovo pericoloso e anche dannoso ovvero comparare la Lituania e l'Italia, due realtà profondamente se non totalmente diverse. L'Italia ha 20 regioni, [110 provincie](http://www.comuni-italiani.it/province.html) e 60 milioni di abitanti di diverse culture e tradizioni. Oltre i numeri anche la storia è totalmente diversa rispetto alla Lituania quindi il paragone lo si può fare con la pizza, i cepelinai e la birra, ma con il resto non ha davvero senso. Perché allora non fare il paragone con la Svizzera o ad esempio con il Principato di Monaco? Se vuoi fare un paragone tra l'Italia è un altro paese, prova con la Francia, la Germania o la Svezia. Almeno è un confrono più equilibrato. Prenderesti sul serio un combattimentro tra me e Bruce Lee? :-)

 Correggimi se mi sbaglio, ma nei tuoi commenti ho come avuto l'impressione di leggere una ragazza lituana avvolta dall'impeto nel difendere le sue origini. Non c'è nulla di male, ma se io scrivo **la mia opinione** sulla Lituania ciò non vuol dire che automaticamente per me l'Italia è meglio. Ho scritto altri articoli sull'Italia dove esprimo **il mio parere senza troppe riserve**, ma prendi tutto questo come una semplice opinione e non come una condanna ad un paese affascinante come la Lituania.


 
> Poi, quando dici che un italiano dovrebbe essere pronto per i 6 mesi senza lavoro in Lituania, io mi chiedo da dove egli prenda quei soldi? nel 70% dei casi dai loro genitori, vero?  
Gli italiani sono tanti e tutti diversi. Non ho ben chiaro da dove prendi quel 70%, ma presumo che sia un dato che rispecchi le tue esperienze, non penso che tu ti sia affidata [all'ISTAT](http://www.istat.it/) o abbia fatto delle ricerche su un gruppo campione. Quindi ci sono tante realtà, ma ti dico che se in futuro potessi aiutare mia figlia, soprattutto economicamente, non ci penserei due volte a farlo.


 
> Mi dispiace tanto per il peggioramento economico così difficile per i lituani, ma il lavoro si trova, basta avere un'idea e sapere fare qualcosa.  
Il lavoro si trova? Parliamo di che tipo di lavoro e di che salario. Anche in Italia si trova lavoro: operatore ecologico, operatore sanitario... L'idea? Le idee sono utili per chi vuole intraprendere...


 
> Conosco tanti stranieri che vivono in Lituania da qualche anno e non sanno manco' una parola in lituano, tuttavia se la cavano benissimo, lanciano i vari progetti, partecipanno ai progetti gia esistenti, insomma, cercano e trovano. l'importante è avere le palle.  
Io ho scritto in [questo articolo](http://www.karimblog.net/2009/09/27/italiano-che-vuoi-lavorare-in-lituania-o-nei-paesi-baltici-hai-fatto-i-conti-prima-di-partire) solamente degli italiani e basandomi sulle mie conoscenze la percentuale di successi continua ancora ad essere molto bassa. Ovvio però che la propria esperienza non fa statistica, ma dato che non sono un giornalista, mi posso permettere di pubblicare. Domanda (retorica): se conoscessi Arnold Schwarzenegger, te la sentiresti di dire che facendo culturismo è possibile diventare governatore di uno stato americano?

