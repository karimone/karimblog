Title: Mobili, mobili e ancora mobili
Date: 2007-10-17
Category: altro
Slug: mobili-mobili-e-ancora-mobili
Author: Karim N Gorjux
Summary: Vava è un quasi cinquantenne dal fisico di un ventenne, ha un figlio ed una moglie bellissimi e lui è uno di quegli amici che valgono un tesoro, è sempre disponibile ad[...]

Vava è un quasi cinquantenne dal fisico di un ventenne, ha un figlio ed una moglie bellissimi e lui è uno di quegli amici che valgono un tesoro, è sempre disponibile ad aiutarmi con le sue competenze di tecnico tuttofare. Anche voi avete un amico del genere?

 Sabato Vava è venuto a casa mia e mi ha aiutato a montare un armadio mastodontico, il lavoro è iniziato alle 9 del mattino ed è finito alle 5 del pomeriggio. Povero Vava, ha fatto tutto senza lamentarsi e senza nemmeno fermarsi per il pranzo, alla fine della giornata mi sentivo in colpa di averlo coinvolto in 'sto casino. Ad ogni modo il risultato è stato soddisfacente, ho passato il sabato e domenica a smontare e montare mobili ed ecco che ora casa nostra ha assunto un altro volto.


 Ho anche la scrivania nuova, un bel 160x65 cm in pino grezzo presto disponibile su flickr..


 PS: Armadio e scrivania presi direttamente [dall'IKEA](http://eiochemipensavo.diludovico.it/2007/06/21/da-dove-arrivano-i-nomi-dellikea/)!

