Title: Dita nel naso
Date: 2008-03-18
Category: Lituania
Slug: dita-nel-naso
Author: Karim N Gorjux
Summary: 

  
[![](http://farm3.static.flickr.com/2342/2343793452_58db96cfac_m.jpg)](http://www.flickr.com/photos/kmen-org/2343793452/ "photo sharing")  


 Da ormai due giorni sono solo in casa. Rita e Greta sono in Lituania, a Telsiai. La nevica e fa freddo, qui invece si sta benissimo, fa caldo, ci si rilassa. Ad essere onesti sono il tipo a cui piace stare per i cavoli suoi. Con il passare del tempo divento sempre più asociale e filtro parecchio le persone con cui passo il tempo...  
  
  
  
...il tempo! Ne abbiamo così poco!  
  
  
  
"...The sun is the same in a relative way, but you're older  
  
Shorter of breath and one day closer to death..."  
  
  
  
Il sole è relativamente lo stesso, ma tu sei più vecchio.  
  
Più corto di un respiro e un giorno più vicino alla morte.  
  
  
  
La traduzione è esatta? E' un pezzo di "Time" dei Pink Floyd, l'album era "Dark Side Of The Moon" forse uno dei migliori della storia della musica.  
  
  
  
La prossima settimana si parte per i Caraibi, se vi dico che non ne ho voglia, mi credereste?



