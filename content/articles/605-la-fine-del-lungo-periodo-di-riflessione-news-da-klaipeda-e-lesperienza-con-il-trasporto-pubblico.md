Title: La fine del lungo periodo di riflessione: news da Klaipeda e l'esperienza con il trasporto pubblico
Date: 2010-05-31
Category: Lituania
Slug: la-fine-del-lungo-periodo-di-riflessione-news-da-klaipeda-e-lesperienza-con-il-trasporto-pubblico
Author: Karim N Gorjux
Summary: La pausa di riflessione è finita. Ho tanti argomenti che mi sono passati per la testa in questo periodo, ma questo piccolo articolo ha l'intenzione di aggiornarti sulle novità qui a Klaipeda.[...]

[![](http://farm4.static.flickr.com/3563/3778613850_be6de4af87_m.jpg "Meridianas di Klaipeda in estate")](http://www.flickr.com/photos/kmen-org/3778613850/)La pausa di riflessione è finita. Ho tanti argomenti che mi sono passati per la testa in questo periodo, ma questo piccolo articolo ha l'intenzione di aggiornarti sulle novità qui a Klaipeda. Siamo ufficialmente in primavera anche se piove e continua a fare freddo a giorni alterni. Il nome Lituania deriva a da lietus che significa pioggia, quindi è tutto normale che piova e che sia nuvoloso.


 Qualche giorno fa Klaipeda ha perso [la Meridianas](http://www.flickr.com/photos/kmen-org/3778613850/ "La Meridianas a Klaipeda"), il simbolo che caratterizza la città ovvero la barca ormeggiata sul fiume che passa nel centro storico. La Meridianas è una barca a velo in legno che non può più navigare ed è stata usata come "kavine" (bar), ma soprattutto come simbolo di Klaipeda. Circa due settimane fa la barca è stata spostata per essere riparata, ora il centro storico ha un buco, un vuoto che si spera duri poco perché non sembra, ma Klaipeda senza la Meridianas è un po' come Parigi senza la torre Eiffel.  
  
Il momento dello spostamento è stato immortalato da [Mirko](http://www.mirkobarreca.net "Il blog di Mirko Barreca") nel suo [album](http://www.flickr.com/photos/39283264@N00/sets/72157624112857578/).


 In questo periodo sono senza macchina. Ho mandato le targhe e il libretto in Italia per fare l'immatricolazione in Lituania in modo da evitare di pagarmi il bollo, **l'assicurazione oligopolica tipicamente italica** e soprattutto per evitarmi fastidi burocratici in terra baltica. In questo periodo di indipendenza "motoria", sia io che Greta che Rita, abbiamo dovuto affidarci al servizio di trasporto pubblico di Klaipeda. Se non fosse per l'asilo di Greta che dista praticamente una decina di km da dove abitiamo noi, il bus sarebbe il sostituto ideale ed economico all'auto, ma purtroppo in alcuni frangenti sentiamo davvero la mancanza.


 **I bus a Klaipeda sono gli scarti di bus di altri paesi come la Danimarca o la Germania** che rivendono i vecchi modelli a paesi come Klaipeda che deve far quadrare i bilanci per forza. Il biglietto pieno costa 1.80 Litas che equivalgono a circa 50 centesimi di Euro e vale per una corsa singola in un autobus solo. L'entrata al bus si può fare solo dalla porta di fianco all'autista, se hai il biglietto devi timbrarlo usando una piccola macchina rossa che fora il biglietto con dei buchi. Ogni convalida è ovviamente diversa da bus a bus. Quando devi scendere, devi usare la porta al centro del bus, in questo modo non incontri chi sta salendo sul bus e non puoi passare il tuo biglietto come si faceva felicemente qualche anno fa. Inoltre le macchine convalidatrici sono posizionate solo vicino all'autista in modo da evitare i furboni che qualche anno fa lasciavano il biglietto nella convalidatrice, ma facevano il foro solo in presenza del controllo.


 Alcuni bus sono puliti, altri sono luridi. Va un po' a fortuna. Ho notato che **gli autisti non sono l'emblema della sicurezza stradale**, mi è già capitato di vedere ciclisti quasi investiti e gente infilzata dalle porte automatiche mentre cerca di scendere alla fermata. Più volte ho visto gli autisti parlare al cellulare senza troppi problemi, ma la cosa che più mi colpisce e continua a stupirmi è che l'autista si improvvisa anche bigliettaio. Se non hai il biglietto lo puoi comprare sul bus al prezzo maggiorato di 2.40 litas (ovvero 70 centesimi di euro circa), vai dall'autista e gli dai i soldi e lui, ovviamente mentre guida, ti da il biglietto e inizia a maledirti se deve contarti il resto. Immagina a Gennaio, durante le frequenti tempeste di neve, come risulta semplice e sicura la doppia mansione dell'autista.


 **L'alternativa al bus è il mini-bus o taxi collettivo**. Sono dei piccoli furgoncini con una decina di posti a sedere, hanno gli stessi numeri delle linee del bus, ma fanno dei tragitti più convenienti alle tasche dell'autista che è un privato e non un dipendente dal comune. Il vantaggio del minibus è che puoi salire in qualsiasi punto del tragitto e scendere in qualsiasi punto tu voglia scendere. Costa 2.50 (0.72 Euro) Litas che risulta decisamente conveniente se non hai il biglietto dell'autobus a portata di mano. Anche il minibus, come il bus, non fa pagare i bambini fino a 7 anni.


 Il minibus è un servizio assolutamente straordinario perché è disponibile la sera e durante il weekend anche a notte tarda. Se hai intenzione di uscire la sera e berti una birra in compagnia, il minibus diventa indispensabile dato che **la tolleranza all'alcol sulle strade lituane è zero**.


 Link: [Orario dei bus di Klaipeda](http://www.marsrutai.info/klaipeda/?a=p.routes&transport_id=bus&t=xhtml&l=en#stops_12_55.69897_21.14714)

