Title: Affogo i dispiaceri tecnologici in Cucina
Date: 2005-04-18
Category: Lituania
Slug: affogo-i-dispiaceri-tecnologici-in-cucina
Author: Karim N Gorjux
Summary: Ho scoperto che non riesco a telefonare alla Apple dalla Lituania, per non disperarmi troppo tengo occupata la mia mente dilettandomi con la cucina. Armato di ben due libri sull'argomento ho gia'[...]

Ho scoperto che non riesco a telefonare alla Apple dalla Lituania, per non disperarmi troppo tengo occupata la mia mente dilettandomi con la cucina. Armato di ben due libri sull'argomento ho gia' sperimentato qualche succulento (ma ci credete?) piatto. Ho cucinato le polpette, le zucchine stufate, i spaghetti alla carbonara e i **piselli alla Montalto** quest'ultimi stranamente erano anche buoni.


 La prossima meta sono i gnocchi fatti in casa, se riesco me li preparo gia' per domani a pranzo, il dilemma e' il sugo. Mia madre se la sbriga in fretta con il burro e salvia, ma io vorrei osare proponendo qualcosa di piu' ostico, sono sicuro che domani mi verra' in mente qualcosa sfogliando tra le ricette.


