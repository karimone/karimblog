Title: Bere un buon bicchiere d'acqua in un paese senza montagne è un lusso non da poco
Date: 2009-04-29
Category: Lituania
Slug: bere-un-buon-bicchiere-dacqua-in-un-paese-senza-montagne-è-un-lusso-non-da-poco
Author: Karim N Gorjux
Summary: Arrivando da una zona montagnosa, ho sempre bevuto acqua di montagna. La mia preferita è sempre stata l'acqua di Vinadio che non è nient'altro che la migliore acqua delle mie zone. Qui[...]

[![](http://farm1.static.flickr.com/224/514534462_88894375a9_m.jpg "Tear")](http://www.flickr.com/photos/hypergurl/514534462/)Arrivando da una zona montagnosa, ho sempre bevuto acqua di montagna. La mia preferita è sempre stata [l'acqua di Vinadio](http://www.santanna.it/) che non è nient'altro che la migliore acqua delle mie zone. Qui in Lituania invece siamo messi davvero male come acqua, a volte mi tocca bere dell'acqua dal sapore così cattivo che mi vien voglia di andare direttamente ad intingere il bicchiere nel mar Baltico.


 I primi mesi compravamo dei bottiglioni da 5 litri, ma poi, grazie ad alcuni consiglio, abbiamo comprato una caraffa con filtro e consumiamo l'acqua del rubinetto filtrata. Il gusto è pessimo.  
  
Ho provato a comprare varie marche di acqua. La [Tichè](http://www.tiche.lt/) è la marca forse più conosciuta e viene estratta a Telsiai a circa 700 metri di profondità. Se la bevi naturale fa schifo, se la bevi frizzante può ancora andare bene, ma il retrogusto schifoso rimane. Per ovviare il problema del gusto, problema di cui nessuno parla, esiste l'acqua aromatizzata al limone che è leggermente meglio dell'acqua normale, ma ridursi a bere acqua chimicamente aromatizzata non è di certo una scelta saggia.


 Esistono anche altre marche. La peggiore che ho provato e che sembra davvero l'acqua imbottigliata direttamente dal baltico è la [Vytautas](http://www.bmv.bmv.lt/?_nm_mid=TkN3eUxERXNNQ3d3&_nm_lid=0&session=no) quindi se pensi di dover fare la spesa in un supermercato lituano in futuro, stai ben lontano dalle bottiglie d'acqua con sopra scritto "Vytautas".


 Esistono anche altre marche più o meno conosciute, come la Vichy che sponsorizza un [parco acquatico](http://www.vandensparkas.lt/) in Vilnius, la [Perrier](http://www.perrier.com/) che è francese e dal nome mi ricorda la Bertier [l'acqua più gasata al mondo](http://www.youtube.com/watch?v=1sQNcE87UQE). Viene anche importata la San Pellegrino, ma i costi dell'acqua non sono gestibili. Per avere un litro d'acqua decente bisogna spendere quasi un euro e a quel punto tanto vale bere la birra. Costa di meno.  
**  
Fate ben attenzione** a quando ordinate dell'acqua frizzante in un locale, alcune marche possono costarvi molto care.


