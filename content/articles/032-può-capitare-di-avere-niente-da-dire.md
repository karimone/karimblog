Title: Può capitare di avere niente da dire?
Date: 2005-01-05
Category: Lettonia
Slug: può-capitare-di-avere-niente-da-dire
Author: Karim N Gorjux
Summary: Oggi l'Europa si è fermata per 3 minuti, in segno di lutto, a favore delle regioni colpite dallo Tsunami. Ho visto varie immagini in televisione, il tutto è molto triste. Da queste[...]

Oggi l'Europa si è fermata per 3 minuti, in segno di lutto, a favore delle regioni colpite dallo [Tsunami](http://it.wikipedia.org/wiki/Tsunami). Ho visto varie immagini in televisione, il tutto è molto triste. Da queste parti se ne parla a causa di un ventina di Lettoni dispersi in Thailandia, non saprei dirvi se sono stati stanziati dei soldi dal governo, ma vedrò di informarmi.


 Oggi sembra una giornata interminabile, stò meditando di prendere alcune decisioni importanti e cercando di capire qualcosa sul significato della mia presenza qui. Qualcosa ho già fatto, a breve avrò qualche sviluppo e soprattutto chiara la situazione.  
La vita in Old Riga è parecchio noiosa, hai tutte le comodità, ma non c'è nulla di interessante da scoprire, non perchè Riga non abbia niente da offrire, tutt'altro, ma il vento è intenso, il freddo è pungente. Alle 16:30 il sole ci accarezza debolmente prima di salutarci e visitare l'altra parte del mondo, mantenere l'entusiasmo per poter avventurarsi, da solo, contro tutte queste avversità può non essere sempre possibile. Penso che sia solo un momento, vedremo in questi giorni.


 Ora penso che comprerò qualcosa da mangiare, cucinerò e leggero un pò aspettando la sera. Appuntamento importante, come ogni appuntamento d'altronde.


