Title: Una mail da Klaipeda
Date: 2006-10-16
Category: Lituania
Slug: una-mail-da-klaipeda
Author: Karim N Gorjux
Summary: Quando intendi a venire in LT ancora? Mai? Hehehe, se no, vengo io initalia.


> *Quando intendi a venire in LT ancora? Mai? Hehehe, se no, vengo io in  
> italia. *

 Verrò in Lituania e anche molto volentieri, ma vista la mia esperienza, ci verrò solo da turista o meglio da ospite. Verrò a salutarti a Klaipeda e andrò anche a trovare i miei suoceri a Telsai, andrò a salutare Massimo a Vilnius e mi godrò la bellissima campagna dello Zamaitija. Sarà diverso...


 Non mi sono trovato benissimo in Lituania, non è il mio paese e troppe differenze non riuscivo a digerirle, ma mi ha dato una moglie ed una figlia stupende; mi sembra una scusa più che valida per tornare spesso. :-)

