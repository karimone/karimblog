Title: Klaipeda: piccola presentazione
Date: 2008-09-11
Category: Lituania
Slug: klaipeda-piccola-presentazione
Author: Karim N Gorjux
Summary: Klaipeda è la terza città lituana in ordine di grandezza. Conta circa 200.000 abitanti, ha un piccolo centro storico famoso per la sua architettura molto particolare simile a quella danese e tedesca.[...]

Klaipeda è la terza città lituana in ordine di grandezza. Conta circa 200.000 abitanti, ha un piccolo centro storico famoso per la sua architettura molto particolare simile a quella danese e tedesca. ha un porto che muove circa 20 milioni di tonnellate di merce l'anno.


   
A me Klaipeda è sempre piaciuta, ha dei bellissimi parchi, bellissime piazze e preferisco di gran lunga stare qui che a Vilnius o qualsiasi altra città lituana. Ho trovato un video su youtube che presenta Klaipeda con una carrellata di immagini.


   
  


