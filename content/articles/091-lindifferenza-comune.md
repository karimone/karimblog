Title: L'indifferenza comune
Date: 2005-08-18
Category: altro
Slug: lindifferenza-comune
Author: Karim N Gorjux
Summary: Ore 10:43 un aereo sorvola Centallo a bassa quota facendo un casino infernale, nel mentre io mi incammino verso casa dove mia sorella dorme (ancora) beatamente. Il vedere e soprattutto sentire l'aereo[...]

Ore 10:43 un aereo sorvola Centallo a bassa quota facendo un casino infernale, nel mentre io mi incammino verso casa dove mia sorella dorme (ancora) beatamente. Il vedere e soprattutto sentire l'aereo mi ha portato alla mente i brutti fattacci successi in questi giorni dagli attentati a Londra agli aerei precipitati causando centinaia di morti.  
  
Mentre penso a queste cosacce brutte quasi quanto il berlusca, mi viene in mente di fare uno scherzo macabro e stronzo a mia sorella. La casa è stranamente pulita, ovvia e inconfondibile firma di mia madre (inutile richiamare all'attenzione del lettore come una bambina di 16 anni sola in casa non sia in grado di mantenere in ordine), mi dirigo dritto verso la camera di mia sorella ovvero l'unica trincea che ancora resiste all'igiene e che mia madre non ha osato combattere. La fagnana è ancora distesa nel letto, faccia in giù, semi-svestita.


 "Shirley! Ma sei ancora a letto?"  
"Eh.. Uh... Ho sonno..."  
"Perché non rispondi al cellulare?"  
"Devo averlo dimenticato in albergo...ZzzzZ" Nel mentre le lancio il cellulare su una chiappa...  
"Shirley, c'è uno stealth americano sopra Centallo e a Milano c'è stato un attentato: una bomba ha ucciso 52 persone"  
"Ah..Uh... ecco perchè c'è sto casino"  
"Shirley, ma alla televisione parlano di guerra!"  
"Vabbè l'importante è che mi lascino dormire..."  
"La terza guerra mondiale!"  
"E sarà anche l'ultima... e io dormo così non mi accorgo di niente... zzZzz"

 Più macabra la sua reazione che il mio scherzo stronzo.


