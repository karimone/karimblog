Title: Il ritorno alla normalità dopo le feste natalizie risulta sempre un po' difficile
Date: 2010-01-07
Category: Lituania
Slug: il-ritorno-alla-normalità-dopo-le-feste-natalizie-risulta-sempre-un-po-difficile
Author: Karim N Gorjux
Summary: Dopo una "breve" pausa natalizia, eccomi nuovamente a scrivere sul mio blog. Ho passato delle belle vacanza anche se mi sono staccato dal computer solo da un paio di giorni e ho[...]

Dopo una "breve" pausa natalizia, eccomi nuovamente a scrivere sul mio blog. Ho passato delle belle vacanza anche se mi sono staccato dal computer solo da un paio di giorni e ho speso il tempo a studiare e seguire dei video corsi online, oltre che leggere libri.


 A Natale mi trovato dai suoceri per il terzo anno consecutivo, la cosa non mi dispiace, anzi preferisco di gran lunga il Natale in Lituania che in Italia dato che non è ancora così avvelenato dal consumismo sfrenato dei tipici paesi sviluppati.  
  
Il capodanno invece è stato semplice anche perché io il capodanno lo sopporto poco, a mezzanotte sono sceso con Rita e Greta in giardino per vedere i fuochi che altre persone hanno gentilmente acquistato per permetterci di godere dello spettacolo. A mezzanotte e 10 minuti siamo ritornati in casa dato il degenero di ubriachi armati di petardi. Se proprio si devono ammazzare, che lo facciano tra di loro.


 Il giorno dopo abbiamo trovato un bel "regalino" vicino alle scale del nostro piano, qualche ragazzo della festa accanto non deve aver retto abbastanza per ripudiare la cena nel giardino e quindi abbiamo dovuto aspettare lunedì per riutilizzare le scale senza usare le sciarpe come mascherina antigas.


 I giorni si sono susseguiti con le varie abbuffate inutili, cioccolatini, dolci tipici e anche qualche visita di italiani in vacanza. L'asilo ha chiuso pochissimo tempo, giusto a cavallo delle feste e il 6 Gennaio, tutto era aperto, alla faccia del Natale ortodosso.


 Tutto questo è successo in 15 giorni ed ora fatico veramente tanto a tornare alla routine quotidiana.


