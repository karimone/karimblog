Title: Continuiamo la vita di sempre
Date: 2006-10-30
Category: Lituania
Slug: continuiamo-la-vita-di-sempre
Author: Karim N Gorjux
Summary: Mi sveglio alle 6 del mattino, guardo se ho da racattare qualche commento caduto nella lista spam ed ecco che ne recupero uno su ottanta. Meno male, tempo addietro li cancellavo senza[...]

Mi sveglio alle 6 del mattino, guardo se ho da racattare qualche commento caduto nella lista spam ed ecco che ne recupero uno su ottanta. Meno male, tempo addietro li cancellavo senza nemmeno guardare. Leggo un paio di email, rispondo ad un'altra e leggo anche il commento di Andrea: **finirà mai questa storia?** Ma dico io.. quale storia? :rolleyes\_tb:

 Siamo agli sgoccioli di Ottobre, mentre in Lituania nevica a Roma un mio carissimo amico si è fatto gli ultimi bagni prima del grande freddo, ieri qui nella Granda si potevano gustare i 21 gradi con il sole. Tra 15 giorni dovrei cambiare abitazione e tra circa 1 mese dovrei riunire la famiglia italo-lituana.


 Sono in procinto di creare una società per i servizi informatici e la distribuzione di bandalarga nelle zone "dimenticate", se tutto và bene dovrei partire già dal 2007. In sostanza ho mosso più acque qui in 3 settimane che in due anni in Lituania, ma non è colpa dei due paesi, è solo questione di lingua e mentalità. Fossi lituano sarebbe stato (quasi) sicuramente il contrario. Pane al pane, vino al vino.


