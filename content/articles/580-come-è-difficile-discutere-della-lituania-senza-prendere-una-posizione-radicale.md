Title: Come è difficile discutere della Lituania senza prendere una posizione radicale
Date: 2009-12-16
Category: Lituania
Slug: come-è-difficile-discutere-della-lituania-senza-prendere-una-posizione-radicale
Author: Karim N Gorjux
Summary: Ho avuto qualche discussione via email con Maurizio, il fondatore e moderatore del più importante forum italiano sulla Lituania. Discussione piacevole che mi ha dato qualche spunto interessante per scrivere questo lungo[...]

![Cepelinai Vs Pizza](http://www.karimblog.net/wp-content/uploads/2009/12/cepelinaiVSpizza.png "Cepelinai Vs Pizza")Ho avuto qualche discussione via email con Maurizio, il fondatore e moderatore del [più importante forum italiano sulla Lituania](http://www.italietuva.com/forum/). Discussione piacevole che mi ha dato qualche spunto interessante per scrivere questo lungo articolo.


 Circa quattro anni fa ho frequentato il forum partecipando con risposte e domande, soprattutto motivato dalla presenza di uno delle persone più strane ed intelligenti che abbia mai conosciuto: [Enrico Quinto](http://it.wikipedia.org/wiki/Utente:Enrico_Quinto).  
  
In quel periodo mi ero appena sposato e stavo vivendo un cambiamento molto importante nella mia vita, stavo diventando padre ed ero da poco diventato marito. All'epoca avevo appena 26 anni, vivevo in un classico condominio russo che non ha aiutato di certo a vivere meglio in Lituania. Avevo delle pressioni continue e tendevo ad essere molto irascibile, cosa che mi ha portato a litigare facilmente andarmene via da quel forum in malo modo.


 Le divergenze che nascono quando si parla della Lituania nascono in seno alle proprie esperienze personali molto diverse per ognuno di noi. Venire a vivere in Lituania con il conto in banca a 9 cifre o venire qui in cerca di fortuna con 500€ in tasca ha una grande influenza sulla valutazione di qualsiasi paese in cui si voglia andare a vivere. Non dico questo riferendomi al mio caso, ma solo per fare un esempio e farti capire che **le variabili in gioco nella valutazione di una situazione delicata come l'immigrazione sono tante**.


 Qualche giorno fa ho scritto un articolo [sul cane gettato dal ponte](http://www.karimblog.net/2009/11/27/lituani-coglioni-il-caso-del-cagnolino-pipiras-gettato-da-un-ponte-nei-pressi-di-kaunas/) che ha ricevuto centinaia di commenti. Se li leggi tutti noterai che la divergenza è radicale: **o sei per la Lituania o sei contro la Lituania**.


 La puntata di [Melog 2.0 di ieri sera (14 Dicembre 2009)](http://www.radio24.ilsole24ore.com/radio24_audio/091214-melog.mp3) discuteva della peculiarità di internet di accentuare le prese di posizione da parte delle persone. Nel forum di italietuva ero intervenuto raccontando tante cose e molti mi hanno accusato di essere contro la Lituania, molte volte ho iniziato in toni pacati per poi finire di essere insultato. Qualcuno, supponendo le mie origini dal nome esotico, mi ha persino accusato di come "noi" trattiamo le donne. La mia colpa, a suo tempo, è stata quella di non usare mezzi termini ed essere troppo duro nel descrivere e soprattutto giudicare le situazioni.


 Vivevo in un famoso blocco russo e **avevo notato che qualcuno sputava sulla mia porta di casa**; pareva che la porta piangesse come le madonne miracolate che si vedono ogni tanto in quei servizi tappa buchi di studio aperto. A pensarci ora potevano essere stati dei ragazzini, forse il figlio del mio vicino di casa che ogni 4 passi riusciva persino a farne uno senza ciondolare. Forse, avessi controllato le altre porte, avrei notato che anche altre porte in quel condominio avevano ricevuto la benedizione da quei ragazzini dalla vita decisamente troppo anticonvenzionale. Ne parlai sul forum e qualcuno scrisse che me lo meritavo, me lo ricordo bene, la frase era del tipo "*Con quello che scrive non mi meraviglio che gli sputino sulla porta*".


 In quel periodo avevo anche litigato con una persona che come me è sposata con una lituana e ha un bimbo. Veniva sul mio blog e **tutti gli articoli che non gli andavano a genio li commentava anche piuttosto duramente**, abbiamo litigato, ma alla fine tutto si è risolto per il meglio e da allora non l'ho mai più sentito. Un'altra persona con cui avevo litigato a suo tempo ora ho il piacere di vederla e sentirla in amicizia ogni volta che ne abbiamo occasione e di questo ne sono felice.


 Ho fatto una ricerca su italietuva e ho notato che si è parlato varie volte di me e del mio blog ed alcune persone **si sono fatte delle domande sul mio conto o espresso giudizi**. Il mio blog rispetto a 4 anni fa è cambiato molto, gli articoli sono molto più obiettivi e ragionati rispetto ad una volta, c'è molto meno impulsività e molta più qualita nello scrivere e questo è un fatto che è evidente a molti.  
La Lituania è sempre la stessa, ma io sono cambiato, la vita ti fa cambiare, volente o nolente, ed in questa "metamorfosi" ho imparato ad evitare la presa di posizione. **Io non sono "pro" Lituania o "contro" la Lituania**. Io scrivo quello che penso, scrivo le mie esperienze ed evito di dare dei giudizi universali e irremovibili. Spesso finisco i miei interventi ponendo delle domande che tu, come mio lettore, **sei invitato a rispondere con la tua sacrosanta opinione**.


 Nel forum di italietuva non intervengo perché ho un brutto ricordo delle reazioni, un po' come le persone che hanno avuto una brutta esperienza con l'acqua e non vogliono più andare in piscina, io evito di intervenire con argomenti particolari nel forum perché **le reazioni possono essere violente per poi spostarsi nella onnipresente comparazione italiana**.


 *"La Lituania è un paese povero e dalle scarse possibilità"*. In altre circostanze potrei scrivere, come ho fatto via email con Maurizio, la Lituania è un paese di merda, ma lo farei solo per rendere più colorito il linguaggio e non per indurre in reazioni che non ho voglia di controbattere.  
**La Lituania mi "preoccupa" per tre ragioni**: ha un'assistenza sanitaria carente, ha un'assistenza sociale insufficiente e non è in grado di fornire una pensione adeguata ai contribuenti. Posso rischiare di mettermi in prima linea sul forum di italietuva con queste argomentazioni senza essere giudicato, senza dovermi sorbire le inutili comparazioni con la clinica Santa Rita od altre comparazioni Lituania - Italia senza ricordarsi che è la solita sfida Davide contro Golia?

 **Ci tengo però a rispondere a qualche domanda** che indirettamente mi sono state rivolte sul forum, ovviamente risponderò senza entrare troppo nelle mie vicende private.


 **Perché sono tornato in Lituania?**  
Perché quando sei sposato e hai una bambina, devi avere il coraggio e la forza di fare delle scelte per il bene della famiglia e non per il tuo bene. Fai buon viso a cattivo gioco e cerchi di vedere i vantaggi dove ci sono i svantaggi. Per ora sto riuscendo nel mio intento.


 **Perché volevo aprire un forum antagonista e perché ora non mi interessa più aprirlo?**  
In effetti ho voluto aprire un forum, ma poi mi sono ricreduto e ho tolto sia l'articolo che riferiva all'idea e sia tutto il forum. Penso che il mio blog sia sufficiente e non ho abbastanza tempo per dedicarmi a moderare un forum, italietuva basta e avanza. I motivi che mi avevano spinto ad aprire il forum non sono quelli di "ritornare sui miei passi" creandomi una rete di contatti, cosa di cui non ho assolutamente bisogno, ma più che altro **è nato tutto da una chiacchierata in un pub di Klaipeda tra italiani e un pizzico della mia passione per l'informatica.** 

 Inoltre ci tengo a rispondere a qualche intervento dove si parla di me

 
> L'unico rilievo che mi permetto di fare a Karim è la assoluta mancanza di memoria. In fondo lui ci ha bazzicato parecchio su questo forum, e sa benissimo che qui i consigli (quelli "seri") a chi chiedeva informazioni (quelle "serie") non sono mai mancati.


 Penso che questo articolo sia un buon alibi alla mia mancanza di memoria.


 
> c'è stato un fraintendimento tra lui e una persona lituana che parla italiano... credo fosse dovuto al fatto che oramai era diventata un'abitudine per lui lamentarsi in continuazione ed altri non ne potevano più!!!.  
> ...e comunque...qualcuno mi sa dire perchè se ne è tornato in Lituania? non stava bene nella sua Italia?

 **Mi lamentavo in continuazione o ogni mio intervento era interpretato come lamento?** A causa dei miei capelli di color nero corvino, alle volte mi tocca dover cambiare marciapiede per evitare incontri spiacevoli, e su questo, ch vive qui in Lituania capisce bene cosa intendo. Questa argomentazione, per un "pro lituania", è vista come un lamento.


 Quando leggo "Non stava bene nella sua Italia?" non posso che dar ragione a Nicoletti quando internet tende a radicalizzare le opinioni. L'Italia è tua e quindi se non è mia, io sono "pro lituania". Io ci stavo bene in Italia con i pro e contro, esattamente come qui in Lituania dove vivo bene, con i pro e i contro.


 
> Secondo me Karim è una di quelle persone che non si trova bene da nessuna parte....e si lamenta in continuazione, fatti sicuramente suoi, la vita è sua ...ed il suo blog anche  
Ognuno ha il diritto di farsi la sua opinione.


 
> La capacità di osservare una cultura diversa senza degenerare in valutazioni personali è una cosa che non riesce mai a nessuno...c'è gente però che si sforza...altra che non si pone neanche il problema di considerare il fatto che esistono altri 5 miliardi di persone che sono differenti da te  
Questa è una frase che mi è piaciuta molto, ma con un piccolo neo. **Avere delle valutazioni personali non è una "degenerazione". E' la qualità delle valutazioni che rende le valutazioni opinabili.**

 Cosa ne pensi? Sei un "pro Lituania" o un "contro Lituania"?

