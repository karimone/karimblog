Title: Visita dentistica all'ambulatorio privato di Klaipeda: impressioni e costi
Date: 2009-04-17
Category: Lituania
Slug: visita-dentistica-allambulatorio-privato-di-klaipeda-impressioni-e-costi
Author: Karim N Gorjux
Summary: Tempo addietro avevo scritto qualcosa sui dentisti, ma dato che questa mattina sono andato dal dentista, mi sento in dovere di aggiornarti sulla mia esperienza.

[![Dentista Pazzo](http://www.karimblog.net/wp-content/uploads/2009/04/funny-dentist-223x300.jpg "Dentista Pazzo")](http://hollywoodjokes.com/funny-videos/more-dental-funny-jokes-videos/)Tempo addietro [avevo scritto](http://www.karimblog.net/2006/03/20/a-morte-i-dentisti/) qualcosa sui dentisti, ma dato che questa mattina sono andato dal dentista, mi sento in dovere di aggiornarti sulla mia esperienza.


 Appuntamento alle nove del mattino in un ambulatorio privato (e nuovo) non lontano dal centro di Klaipeda. L'ambulatorio è uno studio con vari dottori, non ci sono solo dentisti, ma anche dottori semplici, ginecologi, ortopedici e vari altri dottori specializzati di cui non saprei nemmeno scrivere il nome.


 All'ingresso, come in qualsiasi ospedale e discoteca, c'è il guardaroba. Tutte le volte che entro in un ospedale e mi tocca lasciare la giacca, mi stupisco. Non sono ancora riuscito ad abituarmi a questa usanza locale che, associata a quella di togliersi le scarpe in casa, trovo molto utile.  
  
Alle 9 in punto mi siedo sulla sedia del dentista. Lei è una signora bionda, grassottella sui 45 anni, l'assistente non avrà più di venticinque anni, anche lei bionda. **La strumentazione è esattamente la stessa** che ho sempre visto in Italia e anche i procedimenti sono sempre gli stessi, ma io non sono un dottore quindi non ti fidare troppo della mia opinione.


 Dopo un attento controllo risulta che non ho nessuna carie, procediamo con la pulizia dei denti che dura all'incirca poco meno di mezz'ora. La sedia è un poco spartana e mi tocca fare la sciacquata della bocca a fine pulizia al lavabo vicino alla sedia. Mi alzo, mi pulisco e pago. Costo totale della pulizia: 200 litas ovvero 58€ (senza ricevuta). Avessi avuto la registrazione del mio dottore della mutua in quell'ambulatorio, avrei pagato 30 litas di meno, ovvero 49€.


 Non ricordo esattamente quanto mi costava in Italia una pulizia dei denti, per lo stesso tipo di trattamento penso di aver pagato non più di 120€ o 130€.  
In passato ho sentito e letto che i dentisti dei paesi dell'est sono pericolosi e che c'è il rischio di prendere malattie serie come l'epatite.   
La Lituania è un paese del nord anche se quindici anni fa faceva parte dei paesi *dell'est politico*, io sono residente qui e nessuno mi ha mai detto: *"non farti curare dai nostri dentisti perché rischi di prendere l'epatite o avere la bocca rovinata a vita! Tu che sei italiano, vatti a far curare dai dentisti italiani*".


 Io, a conti fatti, i più grandi fastidi con i denti li ho avuti in Italia.


 Quali sono le tue esperienze con i dentisti all'estero? E in Italia?

