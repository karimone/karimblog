Title: Italiano che vuoi lavorare in Lituania (o nei paesi baltici), hai fatto i conti prima di partire?
Date: 2009-09-27
Category: Lituania
Slug: italiano-che-vuoi-lavorare-in-lituania-o-nei-paesi-baltici-hai-fatto-i-conti-prima-di-partire
Author: Karim N Gorjux
Summary: Spesso mi ritrovo ad affrontare la solita discussione sul come affrontare e mantenere nel tempo un rapporto con una lituana (o donna delle tre repubbliche baltiche). Il tutto inizia nei soliti modi:[...]

[![](http://farm3.static.flickr.com/2078/1981510701_650e9d74ba_m.jpg "Lavorare a Klaipeda")](http://www.flickr.com/photos/lentavas/1981510701/)Spesso mi ritrovo ad affrontare la solita discussione sul come affrontare e mantenere nel tempo un rapporto con una lituana (o donna delle tre repubbliche baltiche). Il tutto inizia nei soliti modi: conosci una ragazza durante un erasmus od una vacanza e scocca la scintilla, l'impennata di internet ha portato persino a conoscersi prima di incontrarsi quindi ho conosciuto gente che è innamorato ancora prima di arrivare in Lituania.  
  
Le relazione a distanza non durano, prima o poi bisogna fare una scelta che non è facile da fare. Venire qui in Lituania, trainato come il famoso carro di buoi, pieno di speranze e di entusiasmo non basta. La Lituania è un paese povero, inutile stare li a contaserla, guardando solo ciò che si vuole vedere delle repubbliche baltice. La Lituania ha assaggiato un boom economico dal 2005 al 2008 che ha portato la gente a fare "gli americani", ora si è tornati ai livelli del 2004. La disoccupazione è alle stelle e il pil sta precipitando trimestre dopo trimestre.


 Cercare lavoro qui per un italiano è pura follia. Se proprio devi venire qui, cerca di lavorare per un'azienda che è già presente; inoltre spera che basti parlare in inglese e che non sia richiesto il lituano. Ovviamente devi essere remunito in Euro perché venire qui per fare il pezzente guadagnando in Litas è da insani. Piuttosto che guadagnare in Litas, torna in Italia che bene o male 1000€ al mese si riescono ancora a guadagnare senza fare troppo gli schizzinosi.


 Sono convinto che bisogna dare i soldi ai lituani e non provare a prendergliene, semplicemente perché di soldi, i lituaniani, non ne hanno. Una mia amica è direttrice di un negozio di moda, le commesse che lavorano per lei si sono viste aumentare la mole di lavoro e diminuire lo stipendio con il passare dei mesi. Attualmente guadagnano circa 300€ al mese. Che si fa con 300€ al mese? Niente. **Ci si limita a vivere**. Pensi che qualcuna di queste ragazze si licenzi? Assolutamente no perché tanto se si licenzia una ragazza se ne trovano almeno altre 10 pretendenti qundi chi ha il lavoro se lo tiene ben stretto.


 Per lavorare qui bisogna sapere il lituano, quindi se vuoi lavorare qui devi sapere la lingua altrimenti sei tagliato fuori. Se sai la lingua che fai? Fai il traduttore? Fai l'intermediario? Che fai? Qualsiasi cosa che tu possa fare ha bisogno di tempo, quindi è importante che tu venga qui in Lituania con le risorse per stare almeno **6 mesi senza lavorare.**

 Oltre il problema del lavoro che non si trova, o si trova sottopagato, bisogna tenere conto della pensione e dei servizi. E' vero che qui si pagano poche tasse e si vive tranquilli, ma io ho 30 anni e se tra 30 anni circa voglio vedere una pensione, devo sperare che la Lituania diventi un paese scandinavo per prendere una mensilità decente. Attualmente gli anziani vivono con 200€ se tutto va bene.  
I servizi invece ci sono, l'ospedale funziona, le scuole funzionano, ma anche qui (come in Italia) è questione di fortuna. Mia moglie lavora nell'ospedale di Klaipeda è all'ultima settimana del mese scarseggiano le risorse perché i soldi mancano... spero di non avere da andare all'ospedale!

 I lituani emigrano in massa per cercare lavoro all'estero. I lituani che sanno la lingua e che conoscono il paese scappano, tu che sei italiano, che ci vieni a fare tu qui? La cosa più intelligente da fare e dare lavoro ai lituani per rivendere servizi o prodotti in Italia o altri paesi. Il contrario è assolutamente fuori discussione, **se non con le dovute precauzioni**, e tra gli italiani che conosco, quelli che hanno avuto successo qui, sono davvero molto pochi.


 Per ora tutte le pizzeria e ristoranti italiani che ho visto aprire hanno chiuso inesorabilmente, altri hanno aperto, ma vi terrò informati sui risvolti.


