Title: Ultimo giorno a Demonte
Date: 2008-07-22
Category: altro
Slug: ultimo-giorno-a-demonte
Author: Karim N Gorjux
Summary: Oggi è il mio ultimo giorno di permanenza a Demonte, sono rimasto qui (a sprazzi) per svariati giorni pur di vedere Greta all'aperto a giocare con la sua amichetta Giorgia. Sono rimasto[...]

Oggi è il mio ultimo giorno di permanenza a Demonte, sono rimasto qui (a sprazzi) per svariati giorni pur di vedere Greta all'aperto a giocare con la sua amichetta Giorgia. Sono rimasto qui anche perché appena torno a casa dovrò fare tante tante cose per poter partire prima possibile e assaporare ancora un po' di estate lituana.


 Perché proprio oggi? Perché all'ora di pranzo il **Tour de France** mi passerà sopra la testa, una volta passato il lumacone di bici e sponsor me ne andrò a casa a dare vita al progetto **Little Italy**.


 Mi mancheranno le montagne.


