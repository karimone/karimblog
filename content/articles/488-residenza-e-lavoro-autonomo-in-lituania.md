Title: Residenza e lavoro autonomo in Lituania
Date: 2008-09-17
Category: Lituania
Slug: residenza-e-lavoro-autonomo-in-lituania
Author: Karim N Gorjux
Summary: Ero a Telšiai (per una volta ho scritto come dovrebbe essere scritto, si pronuncia Telsciai).Non mi ci trovavo per caso, ma avevo bisogno di informazioni. Volevo sapere cosa fare per la residenza[...]

Ero a Telšiai (per una volta ho scritto come dovrebbe essere scritto, si pronuncia Telsciai).  
Non mi ci trovavo per caso, ma avevo bisogno di informazioni. Volevo sapere cosa fare per la residenza in Lituania e come lavorare da libero professionista pagando le tasse in Lituania. In fin dei conti ci abito, perché dovrei pagare le tasse in Italia?  
  
Per la residenza e di conseguenza il permesso di soggiorno, è abbastanza facile. Io poi che sono sposato con una lituana ed in più ho una figlia lituana, è ancora più facile. Si presentano i vari documenti più un **saldo** di conto corrente lituano con almeno 800 Litas al mese per dodici mesi e un'assicurazione sanitaria che copra le cure mediche per almeno sei mesi (costa circa 180 Litas). Le pratiche durano all'incirca 1 mese.


 Avere una partita iva da libero professionista in Lituania è leggerissimamente diverso da ciò che capita in Italia. Per prima cosa bisogna avere il **permesso di soggiorno** altrimenti non viene rilasciata la partita iva; tutta la burocrazia viene effettuata nei [vmi](http://www.vmi.lt) che sono degli sportelli presenti in qualsiasi città per informazioni e pagamento delle tasse. Io e Rita ci siamo andati e la discussione tra me (io) e la signora molto gentile è stata questa:

 *  
**Io**: Salve, vorrei sapere cosa devo fare per diventare libero professionista in lituania ed emettere fattura all'estero, prevalentemente in Italia.*

 ***Smg**: lei deve aprire una partita iva che non ha costi di apertura e di chiusura, la procedura richiede circa 5 giorni.*

 ***Io**: Perfetto, ma poi posso fare le fatture con la partita iva da mandare in Italia?*

 ***Smg**: Si. Certo!*

 ***Io**: Ma per registrare le fatture, devo avere un commercialista?*

 ***Smg**: Scusi, ma il commercialista serve solo se ha una UAB (la nostra srl) e vari impiegati. Come libero professionista lei deve emettere fattura e annotarla sul "giornale fatture" che poi ci consegnerà a fine anno.*

 ***Io**: Ma quanto pago di tasse?*

 ***Smg**: Il 15% del guadagno.*

 ***Io**: Ma non esistono fasce di reddito o cose simili?*

 ***Smg**: Assolutamente no, lei paga sempre il 15%.*

 ***Io**: E lo studio di settore?*

 ***Smg**: Guardi che non ci occupiamo di marketing, ma di tasse.*

 ***Io**: Quindi se ho ben capito io emetto fattura, la annoto sul giornale delle fatture e poi a Dicembre vengo qui da lei e registriamo il tutto?*

 ***Smg**: Si.*

 ***Io**: Quindi mi basta fare il 15% del totale delle fatture per sapere quanto dovrò pagare di tasse.*

 ***Smg**: Esatto.*

 ***Io**: E per la pensione?*

 ***Smg**: Lei paga 180 Litas al mese per la pensione. Ma solo se raggiunge un minimo di fatturato. (Il minimo non me lo ricordo)  
**Io**: E l'assistenza sanitaria?*

 ***Smg**: Il 30% delle tasse che lei paga servono per pagare la sua assistenza sanitaria.*

 ***Io**: Perfetto. Un'ultima cosa: se acquistassi del materiale per la mia attività, posso scalarlo dalle tasse?*

 ***Smg**: Funziona in questo modo, se lei ha fatturato ad esempio 50.000 Litas e il computer le è costato 5.000 Litas, puoi sottrarlo dal guadagno, ma a quel punto le tasse non sono più il 15%, ma il 25%. Nel suo caso dovrebbe pagare 11.250 Litas di tasse. Le conviene allora scegliere di pagare il 15% sul totale senza la sottrazione del computer che sono solo Litas. Sostanzialmente è lei che deve scegliere a fine anno quale sistema le conviene.*

 ***Io**: Perfetto. Ho capito. Ci vediamo per la registrazione della partita iva appena ottengo il permesso di soggiorno. Iki!*

 ***Smg**: Iki!  
*

