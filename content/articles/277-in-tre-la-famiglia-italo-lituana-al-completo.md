Title: In tre! La famiglia Italo-Lituana al completo
Date: 2006-12-22
Category: Lituania
Slug: in-tre-la-famiglia-italo-lituana-al-completo
Author: Karim N Gorjux
Summary: Ormai è una settimana che siamo tutti insieme, sono felicissimo e anche un po' spaventato nei confronti di cosa mi sta accadendo, ma tutte le preoccupazioni svaniscono quando prendo in braccio mia[...]

Ormai è una settimana che siamo tutti insieme, sono felicissimo e anche un po' spaventato nei confronti di cosa mi sta accadendo, ma tutte le preoccupazioni svaniscono quando prendo in braccio mia figlia Greta. Ormai ne sono innamoratissimo.


 Non credevo assolutamente che sia Rita che Greta si sarebbero ambientate così bene, forse è presto per dirlo, ma Rita sembra tranquilla, le piace stare a Centallo, le piace la casa e forse le piaccio pure io. :-D Greta passa la maggior parte del tempo a dormire, la sera si addormenta facilmente, mangia, fa le caccone, tutto è nella norma. Abbiamo ricevuto un sacco di visite in questo periodo, molti parenti e amici miei di sempre, Rita parla italiano e si fa capire molto bene, parla, discute, va nei negozi a fare la spesa, ha già persino guidato la macchina per andare a Cuneo.


 In pratica Rita fa molte più cose di quante ne facessi io in Lituania dopo due anni, Rita la lingua l'ha imparata grazie ad un Mr X (modesto) che le ha sempre pazientemente parlato italiano tutti i giorni. **Per ora sembra andare tutto bene**, staremo a vedere con il tempo se continuerà ad essere così o meno.


 Una nota: a Cuneo esistono dei c**orsi d'italiano gratuti per gli stranieri**, il nostro paese in fin dei conti cerca nel bene o nel male di accettare gli stranieri. In Lituania per imparare il lituano dovevo pagare. Xenofobia?

