Title: Anche quest'anno ho seguito il Sanremo dei residenti all'estero: Eurovision 2010
Date: 2010-06-01
Category: Lituania
Slug: anche-questanno-ho-seguito-il-sanremo-dei-residenti-allestero-eurovision-2010
Author: Karim N Gorjux
Summary: In Italia conosciamo solo Sanremo, il festival della canzone italiana, ma in tutta Europa le famiglie passano un intero weekend di fronte al televisore per seguire il contest musicale europeo per eccellenza:[...]

[![](http://www.karimblog.net/wp-content/uploads/2010/06/eurovision-300x270.jpg "Eurovision")](http://www.karimblog.net/wp-content/uploads/2010/06/eurovision.jpg)In Italia conosciamo solo Sanremo, il festival della canzone italiana, ma in tutta Europa le famiglie passano un intero weekend di fronte al televisore per seguire il contest musicale europeo per eccellenza: l'Eurovision.


 Prima di venire qui in Lituania io non sapevo nemmeno della sua esistenza, poi mi sono documentato e ho capito il perché in Italia non ne sappiamo nulla. L'Eurovision è un contest musicale dove ogni nazione viene rappresentata da un proprio gruppo musicale e una nuova canzone scritta per l'occasione.  
  
Il problema è che la nazione vincente ha l'obbligo di organizzare l'Eurovision dell'anno successivo. Quest'anno ha vinto la Germania e quindi il prossimo anno l'Eurovision si terrà in Germania. Questo è il motivo principale per cui l'Italia non partecipa praticamente mai; organizzare l'Eurovision ha dei costi molto alti e il format televisivo è differente dallo stile di Sanremo, meno pubblicità, meno introiti e quindi poco interesse.  
**  
Sinceramente preferisco l'Eurovision al Sanremo**, canzoni originali rispetto alle nostre, stili diversi, culture diverse e molta meno pubblicità. Il livello non è altissimo, ma ci sono delle vere perle musicali anche se poi il voto viene dato dagli europei e quindi il rischio di che vinca la canzone più politicamente supportata e non artisticamente migliore è molto alto.


 Una delle canzoni che ho preferito è ["This is my life" di Anna Bergendahl](http://www.youtube.com/watch?v=ZU892o3J1wY) che ha gareggiato con i colori della Svezia, mentre la Lituania ha gareggiato con una canzone (penosa) degli Inculto: [Eastern European Funk](http://www.youtube.com/watch?v=-u3sMy22qp0).


 **La canzone vincitrice è della Germania**, [Lena "Satellite"](http://www.youtube.com/watch?v=mg3uX9jg_MI), niente di speciale, ma cantante carina. In Italia non è ha parlato nessuno, ma in Europa lo show è seguento da più di 40 paesi.


 Link: [Intervista a Lena (in inglese)](http://www.youtube.com/watch?v=Mdw_DDXsdEU)  
Link: [sito italiano dell'Eurovision](http://www.eurofestival.com/)  
Link: [La vittoria di Toto Cotugno nel 1990](http://www.youtube.com/watch?v=N7uVZhIp_mY)

