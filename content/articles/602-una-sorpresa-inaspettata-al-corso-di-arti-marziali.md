Title: Una sorpresa inaspettata al corso di arti marziali
Date: 2010-04-28
Category: Lituania
Slug: una-sorpresa-inaspettata-al-corso-di-arti-marziali
Author: Karim N Gorjux
Summary: Ho una certa esperienza di arti marziali che spazia dal Karate, boxe, full contact per finire al Jeet Kune Do che è l'arte che penso di aver praticato di più. In Italia,[...]

![](http://www.karimblog.net/wp-content/uploads/2010/04/Application-Wing-Chun.jpg "Application Wing Chun")Ho una certa esperienza di arti marziali che spazia dal Karate, boxe, full contact per finire al Jeet Kune Do che è l'arte che penso di aver praticato di più. In Italia, prima di sposarmi frequentavo la palestra assiduamente e anche la palestra di Jeet Kune Do. Qui in Lituania non è stato facile per me, ma un po' per caso e un po' per buona volontà sono riuscito a ricreare quell'ambiente che in Italia mi ero creato.   
  
Giocando a calcio balilla ho conosciuto vari ragazzi e tra i tanti ho conosciuto Lauras, un ragazzo che frequenta il Wing Tsun e che mi ha consigliato di venire a provare una lezione. La prima volta che sono andato al corso di Kung Fu avevo un po' paura nel ritrovarmi tra i classici lituani armadi, testa rasata e rompi balle. Invece niente di tutto ciò, il gruppo è normalissimo, frequentato da persone normalissime e molto gentili. Cosa mi ha stupito è il maestro che è una persona oltra ad essere molto gentile anche molto competente.


 Ora sono circa due mesi che frequento il corso, ma qualche giorno fa è successo qualcosa di inaspettato. Era appena iniziata la lezione e avevo notato un nuovo iscritto: alto, biondo, occhi azzurri, forse meno di vent'anni. Mi guarda e si avvicina e mi chiede: "Vuoi allenarti con me?". Sono rimasto stupito perché me lo ha chiesto in perfetto italiano e dato che non sono più tanto abituato a sentirmi parlare in italiano da persone che non siano mia figlia o mia moglie, ho risposto d'istinto e con una stronzata. Gli ho chiesto: "Ma sei italiano?"

 Alto, biondo e con gli occhi azzurri, Rimas è un ragazzi di 18 anni che ha vissuto gli ultimi dieci anni in un paesino vicino ad Asti. Ora vive a Klaipeda e mi racconta che dovendo finire la scuola qui si ritrova molto indietro rispetto al programma scolastico della sua classe. L'inglese è praticamente a livello zero e in matematica ha dovuto compensare con delle lezioni private. Non so se Rimas sia un esempio, ma io sono sicuro che rispecchi esattamente la situazione della scuola pubblica italiana rispetto alla Lituania (non parlo delle università ovviamente).


 Sta di fatto che Rimas è stato un regalo inaspettato al corso di Wing Tsun, ora ho un traduttore Lituano - Italiano che mi aiuta a comprendere meglio tutto ciò che dice il maestro e quindi, oltre ad imparare il Kung Fu, miglioro anche il lituano.


