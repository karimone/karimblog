Title: La lingua impossibile
Date: 2007-12-15
Category: Lituania
Slug: la-lingua-impossibile
Author: Karim N Gorjux
Summary: Questa mattina Rita si è alzata prima di me ed è rimasta affascinata nel vedere la neve, sinceramente anche io ci sono rimasto male, era da un po' che qui non nevicava[...]

Questa mattina Rita si è alzata prima di me ed è rimasta affascinata nel vedere la neve, sinceramente anche io ci sono rimasto male, era da un po' che qui non nevicava come si deve e soprattutto è tanto tempo che non nevicava prima di Natale.


 La notizia ha subito sconfinato verso la Lituania, Rita ha subito informato i genitori dell'evento: "neve in Italia!". Non ho mai capito perché il clima italiano, nel nord d'Europa, è paragonato al clima africano.


 Oggi parlare di Lituania ha più senso del solito, ho avuto il piacere di leggere un bell'articolo di Massimo che condivido appieno. L'articolo lo potete leggere [qui](http://www.italialituania.com/2007/12/14/cosa-mi-hanno-insegnato-i-lituani/).


 A mio parere, la Lituania ha tante cose che mi mancano. I prezzi bassi, la burocrazia semplice, il notaio che costa poco, i locali sempre aperti... ma ha anche tante cose con cui non riuscirei mai a convivere, tra tutte la prima cosa è **essere uno straniero in terra straniera**. Fino a quando non sai la loro lingua non potrai mai integrarti e questo è il più grande problema per chi vuole vivere in Lituania (da qui arte sempre il solito discorso sull'utilità della lingua lituana). Penso che se proprio devo andarmene dall'Italia me ne andrei in una delle due grandi u: UK or US.  
  
  
  


