Title: Blogger per sempre
Date: 2006-10-12
Category: altro
Slug: blogger-per-sempre
Author: Karim N Gorjux
Summary: Caro visitatore, forse tu che leggi i miei post hai anche il tuo blog, forse anche tu, come me, sei un blogger. Scrivi e non darti mai per vinto, esprimiti, descrivi, racconta,[...]

Caro visitatore, forse tu che leggi i miei post hai anche il tuo blog, forse anche tu, come me, sei un blogger. Scrivi e non darti mai per vinto, esprimiti, descrivi, racconta, dai sfogo a ciò che pensi, ma **non dare mai troppa importanza ai commenti.** Continua sempre per la tua strada. Ascolta chi è costruttivo, ridi di chi ti insulta e ricorda che anche i migliori blogger italiani hanno i loro nemici, non demoralizzarti, non farti influenzare. Scrivi! **E' la tua libertà, il tuo pensiero.**

