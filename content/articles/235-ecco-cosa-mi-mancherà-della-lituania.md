Title: Ecco cosa mi mancherà della Lituania
Date: 2006-10-11
Category: Lituania
Slug: ecco-cosa-mi-mancherà-della-lituania
Author: Karim N Gorjux
Summary: Ci sono tante cose della cultura lituana che mi sono sempre piaciute, purtroppo sempre meno di quelle che non mi piacciono, ma abbastanza da scriverci un post. Togliersi le scarpe: non è[...]

Ci sono tante cose della cultura lituana che mi sono sempre piaciute, purtroppo sempre meno di quelle che non mi piacciono, ma abbastanza da scriverci un post.  
  
  
 * **Togliersi le scarpe:** non è solamente un'usanza lituana, ma anche altri paesi hanno questa abitudine. Togliersi le scarpe quando si entra in casa di qualcuno è una prassi igienica che dovremmo adottare anche qui in Italia. Sicuramente renderebbe più facile la vita alle casalinghe. In Lituania pure il tecnico internet quando ti entra in casa si toglie le scarpe.

  
 * **Gli orari continuati:** diretta causa del "mangio quando mi pare", l'orario continuato rende liberi di andare in un negozio a qualsiasi ora del giorno fino a tarda serata. Una comodità.

  
 * **Il giallo prima del verde:** non ho mai capito perché in Italia i semafori il giallo viene usato solo prima del rosso. Basterebbe questo piccolo accorgimento per rimuovere l'ansia della partenza.

  
 * **La burocrazia:** forse è dovuto alla popolazione estremamente ridotta rispetto a noi, ma la mia poca esperienza burocratica in Lituania mi ha semplicemente sbalordito: immediati.

  
 * **I sorrisi delle donzelle:** il sorriso in se è sempre piacevole, non ha importanza a quale scopo una bella bionda ti sorrida, ma è pur sempre piacevole. Penso però che succeda più agli stranieri che agli uomini locali.

  
 * **L'estate "lunga" e fresca:** a Klaipeda l'estate è stupenda, il sole tramonta tardi, anzi, tardissimo. Non è il massimo per andare a dormire, ma è molto bello vivere delle giornate così lunghe e per di più fresche.

  
 * **Le cicogne:** nel periodo giusto, guidando in mezzo alla campagna lituana, è possibile incrociare tantissime cicogne. A volte dei pali non molto lontani dalla strada ospitano un grande cespuglio in cima con delle cicogne enormi.

  
 * **I bambini:** ho visto tantissimi bambini per la Lituania, tantissime mamme con i passeggini e soprattutto i bimbi piccoli sono molto socievoli e simpatici. Sono il futuro della Lituania, non hanno provato nessuna occupazione, speriamo in loro.

  
 * **I parchi:** il verde non manca, nemmeno nelle grandi città. Lo avevo notato a Riga, ma la Lituania non è da meno. Tantissimo verde a poca distanza dal centro. Purtroppo in Italia chi vive nelle grandi città non ha queste possibilità.

  
 * **Gli snack per la birra:** non sono un bevitore, non fumo nemmeno, ma gli snack da bere con la birra sono assolutamente fantastici. Sicuramente non sono la miglior dieta del mondo, ma al Jazz Club avevo sempre qualcosa da sgranocchiare. Bisognerebbe importare i kepta duoni (o duona? non ricordo) qui da noi.

  
 * **Il cielo:** praticamente cambia in continuazione. La Lituania non ha montagne e il vento fa da padrone. Ogni tanto a guardare il cielo mi ci perdevo.

  
 * **Il mare:** a Klaipeda oltre a vedere le stupende spiaggie di Nida, completamente pubbliche e coperte da una sabbia stupenda, è possibile fare il bagno nel mar Baltico. A differenza del nostro mediterraneo ha pochissimo sale, a volte ti sembra di essere in un fiume.

  


 Sicuramente ho altre cose da scrivere a riguardo, ma per il momento non mi viene niente in mente.


