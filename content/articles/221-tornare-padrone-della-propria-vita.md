Title: Tornare padrone della propria vita
Date: 2006-10-01
Category: Lituania
Slug: tornare-padrone-della-propria-vita
Author: Karim N Gorjux
Summary: Per ogni problema devo chiedere aiuto a Rita o ai suoi parenti, ogni volta è uno scoglionamento biblico. Il problema attuale è: dove prendo gli scatoloni robusti e la carta da imballaggio?[...]

Per ogni problema devo chiedere aiuto a Rita o ai suoi parenti, ogni volta è uno scoglionamento biblico. Il problema attuale è: **dove prendo gli scatoloni robusti e la carta da imballaggio?** La risposta inequivocabile: "non posso sapere tutto... mio padre non può sapere tutto.." Bah, non mi arrabbio e non me la prendo, sicuramente risolverò anche questo problema, ma sempre con la mente costruisco una situazione simile a ruoli invertiti in Italia. Saprei già come fare, a chi chiedere e dove andare. Soprattuto **a chi chiedere**.


 Dato che Rita mi ha proposto di andare a **comprare** gli scatoloni, penso proprio che ricadiamo nel problema solito di [chiedere](http://www.karimblog.net/2006/09/29/la-donna-lituana-un-mistero). **Non incazziamoci** perché ce le siamo scelte noi la donna lituana.


 Morale della favola: in Italia sarò nuovamente **padrone della mia vita**.


