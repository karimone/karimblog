Title: Ricevo una lettera da un mio lettore: troppa negatività nei confronti dell'Italia
Date: 2008-10-29
Category: Lituania
Slug: ricevo-una-lettera-da-un-mio-lettore-troppa-negatività-nei-confronti-dellitalia
Author: Karim N Gorjux
Summary: Ho ricevuto un interessante lettera che critica la piega che sta prendendo il mio blog, sono troppo negativo con l'Italia e la presento come un paese praticamente allo sfascio. Pubblico volentieri la[...]

Ho ricevuto un interessante lettera che critica la piega che sta prendendo il mio blog, sono troppo negativo con l'Italia e la presento come un paese praticamente allo sfascio. Pubblico volentieri la lettera perché un interessante spunto di riflessione che vorrei condividere con voi tutti. Intanto ammetto di essere troppo negativo nei miei post tricolore e penso ché limiterò di molto le critiche in futuro.  
  
*Innanzitutto mi presento. Mi chiamo Ernesto (ndr: nome di fantasia), ho 25 anni, e anch'io sono in qualche modo come te legato alla Lituania. Nel 2005 ho fatto l'Erasmus a Kaunas, ho conosciuto il paese, ho conosciuto la gente, ho assaporato lo stile di vita e mi sono innamorato di una ragazza, di cui tuttora sto portando avanti la storia nonostante la distanza.. Ritornello già sentito dirai...  
La mia e-mail però non ha lo scopo di raccontarti la mia biografia, ma piuttosto di darti un paio di feedback al tuo blog e alla piega che secondo me sta prendendo.*

 Premetto che da almeno 2 anni sono in costante aggiornamento e almeno una volta alla settimana ci faccio una visita.  
Il blog, è uno strumento bellissimo per condividere pensieri, stati d'animo, emozioni e qualora fosse necessario ricevere critiche.  
I tuoi interventi sono molto interessanti: mi piace quando racconti in maniera molto naturale momenti di vita quotidiana con tua moglie e tua figlia in Italia e in Lituania.  
Li ritengo pertanto utili per me per capire le sfumature di una coppia mista, che all'apparenza non si capirebbero visto che anch'io sono quasi nella stessa barca.


 Ma, ed è qui vengo al punto, sto notando con una certa delusione che il blog sta prendendo una piega in cui non mi ci riconosco più nella lettura.  
Mi riferisco alla particolare negatività e pessimismo che trasmetti nei tuoi post quando devi parlare dell'Italia. Non voglio entrare nel merito e grado del tuo "odio" però sento di dirti di moderare un po' di più certe tue posizioni, in quanto esse possono influenzare molto negativamente l'"opinione pubblica".


 La maggior parte del tuo "target" di lettori è gente che come me ha qualche connessione con la Lituania, e nella maggior parte dei casi sono classiche coppie dove lui è italiano e lei è lituana.  
Una delle maggiori sfide che sono richieste ad una coppia mista affinché costruisca basi solide per il futuro è l'integrazione delle parti nelle rispettive culture.  
Io sono l'interfaccia per la cultura italiana alla mia fidanzata e lei a sua volta è la porta di accesso per me alla cultura lituana.. e deve essere un continuo scambio affinché la coppia cresca.. non esistono manuali d'uso, questa secondo me è la chiave di successo per una solida e duratura relazione mista.  
Se la volontà a questo scambio viene a mancare, la relazione prende una pericolosa direzione unica dove una cultura prende il "sopravvento" grazie alle critiche dell'altra, sotto forma di imparziali confronti e con effetti negativi nella coppia stessa e nei figli. Ti parlo per esperienza di una persona a me molto vicina.


 Anch'io in certi punti mi trovi d'accordo sulla situazione italiana, ma da connazionale ti devo ricordare che siamo nati lì e in qualche modo bisogna coltivare un minimo di orgoglio, soprattutto quando si è a contatto con culture diverse.. non sentirai mai parlare cosi male un lituano della Lituania.


 Quindi, concludendo, criticare in maniera così dura il proprio paese non porta molto lontano, raccoglie qualche consenso iniziale nella condivisione del problema ("si è vero", "uhm hai ragione" ), ma alla fine se non si hanno gli strumenti concreti per risolvere il problema, rimangono solo parole. Parole che hanno un certo peso quando sono inserite in un blog con una certa visibilità quale il tuo. Una negatività che non porta beneficio a nessuno.  
Siamo vivendo in un modo sempre più maledettamente complicato, non necessitiamo dei **"profeti della sciagura"**, ma abbiamo bisogno di più speranza e più positività.


 Ho riassunto così il mio pensiero, in base ad alcune riflessioni che desideravo trasmetterti. La critica assume chiaramente toni morbidi e se possibile costruttivi, è un feedback di un tuo lettore appassionato ai tuoi post che vorrebbe un blog e un mondo più positivo.


 Con simpatia

 Ernesto

