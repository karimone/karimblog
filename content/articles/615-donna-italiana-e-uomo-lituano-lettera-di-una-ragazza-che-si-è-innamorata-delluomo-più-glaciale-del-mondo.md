Title: Donna italiana e uomo lituano: lettera di una ragazza che si è innamorata dell'uomo più glaciale del mondo
Date: 2010-09-06
Category: Lituania
Slug: donna-italiana-e-uomo-lituano-lettera-di-una-ragazza-che-si-è-innamorata-delluomo-più-glaciale-del-mondo
Author: Karim N Gorjux
Summary: Sono abituato a leggere storie tra uomini italiani e donne lituane e sono sicuro che anche tu avrai sentito per lo più di storie di questo tipo. Quando sono stato contattato da[...]

[![](http://www.karimblog.net/wp-content/uploads/2010/09/ragazzo_lituano.jpg "Ragazzo lituano")](http://www.karimblog.net/wp-content/uploads/2010/09/ragazzo_lituano.jpg)  
Sono abituato a leggere storie tra uomini italiani e donne lituane e sono sicuro che anche tu avrai sentito per lo più di storie di questo tipo. Quando [sono stato contattato da una ragazza italiana](http://www.karimblog.net/2006/09/29/la-donna-lituana-un-mistero/#comment-54866) che ha avuto una storia d'amore con un lituano ho subito colto l'occasione e le ho scritto per chiederle se voleva pubblicare qualcosa sul blog. Ha accettato e ha scritto una lettera stupenda che offre un sacco di punti di riflessione sull'uomo lituano.


  
  
*Sono una ragazza siciliana di 25 anni e lui si chiama Darius e l'ho conosciuto sul lavoro. Entrambi eravamo marittimi. A dir la verità quando ho iniziato il mio lavoro, a bordo delle navi di lituani non ce n'erano, poi con il passare degli anni prima uno, poi due ed adesso sono quasi la metà!

*

 Comunque la maggior parte dei lituani erano donne. Di uomini ne ho conosciuti soltanto tre. Le ragazze, nonostante avessi dei pregiudizi data la loro fama di ragazze dell est, **le ho trovate davvero intelligenti, sensibili e mature** e, se devo essere sincera, anche simpatiche nonostante bisogna farle bere un po' per far perdere loro il musone ;-)



 Con il tempo sono diventato amica di alcune di queste lituane, sono ragazze "con le palle" molto sicure di se stesse.




 **All'inizio avevo una relazione con un ragazzo italiano che mi diceva tante cose belle e romantiche, ma in realta erano tutte bugie**. Ormai ero stanca di questo genere di uomini; la maggior parte degli italiani sono falsamente romantici, sono furbi, hanno capito come si fregano le donne e soprattutto sono instancabili predatori. Abitutata a conoscere solo questo genere di uomini, puoi immaginare come potessi ammirare questo ragazzo lituano così impegnato nel lavoro, amico di tutte le ragazze senza mai provarci con nessuna e con un'eleganza e una gentilezza unica. 



 E poi era cosi bello! Alto, muscoloso, dagli occhioni azzurri, i capelli castani sulle spalle e un sorriso dolcissimo che sembrava un bambino! Logicamente da donna italiana dal carattere latino, ho cercato di fargli capire che mi aveva colpito, ma **lui sembrava non capire**. Alla fine sono stati gli amici in comune a fargli aprire gli occhi e cosi una sera mi ha invitato ad uscire e da li è iniziato tutto.




 La nostra storia é stata molto complicata, non c'é stato più permesso lavorare insieme e a volta avevamo anche ferie diverse, ma quando le nostre ferie combaciavano lui stava a casa mia, nonostante i miei grossi problemi familiari. **E' stato il primo e per ora l'unico a tener testa ai miei problemi**, è stato tenace, ci ha provato fino in fondo e in tutti i modi.




 Il fatto che poi questi problemi abbiano causato la fine della nostra storia è stato inevitabile, una cosa piu grande di noi. Io non potevo andare in Lituania da lui nonostante i suoi continui inviti e lui non avrebbe potuto stabilirsi da me.




 **Ricorderò per sempre il suo carattere maturo e dolce allo stesso tempo**... di quando la sera ci siedevamo sul divano in famiglia a guardare i cartoni animati russi che portava lui come ad esempio Mike Pukuotukas o Nu Pogodi che come dice lui sono molto piu istruttivi per i bambini che quelli che si vedono oggi alla tv; Ricordo quando mi aiutava a fare shopping e abbinare i vestiti, cosa per la quale sono totalmente negata, o quando in casa mia era lui a preparare la cena per tutti e.... altre milioni di piccole cose.




 Forse, ed é vero, si dimenticava di S.Valentino e se eravamo lontani non mi telefonava tutti i giorni, non mi mandava messaggi sdolcinati, ma **in modo indiretto mi dava tutto cio che desideravo**.




 Dopotutto oltre ai problemi familiari é stato anche per la mia gelosia che è finita, infatti lui non piaceva solo a me, ma anche alle sue amiche conpaesane che lavorano con noi, ed io come potevo competere con delle bellissime e intelligentissime ragazze lituane "predatrici"? Alla fine ho cominciato a credere che fosse freddo perché mi tradiva e gli facevo scenate di gelosia da brava italiana, proprio a lui che odia chi alza la voce, chi si arrabbia e chi litiga furiosamente. Alla fine mi rendevo conto che litigavo da sola.




 Anche se è finita io credo che lui resterà sempre l'uomo dei miei sogni che come tutti i sogni purtroppo finiscono.




