Title: Arrivederci Riga
Date: 2004-08-31
Category: Lettonia
Slug: arrivederci-riga
Author: Karim N Gorjux
Summary: Sono giunto al termine della mia vacanza esploratrice in Lettonia. Ero stato qui altre volte, ma mai da solo e mai per cosi' tanto tempo. Nei locali e per la strada riconosco[...]

Sono giunto al termine della mia vacanza esploratrice in Lettonia. Ero stato qui altre volte, ma mai da solo e mai per cosi' tanto tempo. Nei locali e per la strada riconosco persone che ho conosciuto e che mi sorridono, ormai mi sento un po' di casa. Dai primi giorni nei condomini della zona sovietica alle oziose giornate a provare a cucinare nel "mio" appartemto in old riga, di acqua sotto i ponti ne e' passata tanta.


 Stare qui in lettonia cosi' a stretto contatto con la gente e' stata un'esperienza irripetibile, al di la' del vivere come loro anche se solo per due settimane (che per me e' stato cosi' difficile da dovermi spostare in old riga), ho vissuto per 31 giorni a stretto contatto con i lettoni cercando di capire il loro stile di vita che credo di aver compreso e assaporato, ma tutto questo grazie a quelle due settimane vissute in periferia. Di come abbia influito Sarkandaugava nella mia considerazione sulla lettonia me ne sono accorto subito quando sono andato in discoteca. Le ragazza e i ragazzi sono tutti vestiti bene anche se ormai riconosco i vestiti "cheap" ovvero terranova e d&g (fac-simile), il tutto per dare un'idea di appartenere ad un ceto sociale diverso...anche se e' solo finzione. Ma cosa dovrebbero fare?

 Ho incontrato gente cordiale, come ad esempio l'autista che mi aiuto' alle 2 del mattino a prendere il bus giusto accompagnandomi pazientemente per indicarmi la fermata, ma ho anche incontrato gente molto chiusa e non molto disposta al dialogo: ad esempio la signora che non mi ha fatto fotografare il negozio o ieri una signora che ha preteso di passare davanti a me alla cassa del supermarket dicendo che e' malata. Tutte cose normali, le vediamo anche nella nostra italia. 

 Mi mancheranno tante cose: sicuramente tutte queste belle bionde, non se ne vedono molte in italia di donne belle cosi' e qui hai l'impressione di vedere una meraviglia della natura ad ogni angolo, sara' difficile abiturare l'occhio quando tornero', ma cio' rendera' piu' piacevole il ritorno (in lithuania... dave? ).... i sorrisi e gli sguardi sono qualcosa che fa' molto piacere. Qui le ragazze ti sorridono e ti guardano dritto negli occhi come non ho mai visto in Italia. Da provare! In controtendenza a cio' che ho scritto mi manchera' il Lido infatti domani a pranzo mangero' nell'aeroporto prima di prendere l'aereo, non e' il cibo piu' salutare del mondo, ma ogni tanto fa' piacere. Mi mancheranno tutti questi boschi e questo verde... mi manchera' anche Kristina che e' stata la mia guida per questo mese... ma ho voglia di tornare in Italia, sapendo che sara' semplice ritornare qui...


