Title: Partenza per l'Italia, novità e alcuni dubbi sul da farsi
Date: 2010-06-12
Category: Lituania
Slug: partenza-per-litalia-novità-e-alcuni-dubbi-sul-da-farsi
Author: Karim N Gorjux
Summary: Ci sarebbero alcuni argomenti di cui vorrei scrivere, ma li approfondirò in futuro. Intanto alcune novità: mia moglie ha finito l'università quindi il motivo principale per cui sono venuto qui è "scaduto"[...]

[![](http://www.karimblog.net/wp-content/uploads/2009/10/lietuva-flag.jpg "lietuva-flag")](http://www.karimblog.net/wp-content/uploads/2009/10/lietuva-flag.jpg)Ci sarebbero alcuni argomenti di cui vorrei scrivere, ma li approfondirò in futuro. Intanto alcune novità: mia moglie ha finito l'università quindi il motivo principale per cui sono venuto qui è "scaduto" e ora siamo liberi di fare le nostre scelte.


 **La seconda novità è che il 25 Giugno sarò in Itali**a. Ho fatto solo il volo di andata e userò RynAir in partenza da Kaunas per Milano. Torno in Italia dopo ben 22 mesi di assenza, sinceramente fatico a preparmi psicologicamente al rientro.  
  
La terza novità che non ho scritto sul blog è che mia moglie è al quinto mese di gravidanza e che **ad Ottobre sarò di nuovo padre**, ma questa volta di un maschietto. 

 Fatte queste premesse, quest'estate si deciderà bene cosa fare in futuro. Sia io che mia moglie siamo indecisi e non sappiamo bene cosa fare, ma in Lituania il lavoro scarseggia e le tasse aumentano quindi le difficoltà ci sono. F**osse per me andrei in Scandinavia evitando l'Italia se non per turismo**, ma la lingua non è un problema di poco conto. Rita sa l'Italiano e bene o male in Italia ho dei parenti, purtroppo però le notizie che leggo non sono confortanti e io sono sempre più restio a tornare.


 C'è da dire però che arrivati ad una certa età e con una famiglai, si fanno le scelte non per piacere, ma per i figli e mio chiedo **se un paese che sta distruggendo la scuola pubblica** sia una scelta valida per il futuro dei miei bambini.


