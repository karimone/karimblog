Title: Prima di addormentarmi, scrivo.
Date: 2005-04-11
Category: altro
Slug: prima-di-addormentarmi-scrivo
Author: Karim N Gorjux
Summary: Mente! Ne sono sicuro! Mente a se stesso, mente a tutti voi. Proprio voi che armati del silenzio gli permettete di continuare. Fatelo scrivere, e se sarete acuti, vi accorgerete di chi[...]

Mente! Ne sono sicuro! Mente a se stesso, mente a tutti voi. Proprio voi che armati del silenzio gli permettete di continuare. Fatelo scrivere, e se sarete acuti, vi accorgerete di chi è veramente.   
Tendi a blatterare, mugugni idiozie, amenità... lo fai con una certa soddisfazione, come se ti aiutasse a renderti vivo, migliore di chi ti circonda. Crei un'immagine di te che vorresti avere, ma non sopporti essere secondo, tu sei il perno, tutto è attorno a te. Vedi attraverso i tuoi occhi e credi di essere al centro. Hai creato questo mondo in cui vivi e solo nel in quel mondo la tua immagine ha un valore. Il tuo mondo è fatto di bugie, egoismo, superficialità, spavalderia; le qualità che ti rendono speciale ai più, ma entrambi sappiamo che in realtà non sei nulla. Ti nascondi perfino a te stesso e hai troppa paura di cercare nella tua anima chi sei veramente, hai troppa paura di conoscerti, ma i giorni passano e intanto ti avvicini verso la fine e sarai sempre la esile e patetica preda delle tue stesse visioni.


 Per poter dare un valore a te stesso, puoi solo sminuire gli altri. L'ultimo tentativo del miserabile prima dell'oblio. Ora io mi domando: *spoglio delle tue convinzioni, ti rimane un'anima che guida i tuoi pensieri attraverso le parole?*

