Title: Permesso di soggiorno per cittadini comunitari
Date: 2007-02-14
Category: Lituania
Slug: permesso-di-soggiorno-per-cittadini-comunitari
Author: Karim N Gorjux
Summary: Il titolo del post è chiaro. Mia moglie Rita ha la carta d'identità italiana, il codice fiscale, la residenza in italia, ma la cittadinanza lituana. Ovviamente da buona lituana non ne vuole[...]

Il titolo del post è chiaro. Mia moglie Rita ha la carta d'identità italiana, il codice fiscale, la residenza in italia, ma la cittadinanza lituana. Ovviamente da buona lituana **non ne vuole sapere nulla** di diventare italiana e quindi rimane pur sempre una straniera anche se comunitaria.


 Primo problema: **l'assistenza sanitaria**. Senza permesso di soggiorno (o carta di soggiorno, sono la stessa cosa), non mi rilasciano il tesserino sanitario.  
Secondo problema: **la divisione dei beni**. Ovviamente in Lituania il matrimonio è stato fatto, senza chiedermi nulla, con la comunione dei beni. Ora che divento pseudo imprenditore ho bisogno di fare la divisione dei beni prima di fondare la società, altrimenti mi ritrovo Rita a dove firmare dei documenti quando in società ci occupiamo di manutenzione straordinaria. Incredibile. E se da buona lituana non fosse d'accordo? :lol:

 Ore 8 del mattino, questura di Cuneo. Il funzionario addetto oltre al suicidio mi consiglia di aspettare, a breve una legge mi salverà le chiappe e il permesso di soggiorno non sarà più necessario. L'alternativa è fare una richiesta di permesso di soggiorno che può richiedere **sette mesi** prima di avere un esito positivo.


 Conclusione? La legge è stata pubblicata sulla [gazzetta ufficiale](http://www.gazzettaufficiale.it/) pochi giorni fa e a conferma di ciò il 20 Febbraio finalmente diventerò imprenditore. Con la divisione dei beni. **Non è più necessario il permesso di soggiorno per i cittadini comunitari.**

