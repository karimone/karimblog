Title: In Lituania non e' facile vivere ovunque
Date: 2015-12-13
Category: Lituania
Slug: in-lituania-non-e-facile-vivere-ovunque
Author: Karim N Gorjux
Summary: La mia prima esperienza lituana e' stata Klaipeda, l'ho vista cambiare negli anni e se devo essere sincero e' cambiata in peggio a causa della continua migrazione dei lituani.

La mia prima esperienza lituana e' stata Klaipeda, l'ho vista cambiare negli anni e se devo essere sincero e' cambiata in peggio a causa della continua migrazione dei lituani.


 In 10 anni Klaipeda e' passata da 186.000 abitanti a 157.000 abitanti circa, ma molti sono i lituani che vivono all'estero, ma non hanno rimosso la propria residenza. Sinceramente quando cammino per la citta', mi rendo conto che sta diventando sempre di piu' una citta' fantasma. Gia' il semplice fatto che **non incontro mai nessuno** delle persone che ho conosciuto negli anni passati, la dice lunga.


 Vilnius in completa controtendenza diventa l'anticamera della migrazione ed infatti la popolazione nella capitale aumenta. Sono stato a Vilnius per vari motivi in questi ultimi mesi e devo dire che e' veramente una citta' che merita di essere vissuta; ovviamente il discorso meteo non lo includo nella valutazione.


 Io attualmente mi trovo **temporaneamente** a Telsiai che e' piena campagna lituana nonostante la citta' faccia circa 30.000 abitanti. Ho gia' spostato due volte il mio ritorno a Melbourne e ringraziando lavoro online, nonostante debba fare una vita sregolata in quanto lavorando per una startup australiana devo anche regolarmi con i fusi orari.


 A parte questo, **la vita nella campagna lituana e' incredibilmente triste e noiosa**. Anche se hai dei soldi da spendere, qui in questi paesi non sai dove spenderli, se hai la macchina non sai dove andare. Il centro e' morto e la popolazione ha dei clamorosi buchi generazionali in quanto ci sono molti ragazzini teenager e tante persone oltre i 50 anni, ma la fascia dai 25 ai 40 anni e' praticamente inesistente.


 A breve dovrebbero arrivare i visti per Rita e i bimbi cosi' andiamo finalmente nell'altro emisfero. Io ovviamente partiro' il prima possibile, in teoria dovrei partire i primi di Gennaio, ma non sono sicuro se spostare ancora i biglietti o meno. :-(

 **Concludendo**: caro amico, se vuoi trasferirti in Lituania vai a Vilnius. Klaipeda non e' male, ma purtroppo e' cambiata troppo ed e' un peccato perche' la citta' e servita dall'aeroporto di Palanga che e' mantenuto in vita da AirBaltic e FlySas, ma Klaipeda sta perdendo davvero tanto rispetto agli anni passati. Per contro a Vilnius te la puoi cavare egregiamente con l'inglese, ci sono persone da tutta l'Europa e ci trovi pure qualche italiano e per fortuna, nel bene o male, c'e' sempre qualcosa da fare nonostante il freddo. Negli altri paesini i nativi lituani si alcolizzano per andare avanti, prova ad immaginare cosa significa per un italiano vivere in mezzo al nulla.


 Le statistiche le ho prese dal sito "istat" lituano: http://goo.gl/Spjhhy

