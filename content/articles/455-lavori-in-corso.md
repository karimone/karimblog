Title: Lavori in corso
Date: 2008-06-04
Category: Lituania
Slug: lavori-in-corso
Author: Karim N Gorjux
Summary: In questi giorni ho dovuto trovare delle soluzioni, molte soluzioni. A dire il vero ne sto ancora cercando, ma già sono contento di aver passato la fase "maronna quanti problemi!" ed essere[...]

In questi giorni ho dovuto trovare delle soluzioni, molte soluzioni. A dire il vero ne sto ancora cercando, ma già sono contento di aver passato la fase "maronna quanti problemi!" ed essere entrato nella fase "cerchiamo qualche soluzione".


 Ad Agosto sarò in Lituania, forse ci andremo in macchina perché dobbiamo portare parecchia roba di Rita con noi. Lei se ne rimarrà con Greta a Klaipeda mentre io me ne tornerò a casa. Nessun divorzio o litigio, ma solo l'ardente desiderio di Rita di finire l'università nel suo paese. Non è facile rinunciare ai propri sogni a 22 anni e la capisco, facciamo uno sforzo entrambi e stringiamo i denti per qualche mese.


 Che fare? Di rimanere su in Lituania non se ne parla! Un conto e andarci 15 giorni in vacanza, possibilmente senza legami, ma un conto è viverci, con una famiglia lavorando e svagando. Impossibile! Ho troppe scottature della mia precedente esperienza, non ho voglia di deprimermi ed ingrassare mangiando cepelinai.


 Riguardo all'ufficio penso di aver trovato una soluzione che dato il caldo e l'imminente partenza di Rita e Greta dovrebbe essere la soluzione ideale.


 Se vedrete più post pubblicati vuol dire che ho avuto un'idea geniale

