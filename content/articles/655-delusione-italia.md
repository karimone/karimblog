Title: Delusione Italia
Date: 2013-09-09
Category: altro
Slug: delusione-italia
Author: Karim N Gorjux
Summary: In questi giorni provo una certa delusione per l'Italia e la mia scelta di trasferirmi qui nel 2011.

In questi giorni provo una certa delusione per l'Italia e la mia scelta di trasferirmi qui nel 2011.


 Ha fatto molto scalpore la puntata di Presa Diretta del Lunedì 2 Settembre 2013. Già solo l'inizio di 5 minuti fa venire voglia di vomitare, lo spaccato in cifre della situazione italiana fa rabbrividire.


  La mia personale esperienza però non fa testo e non la voglio raccontare, sta di fatto che sotto certi aspetti lasciare il paese che mi ospitava è stata una buona cosa, sotto altri decisamente meno. **Purtroppo cosa mi fa più paura è il futuro, non tanto per me, ma per i miei figli**. Il benessere di tante persone che conosco sono appese ad un filo e molti devono ringraziare di essere saliti in piedi sulle spalle dei giganti, ma quanto può durare una situazione del genere? Quanto può cadere l'Italia nel baratro della crisi senza fare nulla di concreto per uscirne?

 Non c'è risposta, chi vivrà vedrà, ma dal 2008 ad oggi c'è stato solo un continuo peggioramento e la crisi sembra il "nulla" che inghiotte tutto come nel libro "La Storia Infinita".


 E' richiesta una buona dose di coraggio per intraprendere la prossima azione.


