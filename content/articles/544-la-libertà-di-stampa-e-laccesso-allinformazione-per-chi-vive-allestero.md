Title: La libertà di stampa e l'accesso all'informazione per chi vive all'estero
Date: 2009-05-25
Category: Lituania
Slug: la-libertà-di-stampa-e-laccesso-allinformazione-per-chi-vive-allestero
Author: Karim N Gorjux
Summary: Non seguo ne la politica e ne la cronaca lituana. Lo so, questo è un male, lo ammetto; in compenso però leggo molto su quanto succede in Italia. Mi informo usando internet,[...]

[![Libertà di stampa](http://www.karimblog.net/wp-content/uploads/2009/05/mappa_delle_liberta-300x197.png "Libertà di stampa")](http://www.karimblog.net/wp-content/uploads/2009/05/mappa_delle_liberta.png)Non seguo ne la politica e ne la cronaca lituana. Lo so, questo è un male, lo ammetto; in compenso però leggo molto su quanto succede in Italia. Mi informo usando internet, leggendo i blog e scaricando i quotidiani dalla rete. In questo articolo mi concentro su alcuni episodi italiani degli ultimi giorni che mi hanno un poco turbato.


 Riesco a scaricare vari quotidiani in rete e ti assicuro che leggo il giornale prima di quanto lo leggessi in Italia. Leggo il corriere della sera che scarico sul p2p, leggo la stampa di Cuneo a cui sono abbonato e riesco anche a mettere mano su una copia digitale della Gazzetta dello sport e della Settimana Enigmistica. E' tutta una cooperazione di residenti all'estero che si da una mano scambiandosi materiale digitale che, per certi giornali come ad esempio la gazzetta dello sport, dovrebbe essere obbligatorio la sola pubblicazione in digitale dato che le notizie sportive sono al 99% "fuffa" per vendere carta. Mettiti nei panni degli alberi, ti andrebbe di morire per far sapere agli italiani le ultime sul giocatore di calcio di turno?  
  
Leggo vari blog, tra cui quello del famoso [Beppe Grillo](http://www.beppegrillo.it) che anche se molti lo criticano e molti lo amano a dismisura, io lo ammiro come blogger e ne condivido parte dei pensieri. Mi è capitato qualche giorno addietro di leggere le [cinque domande](http://www.beppegrillo.it/2009/05/cinque_domande.html) poste al presidente della repubblica Giorgio Napolitano, lo spunto mi è piaciuto, ma cosa più mi ha sorpreso e leggere la risposta sul corriere. L'ho letta una volta, l'ho letta una seconda e alla terza ho rinunciato perché non ho capito nulla a parte il titolo. E' un brutto segno. Il titolo era chiaro, elogiava il nostro presidente Napolitano nel aver dato risposta ad un blogger, ma [l'articolo era incredibilmente ostico](http://www.beppegrillo.it/2009/05/la_bussola_escl.html) e privo di senso. Hai per caso letto l'articolo? Lo hai capito?

 Mia figlia da quando ha 11 mesi ha iniziato a chiamarmi "Papi"; sarai d'accordo con me che la [storia Noemi - Berlusconi](http://www.repubblica.it/2009/05/sezioni/politica/berlusconi-divorzio-2/parla-gino/parla-gino.html) mi mette un pochettino a disagio, anzi, a dire il vero mi ha letteralmente infastidito. La sensazione è la stessa di "Forza Italia" che dopo i mondiali di calcio del 1990 è stato un furto legittimato al tifo italiano. Ora la storia si ripete per i tanti veri "papi" italiani, soltanto che questa vicenda **sembra un riempitivo**, meglio parlare di questo che parlare di altre cose più serie.


 Avrà ragione la [freedom house](http://www.freedomhouse.org/template.cfm?page=470) ad aver classificato l'informazione italiana come parzialmente libera? Non sono l'unico ad [averlo notato](http://flaepitta.blogspot.com/2009/05/il-sorpasso.html), ma per fortuna internet è la risorsa essenziale per il vero pluralismo e una vera democrazia. A mio parere l'accesso ad internet dovrebbe essere una garanzia per chiunque perché l'accesso all'informazione è un diritto sacro santo di ogni cittadino. Mi chiedo però due cose: perché in Italia l'accesso ad internet è ancora così difficile? Perché in Italia si cerca di [imbavagliare internet](http://www.repubblica.it/2009/01/sezioni/tecnologia/internet-leggi/ddl-carlucci/ddl-carlucci.html)?

 Ecco un assaggio di come noi italiani all'estero andiamo in edicola:  
  
* La Stampa di Cuneo [[pdf](http://rapidshare.com/files/236927138/La_Stampa_Cuneo_2009-05-25.pdf)]
  
* La Stampa (Internazionale) [[pdf](http://rapidshare.com/files/236927137/La_Stampa_2009-05-25.pdf)]
  
* Il Corriere della sera [[pdf](http://rapidshare.com/files/236937121/Corriere_2009-05-25.pdf)]
  
* La Settimana Enigmistica n.4024 [[pdf](http://rapidshare.com/files/236585534/settimana_enigmistica_4024.pdf)]
  


