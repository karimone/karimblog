Title: E' meglio Riga o Klaipeda?
Date: 2005-01-22
Category: Lituania
Slug: e-meglio-riga-o-klaipeda
Author: Karim N Gorjux
Summary: In pochi giorni le cose possono cambiare radicalmente o rimanere tali, dato che per spiegare cosa è successo dovrei entrare in fatti molto personali, preferisco rimanere sul vago... molto sul vago. Intanto[...]

In pochi giorni le cose possono cambiare radicalmente o rimanere tali, dato che per spiegare cosa è successo dovrei entrare in fatti molto personali, preferisco rimanere sul vago... molto sul vago. Intanto posso dire di essere arrivato quasi ad una risposta alla domanda **"Ti piace più Riga o Klaipeda?", beh... ora come ora Klaipeda.**

 Sono arrivato a questa conclusione per vari motivi. Il primo perchè la città è piccola e meno dispersiva, mi ricorda Cuneo sotto molti aspetti, il secondo motivo perchè non vengo scambiato per turista sessuale ogni volta che apro bocca o guardo qualcuno. Il terzo perchè ho internet in casa 24h su 24 (godo). (Per la cronaca la banda non viene sprecata e stò scaricando film di interesse culturale con attori del calibro di Lino Banfi, non sò se mi spiego...) :-D

 A parte le nevicate e il freddo gelido, comincio ad orientarmi nella città e ad ampliare le mie conoscenze, ora sono sentimentalmente legato ad una persona quindi i soliti maliziosi che pensano e pensano e pensano usino il cervello per pensare ad altro, foto o altre informazioni le pubblicherò in un altro momento. Per ora sappiate che sono contento, soddisfatto e ho una piacevole convivenza con il Davide.


