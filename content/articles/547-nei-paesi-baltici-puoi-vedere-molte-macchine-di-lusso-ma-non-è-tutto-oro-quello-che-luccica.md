Title: Nei paesi baltici puoi vedere molte macchine di lusso, ma non è tutto oro quello che luccica
Date: 2009-06-05
Category: Lituania
Slug: nei-paesi-baltici-puoi-vedere-molte-macchine-di-lusso-ma-non-è-tutto-oro-quello-che-luccica
Author: Karim N Gorjux
Summary: Forse sarà la nomea di paese dell'est che la Lituania si trascina dietro, forse saranno tutti questi "blocchi" russi che danno un aspetto trasandato a varie vie del paese o forse sarà[...]

[![](http://farm3.static.flickr.com/2430/3594937366_b15f929a5f_m.jpg "Carro e cavallo")](http://www.flickr.com/photos/39283264@N00/3594937366/)Forse sarà la nomea di paese dell'est che la Lituania si trascina dietro, forse saranno tutti questi "blocchi" russi che danno un aspetto trasandato a varie vie del paese o forse sarà che non ho mai visto tante [Lexus](http://www.lexus.it/) in Italia. Sarà quello che vuoi, ma a me tutte ste Lexus che girano per la Lituania mi hanno sempre stupito. Costano di meno? Le regalano nei [Kinder sorpresa](http://www.magic-kinder.com/mk2008/IT_it/html/home.htm)?  
  
La Lexus, se non lo sapessi, è la linea di lusso delle auto Toyota. In Italia abbiamo Mercedes e BMW che la fanno da padrone, mentre qui lo status symbol per eccellenza è avere una Lexus nuova sotto le chiappe.


 All'inizio vedere tutte queste macchine mi ha sconvolto un poco, sembra quasi che qui in Lituania siano tutti ricchi e che avere una macchina vecchia sia una cosa di cui vergognarsi. In realtà è tutta immagine.  
Nel 2005, quando sono venuto qui a Klaipeda per la prima volta, le macchine vecchie erano molte di più, adesso ci sono molte macchine nuove soprattutto in centro città e tutto questo è avvenuto grazie agli anni dell'ottimismo e del debito facile per i paesi baltici.


 Ora la situazione è paradossale. Il mio padrone di casa è un giovane ragazzo di 26 anni. Non riesce a vendere l'appartamento perché il crollo dei prezzi degli immobili unito alla crisi economica, non solo non permette di vendere al prezzo di realizzo, ma ha persino eliminato la possibilità della vendita vista la situazione bancaria attuale.  
Pago le spese di riscaldamento, energia elettrica e spese condominiali che sono intestate al mio padrone di casa; ogni mese paghiamo, ma lui ogni mese si tiene i soldi in tasca e non paga. Dato che tutto è a nome suo, il problema non si pone, ma vuoi sapere con che macchina gira la moglie? Un bel BMW nuovo fiammante.


 Ho anche sentito altri casi drammatici di "pseudo imprenditori" che hanno la Lexus nuova, ma non hanno i mobili in casa o faticano a fare la spesa al supermarket. Persone che ho conosciuto, che vivono in campagna e hanno il cesso in cortile e tutta la casa da riparare, preferiscono fare il leasing per la macchina nuova invece di investire nella riparazione della macchina. Sono situazioni assurde e in alcuni casi drammatiche.


 Io la mia esperienza l'ho avuta con le auto e penso che l'auto sia solo un mezzo ingombrante, scomodo, ma qualche volta utile. Se proprio vuoi ostentare ricchezza, non ti serve la macchina ultimo modello o il cellulare di ultima generazione. I veri ricchi hanno l'autista, la barca e la segretaria. Tutto il resto è una guerra tra poveri.


 Link: Stampa di [Cuneo](http://rapidshare.com/files/240980336/La_Stampa_Cuneo_2009-06-05.pdf) e [Internazionale](http://rapidshare.com/files/240980337/La_Stampa_2009-06-05.pdf)

