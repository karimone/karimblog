Title: Gli italiani non sanno l'inglese, purtroppo è un dato di fatto. In Lituania i bambini parlano tre lingue
Date: 2009-04-02
Category: Lituania
Slug: gli-italiani-non-sanno-linglese-purtroppo-è-un-dato-di-fatto-in-lituania-i-bambini-parlano-tre-lingue
Author: Karim N Gorjux
Summary: Bene o male i miei amici se la sono sempre cavata con i viaggi all'estero. Hanno organizzato le vacanze di gruppo e, anche senza sapere più di dieci parole d'inglese, se la[...]

[![We Spik English](http://www.karimblog.net/wp-content/uploads/2009/04/spik-english-300x203.jpg "We Spik English")](http://www.karimblog.net/wp-content/uploads/2009/04/spik-english.jpg)Bene o male i miei amici se la sono sempre cavata con i viaggi all'estero. Hanno organizzato le vacanze di gruppo e, anche senza sapere più di dieci parole d'inglese, se la sono sempre cavata.


 Quando invito qualche mio amico a visitarmi a Klaipeda, il problema principale è la lingua. La maggior parte dei miei amici non conosce più di dieci d'inglese. We escludiamo chi l'ha studiato bene sia alle superiori che all'università, un paio di amici e non più, tutti gli altri non sanno fare una conversazione base che vada l'oltre "ciao, come stai?", "Bene. Grazie".  
  
Purtroppo non sapere l'inglese è un grossissimo handicap per chi vuole emergere in questo mondo dove la qualità è ormai l'unico vero pregio. L'inglese permette di sconfinare e imparare cose nuove al passo con i tempi. Non voglio incolpare tanto i ragazzi, ma più che altro voglio puntare il dito sulla scuola. A cosa servivano **due ore alla settimana di inglese** alle superiori? A nulla. Per essere davvero valido l'insegnamento dell'inglese, sarebbe stato **appena sufficiente** un'ora al giorno. 

 **Può andare peggio di così?  
**Assolutamente si. Ultimamente vado a leggere le cazzate che i ragazzi scrivono su [facebook](http://www.facebook.com) e ho notato che anche l'italiano è messo male. I teenagers italiani hanno completamente stravolto la lingua italiana non solo usando "k", "x", "cmq" e altre abbreviazioni oscene, ma omettendo persino la punteggiatura base. Stendiamo, infine, un velo pietoso sugli errori grammaticali. Ho letto frasi indecenti dove una parola soltanto riusciva a contenere due o più errori.


 **Com'è la situazione dalle mie parti?  
**In Lituania i bambini di 10 anni parlano correttamente l'inglese. Lo posso garantire di persona perché ho conosciuto molti bambini e tutti hanno intavolato con me discussioni "di un certo livello" usando un inglese fluido e corredato da una pronuncia invidiabile.


 Il passato recente che ha coinvolto questo paese, ha fatto si che molti di questi ragazzi non solo conoscessero l'inglese e il lituano, ma anche il russo. I bambini vanno a vedere i cartoni animati al cinema dove non esiste doppiaggio, ma solo la lingua originale con i sottotitoli. La TV lituana trasmette i film doppiati (malamente), ma molti programmi televisivi sono ancora in russo. In estonia invece il doppiaggio non esiste, tutti i film stranieri vengono solamente sottotitolati in estone.


 **Cosa ci aspetta il futuro?  
**Lascio a voi una breve analisi. In Italia i ragazzi non sanno l'italiano, pensa un po se sapranno mai l'inglese. All'estero i ragazzi conoscono **minimo** due lingue e in gran parte dell'Europa ne parlano persino tre. I nostri ragazzi sono il futuro dell'Italia, ma ci sarà mai un futuro?

