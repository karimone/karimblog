Title: Integrazione nella scuola australiana per nuovi studenti senza la conoscenza dell'inglese
Date: 2016-05-11
Category: Australia
Slug: integrazione-nella-scuola-australiana-per-nuovi-studenti-senza-la-conoscenza-dellinglese
Author: Karim N Gorjux
Summary: Tomas e Greta, appena arrivati in Australia, li ho iscritti alla scuola per studenti stranieri. Essendo loro qui con il visto da residenti hanno diritto fino a 12 mesi di scuola gratuita.

Tomas e Greta, appena arrivati in Australia, li ho iscritti alla scuola per studenti stranieri. Essendo loro qui con il visto da residenti hanno diritto fino a 12 mesi di scuola gratuita.


  [La scuola](http://nobleparkels.vic.edu.au) si trova a Noble Park, nella zona sud est di Melbourne a circa 10km da dove viviamo noi. La cosa stupefacente che si puo' leggere anche sul loro sito e':  

> We provide an excellent intensive English language program and assist students and their families in their transition to a new country.  
Gli studenti sono di diverse nazionalita' per lo piu' in arrivo da paesi "sensibili", molti dei compagni di classe di Greta e Tomas sono afgani ed infatti Greta se ne torna a casa con qualche parola di arabo che sente dire dalle sue amiche.


 Ieri abbiamo avuto il primo incontro con il direttivo della scuola. Circa un'ora prima della fine delle lezioni, ci siamo trovati nella sala professori con altri genitori, la direttrice, l'interprete e l'assistente della scuola. Ci hanno offerto dolci e da bere ed hanno iniziato a spiegarci alcune cose su come funziona la scuola in Australia e nella loro scuola. Mi spiace non aver preso degli appunti, ma la riunione e' stata solo ieri quindi non dovrei dimenticare nulla. Cosa mi ha impressionato era la presenza dell'interprete in arabo.


 I bambini vengono seguiti come nella normale scuola australiana facendo lezioni ordinarie, ma con il preciso compito di intensificare il supporto linguistico. Una volta che sanno la lingua verremo contattati da un insegnante di supporto che ci guidera' alla scelta della scuola vicino a casa nostra e si mettera' in contatto con la scuola per dare informazioni sullo studente e per accoglierlo/a nel miglior modo possibile. In sostanza lo studente non viene inserito con bimbi piu' piccoli e **non viene lasciato indietro.**

 La scuola australiana si alterna con 12 settimane di scuola e 2 settimane di vacanza ad esclusione delle vacanze estive che durano 5/6 settimane ovvero Dicembre/Gennaio.


 Per qualsiasi problema e' possibile parlare con gli incaricati **qualsiasi giorno sia durante che dopo le lezioni**, non e' necessario un appuntamento formale, ma e' caldamente consigliato parlare subito di eventuali problemi non solo a scuola ma anche a casa.


 Dato che molti genitori sono appena arrivati in Australia e hanno delle difficolta' ad integrarsi l'assistente e' disponibile ad aiutare i genitori contattando il Centrelink ed ottenere il supporto di cui hanno bisogno.


 La prossima settimana ci sara' un incontro dove una psicologa spiega come gestire i bambini a casa, la settimana dopo invece ci sara' un'incontro su come funziona la scuola in Australia.


 **E' stato chiesto espressamente** di continuare a parlare, leggere e scrivere nella lingua madre al bambino in modo di non fargli perdere la lingua e la sua cultura. Questo punto e' stato ripetuto piu' volte ed e' stato sottolineato come molto importante per la crescita del bambino.


 Ci sono un paio di cose che mi hanno impressionato. La prima e' che la scuola in Australia e' presa in considerazione e finanziata al meglio: aree informatiche, laptops, lavagne elettroniche, parchi giochi. Non e' la migliore delle scuole, ma non c'e' nulla di cui lamentarsi. La seconda e' che il sistema educativo e' piu' rilassato e molto meno stressante di cosa ho potuto vedere personalmente in Italia sia da studente che da genitore.


 Siamo usciti da quella riunione con il sorriso e tutto mentre nella finanziaria australia 2016 e' stato annunciato un aumento dei fondi alla scuola pubblica di 1.2 MILIARDI di dollari australiani [[link](http://www.theaustralian.com.au/budget-2016/budget-2016-extra-12b-for-schools-to-be-announced-in-tuesdays-speech/news-story/d6b85d0aa3956aef941d2ca430508f1a)]

  

