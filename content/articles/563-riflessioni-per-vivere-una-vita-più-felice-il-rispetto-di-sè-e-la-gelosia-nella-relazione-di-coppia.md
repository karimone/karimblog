Title: Riflessioni per vivere una vita più felice: il rispetto di sè e la gelosia nella relazione di coppia
Date: 2009-09-06
Category: Lituania
Slug: riflessioni-per-vivere-una-vita-più-felice-il-rispetto-di-sè-e-la-gelosia-nella-relazione-di-coppia
Author: Karim N Gorjux
Summary: Molti mi chiedono di scrivere riguardo alle donne lituane, ma dato che io ho una moglie e conosco varie ragazze, non posso parlare con un esperto seduttore, ma posso dire la mia[...]

![](http://farm3.static.flickr.com/2425/3893504022_da943be5e1_m.jpg "La gelosia")Molti mi chiedono di scrivere riguardo alle donne lituane, ma dato che io ho una moglie e conosco varie ragazze, non posso parlare con un esperto seduttore, ma posso dire la mia opinione data l'esperienza e le mie riflessioni.


 Sono convinto che sia importante lavorare su se stessi prima di puntare il dito sul gentil sesso e trovare scuse, difetti e incongruenze nel modo di agire e di pensare delle donne in generale. Avevo già parlato dei [legami di coppia](http://www.karimblog.net/2009/07/28/legami-di-coppia-alcuni-miei-spunti-personali-scritti-in-base-alle-mie-esperienze-e-alle-mie-letture/), ma le riflessioni contenute in questo articolo sono unisex, possono andare bene sia per uomini che per le donne anche se mi limiterò ad esempi che ho potuto constatare di persona.  
  
Il primo punto a cui non bisogna mai scendere a patti è il **rispetto di se stessi**. Se ti sei già innamorato in passato o hai vissuto un'esperienza che ti sembrava innamoramento, ti sarai accorto che nella maggior parte dei casi si tende a chiudere un occhio, se non entrambi, su vari aspetti della relazione. Ci sono tante cose a cui si può chiudere un occhio, la tua fidanzata ha [3 tette](http://thewolfweb.com/photos/00327092.jpg)? Fa nulla! Ha l'alito che puzza? Esistono le mentine! E' un cesso immane? De gustibus non est disputandum. Se però la tua fidanzata (o moglie) ti porta in condizioni di mettere da parte la tua dignità pur di continuare la relazione allora è giusto rivalutare obiettivamente la situazione.


 Le menzogne, gli insulti, la violenza psichica o fisica vengono accettate comunemente in moltissime coppie invece di "rompere" come si dovrebbe. Ma perché queste situazioni che sono viste da occhi esterni come tragiche e grottesche, continuano per anni prima di avere l'epilogo più sensato della scissione della coppia? 

 Io penso e qui sottolineo che è solamente una mia opinione, che **molte persone delegano la propria felicità ad altri** o a fattori esterni. Qualche giorno fa ho visto il film [Into the Wild](http://www.imdb.com/title/tt0758758/), un film stupendo dove John McCandless, un ragazzo di 24 anni, rimane 100 giorni circa in un bus abbandonato nel bel mezzo del parco naturale del [Denali](http://it.wikipedia.org/wiki/Borough_di_Denali) in Alaska.  
Ho pensato a John per vari giorni; pensavo che dopo 3 mesi nel bel mezzo dell'Alaska, in piena solitudine vivendo di caccia e pesca in mezzo alla natura, e riuscendo persino a trovare la felicità in tutto questo, allora John era davvero pronto a tutto soprattutto era pronto per riuscire a trovare la persona giusta con cui vivere perché la sua felicità era già in lui e non dipendeva da nessun altro. Il concetto cardine è questo. La relazione di coppia è formata da **due persone che decidono di vivere la propria vita insieme** e non da due mezze persone che vivono una sola vita.


 Se non sei ricattato dalle tue paure e se non hai basato tutto il tuo equilibrio psicofisico sulle tette della fidanzata allora sei già a buon punto per instaurare un rapporto che preserverà la tua dignità di uomo.


 Un altro aspetto logorante ed inutile nel rapporto di coppia è il tradimento e tutte le seghe mentali ad esso annesse. Tempo fa in una discussione con una persona di 10 anni più di me è emerso il fatto che io non limito mia moglie nell'uscire e nella gente che frequenta. Non dico che può fare quello che vuole e io rimango sempre a sorridere come un deficiente, voglio solo dire che per prima cosa **non spreco il mio tempo a fare il carabiniere** controllando messaggi sul cellulare, email o controllando dove e con chi va. Ho fiducia di mia moglie altrimenti non starei con lei e sono convinto che il vero amore risieda nella **libertà di scelta**.


 Mia moglie ogni tanto esce in discoteca e io rimango a casa con la bimba. Se sei un uomo starai sicuramente storcendo il naso, se sei una donna invece starai sicuramente invidiando Rita. Ebbene qui a Klaipeda non abbiamo parenti che ci possono guardare Greta quindi solo raramente possiamo uscire insieme come se fossimo ancora fidanzati: se vogliamo fare serata non possiamo uscire insieme, sembra quasi la storia di [ladyhawke](http://www.imdb.com/title/tt0089457/). Ovviamente viviamo in perfetta *par condicio*, anche io esco e vado a ballare con gli amici e torno all'ora che mi pare. Anche io ho libertà di fare ciò che voglio e fare le scelte che mi sembrano più opportune in ogni momento della giornata. Ed è giusto che sia così perché il naturale corso di una relazione è **una scelta quotidiana che deve essere intrapresa in piena libertà**.


 Alcuni anni fa una mia ex fidanzata mi ha tradito in modo meschino e subdolo. A suo tempo è stata una brutta botta dato che erano già tre anni che stavamo insieme però l'esperienza mi ha fatto capire che in quei tre anni le mie limitazioni imposte e le mie incazzature da geloso non hanno portato a nulla, anzi tutt'altro, hanno fatto in modo che il rapporto si logorasse sempre di più. Da allora ho capito che essere gelosi è come prendersela se piove o se nevica, non serve a nulla. Se davvero c'è la possibilità di essere traditi dalla fidanzata o dalla moglie è importante che succeda subito in modo da non avere dubbi e troncare la relazione. Che senso ha cercare di vincolare le libertà una persona che ti vuole tradire per tenersela al proprio fianco il più possibile?

 Concludo con una piccola testimonianza. Ho conosciuto una signora madre di un bimbo avuto con un italiano. Purtroppo lei ha lasciato lui ed è tornata a vivere a Klaipeda perché l'italiano la tormentava quotidianamente, non solo non le permetteva di uscire, ma litigava anche se gli altri uomini la guardavano. Che cosa ha ottenuto? Di essere da solo in Italia e di vedere il bambino "a visite".


 Cosa ne pensi?

