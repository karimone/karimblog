Title: Risvegli all'alba senza ritorno
Date: 2007-10-12
Category: altro
Slug: risvegli-allalba-senza-ritorno
Author: Karim N Gorjux
Summary: Se Greta non mangia abbastanza la sera, succede che al mattino, verso le 5, si sveglia e non riesce più ad addormentarsi. Rita è costretta ad alzarsi e a prepararle un biberon[...]

Se Greta non mangia abbastanza la sera, succede che al mattino, verso le 5, si sveglia e non riesce più ad addormentarsi. Rita è costretta ad alzarsi e a prepararle un biberon di latte, nel mentre, prendo la bimba e la tengo vicino a me nel lettone. Una volta che Greta si è scolata il suo biberon, si addormenta.


  
Purtroppo si addormentano solo Greta e Rita, il più delle volte non riesco ad addormentarmi. Mi giro nel letto, tengo gli occhi chiusi, ma alla fine mi alzo. Vado in cucina, apro il computer, mi preparo la colazione e nel mentre affogo due biscotti, aggiorno il mio blog.


  
Potessi, dormirei fino alle 10.


  
  
 [![CIMG6554.JPG](http://farm3.static.flickr.com/2121/1544547905_e66fae2bdb_m.jpg)](http://www.flickr.com/photos/kmen-org/1544547905/ "Condivisione di foto")  
  


