Title: Somewhere Over The Rainbow
Date: 2007-04-14
Category: altro
Slug: somewhere-over-the-rainbow
Author: Karim N Gorjux
Summary: "Somewhere over the rainbow" è la famosa canzone cantata da Judy Garland nel Mago di OZ (1939) è una canzone stupenda ripresa da decine di artisti.Una versione in particolare tra le tante[...]

"Somewhere over the rainbow" è la famosa canzone cantata da [Judy Garland](http://it.wikipedia.org/wiki/Judy_Garland) nel [Mago di OZ ](http://it.wikipedia.org/wiki/Il_mago_di_Oz)(1939) è una canzone stupenda ripresa da decine di artisti.  
  
Una versione in particolare tra le tante mi ha sempre colpito per la sua semplicità e l'emozioni che riusciva a darmi, ka versione di [Israel Kamakawiwo'ole](http://it.wikipedia.org/wiki/Israel_Kamakawiwo%27ole) suonata con il semplice accompagnamento [dell'ukulele](http://it.wikipedia.org/wiki/Ukulele).


 Israle "fratello IZ" a causa della difficile pronunciabilità del suo cognome era un hawaiano puro sangue che nella sua breve vita ha lottato per i diritti del suo popolo, si è sposato, ha avuto una figlia, ma ha anche avuto terribili problemi di peso che l'hanno portato alla morte prematura a 38 anni.


 Oltre al video della sua stupenda interpretazione (con le immagini dello spargimento delle ceneri nel finale), vi consiglio di leggere il [testo](http://www.stlyrics.com/songs/i/israelkamakawiwoole6157/somewhereovertherainbow239745.html) della canzone e dare un'occhiata alla [testimonianza](http://www.fortunecity.com/victorian/tollington/72/iz.htm) di una persona che ha conosciuto "Il gigante buono" IZ.  
  
  


