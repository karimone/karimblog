Title: Il mio più grande incubo in Italia: la cassetta della posta
Date: 2009-07-18
Category: Lituania
Slug: il-mio-più-grande-incubo-in-italia-la-cassetta-della-posta
Author: Karim N Gorjux
Summary: Ieri pomeriggio mi sono ritrovato a fare una gradevole chiacchierata su skype con un mio compaesano. Quasi coetanei, eravamo entrambi in ufficio a lavorare con il computer, lui a Milano in una[...]

[![](http://farm3.static.flickr.com/2128/2235743227_c956dde960_m.jpg "Una cassetta delle lettere")](http://www.flickr.com/photos/franklin_hunting/2235743227/)Ieri pomeriggio mi sono ritrovato a fare una gradevole chiacchierata su skype con un mio compaesano. Quasi coetanei, eravamo entrambi in ufficio a lavorare con il computer, lui a Milano in una grossa ditta, io a Klaipeda in un piccolo ufficio.


 Scrivendo del più e del meno sono emersi i soliti argomenti che mi vengono posti in queste ultime settimane: non torni in Italia? starai sempre li? cosa ti manca? come si sta in Lituania etc. etc.  
  
Io qui sto bene. Partire ora e tornarmene in Italia sarebbe uno shock, dovrei affrontare tutte quelle magagne tutte italiane che qui in Lituania non esistono e detto sinceramente non ne ho la forza fisica e morale. Ci sono degli elementi che mi spingerebbero tornare indietro, ma l'Italia non è il paese che ti permette di vivere facilmente. Tutto costa molto caro e la burocrazia italiana è una spina sul fianco difficile da digerire. 

 Dal titolo del post avrai capito che il mio incubo peggiore in Italia è stata la cassetta della posta. Quando mi trovavo in Italia abitavo in un bell'appartamento ammezzato, dalla porta d'ingresso alla cassetta delle lettere mi dividevano ben 6 scalini e tutti i giorni alle 13 quegli scalini diventavono un piccolo assaggio d'inferno. 

 Dopo il passaggio della postina andavo a prendere la posta terrorizzato, chi mi cerca ora? Cosa vuole? Quanto dovrò pagare? C'era sempre qualche sorpresa, qualche stronzata da pagare, la mia impressione è che le persone comuni in Italia sono viste come limoni da spremere fino all'ultima goccia. Lettere dalla banca, bollette della luce, bollette del gas, la mitica bolletta del telefono con canone-pizzo telecom, la lettera della GEC che ti chiede il bollo auto di una macchina che hai avuto 15 anni prima, le spese condominiali, l'immondizia, l'inps, l'inail etc. etc. etc.


 **In Lituania** ci sono le spese condominiali, l'acqua e il gas, ma è tutto più semplice. **Paghi esattamente quello che utilizzi** e non hai imposte per ogni cosa. Non ricevo mai una lettera dalla banca, questo è fantastico, uso solo l'internet banking e se la banca vuole chiedermi qualcosa o dirmi qualcosa mi contatta via email. Internet mi costa come il canone telecom e non ho il numero di telefono, le bollette gas, acqua sono in base a ciò che realmente consumiamo e hanno cadenza mensile. In pratica via posta non ricevo mai nulla, solo ogni tanto l'avviso di un pacco da ritirare alla posta centrale.


 La Lituania non è il miglior paese d'europa, sicuramente la mentalità della gente non è delle più istruite, ma accostando le differenze con il bel paese si nota chiaramente che l'Italia è il paese dei furbi.


