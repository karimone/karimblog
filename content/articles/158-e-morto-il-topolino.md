Title: E' morto il topolino
Date: 2006-03-23
Category: Lituania
Slug: e-morto-il-topolino
Author: Karim N Gorjux
Summary: Azzolina... è mattina presto! Rita è andata all'università, io sono qui a scrivere questo bel post, domani prendo l'aereo e torno a casa. Data di ritorno a Klaipeda: 29 Aprile 2006.

Azzolina... è mattina presto! Rita è andata all'università, io sono qui a scrivere questo bel post, domani prendo l'aereo e torno a casa. Data di ritorno a Klaipeda: 29 Aprile 2006.  


 Ste cacchio di case comuniste hanno più di 50 anni e purtroppo non ci vivono solo uomini, ma anche topi. Non è che sia una cosa sconvolgete, il topolino in questione era grigio chiaro grande 1,5x2,5cm coda esclusa. Ogni tanto capitava di sentirlo di notte quando c'era il silenzio assoluto, non ha mai fatto male a nessuno, ma purtroppo, quando se ne ha la possibilità, bisogna sfrattarlo.


 Ieri sera ero in camera da letto e sento gridare Rita dal soggiorno. *"Ho visto il topo!"* La caccia è durata più di mezz'ora, il topo era velocissimo e furbissimo, se spostavi un divano, lui si spostava con esso e ci rimaneva sotto. Sono riuscito, con la complicità di Rita, a chiuderlo verso il muro che dà sulla strada. Correva contro il battiscopa all'impazzata per poi sparire dietro il termosifone, quando siamo riusciti a prenderlo quel povero topino aveva la due scope sulla pancia che lo tenevano fermo, purtroppo la situazione è stata fatale ed è morto.


 A dire il vero mi è spiaciuto. Rita, che in certi frangenti mi ricorda una bambina di 6 anni, aveva gli occhi lucidi. Un topino morto fa più compassione che schifo, sarà che sono cresciuto guardando *Tom & Jerry*, sarà che il film [Un topolino sotto sfratto](http://italian.imdb.com/title/tt0119715/?fr=c2l0ZT1pdHx0dD0xfGZiPXV8cG49MHxrdz0xfHE9dW4gdG9wb2xpbm8gc290dG8gc2ZyYXR0b3xmdD0xfG14PTIwfGxtPTUwMHxjbz0xfGh0bWw9MXxubT0x;fc=1;ft=1) mi ha fatto scompisciare dal ridere ogni volta che lo guardavo, ma vedere quel topo morto a pancia in giù con le zampette divaricate mi ha fatto una pena incredibile. Sfido voi a trovare un topo in casa e sbarazzarvene senza ammazzarlo! E' impossibile!  
  
  
adsense  
  


