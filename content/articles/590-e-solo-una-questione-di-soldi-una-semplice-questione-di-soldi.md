Title: E' solo una questione di soldi. Una semplice questione di soldi
Date: 2010-02-17
Category: altro
Slug: e-solo-una-questione-di-soldi-una-semplice-questione-di-soldi
Author: Karim N Gorjux
Summary: 

[![](http://farm4.static.flickr.com/3210/3035489052_7a57df634d_m.jpg "Without Money")](http://www.flickr.com/photos/tobanblack/3035489052/)

 Da quando ho messo il form di contatto (funzionante), ricevo svariati contatti su informazioni sulla Lituania, vivere qui, trovare lavoro e quant'altro. Non ho molte risposte da dare, ma soprattutto non mi prendo nessuna responsabilità.


 Qui in Lituania lavoro non si trova per i lituani, figuriamoci per gli italiani che sanno a malapena l'inglese. Il mio caso è molto particolare perché io mi sono creato un lavoro che funziona su internet e **non ha molta importanza se mi trovo in Lituania o sull'isola di Lost**, l'unica cosa che a me importa è avere una connessione ad Internet.


 Sempre grazie ad internet riesco a vedere i programmi della RAI che più mi interessano. Ho un Mac Mini collegato alla televisione che scarica continuamente le registrazioni dei miei programmi preferiti usando il servizio gratuito di [vcast](http://www.vcast.it/login/login.php) e quindi riesco a vedermi il TG del Piemonte, Blog, Ballarò, Annozero, Ulisse, Presa Diretta...etc etc il tutto saltando a piè pari la pubblicità e senza dover per forza stare davanti alla TV.


 Giusto ieri sera mi sono guardato [Presa Diretta](http://www.presadiretta.rai.it/dl/portali/site/articolo/ContentItem-304de3ed-bbd9-4afb-bd57-da84637dde70.html?homepage "Sito ufficiale di Presa Diretta"), la puntata riguardava la scuola. In poche parole il servizio mostrava come la scuola, sia al nord che al sud, sia ostaggio dei pochi soldi che il governo ha stanziato per l'istruzione. In pratica è una continua mancanza di strumenti, di personale e di fondi per mantenere la scuola un posto sicuro e agevole per i ragazzi.


 La situazione è semplicemente drammatica, ma se hai i soldi, il servizio ti fa entrare nelle scuole paritarie (ovvero private) e ti mostra come sarebbe bello per tuo figlio avere un'istruzione di Seria A: **lezioni in due lingue** (inglese e italiano) e non quelle due ore delle palle alla settimana che non servono a nulla, lavagna elettronica, banchi nuovi, piscina e palestra, mensa, cortile, lezioni di informatica quotidiane etc. etc. il tutto per una retta che si aggira sui 1000€ al mese.


 Vito Scafiti aveva 17 anni, è [morto nel suo liceo a Rivoli](http://roma.indymedia.org/node/6223) in provincia di Torino. Durante una lezione il tetto è crollato e lui è morto, il suo compagno di banco invece è sulla sedia a rotelle. Forse l'unica colpa dei genitori di Vito è di non aver avuto abbastanza soldi per mandare loro figlio in una scuola privata?

 Mentre guardavo e facevo vedere a Rita il servizio, pensavo a come l'Italia sia diventato un paradiso per pochi e soprattutto come la classe politica italiana sia ben lontana dalla realtà di tutti i giorni. Si parla di [riforme della scuola](http://www.youtube.com/watch?v=ol8Dk8WIZ-o) come se bastassa cambiare i nomi ai licei per fare qualcosa di nuovo è innovativo, mentre il problema sono i soldi e **una buone dose di onestà ad ogni passaggio di mano mentre sti benedetti soldi intraprendono il viaggio verso la scuola**.


 Se i tuoi figli andranno alla scuola pubblica e dovrai sperare che abbiano **culo** nel trovare professori giusti ed avere una scuola con dei mezzi, altrimenti saranno rassegnati ad avere in **teoria** una buona preparazione, ma in pratica nulla.


 Alla fine **è solo un discorso di soldi**, se ne hai tanti te ne sbatti le palle e cosa non hai per diritto lo ottieni senza problemi, vita migliore per te e per chi ti sta vicino. Soldi, soldi e soldi, nient'altro. **A volte vorrei davvero andare a vivere nell'isola di Lost, ma senza portarmi il computer dietro.**

