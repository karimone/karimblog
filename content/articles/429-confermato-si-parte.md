Title: Confermato. Si parte!
Date: 2008-03-20
Category: altro
Slug: confermato-si-parte
Author: Karim N Gorjux
Summary: Domenica alle 13:30 partirò da Malpensa, nove ore e mezza dopo atterrerò nella Repubblica Dominicana. Se tutto va bene sarò di ritorno per il 2 o il 3 Aprile.Perché non ho voglia[...]

Domenica alle 13:30 partirò da Malpensa, nove ore e mezza dopo atterrerò nella Repubblica Dominicana. Se tutto va bene sarò di ritorno per il 2 o il 3 Aprile.  
Perché non ho voglia di andarci? Semplicemente perché penso che le cose belle bisogna condividerle con qualcuno di speciale e Rita e Greta non ci sono. Peccato!

 Sembra che ai Caraibi il rischio di annoiarsi è grande, fortunatamente avrò un lavoro da fare altrimenti sarebbe una vera palla. Come farò, invece, a far passare le 10 ore di viaggio?

 Aiuto!

