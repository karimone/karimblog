Title: Uno scorcio di Lituania tratta dal libro "Il compagno Don Camillo"
Date: 2011-07-14
Category: Lituania
Slug: uno-scorcio-di-lituania-tratta-dal-libro-il-compagno-don-camillo
Author: Karim N Gorjux
Summary: Ho sempre letto con piacere i libri di Giovannino Guareschi su Peppone e Don Camillo, nel libro "Il compagno Don Camillo" mi sono imbattuto in una descrizione della campagna Ucraina che mi[...]

[![](http://www.karimblog.net/wp-content/uploads/2011/07/ilcompagnodoncamillo-213x300.jpg "Il compagno Don Camillo")](http://www.karimblog.net/wp-content/uploads/2011/07/ilcompagnodoncamillo.jpg)Ho sempre letto con piacere i libri di Giovannino Guareschi su Peppone e Don Camillo, nel libro "Il compagno Don Camillo" mi sono imbattuto in una descrizione della campagna Ucraina che mi ha riportato un aspetto della Lituania che mi ha sempre colpito molto profondamente.


 Lo aveva notato anche quel mio amico che è venuto a trovarmi per un lungo e freddo fine settimana di Marzo. C'è qualcosa che manca in questi agglomerati di case sperdute nelle infinite pianure lituane; ricordo il mio quartiere tutto nuovo dove non esisteva nessun luogo di ritrovo, le facce erano sempre le stesse e per forza dell'abitudine qualcuno si salutava, ma mancava qualcosa che accomunasse le persone e che non fosse il solito supermarket o centro commerciale.  
  

> [...]  
> La sera stava cadendo: non un albero, non una casa rompevano la monotonia dell'immenso pianoro ondulato percosso dal vento. Soltanto campi di grano che si rincorrevano all'infinito, e non era difficile immaginarli trasformati in un palpitante oceano a spighe dorate, ma neppure il più smagliante sole della fantasia riusciva a scaldare il cuore gelato da quella tristezza.


 Don Camillo pensò alla Bassa: alla nebbia, ai campi impregnati di pioggia, alle stradette fangose. Era un altro genere di tristezza. Nessun vento, nessun gelo - laggiù alla Bassa - riuscivano a spegnere quel calore umano che emanava da tutte le cose toccate dall'uomo.


 Anche sperduto in mezzo alla campagna e sepolto dalla nebbia più densa, un uomo - laggiù alla Bassa - non si sente mai distaccato dal mondo. Un invisibile filo lega sempre agli altri uomini e alla vita e gli strasmette calore e speranza.


 Qui, nessun filo lega l'uomo agli altri uomini. Qui, un uomo è come un mattone: assieme agli altri mattoni forma un muro, è parte necessaria di un solido complesso. Cavato dal muro e buttato in mezzo a un campo, non è più niente, diventa una qualsiasi cosa inutile.


 Qui l'uomo isolato è disperatamente solo.  
[...]

