Title: La mia esperienza personale e la mia visione della mia vita in Lituania è completamente soggettiva
Date: 2011-07-15
Category: Lituania
Slug: la-mia-esperienza-personale-e-la-mia-visione-della-mia-vita-in-lituania-è-completamente-soggettiva
Author: Karim N Gorjux
Summary: Sono tantissimi anni che scrivo su questo blog. Ho iniziato nel 2004 e da allora, quasi per caso, karimblog.net è diventato un punto di riferimento riguardo alla Lituania. Il successo di questo[...]

[![Specchio del mondo](http://www.karimblog.net/wp-content/uploads/2011/07/mirror_world-225x300.jpg "Mirror")](http://www.karimblog.net/wp-content/uploads/2011/07/mirror_world.jpg)Sono tantissimi anni che scrivo su questo blog. Ho iniziato nel 2004 e da allora, quasi per caso, karimblog.net è diventato un punto di riferimento riguardo alla Lituania. Il successo di questo blog però ha travisato quello che è lo scopo di questo blog ovvero raccontare le mie esperienze e le mie personali visioni. **Più volte ho ribadito che non sono un giornalista** e non sono tenuto a raccontare in completa imparzialità la mia vita in Lituania. Se ci pensi bene anche il giornalista che dovrebbe essere imparziale, in realtà ha delle grandi difficoltà ad esserlo altrimenti non esisterebbero i giorni di parte o comunque tendenti verso una fazione politica rispetto ad un'altra.


 Qualche giorno fa ho ricevuto un commento di lode [sul blog di una mia lettrice](http://mcc43.wordpress.com/2011/06/28/i-miei-blogger/ "Il blog di Carla"), dato che è impossibile scrivere ed accontentare le aspettative di tutti ecco che si presenta sul più famoso forum in italiano sulla Lituania [una ricerca di dissensi](http://www.italietuva.com/forum/Fa-Un-Caldo-Terribile-t8530.html "Dissensi a karimblog.net") sulle lodi a questo blog.  
  
In tutti questi anni ho scritto di tutto sulla Lituania, ho fatto il grossolano errore di paragonare l'Italia e la Lituania, ho giudicato i Lituani, ho insultato e alle volte mi sono anche scusato. Poi il tempo passa, si conoscono nuove persone, si cresce ed intanto il mio modo di approcciarsi al paese baltico è cambiato radicalmente ed è cambiato anche il paese.  
  
**Chi legge i miei articoli li paragona alle proprie esperienze e trae un giudizio**. Chi legge del mio ritorno in Italia lo giudica secondo i suoi metri di misura e c'è chi mi dice che ho fatto bene e c'è invece chi mi da del fallito perché attribuisco la mia partenza alle presunte scarse possibilità di benessere in Lituania. Ho smesso di giudicare la Lituania tempo fa perché il mio giudizio era superfluo, inutile. Se giudicavo e incontravo l'apprezzamento di chi legge ricevevo delle lodi, se non le incontravo ricevevo insulti e ognuno comparava e continua a comparare la propria esperienza con la mia. Quindi chi ha avuto una pessima esperienza di fianco a una donna lituana trae il suo giudizio e condanna la mia relazione con mia moglie che dura ormai da 7 anni. Chi è in Lituania e si trova bene giudica il mio rientro come un fallimento, ognuno vuole giudicare perché una volta che giudichi, etichetti e misuri, gli dai un senso, lo percepisci, lo riduci e lo distruggi.


 **A ben pensare, sembra proprio una questione di ego**. Se tu hai avuto una relazione con una donna lituana che **a tuo giudizio** è stata fallimentare allora giudichi la mia situazione che è ben diversa dalla tua esperienza pur di dare senso al tuo **presunto fallimento**. Un commento di qualche ora fa all'articolo "[Le donne lituane: qualcosa di buono c’è](http://www.karimblog.net/2006/10/02/le-donne-lituane-qualcosa-di-buono-ce/ "Donne lituane")" riporta queste parole: "*qualcosa di buono ce.. ahi ahi karim.. m sa che per ora sei stato fortunato.*". 

 A volte mi viene chiesto come sono i lituani o le lituane e chi me lo chiede pretende a parole sia in grado di catalogare un popolo. Certo posso trovare dei tratti comuni riguardo alle fattezze fisiche e la cultura, ma le sfumature di un intero popolo sono difficili da descrivere in una manciata di aggettivi. Ma se il pensiero comune è di poter catalogare esperienze e persone a proprio piacere, qualsiasi cosa che scrivo verrà sempre contestata senza pensare che **il mondo che ci circonda è una pura interpretazione personale**. Più di una volta venivo accusato di scrivere cose non vere e mi arrabbiavo perché non capivo come si potesse mettere in discussione una mia esperienza di vita, poi mi sono reso conto che arrabbiandomi e contestando chi commentava, davo vita alla stessa linea di pensiero che in quel momento veniva puntata su di me.


 In risposta al forum ho scritto:  

>   
> La verità non è così assoluta e oggettiva come si crede. Certo se un cartello è rosso, siamo tutti d'accordo che è rosso, poi arriva un daltonico che non lo vede sto benedetto rosso e ti dice che è nero. Chi ha ragione? Dov'è la realtà?

 Fuori dalla finestra c'è un bel sole, fa caldo e passano le macchine. E' una bella giornata, ma per qualcun'altro non lo è. Per un cieco è tutto nero e per un sordo non c'è rumore. Io penso che abbiate tutti ragione e chi mi odia per ciò che scrivo dal canto suo vede me e i miei scritti in base alla sua vita e la sua esperienza e li giudica a suo modo. A me fa sorridere la critica continua e la ricerca dei consensi quando sono definito un "nulla", ma esporsi su internet scrivendo tanto quanto ho scritto io in questi anni comporta delle conseguenze come la critica continua, nel bene e nel male.


 Se avessi scritto delle cose che avrebbero fatto piacere ad Tizio sicuramente ci sarebbe un Caio che mi criticherebbe per altri motivi. Quindi dopo tanto tempo ho accettato questa posizione dove c'è chi mi dice che scrivo cazzate e invece chi mi dice che ho perfettamente ragione.


 **Quando leggete il mio blog e commentate avete ragione a prescindere**. Non posso sostituire le vostre esperienze e la vostra realtà, ma molti pretendono di cambiare la mia esperienza e la mia realtà per avere ragione su di me. Tempo addietro mi dava fastidio e da qui nascevano i miei commenti molto duri, ora mi è indifferente cosa pensate di me e le critiche che mi fate. Nella vita ho cambiato spesso idea, ho sognato tante cose e ne ho sperimentate altre. Forse ho fallito miseramente come dice qualcuno o forse no, ma non ha assolutamente importanza.  


