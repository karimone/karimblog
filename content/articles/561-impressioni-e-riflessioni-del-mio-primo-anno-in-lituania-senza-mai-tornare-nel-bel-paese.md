Title: Impressioni e riflessioni del mio primo anno in Lituania (senza mai tornare nel bel paese!)
Date: 2009-08-24
Category: Lituania
Slug: impressioni-e-riflessioni-del-mio-primo-anno-in-lituania-senza-mai-tornare-nel-bel-paese
Author: Karim N Gorjux
Summary: Oggi ufficialmente si ricomincia con il solito tram tram. Sveglia alle 7, colazione e via davanti al computer mentre Radio Deejay mi racconta le novità italiane.Ieri Greta ha compiuto tre anni, per[...]

[![](http://farm3.static.flickr.com/2476/3845954330_0115475085_m.jpg "Greta felice del suo terzo compleanno")](http://www.flickr.com/photos/kmen-org/3845954330/in/set-72157621987301041/) Oggi ufficialmente si ricomincia con il solito tram tram. Sveglia alle 7, colazione e via davanti al computer mentre Radio Deejay mi racconta le novità italiane.  
  
Ieri Greta ha compiuto tre anni, per festeggiare siamo stati con i parenti ad un piccolo lago [non troppo lontano](http://maps.google.it/?ie=UTF8&ll=55.894903,22.349625&spn=0.030079,0.067549&z=14) da Telsiai; ci siamo portati un sacco di roba da mangiare e abbiamo fatto anche i "saslykai" ovvero le nostre costine che hanno un gusto molto differente dalla nostra carne alla brace e infatti non è che mi facciano impazzire. Il posto era bello, i bambini si sono divertiti e noi grandi ci siamo rilassati e abbuffatti, il tempo ha retto bene e il compleanno è ben riuscito. Auguri Greta! La tua cuginetta si è messa di buona lena e ha fatto più di cento fotografie che puoi ammirare [qui](http://www.flickr.com/photos/kmen-org/sets/72157621987301041/)!

 Parlando di compleanni, oggi è esattamente **1 anno che abito in Lituania senza mai rientrare nel bel paese**. E' un record assoluto che infrange qualsiasi mio personale primato precedente come i sei mesi del 2006 passati in attesa della nascita di Greta. In questo anno le cose sono molto cambiate, io sono cambiato e la Lituania la vedo con altri occhi, ecco qualche mio spunto personale:

 I lituani sono semplicemente persone. Ci sono le persone brave e ci sono gli ubriaconi e i delinquenti. Basta **scegliersi le persone giuste** con cui avere relazioni. Tempo fa scrissi [un articolo molto negativo sui lituani](http://www.karimblog.net/2006/08/13/caro-lituano/), in parte è vero, ma è solo una parte della realtà. Ultimamente ho conosciuto dei lituani veramente esemplari.


 Ho iniziato a parlicchiare il lituano con gli amici, alle volte vado in giro per locali e conosci sia uomini che donne grazie al mio accento della "bassa lituania" i risultati sono incredibili, **mi parlano e mi sorridono**. Fantastico!

 Le comparazioni Italia - Lituania sono **inutili e poco produttive**. Continuare a paragonare le due nazioni non ha senso ed è solo uno spreco di tempo, ho imparato ad accettare la cultura lituana per quello che è, ne apprezzo i vantaggi e i svantaggi cerco di girarli a mio favore.


 Dell'Italia mi mancano i paesaggi, le edicole e la gente. I paesaggi perché nel cuneese ci sono le Alpi, le edicole perché sono piene di riviste che amo leggere e la gente perché ho un sacco di amici in Italia che mi mancano particolarmente, sia a [Centallo](http://www.centallo.com) (il mio paese) che a Cuneo nella mia [bellissima palestra](http://www.facebook.com//group.php?gid=103582595516&ref=ts).


 Obiettivi per il secondo anno lituano:  
  
* Leggere un libro in lituano, forse il classico primo volume di Harry Potter
  
* Portare il lavoro ad un livello successivo: ufficio dislocato e prima fase di integrazione lituana.

  
* Tornare al peso forma di quando ho messo per la prima volta un piede in Lituania.

  


 Se vi fate un giro nei vecchi articoli di questo blog, noterete che la mia prima permanenza in Lituania dal 2005 al 2006 non è stata delle migliori; sinceramente devo rettificare alcune cose, la Lituania è quello che è, cosa fa davvero la differenza dipende totalmente dal **nostro carattere e dalla nostra apertura mentale**.


