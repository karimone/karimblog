Title: Prime impressioni
Date: 2005-10-02
Category: Lituania
Slug: prime-impressioni
Author: Karim N Gorjux
Summary: Il fatto di essere di nuovo a Klaipeda mi ha leggermente scosso, ho notato con piacere che la città sta cambiando in meglio per avvicinarsi sempre di più allo stile di vita[...]

Il fatto di essere di nuovo a Klaipeda mi ha leggermente scosso, ho notato con piacere che la città sta cambiando in meglio per avvicinarsi sempre di più allo stile di vita europeo, ma la prima cosa che mi colpisce ogni volta che vengo in questo paese sono le case.  
  
Esteriormente i palazzi sovietici fanno ribrezzo, i muri sono incrostati e i balconi sembrano stati costruiti da bambini a cui si è dato loro un po' di eternit. Dato che la maggior parte dei palazzi sono di origine sovietica, girare per la periferia della città lascia a bocca aperta, l'impressione è che tutto deve cadere da un momento all'altro.  
  
  
  


 Cosa mi ha fatto tanto piacere rivedere sono i terminali alle casse dei supermercati. Dei grandi monitor LCD informano il cliente di ogni prodotto che viene passato alla cassa e aggiorna in tempo reale la situazione contabile, non penso che riuscirò mai a fare una foto e mostrarvi questo prodigio di tecnologia (è vietato fotografare nei centri commerciali), ma vi assicuro che i Lituani non si faranno infinocchiare dall'arrivo dell'euro. Mi immagino il classico ragazzo alto, biondo, spesso e intollerante alla sofferenza mentre guarda la sua birra aumentare del 100% nel corso di una notte. Sarebbero capaci di mettere a ferro e fuoco un intero paese (lo spero).


 Un'ultima cosa, anche i Lituani si interessano a noi, su un famoso portale locale è apparsa una notizia bizzarra: una donna di 66 anni ha perso la memoria, l'unica cosa di cui si ricorda è... andate a vedere e provate a capire. [[Articolo](http://www.delfi.lt/news/daily/hot/article.php?id=7606311 "Link su delfi")]

