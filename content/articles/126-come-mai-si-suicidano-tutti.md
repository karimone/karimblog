Title: Come mai si suicidano tutti?
Date: 2005-11-29
Category: Lituania
Slug: come-mai-si-suicidano-tutti
Author: Karim N Gorjux
Summary: A parte il titolo ad effetto, la Lituania ha dei primati tristi, uno tra tutti è il denominatore comune di tutti i paesi nordici: l'alta percentuale di suicidi.Alle 8:30 del mattino è[...]

A parte il titolo ad effetto, la Lituania ha dei primati tristi, uno tra tutti è il denominatore comune di tutti i paesi nordici: **l'alta percentuale di suicidi.**  
  
Alle 8:30 del mattino è ancora buio, ma il colore tende al grigio. Alle 10:30 è cambiato niente, il cielo è grigio e la neve inizia a farsi vedere...  
  
[![Mattinata](http://www.kmen.org/wp-content/upload//Mattina-tm.jpg "Mattinata")](http://www.kmen.org/wp-content/upload//Mattina.jpg)  


 Ora immaginatevi che alle 15:30 fa' già buio e siamo a cavallo, se poi il sabato sera tornate tardi, ovvero alle 4-5 del mattino di Domenica, probabilmente non vi incrocerete con il "sole" per due giorni.  
  
Secondo me il motivo per cui in Italia sono così socievoli e scherzosi è dovuto (anche) al sole, più le giornate sono belle e più c'è il buon umore. Qui si impiccano agli alberi, quindi se passate da queste parti non fate riferimenti a suicidi quando parlate con i locali, eviterete una sicura sensazione di imbarazzo o meglio **una gran figura di merda**.  
  
  
  


  adsense  
  
  
  


