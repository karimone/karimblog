Title: La mia personale analisi della psiche della donna baltica: esempi e consigli per sopravvivere
Date: 2009-03-13
Category: Lituania
Slug: la-mia-personale-analisi-della-psiche-della-donna-baltica-esempi-e-consigli-per-sopravvivere
Author: Karim N Gorjux
Summary: Premessa. Io non sono la verità, ma una verità. Quindi qualcuno di voi potrà risentire di ciò che scrivo e invece altri si troveranno decisamente d'accordo. Ho scritto questo articolo perché penso[...]

[![](http://farm1.static.flickr.com/33/60195448_92225152fa_m.jpg "Crochet")](http://www.flickr.com/photos/mindas/60195448/)Premessa. Io non sono la verità, ma una verità. Quindi qualcuno di voi potrà risentire di ciò che scrivo e invece altri si troveranno decisamente d'accordo. Ho scritto questo articolo perché penso di avere sufficiente esperienza e voce in capitolo da poterlo fare.


 Dato che parlo quasi sempre di donna lituana, vorrei consolarvi estendendo l'argomento alle donne baltiche in generale e riservando un discorso a parte per la donna russa. Quindi in questo articolo parlerò delle caratteristiche delle donne baltiche che ho conosciuto.  
  
**Discutere sugli effetti senza mai discutere di cause  
**  
Mi sono ritrovato molte volte a discutere con le mie fidanzate. Io non ho avuto solo donne straniere, ma anche italiane quindi posso fare il confronto senza rischiare di sbagliare troppo. La donna baltica incarna il genio del male del litigio e bisogna essere sempre molto freddi ed analizzare bene l'evolversi della situazione per non soccombere. Fateci caso quando litigherete la prossima volta o quando dovrete litigare, la partita a scacchi sul chi ha ragione e chi ha torto viaggia con regole differenti dalla logica ed il razionalismo. Lei dà importanza solo a ciò che è per lei importante e mai a cosa è veramente importante e ha fatto scaturire la discussione.


 Io lavoro in casa, ho sempre lavorato in casa. Molti di voi saranno invidiosi per questa mia fortuna di lavorare a 10 metri dalla camera da letto e 5 metri dal frigo, ma l'altro lato della medaglia purtroppo c'è.  
Lavoro per fare entrare soldi e per far stare bene le mie signore, questo è normale ed ogni uomo dotato di logica e un QI al di sotto delle tre cifre lo capirebbe. Purtroppo le donne non lo capiscono. Greta, grazie ai sui 30 mesi di vita, ha un alibi inattaccabile: piange alla porta, mi chiama, vuole giocare. E' normale, è una bimba. Con mia moglie è diverso: sono ormai anni che mi batto per ottenere il Nirvana dell'impiegato in casa. Silenzio, pace, amore e fratellanza. Invece ogni giorno è il primo giorno: entra senza bussare, mi disturba per delle cazzate, fa rumore, interrompe telefonate importanti.


 Tutto questo non ha assolutamente senso, ma la situazione si evolve così: io divento nervoso, le faccio notare i suoi soliti errori ed ecco che in quel momento inizia la lite. Ovviamente qual é il discorso della lite? Il fatto che io l'ho "richiamata"; **il focus si concentra sull'effetto e non sulla causa**.


 Litigare è decisamente inutile. Si ragiona su piani diversi. Lei è orgogliosa e tutte le donne da queste parti hanno un innato orgoglio profondamente radicato nel carattere. Litigare con una persona orgogliosa e come litigare con berlusconi: **le regole cambiano a suo favore.** Si perché lei ha il potere di mescolare le carte in tavola, di ricalcare argomenti passati e persino attribuirti colpe su presunti pensieri. Il tutto pur di aver ragione, non per migliorare il rapporto o per trovare una soluzione, ma solo per aver ragione. Se in un litigio tentate di seguire le sue argomentazioni, avrete person in partenza. Passerete per cornuti e mazziati. Non allontanatevi mai dalla causa del litigio, mai e poi mai!

 **Il vero punto dell'argomento non è mai preso in considerazione  
**  
Non viene mai preso in considerazione perché la realtà è un elemento opinabile. Tutto dipende da come volete vedere le cose e come lei vuole vedere le cose. Sotto questo profilo gli uomini sono molto più logici e razionali mentre le donne ragionano con i sentimenti.   
Mi sono trovato più volte a discutere su quelli che io chiamo **argomenti copertura**. Per arrivare a capire questo contorto meccanismo della mente femminile ho dovuto passare tanto tempo a pensare alle varie discussioni avute con mia moglie e altre ragazze in passato. Se state litigando con la vostra indiavolata baltica pensate bene se l'argomento della discussione è un argomento di copertura o la vera causa della lite.


 Vi faccio un esempio semplice che sono sicuro un mio carissimo amico lettore di questo blog capirà al volo e confermerà la mia teoria. Dovete fare una telefonata per qualche motivo importante nel paese "di lei", purtroppo però dovete chiedere a lei di fare la telefonata perché non siete padroni della lingua o non lo siete abbastanza. Probabilmente le risposte saranno degli argomenti di copertura come questi: "A quest'ora non rispondo", "Meglio passare direttamente in ufficio domani", "Ma tanto al telefono non ti daranno le risposte che cerchi", "Non è il caso telefonare, qui non funziona così..", "Varie ed eventuali puttanate inventate sul momento...".


 Se tentate di discutere sugli argomenti copertura siete finiti. State entrando nel contorto mondo della mente di lei armati della vostra logica che nessun potere ha contro le sue argomentazioni. La vostra unica ancora di salvezza è fare un passo indietro, spolverare della sana psicologia di coppia e **capire la causa.** Non vuole fare la chiama perché: "Si vergogna?", "Non vuole trovarsi in una situazione che non conosce?", "Ha paura?"...


 Le cause possono essere molteplici, ma se seguite i discorsi di lei sarà ben difficile avvicinarsi.


 **L'orgoglio prende il sopravvento. L'assenza di introspezione  
**  
Tutto sto casino di psicologia da manicomio è dovuto da tanto orgoglio e poca conoscenza di se stessi. Se lei sapesse riconoscere ed affrontare le proprie paure e i propri limiti, il 99% delle liti non esisterebbero. Il saper dire scusa, il capire i proprio errori non è un segno di debolezza, ma è uno dei primi elementi che ci contraddistingue dall'animale oltre ad essere la via per un eterno successo. Spostare l'origine di tutti i mali del mondo al di fuori di se stessi è il modo migliore per non migliorare mai.


 Da cosa nasce tutto ciò? Mancanza di affetto? Giovane età? Mancanza di esperienza? Sinceramente non l'ho ancora capito, penso che sia un mix di tutte queste cose messe insieme con l'aggiunta del nostro lascivo comportamento di uomo italiano tutto lusinghe e sorrisi che mi ha portato a farmi una domanda:

 **L'uomo lituano è troglodita e grezzo o è l'unico che ha capito qualcosa?  
**  
Vedo continuamente uomini burberi con il pancione immenso, dai lineamenti grezzi ed inespressivi come il terminator di Schwarzenegger, abbracciati a donne belle, dai lineamenti fini e dai capelli biondi passeggiare per Klaipeda. Penso che la maggior parte degli italiani che conosco si trasformerebbero in uno zerbino solamente per poter stringere la mano a donne del genere, eppure eccole li a fare la coppia meno assortita della razza umana. Perché? Perché forse l'uomo lituano non si fa tutte le seghe mentali partorite in questo lungo articolo, ma risolve tutte le situazioni in tre semplici parole: "calci nel culo".


