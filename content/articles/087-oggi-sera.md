Title: Oggi.. (sera)
Date: 2005-08-11
Category: altro
Slug: oggi-sera
Author: Karim N Gorjux
Summary: Riprendo il discorso dell'ultimo post, ho l'umore che ormai tocca quasi terra. Ho tante cose che vorrei gridare, ma sinceramente non posso. Come potrei dire certe cose senza fare nomi e cognomi[...]

Riprendo il discorso dell'ultimo post, ho l'umore che ormai tocca quasi terra. Ho tante cose che vorrei gridare, ma sinceramente non posso. Come potrei dire certe cose senza fare nomi e cognomi e senza offendere nessuno? Ho talmente tanta tristezza dentro di me che vorrei scappare oggi stesso e non farmi vedere mai più.  
  
Ma scappare dove? Tornare in Lituania? Si potrebbe anche fare, ma ho alcune cose da pianificare qui che potrei portarmi dietro fino al mar baltico. Mi spiace che sia tutto così nero, soprattutto lo sarà dopo il 25, ma non posso fare niente. Cosa mi manca? Cosa vorrei? Mi mancano tante cose, ma non vorrei niente di materiale. Cosa mi scoccia è quello che vedo attorno a me, purtroppo non posso scrivere altrimenti offenderei i diretti interessanti.


 Oggi è successo di tutto, non capisco come mai si sia catalizzato tutto in questa giornata. La mattinata è iniziata con un saluto, un saluto strano che ha confermato alcune supposizioni, purtroppo non posso assolutamente spiegare di più e mi sa tanto che questa frase la posso capire solo io. Le conseguenze di quel saluto sono state la consapevolezza di un muro sempre più alto che ci divide, siamo troppo diversi, io certe cose non riesco nemmeno ad accettarle.


 Nel pomeriggio due rivelazioni: la prima scoperta l'ho fatta quasi per caso, io dovrei rispondere al numero 0171 214889, ma per qualche strano motivo risponde Duilio che si trova a Demonte e poi in qualche modo (ci tengo a sottolineare il qualche modo perché io non risponderò più al telefono) verrò avvertito sul da farsi. La seconda scoperta è stata il non permettere a mia sorella di usare il computer di mia madre per imparare ad usare photoshop, poco male, ci penserò io, ma io continuo a segnare sul libro nero.


 Non dimentico perché sono sicuro che nella vita, prima o poi, tutto si paga.


