Title: Lo zoo di Riga
Date: 2004-08-30
Category: Lettonia
Slug: lo-zoo-di-riga
Author: Karim N Gorjux
Summary: Sono tornato al maza parks per visitare finalmente l'unico ZOO della lettonia. Non e' molto grande, ma e' molto curato: orsi, canguri, cavalli, tigri, serpenti, ragni.. c'e' un po' di tutto. All'interno[...]

Sono tornato al maza parks per visitare finalmente l'unico ZOO della lettonia. Non e' molto grande, ma e' molto curato: orsi, canguri, cavalli, tigri, serpenti, ragni.. c'e' un po' di tutto. All'interno ho fatto conoscenza con un ragazzo di San Pietroburgo con indosso una maglietta dei led zeppelin.. :)  
Da notare la mia foto ai leoni, farla e' stata un'impresa! ma il risultato e' stato grandissimo... (in my humble opinion)

 La zona dei cavalli e' quella che ho preferito. Ho fatto conoscenza con un cavallo nano grigio che mi ha amato dal primo momento (tra nani ci si intende) :-D  
Molto interessante la zona tropicale dove e' tutto tropicale ed anche il clima. I ragni e altri insetti mi hanno fatto parecchio schifo, ma sono riuscito lo stesso a mangiare le schifezze di un locale interno allo zoo e ad avvelenarmi da vero lettone. Qui la maionese, il ketchup e il fritto e' dappertutto! 

 Ho di nuovo noleggiato la bici con Kristina e siamo andati dentro il parco. Interessante come da una strada asfaltata sia possibile entrare in un sentiero ed andare in bici in mezzo al bosco. Il parco e' frequentato da centinaia di roller, consiglio a tutti di venire da queste parti perche' ne vale davvero la pena. (bus 11 dal nationala opera)

