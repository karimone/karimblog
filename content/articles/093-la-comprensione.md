Title: La comprensione
Date: 2005-08-29
Category: altro
Slug: la-comprensione
Author: Karim N Gorjux
Summary: Mentre discutevo con mia madre, non troppo animatamente come al solito, mi chiedevo perché lo facessi. Mia madre è molto orgogliosa, parlare con lei è molto difficile, non ammette nemmeno l'evidenza e[...]

Mentre discutevo con mia madre, non troppo animatamente come al solito, mi chiedevo perché lo facessi. Mia madre è molto orgogliosa, parlare con lei è molto difficile, non ammette nemmeno l'evidenza e questo rende difficile qualsiasi tipo di discussione. Tra le frasi che mi ha detto una mia è rimasta impressa: "*Voi figli siete bravi ad usarmi quando avete bisogno di qualcosa*"  
  
Posso dire che ha ragione, mia madre non mi ha mai fatto mancare niente che non sia acquistabile, ma un figlio non può crescere solo di playstation, libri, dvd, bicicletta, macchina sportiva, computer portatile, ferie pagate, jet privato, palestra in casa. (la differenza tra un maschio adulto ed un bambino è nel prezzo dei suoi giocattoli). Ci sono cose che non si possono comprare. La famiglia della mia ragazza non è benestante come la mia, ma è molto più ricca.


 Ammetto di invidiare mia cugina, non vi dico quale, ma non fa differenza perché entrambe le cugine che frequento hanno la stessa fortuna. Stranamente non provo invidia per l'amico con la macchina che vale circa 15 volte la mia, ma invidio la cugina che vive una vita normale, invidio la tranquillità e la spensieratezza di una banalissima vita normale. Vorrei poter confrontarmi, parlare di me, ascoltare, ridere, cantare... vorrei respirare ciò che vorrei vivere.


 Se non fosse per mia Zia, a cui devo tutto ciò che sono, mi sarei già suicidato da un pezzo (e non lo dico per scherzo), non capisco da dove venga tutto questo carattere estroverso che ho, purtroppo mi rendo conto che sono molto lunatico. Se ho qualcosa che non va' non riesco a recitare la parte spensierata di chi sta bene. Non sono perfetto.


 Questa sera ho sentito alcune affermazioni che confermano la regola che viva in casa "mia", tutto si misura con i soldi. "*Rita deve ringraziare che ha trovato me...*" quando Rita è partita per la Lituania, era dispiaciuta per la nostra separazione, ma per lo più provava pena per me che rimanevo qui. 

 Spero di capire tutto questo e spezzare la catena con me. Spezzare la catena riguardo a tutto.


