Title: I russi in Lituania una realtà concreta ben difficile da sradicare
Date: 2011-01-31
Category: Lituania
Slug: i-russi-in-lituania-una-realtà-concreta-ben-difficile-da-sradicare
Author: Karim N Gorjux
Summary: Secondo alcuni dati che mi sono giunti per via orale (quindi da confermare), sembrerebbe che in Lituania la presenza dei russi sia il 6-7% della popolazione. Qui a Klaipeda si arriva ad[...]

[![](http://www.karimblog.net/wp-content/uploads/2011/01/Russia-Lithuania.jpg "Russia-Lithuania")](http://www.karimblog.net/wp-content/uploads/2011/01/Russia-Lithuania.jpg)Secondo alcuni dati che mi sono giunti per via orale (quindi da confermare), sembrerebbe che in Lituania la presenza dei russi sia il 6-7% della popolazione. Qui a Klaipeda si arriva ad un picco del 30% di russi forse a causa della stretta vicinanza con [Kaliningrad](http://it.wikipedia.org/wiki/Kaliningrad), l'enclave russa sul mar Baltico.


 **Ho avuto modo di conoscere vari russi in Lituania** e generalmente, nel caso della mia personale esperienza, ho notato che **sono più socievoli ed amichevoli** e sotto alcuni punti di vista sono po' come noi italiani.   
Intendiamoci sin da subito, non sto dicendo che i russi sono tutti in un modo o tutti in un'altro, ma mi limito a darti un'immagine in base alle mie esperienze. Se poi verrai qui in Lituania forse avrai esperienze totalmente diverse dalle mie che potrai segnalarmi commentando questo articolo.  
  
La mia esperienza con i russi è stata particolare, per prima cosa il rapporto russi lituani è un po' (tanto) controverso. Dopo il regime sovietico molti russi sono rimasti qui pensando di continuare la loro vita senza troppi problemi e **dopo 20 anni dalla caduta del muro c'è gente che vive qui e non parla ancora il lituano**. Mi sono ritrovato in una situazione assurda di parlare con un signore sulla sessantina che non sapeva il lituano e nemmeno l'inglese quindi non ho avuto modo di scambiare due parole oltre al solito "ciao".


 **Il paradosso è che se parli il lituano in Lituania puoi ancora provare la sensazione di essere uno straniero "only-english"** quando sei circondato da gente che parla solo il russo e malissimo, se non nulla, il lituano. Ho conosciuto un ragazzo di vent'anni che dopo i miei sforzi nel parlare lituano mi ha detto: *"Guarda, io sono russo, se vuoi parlami in inglese che lo capisco meglio del lituano"*. Oltre il problema linguistico che trovo assurdo e persino imbarazzante, c'è una netta divisione tra i lituani ed i russi. I russi tendono generalmente a frequentare russi e i lituani "odiano" in misura diversa i russi per ovvie ragioni storiche e culturali.


 L'odio dei lituani verso i russi è preventivo, non si basa sulla persona dopo averla conosciuta, ma a priori. **Si dice**, e quindi è da verificare, che nel 1980, durante le olimpiadi di Mosca, la Russia abbia mandato tutta la "*feccia*" della capitale a vivere nei paesi baltici. Ho sentito dei russi in Russia e dicono che i russi che vivono nei paesi baltici siano della *"razza peggiore"*. In pratica sti poveracci prendono calci nel sedere ovunque si girino.


 Se vivi qui un po' di anni come me, dopo un po' di tempo riuscirai a farti l'occhio e riconoscere i lituani ed i russi e a sentire le differenze a prima vista. Le donne russe che ho incontrato qui a Klaipeda erano per la maggior parte delle volte *sconce* sia nel vestire che nel parlare, a sentire i lituani sembra che siano tutte così, ma in realtà ho avuto modo di conoscere qualche ragazza di buona famiglia. Devo dire però che le donne russe qui a Klaipeda in linea generale non mi hanno fatto buona impressione, ma è una questione soggettiva.


 Mi sono ritrovato 4-5 volte in compagnie russe ed è veramente demoralizzante non capire niente pur parlando la lingua ufficiale lituana. Giustamente se ti ritrovi in una little italy negli Stati Uniti probabilmente sentiresti parlare italiano pur se la lingua ufficiale è l'Inglese. Purtroppo però qualche parola di russo ancora la capisco e ho notato che il sentimento contro lo straniero che ho già ritrovato con i lituani, è ampiamente diffuso anche tra i russi.


 Non voglio dilungarmi nei dettagli della mie esperienza, ma posso dire che **vivere con i lituani o i russi in generale è molto difficile**. I lituani sono freddi e distaccati, ma più amichevoli se parli il lituano. I russi sono più amichevoli sin da subito, ma è un'amicizia teatrale, di facciata e poco sincera. Detto questo, sempre dal canto della mia esperienza, suggerisco di non fidarsi troppo dei russi fino a quando non si è sicuri della persona che si ha davanti.


 Il discorso con le donne è ovviamente a parte dato che per secondi fini o solo per natura sono sempre le donne le persone più amichevoli e gentili anche se sei uno straniero.


