Title: Lituania - Italia
Date: 2007-06-02
Category: Lituania
Slug: lituania-italia
Author: Karim N Gorjux
Summary: In attesa della partita contro la Lituania, la gazzetta pubblica una piccola guida su come andare a Kaunas e recuperare i biglietti. Ma conviene andare a Kaunas solo per una partita? Fateci[...]

In attesa della partita contro la Lituania, [la gazzetta pubblica](http://www.gazzetta.it/Calcio/Nazionale/Primo_Piano/2007/06_Giugno/01/VADEMECUM%20LITUANIA.shtml) una piccola guida su come andare a Kaunas e recuperare i biglietti. Ma conviene andare a Kaunas solo per una partita? Fateci almeno una vacanza, ma fate attenzione. A Kaunas gli schiaffoni volano facili.


