Title: Particolarità della società lituana vista attraverso gli occhi di un italiano (Parte 3)
Date: 2009-12-18
Category: Lituania
Slug: particolarità-della-società-lituana-vista-attraverso-gli-occhi-di-un-italiano-parte-3
Author: Karim N Gorjux
Summary: Continuo la serie di articoli sulle particolarità della società lituania, se hai perso le puntate precedenti, puoi leggiti la parte 1 e la parte 2. In questa puntata mi occupo di strade.

![Semaforo](http://www.karimblog.net/wp-content/uploads/2009/12/Semaforo.jpg "Semaforo")Continuo la serie di articoli sulle particolarità della società lituania, se hai perso le puntate precedenti, puoi leggiti la [parte 1](http://www.karimblog.net/2009/12/14/particolarita-della-societa-lituana-attraverso-gli-occhi-di-un-italiano/) e la [parte 2](http://www.karimblog.net/2009/12/15/particolarita-della-societa-lituana-vista-attraverso-gli-occhi-di-un-italiano-parte-2/). In questa puntata mi occupo di strade.


 **Il semaforo lituano è una fonte di incazzature infinita**, dopo anni cerco ancora di capire come mai qui a Klaipeda ti devi ricordare le particolarità di qualche semaforo per non trovarti il motore di qualche autovettura sul sedile passeggero. Guido da ormai 12 anni e da quanto ne so io se il semaforo è verde con con il simbolo del cerchio verde puoi partire. Non ha importanza che tu debba girare a destra o a sinitra o andare dritto, se è verde, puoi andare. In Lituania invece devi fare attenzione perché se è verde con il simbolo del cerchio colorato di verde devi fare attenzione che il semaforo non abbia "l'appendice" che delimiti l'uso della corsia a destra o a sinistra.  
  
**A Klaipeda, il semaforo vicino all'akropolis, lo devi conoscere a priori**. Se è verde e tu devi attraversare l'incrocio per andare a sinistra, fai attenzione perché devi sapere che devi aspettare il "tuo verde". Lo devi sapere a priori, non hai attenuanti. A Kretinga il semaforo che imbocca per la strada per Klaipeda segue la stessa natura autoctona qundi se cerchi di attraversare l'incrocia per girare a sinistra fai ben attenzione ad aspettare che si accenda il tuo semaforino con la freccia verde rivolta a sinistra.


 Dato che ho preso la patente 12 anni fa, **forse mi sono perso qualche aggiornamento del codice della strada** e il semaforo con il disco verde è caduto in prescrizione perdendo il significato di precedenza universale. Sono io che cado in errore a non sapere queste cose o c'è qualcosa di sbagliato nei semafori lituani? Il dubbio mi assale maggiormente quando vado a Telsiai e vedo che il semaforo nella piazza del Maxima rispetta i canoni della logica. Freccia avanti di colore verde seguita in da freccia sinistra e destra di colore verde su altro semaforo. Qualcuno può spiegarmi?

 **Il semaforo degno di un romanzo di Lewis Carrol risulterebbe simpatico se non avesse la spiacevole conseguenza di darti in pasto alla polizia lituana**. Se ti sei fatto 2500km per vedere le bellezze dei paesaggi lituani, non sarai certo felice di doverti fare una chiacchierata a gesti con la polizia locale.   
Mi è già successo un paio di volte che la polizia mi fermasse e la cosa che più mi ha incuriosito è che qui **usano farti sedere sul sedile posteriore della loro volante** quando qualcosa devono farti una multa o discutere di qualosa.   
Personalmente mi è sempre andata bene con la polizia, solo una volta mi hanno fermato inventandosi una scusa sciocca pur di crearmi dei fastidi, ma io ho risposto dicendo: "bene, vi seguo in commissariato, chiamo l'ambasciata e se è il caso anche un avvocato". Nulla di tutto ciò accadde, mi lasciarono andare sottolineando la loro magnanimità nei miei confronti.


 Chiamai l'ambasciata lo stesso e mi confermarono che **i soprusi della polizia lituana nei confronti degli italiani è cosa nota**, ma solo raramente le vicende hanno un epilogo da cronaca.


 Riguardo al parcheggio **i lituani tendono a parcheggiare bene**, ma una piccola percentuale parcheggia in modo talmente selvaggio che il più delle volte òìopera si merita una foto da far vedere agli amici. Capita spesso di vedere nei parcheggi dei supermarket dei macchinoni che tendono a prendere più posto possibile nel modo più plateale possibile. Ogni tanto mi adeguo anche io e, nella fretta, mi prendo la licenza poetica del parcheggio creativo, non voglio mica essere discriminato! :-)

 Un'ultima cosa a cui tengo davvero posare l'attenzione è **la manutenzione e lo sviluppo stradale in Lituania**. Quando dalla Polonia sono entrato in Lituania, mi è sembrato di arrivare in Svizzera, le strade Lituane sono davvero ben curate e l'autostrada, oltre che essere gratuita è davvero gradevole da percorrere (a parte la monotonia del paesaggio). Qui a Klaipeda sono stati fatti dei lavori sull'autostrada e nelle strade in centro e devo dire che i tempi di realizzo e la qualità del lavoro (lo dico da semplice guidatore), mi hanno lasciato davvero stupito.


 Mi hanno lasciato stupito perché **sono abituato ormai da anni a percorrere la Torino - Milano e vederla come un cantiere senza tempo e senza fine**. Mi chiedo chi ne dirige i lavori, forse Michael Ende?

 PS: Ci sono altre particolarità che non ho segnalato, la prima che mi viene in mente è il "rispetto del pedone", ma non ho nessuna esperienza degna di essere raccontata sul blog. **Forse puoi aiutarmi tu a completare questo articolo nei commenti?**

