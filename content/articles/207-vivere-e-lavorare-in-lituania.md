Title: Vivere e lavorare in Lituania
Date: 2006-08-28
Category: Lituania
Slug: vivere-e-lavorare-in-lituania
Author: Karim N Gorjux
Summary: Per chi avesse l'intenzione di venire a vivere e lavorare in Lituania, il governo ha diffuso un interessante libretto in Inglese dove vengono illustrate le informazioni essenziali per esaudire il vostro desiderio.[...]

Per chi avesse l'intenzione di venire a vivere e lavorare in Lituania, il governo ha diffuso un interessante libretto in Inglese dove vengono illustrate le informazioni essenziali per esaudire il vostro desiderio. Penso di avere fatto piacere a molti o ad almeno ad una persona (mitico Enrico di Boves) facendo una scansione dellle 20 pagine di questo libretto.


 Forse è meglio che l'opuscolo venga tradotto anche in lituano, visto l'elevato numero di lituani che emigra ogni anno per lavorare in altri paesi (permettetemi la battuta).


 Link: [Living and working in Lithuania](http://www.karimblog.net/wp-content/upload/LivingAndWorkingInLT.pdf) [pdf 1.3Mb]

  technorati tags start tags: [lavorare](http://www.technorati.com/tag/lavorare), [lituani](http://www.technorati.com/tag/lituani), [lithuania](http://www.technorati.com/tag/lithuania), [living](http://www.technorati.com/tag/living), [working](http://www.technorati.com/tag/working), [vivere](http://www.technorati.com/tag/vivere)



  technorati tags end 

