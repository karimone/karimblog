Title: Estonia, il voto elettronico è realtà
Date: 2005-09-07
Category: altro
Slug: estonia-il-voto-elettronico-è-realtà
Author: Karim N Gorjux
Summary: Estonia, il voto elettronico è realtà:La Corte Suprema di Tallin approva l'uso del metodo elettronico a partire dalle prossime elezioni locali, che si svolgeranno ad ottobre. Si votera' da casa. I parlamentari:[...]

[Estonia, il voto elettronico è realtà](http://punto-informatico.it/pbox.asp?i=54777):  
*La Corte Suprema di Tallin approva l'uso del metodo elettronico a partire dalle prossime elezioni locali, che si svolgeranno ad ottobre. Si votera' da casa. I parlamentari: siamo pronti al voto via Internet *  


 Intanto io mi faccio due risate, perchè l'Estonia è pur sempre uno dei fanalini di coda dell'Europa anche se io non ho mai visitato Tallin, ma ho visto sia Vilnius e Riga e posso dire che noi stiamo molto meglio. Il problema è che gli italiani vanno sempre in *[leggera controtendenza](http://www.beppegrillo.it "Sito di Beppe Grillo")*, quindi mentre uno stato come l'Estonia risulta uno dei più informatizzati al mondo, noi siamo ancora costretti ad usare una 56k e il massimo della broadband flat che ci possiamo permettere è una 128kb al prezzo di 44€ al mese.


 Porca puttana dico io! Se proprio ci vogliono abbandonare e non darci l'adsl, ma almeno fate un'offerta isdn ad un prezzo ragionevole. Dato che la 640 kb/s ora costa 19€ al mese, almeno date l'isdn a 10€ al mese canone compreso e non a 44€! Che cazzo di paese è questo???

 Sono sempre dell'idea che l'unica è dare fuoco alle centraline telecom, la protesta costerebbe 1 litro di benzina ad ogni riparazione senza adsl.


 Ma come può l'Italiano medio leggere il giornale la mattina e non spararsi?

