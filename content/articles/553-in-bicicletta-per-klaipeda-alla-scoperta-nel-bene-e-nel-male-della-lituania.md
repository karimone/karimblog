Title: In bicicletta per Klaipeda alla scoperta, nel bene e nel male, della Lituania
Date: 2009-07-15
Category: Lituania
Slug: in-bicicletta-per-klaipeda-alla-scoperta-nel-bene-e-nel-male-della-lituania
Author: Karim N Gorjux
Summary: Da quando giro in bicicletta per Klaipeda, ho avuto modo di notare con piacere che l'urbanistica locale ha considerato appieno la bicicletta come mezzo di trasporto. Ovunque vada ho il mio percorso[...]

[![](http://farm4.static.flickr.com/3116/2743099955_b2bbe6d9b8_m.jpg "Percorso in bicicletta Klaipeda - Palanga")](http://www.flickr.com/photos/29142526@N04/2743099955/)Da quando giro in bicicletta per Klaipeda, ho avuto modo di notare con piacere che l'urbanistica locale ha considerato appieno la bicicletta come mezzo di trasporto. Ovunque vada ho il mio percorso riservato, persino nel bel centro della città.  
  
Girando in bicicletta mi posso permettere di passare in vie che non ho mai visto, posso osservare meglio i dettagli di una città che è in continuo mutamento, ma posso soprattutto osservare le persone.


 Ho notato che tante ragazze e tanti ragazzi passeggiano con le cuffie nelle orecchie, mi è capitato a volte di vedere ragazzi con cuffie enormi passeggiare per le vie del centro non curanti di cose gli stesse accadendo attorno. Ho visto anche signore sulla cinquantina vestite a puntino in modo elegante e signorile in netto contrasto con la controparte maschile, ho visto un ubriaco disteso a pancia in giù su un prato e sembrava più morto che vivo.


 Ho sorriso a delle signore anziane sulle panchine che sembravano uscite dalle cartoline sovietiche che puoi vedere su internet, mi ha fatto piacere vedere i bimbi con le famiglie andare in bicicletta assolutamente non curanti della pioggia, mi ha sorpreso vedere tante mamme con il passeggino e mi è piaciuto tantissimo incrociare gli animali selvatici nei sentieri per la foresta. Mi è dispiaciuto molto vedere bottiglie di vetro e di plastica abbandonate vicino alle panchine del parco, ma tutto sommato mi fa davvero piacere poter scoprire la Lituania così da vicino, nel bene e nel male che sia.


 Link: [Il percorso in bicicletta Klaipeda - Palanga](http://www.cruiseklaipeda.com/?en=1170679382)

