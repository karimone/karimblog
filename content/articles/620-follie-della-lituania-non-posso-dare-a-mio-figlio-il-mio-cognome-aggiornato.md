Title: Follie della Lituania: non posso dare a mio figlio il mio cognome (aggiornato)
Date: 2010-10-14
Category: Lituania
Slug: follie-della-lituania-non-posso-dare-a-mio-figlio-il-mio-cognome-aggiornato
Author: Karim N Gorjux
Summary: La Lituania è un paese con un sacco di problemi, la crisi lo sta mettendo in ginocchio e i lituani che scappano all'estero senza tornare lasciano la Lituania in balia di se[...]

![](http://www.karimblog.net/wp-content/uploads/2010/10/burocrazia_lituana.jpg "Burocrazia Lituana")La Lituania è un paese con un sacco di problemi, la crisi lo sta mettendo in ginocchio e i lituani che scappano all'estero senza tornare lasciano la Lituania in balia di se stessa e con poche speranze per il futuro. Come ha reagito la politica lituana a tutto questo? Ha vietato l'uso delle lettere x,y,q X,W,Q, perché non sono presenti nell'alfabeto lituano e quindi non possono essere usate.  
**  
Il classico caso di protezionismo linguistico culturale**. A questo punto perché non rimuovere anche i numeri arabi, in fin dei conti non appartengono alla cultura lituana! E che non si azzardino a prendere in prestito i numeri romani, da italiano mi oppongo categoricamente alla concessione.  
  
A parte gli scherzi, la Lituania si sta lentamente suicidando. Con tutti i problemi che hanno qui, l'ultima cosa con cui dovrebbero perdere tempo è sulle lettere dell'alfabeto. **Ora sono costretto a fare una guerra burocratica** che ho già pianificato nella mia mente, il primo passo è stato scrivere all'ambasciata italiana a Vilnius, il secondo passo lo fa mia moglie scrivendo a chi di dovere qui in Lituania. Se non si ottiene niente ho due possibilità: presentare il caso all'Unione Europea o rimuovere, se è possibile, la cittadinanza lituana a mio figlio.


 Ecco cosa ho scritto all'ambasciata italiana:  

> *Salve, sono padre di un bambina, Greta Gorjux nata a Klaipeda che ho registrato presso di voi nel 2006. Circa 10 giorni fa ho avuto la gioia di vedere nascere mio figlio Tomas David Gorjux anche lui nato a Klaipeda. Questa mattina mia moglie si reca nei vari uffici per registrare il bambino e la sorpresa è che la Lituania non accetta più la "x" dato che non fa parte dell'alfabeto lituano e quindi per registrarlo hanno messo la "ks" al posto della "x".*

 *Il certificato di nascita sfiora l'assurdo, c'è scritto che Tomas Gorju**ks** è nato il 4 Ottobre 2010 da Karim Gorjux e Rita Gorjux. In pratica mio figlio ha il cognome diverso da sua sorella e persino da me che sono suo padre! Questo ha delle conseguenze burocratiche gratuite a cui vorrei fare a meno. Si immagina solo a prendere l'aereo con tutta la mia famiglia a che problemi andrei incontro? In pratica dovrei attrezzarmi con certificati scritti da un notaio che Tomas Gorjuks è mio figlio. Sono senza parole.*

 *Ora io devo registrare il bimbo anche da voi visto che per fortuna è anche italiano. Non ditemi che dovrò affrontare una guerra burocratica anche con l'Italia per dargli il mio cognome perché sinceramente trovo questo assolutamente folle.*

 *Attendo vostre notizie.*

 *Cordiali Saluti*

 *Karim Gorjux*

