Title: Ma avete sentito Linus e Nicola leggere la mia email in diretta?
Date: 2008-09-29
Category: Lituania
Slug: ma-avete-sentito-linus-e-nicola-leggere-la-mia-email-in-diretta
Author: Karim N Gorjux
Summary: Nella mia lettera a Radio Deejai avevo parlato di un anomala serie di giorni con sole e 18 gradi di temperatura costanti, ma un dubbio mi sorge: avevate capito che, in fondo[...]

Nella mia lettera a Radio Deejai avevo parlato di un anomala serie di giorni con sole e 18 gradi di temperatura costanti, ma un dubbio mi sorge: avevate capito che, in fondo al [penultimo post](http://www.karimblog.net/2008/09/25/saluti-internazionali/), c'era la registrazione audio!? Basta cliccare sul simbolo del play e vi sentite Linus e Nicola leggere la mia email in diretta a Deejay Chiama Italia.


 Per la cronaca: ora piove :-)

