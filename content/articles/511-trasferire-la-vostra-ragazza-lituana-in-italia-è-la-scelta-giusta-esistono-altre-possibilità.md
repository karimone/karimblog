Title: Trasferire la vostra ragazza lituana in Italia è la scelta giusta? Esistono altre possibilità?
Date: 2008-12-22
Category: Lituania
Slug: trasferire-la-vostra-ragazza-lituana-in-italia-è-la-scelta-giusta-esistono-altre-possibilità
Author: Karim N Gorjux
Summary: Ho ricevuto un'email che secondo me rappresenta una situazione molto comune:Buonasera mi chiamo Marco le scrivo da Torino. Bhe per prima cosa le faccio i complimenti per il suo blog. Ho letto[...]

Ho ricevuto un'email che secondo me rappresenta una situazione molto comune:  

> Buonasera mi chiamo Marco le scrivo da Torino. Bhe per prima cosa le faccio i complimenti per il suo blog. Ho letto davvero un sacco di cose interessanti. Ma le scrivo come penso ormai le scriveranno migliaia di persone per avere qualche dritta.   
>   
> Sono fidanzato da circa un anno con una ragazza lituana. Per il momento faccio avanti e indietro e ci vediamo una volta al mese. Ma presto ho intenzione di portarla qui. Contiuiamo a comunicare in inglese e ogni tanto ci infiliamo in mezzo una parola di italiano e di lituano. Il problema più grosso sarà quello di trovarle un lavoro e davvero non so dove sbattere la testa e che idea farmi venire. Ovvio che lei dovrà prima prendere confidenza con l'italiano ma poi? ... su che settore possiamo orientarci? Ha qualche consiglio da darci? 

 Lei ora studia amministrazione e per mantenersi gli studi lavora in un kavinè ... Penso che sarà durissima ma ci voglio credere, tutti mi hanno sconsigliato di portarla qui ma non sto ascoltando nessuno spero non sia un fallimento. Lei avrà tante difficoltà ma ho intenzione di starle vicino. Non so nemmeno da dove cominciare. Se ha qualche consiglio da darmi la ringrazio davvero, colgo l'occasione per farle gli auguri per un buon Natale e per un sereno 2009, Marco.


