Title: Ti accorgi che i dentisti italiani sono dei rapinatori legittimati quando vai da un dentista in Lituania: costi contenuti e servizi ottimo
Date: 2006-03-20
Category: Lituania
Slug: ti-accorgi-che-i-dentisti-italiani-sono-dei-rapinatori-legittimati-quando-vai-da-un-dentista-in-lituania-costi-contenuti-e-servizi-ottimo
Author: Karim N Gorjux
Summary: Un paio di giorni fa sono stato a Telsai. Il viaggio è stato piacevole, un paio di ore di treno attraverso alla Lituania in mezzo a foreste e pianura incontaminata. Il sole[...]

Un paio di giorni fa sono stato a Telsai. Il viaggio è stato piacevole, un paio di ore di treno attraverso alla Lituania in mezzo a foreste e pianura incontaminata. Il sole splendeva nel cielo color azzurro vivo come mai aveva fatto in questi due mesi. La pianura irregolare mostrava una bellezza nascosta che non ricorda. Dune bianche popolate da alberi, volpi, conigli e cerbiatti come solo nei documentari di Piero Angela ho visto.  
  
Il viaggio, nonostante le sue due ore di durata, è stato breve. Il paesaggio mi ha affascinato come non mai e accompagnato dalla musica dell'iPod Nano ha fatto volare le due ore.


 L'ultima volta che sono stato a Telsai ci sono andato in macchina e tre cerbiatti mi hanno attraversato la strada in formazione [Beatles](http://www.popartuk.com/g/l/lglp0597.jpg) ovviamente senza passare sulle strisce e galoppando come delle gazzelle inseguite dal demonio. Mi è rimasto particolarmente impressa quella scena perché mi trovato in un rettilineo abbastanza lungo e procedevo a velocità di crociera. Sulla destra l'ennesima foresta di alberi nascondeva la fauna lituana ai curiosi, sulla sinistra un campo innevato ospitava una cascina in situazioni disperate. In lontananza vedo tre puntini neri muoversi nei pressi della cascina, più mi avvicino e più i puntini prendono forma. Sembrano volpi. Quando mi rendo conto che sono cerbiatti si mettono a correre verso la strada passandomi davanti ad una velocità mai vista per poi essere inghiottiti dalla foresta. Semplicemente stupendo.


 Una volta arrivati a Telsai andiamo a casa dei genitori di Rita. Dobbiamo aspettare mezzogiorno per poi andare dal dentista, **sia io** che Rita abbiamo un appuntamento per una visita. Io i dentisti li odio dal profondo, generalmente ti fanno male sia fisicamente che economicamente e le mie esperienze passate non sono state delle migliori.


 Telsai è una cittadina stupenda, se non fosse per il degrado post-comunista, sotto certi versi mi ricorda i paesini evocati nei racconti Stephen King; i pochi abitanti si conoscono per filo e per segno, la tranquillità che si respira a Telsai è rilassante e alla lunga quasi noiosa. La signora titolare dello studio dentistico ha quasi una cinquantina d'anni, dentatura da attrice di hollywood, capelli corti, fisionomia longilinea ed ovviamente 10 cm sovrastano la mia fronte in totale rispetto dell'altezza media locale. Lo studio è composta da appena due stanze: l'ingresso è la sala d'aspetto di cui il perimetro è ornato dalle tipiche sedie da ufficio. Una porta targata WC, opposta all'ingresso, introduce alla stanza fisiologica; sulla sinistra una doppia porta apre ai clienti la sala dei dolori.


 L'impressione è di una stanza spoglia, i dentisti italiani hanno tante cose che non ho mai capito cosa servissero. Questa signora non ha la segretaria, ha giusto una scrivania, la sedia del dentista e una bancone da laboratorio. Mi ha impressionato parecchio il suo metodo per prendere gli appuntamenti: arriva un signore e chiede di fissare una data, la dottoressa davanti a tutti ispeziona le fauci del cliente e pensa a ciò che deve fare, il bello è che questo signore era in piedi davanti alla porta che divideva lo studio dalla sala d'aspetto. Un paio di considerazioni e via.. prenotazione fatta.


 Il primo a passare sotto i ferri è il sottoscritto, ringraziando il cielo non ho nessun problema e l'operazione è veloce, in 20 minuti si fa tutto. Rita ne ha per più di un'ora. Deve farsi togliere 3 carie in fase avanzata. Dopo aver pazientemente atteso, ecco la parte peggiore, tocca pagare. Secondo voi quanto può essere costato? Facciamo i conti, la mia pulizia di routine 25... Rita ha 3 otturazioni (non sono sicuro che sia il nome giusto)... 60 e quindi ... 85 Litas ovvero **24€ - ventiquattro/00 €**.


 Secondo i miei calcoli, per fare una paragone con l'Italia bisogna moltiplicare per 3 gli alimentari e per le altre cose bisogna fare una moltiplicazione per 2, ma non sempre. Quanto poteva costare in Italia un lavoro del genere? 50€? 100€? boh.. ditemi voi.


 A mio parere, rispetto all'Italia, mi conviene comprare il biglietto d'aereo per la Lituania perché tra il viaggio, fare i lavori dal dentista, salutare i suoceri e tornare a casa mi rimangono ancora due spiccioli per un pizza-cinema il sabato sera. Che ladri!

