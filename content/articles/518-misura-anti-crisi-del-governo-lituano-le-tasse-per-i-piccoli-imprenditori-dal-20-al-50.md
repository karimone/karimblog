Title: Misura anti crisi del governo lituano. Le tasse per i piccoli imprenditori dal 20% al 50%
Date: 2009-01-20
Category: Lituania
Slug: misura-anti-crisi-del-governo-lituano-le-tasse-per-i-piccoli-imprenditori-dal-20-al-50
Author: Karim N Gorjux
Summary: Come misura anti crisi il governo lituano ha deciso di aumentare le tasse alle piccole aziende e i liberi professionisti (come me) che passano da un 20% di tasse ad un 50%[...]

Come misura anti crisi il governo lituano ha deciso di aumentare le tasse alle piccole aziende e i liberi professionisti (come me) che passano da un 20% di tasse ad un 50% circa.  
  
La reazione del popolo non è stata di quelle migliori, a Vilnius ci sono stati degli scontri in piazza con 30 arresti e una decina di feriti, anche in altre città ci sono state manifestazioni in piazza, ad esempio qui a Klaipeda, a Kaunas, a Tauragé ed altre piccole città, ma l'unica ad avere riscontri violenti è stata Vilnius.


 In Italia non se ne parla, ma a Riga c'è stata una manifestazione popolare circa una settimana fa che è finita con scontri tra i cittadini e la polizia, allo stesso modo è successo a Sofia in Bulgaria. Le motivazioni sono sempre le stesse: la crisi, la mancanza di soldi e soprattutto gli interventi del governo.


 Non so come andrà a finire qui in Lituania. A mio avviso, tartassare la povera gente portando le tasse ad oltre il 50% è un azione degna di un genio del male. Uccidere i piccoli per salvare i grandi. 

 Sarebbe stato decisamente più sensato aumentare le tasse del 10% o il 15% per un anno, ma dando una giustificazione plausibile e informando meglio le persone. Portare la tassazione a livello scandinavo senza adeguarne i servizi è da incoscienti.


 A proposito di incoscienza, spero che con il tempo i lituani si prendano più cura della politica, le ultime elezioni hanno portato un governo di persone di spettacolo al potere, decisamente una coscienza del paese che non si addice al nazionalismo lituano.


 In Italia intanto ci si preoccupa di Kakà che è rimasto al Milan.  
  
  


