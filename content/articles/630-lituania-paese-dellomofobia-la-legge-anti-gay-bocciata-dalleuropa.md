Title: Lituania paese dell'omofobia: la legge anti gay bocciata dall'Europa
Date: 2011-02-02
Category: Lituania
Slug: lituania-paese-dellomofobia-la-legge-anti-gay-bocciata-dalleuropa
Author: Karim N Gorjux
Summary: Qualche settimana fa il programma l'Altra Europa di Radio24 mi ha contattato per discutere della legge anti gay di cui si vociferava qualche blog. Per parlarne ho fatto chiamare mia moglie che[...]

[![](http://www.karimblog.net/wp-content/uploads/2011/02/omofobia.jpg "omofobia")](http://www.karimblog.net/wp-content/uploads/2011/02/omofobia.jpg)Qualche settimana fa il programma l'Altra Europa di Radio24 mi ha contattato per discutere della legge anti gay di cui si vociferava qualche blog. Per parlarne ho fatto chiamare mia moglie che dopo una ricerca su internet nei vari siti in lituano non ha trovato nemmeno una parola al riguardo.


 Ieri sul Fatto Quotidiano un articolo riporta la questione soprattutto dopo i**l monito dell'Unione Europea**, ma qui in Lituania della questione omosessuale se ne parla poco. Personalmente ho visto qualcosa tempo fa su un programma di discussione molto seguito: "*Valanda su Ruta*" (un'ora con Ruta), ma nient'altro e soprattutto non si parlava di questa legge.  
  
Quindi sono costretto a rettificare che la legge che vieta di parlare dell'omosessualità è in discussione per essere approvata e a nostra discolpa, mia e di mia moglie, posso garantire che qui in Lituania non se ne parlato.


 Personalmente avevo già capito da parecchio tempo che l'omosessualità in Lituania non è tollerata;** una persona "*ambigua*" deve fare molta attenzione** a dove cammina in quanto la sua vita in questo paese può risultare davvero difficile e pericolosa. Cosa mi ha invece fatto strano è che **l'omosessualità tra donne è largamente diffusa e sopratutto tollerata dagli uomini**. Quando avevo ancora tempo di andare nei night club in Klaipeda (il night club è l'equivalente della nostra discoteca, per donne nude o spettacoli erotici bisogna entrare in un "erotic club" ndr), mi è capitato di vedere donne slanciarsi in effusioni lesbiche senza troppi pudori e soprattutto con il benestare degli uomini presenti.


 **La questione della tolleranza rispetto ai diversi non è solo limitata agli omosessual**i, ma si estende anche ai latini, i neri e gli arabi. In generale c'è un'accentuata xenofobia rispetto agli stranieri soprattutto se sono uomini. Tra i vari casi a cui ho assistito o di cui mi è stato riferito ci sono dei tentativi di pestaggio e persino aggressioni a mano armata. Personalmente mi sono sempre tirato fuori da ogni fastidio parlando il mio lituano maccheronico, ma non sono un gran frequentatore della "Klaipeda by night" quindi il mio caso fa statistica solo fino ad un certo punto. C'è anche da dire che Klaipeda è un piccolo paese piuttosto tranquillo.


 Link: [Articolo sul Fatto quotidiano](http://www.ilfattoquotidiano.it/2011/02/01/leuropa-dice-no-alla-legge-anti-gay-della-lituania/89574/)  
Link: [Puntata dell'Altra Europa del 18 Dicembre 2010](http://www.radio24.ilsole24ore.com/main.php?articolo=natale-azioni-unione-europea-volontariato-canalis-caffe-omosessualita-musica)

