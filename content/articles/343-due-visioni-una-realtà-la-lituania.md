Title: Due visioni, una realtà: la Lituania
Date: 2007-07-04
Category: Lituania
Slug: due-visioni-una-realtà-la-lituania
Author: Karim N Gorjux
Summary: Quando sento parlare i miei amici della Lituania come loro probabile meta turistica, l'invidio. L'invidio perché hanno visto una Lituania bella, divertente, piacevole. Hanno visto questa Lituania:

Quando sento parlare i miei amici della Lituania come loro probabile meta turistica, l'invidio. L'invidio perché hanno visto una Lituania bella, divertente, piacevole. Hanno visto questa Lituania:  
  
  


 Tutti contenti, belle donne, sorrisi, bei posti, niente preoccupazioni. Questo lo si assaggia in 10 giorni di vacanza. 

 Se invece si vive in Lituania, si può assaggiare un altro tipo di vita, quella che è vissuta **quotidianamente** dalla maggior parte delle persone che vivono in un paese freddo, dagli inverni lunghi e che si trovano ancora nel bel mezzo della transizione da **mentalità sovietica** a mentalità occidentale.


 Il [documentario](http://www.insightnewstv.com/d74/) lo potete vedere [qui](http://www.makeni.net/Insight/lithuania.wmv.asx), purtroppo non sono riuscito a scaricarlo e metterlo su youtube. Nel caso ci riuscissi, aggiornerò il post.


 PS: [Questo video turistico](http://youtube.com/watch?v=7x9ujVo3-GU) è decisamente migliore

