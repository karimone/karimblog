Title: Ritorno da Auce
Date: 2004-08-26
Category: Lettonia
Slug: ritorno-da-auce
Author: Karim N Gorjux
Summary: Sono tornato da Auce proprio oggi, non ho potuto pubblicare niente in questi duegiorni a causa del mio viaggio e a causa del mio nuovo raffreddore che mi hacolpito a casa di[...]

Sono tornato da Auce proprio oggi, non ho potuto pubblicare niente in questi due  
giorni a causa del mio viaggio e a causa del mio nuovo raffreddore che mi ha  
colpito a casa di Kristina, ma andiamo con ordine: sono partito la sera del 24  
agosto e non ho avuto tempo di pubblicare la mia pagina del diario che pubblico  
solo ora, ho viaggiato in treno da Riga ad Auce che e' un piccolo paesino di  
3000 abitanti non molto distante dalla Lituania. Il viaggio in treno e' stato  
eterno ed economico, ma consiglio a tutti di fare i viaggi in bus, qui nel nord  
d'europa si puo' viaggiare tramite eurotravel che e' economica ed in piu' molto  
comoda. (I treni sono talmente scomodi che e' impossibile dormire)

 Sono arrivato ad Auce alle 21 circa: la stazione sembra una casa abbandonata dal  
1944, il paese sono giusto due case in croce. Suggestivi i piccoli laghi e un  
castello che ho visto di sera, ma a causa della scarsa illuminazione pubblica  
non ho potuto fare foto. Sono stato ospitato a casa di Kristina dal 24 sera al  
26 mattina. Riguardo la casa non mi sbilancio in commenti, potete solo ritenervi  
fortunati di vivere in Italia. Ho patito il freddo come solo d'inverno lo  
patisco, mi sono raffreddato in poco meno di un'ora e ho passato il 25 agosto a  
testare tutti i metodi della nonna in versione sovietica. (Tra cui un rituale  
con i piedi nudi a bagno in una bacinella d'acqua calda e senape a cui mi sono  
categoricamente rifiutato). Mi sono trovato molto bene, il padre di Kristina non  
era casa perche' e' un marinaio e passa la maggior parte dell'anno nei mari del  
nord per dare una vita degna di essere vissuta alla famiglia. Da quanto ho  
capito e' uno di quei uomini che riuscirebbero a mangiare cose che farebbero  
schifo a Rambo (e' nato e vissuto in Siberia fino a 20 anni).


 La madre e i fratelli di Kristina sono gente molto ospitale e piacevole al di  
la' del freddo della casa, l'ambiente era molto caldo e mi ha stupito l'amore in  
cui vive la famiglia (nessun litigio, gridi o insulti). In questa casa vivono  
tanti valori che nelle nostre case stanno via via svanendo. La madre mi ha  
trattato come se fossi un figlio badando e curando ogni mia esigenza in questi  
due giorni, Il fratello e la sorella di Kristina sono due ragazzi di 17 e 12  
anni molto simpatici ed intelligenti. Diana che e' la sorella piu' piccola canta  
come Celine Dion e parla un discreto inglese (oltre il lettone e il russo) e  
suona il piano e il violino. Edgards suona la chitarra e la tromba e anche lui  
parla inglese. La cultura musicale e' quasi una religione in lettonia, dove i  
bambini gia' in tenera eta' vengono mandati a scuola di musica. (Ho dei filmati  
della bambina che canta con Kristina che suona il piano che sono incredibili).


 Per ora chiudo, non ho molto tempo e devo andare all'aeroporto dove tra   
qualche ora arrivera' Davide...vecchio compare..... 

 NB: Ho lasciato da parte la descrizione della casa o dello stato di degrado della   
citta' volontariamente, la lettonia non e' solo poverta'..


