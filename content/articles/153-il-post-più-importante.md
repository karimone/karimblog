Title: Il post più importante
Date: 2006-02-28
Category: Lituania
Slug: il-post-più-importante
Author: Karim N Gorjux
Summary: Era da un po' che pensavo di scrivere questo post, ho sempre rimandato aspettando il momento giusto e nel mentre, come i miei lettori-amici hanno notato, ho pubblicato dei post che ai[...]

Era da un po' che pensavo di scrivere questo post, ho sempre rimandato aspettando il momento giusto e nel mentre, come i miei lettori-amici hanno notato, ho pubblicato dei post che ai più sono risultati poco interessanti..  
  
In quel paesino di Centallo, tutti parlano e straparlano e dato che il passaparola può distorcere la verità, preferisco fare un po' di chiarezza direttamente qui nel mio angolo di internet. 

 In più di due anni ho raccontato (e censurato) molte delle mie esperienze sia in Lituania che in Italia; sembra di essere in una macchina del tempo, posso tornare indietro e leggere quando sono andato la prima [volta Telsai](http://www.karimblog.net/2005/01/31/viaggio-a-telsiai) o il viaggio che ho fatto da [Riga a Klaipeda](http://www.karimblog.net/2005/01/13/riga-klaipeda) con Davide. Questo blog è per me qualcosa di affascinante e intimo, una creatura che prova a trasmettere qualcosa di mio a chi ama leggere i blog su internet, ma per lo più è la parte artistica di me che, con alti e bassi, si esprime nei post che quasi periodicamente scrivo.


 Lunedì, il mio **futuro bimbo**, ha compiuto 15 settimane di domiciliazione nella pancia di Rita; 15 settimane sono circa 4 mesi e questo significa che a fine Luglio o inizio Agosto diventerò il papà di una bella bimba o di un bel bimbo. Sconvolti? Io non più di tanto, dopo 4 mesi ho avuto tempo di abituarmi all'idea... questa si che una palla colossale! 

 La notizia del bimbo ha reso felice tutta la mia famiglia e anche quella di Rita, perché un bimbo è un dono, qualcosa di magnifico, è la vita. Le conseguenze però, sono pazzesche, più passa il tempo e più mi rendo conto di cosa stia succedendo: devo comprare il bavaglino e il cappellino degli AC/DC su ebay, un Mac per il bimbo che deve imparare l'uso del computer, i film di Bruce Lee da fargli vedere all'ora di merenda... tutto questo indipendentemente dal fatto che sia maschio o femmina.


 A parte gli scherzi... Sento il peso delle responsabilità sulle mie spalle e ora non posso fare più il pirla (immagino che qualcuno se la riderà sotto i baffi dicendo.. "PIRLA!"), ora mi tocca ponderare seriamente le mie azioni. Ho delle responsabilità che hanno e avranno delle conseguenze nei confronti delle persone che amo, un bimbo è un essere umano e non si scherza. 

 Una volta un tizio, leggendomi la mano, mi disse: *"Tu cambierai quando avrai un figlio...."* non continuo, il resto è per me, me lo tengo, ma non credo molto in queste cose.. chissà.. forse ci ha azzeccato di culo :tongue\_tb:

 Sono contento, molto contento (e leggerissimamente spaventato).


