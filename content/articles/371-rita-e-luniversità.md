Title: Rita e l'università
Date: 2007-10-09
Category: Lituania
Slug: rita-e-luniversità
Author: Karim N Gorjux
Summary: Rita ha iniziato il corso di laurea in infermieristica qui a Cuneo. Praticamente le prende tutta la giornata dalle 8 alle 5 del pomeriggio ed infatti mi ritrovo a casa il più[...]

Rita ha iniziato il corso di laurea in infermieristica qui a Cuneo. Praticamente le prende tutta la giornata dalle 8 alle 5 del pomeriggio ed infatti mi ritrovo a casa il più delle volte da solo. (Escludiamo oggi perché Greta è malata ed è qui a casa con me.)

  
A quanto sembra il corso di laurea qui in Italia è molto più serio e completo del corso che seguiva Rita a Klaipeda in Lituania. Una volta ottenuta la laurea qui in Italia sei veramente infermiera, ciò significa che non devi andare in ospedale e farti spiegare il più delle cose, sai esattamente come devi fare perché durante i tre anni di corso ti sei schiavizzato abbastanza a fare tirocinio gratis per conto dell'università.  


  
In Lituania il corso di infermieristica dura 4 anni, Rita era già al terzo anno, ma non sapeva nemmeno fare un'iniezione. L'avevo già detto che la Lituania ha il peggior sistema sanitario d'Europa?

  


