Title: Vantaggi della vita in Lituania: la tranquillità
Date: 2008-10-09
Category: Lituania
Slug: vantaggi-della-vita-in-lituania-la-tranquillità
Author: Karim N Gorjux
Summary: Il primo vantaggio è la tranquillità, ormai sono 40 giorni che sono qui e sono rilassato. Tutti i problemi che ho sono dovuti all'Italia. Mi dispiace dirlo, soprattutto se leggo i post[...]

Il primo vantaggio è la tranquillità, ormai sono 40 giorni che sono qui e sono rilassato. Tutti i problemi che ho sono dovuti all'Italia. Mi dispiace dirlo, soprattutto se leggo i post di due anni fa, ma se fossi rimasto in Lituania avrei fatto un affare risparmiano tanti fastidi ed un sacco di soldi, ma ormai è fatta.


 Grazie al servizio [seguimi](http://www.poste.it/azienda/ufficipostali/a_seguimi.shtml "Seguimi sulle poste italiane") delle poste italiane mi vengono inoltrate le lettere dall'Italia per circa sei mesi, ero obbligato a farlo, ma avessi potuto farne a meno ne avrei fatto volentieri a meno. Ogni volta che ricevo una lettera dall'Italia mi irrigidisco e mi chiedo: c**osa vogliono ora?**

  Sarà per caso un bollo auto non pagato di una UNO 1000 fire del 1988 che ovviamente non ho mai avuto? Un pedaggio autostradale non pagato di un posto in Italia che non ho mai visto? Una bolletta telecom con dei servizi che non ho mai richiesto? Pubblicità?

 Ogni mese, qui in Lituania, ricevo le spese del condominio, non c'è nulla di supposto, pago** esattamente ciò che ho consumato**. Con l'eni, vedi [post precedente](http://www.karimblog.net/2008/10/08/call-center-eni-inferno-andata-e-ritorno/ "Call Center Eni: l'inferno andata e ritorno"), devo sperare che il tecnico legga il contatore prima che venga emessa la bolletta altrimenti pagherò la bolletta presunta e non quella effettiva.


 Internet: qui ho una connessione **DSL** (non è un errore, la "A" non c'è) da 6Mbit e la pago circa 27€ al mese. E' l'internet professionale, non ho dovuto prendere nessun numero telefonico, ho solo la linea dati. Pago solo il mese successivo al consumo e posso pagare direttamente al supermarket mentre pago la verdura. Attivazione in 4 giorni. **Nessuna penale** in caso di rescissione del contratto.


 In Italia sapete come funziona: canone, alice, numero di telefono... devi pagare 30€ al mese e poi ne paghi 45 e devi disattivare i numeri sporcaccioni usando un servizio a pagamento.


 Non ricevo mai **nessuna telefonata** al cellulare per vendermi qualcosa o rifilarmi qualche strano servizio, non riceviamo nemmeno nessuna pubblicità elettorale nella buca delle lettere ed è strano perché qui si vota Domenica! Anzi è stranissimo perché in tutto il paese non ho visto nemmeno un cartellone pubblicitario e poi ho capito... la campagna elettorale non è finanziata con i **soldi pubblici**.


 L'assicurazione della macchina costa **un decimo** di quella italiana ed è comprensiva di carta verde, il rapporto non è equo. Secondo gli stipendi, l'assicurazione dovrebbe costare un quarto o al massimo un terzo ed invece costa un decimo. Non esiste il bollo auto e nemmeno il bollino blu che qui si vede solo sulle banane.


 La posta viene consegnata solo a destinatario e **mai ad altri**. Forse è giusto, ma lo trovo scomodo. Se mi arriva un pacco mi lasciano un bigliettino nella casetta della posta e poi devo recarmi con un documento di identità alla posta centrale per ritirare il pacco. Ovviamente lo lasciano solo a me, nemmeno a mia moglie che porta il mio stesso cognome.


 Il mio amico [Massimo](http://www.tekneri.com "Tekneri") dice che in Italia si frusta solo la bestia che traina il carro, mi spiace, ma è vero. In Italia ero angosciato ogni volta che arrivava una lettera, ogni giorno è una sorpresa ed è brutto perché l'Italia mi piace, ma non è possibile che la vita sia basata tutta sul riuscire a cavarsela in un modo o nell'altro.


 Spremuto come un limone.


