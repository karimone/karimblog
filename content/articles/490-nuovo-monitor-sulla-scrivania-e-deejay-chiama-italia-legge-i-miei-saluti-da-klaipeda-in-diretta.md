Title: Nuovo monitor sulla scrivania e Deejay Chiama Italia legge i miei saluti da Klaipeda in diretta
Date: 2008-09-24
Category: Lituania
Slug: nuovo-monitor-sulla-scrivania-e-deejay-chiama-italia-legge-i-miei-saluti-da-klaipeda-in-diretta
Author: Karim N Gorjux
Summary: Oggi è stata una giornata produttiva. Finalmente ho comprato il monitor nuovo e da oggi lavoro davanti ad un bel Samsung T220 22 pollici attaccato al mio fido MacBook Pro. I motivi[...]

Oggi è stata una giornata produttiva. Finalmente ho comprato il monitor nuovo e da oggi lavoro davanti ad un bel [Samsung T220](http://www.samsung.com/us/consumer/detail/detail.do?group=computersperipherals&type=monitors&subtype=lcd&model_cd=LS22TWHSUV/ZA "Il Samsung T220 sul sito ufficiale") 22 pollici attaccato al mio fido [MacBook Pro](http://www.apple.com/it/macbookpro/ "MacBook Pro sul sito Apple Italia"). I motivi dell'acquisto sono molti, ma tra i tanti uno in particolare: **non ne potevo più**. Gli occhi mi facevano troppo male a stare ore e ore davanti ad uno schermo da 15 pollici. I portatili sono eccezionali per la mobilità, ma il lavoro d'ufficio necessita di spazi aperti.  
Il prossimo passo sarà uno stupendo [iMac 24](http://www.apple.com/it/imac/ "iMac 24 sul sito Apple Italia").  
  
[![Nuova scrivania](http://farm4.static.flickr.com/3107/2884840402_7d82fef0b8_m.jpg)](http://www.flickr.com/photos/kmen-org/2884840402/ "Nuova scrivania di karimblog, su Flickr")  
  
  
La mattina, quando sono davanti al computer, ho l'abitudine di sentirmi [Radio Deejay](http://www.deejay.it "Sito di Radio Deejay") usando [iTunes](http://www.apple.com/it/itunes/ "iTunes e iPod sul sito Apple Italia"). Non mi collego al sito perché è troppo uno spacco di palle, ma ho creato un file (che potete scaricare [qui](http://www.karimblog.net/wp-content/uploads/2008/09/radio-deejay.mov "Scarica il file per ascoltare Radio Deejay da iTunes")) che una volta dato in pasto ad iTunes potete sentirvi Radio Deejay come una qualsiasi altra canzone, l'importante è avere la connessione ad internet. Se usate il Mac ricordatevi di installare prima [Flip4Mac](http://www.flip4mac.com/wmv_download.htm "Download diretto di Flip4Mac"), riavviare e poi caricare il file di Radio Deejay su iTunes.


 Dei programmi di Radio Deejay il mio preferito è [Deejay Chiama Italia](http://mydeejay.deejay.it/deejaychiamaitalia "Pagina di Deejay Chiama Italia sul sito di Radio Deejay") condotto da Linus e Nicola; gli argomenti che trattano sono interessanti e alle volte leggono dei messaggi di persone particolari che scrivono dai posti più disparati :-)  
  
[audio:http://www.karimblog.net/wp-content/uploads/2008/09/deejay-2008-09-24.mp3]  


