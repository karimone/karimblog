Title: Auguri Greta: 1 anno!
Date: 2007-08-23
Category: altro
Slug: auguri-greta-1-anno
Author: Karim N Gorjux
Summary: Alle ore 11:10 locali (12:10 lituane) Greta compie 1 anno, Rita compie l'anniversario dello sforzo fisico più grande della sua vita e io ricordo dopo 1 anno con il lutto al braccia[...]

Alle ore 11:10 locali (12:10 lituane) **Greta compie 1 anno**, Rita compie l'anniversario dello sforzo fisico più grande della sua vita e io ricordo dopo 1 anno con il lutto al braccia la fine della mia **vita di cazzeggiatore**, fancazzista e irresponsabile. Oggi per noi è un gran giorno di festa, ma l'ambiente festoso è già presente in casa nostra già dall'arrivo dei genitori di Rita.


 Dimenticavo: Domenica 19 abbiamo **battezzato** Greta nella chiesa "rossa" di Centallo. La funzione si è tenuta in presenza delle persone a noi più intime, in totale eravamo in 10 con Greta. 

 Ieri ho portato i suoceri nella città grigia di [Torino](http://it.wikipedia.org/wiki/Torino) dove ho avuto l'accortezza di creare **una mappa su google** con i luoghi dei posti visitati (è la prima mappa che includo su un post).  
  
  
[Visualizzazione ingrandita della mappa](http://maps.google.it/maps/ms?f=q&hl=it&geocode=&ie=UTF8&msa=0&msid=100524896520482207968.0004384595d0c6ff43929&om=1&ll=45.066746,7.703717&spn=0.007911,0.040822&source=embed)  
  
  
  
Durante la nostra breve visita in Torino abbiamo:  
  
 3. Parcheggiato nei sotterranei di Piazza Vittorio Veneto.

   
 7.  Visitato il [museo del cinema](http://www.museonazionaledelcinema.org/) all'interno della [Mole Antonelliana](http://it.wikipedia.org/wiki/Mole_Antonelliana) e preso l'ascensore panoramico che sale fino a 85 metri (dei 167 della Mole). Roba da farmi **rabbrividire** ancora al pensiero.

  
 10. Dalla Mole fino in Piazza Castello, per proseguire fino alla [Fnac](http://www.fnac.it) in Via Roma. Tanta roba, ma niente di nuovo. Deluso.

  
12.  Una volta giunti in Piazza San Carlo siamo andati al [Museo Egizio](http://www.museoegizio.it/) dove all'uscita ho incontrato [MagoGae](http://www.magogae.net).

  
 15. Ritorno al parcheggio.

  
 18. Abbuffata al ristorante Messicano *Revolucion* in Corso Casale.

  


 Torino è una città con un centro veramente **bello e suggestivo**, la visita all'interno della Mole Antonelliana ha affascinato Rita e i suoi genitori anche se la barriera culturale e linguistica ha ridotto la visita ad una semplice ammirazione. **L'ascensore panoramico** è stupendo, l'ho preso per la seconda volta nella mia vita e mi ha fatto nuovamente tremare le gambe. Greta è più coraggiosa di me, non un lamento.


 Il museo egizio è bellissimo, ma bisogna avere la passione per apprezzarlo appieno. I **sarcofagi, le statue e le mummi**e sono a dir poco impressionanti. Purtroppo anche qui le informazioni sono solo in inglese, italiano e francese una limitazione per i suoceri.


 Nota di merito a fine serata per il ristorante "Revolucion" in corso casale 194b (a Torino ovviamente). Sembra di essere in Messico per un paio d'ore. I cibi sono fantastici, ben preparati e incredibilmente buoni. **Il prezzo è accettabile:** 25€/30€ a persona che in passato li ho spesi in posti decisamente peggiori di questo. Immancabile il **cantante messicano**. Con le fattezze di Maradona e il doppio dei chili suona la chitarra e canta in un modo incantevole. Il nostro "Carlos Santana" di Torino è stato così gentile da venire al nostro tavolo per cantare gli auguri a Greta, ma mentre io ho favorito di buon gusto, Greta piangeva spaventata. (Solo due cose la fanno piangere così: l'aspirapolvere quando Rita pulisce e il Carlo Santana della Revolucion).


   
[![Greta al ristorante "Revolucion"](http://farm2.static.flickr.com/1330/1213810758_c18c2c4b41_m.jpg)](http://www.flickr.com/photos/kmen-org/1213810758/ "Condivisione di foto")  


