Title: Culture Shock: imbattersi nelle abitudini e le stranezze del paese che ti ospita
Date: 2009-10-13
Category: Lituania
Slug: culture-shock-imbattersi-nelle-abitudini-e-le-stranezze-del-paese-che-ti-ospita
Author: Karim N Gorjux
Summary: Frequentando i lituani, ho iniziato a conoscerli un po' meglio, lo scontro tra culture è inevitabile, non lo posso negare, ma mi difendo semplicemente non prendendo troppo sul serio ciò che accade.[...]

[![](http://farm4.static.flickr.com/3335/3219036752_a8a0a8d8d5_m.jpg "Lituana con bambino")](http://www.flickr.com/photos/stefanvds/3219036752/)Frequentando i lituani, ho iniziato a conoscerli un po' meglio, lo scontro tra culture è inevitabile, non lo posso negare, ma mi difendo semplicemente non prendendo troppo sul serio ciò che accade. Ieri sera stavo rientrando in casa e alla porta del condominio c'era un signore che tentava inutilmente di entrare facendosi dare il numero della combinazione al telefono.  
  
In Lituania la porta d'ingresso del condominio è a combinazione, un po' come in una cassaforte; le vecchie case hanno una combinazione a tasti che devi premere contemporaneamente, mentre le case nuove hanno un numero assegnato ad ogni appartamento. Apro gentilmente la porta al signore dopo l'ennesimo tentativo e come ringrazimento ricevo il nulla completo, mi ha ignorato come se non ci fossi.  
Tempo indietro me la sarei presa e avrei dannato tutta la Lituania e i suoi abitanti, oggi mi faccio una risata, andrò mica a **rovinarmi la giornata per un maleducato?**

 I tornei di calciobalilla sono ricominciati, mi diverto, sparlicchio il lituano, conosco altra gente. E' divertente, ma al di fuori degli eventi organizzati non sono riuscito ad organizzare qualcosa come una partita di calcio. Ho chiesto a qualche mio amico se è una prassi normale che i lituani non siano mai in orario e tutti sono concordi nel dire che i lituani sono precisi solo negli appuntamenti importanti. Sarà vero? Io non ho ancora avuto a che fare con i lituani in ambito business, ma il parere degli amici non penso sia da considerare; è come chiedere ad un venditore di auto se le sue macchine sono buone. 

 Un atteggiamento particolare, confermato da mia moglie ed altre persone, riguarda il rapporto padre e figli. Sembra che gli uomini lituani siano padri solo dei figli della loro compagna; tempo addietro ho conosciuto una ragazza divorziata rimasta sola con il bambino, il padre si è rifatto una vita e non vuole sapere nulla né del figlio e ovviamente nemmeno della ex moglie. La cosa strana è che ora i suoi figli sono quelli che ha con la moglie attuale anche se non sono biologicamente suoi. Comportamento davvero strano e cinico, provo ad immedesimarmi nella situazione, ma al solo pensiero mi viene da vomitare.


 Qui in Lituania si dice che le donne facciano i figli per sè e non per la famiglia, se sei lituana o lituano, sicuramente puoi confermare o smentire la cosa, i commenti sono a tua disposizione.


