Title: Prima di addormentarmi (Oggi parte terza)
Date: 2005-08-11
Category: altro
Slug: prima-di-addormentarmi-oggi-parte-terza
Author: Karim N Gorjux
Summary: Oggi ho visto Paolo, voi lo conoscete grazie a quei pochi commenti che ha lasciato sul mio blog, ma io ho avuto la fortuna di frequentarlo per (non) abbastanza tempo. Sia io[...]

Oggi ho visto Paolo, voi lo conoscete grazie a quei pochi commenti che ha lasciato sul mio blog, ma io ho avuto la fortuna di frequentarlo per (non) abbastanza tempo. Sia io che Paolo avevamo la nostra ancora di salvezza in quel buco di 100 metri quadri in via Nasetta.  
  
Il poterci allenare alle 14 tutti i giorni, per due ore, scherzando e sudando ci ha legato in modo incredibile e ci ha permesso di non affogare vittima di questo mondo di merda. Quelle due ore al giorno erano più di un semplice allenamento, era una felicità semplice che ottenevamo con poco. Tutto grazie a delle grandi amicizie. Forse dentro di me vorrei che quei momenti tornassero perché ancora ricordo la salita per uscire dalla palestra. Sembrava di tornare nel mondo reale ed avevo la sensazione che i miei problemi fossero alla porticina bianca ad aspettarmi. I miei mostri attendevano il mio ritorno da quelle due ore di beatitudine.


 Torno a leggere.


