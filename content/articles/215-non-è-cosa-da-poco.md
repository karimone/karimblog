Title: Non è cosa da poco
Date: 2006-09-27
Category: Lituania
Slug: non-è-cosa-da-poco
Author: Karim N Gorjux
Summary: Ciao Enry, ma per caso hai scritto tu la voce dei "Soon" su wikipedia?No.Eppure c'è, sei citato anche tu!Vado a vedere! A Dopo!

*Ciao Enry, ma per caso hai scritto tu la voce dei "Soon" su wikipedia?  
  
No.  
  
Eppure c'è, sei citato anche tu!  
  
Vado a vedere! A Dopo!  
*

 Il giorno stesso ci vedemmo al [Meridianas](http://www.qedata.se/bilder/gallerier/litauiskt-galleri/klaipeda/klaipeda-meridianas.jpg) per un drink, eravamo io, Enrico e Mirko. Ovviamente la storia di Enrico Quinto presente su wikipedia divenne un ritornello ricorrente: *"Sai com'è, sono su wikipedia... è ovvio che ho ragione, io sono su wikipedia... eh eh, tu sei nessuno io invece... sono su wikipedia!"*.


 Scherzava, lo diceva ridendo, con il sorriso sulle labbra, ma era contento; essere su wikipedia non è cosa alla portata di tutti. Enrico era presente a causa delle sue performance come batterista nel gruppo dei Soon, gruppo musicale milanese meteora dei primi anni '90. Scherzando gli dicevo che avrei modificato la voce riguardante lui pubblicando tutte le cose più segrete che sapevo. Scherzavo, ma alla fine ho veramente scritto [la voce di Enrico Quinto](http://it.wikipedia.org/wiki/Enrico_Quinto), ovviamente nello stile wikipedia, ho appena iniziato, ma ho decisa intenzione di andare avanti.


