Title: La vita non è vita senza grietine
Date: 2007-06-08
Category: Lituania
Slug: la-vita-non-è-vita-senza-grietine
Author: Karim N Gorjux
Summary: Mia moglie per chi non lo avesse ancora capito è lituana. Vive in Italia e mi ha dato una bella, bellissima bambina italo lituana.

Mia moglie per chi non lo avesse ancora capito è lituana. Vive in Italia e mi ha dato una bella, bellissima bambina italo lituana.[![Prodotti Russi](http://farm1.static.flickr.com/253/535613502_4788218a93_m.jpg)](http://www.flickr.com/photos/kmen-org/535613502/ "Photo Sharing")

 Il più grande problema di ambientazione per mia moglie è la cucina. Rita vuole la panna acida ovvero quella specie di philadelpia andato a male che in Lituania è chiamato "grietine", vuole il latte rancido (kefiras), vuole i cetrioli fatti in una certa maniera... vuole una marea di schif ehm prelibatezze lituane che noi italiani nei nostri supermercati non abbiamo mai visto.


 Blinai, cepelinai, guldunai... nulla ha un senso per un lituano senza il grietine. Ma la provincia di Cuneo è piena di sorprese ed infatti veniamo a sapere che a [Gerbola](http://maps.google.it/maps?f=q&hl=it&q=gerbola&sll=41.442726,12.392578&sspn=8.644402,15.314941&ie=UTF8&om=1&ll=44.536164,7.566512&spn=0.00803,0.014956&z=16&iwloc=addr) a 4km da Centallo una signora ha avuto il coraggio di aprire un piccolo negozio di alimentare e importare prodotti russi per accontentare le tante "Rite" che ormai vivono nei dintorni di Cuneo.


 ![Kvas](http://www.karimblog.net/wp-content/uploads/2007/06/images.jpeg "Kvas")  
C'è di tutto in quel negozio, la birra, la tanto amata Kvas di [eio](http://eiochemipensavo.diludovico.it/2005/09/08/mi-ricordo-lunione-sovietica/), i cetrioli in barattolo sott'olio con spezie strane e persino i DVD in lingua russa di vari film e cartoni. Tutto rigorosamente proveniente dalla Russia o dai paesi dell'ex-russia.


 Penso di fare un piacere a tutti includendo i dati per raggiungere questo utilissimo negozio per una sana vita coniugale italo - exURSS.


 *Alimentari Girasole di Abramova Elena  
  
Via Fossano, 69/A  
  
Frazione Gerbola • VillaFalletto (CN)  
  
Tel 0171 94 29 89 • Cell 340 83 00 141  
  
**[Email](mailto:leandr_TA@RIMUOVIlibero.iT)*

  
Per una scansione del biglietto da visita, cliccate [qui](http://farm1.static.flickr.com/253/535887027_621c0e367c.jpg) e [qui](http://farm1.static.flickr.com/236/535886961_e48b905d61.jpg).


