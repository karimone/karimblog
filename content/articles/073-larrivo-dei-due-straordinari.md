Title: L'arrivo dei due str...aordinari
Date: 2005-05-15
Category: Lituania
Slug: larrivo-dei-due-straordinari
Author: Karim N Gorjux
Summary: Non e' che sia stato il massimo ed e' forse anche questo che ha contribuito ad ammalarmi in questi due giorni. L'andata e ritorno a Vilnius tra sole e pioggia ha lasciato[...]

Non e' che sia stato il massimo ed e' forse anche questo che ha contribuito ad ammalarmi in questi due giorni. L'andata e ritorno a Vilnius tra sole e pioggia ha lasciato il suo segno. Ieri avevo la febbre, ma oggi sto meglio. Sul minibus strettissimo e scomodissimo ho conosciuto Gitta una ragazza bella e naturalmente bionda (come la natura vuole da queste parti). Gitta e' simpatica e giovanile esattamente come sua figlia Roberta che a soli 6 anni parla lituano e italiano. Purtroppo la mia macchina fotografica non era con me quindi spero che Davide pubblichi le foto fatte con la sua fotocamera nell'album dedicato a Derio e Samuele.


 I due loschi figuri, di cui la sola presenza viola le leggi del quieto vivere, stanno bene. Penso che si siano gia' ambientati e che stiano iniziando a divertirsi come si deve. Nella [galleria](http://www.kmen.org/gallery) potete vedere con i vostri occhi le loro condizioni psicofisiche. Bevono, dormono e si divertono. Samuele ha gia' fatto le sue belle figure di merda :lol\_tb: altro che cabiria...


 Passo e chiudo

