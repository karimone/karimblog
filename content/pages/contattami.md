Title: Contattami
Date: 2010-12-03 10:20
Slug: contattami
Authors: Karim N Gorjux
Summary: Contatta l'autore di karimblog.net
HeaderClass: bg_header_contact
FirstLineHeader: Contattami

Se volete contattarmi in privato per qualsiasi motivo, fatelo usando questo formi di contatto. Specificate bene chi siete, come volete essere contattati e il motivo. Vi risponderò al più presto nelle modalità che preferite.

**Nota bene:** fatemi sapere chi siete e perché mi contattate, tanti mi hanno contattato come se fossi [l'oracolo di Klaipeda](http://www.karimblog.net/2010/03/29/loracolo-di-klaipeda-risponde-la-posta-dei-lettori/).

<div>
    <form
      action="https://formspree.io/xyygzkqw"
      method="POST"
    >
      <label>
        Your email:
        <input type="text" name="_replyto">
      </label>
      <label>
        Your message:
        <textarea name="message"></textarea>
      </label>
      <button type="submit">Send</button>
    </form>
</div>
