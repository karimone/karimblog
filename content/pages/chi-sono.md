Title: Chi sono?
Date: 2016-05-10
Category: 
Slug: chi-sono
Authors: Karim N Gorjux
Summary: Chi sono? Chi e' la mente dietro karimblog.net?
HeaderClass: bg_header_about
FirstLineHeader: Chi
SecondLineHeader: Sono?

Ciao, mi chiamo Karim Gorjux, ho 36 anni e per dieci anni ho vissuto tra Italia e Lituania e ora vivo a Melbourne in Australia.

Lavoro come sviluppatore web full stack per una startup di Melbourne, ma puoi vedere il [mio profilo linkedin](https://au.linkedin.com/in/karimgorjux) per maggiori dettagli. Sono anche presente su [facebook](https://www.facebook.com/karim.gorjux) e su [twitter](https://twitter.com/kngorjux) (anche se lo uso poco)

Ho abitato per svariati anni, a Klaipeda dove sono nati i miei due bambini e ho conosciuto tante persone. **In Lituania ho vissuto momenti belli e momenti brutti che ho raccontato su questo blog** alle volte di impulso e con rabbia, ma altre volte più pacatamente.

Leggendo il mio blog, è probabile che con il tempo **troverai degli articoli discordanti**, ma questo è solo un [blog](http://it.wikipedia.org/wiki/Blog) ovvero un diario personale che segue le mie vicende e i miei cambiamenti, non è un prodotto giornalistico e non vuole sostituirsi ad una visione oggettiva della Lituania in particolare quindi tutto ciò che ho scritto e (giustamente) opinabile.

Prima di commentare i miei articoli, tieni ben presente che **io non sono la verità assoluta** e che il mio intento non è di scrivere l'ultima parola su tutto. Abbi rispetto di me e degli altri lettori anche se non ti trovi d'accordo con me o con loro.

Se vuoi contattarmi, vai al form di contatto, su [questa pagina](http://www.karimblog.net/contattami/).

Pagina aggiornata il 10 Maggio 2016
