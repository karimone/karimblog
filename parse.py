# -*- coding: utf-8 -*-
"""
parse the articles
"""
import abc
import requests
from collections import namedtuple, defaultdict
from os import listdir
from os.path import dirname, join, realpath

import re
from typing import List

import regex

CWD = dirname(realpath(__file__))  # we are in the dir that contains the settings
ARTICLES_PATH = join(CWD, 'content/articles')

ScanResult = namedtuple("ScanResult", ["result", "article"])


class ReParser:

    def __init__(self, regexp: str, content: str):

        self._regexp = regexp
        self._content = content
        self.re = regex.search(pattern=regexp, string=content)

    def get_group(self, group_name: str):
        return self.re.group(group_name) if self.re else []

    def get_captures(self) -> List:
        return self.re.captures() if self.re else []


class Scan(abc.ABC):

    regexp = None

    def __init__(self, article):
        self._article = article
        self.re_parser = ReParser(regexp=self.regexp, content=article)

    @abc.abstractmethod
    def scan(self):
        pass


class ScanSummary(Scan):

    regexp = r"[S|s]ummary:(?P<content>.*\n)"

    def scan(self):
        content = self.re_parser.get_group("content")
        return True if content else False


class ScanHttpLink(Scan):

    regexp = r"(http|ftp|https)://([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?"

    def scan(self) -> dict:
        urls = self.re_parser.get_captures()
        result = defaultdict(lambda: [])
        for url in urls:
            try:
                response = requests.get(url)
                result[response.status_code].append(url)
            except Exception:
                result[500].append(url)
        return result


def get_articles():
    return [join(ARTICLES_PATH, article) for article in listdir(ARTICLES_PATH)]


def get_file():
    pass


def parse(article):
    with open(article) as f:
        article_read = f.read()
        return {
            "summary": ScanSummary(article_read).scan(),
            "links": ScanHttpLink(article_read).scan(),
        }


def scan_articles():
    result = {
        "summary": 0,
        "links": {}
    }
    articles = get_articles()
    result["articles"] = len(articles)

    for step, article in enumerate(articles, start=1):
        print(f">> Open article: {article}")
        parse_result = parse(article)
        print(f">> Links scan: {parse_result['links']}")
        result["summary"] += 1 if parse_result['summary'] else 0
        result['links'][article] = parse_result["links"]

    return result


def test_scan_article():
    with open("result.txt", "w") as r:
        result = scan_articles()
        r.write(f"Articles: {result['articles']}\n")
        r.write(f"Summaries: {result['summary']}\n")
        for article_url, links in result['links'].items():
            article = article_url.split("/")[-1]
            r.write(f"Article: {article}\n")
            for code, urls in links.items():
                r.write(f"\t{code}: {urls}\n")


if __name__ == "__main__":
    test_scan_article()

