# karimblog
My italian blog built using pelican


# Docker

Build the image. You need this command just to build the image.
```bash
docker build -t karimblog/pelican .
```

When the image is built, you can run the container and connect to it using:
```bash
docker run --rm -ti -p 8000:8000 -v $PWD:/app --name karimblog karimblog/pelican bash
```

When the bash prompt is loaded type
```
make devserver
```

Connect to http://localhost:8000 (or http://127.0.0.1:8000)

The `devserver` put pelican in a refreshing mode, any editing recompile the site on realtime.

### Windows notes

If you have problem, use the powershell instead of the git bash.
Consider the [windows terminal](https://devblogs.microsoft.com/commandline/introducing-windows-terminal/)

If you have problem with the path, include that explicitly (replace `$PATH`)
 ```
 docker run --rm -ti -p 8000:8000 -v PATH:/app --name karimblog karimblog/pelican bash
 ```
