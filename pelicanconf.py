#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
from os.path import curdir, join, dirname, realpath

AUTHOR = 'Karim N Gorjux'
SITENAME = "Karim's Blog"

CWD = dirname(realpath(__file__))  # we are in the dir that contains the settings
PATH = 'content'

TIMEZONE = 'Australia/Melbourne'

DEFAULT_LANG = 'it'

# Feed generation is usually not desired when developing
# FEED_ALL_ATOM = None
FEED_ALL_ATOM = 'feeds/all.atom.xml'
FEED_ALL_RSS = 'feeds/all.rss.xml'
# CATEGORY_FEED_ATOM = None
# TRANSLATION_FEED_ATOM = None
# AUTHOR_FEED_ATOM = None
# AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = join(CWD, "themes/noris")
THEME_STATIC_DIR = "static"
SITEURL = "http://localhost:8000"

PAGE_PATHS = ['pages']
ARTICLE_PATHS = ['articles']


# keeps the compatibility with the old blog in mezzanine
PAGE_URL = '{slug}/'
PAGE_SAVE_AS = '{slug}/index.html'

OUTPUT_PATH='public/'


YEAR_ARCHIVE_SAVE_AS = 'posts/{date:%Y}/index.html'
MONTH_ARCHIVE_SAVE_AS = 'posts/{date:%Y}/{date:%m}/index.html'
DAY_ARCHIVE_SAVE_AS = 'posts/{date:%Y}/{date:%b}/{date:%d}/index.html' 

ARTICLE_URL = 'blog/{slug}/'
ARTICLE_SAVE_AS = 'blog/{slug}/index.html'

DISQUS_SITENAME = "karimone"