FROM debian:buster-slim

ENV USER user

RUN echo -e '\033[36;1m ******* INSTALL SYSTEM ******** \033[0m'

RUN apt-get update && \
    apt-get install python3-minimal \
    make \
    lynx \
    git \
    python3-pip \
    python3-setuptools \
    python3-wheel \
    --no-install-recommends -y && \
    rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/* /root/.cache && \
    apt-get clean

RUN echo -e '\033[36;1m ******* INSTALL PACKAGES ******** \033[0m'
RUN pip3 install pelican Markdown typogrify Pillow Piexif

RUN echo -e '\033[36;1m ******* ADD USER ******** \033[0m'
RUN useradd -m -s /bin/bash ${USER}

USER ${USER}

EXPOSE 8000
WORKDIR /app
