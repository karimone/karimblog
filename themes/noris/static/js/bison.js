$(document).ready(function () {
    accordion_sidebarBlog();
    create_separator_in_post_author();
});

/**accordion()
 * used in the blog sidebar -filterpanel.html
 * called in base.html
 */
function accordion_sidebarBlog() {
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function () {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        }
    }
}
/**create_separator_in_post_author()
 * load the load_windowSize_and_modify_authorSeparator() to create a separator in the row
 *
 */
function create_separator_in_post_author() {
    load_windowSize_and_modify_authorSeparator();
    $(window).resize(function () {
        load_windowSize_and_modify_authorSeparator();


    });
}

/**load_windowSize_and_modify_authorSeparator()
 * * in every post there is the name of the author followed by the publish date.
 * this function inserts a <br> or a space in the middle, according to the display resolution
 */
function load_windowSize_and_modify_authorSeparator() {
    if ($(window).width() <= 479) {

        // is mobile device
        $(".separator").html('<br>');


    }
    else {
        $(".separator").html('&nbsp;');

    }
}